//
//  MaximusTests.swift
//  MaximusTests
//
//  Created by kpit on 07/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import XCTest
@testable import Maximus
@testable import Alamofire
@testable import ObjectMapper

class MaximusTests: XCTestCase, RideManagerDelegate {
    
    var multiplerouteexpect:XCTestExpectation?
    var myRidesexpect:XCTestExpectation?
    var testobj:[RouteList]?
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testSearchLocationDetails() {
        
        let expect = expectation(description: "startlocationtest")
        let googleObj = GooglePlacesService()
        
        googleObj.findPlaceName(placeName: "pune") { response in
            
            if let json = response.result.value
            {
                let place = Mapper<PlaceDetails>().map(JSON: json as! [String:Any])
                let placeList:[Predictions] = place!.predictions!
                XCTAssertNotNil(placeList)
            }
            expect.fulfill()
            
        }
        waitForExpectations(timeout: 10) { error in
            if ((error) != nil) {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
        
    }
    
    func testSearchPlaceId(){
        //place id for pune "ChIJARFGZy6_wjsRQ-Oenb9DjYI"
        let expect = expectation(description: "findlocaitonIdtest")
        let googleObj = GooglePlacesService()
        googleObj.locationDetails(placeId:"ChIJARFGZy6_wjsRQ-Oenb9DjYI", locationType: "", completionHandler: { (response) in
            
            if let theJSONData = try? JSONSerialization.data(
                withJSONObject: response.result.value!,
                options: []) {
                
                if let parsedData = try? JSONSerialization.jsonObject(with: theJSONData) as! [String:Any]{
                    
                    let result = parsedData["result"] as? [String:Any]
                    let geometricLocation = result?["geometry"] as? [String:Any]
                    let location = geometricLocation?["location"] as? [String:Float]
                    
                    XCTAssertNotNil(location)
                }
                
                expect.fulfill()
            }
        })
        
        waitForExpectations(timeout: 10) { error in
            if ((error) != nil) {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testMultipleRoutes(){
        
        multiplerouteexpect = expectation(description: "testmultipleroutes")
        
        var jsonObject = [String:Any]()
        var sourceItem :[String:Any]?
        var destinationItem :[String:Any]?
        let rideManagerObj = RideManager()
        
        sourceItem = ["name":"pune"]
        var latlngSourceItem = [String:Any]()
        latlngSourceItem["latitude"] =  18.5204
        latlngSourceItem["longitude"] = 73.8567
        sourceItem?["latlong"] = latlngSourceItem
        
        
        destinationItem = ["name":"mumbai"]
        var latlngDestItem = [String:Any]()
        latlngDestItem["latitude"] = 19.0760
        latlngDestItem["longitude"] = 72.8777
        destinationItem?["latlong"] = latlngDestItem
        
        jsonObject["source_details"] = sourceItem
        jsonObject["destination_details"] = destinationItem
        
        rideManagerObj.rideManagerDelegate = self
        rideManagerObj.getRoutesForLatLng(data:jsonObject)
        
        waitForExpectations(timeout: 20) { error in
            if ((error) != nil) {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            else{
                XCTAssertTrue(true, "Pass")
            }
        }
    }
    
    func sendRouteDirectionDetails(directionResult: [String : Any]) {
        
        if let obj:DirectionDetails = directionResult["data"] as? DirectionDetails {
            
            if (obj.directionResult?.routeList?.count)! > 0{
                
                testobj = obj.directionResult?.routeList
                XCTAssertNotNil(obj.directionResult?.routeList)
                multiplerouteexpect?.fulfill()
//
//                let rideManagerObj = RideManager()
//                let sourceLatLong = ["latitude" : testobj?[0].legs?[0].startLocation?.lat,
//                                     "longitude" : testobj?[0].legs?[0].startLocation?.lng
//                ]
//                
//                let sourceDic = ["latlong" : sourceLatLong,
//                                 "name": "pune"] as [String : Any]
//                
//                let destinationLatLong = ["latitude" : testobj?[0].legs?[0].endLocation?.lat,
//                                          "longitude" :  testobj?[0].legs?[0].endLocation?.lat]
//                
//                let destinationDic = ["latlong" : destinationLatLong,
//                                      "name" : "mumbai"] as [String : Any]
//                
//                let routes:String = (testobj?.toJSONString(prettyPrint: true))!
//                
//                let ride = [StringConstant.planned:true,
//                            "type" : 0,
//                            "end_location" : destinationDic,
//                            "start_location" : sourceDic,
//                            "start_date" : "2017-04-30T07:21:41",
//                            "finish_date" : "2017-05-30T07:21:41",
//                            "title" : "testride",
//                            "route" : routes
//                    ] as [String : Any]
//                
//                let json:NSString = ride.jsonEncode()
//                print("json", json)
//                rideManagerObj.rideManagerDelegate = self
//                rideManagerObj.createRide(parameter: ride)
            }
        }
    }
    
    
    func rideCreateSuccess(data:[String:Any])
    {
        let createModel:RideCreateResponse = data["data"] as! RideCreateResponse
        XCTAssertNotNil(createModel.route)
        multiplerouteexpect?.fulfill()
    }
    
    
    func testGetMyRides(){
        
        myRidesexpect = expectation(description: "myridesExpect")

        let rideManagerObj = RideManager()
        let concurrentQueue = DispatchQueue(label: "queuename", attributes: .concurrent)
        concurrentQueue.sync {
            rideManagerObj.rideManagerDelegate = self
            rideManagerObj.getMyPlannedRides(type: "\(RequestURL.URL_RIDE_BIKER.rawValue)/recommended", pageNo: 0, size: 10)
        }
        
        waitForExpectations(timeout: 15) { error in
            if ((error) != nil) {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            else{
                XCTAssertTrue(true, "Pass")
            }
        }

    }
    
    func getMyPlannedRideSuccess(data:[String:Any]){
        
        if let obj:[RideCreateResponse] = data["data"] as? [RideCreateResponse]
        {
            XCTAssertNotNil(obj.count)
            myRidesexpect?.fulfill()
        }
    }

}
