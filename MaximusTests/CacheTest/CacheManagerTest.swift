//
//  CacheManagerTest.swift
//  Maximus
//
//  Created by Admin on 24/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import XCTest
@testable import Maximus


class CacheManagerTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testReadWriteCache() {
        let str = "testReadWriteCache"
        let key = "testReadWriteCacheKey"
        
        DataCache.instance.write(object: str as NSCoding, forKey: key)
        let cachedString = DataCache.instance.readString(forKey: key)
        
        XCTAssert(cachedString == str)
    }
    
    func testWriteCacheToDisk() {
        let str = "testWriteCacheToDisk"
        let key = "testWriteCacheToDiskKey"
        
        let expectation = self.expectation(description: "Write to disk is an asynchonous operation")
        
        DataCache.instance.write(object: str as NSCoding, forKey: key)
        DataCache.instance.cleanMemCache()
        
        // wait for write to disk successful
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(1 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC)) {
            let cachedString = DataCache.instance.readString(forKey: key)
            XCTAssert(cachedString == str)
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 2) { (error) in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    
    func testHasDataOnDiskForKey() {
        let str = "testHasDataOnDiskForKey"
        let key = "testHasDataOnDiskForKeyKey"
        let expectation = self.expectation(description: "Write to disk is an asynchonous operation")
        
        DataCache.instance.write(object: str as NSCoding, forKey: key)
        
        // wait for write to disk successful
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(1 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC)) {
            let hasDataOnDisk = DataCache.instance.hasDataOnDiskForKey(key: key)
            XCTAssert(hasDataOnDisk == true)
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 2) { (error) in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testHasDataOnMemForKey() {
        let str = "testHasDataOnMemForKey"
        let key = "testHasDataOnMemForKeyKey"
        
        DataCache.instance.write(object: str as NSCoding, forKey: key)
        let hasDataOnMem = DataCache.instance.hasDataOnMemForKey(key: key)
        
        XCTAssert(hasDataOnMem == true)
    }
    
    func testCleanCache() {
        let str = "testCleanCache"
        let key = "testCleanCacheKey"
        let expectation = self.expectation(description: "Clean is an asynchonous operation")
        
        DataCache.instance.write(object: str as NSCoding, forKey: key)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(1 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC)) {
            DataCache.instance.cleanAll()
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(1 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC)) {
                let cachedString = DataCache.instance.readString(forKey: key)
                XCTAssert(cachedString == nil)
                expectation.fulfill()
            }
        }
        
        waitForExpectations(timeout: 3) { (error) in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
