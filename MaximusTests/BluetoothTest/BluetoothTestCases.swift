//
//  BluetoothTestCases.swift
//  Maximus
//
//  Created by Admin on 09/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import XCTest
@testable import Maximus
@testable import CoreBluetooth

class BluetoothTestCases: XCTestCase {
    
    var deviceList = [Any]()
    
    var expect =  XCTestExpectation()
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class. 
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
//    func testScanBluetoothDevice(){
//        NotificationCenter.default.addObserver(
//            self,
//            selector: #selector(updateBlelistNotification(notification:)),
//            name: NSNotification.Name(rawValue: "BLELSIT_NOTIFICATION"),
//            object: nil)
//
//        NotificationCenter.default.addObserver(
//            self,
//            selector: #selector(BleConnectNotification(notification:)),
//            name: NSNotification.Name(rawValue: "BLECONNECT_NOTIFICATION"),
//            object: nil)
//
//        expect = expectation(description: "startScan")
//       // BluetoothManager.bluetoothManagerObj.startScanning()
//
//        waitForExpectations(timeout: 10) { error in
//            if ((error) != nil) {
//                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
//            }
//        }
//    }
    
    func testConnectToDevice(){
    }
    
    // MARK:Notification Handlers
    func updateBlelistNotification(notification:NSNotification){
        deviceList = notification.object as! [Any]
        XCTAssertNotNil(deviceList)
        expect.fulfill()
    }
    
    func BleConnectNotification(notification:NSNotification){
        print("Connected Ble device is  \(notification.object)")
    }

}
