//
//  RecommendedTest.swift
//  MaximusTests
//
//  Created by APPLE on 09/10/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import XCTest
@testable import Maximus
@testable import Alamofire
@testable import ObjectMapper

class RecommendedTest: XCTestCase {
    var mockData: NSString?
    var analyticsAPIManager: AnalyticsApiManager?
    var incorrectMockData: NSString?
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        analyticsAPIManager = AnalyticsApiManager()
        mockData = "[{\"id\":227,\"card_id\":\"605ec90ad48ba9d72e91f1345f725ad2\",\"card\":{\"score\":58.87106984825879,\"sticky\":true,\"actions\":[{\"icon\":\"\",\"order\":1,\"title\":\"Plan a Ride\",\"tonedown\":false,\"emphasize\":true,\"action_data\":{\"source\":{\"lat\":18.548086,\"lng\":73.796483,\"city\":\"Pune\",\"state\":\"Maharashtra\",\"place_name\":\"Pune\",\"full_address\":\"Mont Vert Biarritz - Ph Ii, Someshwarwadi-Pashan Trail, Pashan, Pune, Maharashtra, 411021, India\"},\"destination\":{\"lat\":17.0977719,\"lng\":74.44998439999999,\"city\":\"Palus\",\"state\":\"Maharashtra\",\"place_name\":\"Palus\",\"full_address\":\"Tasgaon Road, Kirloskar Wadi, Palus, Sangli District, Maharashtra, 416312, India\"}},\"action_type\":\"Plan a Ride\"}],\"card_id\":\"605ec90ad48ba9d72e91f1345f725ad2\",\"visuals\":{\"image\":[\"\"],\"title\":\"Ride to Palus\",\"distance\":{\"unit\":\"km\",\"value\":175.47810429512384},\"duration\":{\"unit\":\"sec\",\"value\":0},\"description\":\"Ride to Palus\",\"end_location\":\"Palus\",\"start_location\":\"Pune\"},\"biker_id\":344986,\"priority\":10,\"item_type\":\"destination-cross-recommendations\",\"allow_dismiss\":false,\"date_generated\":\"2018-10-04 14:28:10.851262\",\"date_published\":\"2018-10-04 14:29:03.449480\",\"destination_id\":null}}]"
        
        incorrectMockData = "[{\"id\":227,\"dummyData\": \"111\" ,\"card_id\":\"605ec90ad48ba9d72e91f1345f725ad2\",\"card\":{\"score\":58.87106984825879,\"sticky\":true,\"actions\":[{\"icon\":\"\",\"order\":1,\"title\":\"Plan a Ride\",\"tonedown\":false,\"emphasize\":true,\"action_data\":{\"source\":{\"lat\":18.548086,\"lng\":73.796483,\"city\":\"Pune\",\"state\":\"Maharashtra\",\"place_name\":\"Pune\",\"full_address\":\"Mont Vert Biarritz - Ph Ii, Someshwarwadi-Pashan Trail, Pashan, Pune, Maharashtra, 411021, India\"},\"destination\":{\"lat\":17.0977719,\"lng\":74.44998439999999,\"city\":\"Palus\",\"state\":\"Maharashtra\",\"place_name\":\"Palus\",\"full_address\":\"Tasgaon Road, Kirloskar Wadi, Palus, Sangli District, Maharashtra, 416312, India\"}},\"action_type\":\"Plan a Ride\"}],\"card_id\":\"605ec90ad48ba9d72e91f1345f725ad2\",\"visuals\":{\"image\":[\"\"],\"title\":\"Ride to Palus\",\"distance\":{\"unit\":\"km\",\"value\":175.47810429512384},\"duration\":{\"unit\":\"sec\",\"value\":0},\"description\":\"Ride to Palus\",\"end_location\":\"Palus\",\"start_location\":\"Pune\"},\"biker_id\":344986,\"priority\":10,\"item_type\":\"destination-cross-recommendations\",\"allow_dismiss\":false,\"date_generated\":\"2018-10-04 14:28:10.851262\",\"date_published\":\"2018-10-04 14:29:03.449480\",\"destination_id\":null}}]"
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        mockData = nil
        analyticsAPIManager = nil
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testRecommendedMockData() {
        let responsedata = analyticsAPIManager?.sendSuccessCallback(jsonString: mockData!, requestType: URLRequestType.GetRecommendedRides)
        let list:[RecommendedResponse] = responsedata![StringConstant.data] as! [RecommendedResponse]
        XCTAssertTrue((list.count) > 0, "Recommendation response present")

    }
    
    func testRecommendedWithEmptyString() {
        let responsedata = analyticsAPIManager?.sendSuccessCallback(jsonString: "", requestType: URLRequestType.GetRecommendedRides)
        if responsedata![StringConstant.keyRequestType] as? String == URLRequestType.GetRecommendedRides {
            if let obj:[RecommendedResponse] = responsedata![StringConstant.data] as? [RecommendedResponse] {
                XCTAssertFalse((obj.count) < 0, "Recommendation response not present")
            }
        }
    }
    
    func testRecommendedWithIncorrectResponse() {
        let responsedata = analyticsAPIManager?.sendSuccessCallback(jsonString: incorrectMockData!, requestType: URLRequestType.GetRecommendedRides)
        let list:[RecommendedResponse] = responsedata![StringConstant.data] as! [RecommendedResponse]
        XCTAssert(list.count > 0, "Recommended response in correct format")
    }
}
