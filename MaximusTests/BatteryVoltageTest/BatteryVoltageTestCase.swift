//
//  BatteryVoltageTestCase.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 12/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import XCTest
@testable import Maximus

class BatteryVoltageTestCase: XCTestCase,RideManagerDelegate {
    
    var promiseBatteryVoltage:XCTestExpectation?
    
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testBikeBatteryMeanVoltage(){
        
        promiseBatteryVoltage = expectation(description: "batteryVoltage")
        let rideManagerObj = RideManager()
        rideManagerObj.rideManagerDelegate = self
        rideManagerObj.getBatteryMeanBikeBatteryVoltage()
        
        waitForExpectations(timeout: 20) { error in
            if ((error) != nil) {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            else{
                XCTAssertTrue(true, "Pass")
            }
        }
    }
    
    
    internal func onFailure(Error: [String : Any])
    {
        XCTFail("Error :\(Error["data"]!)")
    }
    
    func getMeanBikeBatteryVoltageSuccess(data: [String : Any]) {
        
        if let obj:MeanBikeBatteryVoltageModel = data["data"] as? MeanBikeBatteryVoltageModel {
            XCTAssertNotNil(obj.mean_bike_battery_ignition_off_voltage)
            promiseBatteryVoltage?.fulfill()
        }else{
            XCTFail("Model Empty")
        }
    }
    
}
