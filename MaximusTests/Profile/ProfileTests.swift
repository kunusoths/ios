//
//  ProfileTests.swift
//  Maximus
//
//  Created by Namdev Jagtap on 12/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import XCTest
@testable import Maximus
@testable import Alamofire
@testable import ObjectMapper

class ProfileTests: XCTestCase,UserManagerDelegate {
    var defaultVehiclesexpect:XCTestExpectation?
    var expect =  XCTestExpectation()

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testGetBikeModes()  {

        expect = expectation(description: "bikeModelList")
        let user = UserManagerHandler()
        user.userManagerDelegate = self
        user.getAllBikes()

        waitForExpectations(timeout: 20) { error in
            if ((error) != nil) {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            else{
                XCTAssertTrue(true, "Pass")
            }
        }
    }
    
    func testGetBiker() {
        
        expect = expectation(description: "BikerInfo")
        let user = UserManagerHandler()
        user.userManagerDelegate = self
        user.getBiker(bikeID:MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID))
        waitForExpectations(timeout: 15) { error in
            if ((error) != nil) {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            else{
                XCTAssertTrue(true, "Pass")
            }
        }
    }
    
    func testUpdateBiker(){
        
        expect = expectation(description: "BikerInfo")
        let user = UserManagerHandler()
        user.userManagerDelegate = self
        let json :[String:Any]=["share_location":"true",
                                "id":0]
        user.updateBiker(parameter: json)
        waitForExpectations(timeout: 15) { error in
            if ((error) != nil) {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            else{
                XCTAssertTrue(true, "Pass")
            }
        }
        
    }
    
    func testUpdateVehicles(){
        
        expect = expectation(description: "BikerInfo")
        let user = UserManagerHandler()
        user.userManagerDelegate = self
        
        let id:Int? = nil
        
        let json : [String:Any] = ["id":id ?? 0,
                                   "registration_month":1,
                                   "registration_year":2017,
                                   "company":"company" ,
                                   "model":"model" ]


        user.updateVehicles(parameter: json)
        
        waitForExpectations(timeout: 15) { error in
            if ((error) != nil) {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            else{
                XCTAssertTrue(true, "Pass")
            }
        }
        
    }
    
    internal func onFailure(Error: [String : Any])
    {
        XCTFail("Error :\(Error["data"]!)")
    }
    
    func updateAllBikesList(data:[String:Any])  {
        print(data)
        if let bikeModels:AllBikesList = data["data"] as? AllBikesList {
            XCTAssertNotNil( bikeModels.bikeList!)
            expect.fulfill()
        }
    }
    
    func onGetBikerSuccess(data:[String:Any]) {
        print(data)
        if let biker:Biker = data["data"] as? Biker {
            XCTAssertNotNil(biker)
            expect.fulfill()
        }
    }
    
    func onVehiclesUpdate(data:[String:Any]){
        print("Profile:\(data)")
        if let vehicle:DefaultVehicle = data["data"] as? DefaultVehicle {
            XCTAssertNotNil(vehicle)
            expect.fulfill()
        }
    }
    
}
