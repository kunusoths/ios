/*
 * dm_hid_commands.h
 *
 *  Created on: 12-oct-2016
 *      Author: kpit
 */

#ifndef DM_HID_COMMANDS_H_
#define DM_HID_COMMANDS_H_

#include <stdbool.h>
#include <stdint.h>

struct buzzer{
	uint8_t BUZZER_ON_OFF;
	uint8_t VOLUME;

}__attribute__ ((packed));

struct key_pressed_times{
	uint8_t TIMES_PRESSED;

}__attribute__ ((packed));

struct set_led_rb{
	uint8_t		LED_RED_BLUE;
	uint8_t   LED_ON_OFF;
	uint16_t  BLINK_TIME;

}__attribute__ ((packed));

#endif /* BATTERY_VOLTAGE_COMMANDS_H_*/
