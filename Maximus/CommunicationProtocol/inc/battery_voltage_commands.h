/*
 * battery_voltage_commands.h
 *
 *  Created on: 12-Apr-2016
 *      Author: kpit
 */

#ifndef BATTERY_VOLTAGE_COMMANDS_H_
#define BATTERY_VOLTAGE_COMMANDS_H_

#include <stdbool.h>
#include <stdint.h>

struct battery_level{
	uint8_t battery_voltage;

}__attribute__ ((packed));

#endif /* BATTERY_VOLTAGE_COMMANDS_H_*/
