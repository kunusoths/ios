/*
 * rgb_command.h
 *
 *  Created on: 22-june-2016
 *      Author: kpit
 */

#ifndef RGB_COMMAND_H_
#define RGB_COMMAND_H_

#include <stdbool.h>
#include <stdint.h>

struct rgb_set_colour {
	uint8_t RGB_CHANNEL;
	uint8_t IR;
	uint8_t IG;
	uint8_t IB;
	uint16_t Ttime;

}__attribute__ ((packed));


struct rgb_set_brightness{
	uint8_t RGB_CHANNEL;
	uint8_t BRIGHTNESS_LEVEL;
}__attribute__ ((packed));

struct rgb_blinky{
	uint8_t RGB_CHANNEL;
  uint16_t Ttime;
}__attribute__ ((packed));
	
	




#endif /* RGB_COMMAND_H_ */


