/*
 * geometry_commands.h
 *
 *  Created on: 07-Apr-2016
 *      Author: kpit
 */

#ifndef GEOMETRY_COMMANDS_H_
#define GEOMETRY_COMMANDS_H_

#include <stdbool.h>
#include <stdint.h>

struct geometry_pixel {
	uint8_t XH;
	uint8_t XL;
	uint8_t YH;
	uint8_t YL;
	uint8_t Color_R;
	uint8_t Color_G;
	uint8_t Color_B;
}__attribute__ ((packed));

struct geometry_line {
	uint8_t X1H;
	uint8_t X1L;
	uint8_t Y1H;
	uint8_t Y1L;
	uint8_t X2H;
	uint8_t X2L;
	uint8_t Y2H;
	uint8_t Y2L;
	uint8_t Color_R;
	uint8_t Color_G;
	uint8_t Color_B;
}__attribute__ ((packed));

struct geometry_rectangle {
	uint8_t X1H;
	uint8_t X1L;
	uint8_t Y1H;
	uint8_t Y1L;
	uint8_t X2H;
	uint8_t X2L;
	uint8_t Y2H;
	uint8_t Y2L;
	uint8_t Color_R;
	uint8_t Color_G;
	uint8_t Color_B;
	uint8_t Fill;
}__attribute__ ((packed));

struct geometry_round_rectangle {
	uint8_t X1H;
	uint8_t X1L;
	uint8_t Y1H;
	uint8_t Y1L;
	uint8_t X2H;
	uint8_t X2L;
	uint8_t Y2H;
	uint8_t Y2L;
	uint8_t RadiusH;
	uint8_t RadiusL;
	uint8_t Color_R;
	uint8_t Color_G;
	uint8_t Color_B;
	uint8_t Fill;
}__attribute__ ((packed));

struct geometry_arc {
	uint8_t XH;
	uint8_t XL;
	uint8_t YH;
	uint8_t YL;
	uint8_t RadiusXH;
	uint8_t RadiusXL;
	uint8_t RadiusYH;
	uint8_t RadiusYL;
	uint8_t ArcQuadrant;
	uint8_t Color_R;
	uint8_t Color_G;
	uint8_t Color_B;
	uint8_t Fill;
}__attribute__ ((packed));

struct geometry_circle {
	uint8_t XH;
	uint8_t XL;
	uint8_t YH;
	uint8_t YL;
	uint8_t RadiusH;
	uint8_t RadiusL;
	uint8_t Color_R;
	uint8_t Color_G;
	uint8_t Color_B;
	uint8_t Fill;
}__attribute__ ((packed));

struct geometry_elipse {
	uint8_t XH;
	uint8_t XL;
	uint8_t YH;
	uint8_t YL;
	uint8_t RadiusXH;
	uint8_t RadiusXL;
	uint8_t RadiusYH;
	uint8_t RadiusYL;
	uint8_t Color_R;
	uint8_t Color_G;
	uint8_t Color_B;
	uint8_t Fill;
}__attribute__ ((packed));

struct geometry_traingle {
	uint8_t X1H;
	uint8_t X1L;
	uint8_t Y1H;
	uint8_t Y1L;
	uint8_t X2H;
	uint8_t X2L;
	uint8_t Y2H;
	uint8_t Y2L;
	uint8_t X3H;
	uint8_t X3L;
	uint8_t Y3H;
	uint8_t Y3L;
	uint8_t Color_R;
	uint8_t Color_G;
	uint8_t Color_B;
	uint8_t Fill;
}__attribute__ ((packed));

#endif /* GEOMETRY_COMMANDS_H_ */
