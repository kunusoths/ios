/*
 * image_commands.h
 *
 *  Created on: 12-Apr-2016
 *      Author: kpit
 */

#ifndef IMAGE_COMMANDS_H_
#define IMAGE_COMMANDS_H_

#include <stdbool.h>
#include <stdint.h>

struct image_icon {
	uint8_t XH;
	uint8_t XL;
	uint8_t YH;
	uint8_t YL;
	uint8_t WidthH;
	uint8_t WidthL;
	uint8_t HeightH;
	uint8_t HeightL;
	uint32_t Background;
	uint32_t Foreground;
	uint16_t img_size;
	//uint8_t *img_data;
}__attribute__ ((packed));
//struct image_icon_2_colour {
//	uint8_t XH;
//	uint8_t XL;
//	uint8_t YH;
//	uint8_t YL;
//	uint8_t WidthH;
//	uint8_t WidthL;
//	uint8_t HeightH;
//	uint8_t HeightL;
//	uint32_t Background;
//	uint32_t Foreground;
//	uint16_t img_size;
//	//uint8_t *img_data;
//}__attribute__ ((packed));
#endif /* IMAGE_COMMANDS_H_ */
