/*****************************************************************************
 * Copyright (C) 2017 KPIT Technologies Ltd.                                 *
 *                                                                           *
 * This file contains all the preprocessor directives,structures  and function
 * declaration related to communication protocol.                                        *
 *                                                                           *
 *****************************************************************************/

/**
 * \mainpage %Display Module
 */

/**
 * \defgroup CP CP Component
 *
 * \author Imran Pathan
 * \date :  05/18/2017
 *
 * communication protocol (CP) component is library where parsing of commands based on command group is done on receiving packets over BLE(payload
 * data of command is received in multiple packets as BLE has limitation to recieve 20 bytes maximum at a time,here extra fields are attached before
 * header that are total packet and current packet ).Same library on sending part of packets forms header ,crc calculaton + footer  for actual payload
 * data.Packets are recieved on DM or Mobile side until total packets are equal to current packet.
 * Timer on sending device is started ,so that within timeout of timer if  acknowledgement doesnt come from receiving device ,on sending device we can say that payload data is not recieved
 * on receiving device.
 *
 */


/**
 * \ingroup CP
 * \defgroup CPHeader CP Header
 *
 * \author Imran Pathan
 * \date :  05/18/2017
 * \file communication_protocol.h
 * \brief CP Header implementation
 *
 *On receiving device : function that recieves packet data are implemented on recieving side and ultimately parsing of packets takes place .Here
 *indication callback function is called where we get command group ,command ,size of payload and pointer to actual payload buffer.
 *At the same time response/acknowledgement is sent to sending device so that it can understand if command(all the data packet) is recieved correctly
 *on receiving side.
 *On sendind device : API are called,here for every command API are defined through which we can send data specific to commnads.here formation of packet takes place (header+payload data +(crc+footer))
 *timeout timer is started on sending device so  that within certain time if we dont get response from receiving device then sending device can confirm that payload data(command specific data)is not
 *received on receiving device.
 */

/**
 * \addtogroup CP Header
 * @{
 *
 */





#ifndef CP_H_
#define CP_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include "cp_crc.h"
#include "cp_if.h"

#ifdef __linux__
#include <android/log.h>

#define  LOG_TAG   "communication-protocol"

#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#elif __APPLE__

#else
#ifdef STM32L151xE
//#include "stm32l1xx.h"
//#else
//#include "stm32l0xx.h"
#endif
//#include "stm32l0xx_hal.h"

#endif


/**
 * @brief     cp infra action' Below preprocessor directives are to used only by communication protocol.
 *
 */


/* Source ID */
#define SOURCE_ID_PHONE    0x10


#define SOP                0xBBBB 	// Start of packet
#define EOP                0xEEEE	// End of packet

#define TRUE 	           0x01
#define FALSE	           0x00

#define CP_BUFFER_SIZE     50	//100
uint8_t buffer[CP_BUFFER_SIZE];


#define NO_CMD_DATA '\0'
#define ZERO_PAYLOAD_SIZE 0x00

#define REQUEST_TIME_OUT 6000 //mSec

/* KBike Display Protocol Error Definitions */
#define COMMUNICATION_PROTOCOL_VALID_DATA			0x10
#define COMMUNICATION_PROTOCOL_BAD_HEADER_FORMAT	0x11
#define COMMUNICATION_PROTOCOL_BAD_FOOTER_FORMAT	0x21

#define COMMUNICATION_PROTOCOL_BAD_LENGTH			0x12
#define COMMUNICATION_PROTOCOL_INVALID_COMMAND	    0x13
#define COMMUNICATION_PROTOCOL_NO_BUFFER_SPACE	    0x14
#define COMMUNICATION_PROTOCOL_REQUEST_TIME_OUT	    0x15
#define CRC16_CHECKSUM_ERROR                        0x16

/* Response from Display */
#define COMMUNICATION_PROTOCOL_SUCCESS_ACK		    0x4F
#define COMMUNICATION_PROTOCOL_FAIL_ACK		        0x46
#define COMMUNICATION_PROTOCOL_HEARTBEAT_ACK	    0xFF

#define COMMUNICATION_PROTOCOL_MSG_TYPE_COMMAND				0x01
#define COMMUNICATION_PROTOCOL_MSG_TYPE_GEN_REJECT			0x02
#define COMMUNICATION_PROTOCOL_MSG_TYPE_RSP_ACCEPT			0x03
#define COMMUNICATION_PROTOCOL_MSG_TYPE_RSP_REJECT			0x04

#define COMMUNICATION_PROTOCOL_INVALID_CMD	        0x00

#ifndef MAX
# define MAX(x, y) ((x) > (y) ? (x) : (y))
#endif





/**
 * @brief     cp infra action' global variables to store total packet count and current packet count in case of more then on packet received over BLE.
 *
 */
static uint8_t gPrevious_tpck=0;
static uint8_t gTpck_count=0;



/**
 * @brief     cp infra action' parse status code
 * This enumeration describes the status of the parsing of command.
 */

enum communication_protocol_parse_result {
	PARSE_ERROR, PARSE_SUCCESS
};

struct kbike_communication_protocol;

//This  structure is used in header in place of src_id field as per communication protocol
//typedef union
//{
//	struct{
//		uint8_t dmVersion    : 4;
//		uint8_t compression  : 1;
//		uint8_t encryption   : 1;
//		uint8_t sourceId     : 2;
//	}basicInfo;
//}CP_BasicCpPayloadInfoTypeDef;


/**
 * @brief Structure defining header format of communication protocol
 *
 */

typedef struct {
	uint16_t sop;   /**< \brief start of packet. */
	uint8_t src_id; /**< \brief source id(data source : dm or cm). */
	uint8_t session_id; /**< \brief The session identifier. */
	uint8_t msg_type;	/**< \briefCOMMAND, RSP_ACCEPT OR RSP_REJECT. */
	uint8_t cmd_group;  /**< \brief command group. */
	uint8_t cmd;       /**< \brief command . */
	uint16_t size_payload; /**< \brief payload size of payload data. */
}__attribute__ ((packed)) communication_protocol_header;

/**
 * @brief Structure defining footer format of communication protocol
 *
 */
typedef struct {
	uint32_t crc;  /**< \brief CRC 32 value. */
	uint16_t eop;  /**< \brief end of packet. */
}__attribute__ ((packed)) communication_protocol_footer;

typedef struct {
	uint8_t total_packets; /**< \brief Total packets */
	uint8_t packet_num;  /**< \brief Current packet number of total packet. */
}__attribute__ ((packed)) packet_header;

/* prototype for end connection*/
void end_connection(struct kbike_communication_protocol *session);

/**
 *  @brief CP Infra confirmation callback function
 *  Callbacks for when a reply is received to a command that we sent
 *
 */
//struct communication_protocol_cfm {
//
//	void (*callback_cfm)(struct kbike_communication_protocol *, uint8_t);
//
//}__attribute__ ((packed));

/**
 * @brief CP Infra structure defining callback function Indication callback function
 * Callbacks for indicating when we received a new command. The return value
 * indicates whether the command should be rejected or accepted
 */
//struct communication_protocol_ind {
//
//	//	bool (*callback_ind)(struct kbike_communication_protocol *session);
//	bool (*callback_ind)(struct kbike_communication_protocol *, uint8_t, uint8_t, void *, uint32_t);
//
//}__attribute__ ((packed));

/**
 * @brief CP Infra structure defining callback function try send
 * Callback to send protocol packet to remote device
 */
//struct transport_cb {
//	bool (*try_send)(void *, size_t);
//};

/**
 * @brief CP Infra structure defining buffer that will store all the useful  data related to recieved  command
 * Callback to send protocol packet to remote device
 */
struct in_buf {
	bool active;
	uint8_t message_type; /**< \brief COMMAND, RSP_ACCEPT OR RSP_REJECT. */
	uint8_t buf[CP_BUFFER_SIZE]; /**< \brief payload buffer to store complete command data */
	uint16_t data_size;    /**< \brief refer to data size of payload buffre  */
};

/**
 * @brief CP Infra structure defining out message info (response on receiving command)
 *
 */
struct out_msg_info {
	uint8_t msg_type;  /**< \brief COMMAND, RSP_ACCEPT OR RSP_REJECT. */
	uint8_t cmd_grp;  /**< \brief command group. */
	uint8_t cmd;    /**< \brief command . */
};

//struct communication_cp_callbacks {
//	struct communication_protocol_ind *ind_cb;
//	struct communication_protocol_cfm *cfm_cb;
//	struct transport_cb *transport_cb;
//}__attribute__ ((packed));

/**
 * @brief CP Infra structure defining complete session data
 *
 */
struct kbike_communication_protocol {

	CP_CommunicationProtocolHandlers endpoint;

	uint16_t ref;

	char inbuf[20];   /**< \brief buffer to collet packet data on receiving ,20 bytes can be received at a time as its BlE limitation. */
	char outbuf[20];  /**< \brief buffer to collet packet data on sending ,20 bytes can be sent at a time as its BlE limitation. */

	uint16_t imtu;
	uint16_t omtu;

	struct in_buf in;   /**< \brief structure containing payload buffer and payload size etc. */

	struct out_msg_info msginfo;

	bool shutdown;

}__attribute__ ((packed));

/******************************************************************************************
 *   			Display Protocol Interface to UL -> Upper Layer - Start
 *  	Function Calls declared below are APIs to call from application level.
 * ****************************************************************************************/


/***********************************************************************************
 * 						Response Handler Functionality prototype- Start
 ***********************************************************************************/

static bool cp_parse_resp(struct kbike_communication_protocol *session);








/*****************  Display Protocol Interface to UL -> Upper Layer - End *******************/

#ifdef __cplusplus
}
#endif
#endif /* CP_H_ */
