/*
 * flash_icon_commands.h
 *
 *  Created on: 12-Apr-2016
 *      Author: kpit
 */

#ifndef FLASH_ICON_COMMANDS_H_
#define FLASH_ICON_COMMANDS_H_

#include <stdbool.h>
#include <stdint.h>

struct flash_icon {
	uint8_t ICON_ID;
	uint16_t icon_size;

}__attribute__ ((packed));

#endif /* FLASH_ICON_COMMANDS_H_ */
