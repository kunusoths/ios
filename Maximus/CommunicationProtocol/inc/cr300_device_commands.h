/*
 * image_commands.h
 *
 *  Created on: 12-Apr-2016
 *      Author: kpit
 */

#ifndef CR300_DEVICE_COMMANDS_H_
#define CR300_DEVICE_COMMANDS_H_

#include <stdbool.h>
#include <stdint.h>

struct cr300_info {

	uint16_t cr300_data;
	//uint8_t *img_data;
}__attribute__ ((packed));

struct cr300_gps_info {

	uint16_t cr300_gps_data;
	//uint8_t *img_data;
}__attribute__ ((packed));



#endif
