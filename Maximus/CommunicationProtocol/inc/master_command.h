#ifndef MASTER_COMMAND_H_
#define MASTER_COMMAND_H_

#include <stdint.h>

struct bg_color {
	uint8_t color_R;
	uint8_t color_G;
	uint8_t color_B;
}__attribute__ ((packed));

struct date_time{
  
   uint8_t	HH;
	 uint8_t  MIN;
   uint8_t	SS;
	 uint8_t  WD;
	 uint8_t	DD;
   uint8_t	MM;
   uint8_t	YY;
	}__attribute__ ((packed));

struct sleep_mode{
	uint8_t sleep;
	uint8_t sleep_timeout;
}__attribute__ ((packed));

struct display_set_window{
  
   uint8_t	X;
	 uint8_t  Y;
   uint8_t	Width;
	 uint8_t  Height;

	}__attribute__ ((packed));
	

#endif //MASTER_COMMAND_H_
