#if defined(__linux__) || defined (__APPLE__)  

#include <stdio.h>
#include <sys/time.h>
#include <signal.h>


#ifndef timer_INCLUDED
#define timer_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif


int start_timer(int, void (*)(void*), void*);

void stop_timer(void);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif

#endif

