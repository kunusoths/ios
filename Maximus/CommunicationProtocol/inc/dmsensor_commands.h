/*
 * string_command.h
 *
 *  Created on: 12-Apr-2016
 *      Author: kpit
 */

#ifndef DMSENSOR_COMMAND_H_
#define DMSENSOR_COMMAND_H_

#include <stdbool.h>
#include <stdint.h>



struct dm_sensor_als {
uint16_t alsdata;
}__attribute__ ((packed));

struct dm_sensor_proximity {
	uint8_t proxymitydata;
	
}__attribute__ ((packed));

struct dm_sensor_aux {
	uint16_t auxdata;
	
}__attribute__ ((packed));


#endif /* DMSENSOR_COMMAND_H_ */
