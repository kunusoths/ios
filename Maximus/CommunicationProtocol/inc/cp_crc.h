/**
  ******************************************************************************
  * File Name          : crc16_calc.h
  * Description        : This file contains all the function prototypes for
  *                      the crc16_calc.c file
  ******************************************************************************
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __crc16_calc_H
#define __crc16_calc_H
#ifdef __cplusplus
extern "C" {
#endif

/**********************variables and includes********************************************************/

#include "unistd.h"

	 
/**************function prototypes******************************************************/



unsigned short CP_Crc16(unsigned char *cc_msg_data, unsigned short cc_data_length);

unsigned int CP_Crc32(unsigned int *msg_data, unsigned int data_length);

uint32_t CP_CalculateSwCrc32(uint8_t * fileName,uint32_t  sizeOfFile);



#ifdef __cplusplus
}
#endif

#endif /* __crc16_calc_H */

/**
  * @}
  */
