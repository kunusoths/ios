
#ifndef DMSENSOR_CONFIG_COMMANDS_H_
#define DMSENSOR_CONFIG_COMMANDS_H_

#include <stdbool.h>
#include <stdint.h>

//typedef struct{
//	uint8_t ps1_th_lsb;
//	uint8_t ps1_th_msb;
//	uint8_t ps2_th_lsb;
//	uint8_t ps2_th_msb;
//	uint8_t ps3_th_lsb;
//	uint8_t ps3_th_msb;	
//} ps_thres;

//typedef struct{
//	uint8_t ps1_current;
//	uint8_t ps2_current;
//	uint8_t ps3_current;
//}ps_curr;

//typedef struct{
//	uint8_t als_lo_th_lsb;
//	uint8_t als_lo_th_msb;
//	uint8_t als_hi_th_lsb;
//	uint8_t als_hi_th_msb;		
//} als_thres;


struct init_sensors {
	//	uint8_t ps_current;  //proximity current for single PS1 LED Drive
//	ps_curr ps_current;  // as a structure
	uint8_t ps_current[3];  //as an array
	uint8_t	tasklist;  //CHLIST parameter
	uint8_t int_cfg;   //int cfg parameter
	uint8_t irq_enable;
	uint8_t irq_mode1;
	uint8_t irq_mode2;
	uint8_t ps12_led_select;
	uint8_t ps3_led_select;
	uint8_t adcgain;   //PS and ALS IR adc gain
	uint8_t ps_adcmisc;   // PS signal range/ sensitivity
	uint8_t adcmux[3];    //IR LED MUX for PS 
	uint8_t ps_adc_counter;
	uint8_t ps_encoding;   //no value to be assigned
	uint8_t ps_hysteresis; //no value to be assigned
	uint8_t ps_history;    //no value to be assigned

	uint8_t als_ir_adc_misc;
	uint8_t als_ir_adc_mux;
	uint8_t als_ir_adc_counter;
	uint8_t aux_adc_mux;

	uint8_t als_vis_adc_misc;
	uint8_t als_vis_adc_gain;
	uint8_t als_vis_adc_counter;
	uint8_t als_encoding;
	uint8_t als_hysteresis;
	uint8_t als_history;
	
	uint8_t measrate;  //Si114x measurement rate
	uint8_t ps_rate;
	uint8_t als_rate;
	uint8_t adc_offset;

//	ps_thres ps_threshold;
	
	uint8_t ps1_th_lsb;  //proximity(ps) threshold
	uint8_t ps1_th_msb;
	uint8_t ps2_th_lsb;
	uint8_t ps2_th_msb;
	uint8_t ps3_th_lsb;
	uint8_t ps3_th_msb;

//als_thres	als_threshold;
	
	uint8_t als_lo_th_lsb;  //als threshold
	uint8_t als_lo_th_msb;
	uint8_t als_hi_th_lsb;
	uint8_t als_hi_th_msb;

}__attribute__ ((packed));


struct deinit_sensors {
	uint8_t disable_si114x;


}__attribute__ ((packed));

struct als_config {
	uint8_t	tasklist;  //CHLIST parameter related to only ALS
	
	uint8_t irq_enable;
	uint8_t irq_mode1;
	uint8_t adcgain; //for IR ADC GAIN
	
	uint8_t als_ir_adc_misc;
	uint8_t als_ir_adc_mux;
	uint8_t als_ir_adc_counter;
	uint8_t aux_adc_mux;

	uint8_t als_vis_adc_misc;
	uint8_t als_vis_adc_gain;
	uint8_t als_vis_adc_counter;
	uint8_t als_encoding;
	uint8_t als_hysteresis;
	uint8_t als_history;	
  uint8_t als_rate;
	
	//als_thres	als_threshold;
  uint8_t als_lo_th_lsb;  //als threshold
	uint8_t als_lo_th_msb;
	uint8_t als_hi_th_lsb;
	uint8_t als_hi_th_msb;

}__attribute__ ((packed));

struct proximity_config {
	uint8_t ps_current[3];  //as an array	
	uint8_t	tasklist;  //CHLIST parameter	related only to PS
	
	uint8_t irq_enable;
	uint8_t irq_mode1;
	uint8_t irq_mode2;	
	uint8_t ps12_led_select;
	uint8_t ps3_led_select;
	uint8_t adcgain;   //PS and ALS IR adc gain
	uint8_t ps_adcmisc;   // PS signal range/ sensitivity
	uint8_t adcmux[3];    //IR LED MUX for PS and ALS IR
	uint8_t ps_adc_counter;
	uint8_t ps_encoding;
	uint8_t ps_hysteresis;
	uint8_t ps_history;	
	uint8_t ps_rate;
//	ps_thres ps_threshold;
	uint8_t ps1_th_lsb;  //proximity(ps) threshold
	uint8_t ps1_th_msb;
	uint8_t ps2_th_lsb;
	uint8_t ps2_th_msb;
	uint8_t ps3_th_lsb;
	uint8_t ps3_th_msb;


}__attribute__ ((packed));

struct gesture_config{
	uint8_t TEST;


}__attribute__ ((packed));

struct als_enable_disable {
		uint8_t on_off;  //flag to turn ALS off/on
	uint8_t	tasklist;  //CHLIST parameter related to only ALS -- needed

	uint8_t irq_enable;    //not actually needed, needed only to disable interrupts
	uint8_t irq_mode1;     //not actually needed, needed only to disable interrupts
	uint8_t als_rate;  //--needed


}__attribute__ ((packed));

struct proximity_enable_disable {
	uint8_t on_off;	   //flag to turn PS off/on
	uint8_t ps_current[3];	
	uint8_t	tasklist;	

	uint8_t irq_enable;
	uint8_t irq_mode1;
	uint8_t irq_mode2;
	uint8_t ps12_led_select;
	uint8_t ps3_led_select;
	uint8_t ps_rate;


}__attribute__ ((packed));

struct gesture_enable_disable {
	uint8_t TEST;


}__attribute__ ((packed));

struct common_config {
	uint8_t measrate;
  uint8_t adcoffset;
	uint8_t int_cfg;   //int cfg parameter common to both als and ps

}__attribute__ ((packed));


#endif /* DMSENSOR_CONFIG_COMMAND_H_ */


