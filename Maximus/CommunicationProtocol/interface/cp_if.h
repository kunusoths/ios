/****************************************************************************
 * Copyright (C) 2017 KPIT Technologies Ltd.                                *
 *                                                                          *
 * This file is part of communication protocol callback Infra.                                         *
 *                                                                          *
 ****************************************************************************/

/**
 * \mainpage %Display Module
 */

/**
 * \defgroup CP CP Component
 *
 * \author Imran Pathan
 * \date 17.05.17
 * communication protocol(CP) is library that contains logic for parsing of commands as per
 * communication protocol on receiving command .It also do packet formation while sending command as per communication protocol.
 */


/**
 * \ingroup CP
 * \defgroup CPInterface CP Interface
 *
 * \author Imran Pathan
 * \date 17.05.17
 *
 * \brief CP Interface implementation
 *
 * communication protocol callback(CP Infra header(cp_if.h) is file that contains all the structures,enum and preprocessor directives of various commands as per communication protocol(Wiki gitlab)
 */

/**
 * \addtogroup CP Interface
 * @{
 *
 */






#ifndef CP_IF_H_
#define CP_IF_H_

#ifdef __cplusplus
extern "C" {
#endif

#if !defined(__linux__) && !defined (__APPLE__)
//#include "u8g2.h"
//#include "u8g_arm.h"
#include <stdbool.h>
//#include "cp.h"
#else
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#endif




#define SOURCE_ID_CM	   0x11
#define SOURCE_ID_DM       0x12
#define SESSION_ID         0x00
#define MAX_INT            (65536 + 1 )  //value is greater then 2 bytes so enum max size becomes 4 bytes



/* Command Groups for Display Module (LCD) */

/**
 * @brief     cp infra action' Below preprocessor directives are to used to didentify command group and their commands
 *
 */

#define COMMUNICATION_PROTOCOL_CMD_GRP_DM_NEW_PARAMETERS          0x2B
#define COMMUNICATION_PROTOCOL_CMD_GRP_ALERTS                     0x55
#define COMMUNICATION_PROTOCOL_CMD_GRP_DM_SYS_CONFIG_PARAMETERS   0x44
#define COMMUNICATION_PROTOCOL_CMD_GRP_DM_NAVIGATION_CONTROL      0x49
#define COMMUNICATION_PROTOCOL_CMD_GRP_DM_BACKLIGHT_CONTROL       0x23
#define COMMUNICATION_PROTOCOL_CMD_GRP_DM_FIRMWARE_UPDATE         0x1F
#define COMMUNICATION_PROTOCOL_CMD_GRP_DM_HID                     0x48
#define COMMUNICATION_PROTOCOL_CMD_DEFAULT				          0xFF



/* Commands for Group-> DM NEW PARAMETERS */
#define COMMUNICATION_PROTOCOL_CMD_CURRENT_DATE_TIME                     0x01
#define COMMUNICATION_PROTOCOL_CMD_VEHICLE_BATTERY_VOLTAGE               0x02
#define COMMUNICATION_PROTOCOL_CMD_ROUTE_SOURCE_NAME                     0x03
#define COMMUNICATION_PROTOCOL_CMD_ROUTE_DESTINATION_NAME                0x04
#define COMMUNICATION_PROTOCOL_CMD_FULL_JOURNEY_ETA                      0x05
#define COMMUNICATION_PROTOCOL_CMD_FULL_JOURNEY_DISTANCE                 0x06
#define COMMUNICATION_PROTOCOL_CMD_POI_ETA                               0x07
#define COMMUNICATION_PROTOCOL_CMD_POI_DISTANCE                          0x08
#define COMMUNICATION_PROTOCOL_CMD_DAILY_ETA                             0x09
#define COMMUNICATION_PROTOCOL_CMD_DAILY_DISTANCE                        0x10
#define COMMUNICATION_PROTOCOL_CMD_CURRENT_MANUEVER_ICON                 0x11
#define COMMUNICATION_PROTOCOL_CMD_CURRENT_MANUEVER_DISTANCE             0x12
#define COMMUNICATION_PROTOCOL_CMD_NEXT_MANUEVER_ICON                    0x13
#define COMMUNICATION_PROTOCOL_CMD_NEXT_MANUEVER_DISTANCE                0x14
#define COMMUNICATION_PROTOCOL_CMD_BIKE_FULE_LEVEL                       0x15
#define COMMUNICATION_PROTOCOL_CMD_BIKE_MILEAGE_DISTANCE                 0x16
#define COMMUNICATION_PROTOCOL_CMD_TOTAL_TRAVELLED_DISTANCE              0x17
#define COMMUNICATION_PROTOCOL_CMD_DAILY_TRAVELLED_DISTANCE              0x18
#define COMMUNICATION_PROTOCOL_CMD_DM_USER_NAME                          0x20
#define COMMUNICATION_PROTOCOL_CMD_DM_PASSKEY                            0x21
#define COMMUNICATION_PROTOCOL_CMD_FULL_JOURNEY_REMAINING_DISTANCE       0x22
#define COMMUNICATION_PROTOCOL_CMD_DAILY_JOURNEY_REMAINING_DISTANCE      0x23
#define COMMUNICATION_PROTOCOL_CMD_POI_REMAINING_DISTANCE                0x24
#define COMMUNICATION_PROTOCOL_CMD_FULL_JOURNEY_REACHING_TIME            0x25
#define COMMUNICATION_PROTOCOL_CMD_DAILY_JOURNEY_REACHING_TIME           0x26
#define COMMUNICATION_PROTOCOL_CMD_POI_REACHING_TIME                     0x27
#define COMMUNICATION_PROTOCOL_CMD_LCD_BACKLIGHT_BRIGHTNESS_CONTROL      0x28
#define COMMUNICATION_PROTOCOL_CMD_DM_BATTERY_CHARGING_ON_OFF            0x29
#define COMMUNICATION_PROTOCOL_CMD_DM_BATTERY_VOLTAGE                    0x30
#define COMMUNICATION_PROTOCOL_CMD_DM_DISPLAY_ON_OFF                     0x31
#define COMMUNICATION_PROTOCOL_CMD_DAILY_TARGETED_DESTINATION_NAME       0x32
#define COMMUNICATION_PROTOCOL_CMD_DAILY_COMPLETED_RIDE_TIME             0x33
#define COMMUNICATION_PROTOCOL_CMD_FULL_JOURNEY_COMPLTED_RIDE_TIME       0x34
#define COMMUNICATION_PROTOCOL_CMD_SET_HEADING_DIRECTION                 0x36
#define COMMUNICATION_PROTOCOL_CMD_SET_HEADING_DISTANCE                  0x37
#define COMMUNICATION_PROTOCOL_CMD_SET_HEADING_TO_DESTINATION            0x38
#define COMMUNICATION_PROTOCOL_CMD_CURRENT_ICON_ROUND_ABOUT_BEARING_INFO 0x39
#define COMMUNICATION_PROTOCOL_CMD_NEXT_ICON_ROUND_ABOUT_BEARING_INFO    0x40
#define COMMUNICATION_PROTOCOL_CMD_HEADING_BEARING_ANGLE                 0x41
#define COMMUNICATION_PROTOCOL_CMD_ROUTE_DEVIATION_BEARING_INFO        	 0x42


/* Commands for Group-> Alerts*/

#define COMMUNICATION_PROTOCOL_CMD_INCOMING_ALERT			0x01
#define COMMUNICATION_PROTOCOL_CMD_ALERT_RESPONSE			0x02
#define COMMUNICATION_PROTOCOL_CMD_ALERT_RESPONSE_SENT	    0x03
#define COMMUNICATION_PROTOCOL_CMD_ALERT_RESPONSE_FAILED    0x04
#define COMMUNICATION_PROTOCOL_CMD_INCOMING_ALERT_END		0x0A


#define COMMUNICATION_PROTOCOL_CMD_OUTGOING_ALERT			  0x05
#define COMMUNICATION_PROTOCOL_CMD_ALERT_SENT				  0x06
#define COMMUNICATION_PROTOCOL_CMD_ALERT_FAILED				  0x07
#define COMMUNICATION_PROTOCOL_CMD_ALERT_SENT_RESPONSE		  0x08
#define COMMUNICATION_PROTOCOL_CMD_OUTGOING_ALERT_END	      0x09
#define COMMUNICATION_PROTOCOL_CMD_OUTGOING_ALERT_MODE_ENTRY  0x10
#define COMMUNICATION_PROTOCOL_CMD_OUTGOING_ALERT_MODE_EXIT   0x11
#define COMMUNICATION_PROTOCOL_CMD_OUTGOING_ALERT_NO_HELP_RECEIVED		  0x12
#define COMMUNICATION_PROTOCOL_CMD_OUTGOING_ALERT_CLOSED		  0x13



/* Commands for Group-> DM SYSTEM CONGIGURATION PARAMETERS */
#define COMMUNICATION_PROTOCOL_CMD_SET_ALL_SYS_CONFIG_PARAMETERS        0x01
#define COMMUNICATION_PROTOCOL_CMD_SET_DM_SERIAL_NUMBER                 0x02
#define COMMUNICATION_PROTOCOL_CMD_SET_DM_PRODUCTION_WEEK_NUMBER        0x03
#define COMMUNICATION_PROTOCOL_CMD_SET_DM_OPTION_BYTES                  0x04
#define COMMUNICATION_PROTOCOL_CMD_SET_DM_HARDWARE_VERSION              0x05
#define COMMUNICATION_PROTOCOL_CMD_SET_DM_SOFTWARE_VERSION              0x06
#define COMMUNICATION_PROTOCOL_CMD_GET_ALL_SYS_CONFIG_PARAMETERS        0x07
#define COMMUNICATION_PROTOCOL_CMD_GET_DM_SERIAL_NUMBER                 0x08
#define COMMUNICATION_PROTOCOL_CMD_GET_DM_PRODUCTION_WEEK_NUMBER        0x09
#define COMMUNICATION_PROTOCOL_CMD_GET_DM_OPTION_BYTES                  0x10
#define COMMUNICATION_PROTOCOL_CMD_GET_DM_HARDWARE_VERSION              0x11
#define COMMUNICATION_PROTOCOL_CMD_GET_DM_SOFTWARE_VERSION              0x12

/* Commands for Group-> DM NAVIGATION CONTROL*/
#define COMMUNICATION_PROTOCOL_CMD_SET_JOURNEY                0x01
#define COMMUNICATION_PROTOCOL_CMD_DAILY_JOURNEY_START_STOP   0x02
#define COMMUNICATION_PROTOCOL_CMD_FULL_JOURNEY_START_STOP    0x03
#define COMMUNICATION_PROTOCOL_CMD_TBT_START_STOP             0x04
#define COMMUNICATION_PROTOCOL_CMD_TBT_HIDE_SHOW              0x05
#define COMMUNICATION_PROTOCOL_CMD_HEADING_START              0x06
#define COMMUNICATION_PROTOCOL_CMD_DEVIATION_START            0x07
#define COMMUNICATION_PROTOCOL_CMD_RIDE_PAUSE_RESUME          0x08
#define COMMUNICATION_PROTOCOL_CMD_WAYPOINT_REACHED		      0x09
#define COMMUNICATION_PROTOCOL_CMD_GPS_STATUS_INFO		      0x10
#define COMMUNICATION_PROTOCOL_CMD_FOLLOW_THE_ROAD            0x11
#define COMMUNICATION_PROTOCOL_CMD_APPROACHING_POI            0x12
#define COMMUNICATION_PROTOCOL_CMD_APPROACHING_DESTINATION    0x13


/* Commands for Group-> DM Backlight Control */
#define COMMUNICATION_PROTOCOL_CMD_SET_BACKLIGHT_MODE               0x01
#define COMMUNICATION_PROTOCOL_CMD_SET_BACKLIGHT_CONTROL_MODE       0x02
#define COMMUNICATION_PROTOCOL_CMD_SET_MAX_BACKLIGHT_BRIGHTNESS     0x03
#define COMMUNICATION_PROTOCOL_CMD_SET_BACKLIGHT_ON_OFF				0x04





/* Commands for Group-> DM firmware update Commands */
#define COMMUNICATION_PROTOCOL_CMD_FIRMWARE_UPDATE_START    0x11

/* Commands for Group-> DM HID Commands */

#define COMMUNICATION_PROTOCOL_CMD_KEY_PRESS                0x02


//alert expiry events control bits		------------------should we move this to CP??
#define ALERT_EXPIRY_ENABLE_APP_CONTROLLED_BIT 			0x01
#define ALERT_EXPIRY_ENABLE_KEYPRESS_CONTROLLED_BIT 	0x02
#define ALERT_EXPIRY_ENABLE_TIMER_CONTROLLED_BIT 		0x04


/**
 * @brief   enum defining system configuration Type
 *
 */

typedef enum {
	CP_SYSCFG_DEFAULT_TYPE = 12,  /**< \brief word is zero if eeprom having default type system configuration */
	CP_SYSCFG_PRODUCTION_TYPE,   /**< \brief word is one if eeprom having production type system configuratio */
	SysCfgTypeTypeForceMyEnumIntSize = MAX_INT,
} CP_SysCfgTypeTypeDef;



//  /**
//   * @brief   union  defining optional bytes
//   *
//   */
//  typedef union {
//
//    struct {
//  	    unsigned char bit1  : 1;
//  	    unsigned char bit2  : 1;
//  	    unsigned char bit3  : 1;
//  	    unsigned char bit4  : 1;
//  	    unsigned char bit5  : 1;
//  	    unsigned char bit6  : 1;
//  	    unsigned char bit7  : 1;
//  	    unsigned char bit8  : 1;
//  	    unsigned char bit9  : 1;
//  	    unsigned char bit10 : 1;
//  	    unsigned char bit11 : 1;
//  	    unsigned char bit12 : 1;
//  	    unsigned char bit13 : 1;
//  	    unsigned char bit14 : 1;
//  	    unsigned char bit15 : 1;
//  	    unsigned char bit16 : 1;
//  	  } optionBits;
//  	  uint16_t optionBytes; /**< \brief optional bytes*/
//  } CP_OptionBytesInfoTypeDef;

/**
 * @brief   union defining ota status info
 *
 */
typedef union {
	struct {
		unsigned char otaAppStatusBit1  : 1;// 0 = Ota App updation done, 1 = Ota App updation in progress.
		unsigned char otaDataStatusBit2 : 1;// 0 = Ota Data updation done, 1 = Ota Data updation in progress.
		unsigned char bit3  : 1;
		unsigned char bit4  : 1;
		unsigned char bit5  : 1;
		unsigned char bit6  : 1;
		unsigned char bit7  : 1;
		unsigned char bit8  : 1;
		unsigned char bit9  : 1;
		unsigned char bit10  : 1;
		unsigned char bit11  : 1;
		unsigned char bit12  : 1;
		unsigned char bit13  : 1;
		unsigned char bit14  : 1;
		unsigned char bit15  : 1;
		unsigned char bit16  : 1;
		unsigned char bit17  : 1;
		unsigned char bit18  : 1;
		unsigned char bit19  : 1;
		unsigned char bit20  : 1;
		unsigned char bit21  : 1;
		unsigned char bit22  : 1;
		unsigned char bit23  : 1;
		unsigned char bit24  : 1;
		unsigned char bit25  : 1;
		unsigned char bit26  : 1;
		unsigned char bit27  : 1;
		unsigned char bit28  : 1;
		unsigned char bit29  : 1;
		unsigned char bit30  : 1;
		unsigned char bit31  : 1;
		unsigned char bit32  : 1;
	} otaStatusBits;
	uint32_t otaStatusBytes; /**< \brief optional bytes*/
} CP_OtaStatusInfoInfoTypeDef;



/**
 * @brief   structure  defining DM system configuraton parameters
 *
 */
typedef struct {
	uint32_t eolModeStatus;
	uint32_t dmSerialNumberMsb;   /**< \brief dm serial number,stores actual serial number*/
	uint32_t dmSerialNumberLsb;   /**< \brief dm serial number,stores actual serial number*/
	uint32_t dmHardwareVersion; /**< \brief dm hardware version*/
	uint32_t dmSoftwareversion; /**< \brief dm software version*/
	uint32_t dmSoftwareBuildId;
	uint32_t productionWeekNumber; /**< \brief dm production week number*/
	uint32_t optionByteInformation; /**< \brief optional bytes*/
	CP_OtaStatusInfoInfoTypeDef OtaStatus;
} CP_DmConfigParametersTypeDef;


typedef struct {
	uint32_t timeDisplayFormat;
} CP_DmConfigTimeDisplayFormatTypeDef;

//enum definition for information validity
typedef enum{
	PARAMETER_IGNORE_LIVE_FLAG 		= 0,					/**< \brief Parameter is not changeable . */
	PARAMETER_NOT_LIVE,										/**< \brief Information provided by parameter is not live . */
	PARAMETER_LIVE,											/**< \brief Information provided by parameter is live . */
	ParameterLiveStatusForceMyEnumIntSize = MAX_INT,
} CP_ParameterLiveStatusTypeDef;

/*****************************************************ICON IDS IN FLASH************************************************/

/**
 * @brief  Icon IDs stored in Flash
 *
 * The Icon IDs act as the (2 byte) IDs that shall be assigned to each one of the icons, eventually displayed
 * on the DM. Only the icon IDs mentioned below are supported by the DM.
 *
 */
//TO BE UPDATED BY CP

/**< \brief Icon ID 2 . */
/**< \brief Icon ID 3 . */
/**< \brief Icon ID 4 . */
/**< \brief Icon ID 5 . */
/**< \brief Icon ID 6 . */
/**< \brief Icon ID 7 . */
/**< \brief Icon ID 8 . */
/**< \brief Icon ID 9 . */
/**< \brief Icon ID 10 . */

typedef enum{
	ICON_MAXIMUS_SYMBOL				= 0,
	ICON_MAXIMUS_WORD,
	ICON_TICK,
	ICON_CANCEL,
	ICON_ROCKET,
	ICON_DM_BATTERY_EMPTY,
	ICON_MAP,
	ICON_BIKE_BATTERY_EMPTY,
	ICON_CLOCK,
	ICON_ESTIMATED_TIME,
	ICON_ESTIMATED_DISTANCE,
	ICON_SETTINGS_CLOG,
	ICON_NAV_HEADING_S,
	ICON_NAV_STRAIGHT_L,
	ICON_NAV_STRAIGHT_S,
	ICON_NAV_SLIGHT_RIGHT_L,
	ICON_NAV_SLIGHT_RIGHT_S,
	ICON_NAV_SLIGHT_LEFT_L,
	ICON_NAV_SLIGHT_LEFT_S,
	ICON_NAV_SHARP_RIGHT_L,
	ICON_NAV_SHARP_RIGHT_S,
	ICON_NAV_SHARP_LEFT_L,
	ICON_NAV_SHARP_LEFT_S,
	ICON_NAV_ROUND_ABOUT_CURRENT_L,
	ICON_NAV_ROUND_ABOUT_NEXT_S,
	ICON_NAV_UTURN_RIGHT_L,
	ICON_NAV_UTURN_RIGHT_S,
	ICON_NAV_UTURN_LEFT_L,
	ICON_NAV_UTURN_LEFT_S,
	ICON_NAV_RIGHT_L,
	ICON_NAV_RIGHT_S,
	ICON_NAV_LEFT_L,
	ICON_NAV_LEFT_S,
	ICON_NAV_RAMP_RIGHT_L,
	ICON_NAV_RAMP_RIGHT_S,
	ICON_NAV_RAMP_LEFT_L,
	ICON_NAV_RAMP_LEFT_S,
	ICON_NAV_MERGE_L,
	ICON_NAV_MERGE_S,
	ICON_NAV_KEEP_RIGHT_L,
	ICON_NAV_KEEP_RIGHT_S,
	ICON_NAV_KEEP_LEFT_L,
	ICON_NAV_KEEP_LEFT_S,
	ICON_NAV_FORK_RIGHT_L,
	ICON_NAV_FORK_RIGHT_S,
	ICON_NAV_FORK_LEFT_L,
	ICON_NAV_FORK_LEFT_S,
	ICON_NAV_EXIT_HIGHWAY_RIGHT_L,
	ICON_NAV_EXIT_HIGHWAY_RIGHT_S,
	ICON_NAV_EXIT_HIGHWAY_LEFT_L,
	ICON_NAV_EXIT_HIGHWAY_LEFT_S,
	ICON_NAV_WINDING_ROAD_L,
	ICON_NAV_WINDING_ROAD_S,
	ICON_ALERT,
	ICON_ALERT_FUEL_EMPTY,
	ICON_ALERT_FUEL_HALF,
	ICON_ALERT_PUNCTURE,
	ICON_ALERT_ACCIDENT,
	ICON_ALERT_SEVERE_COND,
	ICON_ALERT_CLOCK0,
	ICON_ALERT_SENT,
	ICON_ALERT_HELP_COMING,
	ICON_ALERT_CALL,
	ICON_ALERT_LOW_BIKE_BAT,
	ICON_ALERT_LOW_DM_BAT,
	ICON_ALERT_SUSPICIOUS_BIKE,
	ICON_ALERT_NO_CONNECTION,
	ICON_ARRIVED,
	ICON_JOURNEY_COMPLETE,
	ICON_WEAK_GPS_SIGNAL,
	ICON_FOLLOW_THE_ROAD,
	ICON_APPROACHING_POI,
	ICON_APPROACHING_DESTINATION,
	ICON_SW_VERSION,
	ICON_DM_CHARGING,
	ICON_DM_UPDATING,
	ICON_UNPAIR,
	ICON_URGENT_ALERT_BAR,
	ICON_NAV_PLANNED_ROUTE_DEVIATED,
	ICON_ALERT_YOU_ARE_AHEAD,
	ICON_ALERT_YOU_ARE_BEHIND,
	ICON_ALERT_RIGHT_HAND_CURVE_AHEAD,
	ICON_ALERT_LEFT_HAND_CURVE_AHEAD,
	ICON_ALERT_RIGHT_HAIR_PIN_BEND_AHEAD,
	ICON_ALERT_LEFT_HAIR_PIN_BEND_AHEAD,
	ICON_ALERT_RIGHT_REVERSE_BEND_AHEAD,
	ICON_ALERT_LEFT_REVERSE_BEND_AHEAD,
	ICON_NOT_LIVE,
	ICON_WELCOME_MR_LOW,
	ICON_MAX_ICONS,
	MaximusIconIdForceMyEnumIntSize = MAX_INT,
} CP_MaximusIconIdTypeDef;





/**
 * @brief  enum defining Unit of distance
 *
 below are the units for distance
 * (refer #UNIT_TypeDef).
 */
typedef enum {
	YARD				= 0,				/**< \brief YARD */
	METER,       	    			        /**< \brief METER */
	KILOMETER,	    			        /**< \brief KILOMETER */
	MILES,		        				/**< \brief MILES */
	UnitDistanceForceMyEnumIntSize = MAX_INT,
} CP_UnitDistanceTypeDef;


typedef enum {
	MILI_VOLT		= 0,		  /**< \brief MILI VOLT . */
	VOLT,	        			  /**< \brief VOLTAGE */
	VOLT_PERCENTAGE,
	UnitVoltageForceMyEnumIntSize = MAX_INT,
} CP_UnitVoltageTypeDef;

typedef enum {
	ALTERNATOR_HEALTHY = 0,         /**< \brief Alternator Healthy*/
	ALTERNATOR_OK,            		/**< \brief Alternator OK*/
	ALTERNATOR_BAD,            		/**< \brief Alternator BAD*/
	BATTERY_VERY_GOOD,       		/**< \brief Battery VERY GOOD*/
	BATTERY_GOOD,       			/**< \brief Battery GOOD*/
	BATTERY_OK,        				/**< \brief Battery OK*/
	BATTERY_BAD,        			/**< \brief Battery BAD*/
	VehicleBatteryHealthStatusForceMyEnumIntSize = MAX_INT,
} CP_VehicleBatteryHealthStatusTypeDef;

typedef enum {
	SCALE_TEN = 10,
	SCALE_HUNDRED = 100,
	SCALE_THOUSAND = 1000,
	CP_ScaleForceMyEnumIntSize = MAX_INT,
} CP_ScaleTypeDef;


typedef enum {
	CHARGING_OFF_USB_NOT_CONNECTED = 0,         /**< \brief dm charging enable and usb connected . */
	CHARGING_ON_USB_CONNECTED,            /**< \brief dm charging disable and usb not connected . */
	CHARGING_COMPLETED_USB_CONNECTED,      /**< \brief dm charging 100%done and usb connected . */
	DmChargingForceMyEnumIntSize = MAX_INT,
} CP_DmChargingTypeDef;


typedef enum {
	DISPLAY_ON = 0,         /**< \brief dm display on . */
	DISPALY_OFF,            /**< \brief dm display off . */
	DISPLAY_POWERON,     /**< \brief dm display power on .*/
	DISPLAY_POWEROFF,     /**< \brief dm display power off .*/
	DmDisplayForceMyEnumIntSize = MAX_INT,
} CP_DmDisplayTypeDef;

typedef enum {
	LITRE		= 0,		  /**< \brief litre . */
	UnitFuelLevelForceMyEnumIntSize = MAX_INT,
} CP_UnitFuelLevelTypeDef;

typedef enum  {
	SHORT_PRESS                    =     0,       /**< \brief function completed; no error or event occurred.. */
	ButtonPressEventForceMyEnumIntSize = MAX_INT,
} CP_ButtonPressEventTypeDef;

typedef enum {
	JOURNEY_START = 0,               /**< \brief  daily journey start. */
	JOURNEY_STOP,                  /**< \brief daily journey stop. */
	JourneyOnOffForceMyEnumIntSize = MAX_INT,
} CP_JourneyOnOffTypeDef;

typedef enum {
	TBT_ON = 0,               /**< \brief turn by turn enable. */
	TBT_OFF,                  /**< \brief turn by turn disable. */
	TbtOnOffForceMyEnumIntSize = MAX_INT,
} CP_TbtOnOffTypeDef;


typedef enum {
	DEVIATION_START = 0,              	 		/**< \brief  Deviation detection start. */
	DEVIATION_STOP,                  			/**< \brief Deviation detection stop. */
	DeviationStatusForceMyEnumIntSize = MAX_INT,
} CP_RouteDeviationStatusTypeDef;

typedef enum {
	START = 0,               /**< \brief heading start */
	STOP,                  /**< \brief heading stop. */
	HeadingStatusMyEnumIntSize = MAX_INT,
} CP_HeadingStatusTypeDef;


typedef enum {
	PAUSE = 0,               /**< \brief ride pause */
	RESUME,                  /**< \brief ride resume */
	RideStatusMyEnumIntSize = MAX_INT,
} CP_RideStatusTypeDef;

typedef enum {
	GPS_SIGNAL_GOOD = 0,               /**< \brief gps signal strength good . */
	GPS_SIGNAL_BAD,                  /**< \brief gps signal strength bad. */
	GpsStatusForceMyEnumIntSize = MAX_INT,
} CP_GpsStatusTypeDef;




/**
 * @brief   Structure defining number of letters in string
 *
 */
typedef struct  {
	uint8_t letterCount;  /**< \brief Number of letters in string */
} CP_StringLengthTypeDef;





/******command group -> Dm New Parameters***************************/
typedef enum {
	TWELVE_HOUR_FORMAT = 0, /**< \brief 12 hours format */
	TWENTY_FOUR_HOUR_FORMAT,/**< \brief 24 hours format */
	TimeFormatForceMyEnumIntSize = MAX_INT,
} CP_TimeFormatTypeDef;

typedef enum {
	AM_TIME = 0, /**< \brief am time format */
	PM_TIME,   /**< \brief pm time format */
	TimeFormatAmPmForceMyEnumIntSize = MAX_INT,
} CP_TimeFormatAmPmTypeDef;

typedef enum {
	WEEKDAY_MONDAY = 1, /**< \brief monday */
	WEEKDAY_TUESDAY,   /**< \brief tuesday */
	WEEKDAY_WEDNESDAY, /**< \brief wednesday */
	WEEKDAY_THURSDAY, /**< \brief thursday */
	WEEKDAY_FRIDAY, /**< \brief friday */
	WEEKDAY_SATURDAY, /**< \brief saturday */
	WEEKDAY_SUNDAY, /**< \brief sunday */
	WeekDayForceMyEnumIntSize = MAX_INT,
} CP_WeekDayTypeDef;


typedef enum {
	DAYLIGHTSAVING_SUBTRACT_1HOUR = 0x00020000, /**< \brief subrtact 1 hour  */
	DAYLIGHTSAVING_ADD_1HOUR = 0x00010000,   /**< \brief add 1 hour */
	DAYLIGHTSAVING_NONE = 0x00000000,   /**< \brief day light saving none */
	DayLightSavingForceMyEnumIntSize = MAX_INT,
} CP_DayLightSavingTypeDef;


typedef enum {
	STORE_OPERATION_RESET = 0x00000000, /**< \brief store operation reset  */
	STORE_OPERATION_SET = 0x00040000,   /**< \brief store operation set */
	StoreOperationForceMyEnumIntSize = MAX_INT,
} CP_StoreOperationTypeDef;



/** This structure contains command group,command ,payload data(it is pointer to buffer
 * holding all the payload data) and payload size.
 *
 */
typedef struct {
	uint8_t liveFlag;    /**< \brief Tells if data live(recent) or not. */
	uint8_t timeFormat; /**< \brief Time format */
	uint8_t TimeAmPm; /**< \brief timeAMPm format */
	uint8_t date;          /**< \brief current date. */		//not needed for now
	uint8_t month;          /**< \brief current month. */	//not needed for now
	uint8_t year;          /**< \brief current year. */		//not needed for now
	uint8_t weekDayInfo;   /**< \brief week day. */		//not needed for now
	uint8_t hour;          /**< \brief current hour. */
	uint8_t minute;         /**< \brief current minutes. */
	uint8_t second;          /**< \brief current seconds. */	//not needed for now
	uint32_t dayLightSaving; /**< \brief daylight saving */	//not needed for now
	uint32_t storeOperation; /**< \brief store operation */	//not needed for now
} CP_CurrentDateTimeTypeDef;

/**
 * @brief   Structure defining vehicle battery voltage
 *
 *
 */
typedef struct {
	uint8_t liveFlag;   /**< \brief Tells if data live(recent) or not. */
	uint8_t voltage;    /**< \brief info about bike battery voltage. */
	CP_ScaleTypeDef scale;    /**< \brief divisor (10,100,1000..etc). for example: 3.6 volt ..36(value received fom mobile)/10=3.6 volt */
	CP_UnitVoltageTypeDef  unit;  /**< \brief voltage unit. */
	CP_VehicleBatteryHealthStatusTypeDef BatteryHealthStatus; /**< \brief battery health status. */
} CP_VehicleBatteryVoltageTypeDef;

/**
 * @brief   Structure defining source name
 *
 *
 */
typedef struct {
	uint8_t liveFlag;  /**< \brief Tells if data live(recent) or not. */
	CP_StringLengthTypeDef stringInfo;  /**< \brief buffer containing received string info via communication protocol */
} CP_RouteSourceNameTypeDef;

/**
 * @brief   Structure defining destination name
 *
 *
 */
typedef struct {
	uint8_t liveFlag;  /**< \brief Tells if data live(recent) or not. */
	CP_StringLengthTypeDef stringInfo;  /**< \brief buffer containing received string info via communication protocol */
} CP_RouteDestinationNameTypeDef;

/**
 * @brief   Structure defining estimated time of arrival
 *
 *
 */
typedef struct {
	uint8_t liveFlag;    /**< \brief Tells if data live(recent) or not. */
	uint8_t hour;          /**< \brief current hour. */
	uint8_t minute;         /**< \brief current minutes. */
} CP_FullJourneyEtaTypeDef;
/**
 * @brief   Structure defining estimated time of arrival
 *
 *
 */
typedef struct {
	uint8_t liveFlag;    /**< \brief Tells if data live(recent) or not. */
	uint8_t hour;          /**< \brief current hour. */
	uint8_t minute;         /**< \brief current minutes. */
} CP_DailyRideEtaTypeDef;

/**
 * @brief   Structure defining estimated time of arrival at POI(point of intrest)
 *
 *
 */
typedef struct {
	uint8_t liveFlag;    /**< \brief Tells if data live(recent) or not. */
	uint8_t hour;          /**< \brief current hour. */
	uint8_t minute;         /**< \brief current minutes. */
} CP_PoiEtaTypeDef;


/**
 * @brief   Structure defining estimated distance of journey
 *
 *
 */
typedef struct {
	uint8_t liveFlag;    /**< \brief Tells if data live(recent) or not. */
	uint16_t distance; /**< \brief current maneuver distance. */
	CP_ScaleTypeDef scale;    /**< \brief divisor (10,100,1000..etc). for example: 3.6 meter ..36/10=3.6 meter */
	CP_UnitDistanceTypeDef  unit;  /**< \brief current maneuver unit. */
} CP_FullJourneyDistanceTypeDef;

/**
 * @brief   Structure defining estimated distance of daily journey
 *
 *
 */
typedef struct {
	uint8_t liveFlag;    /**< \brief Tells if data live(recent) or not. */
	uint16_t distance; /**< \brief current maneuver distance. */
	CP_ScaleTypeDef scale;    /**< \brief divisor (10,100,1000..etc). for example: 3.6 meter ..36/10=3.6 meter */
	CP_UnitDistanceTypeDef  unit;  /**< \brief current maneuver unit. */
} CP_DailyRideDistanceTypeDef;

/**
 * @brief   Structure defining estimated distance of POI
 *
 *
 */
typedef struct {
	uint8_t liveFlag;    /**< \brief Tells if data live(recent) or not. */
	uint16_t distance; /**< \brief current maneuver distance. */
	CP_ScaleTypeDef scale;    /**< \brief divisor (10,100,1000..etc). for example: 3.6 meter ..36/10=3.6 meter */
	CP_UnitDistanceTypeDef  unit;  /**< \brief current maneuver unit. */
} CP_PoiDistanceTypeDef;



/**
 * @brief   Structure defining current maneuver icon id
 *
 *
 */
typedef struct {
	uint8_t liveFlag; /**< \brief Tells if data live(recent) or not. */
	CP_MaximusIconIdTypeDef IconID;   /**< \brief info about icon id to display on screen. */
} CP_CurrentManeuverIconTypeDef;



/**
 * @brief   Structure defining current maneuver distance ,scale and unit
 *
 *
 */
typedef struct {
	uint8_t liveFlag; /**< \brief Tells if data live(recent) or not. */
	uint16_t distance; /**< \brief current maneuver distance. */
	CP_ScaleTypeDef scale;    /**< \brief divisor (10,100,1000..etc). for example: 3.6 meter ..36/10=3.6 meter */
	CP_UnitDistanceTypeDef  unit;  /**< \brief current maneuver unit. */
} CP_CurrentManeuverDistanceTypeDef;

/**
 * @brief   Structure defining next maneuver icon id
 *
 *
 */
typedef struct {
	uint8_t liveFlag; /**< \brief Tells if data live(recent) or not. */
	CP_MaximusIconIdTypeDef IconID;   /**< \brief info about icon id to display on screen. */
} CP_NextManeuverIconTypeDef;

/**
 * @brief   Structure defining next maneuver icon
 *
 *
 */
typedef struct {
	uint8_t liveFlag; /**< \brief Tells if data live(recent) or not. */
	uint16_t distance;  /**< \brief next maneuver distance. */
	CP_ScaleTypeDef scale;    /**< \brief divisor (10,100,1000..etc). for example: 3.6 meter ..36/10=3.6 meter */
	CP_UnitDistanceTypeDef  unit;   /**< \brief next maneuver unit. */
} CP_NextManeuverDistanceTypeDef;


/**
 * @brief   Structure defining bike fuel level
 *
 *
 */
typedef struct {
	uint8_t liveFlag;   /**< \brief Tells if data live(recent) or not. */
	uint16_t fuelLevel; /**< \brief bike fuel level. */
	CP_ScaleTypeDef scale;     /**< \brief divisor (10,100,1000..etc).*/
	CP_UnitFuelLevelTypeDef unit;
} CP_BikeFuelLevelTypeDef;

/**
 * @brief   Structure defining bike mileage distance, scale and unit
 *
 *
 */
typedef struct {
	uint8_t liveFlag;  /**< \brief Tells if data live(recent) or not. */
	uint16_t distance; /**< \brief bike mileage distance. */
	CP_ScaleTypeDef scale;    /**< \brief divisor (10,100,1000..etc). for example: 3.6 meter ..36/10=3.6 meter */
	CP_UnitDistanceTypeDef  unit;  /**< \brief bike mileage unit. */
} CP_BikeMileageDistanceTypeDef;

/**
 * @brief   Structure defining total travelled distance and unit
 *
 *
 */
typedef struct {
	uint8_t liveFlag;   /**< \brief Tells if data live(recent) or not. */
	uint16_t distance;  /**< \brief travelled distance. */
	CP_ScaleTypeDef scale;    /**< \brief divisor (10,100,1000..etc). for example: 3.6 meter ..36/10=3.6 meter */
	CP_UnitDistanceTypeDef  unit;   /**< \brief travelled unit. */
} CP_TotalJourneyTravelledDistanceTypeDef;

/**
 * @brief   Structure defining daily travelled distance and unit
 *
 *
 */
typedef struct {
	uint8_t liveFlag;   /**< \brief Tells if data live(recent) or not. */
	uint16_t distance;  /**< \brief travelled distance. */
	CP_ScaleTypeDef scale;    /**< \brief divisor (10,100,1000..etc). for example: 3.6 meter ..36/10=3.6 meter */
	CP_UnitDistanceTypeDef  unit;   /**< \brief travelled unit. */
} CP_DailyTravelledDistanceTypeDef;

/**
 * @brief   Structure defining full journey remaining distance and unit
 *
 *
 */
typedef struct {
	uint8_t liveFlag;   /**< \brief Tells if data live(recent) or not. */
	uint16_t distance;  /**< \brief travelled distance. */
	CP_ScaleTypeDef scale;    /**< \brief divisor (10,100,1000..etc). for example: 3.6 meter ..36/10=3.6 meter */
	CP_UnitDistanceTypeDef  unit;   /**< \brief travelled unit. */
} CP_FullJourneyRemainingDistanceTypeDef;

/**
 * @brief   Structure defining daily journey remaining distance and unit
 *
 *
 */
typedef struct {
	uint8_t liveFlag;   /**< \brief Tells if data live(recent) or not. */
	uint16_t distance;  /**< \brief travelled distance. */
	CP_ScaleTypeDef scale;    /**< \brief divisor (10,100,1000..etc). for example: 3.6 meter ..36/10=3.6 meter */
	CP_UnitDistanceTypeDef  unit;   /**< \brief travelled unit. */
} CP_DailyJourneyRemainingDistanceTypeDef;

/**
 * @brief   Structure defining poi remaining distance and unit
 *
 *
 */
typedef struct {
	uint8_t liveFlag;   /**< \brief Tells if data live(recent) or not. */
	uint16_t distance;  /**< \brief travelled distance. */
	CP_ScaleTypeDef scale;    /**< \brief divisor (10,100,1000..etc). for example: 3.6 meter ..36/10=3.6 meter */
	CP_UnitDistanceTypeDef  unit;   /**< \brief travelled unit. */
} CP_PoiRemainingDistanceTypeDef;



/**
 * @brief   Structure defining full journey Reaching time of arrival .
 *
 *
 */
//typedef struct {
//    uint8_t liveFlag;    /**< \brief Tells if data live(recent) or not. */
//    uint8_t hour;          /**< \brief current hour. */
//	uint8_t minute;         /**< \brief current minutes. */
//}__attribute__ ((packed)) CP_FullJourneyReachingTimeTypeDef;

typedef struct {
	uint8_t liveFlag;    /**< \brief Tells if data live(recent) or not. */
	uint8_t timeFormat; /**< \brief Time format */
	uint8_t TimeAmPm; /**< \brief timeAMPm format */
	uint8_t hour;          /**< \brief current hour. */
	uint8_t minute;         /**< \brief current minutes. */
} CP_FullJourneyReachingTimeTypeDef;


///**
// * @brief   Structure defining Reaching time of arrival at POI(point of intrest)
// *
// *
// */
//typedef struct {
//    uint8_t liveFlag;    /**< \brief Tells if data live(recent) or not. */
//    uint8_t hour;          /**< \brief current hour. */
//	uint8_t minute;         /**< \brief current minutes. */
//}__attribute__ ((packed)) CP_DailyReachingTimeTypeDef;



typedef struct {
	uint8_t liveFlag;    /**< \brief Tells if data live(recent) or not. */
	uint8_t timeFormat; /**< \brief Time format */
	uint8_t TimeAmPm; /**< \brief timeAMPm format */
	uint8_t hour;          /**< \brief current hour. */
	uint8_t minute;         /**< \brief current minutes. */
} CP_DailyReachingTimeTypeDef;


/**
 * @brief   Structure defining Reaching time of arrival at POI(point of intrest)
 *
 *
 */
//typedef struct {
//    uint8_t liveFlag;    /**< \brief Tells if data live(recent) or not. */
//    uint8_t hour;          /**< \brief current hour. */
//	uint8_t minute;         /**< \brief current minutes. */
//}__attribute__ ((packed)) CP_PoiReachingTimeTypeDef;

typedef struct {
	uint8_t liveFlag;    /**< \brief Tells if data live(recent) or not. */
	uint8_t timeFormat; /**< \brief Time format */
	uint8_t TimeAmPm; /**< \brief timeAMPm format */
	uint8_t hour;          /**< \brief current hour. */
	uint8_t minute;         /**< \brief current minutes. */
} CP_PoiReachingTimeTypeDef;



/**
 * @brief   Structure defining full journey remaining distance and unit
 *
 *
 */
typedef struct {
	uint8_t liveFlag;   /**< \brief Tells if data live(recent) or not. */
	uint16_t distance;  /**< \brief travelled distance. */
	CP_ScaleTypeDef scale;    /**< \brief divisor (10,100,1000..etc). for example: 3.6 meter ..36/10=3.6 meter */
	CP_UnitDistanceTypeDef  unit;   /**< \brief travelled unit. */
} CP_HeadingDistanceTypeDef;

/**
 * @brief   Structure defining daily targated destination name
 *
 *
 */
typedef struct {
	uint8_t liveFlag;  /**< \brief Tells if data live(recent) or not. */
	CP_StringLengthTypeDef stringInfo;  /**< \brief buffer containing received string info via communication protocol */
} CP_HeadingDirectionTypeDef;

/**
 * @brief   Structure defining daily targated destination name
 *
 *
 */
typedef struct {
	uint8_t liveFlag;  /**< \brief Tells if data live(recent) or not. */
	CP_StringLengthTypeDef stringInfo;  /**< \brief buffer containing received string info via communication protocol */
} CP_HeadingToTypeDef;


/**
 * @brief   structure for bearing angle(use same for heading bearing angle)
 *
 *
 */
typedef struct {
	uint16_t bearingAngleDegree;  /**< \brief Tells the bearing angle. */
} CP_BearingAngleTypeDef;


/**
 * @brief   structure for route deviation bearing angle and distance .
 *
 *
 */
typedef struct {
	uint8_t liveFlag;    /**< \brief Tells if data live(recent) or not. */
	uint16_t distance; /**< \brief current maneuver distance. */
	CP_ScaleTypeDef scale;    /**< \brief divisor (10,100,1000..etc). for example: 3.6 meter ..36/10=3.6 meter */
	CP_UnitDistanceTypeDef  unit;  /**< \brief current maneuver unit. */
	CP_BearingAngleTypeDef bearingAngle; /**< \brief Tells the bearing angle. */
} CP_RouteDeviationBearingAngleTypeDef;
/**
 * @brief   structure for Round about bearing angle
 *
 *
 */
typedef struct {
	uint8_t exitNumber;
	CP_BearingAngleTypeDef bearingAngle;	;  /**< \brief Tells the bearing angle. */
} CP_RoundAboutBearingAngleToTypeDef;


/**
 * @brief   Structure defining user name
 *
 *
 */
typedef struct {
	uint8_t liveFlag;  /**< \brief Tells if data live(recent) or not. */
	CP_StringLengthTypeDef stringInfo;  /**< \brief buffer containing received string info via communication protocol */
} CP_UserNameTypeDef;

/**
 * @brief   Structure defining passkey for pairing of devices(BLE)
 *
 *
 */
typedef struct {
	uint8_t liveFlag;  /**< \brief Tells if data live(recent) or not. */
	CP_StringLengthTypeDef stringInfo;  /**< \brief buffer containing received string info via communication protocol */
} CP_PasskeyTypeDef;

/**
 * @brief   Structure defining dm charging or not
 *
 *
 */
typedef struct {
	CP_DmChargingTypeDef onOff;  /**< \brief dm battery on/off  */
} CP_DmBatteryChargingStatusTypeDef;

/**
 * @brief   Structure defining dm battery voltage
 *
 *
 */
typedef struct {
	uint8_t voltage;  /**< \brief dm battery voltage  */
	CP_ScaleTypeDef scale;    /**< \brief divisor (10,100,1000..etc). for example: 3.6 volt ..36(value received fom mobile)/10=3.6 volt */
	CP_UnitVoltageTypeDef unit;  /**< \brief voltage unit. */
} CP_DmBatteryVoltageTypeDef;

/**
 * @brief   Structure defining dm display on off
 *
 *
 */
typedef struct {
	CP_DmDisplayTypeDef onOff;  /**< \brief dm battery on/off  */
} CP_DmDisplayOnOffTypeDef;


/**
 * @brief   Structure defining daily targated destination name
 *
 *
 */
typedef struct {
	uint8_t liveFlag;  /**< \brief Tells if data live(recent) or not. */
	CP_StringLengthTypeDef stringInfo;  /**< \brief buffer containing received string info via communication protocol */
} CP_DailyTargatedDestinationNameTypeDef;

/**
 * @brief   Structure defining daily completed ride time
 *
 *
 */
typedef struct {
	uint8_t liveFlag;    /**< \brief Tells if data live(recent) or not. */
	uint8_t hour;          /**< \brief current hour. */
	uint8_t minute;         /**< \brief current minutes. */
} CP_DailyCompletedRideTimeTypeDef;

/**
 * @brief   Structure defining full journey completed ride time
 *
 *
 */
typedef struct {
	uint8_t liveFlag;    /**< \brief Tells if data live(recent) or not. */
	uint8_t days;    		/**< \brief days completed to reach destination. */
	uint8_t hour;          /**< \brief current hour. */
	uint8_t minute;         /**< \brief current minutes. */
} CP_FullJourneyCompletedRideTimeTypeDef;


/******command group -> Dm Navigation control***************************/

/**
 * @brief   Structure defining journey start stop
 *
 *
 */
typedef struct {
	CP_JourneyOnOffTypeDef onOff  ;  /**< \brief set journey */
} CP_SetJourneyTypeDef;

/**
 * @brief   Structure defining full journey start stop
 *
 *
 */
typedef struct {
	CP_JourneyOnOffTypeDef onOff  ;  /**< \brief journey start  */
} CP_FullJourneyStartStopTypeDef;

/**
 * @brief   Structure defining daily journey start stop
 *
 *
 */
typedef struct {
	CP_JourneyOnOffTypeDef onOff  ;  /**< \brief journey start  */
} CP_DailyJourneyStartStopTypeDef;



/**
 * @brief   Structure defining enabling and disabling TBT
 *
 *
 */
typedef struct {
	CP_TbtOnOffTypeDef onOff  ;  /**< \brief navigation tbt on/off */
} CP_TbtStartStopTypeDef;


/**
 * @brief   Structure defining hide and show of TBT
 *
 *
 */
typedef struct {
	CP_TbtOnOffTypeDef onOff  ;  /**< \brief  tbt hide = off ,show = on */
} CP_TbtHideShowTypeDef;


/**
 * @brief   Structure defining heading start stop
 *
 *
 */
typedef struct {
	CP_HeadingStatusTypeDef startStop  ;  /**< \brief  heading start  = 0 ,stop = 1 */
} CP_HeadingStartTypeDef;


/**
 * @brief   Structure defining deviation start stop
 *
 *
 */
typedef struct {
	CP_RouteDeviationStatusTypeDef startStop  ;  		/**< \brief  deviation start  = 0 ,stop = 1 */
} CP_RouteDeviationStartTypeDef;



/**
 * @brief   Structure defining ride pause and resume
 *
 *
 */
typedef struct {
	CP_RideStatusTypeDef pauseResume  ;  		/**< \brief  Ride resume  = 1 ,pause = 0 */
} CP_RideStatusInfoTypeDef;

/**
 * @brief   Structure defining status info of gps signal strength on mobile
 *
 */
typedef struct  {
	CP_GpsStatusTypeDef statusInfo;  /**< \brief status info of gps signal strength on mobile */
} CP_GpsStatusInfoTypeDef;

/**
 * @brief    structure and enums  for command Group-> Alerts
 *
 *
 */


typedef enum {
	NON_URGENT_ALERT = 1 ,               /**< \brief non-urgent alert*/
	URGENT_ALERT ,               /**< \brief urgent alert*/
	CRITICAL_ALERT ,               /**< \brief critical alert*/
	AlertTypeInfoForceMyEnumIntSize = MAX_INT,
} CP_AlertTypeInfoTypeDef;

/* alert id for critical,urgent and non urgent alerts */

typedef enum {
	ACCIDENT  = 0,               /**< \brief accident occured*/
	BIKE_THEFT,                  /**< \brief bike theft*/	//to be separated from here
	CriticalAlertIdForceMyEnumIntSize = MAX_INT,
} CP_CriticalAlertIdTypeDef;

typedef enum {
	SEVERE_CONDITIONS  = 0,  /**< \brief severe conditions*/
	PUNCTURE,     /**< \brief puncture*/
	NO_FUEL,    /**< \brief no fuel*/
	UrgentAlertIdForceMyEnumIntSize = MAX_INT,
} CP_UrgentAlertIdTypeDef;

typedef enum {
	INCOMING_CALL = 0, /**< \brief incoming call*/
	LOW_BIKE_BATTERY,/**< \brief low bike battery*/
	LOW_DM_BATTERY,/**< \brief low dm battery*/
	LOW_FUEL,/**< \brief low fuel*/
	FUEL_PUMP_NON_AVAILABILITY,/**< \brief fuel pump non avaliability*/
//	PLANNED_ROUTE_DEVIATION_DETECTED,/**< \brief planned route deviation detected*/
	YOU_ARE_AHEAD,/**< \brief you are ahead of your group*/
	YOU_ARE_BEHIND,/**< \brief you are behind of your group*/
	RIGHT_HAND_CURVE_AHEAD,/**< \brief Right hand curve ahead*/
	LEFT_HAND_CURVE_AHEAD,/**< \brief Left hand curve ahead*/
	RIGHT_HAIR_PIN_BEND_AHEAD,/**< \brief Right hair pin ahead*/
	LEFT_HAIR_PIN_BEND_AHEAD,/**< \brief Left hair pin ahead*/
	RIGHT_REVERSE_BEND_AHEAD,/**< \brief Right reverse bend ahead*/
	LEFT_REVERSE_BEND_AHEAD,/**< \brief Left reverse bend ahead*/
	NonUrgentAlertIdForceMyEnumIntSize = MAX_INT,
} CP_NonUrgentAlertIdTypeDef;



/**
 * @brief fuel pump remaining distance
 *
 *
 */
typedef struct {
	uint16_t distance;  /**< \brief travelled distance. */
	CP_ScaleTypeDef scale;    /**< \brief divisor (10,100,1000..etc). for example: 3.6 meter ..36/10=3.6 meter */
	CP_UnitDistanceTypeDef  unit;   /**< \brief travelled unit. */
} CP_FuelPumpRemainigDistanceTypeDef;

/**
 * @brief   Structure defining user name
 *
 *
 */
typedef struct {
	CP_StringLengthTypeDef stringInfo;  /**< \brief buffer containing received string info via communication protocol */
} CP_UsereNameTypeDef;


/**
 * @brief   used for incoming alerts
 *
 *
 */
typedef struct {
	uint32_t alertUuid; /**< \brief alert UUID info */
	CP_AlertTypeInfoTypeDef alertType; /**< \brief alert category type info */
	uint8_t alertID;    /**< \brief alert id out of 255 total alerts. */
	uint8_t alertPayloadSize;

	//expiration type control bit - APPLICABLE ONLY FOR NON-URGENT ALERTS
	union {
			uint8_t expirationEventTypeDefinition;			///< \brief expirationEventTypeValidation one byte variable to store bit fields into a single value. */
			struct {
			uint8_t appControlled : 1;						/**< \brief Bit field for enabling app controllability. */
			uint8_t keyPressControlled : 1;					/**< \brief Bit field for enabling key press controllability. */
			uint8_t TimerControlled : 1;					/**< \brief Bit field for enabling timer controllability. */
			uint8_t reserved : 5;
		} controlBits;
	}expirationType;
} CP_IncomingAlertTypeDef;


/* used for alert response command */

typedef enum {
	REJECTED  = 0,               /**< \brief response rejected*/
	ACCEPTED,
	ResponseForceMyEnumIntSize = MAX_INT,
} CP_ResponseTypeDef;

/**
 * @brief   structure defining alert response
 *
 *
 */
typedef struct {
	uint32_t alertUuid; /**< \brief alert UUID info */
	CP_ResponseTypeDef response;
} CP_AlertResponseTypeDef;


/* used for alert response sent  and alert response failed*/
typedef enum {
	RESPONSE_FAILED  = 0,/**< \brief response rejected*/
	RESPONSE_SENT ,/**< \brief response rejected*/
	AlertResponseForceMyEnumIntSize = MAX_INT,
} CP_AlertResponseStatusTypeDef;


/**
 * @brief   structure defining alert response sent status
 *
 *
 */
typedef struct {
	uint32_t alertUuid; /**< \brief alert UUID info */
	CP_AlertResponseStatusTypeDef alertResponse;
} CP_AlertResponseSentStatusTypeDef;



/**
 * @brief   used for alert closure of an incoming alert when ALL Oked by the sender
 *
 *
 */
typedef struct {
	uint32_t alertUuid; /**< \brief alert UUID info */
	CP_AlertTypeInfoTypeDef alertType; /**< \brief alert type info */
	uint8_t alertID;    /**< \brief alert id out of 255 total alerts. */
} CP_IncomingAlertClosureTypeDef;



/**
 * @brief   structure defining  for outgoing alert
 *
 *
 */
typedef struct {
	CP_AlertTypeInfoTypeDef alertType; /**< \brief alert type info */
	uint8_t alertID;    /**< \brief alert id out of 255 total alerts. */
} CP_OutgoingAlertInfoTypeDef;



/* used for alert sent and alert failed command*/
typedef enum {
	ALERT_SENT_FAILED  = 0,/**< \brief alert sent failed*/
	ALERT_SENT ,/**< \brief sent*/
	AlertSentStatusForceMyEnumIntSize = MAX_INT,
} CP_AlertSentTypeDef;


/**
 * @brief   structure defining  alert sent status to cloud from mobile(informing dm about same)
 *
 *
 */
typedef struct {
	//	uint8_t alertUuid; /**< \brief alert UUID info */
	CP_AlertSentTypeDef alertSent;
	CP_AlertTypeInfoTypeDef alertType; /**< \brief alert type info */
	uint8_t alertID;    /**< \brief alert id  */
} CP_AlertSentStatusTypeDef;




/**
 * @brief   structure defining  alert sent response
 *
 *
 */
typedef struct {
	uint32_t alertUuid; /**< \brief alert UUID info */
	CP_ResponseTypeDef sentResponse; /**< \brief alert sent response (accepted / rejected) */
} CP_AlertSentResponseTypeDef;




/******command group -> Dm Firmware update***************************/
typedef enum {
	OTA_ONLY_DATA = 0, /**< \brief OTA for data only*/
	OTA_ONLY_APP , /**< \brief OTA for app only*/
	OTA_DATA_APP, /**< \brief OTA for data and App*/
	OtaTypeIdForceMyEnumIntSize = MAX_INT,
}CP_OtaTypeIdTypeDef;

typedef struct{
	CP_OtaTypeIdTypeDef otaTypeID;     /**< \brief OTA type id,can be data or app+data */
} CP_DmFirmwareUpdateOtaTypeDef;




/******command group -> Dm Backlight control***************************/

typedef enum {
	BRIGHTNESS_ZERO_PERCENTAGE     = 0,         /**< \brief backlight off*/
	BRIGHTNESS_TEN_PERCENTAGE      = 10,         /**< \brief brightness 10%*/
	BRIGHTNESS_TWENTY_PERCENTAGE   = 20,           /**< \brief brightness 20%*/
	BRIGHTNESS_THIRTY_PERCENTAGE   = 30,           /**< \brief brightness 30%*/
	BRIGHTNESS_FOURTY_PERCENTAGE   = 40,           /**< \brief brightness 40%*/
	BRIGHTNESS_FIFTY_PERCENTAGE    = 50,            /**< \brief brightness 50%*/
	BRIGHTNESS_SIXTY_PERCENTAGE    = 60,            /**< \brief brightness 60%*/
	BRIGHTNESS_SEVENTY_PERCENTAGE  = 70,          /**< \brief brightness 70%*/
	BRIGHTNESS_EIGHTY_PERCENTAGE   = 80,           /**< \brief brightness 80%*/
	BRIGHTNESS_NINETY_PERCENTAGE   = 90,           /**< \brief brightness 90%*/
	BRIGHTNESS_HUNDRED_PERCENTAGE  = 100,          /**< \brief brightness 100%*/
	BrightnessLevelForceMyEnumIntSize = MAX_INT,
}  CP_BrightnessLevelTypeDef;

typedef enum {
	BACKLIGHT_OFF     = 0,         /**< \brief backlight off*/
	BACKLIGHT_ON,         			/**< \brief brightness on*/
	BacklightStatusForceMyEnumIntSize = MAX_INT,
} CP_BacklightStatusTypeDef;

typedef enum {
	BACKLIGHT_MODE_DAY     = 0,         		/**< \brief backlight day mode*/
	BACKLIGHT_MODE_TWILIGHT,         			/**< \brief brightness twilight mode*/
	BACKLIGHT_MODE_NIGHT,         				/**< \brief brightness night mode*/
	BacklightModeForceMyEnumIntSize = MAX_INT,
} CP_BacklightModeTypeDef;

typedef enum {
	BACKLIGHT_CONTROL_AUTO     = 0,         /**< \brief backlight auto, switch off after timeout*/
	BACKLIGHT_CONTROL_MANUAL,         		/**< \brief brightness manual, phone needs to send on/off commands*/
	BacklightControlModeForceMyEnumIntSize = MAX_INT,
} CP_BacklightControlModeTypeDef;

//typedef enum {
//	BL_MODE_DAY = 0, /**< \brief backlight day mode*/
//	BL_MODE_TWILIGHT , /**< \brief backlight twilight mode*/
//	BL_MODE_NIGHT, /**< \brief backlight night mode*/
//	SetBacklightModeForceMyEnumIntSize = MAX_INT,
//}CP_SetBacklightModeTypeDef;
//
//typedef enum {
//	BL_CONTROL_AUTO = 0, /**< \brief backlight will be on for some secondsand will get off after timeout(phone sends) */
//	BL_CONTROL_MANUAL , /**< \brief backlight will get on off(phone sends) */
//	SetBacklightControlModeForceMyEnumIntSize = MAX_INT,
//}CP_SetBacklightControlModeTypeDef;
//
//typedef enum { //max bl  brightness  is set as per correct visibility, battery saving of Dm.
//	AT_TWILIGHT , /**< \brief backlight brightness at twilight*/
//	AT_NIGHT, /**< \brief backlight brightness at night*/
//	SetMaxBacklightBrightnessForceMyEnumIntSize = MAX_INT,
//}CP_SetMaxBacklightBrightnessTypeDef;

typedef struct{
	CP_BacklightModeTypeDef blMode;     /**< \brief OTA type id,can be data or app+data */
} CP_BacklightModeDataTypeDef;

typedef struct{
	CP_BacklightControlModeTypeDef blControlMode;     /**< \brief OTA type id,can be data or app+data */
} CP_SetBacklightControlModeDataTypeDef;

//typedef struct{
//	CP_BacklightMaxBrightnessTypeDef blMaxBrightness;     /**< \brief OTA type id,can be data or app+data */
//}__attribute__((packed)) CP_SetMaxBacklightBrightnessOnDmTypeDef;

typedef struct {
	CP_BrightnessLevelTypeDef maxBrightnessTwilightMode;         		/**< \brief Max backlight brightness in twilight mode*/
	CP_BrightnessLevelTypeDef maxBrightnessNightMode;         			/**< \brief Max backlight brightness in night mode*/
} CP_MaxBacklightBrightnessDataTypeDef;

typedef struct{
	CP_BacklightStatusTypeDef backlightOnOff;     /**< \brief Backlight on/off */
} CP_BacklightStatusInfoTypeDef;

typedef struct {
	uint8_t operatingFrequency; /**< \brief frequency in kilo hertz (khz). */
	CP_BrightnessLevelTypeDef brighntnessLevel;
} CP_BacklightBrightnessConrolTypedef;


typedef struct  {
	void (*callback_cfm)(struct kbike_communication_protocol *, uint8_t);
	bool (*callback_ind)(struct kbike_communication_protocol *, uint8_t, uint8_t, void *, uint32_t);
	bool (*try_send)(void *, size_t);
} CP_CommunicationProtocolHandlers;
/***********************************************************************************
 * API - CP_ReceivePayload - Wrapper function.
 *   	 	This function is called from application to receive data from remote device.
 * *********************************************************************************/
bool CP_ReceivePayload(void *sessionData, size_t reveiveDataSize);

/**
 * @brief
 * API - CP_NewSession
 * 			This API is call to initialize display protocol.
 * 			This is use to register MTU size, callback functions used to
 * 			indicate & provide confirmation about various Commands.
 */
//struct kbike_communication_protocol* CP_NewSession(uint8_t imtu, uint8_t omtu,
//		struct communication_protocol_ind *ind_cb, struct communication_protocol_cfm *cfm_cb,
//		struct transport_cb *transport_cb);

struct kbike_communication_protocol* CP_NewSession(uint8_t imtu, uint8_t omtu,
		CP_CommunicationProtocolHandlers *communicationProtocolHandlers);

/***********************************************************************************
 * API - Dmfirmware update Commands - START
 *  	Commands Supported:
 *    firmware_update_start
 ***********************************************************************************/


bool CP_FirmwareUpdateStart(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id,void *data, uint8_t size) ;

/************************** API -Dmsensor Commands - END *****************************/
/***********************************************************************************
 * API - HID Commands - START
 *  	Commands Supported:

 *    key_press

 ***********************************************************************************/


bool CP_KeyPress(struct kbike_communication_protocol *session,
		uint8_t src_id, uint8_t session_id, void *data, uint8_t size);



/************************** API -HID Commands - END *****************************/



/***********************************************************************************
 * API - dm new parameters Commands - START
 *  	Commands Supported:
 *    current_date_time
 *    vehicle_battery_voltage
 *    source_name
 *    destination_name
 *    eta
 *    current_maneuver_icon
 *    current_maneuver_distance_unit
 *    current_maneuver_unit
 *    next_maneuver_icon
 *    next_maneuver_distance
 *    next_maneuver_unit
 *    bike_fuel_level
 *    bike_milage_distance
 *    bike_milage_unit
 *    travelled_distance
 *    travelled_unit
 *    dm_info_parameters
 *    user_name
 ***********************************************************************************/


bool CP_CurrentDateTime(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id,void *data, uint16_t size) ;
bool CP_VehicleBatteryVoltage(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_SourceName(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_DestinationName(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *sdestination_name, uint16_t size) ;
bool CP_FullJourneyEta(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_FullJourneyDistance(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_PoiEta(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_PoiDistance(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_DailyEta(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_DailyDistance(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_CurrentManueverIcon(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_CurrentManueverDistance(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_NextManueverIcon(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_NextManueverDistance(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_BikeFuelLevel(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_BikeMilageDistance(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_TotalTravelledDistance(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_DailyTravelledDistance(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_DmUserName(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id,void *data, uint16_t size) ;
bool CP_Passkey(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_FullJourneyRemainingDistance(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_DailyJourneyRemainingDistance(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_PoiRemainingDistance(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_FullJourneyReachingTime(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_DailyJourneyReachingTime(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_PoiJourneyReachingTime(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_LcdBacklightBrightnessControl(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_LcdDisplayOnOff(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_DmBatteryChargingOnOff(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_DmBatteryVoltage(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_DailyTargatedDestinationName(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_DailyCompletedRideTime(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_FullJourneyCompletedRideTime(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_SetHeadingDirection(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_SetHeadingDistance(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_SetHeadingToDestination(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_CurrentIconRoundAboutBearingInfo(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_NextIconRoundAboutBearingInfo(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_HeadingBearingAngle(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_RouteDeviationBearingInfo(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);

/************************** API - dm new parameters Commands - END *****************************/



/*****  API -command grp alerts***START*********************************************************/
//API for incoming alert
bool CP_IncomingAlerts(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_AlertResponse(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_AlertResponseSent(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_AlertResponseFailed(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_IncomingAlertEnd(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);


//API for outgoing alert
bool CP_OutgoingAlerts(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_Alertsent(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_AlertFailed(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_AlertSentResponse(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_AlertSentNoHelpReceived(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_OutgoingAlertEnd(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_OutgoingAlertModeEntry(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_OutgoingAlertModeExit(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_OutgoingAlertClosed(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);

/*****  API -command grp alerts***END*********************************************************/


/***********************************************************************************
 *   API -cmd grp-> dm sys configuration parameter - START
 *  	Commands Supported:
 *    SetAllSysCfgParameters
 *    SetSysCfgDmSerialNumber
 *    SetSysCfgDmProductionWeekNumber
 *    SetSysCfgDmOptionalBytes
 *    SetSysCfgDmHardwareVersion
 *    SetSysCfgDmSoftwareVersion
 *    GetAllSysCfgParameters
 *    GetSysCfgDmSerialNumber
 *    GetSysCfgDmProductionWeekNumber
 *    GetSysCfgDmOptionalBytes
 *    GetSysCfgDmHardwareVersion
 *    GetSysCfgDmSoftwareVersion
 ***********************************************************************************/
bool CP_SetAllSysCfgParameters(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id,void *data, uint16_t size);
bool CP_SetSysCfgDmSerialNumber(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id,void *data, uint16_t size);
bool CP_SetSysCfgDmProductionWeekNumber(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id,void *data, uint16_t size);
bool CP_SetSysCfgDmOptionalBytes(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id,void *data, uint16_t size) ;
bool CP_SetSysCfgDmHardwareVersion(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id,void *data, uint16_t size);
bool CP_SetSysCfgDmSoftwareVersion(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id,void *data, uint16_t size) ;
bool CP_GetAllSysCfgParameters(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id,void *data, uint16_t size) ;
bool CP_GetSysCfgDmSerialNumber(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id,void *data, uint16_t size) ;
bool CP_GetSysCfgDmProductionWeekNumber(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id,void *data, uint16_t size);
bool CP_GetSysCfgDmOptionBytes(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id,void *data, uint16_t size);
bool CP_GetSysCfgDmHardwareVersion(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id,void *data, uint16_t size);
bool CP_GetSysCfgDmSoftwareVersion(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id,void *data, uint16_t size);
/************************** API - dm system config parameters - END *****************************/


/***********************************************************************************
 * API - dm navigation control Commands - START
 *  	Commands Supported:
 *     journey start stop
 *     daily journey startstop
 *     full journey start stop
 *     tbt start stop
 *     tbt hide show
 *     route deviation start
 *     ride pause resume


 ***********************************************************************************/
bool CP_SetJourney(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_DailyJourneyStartStop(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_FullJourneyStartStop(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_EnableDisableTbt(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_TbtStartStop(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_TbtHideShow(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_HeadingStart(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_RouteDeviationStart(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_RidePauseResume(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_RouteWaypointReached(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_GpsStatusInfo(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_FollowStraightRoad(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_ApproachingPoi(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_ApproachingDestination(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
/************************** API - dm navigation control - END *****************************/

/***********************************************************************************
 * API - backlight  Commands - START
 *  	Commands Supported:
 *    set backlight brightness mode
 *    set backlight brightness control mode
 *    set max backlight brightness


 ***********************************************************************************/


bool CP_SetBacklightMode(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_SetBacklightControlMode(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size);
bool CP_SetMaxBacklightBrightness(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
bool CP_SetMaxBacklightOnOff(struct kbike_communication_protocol *session,
		uint8_t source_id, uint8_t session_id, void *data, uint16_t size) ;
/************************** API - backlight control Commands - END *****************************/

void CP_ParseDataLocal(struct kbike_communication_protocol * session);

#ifdef __cplusplus
}
#endif
#endif /* CP_IF_H_ */
