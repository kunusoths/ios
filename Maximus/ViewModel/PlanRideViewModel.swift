//
//  PlanRideViewModel.swift
//  Maximus
//
//  Created by Isha Ramdasi on 28/06/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import Alamofire


class PlanRideViewModel: NSObject {

    let rideRepo: PlanRideProtocol

    init(planRideRepoDelegate : PlanRideProtocol) {
        self.rideRepo = planRideRepoDelegate
    }
    
    func saveRide(rideResponse: RideCreateResponse,type: String, successCallback: @escaping successData, failureCallback:@escaping errorData) {
        rideRepo.saveRideResponse(rideResponse: rideResponse, type: type, successCallback: { (data) in
            successCallback(data)
        }) { (error) in
            failureCallback(error)
        }
    }
    
    func getRideRouteDetails(rideCard: Rides, type: String,successCallback: @escaping successData, failureCallback:@escaping errorData) {
        rideRepo.getRideDetailsOfOldRides(rideCard: rideCard, type: type, successCallback: { (data) in
            successCallback(data)
        }) { (error) in
            failureCallback(error)
        }
    }
    
    func getRideRouteById(rideID: Int, bikerID: Int, type: String ,successCallback: @escaping successData, failureCallback:@escaping errorData) {
        rideRepo.getRideRouteById(rideID: rideID, bikerId: bikerID, type: type, successCallback: { (data) in
            successCallback(data)
        }) { (error) in
            failureCallback(error)
        }
    }
    
    func getRoutesForLatLng(jsonObj: [String: Any], type: String ,successCallback: @escaping successData, failureCallback:@escaping errorData) {
        rideRepo.getRoutesForLatLng(jsonObj: jsonObj, type: type, successCallback: { (data) in
            successCallback(data)
        }) { (error) in
            failureCallback(error)
        }
    }
    
    func getPOIWithPolyline(polyline: String, request: String, successCallback: @escaping successData, failureCallback:@escaping errorData) {
        rideRepo.getPOIWithPolyline(polyline: polyline, request: request, successCallback: { (data) in
            successCallback(data)
        }) { (error) in
            failureCallback(error)
        }
    }
    
    func getPOIDetails(poiID: String, request: String ,successCallback: @escaping successData, failureCallback:@escaping errorData) {
        rideRepo.getPOIDetails(poiId: poiID, request: request, successCallback: { (data) in
            successCallback(data)
        }) { (error) in
            failureCallback(error)
        }
    }
    
    func getPlannedWavepoints(rideID: Int, request: String ,successCallback: @escaping successData, failureCallback:@escaping errorData) {
        rideRepo.getPlannedWavepoints(rideID: rideID, request: request, successCallback: { (data) in
            successCallback(data)
        }) { (error) in
            failureCallback(error)
        }
    }
    
    func getRideRouteByID(rideId: Int, bikerId:Int, type: String,successCallback: @escaping successData, failureCallback:@escaping errorData){
        rideRepo.getRideRouteById(rideID: rideId, bikerId: bikerId, type: type, successCallback: { (data) in
            successCallback(data)
        }) { (error) in
            failureCallback(error)
        }
    }
    
    func createRide(parameter: [String:Any], rqstType:HTTPMethod, request: String,successCallback: @escaping successData, failureCallback:@escaping errorData) {
        rideRepo.createRide(parameter: parameter, rqstType: rqstType, request: request, successCallback: { (data) in
            successCallback(data)
        }) { (error) in
            failureCallback(error)
        }
    }
    
    func uploadImage(data:Data, url:String, request: String, successCallback: @escaping successData, failureCallback: @escaping errorData ) {
        let imgUploader = ImageUploader()
        imgUploader.uploadImage(data: data, url: url, successCallback: { (data) in
            var dict: [String: Any] = [:]
            dict["RequestType"] = "UploadImage"
            successCallback(dict)
        }) { (errorData) in
            var error: [String: Any] = [:]
            error["description"] = "Image Upload Failed"
            failureCallback(error)
        }
    }
    
    func getRoutesFromGoogleApi(jsonObj: [String: Any], type: String, successCallback: @escaping successData, failureCallback:@escaping errorData) {
        rideRepo.getRoutesFromGoogleApi(jsonObj: jsonObj, type: type, successCallback: { (data) in
            successCallback(data)
        }) { (errorData) in
            failureCallback(errorData)
        }
    }
    
//    func repoSuccessCallback(data: Any) {
//        self.saveRideDelegate?.saveRideSuccess(data: data)
//    }
//
//    func repoFailureCallback(error: [String : Any]) {
//        self.saveRideDelegate?.failure(data: error)
//    }
}

//extension PlanRideViewModel: ImageUploadDelegate {
//    func uploadImageSucess(data: [String : Any], successCallback: @escaping successData, failureCallback: @escaping errorData) {
//        var dict: [String: Any] = [:]
//        dict["RequestType"] = "UploadImage"
////        self.saveRideDelegate?.saveRideSuccess(data: dict)
//    }
//
//    func uploadImageFailed(Error: [String : Any]) {
//        var error: [String: Any] = [:]
//        error["description"] = "Image Upload Failed"
////        self.saveRideDelegate?.failure(data: error)
//    }
//
//}
