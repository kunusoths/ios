//
//  MyRidesViewModel.swift
//  Maximus
//
//  Created by Isha Ramdasi on 26/06/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import CoreData

protocol ReloadMyRidesDelegate: class {
    func reloadData(data: Any)
    func failure(data: [String: Any])
}

class MyRidesViewModel: NSObject {
    weak var reloadDataDelegate: ReloadMyRidesDelegate?
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()

    let rideRepo: RideRepositoryProtocol //= LocationRepository()
    
    
    init(rideRepoDelegate : RideRepositoryProtocol) {
        self.rideRepo = rideRepoDelegate
    }
    
    func getAllPlannedRides(bikerId: Int, pageCount: Int, size: Int, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        rideRepo.getMyPlannedRidesFromRepo(type: "Rides", pageNo: pageCount, size: size, bikerID: bikerId, request: "GetPlannedRides", successCallback: {responseData in
            successCallback(responseData)
        }, failureCallback: {failureData in
            self.repoFailureCallback(error: failureData, successCallback: { (data) in
                successCallback(data)
            }, failureCallback: { (errorData) in
                failureCallback(errorData)
            })
        })
    }

    func getRide(withRideId rideId: Int, andBikerId bikerId: Int, request: String, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        
        rideRepo.getRide(withRideID: rideId, andBikerId: bikerId, request: request, successCallback: { (resData) in
            //
            successCallback(resData)
        }) { (error) in
            //
            self.repoFailureCallback(error: error, successCallback: { (data) in
                //
                successCallback(data)
            }, failureCallback: { (err) in
                //
                failureCallback(err)
            })
        }
        
    }
    
    func getAllRidesOfBiker(request: String, pageNo:Int, bikerID: Int, size: Int, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        rideRepo.getAllRidesOfBiker(request: request, pageNo: pageNo, bikerID: bikerID, size: size, successCallback: {responseData in
            successCallback(responseData)
        }, failureCallback: {failureData in
            self.repoFailureCallback(error: failureData, successCallback: { (data) in
                successCallback(data)
            }, failureCallback: { (errorData) in
                failureCallback(errorData)
            })
        })
    }
    
    func editRide(rideID: String, type:String, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        rideRepo.editRideDetails(rideID: Int(rideID)!, request: type, successCallback: {responseData in
            successCallback(responseData)
        }, failureCallback: {failureData in
            self.repoFailureCallback(error: failureData, successCallback: { (data) in
                successCallback(data)
            }, failureCallback: { (errorData) in
                failureCallback(errorData)
            })
        })
    }
    
    func cloneRide(rideID: Int, data: [String: Any], type: String, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        rideRepo.cloneRide(rideID: rideID, data: data, request: type, successCallback: {responseData in
            successCallback(responseData)
        }, failureCallback: { failureData in
            self.repoFailureCallback(error: failureData, successCallback: { (data) in
                successCallback(data)
            }, failureCallback: { (errorData) in
                failureCallback(errorData)
            })
        })
    }
    
    func leaveRide(rideID: Int, type: String, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        rideRepo.leaveRide(rideID: rideID, request: type, successCallback: {responseData in
            successCallback(responseData)
        }, failureCallback: {failureData in
            self.repoFailureCallback(error: failureData, successCallback: { (data) in
                successCallback(data)
            }, failureCallback: { (errorData) in
                failureCallback(errorData)
            })
        })
    }
    
    func getRideMembers(rideID: Int, type: String, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        rideRepo.getRideMembers(rideID: rideID, request: type, successCallback: {responseData in
            successCallback(responseData)
        }, failureCallback: {failureData in
            self.repoFailureCallback(error: failureData, successCallback: { (data) in
                successCallback(data)
            }, failureCallback: { (errorData) in
                failureCallback(errorData)
            })
        })
    }
    
    func repoFailureCallback(error: [String: Any], successCallback: @escaping successData, failureCallback: @escaping errorData) {
        if let data = error["RequestType"] as? String, data == "DiscardRide" {
            failureCallback((error["data"] as? [String: Any])!)
        } else if let data = error["RequestType"] as? String, data == "JoinRide" {
            failureCallback(error)
        }else if let data = error["RequestType"] as? String, data == "LeaveRide" {
            failureCallback(error)
        } else if let data = error["RequestType"] as? String, data == "CloneRide" {
            failureCallback(error) }
        else {
            if NetworkCheck.isNetwork() {
                if getRidesFromDB().count > 0 {
//                    self.reloadDataDelegate?.reloadData(data: getRidesFromDB())
                    successCallback(getRidesFromDB())
                }else{
                    var dict:[String: Any] = [:]
                    dict["RequestType"] = "GetPlannedRides"
                    dict["data"] = Rides.getNotCompletedMyRideObjs(context: context)
                    
                    dict["isLastPage"] = true
                    successCallback(dict)
                }
            } else {
                failureCallback(error)
            }
        }
    }

    func joinRide(rideID:Int,parameter: [String:Any], requestType: String, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        rideRepo.joinRide(rideID: rideID, parameter: parameter, requestType: requestType, successCallback: {responseData in
            successCallback(responseData)
        }, failureCallback: {failureData in
            self.repoFailureCallback(error: failureData, successCallback: { (data) in
                successCallback(data)
            }, failureCallback: { (errorData) in
                failureCallback(errorData)
            })
        })
    }
    
    func rejectRide(rideID:Int,parameter: [String:Any], requestType: String, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        rideRepo.rejectRide(rideID: rideID, parameter: parameter, requestType: requestType, successCallback: {responseData in
            successCallback(responseData)
        }, failureCallback: {failureData in
            self.repoFailureCallback(error: failureData, successCallback: { (data) in
                successCallback(data)
            }, failureCallback: { (errorData) in
                failureCallback(errorData)
            })
        })
    }
    
    func repoSuccessCallback(data: Any) {
        self.reloadDataDelegate?.reloadData(data: data)
    }
    
    func getRidesFromDB() -> [Rides] {
        let ridesArray = Rides.getNotCompletedMyRideObjs(context: context)
        return ridesArray
    }
    
    func getDataSourceForMyRidesActions(rideCard: Rides) -> [[String: Any]] {
        var dataSourceArray:[[String: Any]] = []
        let  adminId = (rideCard.admin_biker_id)
        if adminId == String(MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID))  {
            //admin
            //not in progress - edit share - later add clone
            var dict: [String: Any] = [:]
            dict["ButtonImg"] = #imageLiteral(resourceName: "share_new")
            dict["LblTitle"] = "SHARE"
            dict["Type"] = "ADMIN"
            dict["dict"] = rideCard
            dataSourceArray.append(dict)
            dict = [:]
            dict["ButtonImg"] = #imageLiteral(resourceName: "edit_orange")
            dict["LblTitle"] = "EDIT RIDE"
            dict["Type"] = "ADMIN"
            dict["dict"] = rideCard
            dataSourceArray.append(dict)
            dict = [:]
            dict["ButtonImg"] = #imageLiteral(resourceName: "repeat_orange")
            dict["LblTitle"] = "CLONE"
            dict["Type"] = "ADMIN"
            dict["dict"] = rideCard
            dataSourceArray.append(dict)
        } else {
            //rider
            //not in progress - leave share - later add clone
            var dict: [String: Any] = [:]
            dict["ButtonImg"] = #imageLiteral(resourceName: "share_new")
            dict["LblTitle"] = "SHARE"
            dict["Type"] = "RIDER"
            dict["dict"] = rideCard
            dataSourceArray.append(dict)
            dict = [:]
            dict["ButtonImg"] = #imageLiteral(resourceName: "download_orange")
            dict["LblTitle"] = "MAKE MINE"
            dict["Type"] = "RIDER"
            dict["dict"] = rideCard
            dataSourceArray.append(dict)
            dict = [:]
            dict["ButtonImg"] = #imageLiteral(resourceName: "leave_ride_orange")
            dict["LblTitle"] = "LEAVE"
            dict["Type"] = "RIDER"
            dict["dict"] = rideCard
            dataSourceArray.append(dict)
        }
        return dataSourceArray
    }
}
