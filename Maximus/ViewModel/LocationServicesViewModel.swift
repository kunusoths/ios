//
//  LocationServicesViewModel.swift
//  Maximus
//
//  Created by APPLE on 03/10/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit


class LocationServicesViewModel: NSObject {
    let locationServiceRepo: BikeLocationProtocol //= LocationRepository()
    
    
    init(locationRepo : BikeLocationProtocol) {
        self.locationServiceRepo = locationRepo
    }
    
    
    func getDeviceLocation(bikerID: Int, requestType: String, handler:@escaping ([String : Any]?, [String : Any]?) -> Void ) {
        locationServiceRepo.getDeviceLocation(bikerID: bikerID, requestType: requestType) { (data, err) in
            
            handler(data , err)
            
        }
    }

}
