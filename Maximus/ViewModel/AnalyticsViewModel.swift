//
//  AnalyticsViewModel.swift
//  Maximus
//
//  Created by APPLE on 01/10/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit

class AnalyticsViewModel: NSObject {
    fileprivate let analyticsRepoObj = AnalyticsRepository()
    
    override init() {
        super.init()
    }
    
    /*
     Get recommended rides
     */
    func getRecommendedRides(bikerID: Int, requestType: String, pageNo: Int, size: Int,successCallback: @escaping successData, failureCallback:@escaping errorData) {
        analyticsRepoObj.getRecommendedRides(bikerID: bikerID, requestType: requestType, pageNo: pageNo, size: size, successCallback: { (data) in
            successCallback(data)
        }) { (errorData) in
            failureCallback(errorData)
        }
    }
}
