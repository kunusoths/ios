//
//  RideStatsViewModel.swift
//  Maximus
//
//  Created by Isha Ramdasi on 16/07/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
protocol RideStatsDelegate: class {
    func rideStatisticSuccess(data: Any)
    func failure(data: [String: Any])
}

class RideStatsViewModel: NSObject, RideStatsRepoCallback {
    weak var rideStatsDelegate: RideStatsDelegate?
    let rideStatsRepo = RideStatsRepository()
    
    override init() {
        super.init()
        rideStatsRepo.rideStatsRepoDelegate = self
    }
    
    func getRideStatsFor(bikerID: Int, page: Int, size: Int, request: String) {
        self.rideStatsRepo.getRideStatsFor(bikerID: bikerID, page: page, size: size, request: request)
    }
    
    func putRideTitle(parameter:[String:Any], request: String){
        self.rideStatsRepo.putRideTitle(parameter: parameter, request: request)
    }
    
    func getRideStats(rideID:Int, bikerID:Int, request: String) {
        self.rideStatsRepo.getRideStats(rideID: rideID, bikerID: bikerID, request: request)
    }
    
    func getRideRoute(rideID:Int, bikerID:Int, request: String) {
        self.rideStatsRepo.getRideRoute(rideID: rideID, bikerID: bikerID, request: request)
    }
    
    func getGraphSpeedPoints(rideID:Int, bikerID:Int, request: String) {
        self.rideStatsRepo.getGraphSpeedPoints(rideID: rideID, bikerID: bikerID, request: request)
    }
    
    func getStickerBoard(bikerID: Int, request: String) {
        self.rideStatsRepo.getStickerBoard(bikerID: bikerID, requestType: request)
    }
    
    func rideStatsRepoSuccessCallback(data: Any) {
        self.rideStatsDelegate?.rideStatisticSuccess(data: data)
    }
    
    func rideStatsRepoFailureCallback(error: [String : Any]) {
        self.rideStatsDelegate?.failure(data: error)
    }
}
