//
//  CurrentRideViewModel.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 16/07/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import Foundation

enum RideType:String {
    
    case NONE    = "NONE"
    case AD_HOC   = "AdHoc"
    case PLANNED = "Planned"
    
}

protocol CurrentRideViewModelDelegate:class {
    //
    func startRideSuccess(data: [String:Any])
    
    func stopRideSuccess(data: [String:Any])
   /*
    * Has keys "data" as Rides. "isRideInProgress",
    */
    func rideInProgressSuccess(data:[String:Any])
    
    func rideAPIFailed(data:[String:Any])
}

class CurrentRideViewModel: NSObject , CurrentRideRepoDelegate {
    
    private let repo = CurrentRideRepositery()
    weak var delegate:CurrentRideViewModelDelegate?
    let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.newBackgroundContext()
    let rideRequestHandler = RideRequestHandler()
    
    override init() {
        super.init()
        print("CurrentRideViewModel: init")
        repo.delegate = self
    }
    
    deinit {
        print("CurrentRideViewModel: deinit")
    }
    
    func startRide(type:RideType, rideId:Int) {
        
        switch type {
            
        case .PLANNED:
            repo.startRide(rideID: rideId)
            
        case .AD_HOC:
            // create ride and save
            break
        default:
            break
            
        }
    }
    
    func isRideInProgress(){
        repo.delegate = self
        repo.repoGetRideProgress()
    }
    
    func stopRide(rideID: Int){
        repo.delegate = self
        repo.stopRide(rideId: rideID)
        self.delegate?.stopRideSuccess(data: [:])
    }
    
    
    //MARK:- CurrentRideRepoDelegate methods
    
    func repoSuccess(data: [String : Any]) {
        //
        print("CurrentRideViewModel: repo Success")
        let rideID = data["rideID"] as? Int

        let type:RideStateAPIType = data["type"] as! RideStateAPIType
        
        switch type {
            
        case .RIDE_START:
            
            print("CurrentRideViewModel: Ride Started")
            RideRequest.deletePreviousRequest(rideID: (rideID?.toString())! , type: .Start, context: context!)
            self.delegate?.startRideSuccess(data: data)
            break
        
        case .RIDE_STOP:
            print("CurrentRideViewModel: Ride Stopped")
            RideRequest.deletePreviousRequest(rideID: (rideID?.toString())!, type: .Stop, context: context!)
            self.delegate?.stopRideSuccess(data: data)
            break
            
        case .RIDE_IN_PROGRESS:
            print("CurrentRideViewModel: Ride is Inprogress")
            if let isRideInProgress:Bool = data["isRideInProgress"] as? Bool,
                isRideInProgress {
                // ride is in progress
                self.delegate?.rideInProgressSuccess(data: data)
            }else{
                // no ride is in progress
            }
            
            
        default:
            break
            
        }
    }
    
    func repoFailed(data: [String : Any]) {
        //
        self.delegate?.rideAPIFailed(data: data)
    }
    
    func offlineRideStartSuccess(data: [String : Any]) {
        
        let rideCard = data["data"] as! Rides
        let data:[String:Any] = ["type": RideStateAPIType.RIDE_START,
                                 "rideData": rideCard]
        self.delegate?.startRideSuccess(data: data)
    }
    
    
}
