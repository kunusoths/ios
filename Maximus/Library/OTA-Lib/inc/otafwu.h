/**
 ******************************************************************************
 * @file    otafwu.h
 * @author  KPIT Technologies
 * @version V1.0.0
 * @date    10-Aug-2017
 * @brief   Header file for ota fw upgrade library source file.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT(c) 2016 KPIT Technologies Ltd.</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef OTA_LIB_SRC_OTAFWU_H_
#define OTA_LIB_SRC_OTAFWU_H_
/* Includes ------------------------------------------------------------------*/
#include "otafwu_if.h"
#include <stdint.h>
#include <stdlib.h>
/* Defines ------------------------------------------------------------------- */
#define NOTIFICATION_WINDOW 8
#define TRUE 1
#define FALSE 0
#define SET 1
#define RESET 0
/* Variables --------------------------------------------------------------- */

typedef struct
{
    unsigned short  replyCounter;
    unsigned short  statusCode;
} OTA_responsePacketTypeDef;

typedef enum
{
    FWU_NOT_STARTED = 1,
    FWU_ONGOING,
    FWU_COMPLETED,
    FWU_ABORTED,
} OTA_modeStatus;

typedef struct
{
    long int previousFilePointerPosition;
    long int currentFilePointerPosition;
} OTA_imageFilePointerInfoTypeDef;


/* Prototypes --------------------------------------------------------------- */
OTA_ReturnType OTA_ReadCurrentImageInfo(void);
OTA_ReturnType OTA_WriteNewImageInfo(void);
OTA_ReturnType OTA_WriteNewDataContent (unsigned char * newImageInfo);
OTA_ReturnType OTA_UpdateStatus(OTA_Status status,float percentage);
OTA_ReturnType OTA_InitiateOta(void);
void OTA_CalculateAvailableSpace(void);
void OTA_CorrectNewImageSize(void);
OTA_ReturnType OTA_RequestStatus(void);
OTA_ReturnType OTA_ConveySesionAbortStatusToDm();
void OTA_resetEverything(void);
#endif /* OTA_LIB_SRC_OTAFWU_H_ */

