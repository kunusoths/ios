/**
 ******************************************************************************
 * @file    otafwu_if.h
 * @author  KPIT Technologies
 * @version V1.0.0
 * @date    10-Aug-2017
 * @brief   Interface Header file for ota fw upgrade library source file.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT(c) 2016 KPIT Technologies Ltd.</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef OTA_LIB_OTAFWU_IF_H_
#define OTA_LIB_OTAFWU_IF_H_
#ifdef __linux__
#define LOG_TAG "OTA"
#endif

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "string.h"
#include "stdbool.h"
#ifdef __linux__
#include <android/log.h>
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#elif __APPLE__
#include <syslog.h>
#endif
/* Defines ------------------------------------------------------------------- */

/* Variables --------------------------------------------------------------- */
typedef enum
{
    OTA_DM_FWU_MODE_SET = 1,
    OTA_INVALID_PATH,
    OTA_ONGOING,
    OTA_CHUNK_SUCCESS,
    OTA_CHUNK_FAILURE,
    OTA_SUCCESS,
    OTA_FAILURE,
    OTA_ABORT,
    OTA_PROCEED_SUFFICIENT_STORAGE,
    OTA_CANCELLED_INSUFFICIENT_STORAGE,
    OTA_COMPLETED,
    OTA_WAIT_FOR_DM_RESPONSE_TIMEOUT,
	OTA_TIMEOUT_ERROR_ABORT,
	OTA_EXCEEDED_REPEATED_ATTEMPT_COUNT,
	OTA_DM_FLASH_ERROR,
	OTA_UNEXPECTED_END_OF_FILE_OCCURED,
	OTA_ERROR_IN_READING_FILE,
	OTA_ERROR_IN_READING_IMAGE_PACKET,
	OTA_DM_FULL_IMAGE_CHECKSUM_ERROR,
} OTA_Status;

typedef enum
{
    FW_OTA_DATA = 1,
    FW_OTA_APP,
    FW_OTA_DATA_APP,
} OTA_Type;

typedef enum
{
    OTA_RETURN_SUCCESS = 0,
    OTA_RETURN_FAILURE,
} OTA_ReturnType;

struct OTA_APP_API_CB {
    OTA_ReturnType (*otaGetCurrentImageInfoAPI)(char*);
    OTA_ReturnType (*otaWriteNewImageAPI)(char*);
    OTA_ReturnType (*otaInitiateSeqNumberAPI)(void);
    OTA_ReturnType (*otaWriteNewContentAPI)(char*);
    OTA_ReturnType (*otaUpdateStatusCB)(OTA_Status,float);
	OTA_ReturnType (*otaUpdateRequest)(char*); 				// write value in ota mode char
}__attribute__ ((packed));

/* Prototypes --------------------------------------------------------------- */
void OTA_WriteResponseCB(unsigned char * responsePacket);
OTA_Status OTA_ValidateImageSizeAndAvailableSpace(unsigned char * imageFile, uint32_t crc);
void OTA_NewSession(struct OTA_APP_API_CB * otaAPILinker);
OTA_ReturnType OTA_StartUpgrade(void);
void OTA_GetModeStatusCB(unsigned char * modestatus);
void OTA_AbortUpgrade();
#endif /* OTA_LIB_OTAFWU_IF_H_ */

