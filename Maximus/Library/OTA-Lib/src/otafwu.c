/**
 ******************************************************************************
 * @file    otafwu.c
 * @author  KPIT Technologies
 * @version V1.0.0
 * @date    10-Aug-2017
 * @brief   Application layer source file for ota fw upgrade over Ble library.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT(c) 2016 KPIT Technologies Ltd.</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

/* Includes ----------------------------------------------------------------- */
#include "otafwu.h"
#include "otafwu_if.h"
#include "unistd.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <stdint.h>
#include <string.h>
/* Defines ---------------------------------------------------------- */

#define PAGE_SIZE 256
#define BUF_SIZE 128
#define BYTE_INCREMENT 128
#define PCKT_SIZE 16

#define OTA_SUCCESS                        0x0000
#define OTA_FLASH_VERIFY_ERROR             0x003C
#define OTA_FLASH_WRITE_ERROR              0x00FF
#define OTA_SEQUENCE_ERROR                 0x00F0
#define OTA_CHECKSUM_ERROR                 0x000F
#define OTA_START_UPGRADE               0xABCD
#define OTA_PCKT_RECEIVED               0x1A00
#define OTA_IMAGE_CHECKSUM_ERROR         0x00CE
#define OTA_UPDATE_DONE_SUCCESSFULLY    0x00DE



#define OTA_MODE_SET            0xBC
#define OTA_MODE_NOT_SET        0xC1

//#define DM_DEFAULT_BASEADDRS    0x08034000
#define DM_DEFAULT_BASEADDRS    0x0802C800
#define DM_DEFAULT_ENDADDRS        0x08080000

#define MAX_REPEATED_ATTEMPTS_COUNT 10

#define ELEMENTS_TO_READ 1
/* Private variables ---------------------------------------------------------*/
unsigned char newImageFilePath[255];
unsigned int newImageSize,newCorrectedImageSize,newImageBase;
unsigned char sizeOffset;
unsigned char currentImageInfo[8] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
unsigned char * ptrCurrentImageInfo;
unsigned char newImageInfo[9] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
unsigned char * ptrNewImageInfo;
unsigned int appBaseAddress,appEndAddress;//dataBaseAddress,dataEndAddress;
unsigned int availableAppSize;//availableDataSize;
char sendLastStatus = 0x42;
char otaSessionAbort = 0x5D;
long int totalBytesWritten;
unsigned int lastChunkByteCount;
char remainingBytes;
float percentage = 0;
unsigned int fileSize = 0;
struct OTA_APP_API_CB * appApiLinkerPtr;
FILE* fwfile;
OTA_imageFilePointerInfoTypeDef dmImagefilepointerInfo;
volatile unsigned short repeatedAttemptsCount;
volatile unsigned short currentSequenceNumber;
volatile unsigned int positionToSeek;
volatile unsigned int nWaitingForResponseCount;

//static _Bool otaOngoing = RESET;
static _Bool lastChunk = RESET;
static _Bool sendnxtpckt = RESET;
OTA_Status status;
OTA_modeStatus otaModeStatus;


uint32_t TestFile(char *Filename);
uint32_t Crc32FastBlock(uint32_t Crc, uint32_t Size, uint32_t *Buffer); // 32-bit units
uint32_t Crc32Fast(uint32_t Crc, uint32_t Data);
uint32_t Crc32Block(uint32_t Crc, uint32_t Size, uint32_t *Buffer); // 32-bit units
uint32_t Crc32(uint32_t Crc, uint32_t Data);
uint32_t fileCRC;

uint8_t *fullFirmwareBufferPtr;
uint8_t *readFirmwareBufferPtr;
uint8_t *startFirmwareBufferPtr;

//static _Bool waitfornxtchunk;
/* ----------------------------------Prototypes ---------------------------------------- */


/* --------------------------------------Code ----------------------------------------- */

OTA_Status OTA_ValidateImageSizeAndAvailableSpace(unsigned char * imageFile, uint32_t crc)
{
    fwfile = 0;
    uint32_t adjustedSize;
    uint32_t calculatedCrc;
    
#ifdef __linux__
    LOGI("Created new instance of OTA lib\r\n");
#elif __APPLE__
    fprintf(stdout,"Created new instance of OTA lib \r\n");
#endif
    
    OTA_ReturnType result;
    OTA_Status status;
    unsigned int temp;
    strncpy(newImageFilePath,imageFile,255);
    fwfile = fopen(newImageFilePath,"rb");
    if (fwfile)
    {
        fseek(fwfile, 0, SEEK_END);
        fileSize = ftell(fwfile);
        
        //making size divisible of 128
        temp = fileSize / 128;
        if((fileSize % 128) == 0)
        {
            adjustedSize = temp* 128;
        }
        else
        {
            adjustedSize = (temp+1)* 128;
        }
        fseek(fwfile, 0, SEEK_SET);
        
        fullFirmwareBufferPtr = (void *) malloc(adjustedSize);
		if(fullFirmwareBufferPtr == NULL)
		{
			fprintf(stdout,"Error in allocating dynamic memory to fullFirmwareBufferPtr\r\n");
		}
		else
		{
			fprintf(stdout,"Success in allocating dynamic memory to fullFirmwareBufferPtr\r\n");
		}
			
        readFirmwareBufferPtr = fullFirmwareBufferPtr;
        startFirmwareBufferPtr = fullFirmwareBufferPtr;
        
        memset((void*)fullFirmwareBufferPtr, 0, adjustedSize); //clearing full firmware buffer
        
        fread(fullFirmwareBufferPtr, fileSize, 1, fwfile);
        
        fclose(fwfile);
        calculatedCrc = Crc32FastBlock(0xFFFFFFFF, adjustedSize >> 2, (void *)startFirmwareBufferPtr);
        //Bring the startFirmwareBufferPtr back to begin as CRC Calculation incremented it
        startFirmwareBufferPtr = fullFirmwareBufferPtr;
        fileCRC = crc;
        
        if (calculatedCrc != fileCRC)
        {
#ifdef __linux__
            LOGI("CRC Check Failed\r\n");
#elif __APPLE__
            fprintf(stdout,"CRC Check Failed\r\n");
#endif
            //Retry reading and calculating again
        }
        else
        {
#ifdef __linux__
            LOGI("CRC Check Passed, continue\r\n");
#elif __APPLE__
            fprintf(stdout,"CRC Check Passed, continue\r\n");
#endif
        }
    }
    else
    {
        status = OTA_INVALID_PATH;
        return status;
    }
    newImageSize = fileSize;
    OTA_CorrectNewImageSize();                        // correcting the image size as to come in the table of half page size of STM32L1
    result = OTA_ReadCurrentImageInfo();            // reading from btlImageChar and updating the global variables, actually it should be done
    if(result != OTA_RETURN_SUCCESS)                //only once not 2 times when upgrading data+ app, though no harm
    {
        // return error
    }
    
    OTA_CalculateAvailableSpace();                    //It updates the base and end addresses of data and app, and also calculates available spaces
    //actually it also needs to be done only once not 2 times when upgrading data+ app, though no harm
#ifdef __linux__
    LOGI("imagesize: %d,,corrected image size: %d \r\n", newImageSize, newCorrectedImageSize);
#elif __APPLE__
    fprintf(stdout,"imagesize: %d,,corrected image size: %d \r\n", newImageSize, newCorrectedImageSize);
#endif
#ifdef __linux__
    //    LOGI("appBaseAddress: %x,,appEndAddress: %x,,dataBaseAddress: %x,,dataEndAddress: %x,,availableAppSize: %x,,availableDataSize: %d \r\n", appBaseAddress, appEndAddress, dataBaseAddress, dataEndAddress, availableAppSize, availableDataSize);
    LOGI("appBaseAddress: %x,,appEndAddress: %x,,,availableAppSize: %x \r\n", appBaseAddress, appEndAddress,availableAppSize);
#elif __APPLE__
    //    fprintf(stdout,"appBaseAddress: %x,appEndAddress: %x,,dataBaseAddress: %x,,dataEndAddress: %x,,availableAppSize: %x,,availableDataSize: %x \r\n", appBaseAddress, appEndAddress, dataBaseAddress, dataEndAddress, availableAppSize, availableDataSize);
    fprintf(stdout,"appBaseAddress: %x,,appEndAddress: %x,,,availableAppSize: %x \r\n", appBaseAddress, appEndAddress,availableAppSize);
#endif
    
    if(newCorrectedImageSize >= availableAppSize)
    {
        status = OTA_CANCELLED_INSUFFICIENT_STORAGE;
        OTA_UpdateStatus(status, 0);
#ifdef __linux__
        LOGI("OTA cancelled due to insufficient storage \r\n");
#elif __APPLE__
        fprintf(stdout,"OTA cancelled due to insufficient storage  \r\n");
#endif
        
    }
    else
    {
        newImageBase = appBaseAddress;
        status = OTA_PROCEED_SUFFICIENT_STORAGE;
        OTA_UpdateStatus(status, 0);
#ifdef __linux__
        LOGI("OTA going to start, sufficient storage \r\n");
#elif __APPLE__
        fprintf(stdout,"OTA going to start, sufficient storage \r\n");
#endif
    }
    return status;
}

OTA_ReturnType OTA_StartUpgrade(void)
{
    fwfile = 0;
    size_t read_ret;
    OTA_ReturnType result;
    OTA_resetEverything();
    result = OTA_WriteNewImageInfo();
    
    if (result == OTA_RETURN_FAILURE)
    {
        // failure in writing new image info in btlNewImageChar, return
    }
    /* **********************
     * call app function to write into btlexpectedseqno descriptor
     *  and return success or failure
     *  result = AppFunction(what to write?);
     * ********************/
    
    if (appApiLinkerPtr == NULL){
        return OTA_RETURN_FAILURE;
    }
    result = appApiLinkerPtr->otaInitiateSeqNumberAPI();
    
    static unsigned char pcktBuffer[20];
    static unsigned char checksum = 0;
    unsigned char i;
    unsigned short temp;
    /* Then write first packet of the data, before writing we need to initiate the required variables */
    
    //    fwfile = fopen(newImageFilePath,"rb");
    //    if(fwfile != 0) //log added for testing
    //    {
    //    #ifdef __linux__
    //       LOGI("file successfully opened \r\n");
    //    #elif __APPLE__
    //        fprintf(stdout,"file successfully opened  \r\n");
    //    #endif
    //    }
    
    while(otaModeStatus == FWU_NOT_STARTED)
    {
        nWaitingForResponseCount++;
        if((nWaitingForResponseCount % 150) == 0)            // 7.5 sec wait
        {
#ifdef __linux__
            LOGI("DM hung, hence aborting \r\n");
#elif __APPLE__
            fprintf(stdout,"DM hung, hence aborting\r\n");
#endif
            OTA_UpdateStatus(OTA_TIMEOUT_ERROR_ABORT,0);
            otaModeStatus = FWU_ABORTED;
            result = OTA_RETURN_FAILURE;
        }
        //        else if((nWaitingForResponseCount % 120) == 0)
        //        {
        //        #ifdef __linux__
        //                LOGI("DM unresponsive, giving it last chance \r\n");
        //        #elif __APPLE__
        //                fprintf(stdout,"DM unresponsive, giving it last chance\r\n");
        //        #endif
        //            OTA_RequestStatus();
        //        }
        //        else if((nWaitingForResponseCount % 100) == 0)           // 3 sec wait
        //        {
        //        #ifdef __linux__
        //                LOGI("DM unresponsive, giving it first chance \r\n");
        //        #elif __APPLE__
        //                fprintf(stdout,"DM unresponsive, giving it first chance\r\n");
        //        #endif
        //OTA_RequestStatus();
        //        }
        usleep(50000);
    }
    sendnxtpckt = SET;
    nWaitingForResponseCount = 0;
    //    waitfornxtchunk = RESET;
    while(otaModeStatus == FWU_ONGOING)
    {
        //        while(!feof(fwfile))
        //        {
        //            while(waitfornxtchunk == RESET)                            // wait here until DM writes half page in flash and gives response accordingly
        //            {
        if(sendnxtpckt == SET)
        {
            /* **********************
             * adjust pos of the file acoording to seq number,prepare nxt seq pckt,calculate checksum, update sequence number
             * check if the current seq is end of chunk, update chunk number(opt), reset sendnxtpckt accordingly,send pckt
             * *******************/
            while(sendnxtpckt == SET)
            {
                if (lastChunk == RESET)
                {    if((newImageSize - totalBytesWritten)<=BUF_SIZE)
                    {
                        lastChunk = SET;
                        remainingBytes = lastChunkByteCount;
#ifdef __linux__
                        LOGI("This is last chunk \r\n");
#elif __APPLE__
                        fprintf(stdout,"This is last chunk \r\n");
#endif
                    }
                }
                if(lastChunk == SET)
                {
                    if(remainingBytes <= 0)
                    {
                        memset(pcktBuffer,0,sizeof(pcktBuffer));
                    }
                    else if(remainingBytes < PCKT_SIZE)
                    {
                        memset(pcktBuffer,0,sizeof(pcktBuffer));
                        
                        memcpy((pcktBuffer + 1), readFirmwareBufferPtr, PCKT_SIZE);
                        readFirmwareBufferPtr = readFirmwareBufferPtr + PCKT_SIZE;
                        //    dmImagefilepointerInfo.previousFilePointerPosition = 0; //Forcing previousFilePointerPosition to zero for last chunk which is less then PCKT_SIZE (16)
                        //    read_ret = fread(&(pcktBuffer[1]),remainingBytes,ELEMENTS_TO_READ,fwfile);
                        //    dmImagefilepointerInfo.currentFilePointerPosition = PCKT_SIZE; //forcing currentFilePointerPosition to PCKT_SIZE(16) for last chunk, so that it is send to DM after
                        // [(currentFilePointerPosition - previousFilePointerPosition) = PCKT_SIZE] check forced for last remaining bytes
                        // less than PCKT_SIZE
                        /*                         if(read_ret != ELEMENTS_TO_READ)
                         {
                         if(feof(fwfile))
                         {
                         #ifdef __linux__
                         LOGI("\r\n unexpected end of file occured");
                         #elif __APPLE__
                         fprintf(stdout, "\r\n unexpected end of file occured");
                         #endif
                         OTA_UpdateStatus(OTA_UNEXPECTED_END_OF_FILE_OCCURED,0);
                         OTA_ConveySesionAbortStatusToDm(); //telling Dm to abort current OTA session
                         otaModeStatus = FWU_ABORTED;
                         break;
                         }
                         else if(ferror(fwfile))
                         {
                         #ifdef __linux__
                         LOGI("\r\n error in reading file");
                         #elif __APPLE__
                         fprintf(stdout, "\r\n error in reading file");
                         #endif
                         OTA_UpdateStatus(OTA_ERROR_IN_READING_FILE,0);
                         OTA_ConveySesionAbortStatusToDm(); //telling Dm to abort current OTA session
                         otaModeStatus = FWU_ABORTED;
                         break;
                         }
                         } */
                        remainingBytes = 0;
                    }
                    else{
                        memset(pcktBuffer,0,sizeof(pcktBuffer));
                        //        dmImagefilepointerInfo.previousFilePointerPosition = dmImagefilepointerInfo.currentFilePointerPosition; //storing current file pointer in previousFilePointerPosition variable
                        
                        memcpy((pcktBuffer + 1), readFirmwareBufferPtr, PCKT_SIZE);
                        readFirmwareBufferPtr = readFirmwareBufferPtr + PCKT_SIZE;
                        //        read_ret = fread(&(pcktBuffer[1]),PCKT_SIZE,ELEMENTS_TO_READ,fwfile);
                        //        dmImagefilepointerInfo.currentFilePointerPosition = ftell(fwfile); //getting currentFilePointerPosition of dm app hex file
                        
                        /* if(read_ret != ELEMENTS_TO_READ)
                         {
                         if(feof(fwfile))
                         {
                         #ifdef __linux__
                         LOGI("\r\n unexpected end of file occured");
                         #elif __APPLE__
                         fprintf(stdout, "\r\n unexpected end of file occured");
                         #endif
                         OTA_UpdateStatus(OTA_UNEXPECTED_END_OF_FILE_OCCURED,0);
                         OTA_ConveySesionAbortStatusToDm(); //telling Dm to abort current OTA session
                         otaModeStatus = FWU_ABORTED;
                         break;
                         }
                         else if(ferror(fwfile))
                         {
                         #ifdef __linux__
                         LOGI("\r\n error in reading file");
                         #elif __APPLE__
                         fprintf(stdout, "\r\n error in reading file");
                         #endif
                         OTA_UpdateStatus(OTA_ERROR_IN_READING_FILE,0);
                         OTA_ConveySesionAbortStatusToDm(); //telling Dm to abort current OTA session
                         otaModeStatus = FWU_ABORTED;
                         break;
                         }
                         } */
                        remainingBytes -= PCKT_SIZE;
                    }
                }
                else{
                    memset(pcktBuffer,0,sizeof(pcktBuffer));
                    memcpy((pcktBuffer + 1), readFirmwareBufferPtr, PCKT_SIZE);
                    readFirmwareBufferPtr = readFirmwareBufferPtr + PCKT_SIZE;
                    //dmImagefilepointerInfo.previousFilePointerPosition = dmImagefilepointerInfo.currentFilePointerPosition; //storing current file pointer in previousFilePointerPosition variable
                    //read_ret = fread(&(pcktBuffer[1]),PCKT_SIZE,1,fwfile);
                    //dmImagefilepointerInfo.currentFilePointerPosition = ftell(fwfile); //getting currentFilePointerPosition of dm app hex file
                    
                    /*                     if(read_ret != ELEMENTS_TO_READ)
                     {
                     if(feof(fwfile))
                     {
                     #ifdef __linux__
                     LOGI("\r\n unexpected end of file occured");
                     #elif __APPLE__
                     fprintf(stdout, "\r\n unexpected end of file occured");
                     #endif
                     OTA_UpdateStatus(OTA_UNEXPECTED_END_OF_FILE_OCCURED,0);
                     OTA_ConveySesionAbortStatusToDm(); //telling Dm to abort current OTA session
                     otaModeStatus = FWU_ABORTED;
                     break;
                     }
                     else if(ferror(fwfile))
                     {
                     #ifdef __linux__
                     LOGI("\r\n error in reading file");
                     #elif __APPLE__
                     fprintf(stdout, "\r\n error in reading file");
                     #endif
                     OTA_UpdateStatus(OTA_ERROR_IN_READING_FILE,0);
                     OTA_ConveySesionAbortStatusToDm(); //telling Dm to abort current OTA session
                     otaModeStatus = FWU_ABORTED;
                     break;
                     }
                     } */
                }
                
                checksum = 0;
                pcktBuffer[17] = 0;
                temp = currentSequenceNumber;
                pcktBuffer[18] = (unsigned char)(temp & 0x00FF);
                temp >>= 8;
                pcktBuffer[19] = (unsigned char)(temp & 0x00FF);
                for(i=1;i<=16;i++)
                {
                    checksum ^= pcktBuffer[i];
                }
                checksum ^= (pcktBuffer[17] ^ pcktBuffer[18] ^ pcktBuffer[19]);
                pcktBuffer[0] = checksum;
                if(((currentSequenceNumber+1)%8) == 0)
                {
                    //waitfornxtchunk = SET;
                    sendnxtpckt = RESET;
                }
                /* **********************
                 * call app function to write into btlnewdatacontent char
                 *  and return success or failure
                 *  result = AppFunction(what to write?);
                 * ********************/
#ifdef __linux__
                LOGI("\r\nSequence number of the packet sent : %d  packet:", currentSequenceNumber);
#elif __APPLE__
                fprintf(stdout, "\r\nSequence number of the packet sent : %d  packet:", currentSequenceNumber);
#endif
                //                for (int i = 0; i<20; i++) {
                //                #ifdef __linux__
                //                LOGI("%x ", pcktBuffer[i]);
                //                #elif __APPLE__
                //                fprintf(stdout,"%x  ", pcktBuffer[i]);
                //                #endif
                //                }
                currentSequenceNumber++;
                OTA_WriteNewDataContent(pcktBuffer);
                
                
                
            }
            // start timer
            //usleep(50000);
            //            }
#ifdef __linux__
            LOGI("waiting for response from DM, current updated seq no:%d\r\n",currentSequenceNumber);
#elif __APPLE__
            fprintf(stdout,"waiting for response from DM  current updated seq no:%d\r\n",currentSequenceNumber);
#endif
            //       }
        }
        else{
            nWaitingForResponseCount++;
            if((nWaitingForResponseCount % 200) == 0)            // after 10 sec
            {
#ifdef __linux__
                LOGI("DM hung, hence aborting \r\n");
#elif __APPLE__
                fprintf(stdout,"DM hung, hence aborting\r\n");
#endif
                OTA_UpdateStatus(OTA_TIMEOUT_ERROR_ABORT,0);
                OTA_ConveySesionAbortStatusToDm(); //telling Dm to abort current OTA session
                otaModeStatus = FWU_ABORTED;
                result = OTA_RETURN_FAILURE;
            }
            else if((nWaitingForResponseCount % 140) == 0)            // after 7 sec
            {
#ifdef __linux__
                LOGI("DM unresponsive, giving it last chance \r\n");
#elif __APPLE__
                fprintf(stdout,"DM unresponsive, giving it last chance\r\n");
#endif
                OTA_RequestStatus();
            }
            else if(((nWaitingForResponseCount % 60) == 0) && (nWaitingForResponseCount<70))                // after 3 sec
            {
#ifdef __linux__
                LOGI("DM unresponsive, giving it first chance \r\n");
#elif __APPLE__
                fprintf(stdout,"DM unresponsive, giving it first chance\r\n");
#endif
                
                OTA_RequestStatus();
                
            }
            usleep(50000);
        }
    }
#ifdef __linux__
    LOGI("DM OTA Completed or Aborted successfully \r\n");
#elif __APPLE__
    fprintf(stdout,"DM OTA Completed or Aborted successfully \r\n");
#endif
    //  fclose(fwfile);
    appApiLinkerPtr = NULL;
    OTA_resetEverything();
    return result;
}

OTA_ReturnType OTA_WriteNewDataContent(unsigned char * dataBuffer)
{
    OTA_ReturnType result;
    /* **********************
     * call app function to send OTA data packet
     *  and return success or failure
     *  result = AppFunction(dataBuffer);
     *  *******************/
    if (appApiLinkerPtr == NULL) {
        return OTA_RETURN_FAILURE;
    }
    result = appApiLinkerPtr->otaWriteNewContentAPI(dataBuffer);
    if(result == OTA_RETURN_FAILURE)
    {
#ifdef __linux__
        LOGI("New data writing failed \r\n");
#elif __APPLE__
        fprintf(stdout,"New data writing failed \r\n");
#endif
    }
    return result;
}
void OTA_GetModeStatusCB(unsigned char * modestatus)
{
    //Reseting file pointer variables on ota upgrade start
    //dmImagefilepointerInfo.previousFilePointerPosition = 0;
    //dmImagefilepointerInfo.currentFilePointerPosition = 0;
    if((*modestatus) == OTA_MODE_SET)
    {
#ifdef __linux__
        LOGI("OTA mode set on DM \r\n");
#elif __APPLE__
        fprintf(stdout,"OTA mode set on DM \r\n");
#endif
        OTA_UpdateStatus(OTA_DM_FWU_MODE_SET, 0);
    }
}
void OTA_WriteResponseCB(unsigned char * response)
{
    unsigned short receivedSeqNum;
    //reset timer
    //write code to get response packet in notification OTA_responsePacketTypeDef variable
    OTA_responsePacketTypeDef * responsePacket;
    responsePacket = (OTA_responsePacketTypeDef *)response;
    //update sequence number,set sendnxtpckt flag
    //give app chunk success event
    receivedSeqNum = responsePacket->replyCounter;
    switch(responsePacket->statusCode)
    {
        
        case OTA_START_UPGRADE :
        {
            if(otaModeStatus == FWU_NOT_STARTED)
            {
#ifdef __linux__
                LOGI("Starting upgrade \r\n");
#elif __APPLE__
                fprintf(stdout,"starting upgrade \r\n");
#endif
                otaModeStatus = FWU_ONGOING;
                OTA_UpdateStatus(OTA_ONGOING, 0);
            }
        }
        break;
        //        case OTA_PCKT_RECEIVED :
        //        {
        //#ifdef __linux__
        //                    LOGI("pckt received by DM \r\n");
        //#elif __APPLE__
        //                    fprintf(stdout,"pckt received by DM \r\n");
        //#endif
        //            sendnxtpckt = SET;
        //        }
        //        break;
        case OTA_SUCCESS :
        {
            if(lastChunk == SET)
            {
                //OTA_UpdateStatus(OTA_COMPLETED,100);
#ifdef __linux__
                //  LOGI("Upgrade completed successfully \r\n");
#elif __APPLE__
                //  fprintf(stdout,"Upgrade completed successfully \r\n");
#endif
                //  otaModeStatus = FWU_COMPLETED;
            }
            else if(receivedSeqNum == currentSequenceNumber)
            {
                currentSequenceNumber = receivedSeqNum;
                //totalBytesWritten = totalBytesWritten + BUF_SIZE;
                totalBytesWritten = receivedSeqNum * PCKT_SIZE;
                float temp = (float)totalBytesWritten/fileSize;
                percentage =  temp * 100;
                OTA_UpdateStatus(OTA_CHUNK_SUCCESS, percentage);
                
                positionToSeek = currentSequenceNumber * PCKT_SIZE;
#ifdef __linux__
                LOGI("position to seek: %d,, received seq no:  %d \r\n",positionToSeek, receivedSeqNum);
#elif __APPLE__
                fprintf(stdout,"position to seek: %d,, received seq no:  %d \r\n",positionToSeek, receivedSeqNum);
#endif
                readFirmwareBufferPtr = startFirmwareBufferPtr + (positionToSeek);//startFirmwareBufferPtr pointer is fixed to initial position
                
                //  fseek(fwfile, positionToSeek, SEEK_SET);// SEEK_SET
                //  dmImagefilepointerInfo.currentFilePointerPosition = ftell(fwfile); //getting currentFilePointerPosition of dm app hex file to avoid error code 50(file reading error)
                
#ifdef __linux__
                LOGI("chunk written, current seq no: %d \r\n",currentSequenceNumber);
#elif __APPLE__
                fprintf(stdout,"Chunk written, current seq no: %d \r\n",currentSequenceNumber);
#endif
                repeatedAttemptsCount = 0;
                //waitfornxtchunk = RESET;
                nWaitingForResponseCount = 0;
                sendnxtpckt = SET;
            }
        }
        break;
        case OTA_FLASH_VERIFY_ERROR :
        {
            OTA_UpdateStatus(OTA_DM_FLASH_ERROR,0);
            OTA_ConveySesionAbortStatusToDm(); //telling Dm to abort current OTA session
            otaModeStatus = FWU_ABORTED;
        }
        break;
        case OTA_FLASH_WRITE_ERROR :
        {
            OTA_UpdateStatus(OTA_DM_FLASH_ERROR,0);
            OTA_ConveySesionAbortStatusToDm(); //telling Dm to abort current OTA session
            otaModeStatus = FWU_ABORTED;
        }
        break;
        case OTA_IMAGE_CHECKSUM_ERROR :
        {
            OTA_UpdateStatus(OTA_DM_FULL_IMAGE_CHECKSUM_ERROR,0);
            otaModeStatus = FWU_ABORTED;
        }
        break;
        case OTA_UPDATE_DONE_SUCCESSFULLY :
        {
            OTA_UpdateStatus(OTA_COMPLETED,100);
#ifdef __linux__
            LOGI("Upgrade completed successfully \r\n");
#elif __APPLE__
            fprintf(stdout,"Upgrade completed successfully \r\n");
#endif
            otaModeStatus = FWU_COMPLETED;
        }
        break;
        case OTA_SEQUENCE_ERROR :
        {
            repeatedAttemptsCount++;
            if(repeatedAttemptsCount > MAX_REPEATED_ATTEMPTS_COUNT)
            {
                OTA_UpdateStatus(OTA_EXCEEDED_REPEATED_ATTEMPT_COUNT,0);
                OTA_ConveySesionAbortStatusToDm(); //telling Dm to abort current OTA session
                otaModeStatus = FWU_ABORTED;
                break;
            }
            if(lastChunk == SET)
            {
                remainingBytes = lastChunkByteCount;
            }
            currentSequenceNumber = receivedSeqNum;
            positionToSeek = currentSequenceNumber * PCKT_SIZE;
            
            readFirmwareBufferPtr = startFirmwareBufferPtr + (positionToSeek); //startFirmwareBufferPtr pointer is fixed to initial position
            
            //fseek(fwfile, positionToSeek, SEEK_SET);// SEEK_SET
            //dmImagefilepointerInfo.currentFilePointerPosition = ftell(fwfile); //getting currentFilePointerPosition of dm app hex file to avoid error code 50(file reading error)
            OTA_UpdateStatus(OTA_CHUNK_FAILURE,0);
#ifdef __linux__
            LOGI(" sequence error ,,position to seek: %d,current seq no.: %d, received seq no:  %d \r\n",positionToSeek,currentSequenceNumber, receivedSeqNum);
#elif __APPLE__
            fprintf(stdout," sequence error ,,position to seek: %d,current seq no.: %d, received seq no:  %d \r\n",positionToSeek,currentSequenceNumber, receivedSeqNum);
#endif
            //waitfornxtchunk = RESET;
            nWaitingForResponseCount = 0;
            sendnxtpckt = SET;
        }
        break;
        case OTA_CHECKSUM_ERROR :
        {
            repeatedAttemptsCount++;
            if(repeatedAttemptsCount > MAX_REPEATED_ATTEMPTS_COUNT)
            {
                OTA_UpdateStatus(OTA_EXCEEDED_REPEATED_ATTEMPT_COUNT,0);
                OTA_ConveySesionAbortStatusToDm(); //telling Dm to abort current OTA session
                otaModeStatus = FWU_ABORTED;
                break;
            }
            if(lastChunk == SET)
            {
                remainingBytes = lastChunkByteCount;
            }
            currentSequenceNumber = receivedSeqNum;
            positionToSeek = currentSequenceNumber * PCKT_SIZE;
            
            readFirmwareBufferPtr = startFirmwareBufferPtr + (positionToSeek); //startFirmwareBufferPtr pointer is fixed to initial position
            //     fseek(fwfile, positionToSeek, SEEK_SET);// SEEK_SET
            //        dmImagefilepointerInfo.currentFilePointerPosition = ftell(fwfile); //getting currentFilePointerPosition of dm app hex file to avoid error code 50(file reading error)
            OTA_UpdateStatus(OTA_CHUNK_FAILURE,0);
#ifdef __linux__
            LOGI(" checksum error ,,position to seek: %d,current seq no.: %d, received seq no:  %d \r\n",positionToSeek,currentSequenceNumber, receivedSeqNum);
#elif __APPLE__
            fprintf(stdout," checksum error ,,position to seek: %d,current seq no.: %d, received seq no:  %d \r\n",positionToSeek,currentSequenceNumber, receivedSeqNum);
#endif
            // waitfornxtchunk = RESET;
            nWaitingForResponseCount = 0;
            sendnxtpckt = SET;
        }
        break;
    }
}

OTA_ReturnType OTA_UpdateStatus(OTA_Status status, float percentage)
{
    OTA_ReturnType result;
    /* **********************
     * call app function to update current OTA status
     *  and return success or failure
     *  result = AppFunction(Status);
     *  *******************/
    
    if (appApiLinkerPtr == NULL) {
        return OTA_RETURN_FAILURE;
    }
    
    result = appApiLinkerPtr->otaUpdateStatusCB(status, percentage);
    if(result == OTA_RETURN_FAILURE)
    {
#ifdef __linux__
        LOGI("status updation failed \r\n");
#elif __APPLE__
        fprintf(stdout,"status updation failed \r\n");
#endif
    }
    return result;
}

OTA_ReturnType OTA_ReadCurrentImageInfo (void)
{
    OTA_ReturnType result;
    /* **********************
     * call app function to read from btlImageChar char
     *  and return success or failure
     *  result = AppFunction(currentImageInfo);
     *  *******************/
    if (appApiLinkerPtr == NULL) {
        return OTA_RETURN_FAILURE;
    }
    //    result = appApiLinkerPtr->otaGetCurrentImageInfoAPI(currentImageInfo);
    ptrCurrentImageInfo = malloc(sizeof(currentImageInfo));
    result = appApiLinkerPtr->otaGetCurrentImageInfoAPI(ptrCurrentImageInfo);
    if(result == OTA_RETURN_FAILURE)
    {
#ifdef __linux__
        LOGI("reading image info failed \r\n");
#elif __APPLE__
        fprintf(stdout,"reading image info failed \r\n");
#endif
    }
    memcpy(currentImageInfo, ptrCurrentImageInfo, sizeof(currentImageInfo));
    free(ptrCurrentImageInfo);
    return result;
}

OTA_ReturnType OTA_WriteNewImageInfo ()
{
    OTA_ReturnType result;
    unsigned char i;
    unsigned int temp;
    temp = newCorrectedImageSize;
    newImageInfo[0] = NOTIFICATION_WINDOW;
    for (i=1;i<5;i++)
    {
        newImageInfo[i] = ((unsigned char)temp & 0xFF);
        temp>>=8;
    }
    temp = newImageBase;
    for (i=5;i<9;i++)
    {
        newImageInfo[i] = ((unsigned char)temp & 0xFF);
        temp>>=8;
    }
    
    /* **********************
     * call app function to write to btlNewImageChar char
     *  and return success or failure
     *  result = AppFunction(newImageInfo);
     *  *******************/
    //    result = appApiLinkerPtr->otaWriteNewImageAPI(newImageInfo);
    //    ptrNewImageInfo = malloc(sizeof(newImageInfo));
    //    ptrNewImageInfo = newImageInfo;
    if (appApiLinkerPtr == NULL) {
        return OTA_RETURN_FAILURE;
    }
    result = appApiLinkerPtr->otaWriteNewImageAPI(newImageInfo);
    if(result == OTA_RETURN_FAILURE)
    {
#ifdef __linux__
        LOGI("new image writing failed \r\n");
#elif __APPLE__
        fprintf(stdout,"New image writing failed \r\n");
#endif
    }
    //    memset(newImageInfo, ptrNewImageInfo, sizeof(newImageInfo));
    //    free(ptrNewImageInfo);
    return result;
}

OTA_ReturnType OTA_RequestStatus()
{
    OTA_ReturnType result;
    /* **********************
     * call app function to write to otastatusmode char
     *  and return success or failure
     *  result = AppFunction(newImageInfo);
     *  *******************/
    if (appApiLinkerPtr == NULL) {
        return OTA_RETURN_FAILURE;
    }
    result = appApiLinkerPtr->otaUpdateRequest(&sendLastStatus);
    if(result == OTA_RETURN_FAILURE)
    {
#ifdef __linux__
        LOGI(" status refresh failed  \r\n");
#elif __APPLE__
        fprintf(stdout,"status referesh failed \r\n");
#endif
    }
    return result;
}

OTA_ReturnType OTA_ConveySesionAbortStatusToDm()
{
    OTA_ReturnType result;
    /* **********************
     * call app function to write to otastatusmode char
     *  and return success or failure
     *  result = AppFunction(newImageInfo);
     *  *******************/
    if (appApiLinkerPtr == NULL) {
        return OTA_RETURN_FAILURE;
    }
    result = appApiLinkerPtr->otaUpdateRequest(&otaSessionAbort);
    if(result == OTA_RETURN_FAILURE)
    {
#ifdef __linux__
        LOGI("Sending abort status to Dm failed\r\n");
#elif __APPLE__
        fprintf(stdout,"Sending abort status to Dm failed\r\n");
#endif
    }
    return result;
}

void OTA_CalculateAvailableSpace()
{
    unsigned int temp;
    unsigned char i;
    appBaseAddress = (int)(currentImageInfo[0]<<24) + (int) (currentImageInfo[1]<<16) + (int)(currentImageInfo[2]<<8) + (int)(currentImageInfo[3]);
    appEndAddress = (int)(currentImageInfo[4]<<24) + (int) (currentImageInfo[5]<<16) + (int)(currentImageInfo[6]<<8) + (int)(currentImageInfo[7]);
    //    dataBaseAddress = (int)(currentImageInfo[8]<<24) + (int) (currentImageInfo[9]<<16) + (int)(currentImageInfo[10]<<8) + (int)(currentImageInfo[11]);
    //    dataEndAddress = (int)(currentImageInfo[12]<<24) + (int) (currentImageInfo[13]<<16) + (int)(currentImageInfo[14]<<8) + (int)(currentImageInfo[15]);
    
    if ((appBaseAddress != 0)&&(appEndAddress !=0))
    {
        appBaseAddress = DM_DEFAULT_BASEADDRS; // Temp 
        appEndAddress = DM_DEFAULT_ENDADDRS;
        availableAppSize = appEndAddress - appBaseAddress;
    }
    else{
        //OTA_UpdateStatus(OTA_ERROR_IN_READING_IMAGE_INFO,0);
        appBaseAddress = DM_DEFAULT_BASEADDRS;
        appEndAddress = DM_DEFAULT_ENDADDRS;
        availableAppSize = appEndAddress - appBaseAddress;
    }
}

void OTA_CorrectNewImageSize()
{
    unsigned int temp;
    temp = newImageSize / BUF_SIZE;
    lastChunkByteCount = newImageSize % BUF_SIZE;
    if(lastChunkByteCount == 0)
    {
        newCorrectedImageSize = temp* BUF_SIZE;
        lastChunkByteCount = 128;
    }
    else
    {
        newCorrectedImageSize = (temp+1)* BUF_SIZE;
    }
    sizeOffset = newCorrectedImageSize - newImageSize;
}

void OTA_NewSession(struct OTA_APP_API_CB * otaAPILinker)
{
    appApiLinkerPtr = otaAPILinker;
}

void OTA_AbortUpgrade()
{
    free(fullFirmwareBufferPtr);
	fprintf(stdout,"Freeing dynamically allocated memory of fullFirmwareBufferPtr\r\n");
	fullFirmwareBufferPtr = NULL;
	readFirmwareBufferPtr = NULL;
	startFirmwareBufferPtr = NULL;
    appApiLinkerPtr = NULL;
    otaModeStatus = FWU_ABORTED;
    
}

void OTA_resetEverything()
{
    lastChunk = RESET;
    sendnxtpckt = RESET;
    otaModeStatus = FWU_NOT_STARTED;
    currentSequenceNumber = 0;
    repeatedAttemptsCount = 0;
    nWaitingForResponseCount = 0;
    totalBytesWritten = 0;
}




//****************************************************************************

uint32_t Crc32(uint32_t Crc, uint32_t Data)
{
    int i;
    
    Crc = Crc ^ Data;
    
    for(i=0; i<32; i++)
    if (Crc & 0x80000000)
    Crc = (Crc << 1) ^ 0x04C11DB7; // Polynomial used in STM32
    else
    Crc = (Crc << 1);
    
    return(Crc);
}

//****************************************************************************

uint32_t Crc32Block(uint32_t Crc, uint32_t Size, uint32_t *Buffer) // 32-bit units
{
    while(Size--)
    Crc = Crc32(Crc, *Buffer++);
    
    return(Crc);
}

//****************************************************************************

uint32_t Crc32Fast(uint32_t Crc, uint32_t Data)
{
    static const uint32_t CrcTable[16] = { // Nibble lookup table for 0x04C11DB7 polynomial
        0x00000000,0x04C11DB7,0x09823B6E,0x0D4326D9,0x130476DC,0x17C56B6B,0x1A864DB2,0x1E475005,
        0x2608EDB8,0x22C9F00F,0x2F8AD6D6,0x2B4BCB61,0x350C9B64,0x31CD86D3,0x3C8EA00A,0x384FBDBD };
    
    Crc = Crc ^ Data; // Apply all 32-bits
    
    // Process 32-bits, 4 at a time, or 8 rounds
    
    Crc = (Crc << 4) ^ CrcTable[Crc >> 28]; // Assumes 32-bit reg, masking index to 4-bits
    Crc = (Crc << 4) ^ CrcTable[Crc >> 28]; //  0x04C11DB7 Polynomial used in STM32
    Crc = (Crc << 4) ^ CrcTable[Crc >> 28];
    Crc = (Crc << 4) ^ CrcTable[Crc >> 28];
    Crc = (Crc << 4) ^ CrcTable[Crc >> 28];
    Crc = (Crc << 4) ^ CrcTable[Crc >> 28];
    Crc = (Crc << 4) ^ CrcTable[Crc >> 28];
    Crc = (Crc << 4) ^ CrcTable[Crc >> 28];
    
    return(Crc);
}

//****************************************************************************

uint32_t Crc32FastBlock(uint32_t Crc, uint32_t Size, uint32_t *Buffer) // 32-bit units
{
    while(Size--)
    Crc = Crc32Fast(Crc, *Buffer++);
    
    return(Crc);
}

//****************************************************************************

/************************ (C) COPYRIGHT KPIT Technologies *****END OF FILE****/

