//
//  Dictionary+Cache.swift
//  Maximus
//
//  Created by Nilesh shinde on 21/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.

import Foundation

extension Dictionary {
    func keysSortedByValue(_ isOrderedBefore: (Value, Value) -> Bool) -> [Key] {
        return Array(self).sorted{ isOrderedBefore($0.1, $1.1) }.map{ $0.0 }
    }
}
