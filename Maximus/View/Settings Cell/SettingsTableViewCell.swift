//
//  SettingsTableViewCell.swift
//  SettingsXIB
//
//  Created by Isha Ramdasi on 20/09/18.
//  Copyright © 2018 Isha Ramdasi. All rights reserved.
//

import UIKit

//protocol for sending button click events from TableViewCell to TableView
protocol SettingsTableViewCellDelegate : class {
    func unPairButtonTapped(cell: SettingsTableViewCell, button: UIButton)
    func connectionStatusButtonTapped(cell:SettingsTableViewCell, button: UIButton)
    func cellButtonTapped(cell:SettingsTableViewCell, button: UIButton)
}

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var unPairButton: UIButton!
    @IBOutlet weak var connectionStatusButton: UIButton!
    @IBOutlet weak var cellButtonTap: UIButton!
    
    weak var delegate: SettingsTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.isExclusiveTouch = true
        cellButtonTap.isExclusiveTouch = true
        self.backgroundColor = AppTheme.THEME_COLOR
        // Initialization code
    }
    
    //unpair Button Click event
    @IBAction func unPairButtonTapped(_ sender: Any) {
        self.delegate?.unPairButtonTapped(cell: self, button: unPairButton)
    }
    
    //Connection Status Button Click event for adding DM
    @IBAction func connectionStatusButtonTapped(_ sender: Any) {
        self.delegate?.connectionStatusButtonTapped(cell: self, button: connectionStatusButton)
    }
    
    @IBAction func cellButtonTapped(_ sender: Any) {
        self.delegate?.cellButtonTapped(cell: self, button: cellButtonTap)
    }
}
