//
//  PlanRideRouteCell.swift
//  Maximus
//
//  Created by Namdev Jagtap on 27/06/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class PlanRideRouteCell: UICollectionViewCell {

    @IBOutlet weak var imgViewPOI: UIImageView!
    @IBOutlet weak var lblSelectPOI: UILabel!
    @IBOutlet weak var selectedPOIView: UIView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
