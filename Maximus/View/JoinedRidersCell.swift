//
//  JoinedRidersCell.swift
//  Maximus
//
//  Created by Abhilash G on 7/11/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class JoinedRidersCell: UITableViewCell {

    @IBOutlet weak var lblRidersName: UILabel!
    @IBOutlet weak var lblRideAdmin: UILabel!
    
    var joinedRider:RideMemberDetails? {
        didSet{
            lblRidersName.text = joinedRider?.name
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.backgroundColor = AppTheme.THEME_COLOR
        //self.lblRideAdmin.backgroundColor = AppTheme.NAVBAR_COLOR
        self.isUserInteractionEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
