//
//  TutorialsCollectionViewCell.swift
//  Maximus
//
//  Created by kpit on 08/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class TutorialsCollectionViewCell: UICollectionViewCell {

    @IBOutlet var imageVw: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
