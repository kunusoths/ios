//
//  CustomInfoWindowView.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 22/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class CustomInfoWindowView: UIView {

    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
