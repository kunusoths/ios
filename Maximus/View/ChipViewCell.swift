//
//  ChipViewCell.swift
//  Maximus
//
//  Created by kpit on 23/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class ChipViewCell: UITableViewCell {

    @IBOutlet weak var entityLabel: UILabel!
    @IBOutlet weak var chipView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
