//
//  PopOver.swift
//  Maximus
//
//  Created by Namdev Jagtap on 21/04/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class PopOver: UIView {
    
    @IBOutlet var lblRideTitle: UILabel!
    @IBOutlet var lblInvitedBy: UILabel!
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var lblWayPoints: UILabel!
    @IBOutlet weak var lblRideFromAndTo: UILabel!
    @IBOutlet weak var lblRideDate:UILabel!
    let rideRepo = RideRepository()
    let planRideRepo = PlanRideRepository()
    var rideViewModel: MyRidesViewModel?
    var planRideModel: PlanRideViewModel? = nil
    
    var rideID = 0
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    class func instanceFromNib() -> PopOver {
        
        return UINib(nibName: "PopOver", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PopOver
    }

    override func draw(_ rect: CGRect) {
        // Drawing code
        rideViewModel = MyRidesViewModel(rideRepoDelegate: rideRepo)
        planRideModel = PlanRideViewModel(planRideRepoDelegate: planRideRepo)
        rideViewModel?.reloadDataDelegate = self

        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.height/2
        self.imgProfile.clipsToBounds = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(PopOver.hidePopOver))
        tapGesture.cancelsTouchesInView = true
        self.viewWithTag(1)?.addGestureRecognizer(tapGesture)
   
    }
    
    func hidePopOver()
    {
        isHidden = true
        self.userInteractionEnabled(isUserInteraction: true)
    }
    
    var objRideCreate:RideCreateResponse? {
            didSet {

                if let id = objRideCreate?.id {
                    rideID = id
                }
                
                if let title = objRideCreate?.title {
                    lblRideTitle.text = title
                }
                var bikerName = "You are invited by "
                if let firstName = objRideCreate?.biker?.first_name{
                    bikerName += firstName
                }
                
                if let lastName = objRideCreate?.biker?.last_name{
                    bikerName += " " + lastName
                }
                self.lblInvitedBy.text = bikerName
                
                if let profileUrl = objRideCreate?.admin_profile_img {
                    print(profileUrl)
                    imgProfile.imageWithUrl(url: profileUrl)
                }
                if let bannerUrl = objRideCreate?.background_imag {
                    imgBanner.imageWithUrl(url: bannerUrl)
                }
                
                //Slice Name and get City Name from it
                var startCityName:String = ""
                if let strName = objRideCreate?.start_location?.name {
                    let arrWords = strName.components(separatedBy: ",")
                    startCityName = arrWords[0]
                }
                //Slice Name and get City Name from it
                var endCityName:String = ""
                if let strName = objRideCreate?.end_location?.name {
                    let arrWords = strName.components(separatedBy: ",")
                    endCityName = arrWords[0]
                }
                var strStartDate:String = ""
                if let dateString:String = objRideCreate?.start_date {
                    let date = dateString.getFormatedDate()
                    strStartDate = self.getStringDate(strDateFormat: "dd MMM", date: date)
                }
                
                var strEndDate:String = ""
                if let dateString:String = objRideCreate?.finish_date {
                    let date = dateString.getFormatedDate()
                    strEndDate = self.getStringDate(strDateFormat: "dd MMM yyyy", date: date)
                }
                
                let strStartToEnd = startCityName+" to "+endCityName
                let strStartDateToEndDate = strStartDate+" to "+strEndDate
                
                lblRideFromAndTo.text = strStartToEnd
                lblRideDate.text = strStartDateToEndDate
                print("\(strStartToEnd)\(strStartDate)\(strEndDate)")
            }
    }

    
    @IBAction func btnDiscard(_ sender: Any) {
        self.makeToastActivity(.center, isUserInteraction: false)

        isHidden = true
        self.userInteractionEnabled(isUserInteraction: true)
        self.hideToastActivity()
//        let rideManager = RideManager()
//        rideManager.rideManagerDelegate = self
//        let json: [String:Any] = ["method":JoinRideTypes.JOIN_SHARE_LINK.rawValue,
//                                  "link":MyDefaults.getData(key: DefaultKeys.KEY_DEEPLINK) as! String]
//        rideViewModel.reloadDataDelegate = self
//        rideViewModel?.rejectRide(rideID: rideID, parameter: json, requestType: "DiscardRide", successCallback: {responseData in
//            self.reloadData(data: responseData)
//        }, failureCallback:{ failureData in
//            self.failure(data: failureData)
//        })
        
    }
    
    @IBAction func btnJoinRide(_ sender: Any) {
        
        self.makeToastActivity(.center, isUserInteraction: false)
        let rideManager = RideManager()
        rideManager.rideManagerDelegate = self
        let json: [String:Any] = ["method":JoinRideTypes.JOIN_SHARE_LINK.rawValue,
                                  "link":MyDefaults.getData(key: DefaultKeys.KEY_DEEPLINK) as! String]
        rideViewModel?.joinRide(rideID: rideID, parameter: json, requestType: "JoinRide", successCallback: {responseData in
            self.reloadData(data: responseData)
        }, failureCallback:{ failureData in
            self.failure(data: failureData)
        })
        
    }
    
    @IBAction func btnViewRoute(_ sender: Any) {
        
//        isHidden = true
//        self.userInteractionEnabled(isUserInteraction: true)
//        let vc = ShareRideDetail()
//        vc.rideDetails = objRideCreate
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let navigationController = UINavigationController(rootViewController: vc)
//        let mainViewController = navigationController
//        let leftViewController = SideMenuVC()
//        let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
//        slideMenuController.changeLeftViewWidth((appDelegate.window?.size.width)!-60)
//        appDelegate.window?.rootViewController = navigationController
    }
    
    @IBOutlet weak var imgCancel: UIImageView!
    func getStringDate(strDateFormat:String ,date:Date)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
      //  dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = strDateFormat
        return dateFormatter.string(from: date)
    }
    
    func userInteractionEnabled (isUserInteraction:Bool)  {
       
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topController.view.isUserInteractionEnabled = isUserInteraction
            // topController should now be your topmost view controller
        }
    }
}

extension PopOver:RideManagerDelegate, ReloadMyRidesDelegate{
    
    // MARK: - API Delegate Methods
    func onGetRideDetail(data:[String:Any]) {
        if let rideData:RideCreateResponse = data["data"] as? RideCreateResponse {
            print(rideData.biker?.first_name ??  "name")
            isHidden = true
            self.userInteractionEnabled(isUserInteraction: true)
            let vc = HomeContainerVC()
            vc.previousVc = StringConstant.rideDetailsVc
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let navigationController = UINavigationController(rootViewController: vc)
            let mainViewController = navigationController
            let leftViewController = SideMenuVC()
            let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
            slideMenuController.changeLeftViewWidth((appDelegate.window?.size.width)!-60)
            appDelegate.window?.rootViewController = slideMenuController
        }
    }
    
    func onRideStatus(data:[String:Any]) {
        self.hideToastActivity()
        if let rideData:RideStartResponse = data["data"] as? RideStartResponse {
            print(rideData.status!)
            isHidden = true
            self.userInteractionEnabled(isUserInteraction: true)
            let vc = HomeContainerVC()
            vc.previousVc = StringConstant.rideDetailsVc
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let navigationController = UINavigationController(rootViewController: vc)
            let mainViewController = navigationController
            let leftViewController = SideMenuVC()
            let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
            slideMenuController.changeLeftViewWidth((appDelegate.window?.size.width)!-60)
            appDelegate.window?.rootViewController = slideMenuController
        }
    }
    
    func onRideDiscardStatus(data:[String:Any]) {
        self.hideToastActivity()
        if let rideData:RideStartResponse = data["data"] as? RideStartResponse {
            print(rideData.status!)
            isHidden = true
            self.userInteractionEnabled(isUserInteraction: true)
           
        }
    }
    
    func onFailure(Error:[String:Any]){
        print("PopOver:\(Error)")
        self.hideToastActivity()
        let errModel:ErrorModel = Error["data"] as! ErrorModel
        print(errModel.message ?? " ")
        self.userInteractionEnabled(isUserInteraction: true)
        self.window?.rootView().makeToast((errModel.description)!)
        isHidden = true
    }
    
    func reloadData(data: Any) {
//        rideViewModel.reloadDataDelegate = self
        if  let dict = data as? [String: Any] , let request = dict["RequestType"] as? String, request == "JoinRide" {
            rideViewModel?.editRide(rideID: String(rideID), type: "GetRideDetails", successCallback: {responseData in
                self.reloadData(data: responseData)
            }, failureCallback:{ failureData in
                self.failure(data: failureData)
            })
        } else if  let dict = data as? [String: Any] , let request = dict["RequestType"] as? String, request == "DiscardRide",let _ = dict["data"] as? RideStartResponse {
            self.hideToastActivity()
            isHidden = true
            self.userInteractionEnabled(isUserInteraction: true)
        } else if  let dict = data as? [String: Any] , let request = dict["RequestType"] as? String, request == "GetRideDetails",let objRidedata = dict["data"] as? RideCreateResponse {
            planRideModel?.getRideRouteById(rideID: (objRidedata.id)!, bikerID: (objRidedata.admin_biker_id)!, type: "GetRideRouteById", successCallback: { (responseData) in
                self.saveRideSuccess(data: responseData)
            }) { (error) in
                self.failure(data: error)
            }
        } else if let _ = data as? [RideMemberDetails]{
            self.hideToastActivity()
            rideJoinedSuccess()
        }
    }
    
    func failure(data: [String : Any]) {
        self.hideToastActivity()
        self.userInteractionEnabled(isUserInteraction: true)
        isHidden = true
        if let errModel:ErrorModel = data["data"] as? ErrorModel {
            print(errModel.message ?? " ")
            self.userInteractionEnabled(isUserInteraction: true)
            self.window?.rootView().makeToast((errModel.description)!)
        } else if let errModel:[String: Any] = data["data"] as? [String: Any] {
            self.userInteractionEnabled(isUserInteraction: true)
            self.window?.rootView().makeToast((errModel["description"] as! String))
        }else {
            self.userInteractionEnabled(isUserInteraction: true)
            self.window?.rootView().makeToast((data["description"] as? String)!)
        }
    }
    
    func saveRideSuccess(data: Any) {
        if  let dict = data as? [String: Any], let request = dict["RequestType"] as? String, request == "GetRideRouteById"{
            let rideCard = dict["data"] as? SingleRouteDetails
//            rideViewModel.reloadDataDelegate = self
            rideViewModel?.getRideMembers(rideID: (rideCard?.id)!, type: "GetRideMembers", successCallback: {responseData in
                self.reloadData(data: responseData)
            }, failureCallback:{ failureData in
                self.failure(data: failureData)
            })
        }
    }
    
    func rideJoinedSuccess() {
        isHidden = true
        self.userInteractionEnabled(isUserInteraction: true)
        let vc = HomeContainerVC()
        vc.previousVc = StringConstant.rideDetailsVc
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let navigationController = UINavigationController(rootViewController: vc)
        let mainViewController = navigationController
        let leftViewController = SideMenuVC()
        let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
        slideMenuController.changeLeftViewWidth((appDelegate.window?.size.width)!-60)
        appDelegate.window?.rootViewController = slideMenuController
    }
}


