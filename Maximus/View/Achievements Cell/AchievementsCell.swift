//
//  AchievementsCell.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 01/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class AchievementsCell: UITableViewCell {
    
    @IBOutlet weak var imgVwBadge: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
