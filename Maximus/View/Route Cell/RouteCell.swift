//
//  RouteCell.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 11/04/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class RouteCell: UICollectionViewCell {

    @IBOutlet weak var btnRouteAction: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btnRouteAction.backgroundColor = .white	//AppTheme.NAVBAR_COLOR
        self.btnRouteAction.setTitleColor(.darkGray, for: .normal)
        // Initialization code
        self.btnRouteAction.titleLabel?.minimumScaleFactor = 0.5
        self.btnRouteAction.titleLabel?.numberOfLines = 1
        self.btnRouteAction.titleLabel?.adjustsFontSizeToFitWidth = true
        self.accessibilityIdentifier = "cltn_planride_routes"
    }

}
