//
//  RideLogCell.swift
//  Maximus
//
//  Created by Admin on 08/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import CoreLocation

class RideLogDetails: NSObject {
    var rideTitle:String?
    var rideStartData: String?
    var rideTime:Int?
    var rideDistance: Double?
    var rideTopSpeed: Double?
    var averageSpeed:Double?
    var rideLocations:[CLLocation]?
    var rideFinishedDate: Date?
}

class RideLogCell: UITableViewCell {

    @IBOutlet weak var lblRideName: UILabel!
    @IBOutlet weak var lblRideDate: UILabel!
    @IBOutlet weak var lblRideTime: UILabel!
    @IBOutlet weak var lblRideKm: UILabel!
    @IBOutlet weak var lblRideTopSpeed: UILabel!
    @IBOutlet weak var imgViewsticker_2: UIImageView!
    @IBOutlet weak var imgViewsticker_1: UIImageView!
    @IBOutlet weak var imgViewsticker_3: UIImageView!
    
    var objlog:RideLogDetails? {
        didSet {
            var distance = String()
            if let dist = objlog?.rideDistance{
                distance = String(format:"%.1f",Double(dist)/Double(1000))
            }else{
                distance = "0.0"
            }
            self.lblRideKm.text = "\(distance) kms"
            
            if let dateString:String = objlog?.rideStartData {
                self.lblRideDate.text = dateString.getFormatedDateString(format: "dd MMM YYYY")
            }else{
                self.lblRideDate.text = "No Date"
            }
            
            self.lblRideName.text = objlog?.rideTitle
            var time:Int = 0
            if let duration = objlog?.rideTime {
                time = Int(duration)
            }
            let (hours,minutes,_):(Int,Int,Int) = time.convertSecondsToHours()
            self.lblRideTime.text = "\(hours) hr \(minutes) min"
            
            var topSpeed = String()
            if let top = objlog?.rideTopSpeed{
                // cm/sec to km/hr
                topSpeed = String(format:"%.0f", (Double(top)*Double(3.6)).rounded())
            }else{
                topSpeed = "00"
            }
            self.lblRideTopSpeed.text = "Top Speed \(topSpeed) kmph"
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
