//
//  ContactUsCollectionViewCell.swift
//  Maximus
//
//  Created by Isha Ramdasi on 17/05/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit

class ContactUsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var contactUsButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        contactUsButton.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }

}
