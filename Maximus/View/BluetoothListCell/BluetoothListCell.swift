//
//  BluetoothListCell.swift
//  Maximus
//
//  Created by Sriram G on 02/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class BluetoothListCell: UITableViewCell {

    
    //@IBOutlet var deviceStatus: UILabel!
    @IBOutlet var deviceName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
