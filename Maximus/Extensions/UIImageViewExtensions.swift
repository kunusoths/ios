//
//  UIImageViewExtensions.swift

import UIKit


extension UIImageView {
    ///
    public convenience init(x: CGFloat, y: CGFloat, w: CGFloat, h: CGFloat, imageName: String) {
        self.init(frame: CGRect(x: x, y: y, width: w, height: h))
        image = UIImage(named: imageName)
    }
    
    ///
    public convenience init(x: CGFloat, y: CGFloat, imageName: String, scaleToWidth: CGFloat) {
        self.init(frame: CGRect(x: x, y: y, width: 0, height: 0))
        image = UIImage(named: imageName)
        if image != nil {
            scaleImageFrameToWidth(width: scaleToWidth)
        } else {
            assertionFailure("EZSwiftExtensions Error: The imageName: '\(imageName)' is invalid!!!")
        }
    }
    
    ///
    public convenience init(x: CGFloat, y: CGFloat, w: CGFloat, h: CGFloat, image: UIImage) {
        self.init(frame: CGRect(x: x, y: y, width: w, height: h))
        self.image = image
    }
    
    ///
    public convenience init(x: CGFloat, y: CGFloat, image: UIImage, scaleToWidth: CGFloat) {
        self.init(frame: CGRect(x: x, y: y, width: 0, height: 0))
        self.image = image
        scaleImageFrameToWidth(width: scaleToWidth)
    }
    
    /// , scales this ImageView size to fit the given width
    public func scaleImageFrameToWidth(width: CGFloat) {
        guard let image = image else {
            print(" Error: The image is not set yet!")
            return
        }
        let widthRatio = image.size.width / width
        let newWidth = image.size.width / widthRatio
        let newHeigth = image.size.height / widthRatio
        frame = CGRect(x: frame.origin.x, y: frame.origin.y, width: newWidth, height: newHeigth)
    }
    
    /// , scales this ImageView size to fit the given height
    public func scaleImageFrameToHeight(height: CGFloat) {
        guard let image = image else {
            print(" Error: The image is not set yet!")
            return
        }
        let heightRatio = image.size.height / height
        let newHeight = image.size.height / heightRatio
        let newWidth = image.size.width / heightRatio
        frame = CGRect(x: frame.origin.x, y: frame.origin.y, width: newWidth, height: newHeight)
    }
    
    ///
    public func roundSquareImage() {
        self.clipsToBounds = true
        self.layer.cornerRadius = self.frame.size.width / 2
    }
    
    ///
    
    public func imageWithUrl(url: String) {
        
        ez.requestImage(url, success: { (image) -> Void in
            if let img = image {
                DispatchQueue.main.async {
                    self.image = img
                }
            }
        })
    }
    
    ///
    public func imageWithUrl(url: String, placeholder: UIImage) {
        self.image = placeholder
        imageWithUrl(url: url)
    }
    
    ///
    public func imageWithUrl(url: String, placeholderNamed: String) {
        if let image = UIImage(named: placeholderNamed) {
            imageWithUrl(url: url, placeholder: image)
        } else {
            imageWithUrl(url: url)
        }
    }
}

let imagecache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    
    func loadImageUsingCacheWithUrlString(urlString: String,indexPath:Int) {
        
        self.image = nil
        
        //check cache for image first
        if let cachedImage = imagecache.object(forKey: urlString as AnyObject) as? UIImage {
            if(self.tag == indexPath){
                DispatchQueue.main.async {
                    self.image = cachedImage
                }
            }
            return
        }
        
        //otherwise fire off a new download
        let urlStr : NSString = urlString as NSString
        let url = NSURL(string: urlString as String)
        
        //let request:NSURLRequest = NSURLRequest(url: url as! URL)
        
        let request = URLRequest(url: URL(string: urlStr as String)!, cachePolicy: NSURLRequest.CachePolicy.returnCacheDataElseLoad, timeoutInterval: 60)
        
        //Get cache response using request object
        let cacheResponse = URLCache.shared.cachedResponse(for: request as URLRequest)
        
        if cacheResponse == nil {
            
            URLSession.shared.dataTask(with: url! as URL, completionHandler: { (data, response, error) in
                
                //download hit an error so lets return out
                if error != nil {
                    print(error ?? "error on downloading Image")
                    return
                }
                
                DispatchQueue.main.async {
                    
                    let cacheResponse = CachedURLResponse(response: response!, data: data!)
                    URLCache.shared.storeCachedResponse(cacheResponse, for: request)
                    
                    
                    if let downloadedImage = UIImage(data: data!) {
                        imagecache.setObject(downloadedImage, forKey: urlString as AnyObject)
                        
                        if(self.tag == indexPath){
                            self.image = downloadedImage
                        }
                    }
                }
                
            }).resume()
        }else{
            
            //let string = NSString(data: cacheResponse!.data, encoding: String.Encoding.utf8.rawValue)
            if let downloadedImage = UIImage(data: cacheResponse!.data) {
                imagecache.setObject(downloadedImage, forKey: urlString as AnyObject)
                
                if(self.tag == indexPath){
                    self.image = downloadedImage
                }
            }
            
            
        }
    }
}


