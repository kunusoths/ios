//
//  AlertViewExtension.swift
//  Maximus
//
//  Created by Admin on 27/12/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//

import Foundation
import UIKit

// MARK: - UIAlertController Extension

extension UIAlertController {
    func showVersionPopup() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = AppVersionViewController()
        window.windowLevel = UIWindowLevelAlert + 1
        
        AppVesrionManager.shared.updaterWindow = window
        
        window.makeKeyAndVisible()
        window.rootViewController!.present(self, animated: true, completion: nil)
    }
}

