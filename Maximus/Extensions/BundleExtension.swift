//
//  BundleExtension.swift
//  Maximus
//
//  Created by Admin on 27/12/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//

import Foundation

// MARK: - Bundle Extension

extension Bundle {
    class func bundleID() -> String? {
        return Bundle.main.bundleIdentifier
    }
    
    class func bestMatchingAppName() -> String {
        let bundleDisplayName = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String
        let bundleName = Bundle.main.object(forInfoDictionaryKey: kCFBundleNameKey as String) as? String
        
        return bundleDisplayName ?? bundleName ?? ""
    }
}

