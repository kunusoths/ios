//
//  UIDateExtensions.swift
//  Maximus
//
//  Created by Sriram G on 25/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation


extension Date
{
    
    func dateToStringWithFormat() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        return dateFormatter.string(from: self)
    }
    
    
    func dateToStringWithFormatTTTT() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        return dateFormatter.string(from: self)
    }
    
    /*
     * Discussion:
     * nalfnkladnfjad adnlfn ladnlk adn
     *
     */
    func dateToStringInUTC() -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.000'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.string(from: self)
    }
    
    func
        dateMediumFormat() -> String
    {
        let formatter = DateFormatter()
        //format feb 02, 2015
        formatter.dateStyle = .medium;
        return formatter.string(from: self)
        
    }
    
    func quickRideDateFormat() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.string(from: self)
    }
    
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
        // or use capitalized(with: locale) if you want
    }
    
    func getHourMinutesSecond()->(Int,Int,Int){
        let calendar = Calendar.current
        let componant = calendar.dateComponents(in: NSTimeZone.local, from: self)
        let hour    = componant.hour
        let minutes = componant.minute
        let seconds = componant.second
        return(hour!,minutes!,seconds!)
    }
    
    func add(minutes: Int) -> Date {
        return Calendar(identifier: .gregorian).date(byAdding: .minute, value: minutes, to: self)!
    }
    
    static func days(since date: Date) -> Int {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: date, to: Date())
        return components.day ?? 0
    }
    
    static func days(since dateString: String) -> Int? {
        let dateformatter = DateFormatter()
        dateformatter.locale = Locale(identifier: "en_US_POSIX")
        dateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        
        guard let releaseDate = dateformatter.date(from: dateString) else {
            return nil
        }
        
        return days(since: releaseDate)
    }
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the amount of nanoseconds from another date
    func nanoseconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.nanosecond], from: date, to: self).nanosecond ?? 0
    }
    
    func offset(from date: Date) -> Int {
        if hours(from: date)   > 0 {
            return -(hours(from: date) * 60 * 60)
        } else if hours(from: date)   < 0{
            return (hours(from: date) * 60 * 60)
        }
        if minutes(from: date) > 0 {
            return -(minutes(from: date) * 60)
        } else if minutes(from: date) < 0 {
            return (minutes(from: date) * 60)
        }
        if seconds(from: date) > 0 {
            return -(seconds(from: date))
        } else if seconds(from: date) < 0{
            return (seconds(from: date))
        }
        if nanoseconds(from: date) > 0 {
            return -(nanoseconds(from: date))
        } else if nanoseconds(from: date) < 0{
            return (nanoseconds(from: date))
        }
        return 0
    }
}
