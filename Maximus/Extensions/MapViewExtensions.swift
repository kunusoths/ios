//
//  MapViewExtensions.swift
//  Maximus
//
//  Created by Admin on 04/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import GoogleMaps

extension GMSMapView {
    //MARK:- Map Utilities
    
    func drawPolylinewith(Locations:[CLLocation], sourceMArker: GMSMarker , destMarker: GMSMarker) {
        let path = GMSMutablePath()
        var bounds = GMSCoordinateBounds()

        DispatchQueue.main.async {
            [weak self] in
            guard let strongSelf = self else { return }
            if Locations.count > 1 {
                
                for location in Locations {
                    bounds = bounds.includingCoordinate(location.coordinate)
                    path.add(location.coordinate)
                }
                
                
                let source = Locations.first
                let destination = Locations.last
                
                
                // Source Marker
                //                let markerSource = GMSMarker(position: originPos)
                sourceMArker.position = (source?.coordinate)!
                sourceMArker.title = "Starting point"
                sourceMArker.map = self
                sourceMArker.icon = UIImage(named: "Biker Location")
                //Destination Marker
//                destMarker.position = (destination?.coordinate)!
//                destMarker.title = "My Location"
//                destMarker.map = self
//                let resizedimage = UIImage.scaleTo(image: UIImage(named: "Map Pointer")!, w: 40.0, h: 44.0)
//                destMarker.icon = resizedimage
                
                let polyline = GMSPolyline(path: path)
                polyline.strokeWidth = 5.0
                polyline.geodesic = true
                polyline.spans = [GMSStyleSpan(color:AppTheme.THEME_ORANGE)]
                polyline.map = self
                CATransaction.begin()
                CATransaction.setValue(Int(2), forKey: kCATransactionAnimationDuration)
                // YOUR CODE IN HERE
                strongSelf.animate(with: GMSCameraUpdate.fit(bounds))
                CATransaction.commit()
                
                //            let updateCamera = GMSCameraUpdate.fit(bounds, withPadding: 50.0)
                //            self.moveCamera(updateCamera)
                
            }
        }
        
    }
    
    
    func drawRoute(obj:RouteLatLngModel,markerSource:GMSMarker,markerDesti:GMSMarker) {
        
        let path = GMSMutablePath()
        var bounds = GMSCoordinateBounds()
        DispatchQueue.main.async {
            [weak self] in
            guard let strongSelf = self else { return }
        for data in obj.LocationList! {
            let position = CLLocationCoordinate2DMake(Double(data.lat ?? 0),Double(data.lng ?? 0))
            bounds = bounds.includingCoordinate(position)
            path.add(position)

        }
        // 1. Get Main thread
            if (obj.LocationList?.count)! >= 1 {
                //Source & Distination Cooridinates
                let locdatafirst:LocationData = (obj.LocationList?.first)!
                let locdatalast:LocationData = (obj.LocationList?.last)!
                let originPos = CLLocationCoordinate2DMake(Double(locdatafirst.lat ?? 0),Double(locdatafirst.lng ?? 0))
                let distinationPos = CLLocationCoordinate2DMake(Double(locdatalast.lat ?? 0),Double(locdatalast.lng ?? 0))
                
                // Source Marker
//                let markerSource = GMSMarker(position: originPos)
                markerSource.position = originPos
                markerSource.title = "Starting point"
                markerSource.map = self
                markerSource.icon = UIImage(named: "Biker Location")
                //Destination Marker
                markerDesti.position = distinationPos
                markerDesti.title = "My Location"
                markerDesti.map = self
                let resizedimage = UIImage.scaleTo(image: UIImage(named: "Map Pointer")!, w: 40.0, h: 44.0)
                markerDesti.icon = resizedimage
            }
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 5.0
            polyline.geodesic = true
            polyline.spans = [GMSStyleSpan(color:AppTheme.THEME_ORANGE)]
            polyline.map = self
            CATransaction.begin()
            CATransaction.setValue(Int(2), forKey: kCATransactionAnimationDuration)
            // YOUR CODE IN HERE
            strongSelf.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))
            CATransaction.commit()
        }
    }
    
    func setDefaultCameraPosition(){
        //world map default
            self.animate(to: GMSCameraPosition.camera(withLatitude: CLLocationDegrees(exactly: 0.0)!, longitude: CLLocationDegrees(exactly: 0.0)!, zoom: 0))
    }
    
    func drawPolylinewithLatLong(rideObj: Rides, sourceMArker: GMSMarker , destMarker: GMSMarker) {
        let path = GMSMutablePath()
        let bounds = GMSCoordinateBounds()
        DispatchQueue.main.async {
            [weak self] in
            guard let strongSelf = self else { return }
            // Source Marker
            //let markerSource = GMSMarker(position: originPos)
            sourceMArker.position = CLLocationCoordinate2D(latitude: rideObj.startLocation_lat, longitude: rideObj.startLocation_long)
            sourceMArker.title = "Starting point"
            sourceMArker.map = self
            sourceMArker.icon = UIImage(named: "From")
            sourceMArker.userData = 0
            bounds.contains(sourceMArker.position)
            // Destination Marker
            destMarker.position = CLLocationCoordinate2D(latitude: rideObj.endLocation_lat, longitude: rideObj.endLocation_long)
            destMarker.title = "My Location"
            destMarker.map = self
            destMarker.icon = UIImage(named: "To")
            destMarker.userData = 1
            bounds.contains(destMarker.position)
            strongSelf.selectedMarker = destMarker
            
            if let _ = rideObj.routeList {
                if let route = NSKeyedUnarchiver.unarchiveObject(with: (rideObj.routeList!) as! Data) as? [String] {
                    for i in 0..<(route.count) {
                        let path: GMSPath = GMSPath(fromEncodedPath: route[i])!
                        let routePolyline = GMSPolyline(path: path)
                        routePolyline.spans = [GMSStyleSpan(color: AppTheme.THEME_ORANGE)]
                        routePolyline.strokeWidth = 3.0
                        routePolyline.map = self
                        routePolyline.geodesic = true
                    }
                }
                
            }
//            let polyline = GMSPolyline(path: path)
//            polyline.strokeWidth = 5.0
//            polyline.geodesic = true
//            polyline.spans = [GMSStyleSpan(color:AppTheme.THEME_ORANGE)]
//            polyline.map = self
          //  strongSelf.animate(with: GMSCameraUpdate.fit(bounds))
        }
        
    }
    
    func plotPolyLineWithPath(firstRideLeg:Legs, lastRideLeg: Legs, totalLegs: [Legs], poiArray: [POIData], jsonObject: [String: Any], bounds: GMSCoordinateBounds ) -> (GMSCoordinateBounds)
    {
        var planRideBounds = bounds
        planRideBounds = GMSCoordinateBounds()
        //Source Marker
        let sourceMarker = GMSMarker()
        sourceMarker.position = CLLocationCoordinate2DMake((firstRideLeg.startLocation?.lat ?? 0), (firstRideLeg.startLocation?.lng ?? 0))
        sourceMarker.map = self
        sourceMarker.icon = UIImage(named: "From")
        sourceMarker.tracksViewChanges = true
        //            sourceMarker.snippet = startAddress
        sourceMarker.userData = 0
        //Destination Marker
        let destiMarker = GMSMarker()
        destiMarker.position = CLLocationCoordinate2DMake((lastRideLeg.endLocation?.lat ?? 0), (lastRideLeg.endLocation?.lng ?? 0))
        destiMarker.map = self
        destiMarker.icon = UIImage(named: "To")
        destiMarker.tracksViewChanges = true
        destiMarker.userData = 1
        
        if let viaArray: [Any] = jsonObject[PlanRideDataSource.ViaPoint] as? [Any] {
            if viaArray.count > 0 {
                for i in 0 ..< viaArray.count {
                    let viaPoint = GMSMarker()
                    let latitude =  poiArray[i].location.coordinate.latitude  //location[StringConstant.LATITUDE] as? Double
                    let longitude =  poiArray[i].location.coordinate.longitude //location[StringConstant.LONGITUDE] as? Double
                    viaPoint.position = CLLocationCoordinate2DMake(latitude, longitude)
                    viaPoint.map = self
                    viaPoint.zIndex = 1
                    switch poiArray[i].poiType {
                    //Fuel Pump_Actionbar_Selected
                    case .Fuel:
                        viaPoint.icon = UIImage(named: "Petrol Small Selected")
                        break
                        
                    case .Hotel:
                        viaPoint.icon = UIImage(named: "Hotel Small Selected")
                        break
                        
                    case .Restaurent:
                        viaPoint.icon = UIImage(named: "Resturant Small Selected")
                        break
                        
                    case .ViaPoint:
                        viaPoint.icon = UIImage(named: "Via-point-in-map")
                        break
                    }
//                    if let markerView: ViaPointMarkerView = Bundle.main.loadNibNamed("ViaPointMarkerView", owner: nil, options: nil)![0] as? ViaPointMarkerView {
//                        markerView.markerText.setTitle("\(i+1)", for: .normal)
//                        viaPoint.iconView = markerView
//                        viaPoint.zIndex = 1
//                    }
                    
                    // viaPoint.icon = UIImage(named: "Via-point-in-map")
                    viaPoint.tracksViewChanges = true
                    planRideBounds = planRideBounds.includingCoordinate(viaPoint.position)
                }
            }
        }
        
        planRideBounds = planRideBounds.includingCoordinate(sourceMarker.position)
        planRideBounds = planRideBounds.includingCoordinate(destiMarker.position)
        let camera = self.camera(for: planRideBounds, insets: UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0))
        self.camera = camera!
        //self.animate(with: GMSCameraUpdate.fit(planRideBounds))
        self.selectedMarker = destiMarker
        return (planRideBounds)
    }
    
    func drawPolylinewithDestiMarker(Locations:[CLLocation], sourceMArker: GMSMarker , destMarker: GMSMarker) {
        let path = GMSMutablePath()
        var bounds = GMSCoordinateBounds()
        
        DispatchQueue.main.async {
            [weak self] in
            guard let strongSelf = self else { return }
            if Locations.count > 1 {
                
                for location in Locations {
                    bounds = bounds.includingCoordinate(location.coordinate)
                    path.add(location.coordinate)
                }
                
                
                let source = Locations.first
                let destination = Locations.last
                
                
                // Source Marker
                //                let markerSource = GMSMarker(position: originPos)
                sourceMArker.position = (source?.coordinate)!
                sourceMArker.title = "Starting point"
                sourceMArker.map = self
                sourceMArker.icon = UIImage(named: "Biker Location")
                //Destination Marker
                destMarker.position = (destination?.coordinate)!
                destMarker.title = "My Location"
                destMarker.map = self
                let resizedimage = UIImage.scaleTo(image: UIImage(named: "Map Pointer")!, w: 40.0, h: 44.0)
                destMarker.icon = resizedimage
                
                let polyline = GMSPolyline(path: path)
                polyline.strokeWidth = 5.0
                polyline.geodesic = true
                polyline.spans = [GMSStyleSpan(color:AppTheme.THEME_ORANGE)]
                polyline.map = self
                CATransaction.begin()
                CATransaction.setValue(Int(2), forKey: kCATransactionAnimationDuration)
                // YOUR CODE IN HERE
                strongSelf.animate(with: GMSCameraUpdate.fit(bounds))
                CATransaction.commit()
                
                //            let updateCamera = GMSCameraUpdate.fit(bounds, withPadding: 50.0)
                //            self.moveCamera(updateCamera)
                
            }
        }
        
    }
}
