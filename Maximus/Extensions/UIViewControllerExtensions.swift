//
//  UIViewControllerExtensions.swift

import UIKit
import SlideMenuControllerSwift

extension UIViewController {
    // MARK: - Notifications
    //TODO: Document this part
    public func addNotificationObserver(_ name: String, selector: Selector) {
        NotificationCenter.default.addObserver(self, selector: selector, name: NSNotification.Name(rawValue: name), object: nil)
    }

    public func removeNotificationObserver(_ name: String) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: name), object: nil)
    }

    public func removeNotificationObserver() {
        NotificationCenter.default.removeObserver(self)
    }

    #if os(iOS)

//    public func addKeyboardWillShowNotification() {
//        self.addNotificationObserver(NSNotification.Name.UIKeyboardWillShow.rawValue, selector: #selector(UIViewController.keyboardWillShowNotification(_:)))
//    }

    public func addKeyboardDidShowNotification() {
        self.addNotificationObserver(NSNotification.Name.UIKeyboardDidShow.rawValue, selector: #selector(UIViewController.keyboardDidShowNotification(_:)))
    }

//    public func addKeyboardWillHideNotification() {
//        self.addNotificationObserver(NSNotification.Name.UIKeyboardWillHide.rawValue, selector: #selector(UIViewController.keyboardWillHideNotification(_:)))
//    }

    public func addKeyboardDidHideNotification() {
        self.addNotificationObserver(NSNotification.Name.UIKeyboardDidHide.rawValue, selector: #selector(UIViewController.keyboardDidHideNotification(_:)))
    }

//    public func removeKeyboardWillShowNotification() {
//        self.removeNotificationObserver(NSNotification.Name.UIKeyboardWillShow.rawValue)
//    }

    public func removeKeyboardDidShowNotification() {
        self.removeNotificationObserver(NSNotification.Name.UIKeyboardDidShow.rawValue)
    }

    public func removeKeyboardWillHideNotification() {
        self.removeNotificationObserver(NSNotification.Name.UIKeyboardWillHide.rawValue)
    }

    public func removeKeyboardDidHideNotification() {
        self.removeNotificationObserver(NSNotification.Name.UIKeyboardDidHide.rawValue)
    }

    public func keyboardDidShowNotification(_ notification: Notification) {
        if let nInfo = (notification as NSNotification).userInfo, let value = nInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue {

            let frame = value.cgRectValue
            keyboardDidShowWithFrame(frame)
        }
    }

//    public func keyboardWillShowNotification(_ notification: Notification) {
//        if let nInfo = (notification as NSNotification).userInfo, let value = nInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue {
//
//            let frame = value.cgRectValue
//            keyboardWillShowWithFrame(frame)
//        }
//    }

//    public func keyboardWillHideNotification(_ notification: Notification) {
//        if let nInfo = (notification as NSNotification).userInfo, let value = nInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue {
//
//            let frame = value.cgRectValue
//            keyboardWillHideWithFrame(frame)
//        }
  //  }

    public func keyboardDidHideNotification(_ notification: Notification) {
        if let nInfo = (notification as NSNotification).userInfo, let value = nInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue {

            let frame = value.cgRectValue
            keyboardDidHideWithFrame(frame)
        }
    }

    public func keyboardWillShowWithFrame(_ frame: CGRect) {

    }

    public func keyboardDidShowWithFrame(_ frame: CGRect) {

    }

    public func keyboardWillHideWithFrame(_ frame: CGRect) {

    }

    public func keyboardDidHideWithFrame(_ frame: CGRect) {

    }

    // Makes the UIViewController register tap events and hides keyboard when clicked somewhere in the ViewController.
    public func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    public func hideKeyboardWhenTappedAroundAndCancelsTouchesInView() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    #endif

    public func dismissKeyboard() {
        view.endEditing(true)
    }

    // MARK: - VC Container

    /// 
    public var top: CGFloat {
        get {
            if let me = self as? UINavigationController, let visibleViewController = me.visibleViewController {
                return visibleViewController.top
            }
            if let nav = self.navigationController {
                if nav.isNavigationBarHidden {
                    return view.top
                } else {
                    return nav.navigationBar.bottom
                }
            } else {
                return view.top
            }
        }
    }

    ///
    public var bottom: CGFloat {
        get {
            if let me = self as? UINavigationController, let visibleViewController = me.visibleViewController {
                return visibleViewController.bottom
            }
            if let tab = tabBarController {
                if tab.tabBar.isHidden {
                    return view.bottom
                } else {
                    return tab.tabBar.top
                }
            } else {
                return view.bottom
            }
        }
    }

    ///
    public var tabBarHeight: CGFloat {
        get {
            if let me = self as? UINavigationController, let visibleViewController = me.visibleViewController {
                return visibleViewController.tabBarHeight
            }
            if let tab = self.tabBarController {
                return tab.tabBar.frame.size.height
            }
            return 0
        }
    }

    ///
    public var navigationBarHeight: CGFloat {
        get {
            if let me = self as? UINavigationController, let visibleViewController = me.visibleViewController {
                return visibleViewController.navigationBarHeight
            }
            if let nav = self.navigationController {
                return nav.navigationBar.h
            }
            return 0
        }
    }

    /// SwiftExtensions
    public var navigationBarColor: UIColor? {
        get {
            if let me = self as? UINavigationController, let visibleViewController = me.visibleViewController {
                return visibleViewController.navigationBarColor
            }
            return navigationController?.navigationBar.tintColor
        } set(value) {
            navigationController?.navigationBar.barTintColor = value
        }
    }

    /// SwiftExtensions
    public var navBar: UINavigationBar? {
        get {
            return navigationController?.navigationBar
        }
    }

    /// SwiftExtensions
    public var applicationFrame: CGRect {
        get {
            return CGRect(x: view.x, y: top, width: view.w, height: bottom - top)
        }
    }

    // MARK: - VC Flow

    /// SwiftExtensions
    public func pushVC(_ vc: UIViewController) {
        navigationController?.pushViewController(vc, animated: true)
    }

    /// SwiftExtensions
    public func popVC() {
        _ = navigationController?.popViewController(animated: true)
    }

    /// SwiftExtensions
    public func presentVC(_ vc: UIViewController) {
        present(vc, animated: true, completion: nil)
    }

    /// SwiftExtensions
    public func dismissVC(completion: (() -> Void)? ) {
        dismiss(animated: true, completion: completion)
    }

    /// SwiftExtensions
    public func addAsChildViewController(_ vc: UIViewController, toView: UIView) {
        self.addChildViewController(vc)
        toView.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
    }

    /// Adds image named: as a UIImageView in the Background
    func setBackgroundImage(_ named: String) {
        let image = UIImage(named: named)
        let imageView = UIImageView(frame: view.frame)
        imageView.image = image
        view.addSubview(imageView)
        view.sendSubview(toBack: imageView)
    }

    /// Adds UIImage as a UIImageView in the Background
    @nonobjc func setBackgroundImage(_ image: UIImage) {
        let imageView = UIImageView(frame: view.frame)
        imageView.image = image
        view.addSubview(imageView)
        view.sendSubview(toBack: imageView)
    }
    
    public func upgradeDM() {
        self.checkIfDmUpdated()
    }
    
    fileprivate func checkIfDmUpdated() {
        do {
            if let file = Bundle.main.url(forResource: DefaultKeys.KEY_OTA_MANIFEST, withExtension: DefaultKeys.KEY_JSON) {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    if let displayModule: [String: Any] = object[DefaultKeys.KEY_OTA_MANIFEST] as? [String : Any] {
                        if let firmware: [String: Any] = displayModule[DefaultKeys.KEY_DISPLAY_MODULE] as? [String : Any] {
                            if let version = firmware[DefaultKeys.KEY_FIRMWARE] as? [String: Any] {
                                if let versionNo: String = version[DefaultKeys.KEY_VERSION] as? String {
                                    if let versionNoInt = Int(versionNo) {
                                        MyDefaults.setInt(value: versionNoInt, key: DefaultKeys.KEY_JSON_VERSION)
                                        if versionNoInt > MyDefaults.getInt(key: DefaultKeys.KEY_DM_VERSION) {
                                            UIApplication.shared.applicationIconBadgeNumber = 1
                                            MyDefaults.setBoolData(value: false, key: DefaultKeys.KEY_UPDATE_SUCCESSFUL)
                                            self.showDMUpgradeAlert()
                                        }
                                    }
                                }
                            }
                        }
                    }
                    print(object)
                } else if let object = json as? [Any] {
                    // json is an array
                    print(object)
                } else {
                    print("JSON is invalid")
                }
            }else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func showDMUpgradeAlert() {
        let alert: UIAlertController = UIAlertController(title: "", message: "Display Module firmware update is available. Please pair with the Display Module and proceed to update through My Support section.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (success) in
            // go to settings
                       let supportContainer = SupportContainerViewController()
            supportContainer.isDMUpgradeMode = true
            if self.slideMenuController() == nil {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let navigationController = UINavigationController(rootViewController: supportContainer)
                let mainViewController = navigationController
                let leftViewController = SideMenuVC()
                let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
                slideMenuController.changeLeftViewWidth(self.view.size.width-60)
                self.navigationController?.viewControllers.last?.view.endEditing(true)
                appDelegate.window?.rootViewController = slideMenuController

            } else {
                self.navigationController?.viewControllers.last?.view.endEditing(true)
                self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: supportContainer), close: true)
            }
            }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func setApplicationBadgeIcon() {
        if  MyDefaults.getInt(key: DefaultKeys.KEY_JSON_VERSION) != 0 {
            if MyDefaults.getInt(key: DefaultKeys.KEY_JSON_VERSION) == MyDefaults.getInt(key: DefaultKeys.KEY_DM_VERSION)  || MyDefaults.getBoolData(key: DefaultKeys.KEY_UPDATE_SUCCESSFUL) || BluetoothManager.sharedInstance().getBLEState() == .DISCONNECT_NP ||  BluetoothManager.sharedInstance().getBLEState() == .DISCONNECT_E || BluetoothManager.sharedInstance().getBLEState() == .DISCONNECT_N || BluetoothManager.sharedInstance().getBLEState() == .DISCONNECT_BONP || BluetoothManager.sharedInstance().getBLEState() == .NONE {
                UIApplication.shared.applicationIconBadgeNumber = 0
            } else {
                UIApplication.shared.applicationIconBadgeNumber = 1
            }
        } else {
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
    }
    
    func showEmailVerifyPopup() {
        if MyDefaults.getBoolData(key: DefaultKeys.KEY_EMAIL_VERIFY) {
            MyDefaults.setBoolData(value: false, key: DefaultKeys.KEY_EMAIL_VERIFY)
            let okAction = UIAlertAction.init(title: "OK", style: .default) { (action) -> Void in
                print("OK")
                MyDefaults.setBoolData(value: true, key: "isEditModeOn")
                let profile = ProfileVC()
                if self.slideMenuController() == nil {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let navigationController = UINavigationController(rootViewController: profile)
                    let mainViewController = navigationController
                    let leftViewController = SideMenuVC()
                    let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
                    slideMenuController.changeLeftViewWidth(self.view.size.width-60)
                    appDelegate.window?.rootViewController = slideMenuController
                    
                } else { self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: profile), close: true)
                }
            }
            
            let alertController = UIAlertController.init(title: "", message: "Entered email address could not be verified. Click OK to update.", preferredStyle: .alert)
            alertController.addAction(okAction)
            self.presentVC(alertController)
        }
    }
    
    func showLocationTurnOnPopup() {
        let alert = UIAlertController.init(title: "", message: "Please turn on the location services", preferredStyle: .alert)
        let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
        let settingsAction = UIAlertAction.init(title: "Settings", style: .default) { (action) in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)")
                })
            }
        }
        alert.addAction(okAction)
        alert.addAction(settingsAction)
        self.presentVC(alert)
    }
    
    func getFirmwareVersionFromManifest() {
        do {
            if let file = Bundle.main.url(forResource: DefaultKeys.KEY_OTA_MANIFEST, withExtension: DefaultKeys.KEY_JSON) {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    if let displayModule: [String: Any] = object[DefaultKeys.KEY_OTA_MANIFEST] as? [String : Any] {
                        if let firmware: [String: Any] = displayModule[DefaultKeys.KEY_DISPLAY_MODULE] as? [String : Any] {
                            if let version = firmware[DefaultKeys.KEY_FIRMWARE] as? [String: Any] {
                                if let versionNo: String = version[DefaultKeys.KEY_VERSION] as? String {
                                    if let versionNoInt = Int(versionNo) {
                                        MyDefaults.setInt(value: versionNoInt, key: DefaultKeys.KEY_JSON_VERSION)
                                    }
                                }
                            }
                        }
                    }
                    print(object)
                } else if let object = json as? [Any] {
                    // json is an array
                    print(object)
                } else {
                    print("JSON is invalid")
                }
            }else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}
