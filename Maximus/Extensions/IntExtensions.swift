//
//  IntExtensions.swift
//  Maximus
//
//  Created by Admin on 02/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation

extension Int {
    func convertSecondsToHours () -> (Int, Int, Int) {
        return (self / 3600, (self % 3600) / 60, (self % 3600) % 60)
    }
    
    func toString() -> String{
        return "\(self)"
        
    }
}
