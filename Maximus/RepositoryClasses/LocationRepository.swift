//
//  LocationRepository.swift
//  Maximus
//
//  Created by APPLE on 03/10/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit

protocol BikeLocationProtocol {
    typealias resultData = ([String : Any]? , [String : Any]?) -> Void
    func getDeviceLocation(bikerID: Int, requestType: String, completionHandler: @escaping resultData)

}


class LocationRepository: NSObject, BikeLocationProtocol {
    
    let locationApiManager = LocationApiManager()
    
    override init() {
        super.init()
    }
    
    func getDeviceLocation(bikerID: Int, requestType: String, completionHandler: @escaping LocationRepository.resultData) {
        if NetworkCheck.isNetwork() {
            self.locationApiManager.getCurrentDeviceLocation(bikerID: bikerID, requestType: requestType) { (data, err) in
                completionHandler(data, err)
            }
        } else {
            var dict = ["data": "You are offline. Go online to view your bike location"]
            dict["RequestType"] = requestType
            //self.locationRepoCallback?.locationRepoFailureCallback(error: dict)
            completionHandler(nil, dict)
        }
    }
    

    
}

class MockLocationRepository: BikeLocationProtocol {
    func getDeviceLocation(bikerID: Int, requestType: String, completionHandler: @escaping MockLocationRepository.resultData) {
        
        let data:[String:Any] = ["bikerID": bikerID,
                                "reqType": requestType]
        completionHandler(data, nil)
    }
    


    
}
