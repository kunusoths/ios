//
//  AnalyticsRepository.swift
//  Maximus
//
//  Created by APPLE on 01/10/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit

class AnalyticsRepository: NSObject {
    fileprivate let apiHandler = AnalyticsApiManager()

    override init() {
        super.init()
    }
    
    /*
     Get recommended rides
     */
    func getRecommendedRides(bikerID: Int, requestType: String, pageNo: Int, size: Int,successCallback: @escaping successData, failureCallback:@escaping errorData) {
        if NetworkCheck.isNetwork() {
            apiHandler.getRecommendedRides(bikerID: bikerID, requestType: requestType, pageNo: pageNo, size: size, successCallback: { (data) in
                successCallback(data)
            }) { (errorData) in
                failureCallback(errorData)
            }
        } else {
            var error: [String: Any] = [:]
            error["description"] = ToastMsgConstant.network
            failureCallback(error)
        }
    }
}
