//
//  PlanRideRepository.swift
//  Maximus
//
//  Created by Isha Ramdasi on 28/06/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import ObjectMapper
import CoreData
import Alamofire

protocol PlanRidesRepoCallback: class {
    func repoFailureCallback(error: [String: Any])
    func repoSuccessCallback(data: Any)
}

protocol PlanRideProtocol {
    func saveRideResponse(rideResponse: RideCreateResponse, type: String, successCallback: @escaping successData, failureCallback:@escaping errorData)
    func getRideDetailsOfOldRides(rideCard: Rides, type: String, successCallback: @escaping successData, failureCallback:@escaping errorData)
    func getRideRouteById(rideID:Int,bikerId:Int, type: String, successCallback: @escaping successData, failureCallback:@escaping errorData)
    func getRoutesForLatLng(jsonObj: [String: Any], type: String, successCallback: @escaping successData, failureCallback:@escaping errorData)
    func getPOIWithPolyline(polyline: String, request: String, successCallback: @escaping successData, failureCallback:@escaping errorData)
    func getPOIDetails(poiId:String, request: String, successCallback: @escaping successData, failureCallback:@escaping errorData)
    func getPlannedWavepoints(rideID: Int, request: String, successCallback: @escaping successData, failureCallback:@escaping errorData)
    func createRide(parameter: [String:Any], rqstType:HTTPMethod, request: String, successCallback: @escaping successData, failureCallback:@escaping errorData)
    func getRoutesFromGoogleApi(jsonObj: [String: Any], type: String, successCallback: @escaping successData, failureCallback:@escaping errorData)
}

class PlanRideRepository: NSObject, PlanRideProtocol {
    
    //weak var planRideRepoDelegate: PlanRidesRepoCallback?
    let apiManager = RidesApiManager()
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()

    override init() {
        super.init()
        //apiManager.cloudCallbackDelegate = self
    }
    
    func saveRideResponse(rideResponse: RideCreateResponse, type: String, successCallback: @escaping successData, failureCallback:@escaping errorData) {
        Rides.insertMyRideCard(rideCreateResponse: rideResponse, context: context, route: rideResponse.tbt_route ?? "", tbtRoute: rideResponse.tbt_route ?? "")
        
        if rideResponse.planned ?? false {
            self.getRideRouteById(rideID: rideResponse.id ?? 0, bikerId: rideResponse.biker?.id ?? 0, type: "GetRideRouteById", successCallback: { (data) in
                successCallback(data)
            }) { (error) in
                failureCallback(error)
            }
            
        }else {
            let data:[String: Any] = ["data": rideResponse,
                                      "RequestType": type]
            successCallback(data)
        }
        
    }
    
    func getRideDetailsOfOldRides(rideCard: Rides, type: String, successCallback: @escaping successData, failureCallback:@escaping errorData) {
        if NetworkCheck.isNetwork() {
            self.getRideRouteById(rideID: Int(rideCard.rideID ?? "")!, bikerId: Int(rideCard.admin_biker_id ?? "")!, type: type, successCallback: { (data) in
                successCallback(data)
            }) { (error) in
                failureCallback(error)
            }
        } else {
            successCallback(["":""])
        }
    }
    
    func cloudFailure(error: [String : Any]) {
//        self.planRideRepoDelegate?.repoFailureCallback(error: error)
        
        
    }
    
    func parseRideRouteByIdData(data: [String: Any]) -> Bool {
        var totalTime = 0
        var totalDistanceInMeters = 0
        if data["RequestType"] as? String ==  "GetRideRouteById" {
            if let objdirectionL:SingleRouteDetails = data["data"] as? SingleRouteDetails {
                
                if let objroute:RouteDetails = Mapper<RouteDetails>().map(JSONString: (objdirectionL.tbt_route)!) {
                    Rides.updateRouteDetailsFor(rideID: String(objdirectionL.id ?? 0), context: context, updatedDetails: objroute, routeDetails: objdirectionL)
                    Rides.addWaypointsInRide(rideID: String(objdirectionL.id ?? 0), context: context, obj: objdirectionL.waypointList ?? [])
                    if let route = objroute.routeList?[0] {
                        if let legs: [Legs] = route.legs {
                            for leg in legs {
                                totalDistanceInMeters += (leg.distance?.value ?? 0)
                                MyDefaults.setInt(value: totalDistanceInMeters, key: "JourneyDistance")
                                totalTime += (leg.duration?.value ?? 0)
                                
                            }
                            MyDefaults.setInt(value: totalTime, key: "JourneyTime")
                            Rides.updateDistanceAndDurationAsPerTBTRoute(rideID: String(objdirectionL.id ?? 0), context: context, distance: totalDistanceInMeters, time: totalTime)
                            print("TBT route saved from repo")
                        }
                    }
                }
            }
        }
        return true
    }
    
    func cloudSuccess(data: [String : Any]) {
        var totalTime = 0
        var totalDistanceInMeters = 0
        if data["RequestType"] as? String ==  "GetRideRouteById" {
            if let objdirectionL:SingleRouteDetails = data["data"] as? SingleRouteDetails {
                
                if let objroute:RouteDetails = Mapper<RouteDetails>().map(JSONString: (objdirectionL.tbt_route)!) {
                    Rides.updateRouteDetailsFor(rideID: String(objdirectionL.id ?? 0), context: context, updatedDetails: objroute, routeDetails: objdirectionL)
                    Rides.addWaypointsInRide(rideID: String(objdirectionL.id ?? 0), context: context, obj: objdirectionL.waypointList ?? [])
                    if let route = objroute.routeList?[0] {
                        if let legs: [Legs] = route.legs {
                            for leg in legs {
                                totalDistanceInMeters += (leg.distance?.value ?? 0)
                                MyDefaults.setInt(value: totalDistanceInMeters, key: "JourneyDistance")
                                totalTime += (leg.duration?.value ?? 0)
                               
                            }
                            MyDefaults.setInt(value: totalTime, key: "JourneyTime")
                            Rides.updateDistanceAndDurationAsPerTBTRoute(rideID: String(objdirectionL.id ?? 0), context: context, distance: totalDistanceInMeters, time: totalTime)
                            print("TBT route saved from repo")
                        }
                    }
                }
            }
        } else if data["RequestType"] as? String == "SaveAdhocRide" {
            Rides.insertMyRideCard(rideCreateResponse: (data["data"] as? RideCreateResponse)!, context: context, route: "", tbtRoute: "")
        }
        //self.planRideRepoDelegate?.repoSuccessCallback(data: data)
    }
    
    func getRideRouteById(rideID:Int,bikerId:Int, type: String, successCallback: @escaping successData, failureCallback:@escaping errorData)
    {
        if Rides.isRideCardPresentInDB(rideId: String(rideID), context: context) {
            let rideCard = Rides.fetchRideWithID(rideID: String(rideID), context: context)
                if NetworkCheck.isNetwork() {
                    self.apiManager.getRideDetails(rideID: rideID, bikerID: bikerId, type: type, successCallback: { (data) in
                        let parsedData = self.parseSingleRouteDetails(data: data, requestType: type)
                        if self.parseRideRouteByIdData(data: parsedData) {
                            successCallback(parsedData)
                        }
                    }) { (error) in
                        failureCallback(error)
                    }
                }
//                } else if rideCard?.tbt_route != nil {
//                    let directionDetails : SingleRouteDetails = SingleRouteDetails()
//                    directionDetails.tbt_route = rideCard?.tbt_route
//                    directionDetails.route = rideCard?.route
//                    directionDetails.distance = Int((rideCard?.total_ride_distance)!)
//                    var directionDetailsData:[String:Any] = [StringConstant.data:directionDetails]
//                    directionDetailsData["RequestType"] = type
//                    if parseRideRouteByIdData(data: directionDetailsData) {
//                        successCallback(directionDetailsData)
//                    }
//                }
                    else {
                    var dict: [String: Any] = [:]
                    dict["RequestType"] = type
                    dict["description"] = "Request time out."
                    failureCallback(dict)
            }
        } else {
            if NetworkCheck.isNetwork() {
                self.apiManager.getRideDetails(rideID: rideID, bikerID: bikerId, type: type, successCallback: { (data) in
                    let dict = self.parseSingleRouteDetails(data: data, requestType: type)
                    if self.parseRideRouteByIdData(data: dict) {
                        successCallback(dict)
                    }
                }) { (error) in
                    failureCallback(error)
                }
            } else {
                failureCallback(RequestTimeOut().json)
            }
        }
    }
    
//    func getDeviceCurrentLocation(bikerID: Int, type: String) {
//        if NetworkCheck.isNetwork() {
//            self.apiManager.getCurrentDeviceLocation(bikerID: bikerID, request: type)
//        } else {
//            var dict: [String: Any] = [:]
//            dict["RequestType"] = type
//            dict["description"] = "Request time out."
//            self.planRideRepoDelegate?.repoFailureCallback(error: dict)
//        }
//    }
    
    func getRoutesForLatLng(jsonObj: [String: Any], type: String, successCallback: @escaping successData, failureCallback:@escaping errorData) {
        if NetworkCheck.isNetwork() {
            self.apiManager.getRoutesForLatLng(data: jsonObj, request: type, successCallback: { (data) in
                let parsedData = self.parseDirectionDetails(data: data, requestType: type)
                successCallback(parsedData)
            }) { (error) in
                failureCallback(error)
            }
        } else {
            var dict: [String: Any] = [:]
            dict["RequestType"] = type
            dict["description"] = "Request time out."
            failureCallback(dict)
        }
    }
    
    func getRoutesFromGoogleApi(jsonObj: [String: Any], type: String, successCallback: @escaping successData, failureCallback:@escaping errorData) {
        if NetworkCheck.isNetwork() {
            let googlePlaceService = GooglePlacesService()
            googlePlaceService.getRouteFromSourceDestinationWaypoints(jsonObj) { (status, result, data) in
                if status == "OK" {
                    let parsedData = self.parseGoogleDirectionDetails(data: data, requestType: type)
                    successCallback(parsedData)
                } else {
                    var dict: [String: Any] = [:]
                    dict["RequestType"] = type
                    dict["description"] = status
                    failureCallback(dict)
                }
            }
        } else {
            var dict: [String: Any] = [:]
            dict["RequestType"] = type
            dict["description"] = "Request time out."
            failureCallback(dict)
        }
    }
    
    func getPOIWithPolyline(polyline: String, request: String, successCallback: @escaping successData, failureCallback:@escaping errorData) {
        if NetworkCheck.isNetwork() {
            apiManager.getPOIWithPolyline(polyline: polyline, request: request, successCallback: { (data) in
                let parsedData = self.parseGetRidePOIModel(data: data, requestType: request)
                successCallback(parsedData)
            }) { (error) in
                failureCallback(error)
            }
        } else {
            var error: [String: Any] = [:]
            error["description"] = ToastMsgConstant.network
            failureCallback(error)
        }
    }
    
    func getPOIDetails(poiId:String, request: String, successCallback: @escaping successData, failureCallback:@escaping errorData) {
        if NetworkCheck.isNetwork() {
            apiManager.getPOIDetails(poiId: poiId, request: request, successCallback: { (data) in
                let parsedData = self.parseGogglePOIDetailsModel(data: data, requestType: request)
                successCallback(parsedData)
            }) { (error) in
                failureCallback(error)
            }
        } else {
            var error: [String: Any] = [:]
            error["description"] = ToastMsgConstant.network
            failureCallback(error)
        }
    }
    
    func getPlannedWavepoints(rideID: Int, request: String, successCallback: @escaping successData, failureCallback:@escaping errorData) {
        if NetworkCheck.isNetwork() {
            apiManager.getPlannedWavepoints(rideID: rideID, request: request, successCallback: {(data) in
                let parsedData = self.parsePlannedWavePointsModel(data: data, requestType: request)
                successCallback(parsedData)
            }, failureCallback:{(error) in
                failureCallback(error)
            } )
        } else {
            var error: [String: Any] = [:]
            error["description"] = ToastMsgConstant.network
            failureCallback(error)
        }
    }
    
    func createRide(parameter: [String:Any], rqstType:HTTPMethod, request: String, successCallback: @escaping successData, failureCallback:@escaping errorData) {
        if NetworkCheck.isNetwork() {
            apiManager.createRide(parameter: parameter, rqstType: rqstType, request: request, successCallback: {data in
              //  let parsedData = self.parseCreateRideResponse(data: data)
                let user = Mapper<RideCreateResponse>().map(JSON:data as! [String : Any])!
                var dataDict:[String:Any] = [StringConstant.data:user]
                dataDict["RequestType"] = request
                successCallback(dataDict)
            }, failureCallback: {error in
                failureCallback(error)
            })
        } else {
            var error: [String: Any] = [:]
            error["description"] = ToastMsgConstant.network
            failureCallback(error)
        }
    }

    func parseCreateRideResponse(data: Any, requestType: String) -> [String: Any]{
        let user = Mapper<RideCreateResponse>().map(JSON:data as! [String : Any])!
        var dataDict:[String:Any] = [:]
        dataDict[StringConstant.data] = user
        dataDict["RequestType"] = requestType
        return dataDict
    }
    
    func parsePlannedWavePointsModel(data: Any, requestType: String) -> [String: Any] {
        let json1 = [StringConstant.wavePointsList:data]
        let poi = Mapper<PlannedWavePointsModel>().map(JSON:json1 )!
        var dataDict:[String:Any] = [:]
        dataDict[StringConstant.data] = poi
        dataDict["RequestType"] = requestType
        return dataDict
    }
    
    func parseGogglePOIDetailsModel(data: Any, requestType: String) -> [String: Any] {
        let poi = Mapper<GogglePOIDetailsModel>().map(JSON: data as! [String : Any])
        var dataDict:[String:Any] = [:]
        dataDict[StringConstant.data] = poi!
        dataDict["RequestType"] = requestType
        return dataDict
    }
    
    func parseGetRidePOIModel(data: Any, requestType: String) -> [String: Any] {
        let json1 = [StringConstant.getPOIList:data]
        let poi = Mapper<GetRidePOIModel>().map(JSON:json1 )!
        var dataDict:[String:Any] = [:]
        dataDict[StringConstant.data] = poi
        dataDict["RequestType"] = requestType
        return dataDict
    }
    
    func parseDirectionDetails(data: Any, requestType: String) -> [String: Any] {
        let directionDetails = Mapper<DirectionDetails>().map(JSON: data as! [String : Any])!
        var directionDetailsData:[String:Any] = [:]
        directionDetailsData[StringConstant.data] = directionDetails
        directionDetailsData["RequestType"] = requestType
        return directionDetailsData
    }
    
    func parseSingleRouteDetails(data: Any, requestType: String) -> [String: Any] {
        let directionDetails = Mapper<SingleRouteDetails>().map(JSON: data as! [String : Any])
        var directionDetailsData:[String:Any] = [:]
        directionDetailsData[StringConstant.data] = directionDetails ?? ""
        directionDetailsData["RequestType"] = requestType
        return directionDetailsData
    }
    
    func parseGoogleDirectionDetails(data: Any, requestType: String) -> [String: Any] {
        var directionDetailsData:[String:Any] = [:]
        directionDetailsData[StringConstant.data] = (data as! [String : Any])["data"]
        directionDetailsData["RequestType"] = requestType
        return directionDetailsData
    }
    
}
