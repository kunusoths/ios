//
//  RideStatsRepository.swift
//  Maximus
//
//  Created by Isha Ramdasi on 16/07/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit

protocol RideStatsRepoCallback:class {
    func rideStatsRepoFailureCallback(error: [String: Any])
    func rideStatsRepoSuccessCallback(data: Any)
}

class RideStatsRepository: NSObject, apiCallback {
    weak var rideStatsRepoDelegate: RideStatsRepoCallback?
    let apiManager = RideStatsApiManager()
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    
    override init() {
        super.init()
        apiManager.cloudCallbackDelegate = self
    }
    
    func getRideStatsFor(bikerID: Int, page: Int, size: Int, request: String) {
        if NetworkCheck.isNetwork() {
            self.apiManager.getRideLogStats(bikerID: bikerID, pageNo: page, size: size, request: request)
        } else {
            var dict:[String: Any] = [:]
            dict["RequestType"] = request
            dict["data"] = Rides.getAllCompletedRides(context: context)
            self.rideStatsRepoDelegate?.rideStatsRepoSuccessCallback(data: dict)
        }
    }
    
    func putRideTitle(parameter:[String:Any], request: String){
        if NetworkCheck.isNetwork() {
            self.apiManager.putRideTitle(parameter: parameter, request: request)
        } else {
            var error: [String: Any] = [:]
            error["description"] = ToastMsgConstant.network
            self.rideStatsRepoDelegate?.rideStatsRepoFailureCallback(error: error)
        }
    }
    
    func getRideStats(rideID:Int, bikerID:Int, request: String) {
        if NetworkCheck.isNetwork() {
            self.apiManager.getRideStats(rideID: rideID, bikerID: bikerID, request: request)
        } else {
            
        }
    }
    
    func getRideRoute(rideID:Int, bikerID:Int, request: String) {
        if NetworkCheck.isNetwork() {
            self.apiManager.getRideRoute(rideID: rideID, bikerID: bikerID, request: request)
        } else {
            var error: [String: Any] = [:]
            error["description"] = ToastMsgConstant.network
            self.rideStatsRepoDelegate?.rideStatsRepoFailureCallback(error: error)
        }
    }
    
    func getGraphSpeedPoints(rideID:Int, bikerID:Int, request: String) {
        if NetworkCheck.isNetwork() {
            self.apiManager.getGraphSpeedPoints(rideID: rideID, bikerID: bikerID, request: request)
        } else {
            var error: [String: Any] = [:]
            error["description"] = ToastMsgConstant.network
            self.rideStatsRepoDelegate?.rideStatsRepoFailureCallback(error: error)
        }
    }
    
    func getStickerBoard(bikerID:Int, requestType: String) {
      //  if NetworkCheck.isNetwork() {
            self.apiManager.getStickerBoard(bikerID: bikerID, requestType: requestType)
//        } else {
//            var error: [String: Any] = [:]
//            error["description"] = ToastMsgConstant.network
//            self.rideStatsRepoDelegate?.rideStatsRepoFailureCallback(error: error)
//        }
    }
    
    func cloudSuccess(data: [String : Any]) {
        if data["RequestType"] as? String == "GetRideStats" {
            if let rideLogs = data["data"] as? [RideLogModel] {
                for i in 0..<rideLogs.count {
                    Rides.insertRideLogDetails(rideLog: rideLogs[i], context: context)
                }
            }
            var dict:[String: Any] = [:]
            dict["RequestType"] = data["RequestType"]
            dict["data"] = Rides.getAllCompletedRides(context: context)
            self.rideStatsRepoDelegate?.rideStatsRepoSuccessCallback(data: dict)
        } else if data["RequestType"] as? String == "GetGraphSpeedPoints" {
            var locationSet: [LocationDetails] = []
            if let speedPointsModel:SpeedPointsModel = data["data"] as? SpeedPointsModel {
                for speedData in speedPointsModel.SpeedList!
                {
                    let locationDetails = LocationDetails(context: context)
                    locationDetails.ground_speed = speedData.ground_speed ?? 0
                    locationDetails.max_ground_speed = speedData.max_ground_speed ?? 0
                    locationDetails.min_ground_speed = speedData.min_ground_speed ?? 0
                    locationDetails.rideID = String(speedPointsModel.RideId!)
                   // speedPoints.append(Double(speedData.ground_speed!)*0.036)

                    locationDetails.latitude = speedData.lat ?? 0
                    locationDetails.longitude = speedData.lng ?? 0
                    locationDetails.packettimestamp = speedData.packettimestamp
                    locationSet.append(locationDetails)
                }
                Rides.addGraphDetails(rideID: String(speedPointsModel.RideId ?? 0), context: context, graphDetails: NSSet(array: locationSet))
                var dictData:[String: Any] = [:]
                dictData["data"] = locationSet
                self.rideStatsRepoDelegate?.rideStatsRepoSuccessCallback(data: dictData)
            }
        } else if data["RequestType"] as? String == "PutRideTitle" {
            if let rideData = data["data"] as? RideCreateResponse {
                Rides.updateRideTitle(title: rideData.title!, rideID: String(rideData.id ?? 0), context: context)
                self.rideStatsRepoDelegate?.rideStatsRepoSuccessCallback(data: data)
            }
        } else {
            self.rideStatsRepoDelegate?.rideStatsRepoSuccessCallback(data: data)
        }
    }
    
    func cloudFailure(error: [String : Any]) {
        self.rideStatsRepoDelegate?.rideStatsRepoFailureCallback(error: error)
    }

}
