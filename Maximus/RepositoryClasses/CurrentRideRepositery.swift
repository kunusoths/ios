//
//  CurrentRideRepositery.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 16/07/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import Foundation
import CoreData
import UIKit

protocol CurrentRideRepoDelegate:class {
    func repoSuccess(data:[String:Any])
    func repoFailed(data:[String:Any])
    func offlineRideStartSuccess(data:[String:Any])
}

class CurrentRideRepositery: NSObject, RideStateAPIHandlerDelegate, PlanRidesRepoCallback {
    
    fileprivate let apiHandler = RideStateAPIHandler()
    weak var delegate:CurrentRideRepoDelegate?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    let viewcontext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    var bikerDetails:BikerDetails!
    let planRideRepo = PlanRideRepository()
    var rideData : RideCreateResponse?
    var oldRideID = ""
    
    override init() {
        super.init()
        print("CurrentRideRepositery Init")
        self.bikerDetails = BikerDetails.getBikerDetails(context: context)
        apiHandler.delegate = self
    }
    
    deinit {
         print("CurrentRideRepositery deinit")
    }
    
    func startAdhocRide() {
        let dic = [StringConstant.planned:false]
//        rideRequestHandler.executeDelegate = self
        RideRequestHandler.requestHandlerSharedInstance.addRequestObj(rideID: "", rideType: .AD_HOC, requestType: .Create, requestJsonString: dic.jsonEncode() as String, context: context)
        Rides.insertAdhocRideCard(dummyRideID: (MyDefaults.getData(key: DefaultKeys.KEY_REQUEST_OBJ_ID) as? String)!, dummyRideTitle: Constants.getAdHocRideTitle(), context: context)
        RideRequestHandler.requestHandlerSharedInstance.addRequestObj(rideID: (MyDefaults.getData(key: DefaultKeys.KEY_REQUEST_OBJ_ID) as? String)!, rideType: .AD_HOC, requestType: .Start, requestJsonString: "", context: context)
        guard let rideDetails = Rides.getMyProgressRide(context: context) else {
            return
        }
        
        print("CurrentRideRepo : Current Ride id is \(String(describing: rideDetails.rideID))")
        JourneyManager.sharedInstance.rideStarted(rideDetails: rideDetails)
        let rideData:[String:Any] = ["data": rideDetails]
        self.delegate?.offlineRideStartSuccess(data: rideData)
    }
    
    func startRide(rideID: Int) {
        RideRequestHandler.requestHandlerSharedInstance.addRequestObj(rideID: String(rideID), rideType: .PLANNED, requestType: .Start, requestJsonString: "", context: context)
        Rides.updateRideStatusAfterStartingRide(rideID: String(rideID), context: context)
        guard let rideDetails = Rides.fetchRideWithID(rideID: String(rideID), context: context) else {
            return
        }
        
        print("CurrentRideRepo : Current Ride id is \(String(describing: rideDetails.rideID))")
        JourneyManager.sharedInstance.rideStarted(rideDetails: rideDetails)
        let rideData:[String:Any] = ["data": rideDetails]

        self.delegate?.offlineRideStartSuccess(data: rideData)
    }
    
    func stopRide(rideId: Int) {
        let rideObj = Rides.fetchRideWithID(rideID: String(rideId), context: context)
        print("Stop Ride rideId: \(rideObj?.rideID ?? "ww") , type: \(rideObj?.planned ?? false)")
        let type = rideObj?.planned ?? false ? RideType.PLANNED : RideType.AD_HOC
        RideRequestHandler.requestHandlerSharedInstance.addRequestObj(rideID: String(rideId), rideType: type, requestType: .Stop, requestJsonString: "", context: context)
        Rides.setRideAsCompleted(rideID: String(rideId), context: viewcontext)
        JourneyManager.sharedInstance.rideHasBeenStopped()
        let isVirtualCm = MyDefaults.getBoolData(key: DefaultKeys.IS_CM_VIRTUAL)
        if isVirtualCm {
            //Stop VCMPacket Timer
            let vcmPktManager = VCMPacketManager.sharedInstance
            // vcmPktManager.vcmDelegate = self
            vcmPktManager.invalidateVCMTimer()
            MyDefaults.setInt(value: 0, key: DefaultKeys.MESSAGE_NUMERATOR)
        }
    }
   
    
    func repoStartRide(type: RideType, rideID:Int, parameter:[String:Any])  {
        print("Offline start stop: Repo start ride API call..")
        oldRideID = String(rideID)
        apiHandler.apiStartRide(type: type, rideID: rideID, parameter: parameter)
    }
    
    func repoRideStop(rideId: Int, rideType: RideType, parameter:[String:Any]){
        if NetworkCheck.isNetwork() {
            let isVirtualCm = MyDefaults.getBoolData(key: DefaultKeys.IS_CM_VIRTUAL)
            if isVirtualCm {
                //Stop VCMPacket Timer
                let vcmPktManager = VCMPacketManager.sharedInstance
                // vcmPktManager.vcmDelegate = self
                vcmPktManager.invalidateVCMTimer()
                
                
                MyDefaults.setInt(value: 0, key: DefaultKeys.MESSAGE_NUMERATOR)
                vcmPktManager.sendPacketToCloud()
            }
            print("Offline start stop: Repo stop ride API call..")
          //  Rides.setRideAsCompleted(rideID: String(rideId), context: context)
            apiHandler.apiStopRide(rideID: rideId, parameter: parameter)
        }else{
            
        }
    }
    
    func repoGetRideProgress(){
        if NetworkCheck.isNetwork() {
            let bikerId = self.bikerDetails.biker_id?.toInt() ?? 0
            print("Offline start stop: Repo start in progress API call..")
            apiHandler.apiGetProgreesRide(bikerID: bikerId)
        }else{
            
            if let rideDetails = Rides.getMyProgressRide(context: context) {
                print("CurrentRideRepo : Ride id \(String(describing: rideDetails.biker_ID))")
                JourneyManager.sharedInstance.rideIsInProgress(rideDetails: rideDetails)
                var data:[String:Any] = [:]
                data["type"] = RideStateAPIType.RIDE_IN_PROGRESS
                data["data"] = rideDetails
                data["isRideInProgress"] = true
                self.delegate?.repoSuccess(data: data)
            }else{
                var data:[String:Any] = [:]
                data["type"] = RideStateAPIType.RIDE_IN_PROGRESS
                data["isRideInProgress"] = false
                self.delegate?.repoSuccess(data: data)
            }
            
        }
    }
    
    //MARK:- RideStateAPIHandlerDelegate Methods
    
    func rideStateAPISuccess(response: [String : Any]) {
        //
        
        let type:RideStateAPIType = response["type"] as! RideStateAPIType
        let rideId:Int = response["rideID"] as! Int

        switch type {
            
        case .RIDE_START:
            
            print("CurrentRideRepo : Ride start api with id \(rideId) has been synced ------------")
            if oldRideID != "" {
                Rides.updateDummyRideID(oldRideId: oldRideID, newRideID: rideId.toString(), context: context)

            }
            // Do not update db on cloud response
            //Rides.updateRideStatusAfterStartingRide(rideID: rideId.toString() , context: context)
            RideRequest.deletePreviousRequest(rideID: rideId.toString(), type: .Start, context: context)
            RideRequest.updateRideIDOfAdhocRide(oldRideID: oldRideID, rideID: rideId.toString(), context: context)
            guard let rideDetails = Rides.getMyProgressRide(context: context) else {
                return
            }

            print("CurrentRideRepo : Current Ride id is \(String(describing: rideDetails.rideID))")

            let data:[String:Any] = ["type": RideStateAPIType.RIDE_START,
                                     "rideData": rideDetails]

            self.delegate?.repoSuccess(data: data)
            RideRequestHandler.requestHandlerSharedInstance.startExecutingRequestObj()
            break
        
        case .RIDE_STOP:
            print("CurrentRideRepo : Ride Stop api with id \(rideId) is synced ----------")
            self.delegate?.repoSuccess(data: response)
            RideRequest.deletePreviousRequest(rideID: rideId.toString(), type: .Stop, context: context)
            RideRequestHandler.requestHandlerSharedInstance.startExecutingRequestObj()
            VCMPacketManager.sharedInstance.sendPacketToCloud()
            break
            
        case .RIDE_IN_PROGRESS:
            
            print("CurrentRideRepo : Ride Is in Progress")
            
            var data:[String:Any] = [:]
            data["type"] = RideStateAPIType.RIDE_IN_PROGRESS
            
            if let obj:RideInprogressModel = response["data"] as? RideInprogressModel {
                if (obj.rideInprogressList?.count)! > 0 {
                    let rideData:RideCreateResponse = (obj.rideInprogressList?[0])! as RideCreateResponse
                    
                    if rideData.status == 1 {
                        self.rideData = rideData
                        //planRideRepo.planRideRepoDelegate = self
                        planRideRepo.saveRideResponse(rideResponse: rideData, type: "GetRideRouteById", successCallback: { (data) in
                            
                        }) { (error) in
                            
                        }
                        //
                        if let ride = Rides.getMyProgressRide(context: context) {
                            JourneyManager.sharedInstance.rideStarted(rideDetails: ride)
                        }
//                        Rides.insertMyRideCard(rideCreateResponse: rideData, context: context, route: "", tbtRoute: "")
                    }
                    else{
                        // no ride is in progress
                        data["isRideInProgress"] = false

                        JourneyManager.sharedInstance.noRideISInProgress()
                        self.delegate?.repoSuccess(data: data)

                    }
                    
                }else{
                    // No ride in progress
                    data["isRideInProgress"] = false

                    JourneyManager.sharedInstance.noRideISInProgress()
                    self.delegate?.repoSuccess(data: data)

                }
            }
            RideRequestHandler.requestHandlerSharedInstance.startExecutingRequestObj()
            
            break
                
        default:
            break
        }

    }
    
    func rideStateAPIFailed(response: [String : Any]) {
        guard let responseData = response[StringConstant.data] as? [String: Any] else {
            RideRequestHandler.requestHandlerSharedInstance.startExecutingRequestObj()
            return
        }
        guard let responseCode = responseData["error_code"] as? Int else {
            RideRequestHandler.requestHandlerSharedInstance.startExecutingRequestObj()

            return
        }
        print("Ride State API failed with code \(String(describing: responseCode))")
        let rideID = response["rideID"] as! Int
        switch responseCode {
        case RideErrors.ERROR_RIDE_NOT_FOUND:
            RideRequest.deletePreviousRequest(rideID: rideID.toString(), type: response["type"] as! RequestType, context: context)
          //  Rides.deleteRide(rideID: rideID.toString(), context: context)
            // ride id not found
            break
        case RideErrors.ERROR_RIDE_OTHER_STARTED_RIDE_PRESENT:
           // already ride started
            RideRequest.deletePreviousRequest(rideID: rideID.toString(), type: response["type"] as! RequestType, context: context)
            break
        case RideErrors.ERROR_RIDE_NOT_YET_STARTED, RideErrors.ERROR_RIDE_STATUS_NOT_VALID_TO_FINISH:
            // ride not yet started but trying to stop
            RideRequest.deletePreviousRequest(rideID: rideID.toString(), type: response["type"] as! RequestType, context: context)
//            Rides.deleteRide(rideID: rideID.toString(), context: context)
            break
        case RideErrors.ERROR_RIDE_STATUS_NOT_VALID_TO_START, RideErrors.ERROR_RIDE_ALREADY_FINISHED:
            //already stoped aborted cancelled ride
            RideRequest.deletePreviousRequest(rideID: rideID.toString(), type: response["type"] as! RequestType, context: context)

            break
        case RideErrors.ERROR_RIDE_FUTURE_DATE_STARTED_NOT_ALLOWED, RideErrors.ERROR_RIDE_FUTURE_DATE_FINISHED_NOT_ALLOWED, RideErrors.ERROR_RIDE_DATE_STARTED_OLDER_THAN_PLANNED_START_DATE, RideErrors.ERROR_RIDE_DATE_STARTED_GERATER_THAN_PLANNED_FINISH_DATE, RideErrors.ERROR_RIDE_DATE_FINISHED_BEFORE_DATE_STARTED_NOT_ALLOWED:
            // start stop not allowed due to time difference
            break
        default:
            break
        }
        RideRequestHandler.requestHandlerSharedInstance.startExecutingRequestObj()
        self.delegate?.repoFailed(data: response)
    }
    
    func repoSuccessCallback(data: Any) {
        var resData:[String:Any] = [:]
        resData["type"] = RideStateAPIType.RIDE_IN_PROGRESS
        if oldRideID != "" {
            Rides.updateDummyRideID(oldRideId: oldRideID, newRideID: String(self.rideData?.id ?? 0), context: context)
        }
        Rides.updateRideStatusAfterStartingRide(rideID: String(self.rideData?.id ?? 0), context: context)
        
        if let rideDetails = Rides.getMyProgressRide(context: context) {
            print("CurrentRideRepo : Ride id \(rideDetails.rideID ?? "")")
            JourneyManager.sharedInstance.rideIsInProgress(rideDetails: rideDetails)
            resData["data"] = rideDetails
            resData["isRideInProgress"] = true
        }
        self.delegate?.repoSuccess(data: resData)

    }
    
    func repoFailureCallback(error: [String : Any]) {
        RideRequestHandler.requestHandlerSharedInstance.startExecutingRequestObj()
    }
    
}
