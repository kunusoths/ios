//
//  RideRepository.swift
//  Maximus
//
//  Created by Isha Ramdasi on 26/06/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import ObjectMapper
import CoreData

protocol RidesRepoCallback: class {
    func repoFailureCallback(error: [String: Any])
    func repoSuccessCallback(data:Any)
}

protocol RideRepositoryProtocol {
    func getMyPlannedRidesFromRepo(type:String, pageNo: Int, size: Int, bikerID: Int, request: String, successCallback: @escaping successData, failureCallback: @escaping errorData)
    func getAllRidesOfBiker(request: String, pageNo:Int, bikerID: Int, size: Int, successCallback: @escaping successData, failureCallback: @escaping errorData)
    func getRide(withRideID rideId: Int, andBikerId bikerId: Int, request: String, successCallback: @escaping successData, failureCallback: @escaping errorData)
    func editRideDetails(rideID: Int, request: String, successCallback: @escaping successData, failureCallback: @escaping errorData)
    func cloneRide(rideID: Int, data: [String: Any], request: String, successCallback: @escaping successData, failureCallback: @escaping errorData)
    func leaveRide(rideID: Int, request: String, successCallback: @escaping successData, failureCallback: @escaping errorData)
    func cachePOI(rideID: Int, request: String, successCallback: @escaping successData, failureCallback: @escaping errorData)
    func getRideMembers(rideID: Int, request: String, successCallback: @escaping successData, failureCallback: @escaping errorData)
    func joinRide(rideID:Int,parameter: [String:Any], requestType: String, successCallback: @escaping successData, failureCallback: @escaping errorData)
    func rejectRide(rideID:Int,parameter: [String:Any], requestType: String, successCallback: @escaping successData, failureCallback: @escaping errorData)
}

class RideRepository: NSObject, RideRepositoryProtocol {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    weak var repoDelegate: RidesRepoCallback?
    var completedRides:[Rides] = []
    let apiManager = RidesApiManager()
    var currentRideID = 0
    
    override init() {
        super.init()
        //apiManager.cloudCallbackDelegate = nil
        //apiManager.cloudCallbackDelegate = self
    }
    
    func getMyPlannedRidesFromRepo(type:String, pageNo: Int, size: Int, bikerID: Int, request: String, successCallback: @escaping successData, failureCallback: @escaping errorData)
    {
        if NetworkCheck.isNetwork() {
            apiManager.getMyPlannedRides(type: type, pageNo: pageNo, size: size, bikerID: String(bikerID), request: request, successCallback: { data in
                let user = Mapper<RideCreateResponse>().mapArray(JSONString: data as! String)
                var data:[String:Any] = [StringConstant.data:user ?? ""]
                data["RequestType"] = request
                successCallback(data)
            }, failureCallback: {errorData in
                let ErrorModelObj = Mapper<ErrorModel>().map(JSONString: errorData as! String)
                let errorDataDict:[String:Any] = ["data":ErrorModelObj ?? " "]
                failureCallback(errorDataDict)
            })
        } else {
            completedRides = Rides.getNotCompletedMyRideObjs(context: context)
            var dict:[String: Any] = [:]
            dict["RequestType"] = "GetPlannedRides"
            dict["data"] = completedRides
             dict["isLastPage"] = true
            self.repoDelegate?.repoSuccessCallback(data: dict)
        }
    }
    
    func getAllRidesOfBiker(request: String, pageNo:Int, bikerID: Int, size: Int, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        if NetworkCheck.isNetwork() {
            apiManager.getAllRidesOfBiker(bikerID: bikerID, requestType: request, pageNo: pageNo, size: size, successCallback: {responseData in
                let parsedData = self.parseRideResponse(data: responseData as! String, requestType: request)
                successCallback(parsedData)
            }, failureCallback: {errorData in
                var data:[String:Any] = [StringConstant.data:errorData]
                data["RequestType"] = request
                failureCallback(data)
            })
        } else {
            completedRides = Rides.getNotCompletedMyRideObjs(context: context)
            var dict:[String: Any] = [:]
            dict["RequestType"] = "GetPlannedRides"
            dict["data"] = completedRides

             dict["isLastPage"] = true
            successCallback(dict)
        }
    }
    
    func getRide(withRideID rideId: Int, andBikerId bikerId: Int, request: String, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        if NetworkCheck.isNetwork() {
            self.apiManager.getRideOfBiker(rideID: rideId, bikerId: bikerId, requestType: request, successCallback: {responseData in
                let parsedData = self.parseRideOfBikerResponse(responseData: responseData as! [String : Any], requestType: request)
                successCallback(parsedData)
            }, failureCallback: {errorData in
                var data:[String:Any] = [StringConstant.data:(errorData)]
                data["RequestType"] = request
                failureCallback(data)
            })
        } else {
            
        }
    }
    
    func getRidesFromDB() -> [Rides] {
        let ridesArray = Rides.getAllRidesDetails(context: context)
        return ridesArray
    }
    
    func editRideDetails(rideID: Int, request: String, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        if NetworkCheck.isNetwork() {
            apiManager.getRide(rideID: rideID, request: request, successCallback: {responseData in
                let bikes = Mapper<RideCreateResponse>().map(JSON:responseData as! [String : Any])!
                var data:[String:Any] = [StringConstant.data:bikes]
                data["RequestType"] = request
                let parsedData = self.parseRideDetails(responseData: data, requestType: request)
                successCallback(parsedData)
            }, failureCallback: {errorData in
                var data:[String:Any] = [StringConstant.data:(errorData)]
                data["RequestType"] = request
                failureCallback(data)
            })
        } else {
            //failure
            var error: [String: Any] = [:]
            error["description"] = ToastMsgConstant.network
            //self.repoDelegate?.repoFailureCallback(error: error)
            failureCallback(error)
        }
    }
    
    func cloneRide(rideID: Int, data: [String: Any], request: String, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        if NetworkCheck.isNetwork() {
            apiManager.cloneRide(rideID: rideID, data: data, request: request, successCallback: {responseData in
                let cloneRide = Mapper<RideCreateResponse>().map(JSON:responseData as! [String : Any])!
                var data:[String:Any] = [StringConstant.data:cloneRide]
                data["RequestType"] = request
                let parsedData = self.parseCloneRideResponse(responseData: data, requestType: request)
                successCallback(parsedData)
            }, failureCallback: {errorData in
                var data:[String:Any] = [StringConstant.data:(errorData)]
                data["RequestType"] = request
                failureCallback(data)
            })
        } else {
            var error: [String: Any] = [:]
            error["description"] = ToastMsgConstant.network
//            self.repoDelegate?.repoFailureCallback(error: error)
            failureCallback(error)
        }
    }
    
    func leaveRide(rideID: Int, request: String, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        if NetworkCheck.isNetwork() {
            apiManager.leaveRide(rideID: rideID, request: request, successCallback: {responseData in
                var data: [String: Any] = [:]
                data["RequestType"] = request
                data["rideID"] = rideID
                let parsedData = self.parseLeaveRide(data: data, request: request)
                successCallback(parsedData)
            }, failureCallback: {errorData in
                var data:[String:Any] = [StringConstant.data:errorData]
                data["RequestType"] = request
                failureCallback(data)
            })
        } else {
            var error: [String: Any] = [:]
            error["description"] = ToastMsgConstant.network
//            self.repoDelegate?.repoFailureCallback(error: error)
            failureCallback(error)
        }
    }
    
    func cachePOI(rideID: Int, request: String, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        if NetworkCheck.isNetwork() {
            apiManager.getPOI(rideID: rideID, isInsertintoDB: true, request: request, successCallback: {data in
                MyDefaults.setBoolData(value: true, key: "isPOICached")
                successCallback(Data())
            }, failureCallback: {errorData in
                failureCallback(errorData)
            })
        } else {
            var error: [String: Any] = [:]
            error["description"] = ToastMsgConstant.network
//            self.repoDelegate?.repoFailureCallback(error: error)
            failureCallback(error)
        }
    }
    
    func getRideMembers(rideID: Int, request: String, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        currentRideID = rideID
        if NetworkCheck.isNetwork() {
            apiManager.getRideMembers(rideID: rideID, request: request, successCallback: {data in
                let parsedData = self.parseRideMember(data: data as! NSString, request: request)
                successCallback(parsedData)
            }, failureCallback: {errorData in
                failureCallback(errorData)
            })
        } else {
            let list = Rides.getRideMembers(rideID: String(rideID), context: context)
//            self.repoDelegate?.repoSuccessCallback(data: list)
            var data:[String:Any] = [StringConstant.data:list]
            data["RequestType"] = request
            successCallback(list)
        }
    }
    
    func cachingPOISuccess() {
        MyDefaults.setBoolData(value: true, key: "isPOICached")
        self.repoDelegate?.repoSuccessCallback(data: Data())
    }
    
    func joinRide(rideID:Int,parameter: [String:Any], requestType: String, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        apiManager.joinRide(rideID: rideID, parameter: parameter, requestType: requestType, successCallback: {responseData in
            let user = Mapper<RideStartResponse>().map(JSON:responseData as! [String : Any])!
            var data:[String:Any] = [StringConstant.data:user]
            data["RequestType"] = requestType
            successCallback(data)
        }, failureCallback: {errorData in
            var data:[String:Any] = [StringConstant.data:errorData]
            data["RequestType"] = requestType
            failureCallback(data)
        })
    }
    
    func rejectRide(rideID:Int,parameter: [String:Any], requestType: String, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        apiManager.rejectRide(rideID: rideID, parameter: parameter, requestType: requestType, successCallback: {responseData in
            let user = Mapper<RideStartResponse>().map(JSON:responseData as! [String : Any])!
            var data:[String:Any] = [StringConstant.data:user]
            data["RequestType"] = requestType
            successCallback(data)
        }, failureCallback: {errorData in
            var data:[String:Any] = [StringConstant.data:errorData]
            data["RequestType"] = requestType
            failureCallback(data)
        })
    }
    
    
    func parseRideResponse(data:String, requestType: String) -> [String: Any] {
        let user = Mapper<RideCreateResponse>().mapArray(JSONString: data as! String)
        var data:[String:Any] = [StringConstant.data:user ?? []]
        data["RequestType"] = requestType
        if data["RequestType"] as? String == "GetPlannedRides" {
            if let ridesArray = data["data"] as? [RideCreateResponse] {
                for i in 0..<ridesArray.count {
                    if ridesArray[i].status != 2 {
                        Rides.insertMyRideCard(rideCreateResponse: ridesArray[i], context: context, route: "", tbtRoute: "")
                    }
                }
            }
        }
            completedRides = Rides.getNotCompletedMyRideObjs(context: context)
            var dict:[String: Any] = [:]
            dict["RequestType"] = requestType
            dict["data"] = completedRides
            dict["isLastPage"] = (data["data"] as? [RideCreateResponse])?.count == 0 ? true : false

//            self.repoDelegate?.repoSuccessCallback(data: dict)
            return dict
        
    }
    
    func parseRideOfBikerResponse(responseData: [String: Any], requestType: String) -> [String: Any] {
        let directionDetails = Mapper<RideCreateResponse>().map(JSON: responseData)
        var data:[String:Any] = [StringConstant.data:directionDetails!]
        data["RequestType"] = requestType
        if data["RequestType"] as? String == "GetRideOfBiker" {
            //update ride details
            if let rideResponse = data["data"] as? RideCreateResponse {
                Rides.insertMyRideCard(rideCreateResponse: rideResponse, context: context, route: "", tbtRoute: "")
            }
        }
        return data
    }
    
    func parseRideDetails(responseData: [String: Any], requestType: String) -> [String: Any] {
        if responseData["RequestType"] as? String == "GetRideDetails" {
            if let rideResponse = responseData["data"] as? RideCreateResponse {
                Rides.insertMyRideCard(rideCreateResponse: rideResponse, context: context, route: "", tbtRoute: "")
            }
        }
        return responseData
    }
    
    func parseCloneRideResponse(responseData: [String: Any], requestType: String) -> [String: Any] {
        if responseData["RequestType"] as? String == "CloneRide" {
            if let rideResponse = responseData["data"] as? RideCreateResponse {
                Rides.insertMyRideCard(rideCreateResponse: rideResponse, context: context, route: "", tbtRoute: "")
            }
        }
        return responseData
    }
    
    
    func parseLeaveRide(data: [String: Any], request: String) -> [String: Any] {
        if data["RequestType"] as? String == "LeaveRide", let rideID = data["rideID"] as? Int {
            Rides.deleteRide(rideID: String(rideID), context: context)
        }
        return data
    }
    
    func parseRideMember(data: NSString, request: String) -> [RideMemberDetails] {
        if let rideMemberlst = Mapper<RideMember>().mapArray(JSONString: data as String) {
            var data:[String:Any] = [StringConstant.data:rideMemberlst]
            data["RequestType"] = request
            if let memberList = data["data"] as? [RideMember] {
                Rides.addRideMemberDetails(rideID: String(currentRideID), context: context, obj: memberList)
            }
        }
        let list = Rides.getRideMembers(rideID: String(currentRideID), context: context)
//        self.repoDelegate?.repoSuccessCallback(data: list)
        return list
    }

    func cloudSuccess(data: [String : Any]) {
        if data["RequestType"] as? String == "GetPlannedRides" {
            if let ridesArray = data["data"] as? [RideCreateResponse] {
                for i in 0..<ridesArray.count {
                    print("Ride Title before inserting: \(ridesArray[i].title)")
                    Rides.insertMyRideCard(rideCreateResponse: ridesArray[i], context: context, route: "", tbtRoute: "")
                }
            }
            completedRides = Rides.getNotCompletedMyRideObjs(context: context)
            var dict:[String: Any] = [:]
            dict["RequestType"] = data["RequestType"]
            dict["data"] = completedRides
            dict["isLastPage"] = (data["data"] as? [RideCreateResponse])?.count == 0 ? true : false
            self.repoDelegate?.repoSuccessCallback(data: dict)
        } else if data["RequestType"] as? String == "CloneRide" {
            if let rideResponse = data["data"] as? RideCreateResponse {
                Rides.insertMyRideCard(rideCreateResponse: rideResponse, context: context, route: "", tbtRoute: "")
            }
            self.repoDelegate?.repoSuccessCallback(data: data)
        } else if data["RequestType"] as? String == "LeaveRide", let rideID = data["rideID"] as? Int {
                Rides.deleteRide(rideID: String(rideID), context: context)
                self.repoDelegate?.repoSuccessCallback(data: data)
        } else if data["RequestType"] as? String == "GetRideMembers" {
            if let memberList = data["data"] as? [RideMember] {
                Rides.addRideMemberDetails(rideID: String(currentRideID), context: context, obj: memberList)
            }
            let list = Rides.getRideMembers(rideID: String(currentRideID), context: context)
            self.repoDelegate?.repoSuccessCallback(data: list)
        } else if data["RequestType"] as? String == "GetRideDetails" {
            if let rideResponse = data["data"] as? RideCreateResponse {
                Rides.insertMyRideCard(rideCreateResponse: rideResponse, context: context, route: "", tbtRoute: "")
            }
            self.repoDelegate?.repoSuccessCallback(data: data)
        }else if data["RequestType"] as? String == "SaveRide"{
            
            if let rideResponse = data["data"] as? RideCreateResponse {
                Rides.insertMyRideCard(rideCreateResponse: rideResponse, context: context, route: "", tbtRoute: "")
            }
            self.repoDelegate?.repoSuccessCallback(data: data)
            
        } else if data["RequestType"] as? String == "JoinRide"{
            self.repoDelegate?.repoSuccessCallback(data: data)
        } else if data["RequestType"] as? String == "GetRideOfBiker" {
            //update ride details
            if let rideResponse = data["data"] as? RideCreateResponse {
                Rides.insertMyRideCard(rideCreateResponse: rideResponse, context: context, route: "", tbtRoute: "")
            }
            self.repoDelegate?.repoSuccessCallback(data: data)
        }
    }
    
    func cloudFailure(error: [String : Any]) {
        self.repoDelegate?.repoFailureCallback(error: error)
    }

}
