//
//  StepNotStarted.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 13/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class StepNotStarted: StepState {

   
    func isStepStarted(context: StepContext) -> Bool{
        return true
    }
    
    
    func isStepInProgress(context: StepContext) -> Bool{
        return false
    }
    
    
    func isStepCompleted(context: StepContext) -> Bool{
        return false
    }
    
    func isStepSkipped(context: StepContext) -> Bool {
        return false
    }
}
