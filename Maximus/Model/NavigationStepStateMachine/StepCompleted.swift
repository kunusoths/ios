//
//  StepCompleted.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 13/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class StepCompleted: StepState {
    let stepDetail:NSStepProgress
    let sinkManager = SinkManager.sharedInstance
    
    init(stepDetail:NSStepProgress) {
        self.stepDetail = stepDetail
        self.notifyStepCompleted()
    }
    
    private func notifyStepCompleted(){
        self.sinkManager.notifyObservers(notificationKey: sinkManager.SINK_NOTIFICATION_STEP_COMPLETED, sender: self)
    }
    
    func isStepStarted(context: StepContext) -> Bool{
        return false
    }
    
    
    func isStepInProgress(context: StepContext) -> Bool{
        return false
    }
    
    
    func isStepCompleted(context: StepContext) -> Bool{
        return true
    }
    
    func isStepSkipped(context: StepContext) -> Bool {
        return false
    }
    
}
