//
//  StepContext.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 13/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation

final class StepContext {
    
    private var state: StepState = StepNotStarted()
    

    func changeStateToStepStarted(stepDetail:NSStepProgress){
        state = StepStarted(stepDetail: stepDetail)
    }
    
    func changeStateToStepCompleted(stepDetail:NSStepProgress){
        state = StepCompleted(stepDetail: stepDetail)
    }
    
    func changeStateToStepInProgress(stepDetail:NSStepProgress){
        state = StepInProgress(stepDetail: stepDetail)
    }
    
    func changeStateToStepSkipped(stepDetail:NSStepProgress){
        state =  StepSkipped(stepDetail: stepDetail)
    }
    
}

protocol StepState {
    
    func isStepStarted(context: StepContext) -> Bool
    func isStepInProgress(context: StepContext) -> Bool
    func isStepCompleted(context: StepContext) -> Bool
    func isStepSkipped(context: StepContext) -> Bool
    
}
