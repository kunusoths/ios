//
//  StepStarted.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 13/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class StepStarted: StepState {
    
    let stepDetail:NSStepProgress
    let sinkManager = SinkManager.sharedInstance
    
    init(stepDetail:NSStepProgress) {
        self.stepDetail = stepDetail
        self.notifyStepStarted()
    }
    
    private func notifyStepStarted(){
        
       // MaximusDM.sharedInstance.tbtHideShowTbt(hideShow: Int(TBT_ON.rawValue))
        self.sinkManager.notifyObservers(notificationKey: sinkManager.SINK_NOTIFICATION_STEP_STARTED, sender: self)
    }
    
    func isStepStarted(context: StepContext) -> Bool{
        return true
    }
    
    
    func isStepInProgress(context: StepContext) -> Bool{
        return false
    }
    
    
    func isStepCompleted(context: StepContext) -> Bool{
        return false
    }
    
    func isStepSkipped(context: StepContext) -> Bool {
        return false
    }
}
