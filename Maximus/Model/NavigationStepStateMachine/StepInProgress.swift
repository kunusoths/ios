//
//  StepInProgress.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 13/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class StepInProgress: StepState {
    
    let stepDetail:NSStepProgress
    let sinkManager = SinkManager.sharedInstance
    let maximusDm = MaximusDM.sharedInstance
    
    var prevCurrentIcon:Int = 555
    var prevNextIcon:Int = 555
    var isTbtShown = true
    
    init(stepDetail:NSStepProgress) {
        self.stepDetail = stepDetail
        self.notifyStepInProgress()
    }
    
    private func notifyStepInProgress(){
        let stepRemainingDistance = self.stepDetail.getDistanceRemaining()?.getMeters()
                
        self.sinkManager.notifyObservers(notificationKey: sinkManager.SINK_NOTIFICATION_STEP_IN_PROGRESS, sender: self, data:["stepRemainingDistance": stepRemainingDistance ?? 0])
        
    }
    
    func isStepStarted(context: StepContext) -> Bool{
        return false
    }
    
    
    func isStepInProgress(context: StepContext) -> Bool{
        return true
    }
    
    
    func isStepCompleted(context: StepContext) -> Bool{
        return false
    }
    
    func isStepSkipped(context: StepContext) -> Bool {
        return false
    }

}
