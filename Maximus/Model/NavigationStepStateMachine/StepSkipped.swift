//
//  StepSkipped.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 07/09/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import Foundation

class StepSkipped: StepState {
    let stepDetail:NSStepProgress
    let sinkManager = SinkManager.sharedInstance
    
    init(stepDetail:NSStepProgress) {
        self.stepDetail = stepDetail
        self.notifyStepSkipped()
    }
    
    private func notifyStepSkipped(){
        self.sinkManager.notifyObservers(notificationKey: sinkManager.SINK_NOTIFICATION_STEP_SKIPPED, sender: self)
    }
    
    func isStepStarted(context: StepContext) -> Bool{
        return false
    }
    
    
    func isStepInProgress(context: StepContext) -> Bool{
        return false
    }
    
    
    func isStepCompleted(context: StepContext) -> Bool{
        return false
    }
    
    func isStepSkipped(context: StepContext) -> Bool {
        return true
    }
    
}
