//
//  ProcessHeading.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 12/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation

class ProcessHeading:NSObject {
    
    var oldHeadingAngle     = -1;
    var oldHeadingInfo      = "";
    var oldHeadDestination  = "";
    var oldDistance:Int64   = 0;
    
    
    ///Singleton instance of Class
    private static var privateShared : ProcessHeading?
    class func sharedInstance() -> ProcessHeading {
        guard let shared = privateShared else {
            privateShared = ProcessHeading()
            return privateShared!
        }
        return shared
    }
    
    ///Shared Instance Released here
    class func destroyInstance() {
        privateShared = nil
    }

    
    
    func notifyHeading(headStatus: NSStatus) {
        
        var destName = ""
        if let name = headStatus.getNextStep()?.getStreetName() {
            destName = name
        }
        
        let dist = headStatus.getManeuver()?.getPoint()?.getDistance()?.getMeters()

        let headType:NSManeuverType = (headStatus.getManeuver()?.getType())!
        var headStr = ""
        
        switch (headType) {
            
            /*   # Cardinal Direction */
            
        case .NAVIGATORMANEUVERHEADNORTH:
            headStr = "North"          //icon_to_be_added
            break;
            
        case .NAVIGATORMANEUVERHEADEAST:
            headStr = "East"          //icon_to_be_added
            break;
            
        case .NAVIGATORMANEUVERHEADSOUTH:
            headStr = "South"          //icon_to_be_added
            break;
            
        case .NAVIGATORMANEUVERHEADWEST:
            headStr = "West"           ////icon_to_be_added
            break;

            
            /*   # Intercardinal Direction	 */
            
        case .NAVIGATORMANEUVERHEADNORTHEAST:
            headStr = "North East"      //icon_to_be_added
            break;
            
        case .NAVIGATORMANEUVERHEADNORTHWEST:
            headStr = "North West"      //icon_to_be_added
            break;
            
            
        case .NAVIGATORMANEUVERHEADSOUTHEAST:
            headStr = "South East"      //icon_to_be_added
            break;
            
        case .NAVIGATORMANEUVERHEADSOUTHWEST:
            headStr = "South West"      //icon_to_be_added
            break;
            
            
            /*   # Intercardinal Divisions	 */
            
        case .NAVIGATORMANEUVERHEADNORTHNORTHEAST:
            headStr = "North North East"     //icon_to_be_added
            break;
            
        case .NAVIGATORMANEUVERHEADNORTHNORTHWEST:
            headStr = "North North West"     //icon_to_be_added
            break;
            
        case .NAVIGATORMANEUVERHEADEASTNORTHEAST:
            headStr = "East North East"      //icon_to_be_added
            break;
            
        case .NAVIGATORMANEUVERHEADEASTSOUTHEAST:
            headStr = "East South East"      //icon_to_be_added
            break;
            
        case .NAVIGATORMANEUVERHEADSOUTHSOUTHEAST:
            headStr = "South South East"     //icon_to_be_added
            break;
            
        case .NAVIGATORMANEUVERHEADSOUTHSOUTHWEST:
            headStr = "South South West"     //icon_to_be_added
            break;
            
        case .NAVIGATORMANEUVERHEADWESTSOUTHWEST:
            headStr = "West South West"      //icon_to_be_added
            break;
            
        case .NAVIGATORMANEUVERHEADWESTNORTHWEST:
            headStr = "West North West"      //icon_to_be_added
            break;
        
        default:
            headStr = "\(headType.rawValue)"
            break
            
        }
        
        ///Send Heading Angle
        if headStatus.hasRelativeBearing() && !headStatus.getRelativeBearing().isNaN{
            let headingAngle:Int = Int(headStatus.getRelativeBearing())
            MaximusDM.sharedInstance.sendHeadingAngle(angle: headingAngle)
            oldHeadingAngle = headingAngle
        }
        
        ///Send current heading string
        
        if headStr != oldHeadingInfo || headStr.isBlank {
            MaximusDM.sharedInstance.tbtHeadingDirection(nextTurn: headStr)
            oldHeadingInfo = headStr
        }

        ///Send current heading to Destination string
        if destName != oldHeadDestination || destName.isBlank {
            MaximusDM.sharedInstance.tbtHeadingDestination(destination: destName)
            oldHeadDestination = destName
        }

        ///Send Distance towords Destination
        if let distance = dist {
            if distance != oldDistance {
                MaximusDM.sharedInstance.tbtHeadingDistance(distance: Int(distance))
                oldDistance  = distance
            }
        }
        ///Send Heading Start/Stop
        MaximusDM.sharedInstance.tbtHeadingStart(startStatus: Int(START.rawValue))
     
    }
    
    func clearOldHeadingInstructions(){
        /* Clear info */
        
        oldHeadingAngle     = -1;
        oldHeadingInfo      = "";
        oldHeadDestination  = "";
        oldDistance         = 0;
    }
    

}
