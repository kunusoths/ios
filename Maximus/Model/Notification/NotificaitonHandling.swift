//
//  NotificaitonHandling.swift
//  Maximus
//
//  Created by Sriram G on 14/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import UserNotifications

class NotificationHandler:NSObject
{
    
    // MARK:LOCAL NOTIFICATION
    /*
    func authoriseLocalNotification()
    {
        UNUserNotificationCenter.current().getNotificationSettings { (notificationSettings) in
            switch notificationSettings.authorizationStatus {
            case .notDetermined:
                self.requestAuthorization(completionHandler: { (success) in
                    guard success else { return }
                    
                    self.scheduleOtpNotification()
                })
            case .authorized:
                self.scheduleOtpNotification()
            case .denied:
                print("Application Not Allowed to Display Local OTP Notifications")
            }
        }
        
    }
    
    private func requestAuthorization(completionHandler: @escaping (_ success: Bool) -> ()) {
        // Request Authorization
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
            if let error = error {
                print("Request Authorization Failed (\(error), \(error.localizedDescription))")
            }
            
            completionHandler(success)
        }
    }
    
    func scheduleOtpNotification()
    {
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = "Maximus Pro"
        notificationContent.subtitle = ""
        notificationContent.body = "Please verify your phone number to start getting theft alerts and location tracking alerts."
        notificationContent.categoryIdentifier = "otp_notificationCategory"
        notificationContent.userInfo = ["type":"otp_notificationCategory"]
        
        // Add Trigger
        let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 1.0, repeats: false)
        
        // Create Notification Request
        let notificationRequest = UNNotificationRequest(identifier: "Otp_local_notification", content: notificationContent, trigger: notificationTrigger)
        
        // Add Request to User Notification Center
        UNUserNotificationCenter.current().add(notificationRequest) { (error) in
            if let error = error {
                print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
            }
        }
    }
    
    func scheduleEmergencyContactNotification()
    {
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = "Maximus Pro"
        notificationContent.subtitle = ""
        notificationContent.body = "Please add your emergency contacts."
        notificationContent.categoryIdentifier = "emergencyContact_notificationCategory"
        notificationContent.userInfo = ["type":"emergencyContact_notificationCategory"]
        
        // Add Trigger
        var datematch = DateComponents()
        datematch.hour = 10
        let notificationTrigger = UNCalendarNotificationTrigger(dateMatching: datematch, repeats: true)
        notificationTrigger.nextTriggerDate() // "Jan 4, 2017, 12:00 PM"
        
        // Create Notification Request
        let notificationRequest = UNNotificationRequest(identifier: "emergencyContact_notification", content: notificationContent, trigger: notificationTrigger)
        
        // Add Request to User Notification Center
        UNUserNotificationCenter.current().add(notificationRequest) { (error) in
            if let error = error {
                print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
            }
        }
    }
    */
}
