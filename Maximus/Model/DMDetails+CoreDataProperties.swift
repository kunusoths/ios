//
//  DMDetails+CoreDataProperties.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 05/10/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//
//

import Foundation
import CoreData


extension DMDetails {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DMDetails> {
        return NSFetchRequest<DMDetails>(entityName: "DMDetails")
    }

    @NSManaged public var firmware_build_number: String?
    @NSManaged public var firmware_version: String?
    @NSManaged public var isSynced: Bool
    @NSManaged public var sr_no: String?

}
