//
//  RideRequest+CoreDataProperties.swift
//  Maximus
//
//  Created by Isha Ramdasi on 30/07/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//
//

import Foundation
import CoreData


extension RideRequest {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RideRequest> {
        return NSFetchRequest<RideRequest>(entityName: "RideRequest")
    }

    @NSManaged public var iD: Int64
    @NSManaged public var rideID: String?
    @NSManaged public var rideType: String?
    @NSManaged public var isRideStopped: Bool
    @NSManaged public var createdTime: String?
    @NSManaged public var requestType: String?
    @NSManaged public var requestJson: String?
    @NSManaged public var placeholder: String?

}

extension RideRequest {
    class func createRequestObj(rideID: String, rideType: RideType, requestType: RequestType, requestJsonString: String, context: NSManagedObjectContext) {
        
        let objID = MyDefaults.getData(key: DefaultKeys.KEY_REQUEST_OBJ_ID) as? String
        let integerValue: Int64 = Int64(objID ?? "")!
      //  let offset = MyDefaults.getInt(key: DefaultKeys.KEY_SERVER_OFFSET_TIME)
        let date = Date().addingTimeInterval(-2)
        MyDefaults.setData(value:String(integerValue+1) , key: DefaultKeys.KEY_REQUEST_OBJ_ID)
        let rideRequestObj = RideRequest(context: context)
        rideRequestObj.rideID = rideID == "" ? MyDefaults.getData(key: DefaultKeys.KEY_REQUEST_OBJ_ID) as? String : rideID
        rideRequestObj.rideType = rideType.rawValue
        rideRequestObj.iD = Int64((MyDefaults.getData(key: DefaultKeys.KEY_REQUEST_OBJ_ID) as? String)!) ?? 0
        rideRequestObj.requestType = requestType.rawValue
        rideRequestObj.requestJson = requestJsonString
        rideRequestObj.isRideStopped = false
        rideRequestObj.createdTime = date.dateToStringInUTC()
        
        if requestType == .Stop {
            self.updateRequestObjIfRideIsStopped(rideID: rideID, rideType: .Start, context: context)
        }
        do{
            try context.save()
        } catch {
            
        }
    }
    
    class func isRideRequestPresentInDB(rideId: String, rideType: RequestType, context: NSManagedObjectContext) -> Bool {
        let fetchRq = NSFetchRequest<RideRequest>(entityName: "RideRequest")
        let type = rideType.rawValue
        let fetchpredicate = NSPredicate(format: "rideID == %@ && requestType == %@", rideId, type)
        fetchRq.predicate = fetchpredicate
        do {
            let rideRequest = try context.fetch(fetchRq)
                if rideRequest.count > 0{
                    return true
                }
            
        } catch {
            
        }
        return false
    }
    
    class func updateRequestObjIfRideIsStopped(rideID: String, rideType: RequestType, context: NSManagedObjectContext) {
        if self.isRideRequestPresentInDB(rideId: rideID, rideType: rideType, context: context) {
            print("Updating ride start request with id \(rideID)")
            let rideRequest = self.fetchStartRequestObj(rideID: rideID, rideType: rideType, context: context)
            rideRequest?.isRideStopped = true
            do{
                try context.save()
            } catch {
                
            }
        } else {
             print(" No start request is present with id \(rideID)")
        }

    }
    
    class func fetchStartRequestObj(rideID: String, rideType: RequestType, context: NSManagedObjectContext) -> RideRequest? {
        let fetchRq = NSFetchRequest<RideRequest>(entityName: "RideRequest")
        let fetchpredicate = NSPredicate(format: "rideID == %@ && requestType == %@", rideID, rideType.rawValue)
        fetchRq.predicate = fetchpredicate
        do {
            let rideRequest = try context.fetch(fetchRq)
            if rideRequest.count > 0{
                return rideRequest.first
            }
        } catch {
            
        }
        return nil
    }
    
    class func fetchAllRideRequestObjs(context: NSManagedObjectContext) -> [RideRequest]? {
        let fetchRq = NSFetchRequest<RideRequest>(entityName: "RideRequest")
        do {
            let rideRequest = try context.fetch(fetchRq)
            if rideRequest.count > 0{
                return rideRequest
            }
        } catch {
            
        }
        return nil
    }
    
    class func updateRideIDOfAdhocRide(oldRideID: String, rideID: String, context: NSManagedObjectContext) {
        let fetchRq = NSFetchRequest<RideRequest>(entityName: "RideRequest")
        let fetchpredicate = NSPredicate(format: "rideID == %@", oldRideID)
        fetchRq.predicate = fetchpredicate
        do {
            let rideRequest = try context.fetch(fetchRq)
            if rideRequest.count > 0 {
                for rideObj in rideRequest {
                        rideObj.rideID = rideID
                    do {
                        try context.save()
                    } catch {
                        
                    }
                }
            }
            
        } catch {
            
        }
        
    }
    
    class func deletePreviousRequest(rideID: String, type: RequestType, context: NSManagedObjectContext) {
        let fetchRq = NSFetchRequest<RideRequest>(entityName: "RideRequest")
        let fetchpredicate = NSPredicate(format: "rideID == %@ && requestType == %@", rideID, type.rawValue)
        fetchRq.predicate = fetchpredicate
        do {
            let rideRequest = try context.fetch(fetchRq)
            if rideRequest.count > 0{
                context.delete(rideRequest.first!)
            }
        } catch {
            
        }
        do {
            try context.save()
        } catch {
            
        }
    }
    
    class func deleteRequest(context: NSManagedObjectContext) {
        let fetchRq = NSFetchRequest<RideRequest>(entityName: "RideRequest")
        do {
            let rideRequest = try context.fetch(fetchRq)
            if rideRequest.count > 0{
                context.delete(rideRequest.first!)
            }
        } catch {
            
        }
        do {
            try context.save()
        } catch {
            
        }
    }
    
    class func deleteAlRideRequest(context: NSManagedObjectContext) {
        let fetchRq = NSFetchRequest<RideRequest>(entityName: "RideRequest")
        do {
            let rideRequest = try context.fetch(fetchRq)
            for ride in rideRequest {
                    context.delete(ride)
            }
        } catch {
            
        }
        do {
            try context.save()
        } catch {
            
        }
    }
    
    class func fetchRideRequestForRideId(rideID: String, type: RequestType, context: NSManagedObjectContext) -> Bool {
        let fetchRq = NSFetchRequest<RideRequest>(entityName: "RideRequest")
        let fetchpredicate = NSPredicate(format: "rideID == %@ && requestType == %@", rideID, type.rawValue)
        fetchRq.predicate = fetchpredicate
        do {
            let rideRequest = try context.fetch(fetchRq)
            if rideRequest.count > 0{
                return true
            }
        } catch {
            
        }
        return false
    }
}
