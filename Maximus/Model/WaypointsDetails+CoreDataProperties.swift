//
//  WaypointsDetails+CoreDataProperties.swift
//  Maximus
//
//  Created by Isha Ramdasi on 26/06/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//
//

import Foundation
import CoreData


extension WaypointsDetails {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WaypointsDetails> {
        return NSFetchRequest<WaypointsDetails>(entityName: "WaypointsDetails")
    }

    @NSManaged public var date_added: String?
    @NSManaged public var poi_id: String?
    @NSManaged public var id: String?
    @NSManaged public var ride_id: String?
    @NSManaged public var route_order: String?
    @NSManaged public var categories: NSObject?
    @NSManaged public var types: NSObject?
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var name: String?

}
