//
//  RequestHeaders.swift
//  Maximus
//
//  Created by Admin on 20/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation

//MARK: ******* Headers for Request *********
class RequestHeader {
    class func getHeadersWithouthAuth()-> [String:String]
    {
        let headers = [
            "Accept"        : "application/json",
            "Content-Type"  : "application/json"
        ]
        return headers
    }
    
    class func getHeadersWithAuth()-> [String:String]
    {
        let headers = [
            "Accept"        : "application/json",
            "Content-Type"  : "application/json",
            "Authorization" : NSString(format: "Bearer %@", MyDefaults.getData(key: DefaultKeys.KEY_TOKEN) as! CVarArg)
        ]
        return headers as [String : String]
    }
    
    class func getHeaderForFormData() -> [String:String]
    {
        let headers = [
            "Content-Disposition": "form-data",
            "Authorization" : NSString(format: "Bearer %@", MyDefaults.getData(key: DefaultKeys.KEY_TOKEN) as! CVarArg)
        ]
        
        return headers as [String : String]
    }
    
}
