//
//  CloudManager.swift
//  Maximus
//
//  Created by Admin on 14/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import Alamofire
import OAuthSwift
import ObjectMapper

class CloudManager:OAuthSwiftCredentialHeadersFactory{
    
    let errorResponse:DataResponse<Any>? = nil
    let stringErrorResponse: DataResponse<String>? = nil
    
    var accessToken: String?
    var oauthswift: OAuth2Swift?
    
    func makeCloudRequest(url:String,parameter:[String:Any],requestType:HTTPMethod ,completionHandler: @escaping (DataResponse<Any>?) -> Void ){
        var method:OAuthSwiftHTTPRequest.Method?
        var parameters:[String:Any] = parameter
        if requestType == .post{
            method = .POST
        }else if requestType == .get{
            method = .GET
            parameters = [:]
        }else if requestType == .put{
            method = .PUT
        }
        self.oauthswift = OAuth2Swift(
            consumerKey: StringConstant.oauth_client_id,         // [1] Enter google app settings
            consumerSecret: KeychainService.shared[KEY_OAUTH.KEY_NAME_OAUTH_CLIENT_SECRET]!,        // No secret required
            authorizeUrl: RequestURL.URL_OAUTH_ACCESS_TOKEN.rawValue,
            accessTokenUrl: RequestURL.URL_OAUTH_ACCESS_TOKEN.rawValue,
            responseType: StringConstant.token
        )
        if NetworkCheck.isNetwork() {
            let queue = DispatchQueue(label: "com.test.api", qos: .background, attributes: .concurrent)
            queue.async {
                self.oauthswift!.client.credential.oauthRefreshToken = MyDefaults.getData(key: DefaultKeys.KEY_REFRESH_TOKEN) as! String
                self.oauthswift!.client.credential.headersFactory = self
                self.oauthswift!.startAuthorizedRequest(url, method: method!, parameters: parameters,headers:nil,renewHeaders:[:],onTokenRenewal: { (credential) in
                        MyDefaults.setData(value: credential.oauthToken, key: DefaultKeys.KEY_TOKEN)
                        MyDefaults.setData(value:credential.oauthRefreshToken,key:DefaultKeys.KEY_REFRESH_TOKEN)
                        print("AccessToken Renewed")
                    }, success: { (response) in
                        try? completionHandler(DataResponse(request: response.request, response: response.response, data: response.data, result: .success(try response.jsonObject())))
                    }, failure: { (OAuthSwiftError) in
                        do{
                            let error = (OAuthSwiftError.errorUserInfo["error"] as! NSError).userInfo as! [String:Any]
                            if let data = error[StringConstant.responseDataKey]{
                                let response = try JSONSerialization.jsonObject(with: data  as! Data, options: [])
                                completionHandler(DataResponse(request: OAuthSwiftError.errorUserInfo[StringConstant.request] as? URLRequest, response: error[StringConstant.responseKey] as? HTTPURLResponse, data: data as? Data, result: .success(response)))
                            }else{
                                completionHandler(self.errorResponse)
                            }
                        } catch let error as NSError {
                            print(error)
                        }
                    })
            }
        }else{
            
            if let view = Constants.appDelegate?.window?.rootView(){
                ToastPopUp.showNetworkPOPup(message:ToastMsgConstant.network ,view: view)
            }
            completionHandler(errorResponse)
        }
    }
    
    //for register Requests of POST request Type
    func makeCloudRequestWithClientCredentials(url:String,parameter:[String:Any],requestType:HTTPMethod ,completionHandler: @escaping (DataResponse<Any>?) -> Void ){
        if NetworkCheck.isNetwork() {
            let queue = DispatchQueue(label: "com.test.api", qos: .background, attributes: .concurrent)
            queue.async {
                if(requestType == .post) {
                    self.generateClientOauthAccessToken(url:url,parameter:parameter,requestType:requestType,completionHandler: completionHandler)
                }else{
                    //register Requests are only POST type request
                }
            }
        }else{
            
            if let view = Constants.appDelegate?.window?.rootView(){
                ToastPopUp.showNetworkPOPup(message:ToastMsgConstant.network ,view: view)
            }
            completionHandler(errorResponse)
        }
    }
    
    func makeCloudRequestForStringResponse(url:String,parameter:[String:Any],headers:[String:String],requestType:HTTPMethod ,completionHandler: @escaping (DataResponse<String>?) -> Void ){
        
        if NetworkCheck.isNetwork() {
            
            if(requestType == .put || requestType == .post){
                Alamofire.request(url, method: requestType, parameters: parameter, encoding: JSONEncoding.default, headers: headers).responseString { response in
                    completionHandler(response)
                }
            }else{
                Alamofire.request(url, method: requestType, encoding: JSONEncoding.default, headers: headers).responseString { response in
                    completionHandler(response)
                }
            }
        }else{
            
            if let view = Constants.appDelegate?.window?.rootView(){
                ToastPopUp.showNetworkPOPup(message:ToastMsgConstant.network ,view: view)
            }
            completionHandler(stringErrorResponse)
        }
    }
    
    func makeCloudGetWithCacheRequest(url:String,parameter:[String:Any],headers:[String:String],requestType:HTTPMethod ,completionHandler: @escaping (CachedURLResponse?, Int?) -> Void ){
        
        let errorResponse:CachedURLResponse? = nil
        if(requestType == .get){
            var request = URLRequest(url: URL(string: url)!, cachePolicy: NSURLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
            

            request.allHTTPHeaderFields = headers
            request.httpMethod = requestType.rawValue
            
            let cacheResponse = URLCache.shared.cachedResponse(for: request as URLRequest)
            if cacheResponse != nil {
                completionHandler(cacheResponse, 200)
            }
            
            if (NetworkReachabilityManager()?.isReachable)! {
                
                Alamofire.request(request).responseJSON { response in
                    
                    if let json = response.result.value{
                        if (response.response?.statusCode)! >= 200 && (response.response?.statusCode)! < 300
                        {
                            
                            let cacheResponse = CachedURLResponse(response: response.response!, data: response.data!)
                            URLCache.shared.storeCachedResponse(cacheResponse, for: request as URLRequest)
                            completionHandler(cacheResponse, response.response?.statusCode)
                        }
                        else{
                            
                            let cacheResponse = CachedURLResponse(response:response.response! as URLResponse, data: response.data!)
                            completionHandler(cacheResponse, response.response?.statusCode)
                        }
                    }else{
                        completionHandler(errorResponse,0)
                    }
                    
                    
                }
            }else{
                if cacheResponse == nil {
                    completionHandler(errorResponse,0)
                }
                //Get cache response using request object
                /* let cacheResponse = URLCache.shared.cachedResponse(for: request as URLRequest)
                 if cacheResponse != nil {
                 completionHandler(cacheResponse)
                 }else{*/
                //}
            }
        }
    }
    
    
    func makeCloudPostRequestWithArray(url:String,parameter:NSMutableArray,headers:[String:String],requestType:HTTPMethod ,completionHandler: @escaping (DataResponse<Any>?) -> Void ){
        
        if (NetworkReachabilityManager()?.isReachable)! {
            
            if(requestType == .post){
                var request = URLRequest(url:URL(string: url)!)
                request.httpMethod = "POST"
                request.allHTTPHeaderFields = headers
                request.httpBody = try! JSONSerialization.data(withJSONObject: parameter)
                
                Alamofire.request(request)
                    .responseJSON { response in
                        completionHandler(response)
                }
                
            }
        }else{
            if let view = Constants.appDelegate?.window?.rootView(){
                ToastPopUp.showNetworkPOPup(message:ToastMsgConstant.network ,view: view)
            }
                completionHandler(errorResponse)
        }
    }
    
    func makeUploadRequest(url:String, data:Data, name:String, filename:String, mimetype:String,headers:[String:String],completionHandler: @escaping (DataResponse<Any>)  -> Void){
        
        //
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(data, withName:name ,fileName: filename, mimeType: mimetype)
                
        },
            to: url,
            headers: headers,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint(response)
                        completionHandler(response)
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        }
        )
    }
    
}


extension CloudManager{
    //for generating oauthclient access Token for first time for registering user
    func generateClientOauthAccessToken(url:String,parameter:[String:Any],requestType:HTTPMethod,completionHandler: @escaping (DataResponse<Any>?) -> Void ){
        let session = URLSession.shared
        
        let accessTokenURL = URL(string:RequestURL.URL_OAUTH_ACCESS_TOKEN.rawValue)!
        var accessTokenRequest = URLRequest(url:accessTokenURL)
        accessTokenRequest.httpMethod = "POST"
        let param = "&\(StringConstant.grantType)=client_credentials" + "&\(StringConstant.clientId)=\(StringConstant.oauth_client_id)" + "&\(StringConstant.clientSecret)=\(KeychainService.shared[KEY_OAUTH.KEY_NAME_OAUTH_CLIENT_SECRET]!)"
        accessTokenRequest.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        accessTokenRequest.httpBody=param.data(using: String.Encoding.utf8)
        let accessTokenTask = session.dataTask(with: accessTokenRequest as URLRequest, completionHandler: {
            (data, response, error) in
            if let _ = error {
                print(error)
                DispatchQueue.main.async {
                    completionHandler(self.errorResponse)
                }
            }
            else if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == 200 {
                    print("ClientAccessToken Generation Successful")
                    guard let _:Data = data else
                    {
                        return
                    }
                    let json:Any?
                    do
                    {
                        json = try JSONSerialization.jsonObject(with: data!, options: [])
                    }
                    catch
                    {
                        return
                    }
                    guard let server_response = json as? NSDictionary else
                    {
                        return
                    }
                    self.accessToken = server_response[StringConstant.accessToken] as? String
                    self.registerUser(parameter,completionHandler: completionHandler)
                }
                else {
                    print("ClientAccessToken Generation Failed")
                    DispatchQueue.main.async {
                        completionHandler(self.errorResponse)
                    }
                }
            }
            
        })
        accessTokenTask.resume()
    }
    
    //for generating oauthclient access Token for string response forgot password api call
    func generateClientOauthAccessToken(url:String,parameter:[String:Any],header:[String:String],requestType:HTTPMethod,completionHandler: @escaping (DataResponse<String>?) -> Void ){
        let session = URLSession.shared
        
        let accessTokenURL = URL(string:RequestURL.URL_OAUTH_ACCESS_TOKEN.rawValue)!
        var accessTokenRequest = URLRequest(url:accessTokenURL)
        accessTokenRequest.httpMethod = "POST"
        let param = "&\(StringConstant.grantType)=client_credentials" + "&\(StringConstant.clientId)=\(StringConstant.oauth_client_id)" + "&\(StringConstant.clientSecret)=\(KeychainService.shared[KEY_OAUTH.KEY_NAME_OAUTH_CLIENT_SECRET]!)"
        accessTokenRequest.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        accessTokenRequest.httpBody=param.data(using: String.Encoding.utf8)
        let accessTokenTask = session.dataTask(with: accessTokenRequest as URLRequest, completionHandler: {
            (data, response, error) in
            if let _ = error {
                print(error)
                DispatchQueue.main.async {
                    completionHandler(self.stringErrorResponse)
                }
            }
            else if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == 200 {
                    print("ClientAccessToken Generation Successful")
                    guard let _:Data = data else
                    {
                        return
                    }
                    let json:Any?
                    do
                    {
                        json = try JSONSerialization.jsonObject(with: data!, options: [])
                    }
                    catch
                    {
                        return
                    }
                    guard let server_response = json as? NSDictionary else
                    {
                        return
                    }
                    self.accessToken = server_response[StringConstant.accessToken] as? String
                    var headers = header
                    headers.updateValue("Bearer  \(self.accessToken ?? "")", forKey: "Authorization")
                    self.makeCloudRequestForStringResponse(url: url, parameter: parameter, headers: headers, requestType: requestType, completionHandler: completionHandler)
                }
                else {
                    print("ClientAccessToken Generation Failed")
                    DispatchQueue.main.async {
                        completionHandler(self.stringErrorResponse)
                    }
                }
            }
            
        })
        accessTokenTask.resume()
    }
    
    //for registering user
    func registerUser(_ parameter:[String:Any],completionHandler: @escaping (DataResponse<Any>?) -> Void ){
        let url =  URL(string: RequestURL.URL_OAUTH_REGISTER.rawValue)!
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer  \(self.accessToken ?? "")", forHTTPHeaderField: "Authorization")
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameter, options: .prettyPrinted)
        
        Alamofire.request(request).responseJSON {
            (response) in
            completionHandler(response)
        }
        
    }
    
    func make(_ url: URL, method: OAuthSwiftHTTPRequest.Method, parameters: OAuthSwift.Parameters, body: Data?) -> [String : String] {
        if url == URL(string:RequestURL.URL_OAUTH_ACCESS_TOKEN.rawValue) {
            return [:]
        }else{
            return RequestHeader.getHeadersWithAuth()
        }
    }
}

