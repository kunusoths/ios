//
//  JourneyManager.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 21/06/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import CoreLocation
import ObjectMapper
import CoreData

@objc protocol JourneyManagerDelegate {
    @objc optional func journeyRideStartedSuccess(rideDetails:RideCreateResponse)
    @objc optional func journeyRideStopedSuccess()
    @objc optional func journeyRideRouteSuccess(tbt_Route:String)
    @objc optional func journeyRideIsInProgress(rideDetails:Rides)
    @objc optional func journeyNoRideInProgress()
    @objc optional func lostConnection()
    @objc optional func onFailure(Error:[String:Any])
}

enum RideStates{
    case InProgress
    case Paused
    case Finished
}

enum RideTypes{
    case AdHoc
    case Planned
    case None
}

enum AlertSendingType{
    case Unknown
    case AboutToSent
    case Sent
    case ReSent
    case OptingOut
}

enum AlertModeStatus{
    case On
    case Off
}

enum AlertGenerator{
    case Unknown
    case DM
    case App
}


final class JourneyManager: NSObject,RideManagerDelegate, VCMPacketManagerDelegate{
    
    //MARK:- Properties
    static let sharedInstance = JourneyManager()
    
    weak var journeyManagerDelegate:JourneyManagerDelegate?
    
    fileprivate var bikerID:NSInteger?
    
    fileprivate var rideType:RideTypes?
    fileprivate var rideDetails:Rides?
    
    fileprivate var rideObj:RideManager! // = RideManager()
    
    fileprivate var alertSendingTypes:AlertSendingType = AlertSendingType.Unknown
    fileprivate var alertGeneratedBy:AlertGenerator = AlertGenerator.Unknown
    fileprivate var alertModeState:AlertModeStatus = AlertModeStatus.Off
    
    let sinkManager = SinkManager.sharedInstance
    let rideStateHandler = RideStateHandler.sharedInstance

    var rideIdHold:String?
    var isQuickRide = false
    
    //MARK:- Private Methods
    private override init() {
        super.init()
        self.rideObj = RideManager()
        
        if rideStateHandler.isAnyRideInProgress() {
            self.rideType = rideStateHandler.getCurrentRideType()
            self.rideDetails = rideStateHandler.getCurrentRideDetails()
        }
        self.rideObj.rideManagerDelegate = self
        
    }
    

    //MARK:- Public Methods
    
    func getAlertMode() -> AlertModeStatus{
        return self.alertModeState
    }
    
    func setAlertModeState(state:AlertModeStatus){
        self.alertModeState = state
    }
    
    func getAlertGenerator() -> AlertGenerator{
        return self.alertGeneratedBy
    }
    
    func setAlertGenerator(generator:AlertGenerator){
        self.alertGeneratedBy = generator
    }
    
    func getAlertSendingType() -> AlertSendingType{
        return self.alertSendingTypes
    }
    
    func setAlertSendingType(type:AlertSendingType){
        self.alertSendingTypes = type
    }
    
    func isAnyRideInProgress() -> Bool{
        return rideStateHandler.isAnyRideInProgress()
    }
    
    func rideProgressType() -> RideTypes{
        return rideStateHandler.getCurrentRideType()
    }
    
    func currentRideId() -> NSInteger {
        return rideStateHandler.getCurrentRideId()
    }
    
    func currentRideDetails() -> Rides {
        return rideStateHandler.getCurrentRideDetails()
    }
    
    func getTotalLegCompleted() -> Int{
        return rideStateHandler.getCurrentRideTotalLegCompleted()
    }
    
    func getTBTRoute() -> String{
        return rideStateHandler.getCurrentRideTbtRoute()
    }
    
    //Get Status of navigtion Engine
//    func getStatusOFNavigationEngine()->NavigationStatus{
//        return rideStateHandler.getStatusOFNavigationEngine()
//    }
    
    func updateCurrentRideDetails(rideDetails:Rides) {
        self.rideDetails = rideDetails
        rideStateHandler.setCurrentRideDetails(rideDetails: self.rideDetails!, rideType: self.rideType ?? .None, rideState: RideStates.InProgress)
    }
    
    func refreshCurrentRideDetails(){
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
        if let ride = Rides.getMyProgressRide(context: context){
            self.rideDetails = ride
            guard let isPlanned = rideDetails?.planned else {
                return
            }
            let type:RideTypes!
        
            if isPlanned {
                type = RideTypes.Planned
            }else {
                type = RideTypes.AdHoc
            }
            rideStateHandler.setCurrentRideDetails(rideDetails: ride, rideType: type, rideState: RideStates.InProgress)
        }
    }
    
    func rideStarted(rideDetails: Rides) {
        
        self.rideDetails = rideDetails
        rideStateHandler.setCurrentRideDetails(rideDetails: self.rideDetails!, rideType: self.rideType ?? .None, rideState: RideStates.InProgress)
        MyDefaults.setBoolData(value: false, key: DefaultKeys.IS_RIDE_COMPLETED)

        if rideDetails.planned, let route = self.rideDetails?.tbt_route {
            MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_JOIRNEY_COMPLETE)
            
            self.journeyStartNavigationEngine(tbt_route: route)
            //On ride started always on Audio Alerts
            NavigationLocator.sharedInstance().enableAudioAttributes()
            self.rideType = .Planned
        } else {
            self.rideType = .AdHoc
        }
        
        //Check is CM is Virtual, if yes start calculating Local stats and save virtual cm packet
        let isVirtualCm = MyDefaults.getBoolData(key: DefaultKeys.IS_CM_VIRTUAL)
        if isVirtualCm {
            VCMPacketManager.sharedInstance.rideHasBeenStarted(rideId: (self.rideDetails?.rideID?.toInt())!)
        }
        
        let _ = RideStatsCalculater()

        sinkManager.notifyObservers(notificationKey: sinkManager.SINK_NOTIFICATION_RIDE_STARTED, sender: self)
        
        rideStateHandler.setRideStartTime(rideStartTime: Date())
        //self.journeyManagerDelegate?.journeyRideStartedSuccess!(rideDetails: self.rideDetails)
        
        if rideDetails.planned {
            sinkManager.notifyObservers(notificationKey: sinkManager.SINK_NOTIFICATION_JOURNEY_STARTED, sender: self)
        }
        
    }
    
    func rideHasBeenStopped() {
        
        var uploadFile = false
        var rideId = 0
        var riderName = ""
        
        self.sinkManager.notifyObservers(notificationKey: sinkManager.SINK_NOTIFICATION_RIDE_ROUTE_FINISHED, sender: self)
        sinkManager.notifyObservers(notificationKey: sinkManager.SINK_NOTIFICATION_RIDE_STOPPED, sender: self)
        
        //self.rideStateHandler.stopNavigationEngine()
        MyDefaults.setBoolData(value: true, key: DefaultKeys.IS_RIDE_COMPLETED)

        
        if self.rideType == RideTypes.Planned {
            
            if let Id = self.rideDetails?.rideID?.toInt() {
                uploadFile = true
                rideId = Id
            }
        }
        
        //Explicitly stopped the Ride
        
        self.setNoRideInProgress()
        
        if uploadFile {
            FCMFileUploadManager.fcmManager.renameNavEngineFile(rideID: rideId)
//            fcmRef.uploadFileToFCMStorage(rideId: rideId, riderName: riderName)
            
        }
        
    }
    
    func rideIsInProgress(rideDetails: Rides){
        
        if(rideDetails.planned) {
            //Planned Ride Logic is Here
            rideType = RideTypes.Planned
        }else{
            //Ad-Hoc Ride Logic is Here
            rideType = RideTypes.AdHoc
        }
        self.rideDetails = rideDetails
        rideStateHandler.setCurrentRideDetails(rideDetails: self.rideDetails!, rideType: self.rideType ?? .None, rideState: RideStates.InProgress)

        if rideDetails.planned, let route = self.rideDetails?.tbt_route {
            MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_JOIRNEY_COMPLETE)
            
            self.journeyStartNavigationEngine(tbt_route: route) //Crashhere
        }
        
        rideStateHandler.setCurrentRideDetails(rideDetails: rideDetails, rideType: rideType!, rideState: RideStates.InProgress)
        
    }
    
    func noRideISInProgress() {
        self.setNoRideInProgress()
    }
    
    func setNoRideInProgress(){
        self.rideType = nil
        //self.rideDetails = nil
        rideStateHandler.clearCurrentRideDetails()
    }
    
    func getJourneyRideRouteById(rideID: NSInteger, bikerId:NSInteger){
        rideObj.getRideRouteById(rideID:rideID, bikerId:bikerId)
    }
    
    func recalulateRideStats(rideId: Int) {
        print("Recalculating ride stats for id \(rideId)")
        self.rideObj.recalculateStats(rideID: rideId)
    }
    
    func getRideProgressApi(){
        self.rideObj.getCurrentProgressRides()
    }
    
    func updateNavigationEngineState() {
        if self.rideDetails?.planned ?? false, let route = self.rideDetails?.tbt_route {
            self.journeyStartNavigationEngine(tbt_route: route)
            
            let isAudioOn = MyDefaults.getBoolData(key:  DefaultKeys.KEY_AUDIO_STATUS)
            if isAudioOn {
                NavigationLocator.sharedInstance().enableAudioAttributes()
            }else {
                NavigationLocator.sharedInstance().disableAudioAttributes()
            }
        }
    }
    
    func journeyStartNavigationEngine(tbt_route:String){
        //************* Started Navigation Engine *********
        switch NavigationLocator.sharedInstance().getStatusOfNavigation() {
        case .NAVIGATORSTATEACTIVE:
            print("Navigation engine Already started")
            sinkManager.notifyObservers(notificationKey: sinkManager.SINK_NOTIFICATION_JOURNEY_STARTED, sender: self)
            break
        ///NAVIGATORSTATEERROR: Means its nullable Navigator object in initial state
        case .NAVIGATORSTATEERROR:
             print("Navigation engine State Error")
            self.startNavigationEngine(tbt_route: tbt_route)
            break
            
        case .NAVIGATORSTATEPAUSED:
             print("Navigation engine State Paused")
            rideStateHandler.resumeNavigationEngine()
            break
            
        case .NAVIGATORSTATECOMPLETED:
            print("Navigation engine State Completed")
            self.startNavigationEngine(tbt_route: tbt_route)
            break
            
            
        default:
            print("Navigation State:\(NavigationLocator.sharedInstance().getStatusOfNavigation())")
            self.startNavigationEngine(tbt_route: tbt_route)
            break
        }
    }
    
    func startNavigationEngine(tbt_route:String) {
        let isRideCompleted = MyDefaults.getBoolData(key: DefaultKeys.IS_RIDE_COMPLETED)
        if isRideCompleted {
            print("Ride is finished")
        }else{
            print("Starting Navigation Engine ")
            rideStateHandler.setCurrentRideTbtRoute(rideTbtRoute: tbt_route)
            rideStateHandler.setTbtRouteToNavigation(TBT_Route: tbt_route)
            rideStateHandler.startNavigationEngine()
        }
    }
    
    func saveLastPacketSentTime(rideId:Int){
            let offlineStatsContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
            let nowTimeStamp = NSDate()
            AchievementDataObj.updateLastPacketTime(time: nowTimeStamp, context: offlineStatsContext, rideID: String(rideId))
    }
    
    
    //MARK:- VCM Packet Manager Delegate
    
    func allPacketsSent(){
        VCMPacketManager.sharedInstance.vcmDelegate = nil
    }
    
    func writingFailed() {
     //   journeyManagerDelegate?.lostConnection!()
    }
    
    func onFailure(Error: [String : Any]) {
        //
    }
    
    
}

