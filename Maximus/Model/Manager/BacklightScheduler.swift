//
//  BacklightScheduler.swift
//  Maximus
//
//  Created by Isha Ramdasi on 13/06/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit

class BacklightScheduler: NSObject {
    
    fileprivate let MODE_DAY:UInt32 = 0
    fileprivate let MODE_TWILIGHT:UInt32 = 1
    fileprivate let MODE_NIGHT:UInt32 = 2
    fileprivate let MODE_EVENING_TWILIGHT: UInt32 = 4
    
    let backlightManager = BacklightManager()
    var backlightTriggerTimer: Timer!
    let backgroundQueue = DispatchQueue.global(qos: .userInitiated)
    
    private static var privateShared : BacklightScheduler?
    class func sharedInstance() -> BacklightScheduler { // change class to final to prevent override
        guard let shared = privateShared else {
            privateShared = BacklightScheduler()
            return privateShared!
        }
        return shared
    }
    
    func secondsFromBeginningOfTheDay(time: Date) -> TimeInterval {
        let calendar = Calendar.current
        // omitting fractions of seconds for simplicity
        let dateComponents = calendar.dateComponents([.hour, .minute, .second], from: time)
        
        let dateSeconds = dateComponents.hour! * 3600 + dateComponents.minute! * 60 + dateComponents.second!
        
        return TimeInterval(dateSeconds)
    }

    func setBacklightScheduler(timeInterval: TimeInterval) {
        DispatchQueue.main.async {
            if self.backlightTriggerTimer == nil {
                print("setBacklightScheduler")
                self.backlightTriggerTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(self.sendCommandToDM), userInfo: nil, repeats: false)
            }
        }
    }
    
    func sendCommandToDM() {
        print("sendCommandToDM")
        MaximusDM.sharedInstance.sendBackLightControlModeToDM()
        invalidateTimer()
        calculateTimerInterval()
    }
    
    func calculateTimerInterval() {
        let currentTime = getCurrentTime()
        let timeForMode = getTimeAsPerMode()
        var difference:TimeInterval = secondsFromBeginningOfTheDay(time: timeForMode) - secondsFromBeginningOfTheDay(time: currentTime)
        if difference > 0 {
            print("time interval:\(difference)")
            setBacklightScheduler(timeInterval: difference)
        } else {
            difference = (24*60*60) - abs(difference)
            print("time interval:\(difference)")
            setBacklightScheduler(timeInterval: difference)
        }
    }
    
    func getCurrentTime() -> Date {
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        let timeFormat = "dd-MM-yyyy HH:mm:ss"
        dateFormatter.dateFormat = timeFormat
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let currentDateString = dateFormatter.string(from: currentDate)
        let currentDateFromString = dateFormatter.date(from: currentDateString)
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm:ss"
        timeFormatter.timeZone = TimeZone(abbreviation: "UTC")
        timeFormatter.locale = Locale(identifier: "en_US_POSIX")
        let dateStr = timeFormatter.string(from: currentDateFromString!)
        let dateInDate = timeFormatter.date(from: dateStr)
        return dateInDate ?? Date()
    }
    
    func getTimeAsPerMode() -> Date {
        let dateFormatter = DateFormatter()
        let timeFormat = "dd-MM-yyyy HH:mm:ss"
        dateFormatter.dateFormat = timeFormat
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm:ss"
        
        var timeAsPerMode = "01-06-2018 14:00:00"
        
        let mode:UInt32 =  getNextMode(currentMode: backlightManager.identifyBacklightMode())
        
        switch mode {
        case MODE_DAY:
            timeAsPerMode = "01-06-2018 02:00:00"
            break
        case MODE_TWILIGHT:
            timeAsPerMode = "01-06-2018 00:00:00"
            break
        case MODE_EVENING_TWILIGHT:
            timeAsPerMode = "01-06-2018 12:00:00"
            break
        case MODE_NIGHT:
            timeAsPerMode = "01-06-2018 14:00:00"
            break
        default:
            break
        }
        
        let eveDate = dateFormatter.date(from: timeAsPerMode)
        let eveStr = timeFormatter.string(from: eveDate ?? Date())
        let eveDateFinal = timeFormatter.date(from: eveStr)
        return eveDateFinal ?? Date()
    }
    
    func invalidateTimer() {
        if backlightTriggerTimer != nil {
            backlightTriggerTimer?.invalidate()
            backlightTriggerTimer = nil
        }
    }
    
    func getNextMode(currentMode:UInt32) -> UInt32 {
        switch currentMode {
        case MODE_DAY:
            return MODE_EVENING_TWILIGHT
        case MODE_EVENING_TWILIGHT:
            return MODE_NIGHT
        case MODE_NIGHT:
            return MODE_TWILIGHT
        case MODE_TWILIGHT:
            return MODE_DAY
        default:
            break
        }
        return MODE_DAY
    }
}
