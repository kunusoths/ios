//
//  SinkManager.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 18/07/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import Alamofire

final class SinkManager: NSObject {
    
    //MARK:- Properties
    let SINK_NOTIFICATION_REMAINING_DISTANCE = "com.remainingDistance.NotificationKey"
    let SINK_NOTIFICATION_CURRENT_TURN = "com.currentTurn.NotificationKey"
    let SINK_NOTIFICATION_NEXT_TURN = "com.nextTurn.NotificationKey"
    let SINK_NOTIFICATION_RIDE_ROUTE_FINISHED = "com.rideRouteFinished.NotificationKey"
    
    let SINK_NOTIFICATION_FIRST_POI = "com.firstPoi.NotificationKey"
    let SINK_NOTIFICATION_SECOND_POI = "com.secondPoi.NotificationKey"
    let SINK_NOTIFICATION_THIRD_POI = "com.thirdPoi.NotificationKey"
    
    let SINK_NOTIFICATION_DEVIATION_DETECTED = "com.deviationDetected.NotificationKey"
    
    let SINK_NOTIFICATION_JOURNEY_STARTED = "com.journeyStated.NotificationKey"
    let SINK_NOTIFICATION_RIDE_STARTED = "com.maximuspro.RideStarted"
    let SINK_NOTIFICATION_RIDE_STOPPED = "com.maximuspro.RideStopped"
    let SINK_NOTIFICATION_RIDE_INPROGRESS = "com.maximuspro.RideInProgress"
    
    //let SINK_NOTIFICATION_STOP_RIDE = "com.stop.ride"
    let SINK_NOTIFICATION_LEG_PROGRESS_COMPLETED = "com.legProgressCompleted.NotificationKey"
    
    let SINK_NOTIFICATION_LEG_STARTED = "com.leg.started"
    let SINK_NOTIFICATION_LEG_IN_PROGRESS = "com.leg.in.progress"
    let SINK_NOTIFICATION_SKIPPED = "com.leg.skipped"
    let SINK_NOTIFICATION_STEP_STARTED = "com.step.started"
    let SINK_NOTIFICATION_STEP_IN_PROGRESS = "com.step.in.progress"
    let SINK_NOTIFICATION_STEP_COMPLETED = "com.step.completed"
    let SINK_NOTIFICATION_STEP_SKIPPED = "com.step.skipped"
    
    let SINK_NOTIFICATION_CURRENT_MANUEVER = "com.current.manuever"
    let SINK_NOTIFICATION_NEXT_MANUEVER = "com.next.manuever"
    let SINK_NOTIFICATION_CURRENT_MANUEVER_INFO = "com.next.manuever.detailedDiscription"
    
    let SINK_NOTIFICATION_PLOT_STATUS_END_POINTS = "com.plotStatusEndPoints.NotificationKey"
    let SINK_NOTIFICATION_PLOT_STATUS_ROUTE = "com.plotStatusRoute.NotificationKey"
    
    //    let SINK_NOTIFICATION_BLE_RECONNECT = "ble_reconnect.NotificationKey"
    //    let SINK_NOTIFICATION_BLE_DISCONNECT = "ble_disconnect.NotificationKey"
    
    
    let ALERT_NOTIFICATION_GENERATED_SUCESSFULLY = "com.AlertGenerateSucess.NotificationKey"
    let ALERT_NOTIFICATION_GENERATE_FAILURE      = "com.AlertGenerateFail.NotificationKey"
    let ALERT_NOTIFICATION_SEND                  = "com.AlertSend.NotificationKey"
    let ALERT_NOTIFICATION_WAITING_FOR_RESPONSE  = "com.AlertWaitingForResponse.NotificationKey"
    let ALERT_NOTIFICATION_DISCARDED             = "com.AlertDiscarded.NotificationKey"
    //let ALERT_NOTIFICATION_RECEIVED              = "com.AlertReceived.NotificationKey"
    let ALERT_NOTIFICATION_ACKNOWLEDGE           = "com.AlertAcknowledge.NotificationKey"
    let ALERT_NOTIFICATION_CLOSED                = "com.AlertClosed.NotificationKey"
    //let ALERT_NOTIFICATION_GENERATED_CLOSED      = "com.AlertGeneratedClosed.NotificationKey"
    let ALERT_NOTIFICATION_ANOTHER_ALERT_RECEIVED = "com.AlertAnotherReceived.NotificationKey"
    
    let ALERT_NOTIFICATION_ALERT_RESPONSE_FAILURE = "com.AlertResponseFailure.NotificationKey"
    
    let ALERT_NOTIFICATION_ACK_WAIT_TIMEDOUT = "com.AlertAckWaitTimedOut.NotificationKey"
    let ALERT_NOTIFICATION_RESPONSE_WAIT_TIMEDOUT = "com.AlertResponseWaitTimedOut.NotificationKey"
    
    let ALERT_NOTIFICATION_TIMER_STARTED   = "com.AlertTimerStarted.NotificationKey"
    
    ///* Bluetooth Notification Observer Keys *///
    
    let SINK_NOTIFICATION_BLUETOOTH_STATE_UPDATE   = "com.BluetoothStatus.NotificationKey"
    
    ///* Alert Notification Observer Keys *///
    
    let ALERT_NOTIFICATION_CLOSE_ALERT_EVENT_FROM_DM = "com.CloseAlertEventFromDmNotificationKey"
    
    let SINK_NOTIFICATION_LOCATION_UPDATE = "com.maximuspro.location_update"
    
     let SINK_NOTIFICATION_NETWORK_STATE_CHANE   = "com.maximuspro.network"
    
    static let sharedInstance = SinkManager()
    var observers:[String:Any] = [:]
    
    private override init() {
        //
        super.init()
        self.ListenNetwork()
    }
    //MARK:- Methods
    func addObserver(observer:Any, notificationKey:String, selectorName block: @escaping (Notification) -> Swift.Void){
        
        let observer = NotificationCenter.default.addObserver(forName: NSNotification.Name(notificationKey),
                                                              object: nil,
                                                              queue: nil,
                                                              using: block)
        observers[notificationKey] = observer
    }
    
    func notifyObservers(notificationKey:String, sender:Any, data: [AnyHashable : Any]? = nil){
        
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: Notification.Name(notificationKey),
                                            object: sender,
                                            userInfo: data)
        }
    }
    
    func removeObserver(observer:Any, notificationKey:String){
        
        if let observer1 = observers[notificationKey]{
            NotificationCenter.default.removeObserver(observer1,
                                                      name: Notification.Name(notificationKey),
                                                      object: nil)
        }
    }
    
    
    
    //MARK:- VC based Methods
    func changeRegisterationForDailyIterneryUpdates(dailyIternary:RideStatusVc,register:Bool){
        if(register){
            //register to updates
            addObserver(observer: dailyIternary, notificationKey: SINK_NOTIFICATION_LEG_PROGRESS_COMPLETED, selectorName: dailyIternary.catchNotificationLegProgressCompleted)
            addObserver(observer: dailyIternary, notificationKey: SINK_NOTIFICATION_PLOT_STATUS_END_POINTS, selectorName: dailyIternary.catchNotificationPlotStatusRouteEndPoints)
            addObserver(observer: dailyIternary, notificationKey: SINK_NOTIFICATION_PLOT_STATUS_ROUTE, selectorName: dailyIternary.catchNotificationPlotStatusRoute)
        }else{
            //deregister notification updates
            
            removeObserver(observer: dailyIternary, notificationKey: SINK_NOTIFICATION_LEG_PROGRESS_COMPLETED)
            removeObserver(observer: dailyIternary, notificationKey: SINK_NOTIFICATION_PLOT_STATUS_END_POINTS)
            removeObserver(observer: dailyIternary, notificationKey: SINK_NOTIFICATION_PLOT_STATUS_ROUTE)
        }
    }
    
    func ListenNetwork() {
        let net = NetworkReachabilityManager()
        net?.startListening()
        var data:[AnyHashable:Any] = [:]
            net?.listener = { status in
            print("is reachable :  \(status)")
            if net?.isReachable ?? false {
                
                switch status {
                    
                case .reachable(.ethernetOrWiFi), .reachable(.wwan):
                    print("The network is reachable over the WiFi connection")
                    data = ["isReachable":true]
                    RideRequestHandler.requestHandlerSharedInstance.startExecutingRequestObj()
                case .notReachable:
                    print("The network is not reachable")
                    data = ["isReachable":false]
                case .unknown :
                    data = ["isReachable":false]
                    break
                    
                }
            }else{
                 data = ["isReachable":false]
            }
            self.notifyObservers(notificationKey: self.SINK_NOTIFICATION_NETWORK_STATE_CHANE, sender: self, data: data)
        }
    }
    
}


