//
//  CMregistrationhandler.swift
//  Maximus
//
//  Created by kpit on 16/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper


@objc protocol CMRegistrationDelegate {
    
   // func onSuccess(response:DataResponse<Any>,message:String)
    @objc optional func onCMRegistrationSuccess(data:[String:Any])
    @objc optional func onFailure(Error:[String:Any])
}

class CMregistrationhandler {
    
    var cmRegistrationDelegate:CMRegistrationDelegate?
    let cloudHandlerObj = CloudHandler()

    
    func validateCM(serialNumber:String ,vehicleID:Int)  {
        let parameter:[String:Any] = [
                                        StringConstant.serial_number :serialNumber,
                                        StringConstant.vehicle_id    : vehicleID
                                     ]
        print(parameter)
        
        self.cloudHandlerObj.makeCloudRequest(subUrl: RequestURL.URL_CM_REGISTRATION.rawValue, parameter: parameter, isheaderAUTH: true, requestType: .post){ response in
            
            if let json = response?.result.value {
                
                print(json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let cmSuccess = Mapper<CMRegistrationModel>().map(JSON:json as! [String : Any])!
                    let data:[String:Any] = [StringConstant.data:cmSuccess]
                    MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_CMREGISTER)//-----Save CM Registration Status
                    self.cmRegistrationDelegate?.onCMRegistrationSuccess!(data: data)
                }
                else
                {
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                let json:[String:Any] = ["description":"Request timed out.",
                                         "errorCode":1001,"fieldError":"","message":"Device is already mapped with other vehicle."]
                self.errorCall(value: json)
            }

        }
    }
    
    func errorCall(value:[String : Any]!){
        let ErrorModelObj = Mapper<ErrorModel>().map(JSON:value)
        let errorData:[String:Any] = [StringConstant.data:ErrorModelObj ?? " "]
        self.cmRegistrationDelegate?.onFailure!(Error: errorData)
    }

}
