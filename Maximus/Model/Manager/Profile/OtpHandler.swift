//
//  OtpHandler.swift
//  Maximus
//
//  Created by Sriram G on 18/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import Alamofire

protocol IOtpListner
{
    func onSuccess(response:DataResponse<Any>)
    func onFailure(response:DataResponse<Any>)
    func onPutPhoneNumberSuccess(response:DataResponse<Any>)
    func onGenerateOtpSuccess(response:DataResponse<Any>)
    func onPutPhoneNumberFailure(response:DataResponse<Any>)
    
}

class OtpHandler {
    
    var optListnerDelegate:IOtpListner?
    var cloudHandlerObj = CloudHandler()
    
    func getOtpForMobile(subUrl:String, parameter:[String:String])
    {
        self.cloudHandlerObj.makeCloudRequest(subUrl: subUrl, parameter:parameter, isheaderAUTH: true, requestType: .post){ response in
            print (response ?? "")
            if let JSON = response?.result.value {
                print("JSON: \(JSON)")
                self.optListnerDelegate?.onGenerateOtpSuccess(response: response!)
            }
        }

    }
    
    func validateOtp(subUrl:String,parameter:[String:String])
    {
        self.cloudHandlerObj.makeCloudRequest(subUrl: subUrl, parameter:parameter, isheaderAUTH: true, requestType: .post){ response in
            print (response ?? "")
            if let JSON = response?.result.value {
                print("JSON: \(JSON)")
                self.optListnerDelegate?.onSuccess(response: response!)
            }
            else
            {
                self.optListnerDelegate?.onFailure(response: response!)
            }
        }
    }
    
    func putPhoneNumber(subUrl:String,parameter:[String:Any])
    {
        
        self.cloudHandlerObj.makeCloudRequest(subUrl: subUrl, parameter: parameter, isheaderAUTH:true , requestType: .put)
        { response in
            print("Response", response ?? "")
            print("status code", response?.response?.statusCode ?? 55)
            if let json = response?.result.value {
                print("json", json)
                if response?.response?.statusCode == 200
                {
                    self.optListnerDelegate?.onPutPhoneNumberSuccess(response: response!)
                
                }else{
                    self.optListnerDelegate?.onPutPhoneNumberFailure(response: response!)
                }
                
            }
        }
    }
    
    
//    func onSuccess(response:DataResponse<Any>)
//    {
//        self.optListnerDelegate?.onSuccess(response: response)
//    }
//    
//    func onFailure(response:DataResponse<Any>){
//        self.optListnerDelegate?.onFailure(response: response)
//    }
}
