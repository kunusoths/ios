//
//  BikerDetails+CoreDataProperties.swift
//  Maximus
//
//  Created by Isha Ramdasi on 05/06/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//
//

import Foundation
import CoreData
import Crashlytics

extension BikerDetails {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BikerDetails> {
        return NSFetchRequest<BikerDetails>(entityName: "BikerDetails")
    }

    @NSManaged public var biker_id: String?
    @NSManaged public var user_name: String?
    @NSManaged public var first_name: String?
    @NSManaged public var middle_name: String?
    @NSManaged public var last_name: String?
    @NSManaged public var phone_no: String?
    @NSManaged public var email: String?
    @NSManaged public var is_otp_verified: Bool
    @NSManaged public var share_location: Bool
    @NSManaged public var profile_image_url: String?
    @NSManaged public var background_image_url: String?
    @NSManaged public var is_email_verified: Int16
    @NSManaged public var blood_group: String?
    @NSManaged public var licence_no: String?
    @NSManaged public var address: String?
    @NSManaged public var licence_issued: String?
    @NSManaged public var licence_valid_till: String?
    @NSManaged public var emergency_contacts: NSObject?
    @NSManaged public var user_id: String?
    @NSManaged public var default_vehicle_id: String
    @NSManaged public var auth_method: String?
    @NSManaged public var cm_paired: Bool
    @NSManaged public var vehicleObj: VehicleDetails?
    @NSManaged public var rideObj: NSSet?
    @NSManaged public var is_virtual_cm:Bool
    @NSManaged public var device_unit_id: String?

}

// MARK: Generated accessors for rideObj
extension BikerDetails {

    @objc(addRideObjObject:)
    @NSManaged public func addToRideObj(_ value: Rides)

    @objc(removeRideObjObject:)
    @NSManaged public func removeFromRideObj(_ value: Rides)

    @objc(addRideObj:)
    @NSManaged public func addToRideObj(_ values: NSSet)
    
    @objc(removeRideObj:)
    @NSManaged public func removeFromRideObj(_ values: NSSet)
    
    @objc(addVehicleObj:)
    @NSManaged public func addToVehicleDetailsObj(_ value: VehicleDetails)
    
    @objc(removeVehicleObj:)
    @NSManaged public func removeVehicleDetailsObj(_ value: VehicleDetails)
    
    class func isBikerPresent(context:NSManagedObjectContext, bikerId:String) -> Bool {
        
        let bikerFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "BikerDetails")
        bikerFetch.predicate = NSPredicate(format: "biker_id == %@", bikerId)
        
        do{
            if let biker = try context.fetch(bikerFetch) as? [BikerDetails] {
                if biker.count > 0 {
                    return true
                }
            }
            
        }catch{
                
        }
        return false
    }
    
    class func getBikerDetails(context: NSManagedObjectContext) -> BikerDetails? {
        let bikerFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "BikerDetails")
       // bikerFetch.predicate = NSPredicate(format: "biker_id == %@", "\(bikerID)")
        
        do{
            if let biker = try context.fetch(bikerFetch) as? [BikerDetails] {
                if biker.count > 0 {
                    return biker.first!
                }
            }
            
        }catch{
            
        }
        return nil
    }
    
    class func createBiker(context:NSManagedObjectContext, id: String) {
        
        let bikerDetails:BikerDetails!
        if self.isBikerPresent(context: context, bikerId: id){
            bikerDetails = self.getBikerDetails(context: context)
        }else{
            bikerDetails = BikerDetails(context: context)
        }
     
        bikerDetails.biker_id = id
        
        do {
            try context.save()
        } catch {
            
        }
        
    }
    
    class func updateOTPStatus(context:NSManagedObjectContext , status: Bool) {
        
        var bikerDetails:BikerDetails?
        bikerDetails = getBikerDetails(context: context)
        
        bikerDetails?.is_otp_verified = status
        
        do {
            try context.save()
        } catch {
            
        }
        
    }
    
    class func updateBiker(context:NSManagedObjectContext , biker: Biker) {
        
        var bikerDetails:BikerDetails?
        
        if isBikerPresent(context: context, bikerId: "\(biker.id!)"){
            bikerDetails = getBikerDetails(context: context)
            
        }else {
              bikerDetails = BikerDetails(context: context)
            
        }
        #if CLOUD_SERVER_STAGING
        Answers.logCustomEvent(withName: "*** Biker details ", customAttributes: ["bikerId": "\(biker.id ?? 0)", "bikerName":"\(biker.first_name ?? "")" , "isCmPaired": "\(biker.cm_paired ?? false)"])
        #endif
        
        bikerDetails?.first_name = biker.first_name
        bikerDetails?.last_name = biker.last_name
        print("Image url DB : ------------ \(biker.profile_image_url!)")
        bikerDetails?.profile_image_url = biker.profile_image_url
        bikerDetails?.email = biker.email1
        bikerDetails?.address = biker.address
        bikerDetails?.middle_name = biker.middle_name
        bikerDetails?.blood_group = biker.blood_group
        bikerDetails?.biker_id = String(biker.id ?? 0)
        bikerDetails?.licence_issued = biker.license_issued
        bikerDetails?.licence_no = biker.license_number
        bikerDetails?.licence_valid_till = biker.license_valid_till
        bikerDetails?.phone_no = biker.phone_mobile1
        bikerDetails?.share_location = biker.share_location ?? false
       // bikerDetails?.cm_paired = biker.cm_paired ?? false
        bikerDetails?.is_otp_verified = biker.otp_verified ?? false
        if let auth_method = biker.auth_method?.toString(){
            bikerDetails?.auth_method = (biker.auth_method?.toString())!
        }else{
            bikerDetails?.auth_method = AUTH_METHOD.UNKNOWN.rawValue
        }

        
        if let isVerified = biker.email1_verified {
            bikerDetails?.is_email_verified = isVerified ? 1 : 0
        }else{
            bikerDetails?.is_email_verified = -1
        }
        bikerDetails?.default_vehicle_id = (biker.default_vehicle?.id?.toString())!
        let contacts = biker.emergency_contacts
        
        var totalContactList:[Any] = []
        
        for contact in contacts! {
            var contactDict: [String: Any] = [:]
            contactDict["name"] = contact.name
            contactDict["phone"] = contact.phone
            totalContactList.append(contactDict)
        }
        
        
        bikerDetails?.emergency_contacts = totalContactList as  NSObject
        
        do {
            try context.save()
        } catch {
            
        }
    }
    
    class func addBikerVehicle(context:NSManagedObjectContext,  vehicle:DefaultVehicle){
        
        let vehicleDetails = VehicleDetails(context: context)
        
        vehicleDetails.make = vehicle.company
        vehicleDetails.model = vehicle.model
        vehicleDetails.registrationYear = String(vehicle.registration_year ??  0)
        vehicleDetails.registrationMonth = String(vehicle.registration_month ?? 0)
        
        if let bikerDetails = getBikerDetails(context: context){
            bikerDetails.vehicleObj = vehicleDetails
            
            do {
                try context.save()
            } catch {
                
            }
        }
    }
    
    class func updateBikerVehicle(context:NSManagedObjectContext,vehicle:DefaultVehicle){
        
        if let bikerDetails = getBikerDetails(context: context){
            let vehicleDetails = bikerDetails.vehicleObj
            vehicleDetails?.make = vehicle.company
            vehicleDetails?.model = vehicle.model
            vehicleDetails?.registrationYear = String(vehicle.registration_year ??  0)
            vehicleDetails?.registrationMonth = String(vehicle.registration_month ?? 0)
            
            do {
                try context.save()
            } catch {
                print("Update Vehicle failed")
            }
        }
    }

    class func getVehicle(context: NSManagedObjectContext) -> VehicleDetails? {
        let bikerFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "VehicleDetails")
        // bikerFetch.predicate = NSPredicate(format: "biker_id == %@", "\(bikerID)")
        
        do{
            if let vehicle = try context.fetch(bikerFetch) as? [VehicleDetails] {
                if vehicle.count > 0 {
                    return vehicle.first!
                }
            }
            
        }catch{
            
        }
        return nil
    }
    
    class func udpateDeviceModel(context:NSManagedObjectContext, activeDevice: ActiveDevicesModel){
        
        let bikerDetails = getBikerDetails(context: context)
        bikerDetails?.is_virtual_cm = activeDevice.isVirtualCm ?? false
        bikerDetails?.device_unit_id = activeDevice.device_unit_id
        bikerDetails?.vehicleObj?.isVirtualCm = activeDevice.isVirtualCm ?? false
        
        if activeDevice.device_unit_id != nil {
            bikerDetails?.cm_paired = true
        }else{
            bikerDetails?.cm_paired = false
        }
        bikerDetails?.vehicleObj?.device_unit_id = activeDevice.device_unit_id
        
        //let vehicle = bikerDetails?.vehicleObj
        
        do {
            try context.save()
        } catch {
            
        }
    }
    
}
