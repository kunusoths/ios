//
//  RideManager.swift
//  Maximus
//
//  Created by Sriram G on 21/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

@objc protocol RideManagerDelegate {
    
    @objc optional func sendBatteryVoltageList(batteryObj:[String:Any])
    @objc optional func getdeviceLocationSuccess(data:[String:Any])
    @objc optional func rideCreateSuccess(data:[String:Any])
    @objc optional func rideStartSuccess(data:[String:Any])
    @objc optional func rideStopSuccess(data:[String:Any])
    @objc optional func rideStatsSuccess(data:[String:Any])
    @objc optional func rideLogStatsSuccess(data:[String:Any])
    @objc optional func rideRouteSuccess(data:[String:Any])
    @objc optional func rideHonorBadgeSuccess(data:[String:Any])
    @objc optional func graphSpeedPointSuccess(data:[String:Any])
    @objc optional func onFailure(Error:[String:Any])
    @objc optional func getStickerSuccess(data:[String:Any])
    @objc optional func getMyPlannedRideSuccess(data:[String:Any])
    @objc optional func rideTitleCreateSuccess(data:[String:Any])
    @objc optional func getRideProgressSuccess(data:[String:Any])
    @objc optional func sendRideStatistics(rideStatsItem:[String:Any])
    @objc optional func sendRouteDirectionDetails(directionResult:[String:Any])
    @objc optional func onGetRideDetail(data:[String:Any])
    @objc optional func getMeanBikeBatteryVoltageSuccess(data:[String:Any])
    @objc optional func getPOISuccess(data: [String:Any])
    @objc optional func getDailyTargetSuccess(data: [String:Any])
    @objc optional func getWavePointsSuccess(data: [String:Any])
    @objc optional func postPlannedWaypointsListSuccess(data: [String:Any])
    @objc optional func getPOIDetailsSuccess(data: [String:Any])
    @objc optional func onRideStatus(data:[String:Any])
    @objc optional func onRideDiscardStatus(data:[String:Any])
    @objc optional func getRideMemberList(data: [String:Any])
    @objc optional func getDirectionResultSuccess(route: String)
    @objc optional func leaveRideSuccess()
    @objc optional func cloneRideSuccess(data:[String: Any])
}

class RideManager:NSObject
{
    var rideManagerDelegate:RideManagerDelegate?
    var cloudHandlerObj = CloudHandler()
    // MARK:Public Methods
    func getCurrentDeviceLocation(bikerID:Int)
    {
        //TODO:need to put dynamic rideid
        let suburl = "\(RequestURL.URL_ANALYTICS_BIKERS.rawValue)\(bikerID)\(RequestURL.URL_DEVICE_DATA.rawValue)"
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter:["":""], isheaderAUTH: true, requestType: .get){ response in
            
            if let json = response?.result.value {
                print(json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let user = Mapper<DeviceDataResponse>().map(JSON:json as! [String : Any])!
                    let data:[String:Any] = [StringConstant.data:user]
                    self.rideManagerDelegate?.getdeviceLocationSuccess!(data: data)
                }
                else {
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    // MARK:- Google Direction Api
    func getGoogleDirectionRoute(url: String) {
        Alamofire.request(url).responseString { response in
            
            if (response.response?.statusCode)! >= 200 && (response.response?.statusCode)! < 300 {
                let directionResult = response.result.value
                print("Direction Result :", directionResult ?? "Null")
                self.rideManagerDelegate?.getDirectionResultSuccess!(route: directionResult!)
            }
            else
            {
                self.errorCall(value: RequestTimeOut().json)
            }
        }
        
    }
    
    
    
    // MARK:Public Methods
    func createRide(parameter: [String:Any], rqstType:HTTPMethod)
    {
        //TODO:need to put dynamic rideid, bikerid
        let suburl = "\("/rides")"
        cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: parameter, isheaderAUTH: true, requestType: rqstType){ response in
            
            if let json = response?.result.value {
                print("Ride Create Response", json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let user = Mapper<RideCreateResponse>().map(JSON:json as! [String : Any])!
                    
                    let data:[String:Any] = [StringConstant.data:user]
                    
                    let createModel:RideCreateResponse = data["data"] as! RideCreateResponse
                    let rideid = createModel.id
                    MyDefaults.setInt(value: rideid!, key: DefaultKeys.KEY_POI_RIDEID)

                    if(createModel.planned)!{
                        self.getPOI(rideID: rideid!,isInsertintoDB: true)
                    }
                    self.rideManagerDelegate?.rideCreateSuccess!(data: data)
                }
                else
                {
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    func startRide(rideID:Int)
    {
        //TODO:need to put dynamic rideid, bikerid
        let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)\(RequestURL.URL_START.rawValue)"
        
        cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter:["":""], isheaderAUTH: true, requestType: .post){ response in
            
            if let json = response?.result.value {
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    MyDefaults.setInt (value: rideID , key: DefaultKeys.KEY_RIDEID)
                    let user = Mapper<RideStartResponse>().map(JSON:json as! [String : Any])!
                    let data:[String:Any] = [StringConstant.data:user]
                    self.rideManagerDelegate?.rideStartSuccess!(data: data)
                }
                else
                {
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    func stopRide(rideID:Int)
    {
        //TODO:need to put dynamic rideid, bikerid
        let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)\(RequestURL.URL_STOP.rawValue)"
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter:["":""], isheaderAUTH: true, requestType: .post){ response in
            print(response ?? "")
            if let json = response?.result.value {
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let user = Mapper<RideStartResponse>().map(JSON:json as! [String : Any])!
                    let data:[String:Any] = [StringConstant.data:user]
                    self.rideManagerDelegate?.rideStopSuccess!(data: data)
                }
                else{
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    func getRideStats(rideID:Int, bikerID:Int)
    {
        let suburl = "\(RequestURL.URL_RIDE_STATUS.rawValue)\(rideID)\(RequestURL.URL_BIKER.rawValue)\(bikerID)\(RequestURL.URL_STATS.rawValue)"
        
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ response in
            if let json = response?.result.value {
                print("JSON ",json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let user = Mapper<RideStatisticsModel>().map(JSON:json as! [String : Any])!
                    let data:[String:Any] = [StringConstant.data:user]
                    self.rideManagerDelegate?.rideStatsSuccess!(data: data)
                }
                else{
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                //self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    func recalculateStats(rideID:Int)
    {
        let suburl = "\(RequestURL.URL_RIDE_STATUS.rawValue)\(rideID)\(RequestURL.URL_RIDE_STATS_RECALCULATE.rawValue)"
        
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ response in
            if let json = response?.result.value {
                print("Recalculate Response :",json)
                print("Status code \(String(describing: response?.response?.statusCode))")
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    print("Recalculate success ")

                }
                else{
                    print("Recalculate failed ")
                    
       //             self.errorCall(value: json as! [String : Any])
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    
    func getRideLogStats(bikerID:Int, pageNo: Int, size: Int)
    {
        //TODO:need to put dynamic rideid
        let suburl = "\(RequestURL.URL_ANALYTICS_BIKERS.rawValue)\(bikerID)/ride_stats?page=\(pageNo)&size=\(size)&sort=id,desc"

//        if NetworkCheck.isNetwork() {
//            self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get, completionHandler: { (response) in
//                if let statusCode = response?.response?.statusCode {
//                    let jsonString = NSString(data: (response?.data)!, encoding: String.Encoding.utf8.rawValue)
//
//                    if statusCode >= 200 && statusCode < 300
//                    {
//                        let user = Mapper<RideLogModel>().mapArray(JSONString: jsonString! as String)
//                        let data:[String:Any] = [StringConstant.data:user!]
//                        self.rideManagerDelegate?.rideLogStatsSuccess!(data: data)
//                    }else{
//
//                        let ErrorModelObj = Mapper<ErrorModel>().map(JSONString: jsonString! as String)
//                        let errorData:[String:Any] = ["data":ErrorModelObj ?? " "]
//                        self.rideManagerDelegate?.onFailure!(Error: errorData)
//                    }
//                } else {
//                    self.errorCall(value: RequestTimeOut().json)
//                }
//            })
//        } else {
            self.cloudHandlerObj.makeCloudGetWithCacheRequest(subUrl: suburl, parameter:["":""], isheaderAUTH: true, requestType: .get){ cachedResponse, statusCode in
                
                if let response = cachedResponse {
                    
                    let jsonString = NSString(data: response.data, encoding: String.Encoding.utf8.rawValue)
                    
                    if statusCode! >= 200 && statusCode! < 300
                    {
                        let user = Mapper<RideLogModel>().mapArray(JSONString: jsonString! as String)
                        let data:[String:Any] = [StringConstant.data:user!]
                        self.rideManagerDelegate?.rideLogStatsSuccess!(data: data)
                    }else{
                        
                        let ErrorModelObj = Mapper<ErrorModel>().map(JSONString: jsonString! as String)
                        let errorData:[String:Any] = ["data":ErrorModelObj ?? " "]
                        self.rideManagerDelegate?.onFailure!(Error: errorData)
                    }
                }else{
                    self.errorCall(value: RequestTimeOut().json)
                }
        }
        }
   // }
    
    //    func getRideLogStats(bikerID:Int)
    //    {
    //        //TODO:need to put dynamic rideid
    //        let suburl = "\(RequestURL.URL_ANALYTICS_BIKERS.rawValue)\(bikerID)\(RequestURL.URL_RIDE_STATS.rawValue)"
    //        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter:["":""], isheaderAUTH: true, requestType: .get){ response in
    //
    //            if let json = response?.result.value {
    //                print(json)
    //                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
    //                {
    //                    let json1 = [StringConstant.rideLogList:json]
    //                    let user = Mapper<RideLogList>().map(JSON:json1 )!
    //                    let data:[String:Any] = [StringConstant.data:user]
    //                    self.rideManagerDelegate?.rideLogStatsSuccess!(data: data)
    //                }
    //                else{
    //                    self.errorCall(value: json as! [String : Any])
    //                }
    //            }else{
    //                self.errorCall(value: RequestTimeOut().json)
    //            }
    //        }
    //    }
    
    //        func getRideRoute(rideID:Int, bikerID:Int)
    //        {
    //            let suburl = "\(RequestURL.URL_RIDE_STATUS.rawValue)\(rideID)\(RequestURL.URL_BIKER.rawValue)\(bikerID)\(RequestURL.URL_ROUTE.rawValue)"
    //
    //            self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ response in
    //
    //                if let json = response?.result.value {
    //
    //                    if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
    //                    {
    //                        let user = Mapper<RouteLatLngModel>().map(JSON:json as! [String : Any])!
    //                        let data:[String:Any] = [StringConstant.data:user]
    //                        self.rideManagerDelegate?.rideRouteSuccess!(data: data)
    //                    }
    //                    else{
    //                        self.errorCall(value: json as! [String : Any])
    //                    }
    //                }else{
    //                    self.errorCall(value: RequestTimeOut().json)
    //                }
    //            }
    //        }
    
    
    func getRideRoute(rideID:Int, bikerID:Int)
    {
        let suburl = "\(RequestURL.URL_RIDE_STATUS.rawValue)\(rideID)\(RequestURL.URL_BIKER.rawValue)\(bikerID)\(RequestURL.URL_ROUTE.rawValue)"
        
        self.cloudHandlerObj.makeCloudGetWithCacheRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ cachedResponse, statusCode in
            
            if let response = cachedResponse {
                
                let jsonString = NSString(data: response.data, encoding: String.Encoding.utf8.rawValue)
                
                if statusCode! >= 200 && statusCode! < 300
                {
                    let user = Mapper<RouteLatLngModel>().map(JSONString: jsonString as! String)
                    let data:[String:Any] = [StringConstant.data:user!]
                    self.rideManagerDelegate?.rideRouteSuccess!(data: data)
                }else{
                    
                    let ErrorModelObj = Mapper<ErrorModel>().map(JSONString: jsonString as! String)
                    let errorData:[String:Any] = ["data":ErrorModelObj ?? " "]
                    self.rideManagerDelegate?.onFailure!(Error: errorData)
                }
            }else{
              //  self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    
    //TODO:Speeds point dummy API
    //    func getGraphSpeedPoints(rideID:Int, bikerID:Int)
    //    {
    //        let suburl = "\(RequestURL.URL_RIDE_STATUS.rawValue)\(rideID)\(RequestURL.URL_BIKER.rawValue)\(bikerID)\(RequestURL.URL_SPEEDPOINT.rawValue)"
    //
    //        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ response in
    //            if let json = response?.result.value {
    //                print(json)
    //                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
    //                {
    //                    let user = Mapper<SpeedPointsModel>().map(JSON:json as! [String : Any])!
    //                    let data:[String:Any] = [StringConstant.data:user]
    //                    self.rideManagerDelegate?.graphSpeedPointSuccess!(data: data)
    //                }
    //                else{
    //                    self.errorCall(value: json as! [String : Any])
    //                }
    //            }else{
    //                self.errorCall(value: RequestTimeOut().json)
    //            }
    //        }
    //    }
    
    func getGraphSpeedPoints(rideID:Int, bikerID:Int)
    {
        let suburl = "\(RequestURL.URL_RIDE_STATUS.rawValue)\(rideID)\(RequestURL.URL_BIKER.rawValue)\(bikerID)\(RequestURL.URL_SPEEDPOINT.rawValue)"
        
        self.cloudHandlerObj.makeCloudGetWithCacheRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ cachedResponse, statusCode in
            
            if let response = cachedResponse {
                let jsonString = NSString(data: response.data, encoding: String.Encoding.utf8.rawValue)
                
                if statusCode! >= 200 && statusCode! < 300
                {
                    let user = Mapper<SpeedPointsModel>().map(JSONString: jsonString as! String)
                    let data:[String:Any] = [StringConstant.data:user!]
                    self.rideManagerDelegate?.graphSpeedPointSuccess!(data: data)
                }else{
                    
                    let ErrorModelObj = Mapper<ErrorModel>().map(JSONString: jsonString as! String)
                    let errorData:[String:Any] = ["data":ErrorModelObj ?? " "]
                    self.rideManagerDelegate?.onFailure!(Error: errorData)
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    
    func putRideTitle(parameter:[String:Any]){
        let subUrl = "/rides"
        print("put request json", parameter)
        self.cloudHandlerObj.makeCloudRequest(subUrl: subUrl, parameter: parameter, isheaderAUTH: true, requestType: .put){
            response in
            
            if let json = response?.result.value {
                
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let user = Mapper<RideCreateResponse>().map(JSON:json as! [String : Any])!
                    let data:[String:Any] = [StringConstant.data:user]
                    self.rideManagerDelegate?.rideTitleCreateSuccess!(data: data)
                }
                else
                {
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
        
        
    }
    
    func putDailyTarget(parameter: [String:Any]) {
        
        let subUrl = "/rides"
        print("put request json", parameter)
        self.cloudHandlerObj.makeCloudRequest(subUrl: subUrl, parameter: parameter, isheaderAUTH: true, requestType: .put){
            response in
            
            if let json = response?.result.value {
                print("response json", json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let user = Mapper<RideCreateResponse>().map(JSON:json as! [String : Any])!
                    let data:[String:Any] = [StringConstant.data:user]
                    self.rideManagerDelegate?.getDailyTargetSuccess!(data: data)
                }
                else
                {
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    
    func getBatteryMeanBikeBatteryVoltage(){
        
        let vehicleId:Int = MyDefaults.getInt(key: DefaultKeys.KEY_DefaultVehicleID)
        
        let currentDate: Date = Date()
        let currentDateTimeFormat = currentDate.dateToStringWithFormat();
        
        let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        let yesterdayDate = yesterday!.dateToStringWithFormat()
        
        
        let customAllowedSet =  NSCharacterSet(charactersIn:":=\"#%/<>?@\\^`{|}-+").inverted
        
        let endString = currentDateTimeFormat.addingPercentEncoding(withAllowedCharacters: customAllowedSet)
        let startString = yesterdayDate.addingPercentEncoding(withAllowedCharacters: customAllowedSet)
        
        self.cloudHandlerObj.makeCloudRequest(subUrl:"\(RequestURL.URL_VEHICLES.rawValue)\(vehicleId)\(RequestURL.URL_MEANBIKEBATTERYVOLTAGE.rawValue)?startDateTime=\(startString!)&endDateTime=\(endString!)", parameter:["":""], isheaderAUTH: true, requestType: .get)
        { response in
            if let json = response?.result.value
                
            {print(json)
                if response?.response?.statusCode == 200
                {
                    
                    let batteryVoltageModelObj = Mapper<MeanBikeBatteryVoltageModel>().map(JSON:response?.result.value as! [String : Any])!
                    let data:[String:Any] = [StringConstant.data:batteryVoltageModelObj]
                    self.rideManagerDelegate?.getMeanBikeBatteryVoltageSuccess!(data: data)
                }
                else
                {
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
        
        
        
    }
    
    func getBatteryHealth()
    {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
        let bikerID = (BikerDetails.getBikerDetails(context: context)?.biker_id?.toInt()) ?? 0
        
        let currentDate: Date = Date()
        let currentDateTimeFormat =  currentDate.dateToStringWithFormat();
        
        let yesterday = Calendar.current.date(byAdding: .day, value: -14, to: Date())
        let prev14Date = yesterday!.dateToStringWithFormat()
        
        let items: [String: Any] = [StringConstant.biker_id: bikerID, StringConstant.end_date:currentDateTimeFormat, StringConstant.start_date:prev14Date]
        
        self.cloudHandlerObj.makeCloudRequest(subUrl: RequestURL.URL_BATTERYVOLTAGE.rawValue, parameter:items, isheaderAUTH: true, requestType: .post)
        { response in
            if let json = response?.result.value
            {
                if response?.response?.statusCode == 200
                {
                    let batteryVoltageModelObj = Mapper<BatteryVoltageModel>().map(JSON:response?.result.value as! [String : Any])!
                    
                    var batteryVoltageItem = [String:Any]()
                    
                    batteryVoltageItem.updateValue(batteryVoltageModelObj.BatteryVoltageList, forKey: "data")
                    batteryVoltageItem.updateValue(yesterday!, forKey: "prev_date")
                    
                    self.rideManagerDelegate?.sendBatteryVoltageList!(batteryObj: batteryVoltageItem)
                }
                else
                {
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    func getRideHonorBadge(rideID:Int, bikerID:Int)
    {
        let suburl = "\(RequestURL.URL_RIDE_STATUS.rawValue)\(rideID)\(RequestURL.URL_BIKER.rawValue)\(bikerID)\(RequestURL.URL_BADGE.rawValue)"
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter:["":""], isheaderAUTH: true, requestType: .get){ response in
            
            if let json = response?.result.value {
                print("STICKER: \(json)")
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let json1 = [StringConstant.rideBadgeList:json]
                    let user = Mapper<HonorStickerResponse>().map(JSON:json1 )!
                    let data:[String:Any] = [StringConstant.data:user]
                    self.rideManagerDelegate?.rideHonorBadgeSuccess!(data: data)
                }
                else{
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    //Get Stickers for Sticker Board
    //    func getStickerBoard(bikerID:Int)
    //    {
    //        //TODO:need to put dynamic rideid
    //        let suburl = "\(RequestURL.URL_ANALYTICS_BIKERS.rawValue)\(bikerID)\(RequestURL.URL_STICKERBOARD.rawValue)"
    //        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter:["":""], isheaderAUTH: true, requestType: .get){ response in
    //
    //            if let json = response?.result.value {
    //                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
    //                {
    //                    let user = Mapper<StickerBoardResponse>().map(JSON:json as! [String : Any])!
    //                    let data:[String:Any] = [StringConstant.data:user]
    //                    self.rideManagerDelegate?.getStickerSuccess!(data: data)
    //                }
    //                else{
    //                    self.errorCall(value: json as! [String : Any])
    //                }
    //            } else {
    //                self.errorCall(value: RequestTimeOut().json)
    //            }
    //        }
    //    }
    
    
    
    //Get Stickers for Sticker Board
    func getCurrentProgressRides()
    {
        //TODO:need to put dynamic rideid
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
        let bikerID = (BikerDetails.getBikerDetails(context: context)?.biker_id?.toInt()) ?? 0
        let suburl = "\(RequestURL.URL_RIDE_BIKER.rawValue)\(bikerID)\(RequestURL.URL_CURENT_RIDEPROGRES.rawValue)"
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter:["":""], isheaderAUTH: true, requestType: .get){ response in
            
            if let json = response?.result.value {
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    print(json)
                    let json1 = [StringConstant.rideInprogresList:json]
                    let user = Mapper<RideInprogressModel>().map(JSON:json1)!
                    let data:[String:Any] = [StringConstant.data:user]
                    self.rideManagerDelegate?.getRideProgressSuccess!(data: data)
                }
                else{
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                print(response?.result.error ?? " ")
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    func getRoutesForLatLng(data:[String:Any])
    {
        let suburl = "\(RequestURL.URL_ROUTING.rawValue)"
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: data, isheaderAUTH: false, requestType: .post)
        { (response) in
            
            if let json = response?.result.value {
                print(json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300{
                    let directionDetails = Mapper<DirectionDetails>().map(JSON: json as! [String : Any])!
                    let directionDetailsData:[String:Any] = [StringConstant.data:directionDetails]
                    self.rideManagerDelegate?.sendRouteDirectionDetails!(directionResult:directionDetailsData)
                }else{
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                print(response?.result.error ?? " ")
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    
    func getMyPlannedRides(type:String, pageNo: Int, size: Int)
    {
        //http://103.243.226.150:8085/api/v1/rides/biker/joined
        //http://103.243.226.150:8085/api/v1/rides/biker/joined?page=0&size=1000&sort=dateCreated,asc
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
        let bikerID = (BikerDetails.getBikerDetails(context: context)?.biker_id?.toInt()) ?? 0
        
        var suburl = ""
        if type == "RECOMMENDED" {
            suburl = "\(RequestURL.URL_RIDE_BIKER.rawValue)\(bikerID)\(RequestURL.URL_RECOMMENDED.rawValue)"
        } else {
            suburl = "\(RequestURL.URL_RIDE_BIKER.rawValue)\(bikerID)/joined?page=\(pageNo)&size=\(size)&sort=startDate,asc"
        }
        
        
        //TODO:need to put dynamic rideid
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get) { cachedResponse in
            
            if let response = cachedResponse {
                
                let jsonString = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                if let status = response.response?.statusCode {
                    if  (status >= 200 && status < 300)
                    {
                        let user = Mapper<RideCreateResponse>().mapArray(JSONString: jsonString! as String)
                        let data:[String:Any] = [StringConstant.data:user!]
                        self.rideManagerDelegate?.getMyPlannedRideSuccess!(data: data)
                        
                    }else{
                        
                        let ErrorModelObj = Mapper<ErrorModel>().map(JSONString: jsonString! as String)
                        let errorData:[String:Any] = ["data":ErrorModelObj ?? " "]
                        self.rideManagerDelegate?.onFailure!(Error: errorData)
                    }
                }
            }else{
                
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    func getRide(rideID:Int)  {
        
        let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)"
        
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ response in
            if let json = response?.result.value {
                print("JSON ",json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    
                    let bikes = Mapper<RideCreateResponse>().map(JSON:json as! [String : Any])!
                    let data:[String:Any] = [StringConstant.data:bikes]
                    self.rideManagerDelegate?.onGetRideDetail!(data: data)
                    
                }
                else{
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
        
    }
    
    func leaveRide(rideID: Int) {
        let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)\(RequestURL.URL_LEAVE_RIDE.rawValue)"
        
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .post){ response in
            if let json = response?.result.value {
                print("JSON ",json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    self.rideManagerDelegate?.leaveRideSuccess!()
                }
                else{
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    
    func getRideRouteById(rideID:Int,completionHandler: @escaping (DataResponse<Any>?) -> Void)
    {
        DispatchQueue.global(qos: .background).async {
            let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)"
            self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ response in
                completionHandler(response)
            }
        }
    }
    
    
    func getRideRouteById(rideID:Int,bikerId:Int)
    {
        
       if isRideIDPresentIntoDB(rideId: rideID, table: SQLTableType.PLANNED_ROUTE) {
            let arr:[PlannedRoute] = self.fetchAllTableRecordWithRespID(table: SQLTableType.PLANNED_ROUTE, id: rideID) as! [PlannedRoute]
            print(arr)
            let objPlanned:PlannedRoute = arr.last!
            let directionDetails : SingleRouteDetails = SingleRouteDetails()
            directionDetails.tbt_route = objPlanned.planRideTBTRoute
            directionDetails.route = objPlanned.planRideRoute
            directionDetails.distance = 0
            let directionDetailsData:[String:Any] = [StringConstant.data:directionDetails]
            self.rideManagerDelegate?.sendRouteDirectionDetails!(directionResult:directionDetailsData)
            
        } else {
            
            var suburl = ""
            if (bikerId == 0) {
                suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)"
            }else{
                suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)\(RequestURL.URL_BIKER.rawValue)\(bikerId)"
            }
            
                self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get, completionHandler: { response in
                    if let json = response?.result.value {
                        if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                        {
                            let directionDetails = Mapper<SingleRouteDetails>().map(JSON: json as! [String: Any])
                            let directionDetailsData:[String:Any] = [StringConstant.data:directionDetails!]
                            self.rideManagerDelegate?.sendRouteDirectionDetails!(directionResult:directionDetailsData)
                        } else {
                            self.errorCall(value: json as! [String : Any])
                        }
                    } else {
                        self.errorCall(value: RequestTimeOut().json)
                    }
                })
        
            }
    }
    
    
    
    
    func getPOI(rideID:Int,isInsertintoDB:Bool)  {
        
        let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)\(RequestURL.URL_POI.rawValue)"
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ response in
            if let json = response?.result.value {
                print("poi response", json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let json1 = [StringConstant.getPOIList:json]
                    let poi = Mapper<GetRidePOIModel>().map(JSON:json1 )!
                    let data:[String:Any] = [StringConstant.data:poi]
                    
                    if isInsertintoDB{
                        self.insertPOIintoDB(data: data, poi_rideID: rideID)
                    }
                    else
                    {
                        self.rideManagerDelegate?.getPOISuccess!(data: data)
                    }
                }
                else
                {
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    func getPOIWithPolyline(polyline: String) {
        
        let suburl = "\(RequestURL.URL_LOCATION.rawValue)\(RequestURL.URL_POI_POLYLINE.rawValue)?polyline=\(polyline.urlEncoded())&radius=1000"
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter:[:],isheaderAUTH: true, requestType: .get) { response in
            if let json = response?.result.value {
              //  print(json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let json1 = [StringConstant.getPOIList:json]
                    let poi = Mapper<GetRidePOIModel>().map(JSON:json1 )!
                    let data:[String:Any] = [StringConstant.data:poi]
                    self.rideManagerDelegate?.getPOISuccess!(data: data)
                }
                else{
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    func getPlannedWavepoints(rideID:Int)  {
        
        let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)\(RequestURL.URL_PlANNED_WAVEPOINTS.rawValue)"
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ response in
            if let json = response?.result.value {
              //  print(json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let json1 = [StringConstant.wavePointsList:json]
                    let poi = Mapper<PlannedWavePointsModel>().map(JSON:json1 )!
                    let data:[String:Any] = [StringConstant.data:poi]
                    self.rideManagerDelegate?.getWavePointsSuccess!(data: data)
                }
                else{
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
        
    }
    
    
    func getPlannedWaypoints(rideID:Int,completionHandler: @escaping (DataResponse<Any>?) -> Void) {
        
        let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)\(RequestURL.URL_PlANNED_WAVEPOINTS.rawValue)"
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ response in
            completionHandler(response)
        }
    }
    
    
    func postPlannedWaypointsList(rideID:Int, selectedPOIArray:[POIListResponse])
    {
        //print(rideID)
        
        var i = 0
        let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)\(RequestURL.URL_PLANNED_WAVEPOINTS_LIST.rawValue)"
        
        let jsonArrayObject: NSMutableArray = NSMutableArray()
        
        for poi in selectedPOIArray{
            print(poi)
            
            let jsonObject: NSMutableDictionary = NSMutableDictionary()
            jsonObject.setValue(poi.name, forKey: StringConstant.NAME)
            let date = Date()
            jsonObject.setValue(date.dateToStringWithFormatTTTT(), forKey: StringConstant.DATE_ADDED)
            
            
            let dic = poi.categories
            jsonObject.setValue(dic, forKey: StringConstant.CATEGORIES)
            
            let dic2 = [StringConstant.ALTITUDE : poi.cllLocation?.altitude, StringConstant.LATITUDE : poi.cllLocation?.coordinate.latitude, StringConstant.LONGITUDE : poi.cllLocation?.coordinate.longitude]
            jsonObject.setValue(dic2, forKey: StringConstant.LOCATION)
            
            if let poiTypes = poi.types{
                jsonObject.setValue(poiTypes, forKey:StringConstant.TYPES )
            }else{
                
                let poitype = [StringConstant.PLACES_OF_INTEREST]
                jsonObject.setValue(poitype, forKey:StringConstant.TYPES )
                
            }
            
            jsonObject.setValue(poi.poiId, forKey: StringConstant.POI_ID)
            if let db_id = poi.wavePoint_id{
                jsonObject.setValue(db_id, forKey: StringConstant.ID)
            }
            
            
            jsonObject.setValue(i+1, forKey: StringConstant.ROUTE_ORDER)
            jsonArrayObject.add(jsonObject)
            i = i+1
        }
        
        //        for poi in jsonArrayObject{
        //            print("**************************")
        //            print(poi)
        //        }
        
        
        cloudHandlerObj.makeCloudPostRequestWithArray(subUrl: suburl, parameter:jsonArrayObject, isheaderAUTH: true, requestType: .post){ response in
            
            if let json = response?.result.value {
                print("Response")
                print(json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    
                    let json1 = [StringConstant.wavePointsList:json]
                    let poi = Mapper<PlannedWavePointsModel>().map(JSON:json1 )!
                    let data:[String:Any] = [StringConstant.data:poi]
                    self.rideManagerDelegate?.postPlannedWaypointsListSuccess!(data: data)
                    print("@Ridemanager: Asyncronously TBT route insertion called")
                    self.fetchTBTRouteAndPlannedWayPoints(rideID: rideID,isInsert: false)
                    
                }
                else
                {
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    func getPOIDetails(poiId:String)
    {
        let suburl = Constants.apiKeyGooglePOIDetailsURL+poiId+Constants.apiKeyGooglePOIDetailsURLPART2+Constants.APIKEY_MAPS!
        
        self.cloudHandlerObj.makeAccuWeatherCloudRequest(subUrl: suburl, parameter:["":""], isheaderAUTH: false, requestType: .get){ response in
            
            if let json = response?.result.value {
                //print(json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let poi = Mapper<GogglePOIDetailsModel>().map(JSON: json as! [String : Any])
                    let data:[String:Any] = [StringConstant.data:poi!]
                    self.rideManagerDelegate?.getPOIDetailsSuccess!(data: data)
                    
                }
                else {
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    

    
    func errorCall(value:[String : Any]!){
        let ErrorModelObj = Mapper<ErrorModel>().map(JSON:value)
        let errorData:[String:Any] = ["data":ErrorModelObj ?? " "]
        self.rideManagerDelegate?.onFailure!(Error: errorData)
    }
    
    
    // MARK:SQLite Methods - POI
    func deg2rad(deg:Double) -> Double {
        return (deg * M_PI / 180.0);
    }
    
    func sind(degrees: Double) -> Double {
        return sin(degrees)
    }
    
    func cosd(degrees: Double) -> Double {
        return cos(degrees)
    }
    
    func isRideIDPresentIntoDB(rideId:Int,table:SQLTableType) -> Bool {
        
        var flag = false
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        do {
            try db?.createTable(table: PointsOfInterests.self)
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        do {
            
            switch table {
            case SQLTableType.POINT_OF_INTEREST:
                let arrRecord = try db?.fetchAllTableRecordWithRespID(table: SQLTableType.POINT_OF_INTEREST, queryString: DBQuery.QUERY_FETCH_IS_RIDEID, id: Int32(rideId))
                
                if (arrRecord?.count)! > 0 {
                    flag = true
                }
                
                break
            case SQLTableType.PLANNED_ROUTE:
                let arrRecord = try db?.fetchAllTableRecordWithRespID(table: SQLTableType.PLANNED_ROUTE, queryString: DBQuery.QUERY_FETCH_IS_ROUTE, id: Int32(rideId))
                
                if (arrRecord?.count)! > 0 {
                    flag = true
                }
                break
                
            case SQLTableType.RECEIVE_ALERT:
                let arrRecord = try db?.fetchAllTableRecordWithRespID(table: SQLTableType.PLANNED_ROUTE, queryString: DBQuery.QUERY_FETCH_IS_ROUTE, id: Int32(rideId))
                
                if (arrRecord?.count)! > 0 {
                    flag = true
                }
                break
            case SQLTableType.SENDING_ALERT:
                let arrRecord = try db?.fetchAllTableRecordWithRespID(table: SQLTableType.PLANNED_ROUTE, queryString: DBQuery.QUERY_FETCH_IS_ROUTE, id: Int32(rideId))
                
                if (arrRecord?.count)! > 0 {
                    flag = true
                }
                break
                
                
            }
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        return flag
    }
    
    func insertPOIintoDB(data:[String:Any],poi_rideID:Int) {
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        do {
            try db?.createTable(table: PointsOfInterests.self)
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        DispatchQueue.background(background: {
            // do something in background
            
            if let obj:GetRidePOIModel = data["data"] as? GetRidePOIModel {
                if (obj.POIList?.count)! > 0 {
                    
                    if let poiList = obj.POIList{
                        do {
                            try db?.insertIntoTable(table: SQLTableType.POINT_OF_INTEREST, queryString: DBQuery.QUERY_POINT_OF_INTREST_INSERT_INTO, data:poiList)
                            
                        } catch {
                            print(db?.errorMessage ?? "error inserting")
                        }
                    }
                    
                }else{
                    
                }
            }
            
            
        }, completion:{
            // when background job finished, do something in main thread
            print("Insertion completed !!! ")
            self.fetchTBTRouteAndPlannedWayPoints(rideID: poi_rideID,isInsert: true)
            //            self.fetchTBTRouteAndPlannedWayPoints(rideID: poi_rideID
            
        })
    }
    
    // MARK:SQLite Methods -  PlannedRoute
    func stringToJson(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func jsonToString(json: AnyObject) -> String{
        
        var convertedString : String = ""
        do {
            
            
            let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            convertedString = String(data: data1, encoding: String.Encoding.utf8)! // the data will be converted to the string
            //print("converted string - ",convertedString ) // <-- here is ur string
            
        } catch let myJSONError {
            print(myJSONError)
        }
        return convertedString
    }
    
    func fetchTBTRouteAndPlannedWayPoints(rideID:Int,isInsert:Bool)  {
        
        var wayPointJsonStr:String = ""
        getPlannedWaypoints(rideID: rideID) {
            response in
            if let json = response?.result.value {
                wayPointJsonStr = self.jsonToString(json: json as AnyObject)
                //3 seconds = 3e+9 nanoseconds
                DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 3, execute: {

                    self.getRideRouteById(rideID: rideID){
                        response in
                        if let json = response?.result.value{
                            print("@Ridemanager: Cached planned route & TBT Route Success ")
                            print(json)
                            print("############################################")
                            
                            if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300{
                                let directionDetails = Mapper<SingleRouteDetails>().map(JSON: json as! [String : Any])
                                print("@Ridemanager: TBT Route inserting............")
                                if let route = directionDetails?.route, let tbtRoute = directionDetails?.tbt_route {
                                    DispatchQueue.global(qos: .background).async {
                                        if self.isRideIDPresentIntoDB(rideId: rideID, table: SQLTableType.PLANNED_ROUTE) {
                                            self.insertValueIntoDB(rideID: rideID, route: route, tbtRoute: tbtRoute, wayPnt: wayPointJsonStr,isInsert: false)
                                        } else {
                                            self.insertValueIntoDB(rideID: rideID, route: route, tbtRoute: tbtRoute, wayPnt: wayPointJsonStr,isInsert: isInsert)
                                        }
                                    }
                                }
                                
                            }
                        }
                        
                    }
                })
            }
        }
    }
    
    //MARK: InsertInto SQLITE
    func insertValueIntoDB(rideID:Int,route:String,tbtRoute:String,wayPnt:String,isInsert:Bool)  {
        
        print("@Ridemanager: rideID ",rideID," ROUTE ",route," TBTROUTE ",tbtRoute," waypnt ",wayPnt)
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("@Ridemanager: Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        
        do {
            try db?.createTable(table: PlannedRoute.self)
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        do{
            if isInsert {
                try db?.insertIntoTable(table: SQLTableType.PLANNED_ROUTE, queryString: DBQuery.QUERY_PLANNED_ROUTE_INSERT_INTO, data: PlannedRoute(id: Int32(rideID), planRideRoute: route != nil ? route:"", planRideWayPoints: wayPnt != nil ? wayPnt:"", planRideTBTRoute: tbtRoute != nil ? tbtRoute:""))
            }
            else
            {
                try db?.deleteTableRecordWithRespID(table: SQLTableType.PLANNED_ROUTE, queryString: DBQuery.QUERY_PLANNED_ROUTE_DELETE_ROW, id: Int32(rideID))

                try db?.insertIntoTable(table: SQLTableType.PLANNED_ROUTE, queryString: DBQuery.QUERY_PLANNED_ROUTE_INSERT_INTO, data: PlannedRoute(id: Int32(rideID), planRideRoute: route != nil ? route:"", planRideWayPoints: wayPnt != nil ? wayPnt:"", planRideTBTRoute: tbtRoute != nil ? tbtRoute:""))
            }
            
        } catch {
            print(db?.errorMessage ?? "error Message while inserting a value intoTable")
        }
        
    }
    
    //MARK: FETCH RECORD FROM SQLITE
    func fetchAllTableRecordWithRespID(table:SQLTableType,id: Int) ->[Any] {
        
        var arrResult:[Any]?
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("@Ridemanager: Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        do {
            try db?.createTable(table: PointsOfInterests.self)
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        do {
            
            switch table {
            case SQLTableType.POINT_OF_INTEREST:
                //                print("count ",arrResult.count)
                break
                
            case SQLTableType.PLANNED_ROUTE:
                
                arrResult = try db?.fetchAllTableRecordWithRespID(table:SQLTableType.PLANNED_ROUTE, queryString: DBQuery.QUERY_FETCH_PLANNED_ROUTE_BY_ID, id: Int32(id))
                
                let arrr = arrResult as! [PlannedRoute]
                print("arrResult route ",arrr.get(at: 0)?.planRideRoute ?? "")
                break
            default:
                break
            }
            
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        
        return arrResult!
        
    }
}

