//
//  AlertStateCluster.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 06/10/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
//ahead
struct DirectionsConstant {
    static let behind:String    = "BEHIND"
    static let ahed:String      = "AHED"
}

class AlertStateCluster: AlertState {
    
    
    var alertDetails:AlertClusterResponseModel?
    
    init(alertDetails:AlertClusterResponseModel) {
        self.alertDetails = alertDetails
        self.sendToDM()
    }
    
    func sendToDM(){
        
        var isBehind:Bool = false
        
        if (alertDetails?.direction == DirectionsConstant.behind){
            isBehind = true
        }
        
        var value:Int32?
        if isBehind {
            value = Int32(CP_NonUrgentAlertIdTypeDef(rawValue: 6).rawValue)
        }else{
            value = Int32(CP_NonUrgentAlertIdTypeDef(rawValue: 5).rawValue)
        }
        
        CPWrapper.sharedInstance().cp_showNonUrgentIncomingAlert(value!, andAlertType: CP_AlertTypeInfoTypeDef(rawValue: 1), andUUID: 0, andStructure: nil)
    }
}
