//
//  AlertStateHelpOnWay.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 28/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class AlertStateHelpOnWay: AlertState {

    let alertDetail:AlertApiResponseModel?
    
    init(alertDetail:AlertApiResponseModel) {
        self.alertDetail = alertDetail
        self.updateDatabase()
        self.notifyState()
    }
    
    func updateDatabase(){
        
        
    }
    
    func notifyState(){
        
        let sinkManager = SinkManager()
        sinkManager.notifyObservers(notificationKey: SinkManager.ALERT_NOTIFICATION_HELP_ON_WAY, sender: self)
        
        
    }

}
