 //
 //  AlertStateCheckAndShowAlertState.swift
 //  Maximus
 //
 //  Created by Appsplanet Macbook on 17/09/17.
 //  Copyright © 2017 com.kpit.maximus. All rights reserved.
 //
 
 import UIKit
 
 class AlertStateCheckAndShowAlertState: AlertState {
    
    var alertState:Int? //= MyDefaults.getInt(key: AlertConstant.KEY_ALERT_STATE)
    let alertDbManager = AlertDatabaseManager()
    var alertId:Int32?
    var alertHelperBikerName:String?
    var alertType:String?
    
    func checkIfOpenSentAlerts(){
        let sentAlerts = self.alertDbManager.getAlertsFromSentAlertTableWithStatus(status: AlertConstant.DB_STATE_SENT)
        let arrSentAlerts = sentAlerts as! [SendingAlert]
        
        let reSentAlerts = self.alertDbManager.getAlertsFromSentAlertTableWithStatus(status: AlertConstant.DB_STATE_RESEND)
        let arrReSentAlerts = reSentAlerts as! [SendingAlert]
        
        
        
        var total_alerts = [SendingAlert]()
        
        if arrSentAlerts.count > 0 && arrReSentAlerts.count > 0
        {
            total_alerts = arrSentAlerts + arrReSentAlerts
        }else if arrReSentAlerts.count > 0{
            total_alerts = arrReSentAlerts
            
        }else if arrSentAlerts.count > 0{
            
            total_alerts = arrSentAlerts
        }
        
        if total_alerts.count > 0{
            self.alertState = AlertConstant.KEY_ALERT_STATE_SENT
            
            if arrReSentAlerts.count > 0 {
                self.alertId = arrReSentAlerts.get(at: 0)?.alertId
                self.alertType = arrReSentAlerts.get(at: 0)?.alertShortName
                
            }else{
                self.alertId = arrSentAlerts.get(at: 0)?.alertId
                self.alertType = arrSentAlerts.get(at: 0)?.alertShortName
            }
        }
    }
    
    func checkIfOpenSentDeliveredAlerts(){
        let sentAlerts = self.alertDbManager.getAlertsFromSentAlertTableWithStatus(status: AlertConstant.DB_STATE_DELIVERED)
        let arrSentAlerts = sentAlerts as! [SendingAlert]
        if arrSentAlerts.count > 0{
            self.alertState = AlertConstant.KEY_ALERT_STATE_SENT
            self.alertId = arrSentAlerts.get(at: 0)?.alertId
            self.alertType = arrSentAlerts.get(at: 0)?.alertShortName
        }
    }
    
    func checkIfOpenSentHelpOnWayAlerts(){
        let sentAlerts = self.alertDbManager.getAlertsFromSentAlertTableWithStatus(status: AlertConstant.DB_STATE_ACKNOWLEDGED)
        let arrSentAlerts = sentAlerts as! [SendingAlert]
        if arrSentAlerts.count > 0{
            self.alertState = AlertConstant.KEY_ALERT_STATE_HELP_ON_WAY
            self.alertId = arrSentAlerts.get(at: 0)?.alertId
            self.alertHelperBikerName = (arrSentAlerts.get(at: 0)?.alertRespondantBikerName)
        }
    }
    
    func checkIfOpenReceivedAlerts(){
        let arrResult = alertDbManager.getAlertsFromReceivedAlertTableWithStatus(status: AlertConstant.DB_STATE_RECEIVED)
        let alertsArray = arrResult as! [ReceiveAlert]
        if alertsArray.count > 0{
            self.alertState = AlertConstant.KEY_ALERT_RECEIVED
        }
    }
    
    func checkIfOpenReceivedAckAlerts(){
        let arrResult = alertDbManager.getAlertsFromReceivedAlertTableWithStatus(status: AlertConstant.DB_STATE_ACKNOWLEDGED)
        let alertsArray = arrResult as! [ReceiveAlert]
        if alertsArray.count > 0{
            self.alertState = AlertConstant.KEY_ALERT_RECEIVED
        }
    }
    
    
    init() {
        
        print("ALERTS LOG:- ALERT STATE CHECK AND SHOW ALERT STATE")
        
        checkIfOpenSentHelpOnWayAlerts()
        
        if alertState == nil {
            checkIfOpenSentDeliveredAlerts()
        }
        
        if alertState == nil {
            checkIfOpenSentAlerts()
        }
        
        if alertState == nil {
            checkIfOpenReceivedAckAlerts()
        }
        
        if alertState == nil {
            checkIfOpenReceivedAlerts()
        }
        
        if let alertState = self.alertState{
            switch alertState {
            case AlertConstant.KEY_ALERT_STATE_SENT:
                print("ALERTS LOG:- ALERT STATE SENT DETECTED")
                JourneyManager.sharedInstance.setAlertModeState(state: AlertModeStatus.On)
                
                let vcAlert = AlertSentVC()
                if let alertId =  self.alertId {
                    print("ALERTS LOG:- \(self.alertId)")
                    vcAlert.generatedAlertId = Int(alertId)
                }
                
                if let type = self.alertType {
                    vcAlert.alertType = type
                }
                UIApplication.topViewController()?.presentVC(vcAlert)
                
            case AlertConstant.KEY_ALERT_STATE_HELP_ON_WAY:
                print("ALERTS LOG:- ALERT STATE HELP ON WAY DETECTED")
                JourneyManager.sharedInstance.setAlertModeState(state: AlertModeStatus.On)
                
                
                let resumeRideVc = ResumeRideVC()
                if let alertId =  self.alertId {
                    print("ALERTS LOG:- \(alertId)")
                    resumeRideVc.generatedAlertId = "\(alertId)"
                }
                if let bikerName =  self.alertHelperBikerName {
                    print("ALERTS LOG:- \(bikerName)")
                    resumeRideVc.helperBikerName = bikerName
                }
                UIApplication.topViewController()?.presentVC(resumeRideVc)
                
            case AlertConstant.KEY_ALERT_RECEIVED:
                print("ALERTS LOG:- ALERT STATE RECEIVED DETECTED")
                
                let context = AlertContext()
                context.changeStateToShowReceivedAlert()
                
            default:
                break
            }
        }
    }
 }
 
 
 /* if let alertId =  MyDefaults.getData(key: AlertConstant.KEY_GENERATED_ALERT_ID) as? String{
  receivedAlertVc.generatedAlertId = alertId
  }
  if let bikerName =  MyDefaults.getData(key: AlertConstant.KEY_BIKER_NAME) as? String{
  receivedAlertVc.bikerName = bikerName
  }
  if let uuid =  MyDefaults.getData(key: AlertConstant.KEY_UUID) as? String{
  receivedAlertVc.uuid = uuid
  }
  if let alertType =  MyDefaults.getData(key: AlertConstant.KEY_GENERATED_ALERT_TYPE) as? String{
  receivedAlertVc.generatedAlertType = alertType
  }
  if let bikerId =  MyDefaults.getData(key: AlertConstant.KEY_GENERATED_BIKER_ID) as? String{
  receivedAlertVc.generatedBikerId = bikerId
  }*/
 
 
