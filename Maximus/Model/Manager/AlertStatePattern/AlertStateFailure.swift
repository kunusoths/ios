//
//  AlertStateFailure.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 14/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class AlertStateFailure: AlertState {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var messageFailure:String = "Error Sending Alert"
    
    init(messageFailure:String) {
        self.messageFailure = messageFailure
        print("ALERTS LOG:- ALERT STATE FAILURE \(messageFailure)")
        
        PopUpUtility().showDefaultPopUpWith(message: self.messageFailure)
        CPWrapper.sharedInstance().cp_AlertFailed_Outgoing( CP_AlertSentTypeDef(rawValue: 0))
    }
}

