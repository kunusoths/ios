//
//  AlertStateSendDeliveryAck.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 14/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class AlertStateSendDeliveryAck: AlertState,AlertApiManagerDelegate {
    
    let alertUuid:String?
    let alertManager:AlertApiManager = AlertApiManager()
    
    init(alertUuid:String) {
        self.alertUuid = alertUuid
        alertManager.alertManagerDelegate = self
    
        print("ALERTS LOG:- ALERT STATE SEND DELIVERY ACK \(alertUuid)")
        self.makeApiCallToDeliveryAck()
    }
    
    func makeApiCallToDeliveryAck(){
        if let alertUuid = alertUuid {
            alertManager.sentAlertDeliveryAckToSender(uuid: alertUuid)
        }
    }
    
    //MARK:- AlertApiManager Delegates
    func alertDeliveredAckSuccess(message: String) {
        print("ALERTS LOG:- \(message)")
    }
    
    func onFailure(Error: [String : Any]) {
       let errModel:ErrorModel = Error["data"] as! ErrorModel
         print("ALERTS LOG:- \(errModel.description)")
        print()
    }
}
