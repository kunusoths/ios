//
//  AlertStateDiscard.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 28/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class AlertStateDiscard: AlertState {
    
    //let alertDetail:AlertApiResponseModel?
    let alertId:Int32?
    let alertDbManager = AlertDatabaseManager()
    let sinkManager = SinkManager.sharedInstance
    
    //init(alertDetail:AlertApiResponseModel) {
    init(alertId:Int32) {
        self.alertId = alertId
        print("ALERTS LOG:- ALERT STATE DISCARD\(self.alertId)")
        self.updateDatabase()
        
        if JourneyManager.sharedInstance.getAlertSendingType() != AlertSendingType.OptingOut{
            self.updateDM()
        }else{
            JourneyManager.sharedInstance.setAlertSendingType(type: AlertSendingType.Unknown)
        }
        self.notifyState()
    }
    
    func updateDatabase(){
        alertDbManager.updateReceivedAlertStatusWith(status: AlertConstant.DB_STATE_DISCARD, havingAlertid: self.alertId!)
    }
    
    func updateDM(){
        CPWrapper.sharedInstance().cp_AlertResponseSent_Incoming(12, andAlertType: CP_AlertTypeInfoTypeDef(rawValue: 2))
    }
    
    func notifyState(){
        
        sinkManager.notifyObservers(notificationKey: sinkManager.ALERT_NOTIFICATION_DISCARDED, sender: self)
    }
    
    func updateAppUI(){
        let arrResult = alertDbManager.getAlertsFromReceivedAlertTableWithStatus(status:AlertConstant.DB_STATE_RECEIVED)
        if arrResult.count != 0{
            let context = AlertContext()
            context.changeStateToCheckAndShowAlertState()
            
        }
    }
}
