//
//  AlertStateShowAlert.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 17/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class AlertStateShowAlert: AlertState {
    
    //variable indicates whether update alert state to closed state should be done or not.
    static var alertUpdateToClosedState:Bool = true
    init() {
        print("ALERTS LOG:- ALERT STATE SHOW ALERT")
        self.getAlertWithHightestPriorityIfAny()
    }
    
    func getAlertWithHightestPriorityIfAny(){
        
        let alertDbManager = AlertDatabaseManager()
        
        let arrResult = alertDbManager.getAlertsFromReceivedAlertTableWithStatus(status: AlertConstant.DB_STATE_RECEIVED)
        print("ALERTS LOG:- ALL OPEN ALERTS \(arrResult)")
        let alertsArray = arrResult as! [ReceiveAlert]
        var alertCategory:[String] = []
        var alertShortCode:[String] = []
        let journeyManager = JourneyManager.sharedInstance
        if alertsArray.count > 1 && journeyManager.isAnyRideInProgress(){
            
            for alert in alertsArray{
                print("\n\nALERTS LOG:- ALL OPEN ALERTS \(alert.alertId)\t \(alert.alertOriginatedByBikerName)\t \(alert.alertShortName)\t \(alert.alertUUID)")
                alertCategory.append(alert.alertCategory)
                alertShortCode.append(alert.alertShortName)
            }
            
            var indexALERT = -1
            
            if alertCategory.contains(AlertConstant.TYPE_CRITICAL_STRING){
                indexALERT = alertCategory.index(of: AlertConstant.TYPE_CRITICAL_STRING)!
            }
            
            if indexALERT == -1 {
                
                if alertCategory.contains(AlertConstant.TYPE_URGENT_STRING){
                indexALERT = alertCategory.index(of: AlertConstant.TYPE_URGENT_STRING)!
                }
                
                if alertCategory.contains(AlertConstant.TYPE_URGENT_STRING){
                let indexes = alertCategory.indexes(of: AlertConstant.TYPE_URGENT_STRING)
                    
                    for index in indexes{
                        if alertShortCode[index] == AlertConstant.TYPE_SEVERE_CONDITION_STRING{
                            indexALERT = index
                            break
                        }
                        
                        if alertShortCode[index] == AlertConstant.TYPE_PUNCTURE_STRING{
                            indexALERT = index
                            break
                        }
                        
                        if alertShortCode[index] == AlertConstant.TYPE_NO_FUEL_STRING{
                            indexALERT = index
                            break
                        }
                    }
                }
            }
            
            if indexALERT == -1 {
                if alertCategory.contains(AlertConstant.TYPE_NON_URGENT_STRING){
                    indexALERT = alertCategory.index(of: AlertConstant.TYPE_NON_URGENT_STRING)!
                }
            }
            
            if let alert = alertsArray.get(at: indexALERT)
            {
                //when alerts recevied are more than one, all alerts except the one with highest priority is updated to closed state.
                for index in 0...alertsArray.count - 1{
                    if index != indexALERT{
                        AlertStateShowAlert.alertUpdateToClosedState = false
                        print("alert closed: " + (alertsArray.get(at: index)?.alertCategory)!)
                        alertDbManager.updateReceivedAlertStatusWith(status: AlertConstant.DB_STATE_CLOSED, havingAlertid: Int32(alertsArray.get(at: index)!.alertId))
                    }
                }
                self.presentReceivedAlertIfAny(alertId: "\(alert.alertId)", bikerName: alert.alertOriginatedByBikerName, uuid: alert.alertUUID, alertType: alert.alertShortName, bikerId: "\(alert.alertOriginatedByBikerId)")
            }
        }else if alertsArray.count > 0 && journeyManager.isAnyRideInProgress(){
            // default case when one alert is received.
            if let alert = alertsArray.get(at: 0){
                self.presentReceivedAlertIfAny(alertId: "\(alert.alertId)", bikerName: alert.alertOriginatedByBikerName, uuid: alert.alertUUID, alertType: alert.alertShortName, bikerId: "\(alert.alertOriginatedByBikerId)")
            }
        }
        else{
            // closing all open alerts if any after ride is stopped
            for alert in alertsArray{
                AlertStateShowAlert.alertUpdateToClosedState = false
                print("alert closed: " + alert.alertCategory)
                alertDbManager.updateReceivedAlertStatusWith(status: AlertConstant.DB_STATE_CLOSED, havingAlertid: Int32(alert.alertId))
            }
        }
    }
    
    func presentReceivedAlertIfAny(alertId:String, bikerName:String, uuid:String, alertType:String, bikerId:String){
        
        print("ALERTS LOG:- ALERT SHOWN \(alertId) \(bikerName) \(uuid) \(alertType) \(bikerId)")
        
        let receivedAlertVc = ReceiveAlertVC()
        receivedAlertVc.generatedAlertId = alertId
        receivedAlertVc.bikerName = bikerName
        receivedAlertVc.uuid = uuid
        receivedAlertVc.generatedAlertType = alertType
        receivedAlertVc.generatedBikerId = bikerId
      
        weak var pvc = UIApplication.topViewController()?.presentingViewController
        if pvc != nil {
            UIApplication.topViewController()?.dismiss(animated: true, completion: {
                pvc?.present(receivedAlertVc, animated: true, completion: nil)
            })
        }else{
            UIApplication.topViewController()?.presentVC(receivedAlertVc)
        }
    }
}
