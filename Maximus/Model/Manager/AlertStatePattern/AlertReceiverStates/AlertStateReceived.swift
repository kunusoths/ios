//
//  AlertStateReceived.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 28/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class AlertStateReceived: AlertState {
    
    let alertDetail:AlertNotificationResponseModel?
    let alertDbManager = AlertDatabaseManager()
    var showAlert:Bool = true
    
    func checkIfOpenSentAlerts(){
        let sentAlerts = self.alertDbManager.getAlertsFromSentAlertTableWithStatus(status: AlertConstant.DB_STATE_SENT)
        let arrSentAlerts = sentAlerts as! [SendingAlert]
        if arrSentAlerts.count > 0{
            showAlert = false
        }
        
        let reSentAlerts = self.alertDbManager.getAlertsFromSentAlertTableWithStatus(status: AlertConstant.DB_STATE_RESEND)
        let arrReSentAlerts = reSentAlerts as! [SendingAlert]
        if arrReSentAlerts.count > 0{
            showAlert = false
        }
    }
    
    
    func checkIfOpenSentDeliveredAlerts(){
        let sentAlerts = self.alertDbManager.getAlertsFromSentAlertTableWithStatus(status: AlertConstant.DB_STATE_DELIVERED)
        let arrSentAlerts = sentAlerts as! [SendingAlert]
        if arrSentAlerts.count > 0{
            showAlert = false
        }
    }
    
    func checkIfOpenSentHelpOnWayAlerts(){
        let sentAlerts = self.alertDbManager.getAlertsFromSentAlertTableWithStatus(status: AlertConstant.DB_STATE_ACKNOWLEDGED)
        let arrSentAlerts = sentAlerts as! [SendingAlert]
        if arrSentAlerts.count > 0{
            showAlert = false
        }
    }
    
    init(alertDetail:AlertNotificationResponseModel) {
        self.alertDetail = alertDetail
        print("ALERTS LOG:- ALERT STATE RECEIVED\t alertId = \(alertDetail.alert_id)\t \(alertDetail.alert_short_code)\t \(alertDetail.uuid)")
        
        
        var doUpdate = true
        
        let alerts:[ReceiveAlert] = self.alertDbManager.getAlertFromReceivedAlertTableWithId(Id: (self.alertDetail?.alert_id)!) as! [ReceiveAlert]
        
        if alerts.count > 0 {
            
            let alert = alerts[0]
            if alert.alertStatus == AlertConstant.DB_STATE_CLOSED {
                doUpdate = false
            }
        }
        
        
        if doUpdate {
            if showAlert{
                checkIfOpenSentAlerts()
            }
            if showAlert{
                checkIfOpenSentDeliveredAlerts()
            }
            if showAlert{
                checkIfOpenSentHelpOnWayAlerts()
            }
            
            if JourneyManager.sharedInstance.getAlertSendingType() == AlertSendingType.AboutToSent{
                showAlert = false
            }
            
            if JourneyManager.sharedInstance.getAlertMode() == AlertModeStatus.On{
                showAlert = false
            }
            
            if showAlert{
                self.updateDatabase()
                if showAlert{
                    self.updateDMState()
                }
                
            }
        }
    }
    
    func updateDatabase(){
        
        
        let alerts:[ReceiveAlert] = self.alertDbManager.getAlertFromReceivedAlertTableWithId(Id: (self.alertDetail?.alert_id)!) as! [ReceiveAlert]
        
        if alerts.count > 0 {
            
            let alert = alerts[0]
            if alert.alertStatus != AlertConstant.DB_STATE_RECEIVED {
                alertDbManager.updateReceivedAlertStatusWith(status: AlertConstant.DB_STATE_RECEIVED, havingAlertid: Int32((self.alertDetail?.alert_id)!)!)
                
            }else{
                self.showAlert = false
                
            }
            
            
        }else{
            
            alertDbManager.insertReceivedAlertDetails(alertId: Int32((self.alertDetail?.alert_id)!)!,
                                                      alertRideId: Int32((self.alertDetail?.ride_id)!)!,
                                                      alertUUID: (self.alertDetail?.uuid)!,
                                                      alertStatus: AlertConstant.DB_STATE_RECEIVED,
                                                      alertOriginatedByBikerId: Int32((self.alertDetail?.originator_biker_id)!)!,
                                                      alertOriginatedByBikerName: (self.alertDetail?.originator_biker_name)!,
                                                      alertShortName: (self.alertDetail?.alert_short_code)!,
                                                      alertCategory: (self.alertDetail?.category)!,
                                                      alertPriority: Int32((self.alertDetail?.alert_priority)!)!,
                                                      alertHelped: Int32(0),
                                                      alertLocation: (self.alertDetail?.originator_location)!)
        }
    }
    
    func updateDMState(){
        
        var strNameInitials = ""
        
        if let name = self.alertDetail?.originator_biker_name {
            if name.contains(" "){
                strNameInitials = name.getInitialsFromFullNames()
            }else{
                if (name.length > 2) {
                    strNameInitials = name.substring(to:name.index(name.startIndex, offsetBy: 2))
                }
            }
        }
        
        let alertId = alertDbManager.getAlertIdByType(alertType: (alertDetail?.alert_short_code)!)
        let alertCategory = alertDbManager.getCategoryIdByName(category: (alertDetail?.category)!)
        let uuid = alertDetail?.alert_id?.toInt()
        
        CPWrapper.sharedInstance().cp_showUrgentIncomingAlert(Int32(alertId), andAlertType: CP_AlertTypeInfoTypeDef(rawValue: alertCategory), andUUID: Int32(uuid!), andName: strNameInitials)
    }
    
    func updateAppUIState(){
        let context = AlertContext()
        context.changeStateToShowReceivedAlert()
    }
    
    
}
