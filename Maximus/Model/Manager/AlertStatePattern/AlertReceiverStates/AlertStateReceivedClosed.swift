//
//  AlertStateReceivedClosed.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 08/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class AlertStateReceivedClosed: AlertState {
  
    let alertId:String?
    let alertDbManager = AlertDatabaseManager()
    let sinkManager = SinkManager.sharedInstance

    
    init(alertId:String) {
        self.alertId = alertId
        print("ALERTS LOG:- ALERT STATE RECEIVED CLOSED \(alertId)")
        
        self.updateDatabase()
        
        if JourneyManager.sharedInstance.getAlertSendingType() != AlertSendingType.OptingOut{
            self.updateDM()
        }else{
            JourneyManager.sharedInstance.setAlertSendingType(type: AlertSendingType.Unknown)
        }
       
        self.notifyState()
    }
    
    func updateDatabase(){
        alertDbManager.updateReceivedAlertStatusWith(status: AlertConstant.DB_STATE_CLOSED, havingAlertid: Int32(alertId!)!)
    }
    
    func updateDM(){
        let arrResult = alertDbManager.getAlertFromReceivedAlertTableWithId(Id: self.alertId!)
        //getAlertsFromReceivedAlertTableWithStatus(status: AlertConstant.DB_STATE_RECEIVED)
        let alertsArray = arrResult as! [ReceiveAlert]
        if alertsArray.count > 0{
            if let obj = alertsArray.get(at: 0){
                if "\(obj.alertId)" == self.alertId {
                    let cat = alertDbManager.getCategoryIdByName(category: obj.alertCategory)
                    let alerttype = alertDbManager.getAlertIdByType(alertType: obj.alertShortName)
                    CPWrapper.sharedInstance().cp_AlertEnd_Incoming(Int32(alerttype), andType: CP_AlertTypeInfoTypeDef(rawValue: cat), andUUID: Int32(self.alertId!.toInt()!))
                }
            }
        }
    }
    
    func notifyState(){
        
        UIApplication.topViewController()?.dismiss(animated: true, completion: {
           
        })
        sinkManager.notifyObservers(notificationKey: sinkManager.ALERT_NOTIFICATION_CLOSED, sender: self)
    }
}
