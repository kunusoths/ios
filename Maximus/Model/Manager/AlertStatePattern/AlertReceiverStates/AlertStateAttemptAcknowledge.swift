//
//  AlertStateAttemptAcknowledge.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 14/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class AlertStateAttemptAcknowledge: AlertState,AlertApiManagerDelegate {
    
    let alertManager:AlertApiManager = AlertApiManager()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var generatedAlertId:String?
    var uuid:String?
    var alertResponseType:Int?

    init(generatedAlertId:String, uuid:String, alertResponseType:Int) {
        self.generatedAlertId = generatedAlertId
        self.uuid = uuid
        self.alertResponseType = alertResponseType
        alertManager.alertManagerDelegate = self
        print("ALERTS LOG:- ALERT STATE ATTEMPT ACKNOWLEDGE")
        makeApiCall()
    }
    
    func makeApiCall(){
        print("ALERTS LOG:- \(generatedAlertId)")
        print("ALERTS LOG:- \(uuid)")
        print("ALERTS LOG:- \(alertResponseType)")
        
        alertManager.respondToReceivedAlert(responseId: alertResponseType!, generatedAlertId: generatedAlertId!, uuid: uuid!)
    }
    
    func notifyObservers(){
        //Notify VC to dismiss itself
        let sinkManager = SinkManager.sharedInstance
        sinkManager.notifyObservers(notificationKey: sinkManager.ALERT_NOTIFICATION_DISCARDED, sender: self)
    }


    //MARK:- AlertAPIManager Delegates
    func alertRespondSuccess(message: String) {
        if message == "Done"{
            self.notifyObservers()
        }
        else{
            
            /*let alert = UIAlertController(title: "MaximusPro", message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action in*/
            UIApplication.topViewController()?.view.makeToast(message)

                self.notifyObservers()
            //}))
            //UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
        }
    }
    
    func onFailure(Error: [String : Any]) {
        SinkManager.sharedInstance.notifyObservers(notificationKey: SinkManager.sharedInstance.ALERT_NOTIFICATION_ALERT_RESPONSE_FAILURE, sender: self)
         CPWrapper.sharedInstance().cp_AlertResponseFailed_Incoming(12, andAlertType: CP_AlertTypeInfoTypeDef(rawValue: 2))
        let errModel:ErrorModel = Error["data"] as! ErrorModel
        PopUpUtility().showDefaultPopUpWith(message: (errModel.description)!)
    }

}
