//
//  AlertStateAcknowledge.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 28/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class AlertStateAcknowledge: AlertState {
    
    let alertDetail:AlertApiResponseModel?
    let alertDbManager = AlertDatabaseManager()
    let sinkManager = SinkManager.sharedInstance
    
    init(alertDetail:AlertApiResponseModel) {
        self.alertDetail = alertDetail
        print("ALERTS LOG:- ALERT STATE ACKNOWLEDGE alert id = \(alertDetail.alert_id)\t id = \(alertDetail.id)\t \(alertDetail.alert_type?.short_name)")
        
        self.updateDatabase()
        
        if JourneyManager.sharedInstance.getAlertSendingType() != AlertSendingType.OptingOut{
            self.updateDM()
        }else{
          JourneyManager.sharedInstance.setAlertSendingType(type: AlertSendingType.Unknown)
        }
        self.notifyState()
        self.updateAppUI()
    }
    
    func updateAppUI(){
        let optOptVC = OptOutVC()
        optOptVC.rideID = self.alertDetail?.ride_id
        
        if let aId = self.alertDetail?.alert_id {
            optOptVC.alertId = "\(aId)"//self.alertDetail?.alert_id
        }
        
        let navAlertVC: UINavigationController = UINavigationController(rootViewController: optOptVC)
        
        
        weak var pvc = UIApplication.topViewController()?.presentingViewController
        if pvc != nil {
            UIApplication.topViewController()?.dismiss(animated: true, completion: {
                pvc?.present(navAlertVC, animated: true, completion: nil)
            })
        }else{
            UIApplication.topViewController()?.presentVC(navAlertVC)
            
        }
        
    }
    
    func updateDatabase(){
        alertDbManager.updateReceivedAlertStatusWith(status: AlertConstant.DB_STATE_ACKNOWLEDGED, havingAlertid: Int32((self.alertDetail?.alert_id)!))
    }
    
    func updateDM(){
        CPWrapper.sharedInstance().cp_AlertResponseSent_Incoming(12, andAlertType: CP_AlertTypeInfoTypeDef(rawValue: 2))
    }
    
    func notifyState(){
        sinkManager.notifyObservers(notificationKey: sinkManager.ALERT_NOTIFICATION_ACKNOWLEDGE, sender: self)
    }
}
