//
//  AlertStateReceived.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 28/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class AlertStateReceived: AlertState {
    
    let alertDetail:AlertNotificationResponseModel?
    
    init(alertDetail:AlertNotificationResponseModel) {
        self.alertDetail = alertDetail
        
        if let uuid = self.alertDetail?.uuid{
            MyDefaults.setData(value: uuid, key:AlertConstant.ALERT_UUID)
        }
        
        if let alertId = self.alertDetail?.alert_id{
            MyDefaults.setData(value: alertId, key:AlertConstant.ALERT_RECEIVED_ID)
        }
        
        self.updateDatabase()
        self.notifyState()
    }
    
    func updateDatabase(){
        
        
    }
    
    func notifyState(){
        
        let sinkManager = SinkManager()
        sinkManager.notifyObservers(notificationKey: SinkManager.ALERT_NOTIFICATION_RECEIVED, sender: self)
    }
}
