//
//  AlertContext.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 28/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation

final class AlertContext {
    
    private var state: AlertState = AlertStateUnknown()
    
    //MARK:- Alert Sender States
    func changeStateToAlertGenerate(alertType:String){
     state = AlertStateGenerate(alertType: alertType)
    }
    
    func changeStateToAlertFailure(messageFailure:String){
        state = AlertStateFailure(messageFailure:messageFailure)
    }
    
    func changeStateToAlertSend(alertDetail:AlertApiResponseModel){
        state = AlertStateSend(alertDetail:alertDetail)
    }
    
    func changeStateToAlertDelivered(alertDetail:AlertNotificationResponseModel){
        state = AlertStateDelivered(alertDetail: alertDetail)
    }

    
    func changeStateToAlertHelpOnWay(alertDetail:AlertNotificationResponseModel){
        state = AlertStateHelpOnWay(alertDetail:alertDetail)
    }
    
    func changeStateToAlertGeneratedClosed(alertId:String){
        state = AlertStateGeneratedClosed(alertId: alertId)
    }
    
    func changeStateToAlertAutoClose(alertId:Int){
        state = AlertStateAutoCloseAlert(alertId: alertId)
    }
    
    func changeStateToAlertNoHelpReceived(alertId:Int){
        state = AlertStateNoHelpReceived(alertId: alertId)
    }
    
    //MARK:- Alert Receiver States
    
    func changeStateToAlertReceived(alertDetail:AlertNotificationResponseModel){
        state = AlertStateReceived(alertDetail:alertDetail)
    }
    
    func changeStateToShowReceivedAlert(){
        state = AlertStateShowAlert()
    }
    
    func changeStateToSendDeliveryACK(alertUuid: String){
       state = AlertStateSendDeliveryAck(alertUuid: alertUuid)
    }
    
    func changeStateToAttemptAck(responseId: Int, generatedAlertId: String, uuid:String){
        state = AlertStateAttemptAcknowledge(generatedAlertId: generatedAlertId, uuid: uuid, alertResponseType: responseId)
    }
    
    func changeStateToAlertAcknowledge(alertDetail:AlertApiResponseModel){
        state = AlertStateAcknowledge(alertDetail:alertDetail)
    }
    
    func changeStateToAlertDiscard(alertId:Int32){
        state = AlertStateDiscard(alertId:alertId)
    }

    func changeStateToAlertStateReceivedClosed(alertId:String){
        state = AlertStateReceivedClosed(alertId:alertId)
    }
    
    
    func changeStateToCheckAndShowAlertState(){
    
      state = AlertStateCheckAndShowAlertState()
    }
    
    func changeStateToClusterAlert(alertDetail:AlertClusterResponseModel){
    
        state = AlertStateCluster(alertDetails:alertDetail)
    }

}


protocol AlertState {
    
    
   /*func isLegStarted(context: LegContext) -> Bool
    func isLegInProgress(context: LegContext) -> Bool
    func isLegCompleted(context: LegContext) -> Bool*/
    
}
