//
//  AlertStateNoHelpReceived.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 11/10/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class AlertStateNoHelpReceived: AlertState {
    
    var alertId:Int?
    let sinkManager = SinkManager.sharedInstance
    let alertDbManager = AlertDatabaseManager()
    
    init(alertId:Int) {
        self.alertId = alertId
        self.updateDatabase()
        self.updateAppUI()
        self.updateDM()
        
    }
    
    func updateDatabase(){
        alertDbManager.updateSentAlertStatusWith(status: AlertConstant.DB_STATE_HELP_NOT_RECEIVED, havingAlertid: Int32(self.alertId!))
        
    }
    
    func updateAppUI(){
        
        sinkManager.notifyObservers(notificationKey: sinkManager.ALERT_NOTIFICATION_RESPONSE_WAIT_TIMEDOUT, sender: self)
        
       /* weak var pvc = UIApplication.topViewController()?.presentingViewController
        if pvc != nil {
            UIApplication.topViewController()?.dismiss(animated: true, completion: {
            })
        }*/
    }
    
    func updateDM(){
        CPWrapper.sharedInstance().cp_AlertNOHelpReceived_Outgoing()
    }
}
