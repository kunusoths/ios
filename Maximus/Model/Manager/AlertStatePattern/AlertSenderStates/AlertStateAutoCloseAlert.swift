//
//  AlertStateAutoCloseAlert.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 10/10/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class AlertStateAutoCloseAlert: AlertState,AlertApiManagerDelegate {
    
    let alertDbManager = AlertDatabaseManager()
    let alertManager:AlertApiManager = AlertApiManager()
    let sinkManager = SinkManager.sharedInstance
    var alertId:Int?
    
    
    init(alertId:Int) {
        alertManager.alertManagerDelegate = self
        self.alertId = alertId
        
        self.updateDB()
        self.updateUI()
        self.notifyDM()
       
    }
    
    func updateDB(){
        
        alertDbManager.updateSentAlertStatusWith(status: AlertConstant.DB_STATE_AUTO_CANCEL, havingAlertid: Int32(self.alertId!))
    
    }
    
    func updateUI(){
         sinkManager.notifyObservers(notificationKey: sinkManager.ALERT_NOTIFICATION_ACK_WAIT_TIMEDOUT, sender: self)
      
    }
    
    func notifyDM(){
       
        CPWrapper.sharedInstance().cp_AlertFailed_Outgoing(CP_AlertSentTypeDef(rawValue: 0))
    }
}
