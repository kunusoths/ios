//
//  AlertStateDelivered.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 14/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class AlertStateDelivered: AlertState {
    
    let alertDetail:AlertNotificationResponseModel?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let alertDbManager = AlertDatabaseManager()
    let sinkManager = SinkManager.sharedInstance
    
    init(alertDetail:AlertNotificationResponseModel) {
        self.alertDetail = alertDetail
        
        var doUpdate = true
        
        let alerts:[SendingAlert] = self.alertDbManager.getAlertFromSendAlertTableWithId(Id: alertDetail.alert_id!) as! [SendingAlert]
        
        if alerts.count > 0 {
            let alert:SendingAlert = alerts[0]
            if alert.alertStatus == AlertConstant.DB_STATE_CLOSED{
                doUpdate = false
            }
        }
        
        if doUpdate {
            
            print("ALERTS LOG:- ALERT STATE DELIVERED \(alertDetail.alert_id)\t \(alertDetail.alert_short_code)\t \(alertDetail.uuid)\t \(alertDetail.originator_biker_id)")
            
            var updateDb:Bool = true
            var updateUI:Bool = true
            
            let sentAlerts = self.alertDbManager.getAlertsFromSentAlertTableWithStatus(status: AlertConstant.DB_STATE_ACKNOWLEDGED)
            let arrSentAlerts = sentAlerts as! [SendingAlert]
            if arrSentAlerts.count > 0{
                
                for alert in arrSentAlerts{
                    if alert.alertId == Int32(((self.alertDetail?.alert_id)?.toInt())!){
                        updateDb = false
                        break
                    }
                }
            }
            
            if JourneyManager.sharedInstance.getAlertSendingType() == AlertSendingType.ReSent{
                updateUI = false
            }
            else if JourneyManager.sharedInstance.getAlertSendingType() == AlertSendingType.AboutToSent{
                //updating ui when alert screen is opened on phone and alert is generated through DM.
                updateUI = true
            }
            
            if updateDb {
                self.updateDatabase()
                if updateUI {
                    self.updateAppUI()
                    
                }else{
                    
                    sinkManager.notifyObservers(notificationKey: sinkManager.ALERT_NOTIFICATION_TIMER_STARTED, sender: self)
                }
                self.notifyDM()
                self.startTimer()
            }
        }
    }
    
    func updateDatabase(){
        
        alertDbManager.updateSentAlertStatusWith(status: AlertConstant.DB_STATE_DELIVERED, havingAlertid: Int32((self.alertDetail?.alert_id)!)!)
    }
    
    func updateAppUI(){
        let alertSentVC = AlertSentVC()
        alertSentVC.generatedAlertId = (alertDetail?.alert_id)?.toInt()
        alertSentVC.alertType = alertDetail?.alert_short_code
        
        weak var pvc = UIApplication.topViewController()?.presentingViewController
        
        
        if pvc != nil {
            UIApplication.topViewController()?.dismiss(animated: true, completion: {
                pvc?.present(alertSentVC, animated: true, completion: nil)
            })
        }else{
            UIApplication.topViewController()?.presentVC(alertSentVC)
        }
    }
    
    func notifyDM(){
        CPWrapper.sharedInstance().cp_Alertsent_Outgoing(Int32((alertDetail?.alert_id?.toInt())!), andStatus: CP_AlertSentTypeDef(rawValue: 1), andAlertType: CP_AlertTypeInfoTypeDef(rawValue: 1))
    }
    
    func startTimer(){
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 30.0) {
            
            if let idAlert = self.alertDetail?.alert_id{
                
                let alerts:[SendingAlert] = self.alertDbManager.getAlertFromSendAlertTableWithId(Id: idAlert) as! [SendingAlert]
                
                if alerts.count > 0 {
                    let alert:SendingAlert = alerts[0]
                    if alert.alertStatus == AlertConstant.DB_STATE_DELIVERED{
                        print("close needed")
                        
                        let alertContext = AlertContext()
                        alertContext.changeStateToAlertNoHelpReceived(alertId: ((self.alertDetail?.alert_id)?.toInt())!)
                    }
                }
            }
        }
        
    }
    
    
}
