//
//  AlertStateSend.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 28/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit


class AlertStateSend: AlertState {
    
    let alertDbManager = AlertDatabaseManager()
    let alertDetail:AlertApiResponseModel?
    
    init(alertDetail:AlertApiResponseModel) {
        self.alertDetail = alertDetail
        
        print("ALERTS LOG:- ALERT STATE SEND\t id =\(alertDetail.id)\t alert_d = \(alertDetail.alert_id)\t alertType = \(alertDetail.alert_type?.short_name)\t triggeredById = \(alertDetail.triggered_by_id)")
        self.updateDatabase()
        self.notifyDM()
        self.startTimer()
    }
    
    func updateDatabase(){
        
        let alerts:[SendingAlert] = self.alertDbManager.getAlertFromSendAlertTableWithId(Id: "\(self.alertDetail!.id!)") as! [SendingAlert]
        
        if alerts.count > 0 {
            alertDbManager.updateSentAlertStatusWith(status: AlertConstant.DB_STATE_RESEND, havingAlertid: Int32((self.alertDetail?.id)!))
        }else{
            
            alertDbManager.insertGeneratedAlertDetails(alertId: Int32((self.alertDetail?.id)!),
                                                       alertTriggeredById: Int32((self.alertDetail?.triggered_by_id)!),
                                                       alertRideId: Int32((self.alertDetail?.ride_id)!),
                                                       alertShortName: (self.alertDetail?.alert_type?.short_name)!,
                                                       alertCategory: (self.alertDetail?.alert_type?.category)!,
                                                       alertPriority: Int32((self.alertDetail?.alert_type?.priority)!),
                                                       alertStatus: AlertConstant.DB_STATE_SENT,
                                                       alertHelpReceived: "none",
                                                       alertRespondantBikerName: "none")
            
        }
    }
    
    func notifyDM(){
        let alertCategory = alertDbManager.getCategoryIdByName(category: (alertDetail?.alert_type?.category)!)
        
        let alertIdDM = alertDbManager.getAlertIdByType(alertType: (alertDetail?.alert_type?.short_name)!)
        
        if (JourneyManager.sharedInstance.getAlertGenerator() == AlertGenerator.App){
            CPWrapper.sharedInstance().cp_OutgoingAlerts(Int32(alertIdDM), andAlertType: CP_AlertTypeInfoTypeDef(rawValue: alertCategory))
        }
    }
    
    func startTimer(){
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 30.0) {
            
            let alerts:[SendingAlert] = self.alertDbManager.getAlertFromSendAlertTableWithId(Id: "\(self.alertDetail!.id!)") as! [SendingAlert]
            
            if alerts.count > 0 {
                let alert:SendingAlert = alerts[0]
                if alert.alertStatus == AlertConstant.DB_STATE_SENT || alert.alertStatus == AlertConstant.DB_STATE_RESEND{
                    print("close needed")
                    
                    let alertContext = AlertContext()
                    alertContext.changeStateToAlertAutoClose(alertId: (self.alertDetail?.id!)!)
                }
            }
        }
    }
}
