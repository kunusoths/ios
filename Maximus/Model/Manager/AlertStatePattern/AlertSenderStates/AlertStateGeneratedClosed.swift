//
//  AlertStateClosed.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 29/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class AlertStateGeneratedClosed: AlertState {
    
    let alertId:String?
    let sinkManager = SinkManager.sharedInstance
    
    init(alertId:String) {
        self.alertId = alertId
        print("ALERTS LOG:- ALERT STATE GENERATED CLOSED \(alertId)")
        
        self.updateDatabase()
        self.updateDM()
    }
    
    func updateDatabase(){
        let alertDbManager = AlertDatabaseManager()
        alertDbManager.updateSentAlertStatusWith(status: AlertConstant.DB_STATE_CLOSED, havingAlertid: Int32((alertId?.toInt())!))
        /*MyDefaults.setInt(value:AlertConstant.KEY_ALERT_STATE_CLOSE, key:AlertConstant.KEY_ALERT_STATE)*/
    }
    
    func updateDM(){
        CPWrapper.sharedInstance().cp_AlertEnd_Outgoing(CP_AlertSentTypeDef(rawValue: 1))
        JourneyManager.sharedInstance.setAlertModeState(state: AlertModeStatus.Off)
    }
    
}
