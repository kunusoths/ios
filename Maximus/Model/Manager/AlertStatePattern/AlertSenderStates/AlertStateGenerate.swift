//
//  AlertStateGenerate.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 08/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class AlertStateGenerate: AlertState,AlertApiManagerDelegate {
    
    let alertDbManager = AlertDatabaseManager()
    let alertManager:AlertApiManager = AlertApiManager()
    let sinkManager = SinkManager.sharedInstance
    var alertType:String?
    var alertDetails:AlertApiResponseModel?
    
    init(alertType:String) {
        self.alertType = alertType
        print("ALERTS LOG:- ALERT STATE GENERATE :- ALERT TYPE \(alertType)")
        if let topView = UIApplication.topViewController()?.view{
            ///-----Start Loading indicator
            LoadingHandler.shared.startloading(interactionEnable: false, view: topView)
        }
        alertManager.alertManagerDelegate = self
        alertManager.generateALert(alertType: self.alertType!)
    }
    
    //MARK:- AlertAPIManager Delegates
    func generateAlertSuccess(alertObj: [String : Any]) {
        if let alert:AlertApiResponseModel = alertObj[StringConstant.data] as? AlertApiResponseModel{
            
            print("alertID \(alert.alert_id)")
            sinkManager.notifyObservers(notificationKey: sinkManager.ALERT_NOTIFICATION_GENERATED_SUCESSFULLY, sender: self)
        }
    }
    
    
    
    func onFailure(Error: [String : Any]) {
       
        LoadingHandler.shared.stoploading()///-----Stop Loading indicator
        if let errModel = Error["data"] as? ErrorModel {
            if let view = (UIApplication.shared.delegate as? AppDelegate)?.window?.rootView() {
                view.makeToast(errModel.description ?? "")
            }
        }
        //Call Alert send failure
        sinkManager.notifyObservers(notificationKey: sinkManager.ALERT_NOTIFICATION_GENERATE_FAILURE, sender: self)
        CPWrapper.sharedInstance().cp_AlertFailed_Outgoing(CP_AlertSentTypeDef(rawValue: 0))
        /* let errModel:ErrorModel = Error["data"] as! ErrorModel
         let alertContext:AlertContext = AlertContext()
         alertContext.changeStateToAlertFailure(messageFailure: (errModel.description)!)*/
    }
}
