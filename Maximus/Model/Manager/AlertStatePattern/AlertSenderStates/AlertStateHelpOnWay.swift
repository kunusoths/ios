//
//  AlertStateHelpOnWay.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 28/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class AlertStateHelpOnWay: AlertState {
    
    let alertDetail:AlertNotificationResponseModel?
    let alertDbManager = AlertDatabaseManager()
    var bikerName = "none"
    
    init(alertDetail:AlertNotificationResponseModel) {
        self.alertDetail = alertDetail
        
        var doUpdate = true
        
        let alerts:[SendingAlert] = self.alertDbManager.getAlertFromSendAlertTableWithId(Id: alertDetail.alert_id!) as! [SendingAlert]
        
        if alerts.count > 0 {
            let alert:SendingAlert = alerts[0]
            if alert.alertStatus == AlertConstant.DB_STATE_CLOSED{
                doUpdate = false
            }
        }
        
        if doUpdate {
            
            print("ALERTS LOG:- ALERT STATE HELP ON WAY \(alertDetail.alert_id)\t \(alertDetail.alert_short_code)\t \(alertDetail.uuid)\t \(alertDetail.originator_biker_id)")
            
            if let bikerName = self.alertDetail?.respondent_biker_name{
                self.bikerName = bikerName
            }
            
            self.updateDatabase()
            self.updateAppUI()
            self.updateDM()
        }
    }
    
    func updateDatabase(){
        
        
        alertDbManager.updateSentAlertStatusWith(status: AlertConstant.DB_STATE_ACKNOWLEDGED, havingAlertid: Int32((self.alertDetail?.alert_id)!)!)
        
        
        let alerts:[SendingAlert] = self.alertDbManager.getAlertFromSendAlertTableWithId(Id: self.alertDetail!.alert_id!) as! [SendingAlert]
        
        if alerts.count > 0 {
            let alert = alerts[0]
            
            if alert.alertRespondantBikerName != "none"{
                bikerName = "\(bikerName),\(alert.alertRespondantBikerName)"
            }
        }
        
        alertDbManager.updateSentAlertHelperBikerWith(name: bikerName, havingAlertid: Int32((self.alertDetail?.alert_id)!)!)
        
        
    }
    
    func updateAppUI(){
        let resumeRideVc = ResumeRideVC()
        resumeRideVc.generatedAlertId = self.alertDetail?.alert_id
        resumeRideVc.helperBikerName = self.bikerName//self.alertDetail?.respondent_biker_name
        
        
        let navigationController = UINavigationController(rootViewController:resumeRideVc)
        
        weak var pvc = UIApplication.topViewController()?.presentingViewController
        if pvc != nil {
            UIApplication.topViewController()?.dismiss(animated: true, completion: {
                
                pvc?.present(navigationController, animated: true, completion: nil)
            })
        }else{
            UIApplication.topViewController()?.presentVC(navigationController)
        }
    }
    
    func updateDM(){
        CPWrapper.sharedInstance().cp_AlertSentResponse_Outgoing(Int32((alertDetail?.alert_id)!)!, andUUID:  Int32((alertDetail?.alert_id)!)!, andStatus: CP_ResponseTypeDef(rawValue: 1))
    }
    
    
}
