//
//  FCMFileUploadManager.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 18/10/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

class FCMFileUploadManager: NSObject {
    static let fcmManager = FCMFileUploadManager()
    let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    let navigationLogFileName = "navengine.log"
    let bleLogFileName = "ble.log"
    let backgroundQueue = DispatchQueue.global(qos: .background)

    private override init() {
        super.init()
    }
    
    func uploadFileToFCMStorage(rideId:Int, riderName:String) {
        
        // Get a reference to the storage service using the default Firebase App
       // let storage = Storage.storage()

        // Create a root reference
        let storageRef = Storage.storage().reference(forURL: "gs://maximuspro-179909.appspot.com")
        
        let naigationFilePath = "\(documentsPath)/\(rideId)_\(navigationLogFileName)"
        let localFile:URL = URL.init(fileURLWithPath: naigationFilePath)
        
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: localFile.path) {
            
            let currentDate = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy"
            formatter.locale = Locale(identifier: "en_US_POSIX")
            let formattedDate = formatter.string(from: currentDate)
            
            let folderFormatter = DateFormatter()
            
            folderFormatter.timeZone = TimeZone(abbreviation: "UTC")
            folderFormatter.dateFormat = "yyyyMMddHHmmss"
            folderFormatter.locale = Locale(identifier: "en_US_POSIX")
            let folderDate = folderFormatter.string(from: currentDate)
            
            let bikerId:Int = MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID)
            
            let fileName = "\(rideId)-NE-\(folderDate).log"
            
            
            // Create a reference to the file you want to upload
            let riversRef = storageRef.child("\(formattedDate)/\(bikerId)/\(fileName)")
            
                do {
                    let dataToUpload = try Data(contentsOf: localFile)
                    riversRef.putData(dataToUpload, metadata: nil, completion: { (metadata, error) in
                        if let error = error {
                            print(error.localizedDescription)
                        } else {
                            // file uploaded successfully
                            print(metadata.debugDescription)
                            self.deleteFileAfterSuccessfulUpload(filePath: localFile.path)
                        }
                    })
                } catch {
                    print("Log File Not Available")
                }
        }
    }
    
    func deleteFileAfterSuccessfulUpload(filePath: String) {
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: filePath) {
            do {
                try fileManager.removeItem(atPath: filePath)
            }
            catch {
                print("Ooops! Something went wrong while deleting file")
            }
        }
    }
    
    
    func renameNavEngineFile(rideID: Int) {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let naigationFilePath = "\(documentsPath)/\(navigationLogFileName)"
        let localFile:URL = URL.init(fileURLWithPath: naigationFilePath)
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: localFile.path) {
            let originPath = documentsPath.appending("/\(navigationLogFileName)")
            let destinationPath = documentsPath.appending("/\(rideID)_\(navigationLogFileName)")
            do {
                try FileManager.default.moveItem(atPath: originPath, toPath: destinationPath)
                self.uploadFileToFCMStorage(rideId: rideID, riderName: "")
            } catch {
                
            }
        }
    }
    
    func createBluetoothLogsFile(riderID: Int){
        // Get a reference to the storage service using the default Firebase App
        //let storage = Storage.storage()
        
        // Create a root reference
       // let storageRef = storage.reference()
        
        let bleFilePath = "\(documentsPath)/\(bleLogFileName)"
        let localFile:URL = URL.init(fileURLWithPath: bleFilePath)
        var logStr = ""
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: localFile.path) {
                MyDefaults.setBoolData(value: true, key: "LogFileCreated")
                print(localFile.path)
                logStr.append("Start Writing..")
                do {
                    try
                    logStr.write(toFile: localFile.path, atomically: false, encoding: String.Encoding.utf8)
                } catch {
                    logStr.append("Failed to write..")
                    do {
                        try
                            logStr.write(toFile: localFile.path, atomically: false, encoding: String.Encoding.utf8)
                    } catch {
                        
                    }
                }
            
        } else {
            //create
            fileManager.createFile(atPath: localFile.path, contents: nil, attributes: nil)
            MyDefaults.setBoolData(value: true, key: "LogFileCreated")
            print(localFile.path)
            do {
                try
                    logStr.write(toFile: localFile.path, atomically: false, encoding: String.Encoding.utf8)
            } catch {
                logStr.append("Failed to write..")
                do {
                    try
                        logStr.write(toFile: localFile.path, atomically: false, encoding: String.Encoding.utf8)
                } catch {
                    
                }
            }
        }
    }
    
    func uploadBleLogs() {
        
        // Get a reference to the storage service using the default Firebase App
        let storage = Storage.storage()
        
        // Create a root reference
        let storageRef = storage.reference()
        
        let bleFilePath = "\(documentsPath)/\(bleLogFileName)"
        let localFile:URL = URL.init(fileURLWithPath: bleFilePath)
        let currentDate = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let formattedDate = formatter.string(from: currentDate)
        let fileManager = FileManager.default
            if fileManager.fileExists(atPath: localFile.path) {
    //            let data = fileManager.contents(atPath: localFile.path)
                let  fileName = "\(MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID))_ble.log"
                
                let riversRef = storageRef.child("iosLogs/\(formattedDate)/BLE/\(MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID))/\(fileName)")
                do {
                    let dataToUpload = try Data(contentsOf: localFile)
                    riversRef.putData(dataToUpload, metadata: nil, completion: { (metadata, error) in
                        if let _ = error {
                            
                        } else {
                        }
                    })
                } catch {
                    
                }
        }
    }
    
    func writeToLogFile(logToWrite: String) {
        backgroundQueue.async {
            let currentDate = Date()
            let formatter = DateFormatter()
            var logStr = ""
            formatter.dateFormat = "dd-MM-yyyy hh:mm:ss"
            formatter.timeZone = TimeZone(abbreviation: "UTC")
            formatter.locale = Locale(identifier: "en_US_POSIX")
            let formattedDate = formatter.string(from: currentDate)
            logStr.append(formattedDate)
            logStr.append(" \(logToWrite)\n")
            
            let bleFilePath = "\(self.documentsPath)/\(self.bleLogFileName)"
            let localFile:URL = URL.init(fileURLWithPath: bleFilePath)
            
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: localFile.path) {
                let file: FileHandle? = FileHandle(forUpdatingAtPath: localFile.path)
                
                if file == nil {
                    print("File open failed")
                } else {
                    if let data = logStr.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) {
                        file?.seekToEndOfFile()
                        do {
                            try file?.write(data)
                        } catch {
                            
                        }
                        file?.closeFile()
                        }
                }
            }
        }
    }
}
