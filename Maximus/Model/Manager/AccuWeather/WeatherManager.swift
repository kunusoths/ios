//
//  
//WeatherManager.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 24/04/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

@objc protocol WeatherManagerDelegate {
    
    @objc optional func getLocationKeySuccess(locationKey:String)
    @objc optional func getTemperatureSuccess(currentTemperature:String)
    @objc optional func onFailure(Error:[String:Any])
}


class WeatherManager: NSObject {
    var weatheranagerDelegate:WeatherManagerDelegate?
    var cloudHandlerObj = CloudHandler()
    var accuWeatherApiKey = "cqvBtrQRJelQF6TeSZBUdY8qu0Qlqtwu"
    
    func getLocationKey(latitude:Double, longitude:Double ,completionHandler: @escaping (String) -> Void){

        let suburl = "http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?q=\(latitude),\(longitude)&apikey=\(accuWeatherApiKey)"
     
        self.cloudHandlerObj.makeAccuWeatherCloudRequest(subUrl: suburl, parameter:["":""], isheaderAUTH: false, requestType: .get){ response in
            
            if let json = response?.result.value {
                print(json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let user = Mapper<AccuWeatherModel>().map(JSON:json as! [String : Any])!
                    completionHandler(user.Key!)
                    //self.accuManagerDelegate?.getLocationKeySuccess!(locationKey: user.Key!)

                }
                else {
                    completionHandler("")
                    print(json)
                    //self.errorCall(value: json as! [String : Any])
                }
            }else{
                completionHandler("")
                print(RequestTimeOut().json)
                //self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
//    func getCurrentTemperature(locationKey:String ,completionHandler: @escaping (String) -> Void)
//    {
//        let suburl = "http://dataservice.accuweather.com/currentconditions/v1/\(locationKey)?apikey=\(accuWeatherApiKey)"
//        
//        self.cloudHandlerObj.makeAccuWeatherCloudRequest(subUrl: suburl, parameter:["":""], isheaderAUTH: false, requestType: .get){ response in
//            
//            if let json = response?.result.value {
//                print(json)
//                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
//                {
//                    let user = Mapper<AccuWeatherModel>().mapArray(JSONArray: json as! [[String : Any]])
//                    
//                    let value = user![0].Temperature!.Metric!.Value.rounded()
//                    completionHandler("\(value)")
//                  //  self.accuManagerDelegate?.getTemperatureSuccess!(currentTemperature: "\(value)")
//                }
//                else {
//                    completionHandler("")
//                    print(json)
//                   //self.errorCall(value: json as! [String : Any])
//                }
//            }else{
//                completionHandler("")
//                print(RequestTimeOut().json)
//                //self.errorCall(value: RequestTimeOut().json)
//            }
//        }
//    }
//    
    func getCurrentTemperatureFromOW(lat: Double, lng: Double,completionHandler: @escaping (String) -> Void){
        
        let weatherUrl = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lng)&units=metric&type=accurate&APPID=\(Constants.openWheatherApiKey)"
        print("open wheather url ", weatherUrl)
        
        self.cloudHandlerObj.makeAccuWeatherCloudRequest(subUrl: weatherUrl, parameter: ["":""], isheaderAUTH: false, requestType: .get){ response in
            
            if let json = response?.result.value {
                print(json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300{
                    let weather = Mapper<OpenWheather>().map(JSON:json as! [String : Any])!
                    let strTemperature = String(format:"%.2f", (weather.main?.temperature)!)
                   // self.weatheranagerDelegate?.getTemperatureSuccess!(currentTemperature: strTemperature)
                    completionHandler(strTemperature)
                }else {
                    //self.errorCall(value: json as! [String : Any])
                    completionHandler("")
                }
                
            }else{
                completionHandler("")
                //self.errorCall(value: RequestTimeOut().json)
            }
            
        }
    }

    func errorCall(value:[String : Any]!){
        let ErrorModelObj = Mapper<ErrorModel>().map(JSON:value)
        let errorData:[String:Any] = ["data":ErrorModelObj ?? " "]
        //self.accuManagerDelegate?.onFailure!(Error: errorData)
    }
}
