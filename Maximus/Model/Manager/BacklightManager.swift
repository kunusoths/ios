//
//  BacklightManager.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 14/11/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//

import UIKit

class BacklightManager: NSObject {
    
    fileprivate let MODE_DAY:UInt32 = 0
    fileprivate let MODE_TWILIGHT:UInt32 = 1
    fileprivate let MODE_NIGHT:UInt32 = 2
    fileprivate let MODE_EVENING_TWILIGHT: UInt32 = 4
    
    fileprivate let BACKLIGHT_MODE_AUTO:UInt32 = 0
    fileprivate let BACKLIGHT_MODE_MANUAL:UInt32 = 1
    
    
    func getBackLightMode() -> UInt32{
        
        let backlightMode:UInt32 = identifyBacklightMode()
        
        switch backlightMode {
        case MODE_DAY:
            return MODE_DAY
        case MODE_TWILIGHT:
            return MODE_TWILIGHT
        case MODE_NIGHT:
            return MODE_NIGHT
        case MODE_EVENING_TWILIGHT:
            return MODE_TWILIGHT
        default:
            break
        }
        return MODE_DAY
    }
    
    func getBackLightAutoMode() -> UInt32{
        return BACKLIGHT_MODE_AUTO
    }
    
    func getBackLightAutoManual() -> UInt32{
        return BACKLIGHT_MODE_MANUAL
    }
    
    func identifyBacklightMode() -> UInt32{
        
        var backlightMode:UInt32 = 0
        let now = Date()
        
        let five_thirty_am = dateAt(hours: 05, minutes: 30)
        let seven_thirty_am = dateAt(hours: 07, minutes: 30)
        let five_thirty_pm = dateAt(hours: 17, minutes: 30)
        let seven_thirty_pm = dateAt(hours: 19, minutes: 30)
        
        if now >= five_thirty_am && now <= seven_thirty_am{
            backlightMode = MODE_TWILIGHT
            
        }else if now >= seven_thirty_am && now <= five_thirty_pm{
            backlightMode = MODE_DAY
            
        }else if now >= five_thirty_pm && now <= seven_thirty_pm{
            backlightMode = MODE_EVENING_TWILIGHT
            
        }else if now >= seven_thirty_pm {
            backlightMode = MODE_NIGHT
            
        }else if now <= five_thirty_am {
            backlightMode = MODE_NIGHT
        }
        
        return backlightMode
    }
    
    func dateAt(hours: Int, minutes: Int) -> Date
    {
        let calendar = Calendar.current
        let date = Date()
       
        var components = calendar.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
        
        components.hour = hours
        components.minute = minutes
        components.second = 0
        
        let newDate = calendar.date(from: components)!
        return newDate
    }
}


