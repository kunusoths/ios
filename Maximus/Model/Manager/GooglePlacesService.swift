//
//  GooglePlacesService.swift
//  Maximus
//
//  Created by Sriram G on 27/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import Alamofire
import CoreLocation
import ObjectMapper

let kPlaceURL        = "https://maps.googleapis.com/maps/api/place/autocomplete/json?"
let kPlaceDetailsURL = "https://maps.googleapis.com/maps/api/place/details/json?"

let geocodeBaseUrl = "https://maps.googleapis.com/maps/api/geocode/json?"

let baseURLDirections = "https://maps.googleapis.com/maps/api/directions/json?"


class GooglePlacesService: NSObject
{
    
    let APIKey = Constants.APIKEY_MAPS!
    var selectedRoute: NSDictionary!
    var overviewPolyline: NSDictionary!
    var originCoordinate: CLLocationCoordinate2D!
    var destinationCoordinate: CLLocationCoordinate2D!
    var originAddress: String!
    var destinationAddress: String!
    
    var totalDistanceInMeters: UInt = 0
    var totalDistance: String!
    var totalDurationInSeconds: UInt = 0
    var totalDuration: String!
    
    func findPlaceName (placeName:String,completionHandler: @escaping (DataResponse<Any>) -> Void)
    {
        //Allow for selected countries
        let regionCode = Locale.current.regionCode ?? "IN"
        let urlString = "\(kPlaceURL)input=\(placeName)&components=country:\(regionCode)&key=\(APIKey)"
        // Allow all countries
        //let urlString = "\(kPlaceURL)input=\(placeName)&key=\(APIKey)"

        print(urlString)
        if (NetworkReachabilityManager()?.isReachable)!
        {
            let url = URL(string:urlString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)!
            var request = URLRequest(url: url as URL)
            request.timeoutInterval = 60
            Alamofire.request(request)
                .responseJSON { response in
                    completionHandler(response)
            }
        }else{
            if let view = Constants.appDelegate?.window?.rootView(){
                ToastPopUp.showNetworkPOPup(message:ToastMsgConstant.network ,view: view)
            }
        }
    }
    
    func locationDetails(placeId:String, locationType:String,completionHandler: @escaping (DataResponse<Any>) -> Void)
    {
        let urlString = "\(kPlaceDetailsURL)placeid=\(placeId)&key=\(APIKey)"
        print(urlString)
        if (NetworkReachabilityManager()?.isReachable)!
        {
            let url =  NSURL(string:urlString)
            var request = URLRequest(url: url! as URL)
            request.timeoutInterval = 60
            Alamofire.request(request)
                .responseJSON { response in
                    completionHandler(response)
            }
        }else{
            if let view = Constants.appDelegate?.window?.rootView(){
                ToastPopUp.showNetworkPOPup(message:ToastMsgConstant.network ,view: view)
            }
        }
    }
    
    
    func getAddressForLatLng(latitude: String, longitude: String, completionHandler: @escaping (NSDictionary) -> Void) {
        let url = NSURL(string: "\(geocodeBaseUrl)latlng=\(latitude),\(longitude)&key=\(APIKey)")
        if let finalUrl = url {
            do {
                let data = try Data(contentsOf: (finalUrl as URL), options: Data.ReadingOptions.dataReadingMapped)//NSData(contentsOf: url! as URL)
                let json = try! JSONSerialization.jsonObject(with: data , options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                
                completionHandler(json)
            } catch {
                
            }
        }
    }
    
    
    func getDirections(_ origin: String!, destination: String!, waypoints: Array<String>!, travelMode: AnyObject!, completionHandler: @escaping ((_ status: String, _ success: Bool) -> Void)) {
        if let originLocation = origin {
            if let destinationLocation = destination {
                var directionsURLString = baseURLDirections + "origin=" + originLocation + "&destination=" + destinationLocation
                
                
                if let routeWaypoints = waypoints {
                    if(waypoints.count > 0) {
                        directionsURLString += "&waypoints=optimize:true"
                        
                        for waypoint in routeWaypoints {
                            directionsURLString += "|" + waypoint
                        }
                    }
                }
                directionsURLString += "&mode=" + "driving" + "&key=" + Constants.APIKEY_MAPS!
                
                directionsURLString = directionsURLString.addingPercentEscapes(using: String.Encoding.utf8)!
                
                let directionsURL = URL(string: directionsURLString)
                DispatchQueue.main.async(execute: { () -> Void in
                    let directionsData = try? Data(contentsOf: directionsURL!)
                    
                    
                    do {
                        let dictionary:NSDictionary?
                        let dictionary1 = try JSONSerialization.jsonObject(with: directionsData!, options:JSONSerialization.ReadingOptions.mutableContainers)
                        guard let JSONDictionary: NSDictionary = dictionary1 as? NSDictionary /*as Dictionary<NSObject, AnyObject>*/else {
                            print("Not a Dictionary")
                            // put in function
                            return
                        }
                        
                        dictionary = JSONDictionary
                        print("JSONDictionary! \(JSONDictionary)")
                        let status = dictionary?["status"] as! String
                        
                        if status == "OK" {
                            self.selectedRoute = (dictionary?["routes"] as! Array<NSDictionary>)[0] as NSDictionary
                            self.overviewPolyline = self.selectedRoute["overview_polyline"] as? NSDictionary
                            
                            let legs = self.selectedRoute["legs"] as! Array<NSDictionary>
                            
                            let startLocationDictionary = legs[0]["start_location"] as! NSDictionary
                            self.originCoordinate = CLLocationCoordinate2DMake(startLocationDictionary["lat"] as! Double, startLocationDictionary["lng"] as! Double)
                            
                            let endLocationDictionary = legs[legs.count - 1]["end_location"] as! NSDictionary
                            self.destinationCoordinate = CLLocationCoordinate2DMake(endLocationDictionary["lat"] as! Double, endLocationDictionary["lng"] as! Double)
                            
                            self.originAddress = (legs[0]["start_address"] as! String)
                            self.destinationAddress = (legs[legs.count - 1]["end_address"] as! String)
                            
                            self.calculateTotalDistanceAndDuration()
                            
                            completionHandler(status, true)
                        }
                        else {
                            completionHandler(status, false)
                        }
                        
                    }
                    catch let JSONError as NSError {
                        print("\(JSONError)")
                        completionHandler("", false)
                    }
                    
                })
            }
            else {
                completionHandler("Destination is nil.", false)
            }
        }
        else {
            completionHandler("Origin is nil", false)
        }
    }
    
    func calculateTotalDistanceAndDuration() {
        
        print(self.selectedRoute)
        let legs = self.selectedRoute["legs"] as! Array<NSDictionary>
        totalDistanceInMeters = 0
        totalDurationInSeconds = 0
        for leg in legs {
            totalDistanceInMeters += (leg["distance"] as! NSDictionary)["value"] as! UInt
            totalDurationInSeconds += (leg["duration"] as! NSDictionary)["value"] as! UInt
        }
        
        let doubleMeter:Double = Double(totalDistanceInMeters)
        let distanceInKilometers: Double = Double(doubleMeter / 1000)
        print(distanceInKilometers)
        
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 2
        formatter.roundingMode = .down
        formatter.numberStyle = .decimal
        formatter.alwaysShowsDecimalSeparator = true
        formatter.minimumFractionDigits = 2
        
        totalDistance = formatter.string(from: NSNumber(value: distanceInKilometers))
    }
    
    
    func formDirectionUrl(jsonObj: [String: Any]) -> URL {
        let sourceDict = jsonObj[StringConstant.SOURCE_DETAILS] as? [String: Any]
        let destinationDict = jsonObj[StringConstant.DESTINATION_DETAILS] as? [String: Any]
        
        let srcLatLng:[String:Any] = (sourceDict!["latlong"] as? [String:Any])!
        let lat = srcLatLng["latitude"] as? Double
        let lng = srcLatLng["longitude"] as? Double
        
        let srcLoc:String = "\(lat!),\(lng!)"
        
        
        let destLatLng:[String:Any] = (destinationDict!["latlong"] as? [String:Any])!
        let dlat = destLatLng["latitude"] as? Double
        let dlng = destLatLng["longitude"] as? Double
        
        let destLoc:String = "\(dlat!),\(dlng!)"
        
        var directionsURLString = baseURLDirections + "origin=" + srcLoc + "&destination=" + destLoc
        //https://maps.googleapis.com/maps/api/directions/json?origin=sydney,au&destination=perth,au&waypoints=via:-37.81223%2C144.96254%7Cvia:-34.92788%2C138.60008&key=AIzaSyAZvTGADdN-3scRvz0f4jeBqjGprZkMwAo
        if let viaPointArray:[[String: Any]] = jsonObj[StringConstant.VIA_POINTS_DETAILS] as? [[String: Any]] {
            if viaPointArray.count > 0 {
                directionsURLString += "&waypoints="
                for viapoint in viaPointArray {
                    let location:[String:Double] = viapoint["location"] as! [String : Double]
                    let lat = location["latitude"]
                    let lng = location["longitude"]
                    directionsURLString += "\(lat!)" + String("%2C") + "\(lng!)" + String("%7C")
                }
                
                directionsURLString = directionsURLString.replacingOccurrences(of: "%7C", with: "", options: String.CompareOptions.literal, range: Range(NSRange(location: (directionsURLString.length - 3), length: 3), in: directionsURLString))
            }
        }
        
        directionsURLString += "&alternatives=true" + "&key=" + Constants.APIKEY_MAPS! //"&mode=" + "driving"
        
        directionsURLString = directionsURLString.replacingOccurrences(of: " ", with: "%20")//addingPercentEscapes(using: String.Encoding.utf8)!
        let directionsURL = URL(string: directionsURLString)
        return directionsURL!
    }
    
    func getRouteFromSourceDestinationWaypoints(_ jsonObj: [String: Any], completionHandler: @escaping ((_ status: String, _ success: Bool, _ directionResult: [String: Any]) -> Void)) {
        let directionUrl = self.formDirectionUrl(jsonObj: jsonObj)
        DispatchQueue.main.async(execute: { () -> Void in
            guard let directionsData = try? Data(contentsOf: directionUrl) else {
                completionHandler("", false, [:])
                return
            }
            
            
            do {
                var dictionary:NSDictionary?
                dictionary = try JSONSerialization.jsonObject(with: directionsData, options:JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                guard let JSONDictionary: NSDictionary = dictionary /*as Dictionary<NSObject, AnyObject>*/else {
                    print("Not a Dictionary")
                    return
                }
                
                dictionary = JSONDictionary
                print("JSONDictionary! \(JSONDictionary)")
                let status = dictionary?["status"] as! String
                
                if status == "OK" {
                    self.selectedRoute = (dictionary?["routes"] as! Array<NSDictionary>)[0] as NSDictionary
                    let directionDetails = Mapper<RouteDetails>().map(JSON: JSONDictionary as! [String : Any])!
                    var directionDetailsData:[String:Any] = [:]
                    directionDetailsData[StringConstant.data] = directionDetails
                    completionHandler(status, true, directionDetailsData)
                } else
                    if status == "NOT_FOUND" || status == "ZERO_RESULTS" {
                        let srcDict = jsonObj[StringConstant.SOURCE_DETAILS] as? [String:Any]
                        let destDict = jsonObj[StringConstant.DESTINATION_DETAILS] as? [String:Any]
                        if let result = self.getSnappedRoute(src: srcDict!, dest: destDict!, routeJson: jsonObj) {
                            completionHandler("OK", true, result)
                        }else {
                            completionHandler(status, false, [:])
                        }
                    }
                    else {
                        completionHandler(status, false, [:])
                }
                
            }
            catch let JSONError as NSError {
                print("\(JSONError)")
                completionHandler("", false, [:])
            }
            
        })
    }
    
    func getSnappedRoute(src:[String:Any] , dest:[String:Any], routeJson:[String:Any]) -> [String:Any]? {
        var routeData:[String:Any]?
        
        guard let srcData = self.snapRoads(locDict: src) else {
            return nil
        }
        guard let destData = self.snapRoads(locDict: dest) else {
            return nil
        }
        
        var newRoute = routeJson
        newRoute[StringConstant.SOURCE_DETAILS] = srcData
        newRoute[StringConstant.DESTINATION_DETAILS] = destData
        let directionUrl = self.formDirectionUrl(jsonObj: newRoute)
        
       // DispatchQueue.main.async(execute: { () -> Void in
            let directionsData = try? Data(contentsOf: directionUrl)
            
            
            do {
                var dictionary:NSDictionary?
                dictionary = try JSONSerialization.jsonObject(with: directionsData!, options:JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                guard let JSONDictionary: NSDictionary = dictionary /*as Dictionary<NSObject, AnyObject>*/else {
                    print("Not a Dictionary")
                    return nil
                }
                
                dictionary = JSONDictionary
                print("JSONDictionary! \(JSONDictionary)")
                let status = dictionary?["status"] as! String
                
                if status == "OK" {
                    self.selectedRoute = (dictionary?["routes"] as! Array<NSDictionary>)[0] as NSDictionary
                    let directionDetails = Mapper<RouteDetails>().map(JSON: JSONDictionary as! [String : Any])!
                    var directionDetailsData:[String:Any] = [:]
                    directionDetailsData[StringConstant.data] = directionDetails
                 //   completionHandler(status, true, directionDetailsData)
                    routeData = directionDetailsData
                   // return directionDetailsData
                }else {
                      //  completionHandler(status, false, [:])
                    //return nil
                }
                
            }
                
            catch let JSONError as NSError {
                print("\(JSONError)")
               // return nil
                //completionHandler("", false, [:])
            }
            
    //    })
        
        return routeData
      //  self.getRouteFromSourceDestinationWaypoints(<#T##jsonObj: [String : Any]##[String : Any]#>, completionHandler: <#T##((String, Bool, [String : Any]) -> Void)##((String, Bool, [String : Any]) -> Void)##(String, Bool, [String : Any]) -> Void#>)
        
    }
    
    
    func snapRoads(locDict:[String:Any]) -> [String:Any]? {
        var data:[String:Any]?
        
        let url = "https://roads.googleapis.com/v1/snapToRoads?"
        
        var srcLatLng:[String:Any] = (locDict["latlong"] as? [String:Any])!
        let lat = srcLatLng["latitude"] as? Double
        let lng = srcLatLng["longitude"] as? Double
        
        //path = "\(lat),\(lng)" + "&interpolate=true" + "&key=" + Constants.APIKEY_MAPS!
        
        let snapstr = url + "path=" + "\(lat!),\(lng!)" + "&interpolate=true" + "&key=" + Constants.APIKEY_MAPS!
        
        guard let snapURL = URL(string: snapstr) else {
            return nil
        }
        let snapData = try? Data(contentsOf: snapURL)
        
        do {
            var dictionary:NSDictionary?
            dictionary = try JSONSerialization.jsonObject(with: snapData!, options:JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
            guard let JSONDictionary: NSDictionary = dictionary /*as Dictionary<NSObject, AnyObject>*/else {
                print("Not a Dictionary")
                return nil
            }
            
            dictionary = JSONDictionary
            print("JSONDictionary! \(JSONDictionary)")
            //Mapper<RouteDetails>().map(JSON: JSONDictionary as! [String : Any])!
             let snappedPoints = Mapper<SnappedPoints>().map(JSON: JSONDictionary as! [String : Any])!
             print("Hello snap")
            if snappedPoints.points?.count ?? 0 > 0 {
                let lat:Double = (snappedPoints.points?.first?.location?.latitude!)!
                let lng:Double = (snappedPoints.points?.first?.location?.longitude!)!
                srcLatLng = ["latitude": lat,
                             "longitude": lng]
                data = locDict
                data!["latlong"] = srcLatLng
            }else {
                
            }

            
        } catch {
            
        }
        return data
    }

}

class SnappedPoints:Mappable{
    
    var points:[Point]?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        points        <- map["snappedPoints"]
    }
    
}

class Point:Mappable {
    
    var location:Location?
    var placeId:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        location        <- map["location"]
        placeId        <- map["placeId"]
    }
    
}
