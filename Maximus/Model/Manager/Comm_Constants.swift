//
//  Comm_Constants.swift
//  Maximus
//
//  Created by Admin on 26/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation

struct SCALE_TYPE{
    static let ZERO:Int32     = 0
    static let TEN:Int32      = 10
    static let HUNDRED:Int32  = 100
    static let THOUSAND:Int32 = 1000
}
/* Note:
 0 - default
 1 - Not Live Information
 2 - Its Live Information coming from Navigation
 */

struct LIVE_FLAG {
    static let ZERO:Int32     = 0 //--Ignore
    static let ONE:Int32     = 1 //--Not Live
    static let TWO:Int32    = 2 //--Live Info
}

struct DISTANCE_UNIT {
    static let YARDS:Int32     = 0
    static let METER:Int32     = 1
    static let KILOMETER:Int32 = 2
    static let MILES:Int32     = 3
}

struct TIME_FORMAT {
    static let ZERO:Int32     = 0 //12 Hrs Format
    static let ONE:Int32      = 1  //24 Hrs Format
}

struct TIME_AM_PM {
    static let ZERO:Int32    = 0 //AM
    static let ONE:Int32     = 1 //PM
}

enum IconType : Int {
    case SMALL = 0
    case LARGE
}

enum ExitNumbers : Int {
    case ZERO = 0
    case ONE
    case TWO
    case THREE
    case FOUR
    case FIVE
    case SIX
    case SEVEN
}

