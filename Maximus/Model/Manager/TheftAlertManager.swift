//
//  TheftAlertManager.swift
//  Maximus
//
//  Created by kpit on 10/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

@objc protocol TheftAlertDelegate
{
    @objc optional func onGetArmStatusSuccess(data:[String:Any])
    @objc optional func onPostArmStatusSuccess(data:[String:Any])
    @objc optional func onFailure(Error:[String:Any])

}

class TheftAlertManager: NSObject
{
    var theftAlertDelegate:TheftAlertDelegate?
    var cloudHandler = CloudHandler()
    
    func getArmStatus(subUrl:String){
        
        self.cloudHandler.makeCloudRequest(subUrl: subUrl, parameter: ["":""], isheaderAUTH: true, requestType: .get)
        { response in
            if let json = response?.result.value{
                print("Json Response \(json)")
                
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300{
                    let armStatus = Mapper<ArmDisArmModel>().map(JSON:json as! [String:Any])!
                    let data:[String:Any] = [StringConstant.armDisarmStatus:armStatus]
                    self.theftAlertDelegate?.onGetArmStatusSuccess!(data: data)
                }
            }
        }
    }
    
    func postArmStatus(subUrl:String){
        
        self.cloudHandler.makeCloudRequest(subUrl: subUrl, parameter: ["":""], isheaderAUTH: true, requestType: .post)
        { response in
            if let json = response?.result.value,let statusCode = response?.response?.statusCode{
                print("Json Response \(json)")
                
                if (statusCode) >= 200 && (statusCode) < 300{
                    let armStatus = Mapper<ArmDisArmModel>().map(JSON:json as! [String:Any])!
                    let data:[String:Any] = [StringConstant.armDisarmStatus:armStatus]
                    self.theftAlertDelegate?.onPostArmStatusSuccess!(data: data)
                }
                else{
                    self.errorCall(value: json as! [String : Any])
                }
            } else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    func errorCall(value:[String : Any]!){
        let ErrorModelObj = Mapper<ErrorModel>().map(JSON:value)
        let errorData:[String:Any] = ["data":ErrorModelObj ?? " "]
        self.theftAlertDelegate?.onFailure!(Error:errorData)
    }
    
}
