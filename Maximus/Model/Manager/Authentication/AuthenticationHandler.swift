    //
    //  AuthenticationHandler.swift
    //  Maximus
    //
    //  Created by Admin on 17/02/17.
    //  Copyright © 2017 com.kpit.maximus. All rights reserved.
    //
    
    import Foundation
    import Alamofire
    import ObjectMapper
    
    //Enum TypeOfAuthURL is for to check that which Authentication URL is Called
    public enum TypeOfAuthURL: String {
        case Registration       = "Registration"
        case Authentication     = "Authentication"
    }
    
    // need to add network manger if not network fetch data from local DB
    @objc protocol UserManagerDelegate {
        
        @objc optional func onSocialLoginAuthenticationSuccess(data:[String:Any])
        @objc optional func onGetBikerSuccess(data:[String:Any])
        @objc optional func updateAllBikesList(data:[String:Any])
        @objc optional func onFailure(Error:[String:Any])
        @objc optional func onVehiclesUpdate(data:[String:Any])
        @objc optional func activeDeviceIsMapped(data:[String:Any])
        @objc optional func noActiveDeviceIsMapped(Error:[String:Any])
    }
    
    class UserManagerHandler {
        
        var userManagerDelegate:UserManagerDelegate?
        var cloudHandlerObj = CloudHandler()
        
        
        func aunthenticateWithCloud(loginMethod:String , objModel:[String: Any]) {
            self.callAuthenticateAndRegistaration(loginMethod:loginMethod , objModel:objModel , authType:.Authentication)
        }
        
        
        func callAuthenticateAndRegistaration(loginMethod:String , objModel:[String: Any] , authType:TypeOfAuthURL )  {
            
            var urlAuthentication = String()
            //let json:NSString = objModel.jsonEncode()
            var parameter:[String:Any] = [StringConstant.auth_method:loginMethod,
                                          StringConstant.auth_metadata: objModel]
            if loginMethod != LoginMethod.FACEBOOK.rawValue && (authType == TypeOfAuthURL.Authentication){
                urlAuthentication =  RequestURL.URL_AUTHENTICATION.rawValue
                parameter = [StringConstant.auth_method: loginMethod,
                             StringConstant.grantType:StringConstant.password,
                             StringConstant.clientId:StringConstant.oauth_client_id,
                             StringConstant.clientSecret:KeychainService.shared[KEY_OAUTH.KEY_NAME_OAUTH_CLIENT_SECRET]!,
                             StringConstant.username:objModel[StringConstant.username] as! String,
                             StringConstant.password:objModel[StringConstant.password] as! String,
                            StringConstant.scope:StringConstant.offline_access]
            }else{
                urlAuthentication =  RequestURL.URL_AUTHENTICATION.rawValue
                parameter = [StringConstant.subject_token_type: StringConstant.tokentype_accesstoken,
                             StringConstant.grantType:StringConstant.granttype_tokenexchange,
                             StringConstant.clientId:StringConstant.oauth_client_id,
                             StringConstant.clientSecret:KeychainService.shared[KEY_OAUTH.KEY_NAME_OAUTH_CLIENT_SECRET]!,
                             StringConstant.subject_token:objModel[StringConstant.accessToken] as! String,
                             StringConstant.subject_issuer:loginMethod,
                             StringConstant.scope:StringConstant.offline_access]
            }
           
            
            
            
            self.cloudHandlerObj.makeCloudRequest(subUrl: urlAuthentication, parameter: parameter, isheaderAUTH: false, requestType: .post){ response in
                
                if let json = response?.result.value,let statusCode = response?.response?.statusCode {
                    
                    if (statusCode) >= 200 && (statusCode) < 300
                    {
                        
                        if(authType == TypeOfAuthURL.Authentication )
                        {
                            var json1 = json as! [String : Any]
                            json1[StringConstant.id_token] = json1[StringConstant.accessToken]
                            json1[StringConstant.refreshToken] = json1[StringConstant.refreshToken]
                            MyDefaults.setData( value: json1[StringConstant.accessToken] as! String, key: DefaultKeys.KEY_TOKEN)
                            MyDefaults.setData(value: json1[StringConstant.refreshToken] as! String, key: DefaultKeys.KEY_REFRESH_TOKEN)
                            let user = Mapper<AuthenticationModel>().map(JSON:json1)!
                            self.apiGetBikerId(user)
                        }else{
                            if loginMethod == "username_password" {
                                var obj: [String: Any] = [:]
                                obj[StringConstant.rememberMe] = "true"
                                obj[StringConstant.username] = objModel[StringConstant.email]
                                obj[StringConstant.password] = objModel[StringConstant.password]
                                 self.callAuthenticateAndRegistaration(loginMethod:loginMethod , objModel:obj , authType:.Authentication)
                            } else {
                                 self.callAuthenticateAndRegistaration(loginMethod:loginMethod , objModel:objModel , authType:.Authentication)
                            }
                        }
                    }
                    else if statusCode == 400 || statusCode == 401 {
                        
                        if(authType == TypeOfAuthURL.Authentication && loginMethod != "username_password")
                        {
                            self.callAuthenticateAndRegistaration(loginMethod:loginMethod , objModel:objModel , authType:.Registration)
                        }else
                        {
                            self.errorCall(value: json as! [String : Any])
                        }
                    }
                    else
                    {
                        self.errorCall(value: json as! [String : Any])
                    }
                }else{
                    self.errorCall(value: RequestTimeOut().json)
                }
                
            }
        }
        
        func getActiveDeviceMapping(){
            
            let vehicleId:Int = MyDefaults.getInt(key: DefaultKeys.KEY_DefaultVehicleID)
            
            self.cloudHandlerObj.makeCloudRequest(subUrl:"\(RequestURL.URL_VEHICLES.rawValue)\(vehicleId)\(RequestURL.URL_ACTIVE_DEVICE_MAPPING.rawValue)", parameter:["":""], isheaderAUTH: true, requestType: .get)
            {
                response in
                
                print(response?.request?.url ?? "")
                
                let jsonString = NSString(data: (response?.data!)!, encoding: String.Encoding.utf8.rawValue)
                
                if let json = response?.result.value {
                    print(json)
                    if response?.response?.statusCode == 200
                    {
                        let userObj = Mapper<ActiveDevicesModel>().map(JSON:response?.result.value as! [String : Any])!
                        if userObj.isVirtualCm != nil {
                            MyDefaults.setBoolData(value: userObj.isVirtualCm!, key: DefaultKeys.IS_CM_VIRTUAL)
                        }

                        let data:[String:Any] = [StringConstant.data:userObj]
                        self.userManagerDelegate?.activeDeviceIsMapped!(data: data)
                    }
                    else
                    {
                        let ErrorModelObj = Mapper<ErrorModel>().map(JSONString: jsonString as! String)
                        let errorData:[String:Any] = [StringConstant.data:ErrorModelObj ?? " "]
                        self.userManagerDelegate?.noActiveDeviceIsMapped!(Error: errorData)
                    }
                }else{
                    self.errorCall(value: RequestTimeOut().json)
                }
            }
        }
        
        //api for getting bikerid with oauth access token
        func apiGetBikerId(_ authModel:AuthenticationModel){
            let suburl = "\(RequestURL.URL_BIKER_OAUTH.rawValue)"
            self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ response in
                
                if let json = response?.result.value {
                    let statusCode = response?.response?.statusCode
                    print("getBiker Sucess json : \(json)")
                    if (statusCode)! >= 200 && (statusCode)! < 300
                    {
                        let user = Mapper<AuthenticationModel>().map(JSON:json as! [String : Any])!
                        user.id_token = authModel.id_token
                      
                        let data:[String:Any] = [StringConstant.data:user]
                        
                       self.userManagerDelegate?.onSocialLoginAuthenticationSuccess!(data: data)
                        
                    } else {
                        // Failed
                    }
                }else{
                    //json absent
                }
            }
        }
        
        func getBiker(bikeID:Int)  {
            
            let suburl = "\(RequestURL.URL_BIKER.rawValue)\(bikeID)"
            self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ response in
                
                if let json = response?.result.value {
                    let statusCode = response?.response?.statusCode
                    print("json : \(json)")
                    if (statusCode)! >= 200 && (statusCode)! < 300
                    {
                        let user = Mapper<Biker>().map(JSON:json as! [String : Any])!
                        let data:[String:Any] = [StringConstant.data:user]
                        self.userManagerDelegate?.onGetBikerSuccess!(data: data)
                        
                    } else {
                        self.errorCall(value: json as! [String : Any])
                    }
                }else{
                    self.errorCall(value: RequestTimeOut().json)
                }
                
            }
        }
        
        func getAllBikes()  {
            
            let suburl = "\(RequestURL.URL_BIKES.rawValue)\(RequestURL.URL_BIKE_MODEL.rawValue)"
            
            self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ response in
                if let json = response?.result.value {
                    let statusCode = response?.response?.statusCode
                    print("JSON ",json)
                    if (statusCode)! >= 200 && (statusCode)! < 300
                    {
                        let listJson = [StringConstant.AllBikesList:json]
                        let bikes = Mapper<AllBikesList>().map(JSON:listJson)
                        let data:[String:Any] = [StringConstant.data:bikes ?? ""]
                        self.userManagerDelegate?.updateAllBikesList!(data: data)
                        
                    } else {
                        self.errorCall(value: json as! [String : Any])
                    }
                }else{
                    self.errorCall(value: RequestTimeOut().json)
                }
            }
        }
        
        
        func updateBiker(parameter:[String:Any])  {
            
            self.cloudHandlerObj.makeCloudRequest(subUrl: RequestURL.URL_BIKER.rawValue, parameter: parameter, isheaderAUTH: true, requestType: .put) { response in
                
                if let json = response?.result.value {
                    let statusCode = response?.response?.statusCode
                    print("Update Biker Res JSON ",json)
                    if (statusCode)! >= 200 && (statusCode)! < 300
                    {
                        let bikes = Mapper<Biker>().map(JSON:json as! [String : Any])!
                        let data:[String:Any] = [StringConstant.data:bikes]
                        self.userManagerDelegate?.onGetBikerSuccess!(data: data)
                        
                    } else {
                        self.errorCall(value: json as! [String : Any])
                    }
                }else{
                    self.errorCall(value: RequestTimeOut().json)
                }
                
            }
        }
        
        func updateVehicles(parameter:[String:Any])  {
            
            self.cloudHandlerObj.makeCloudRequest(subUrl: RequestURL.URL_VEHICLE.rawValue, parameter: parameter, isheaderAUTH: true, requestType: .put) { response in
                print(response ?? "")
                if let json = response?.result.value {
                    let statusCode = response?.response?.statusCode
                    print("statusCode ",statusCode ?? 0,"JSON ",json)
                    if (statusCode)! >= 200 && (statusCode)! < 300
                    {
                        let defaultVehicle = Mapper<DefaultVehicle>().map(JSON:json as! [String : Any])!
                        let data:[String:Any] = [StringConstant.data:defaultVehicle]
                        self.userManagerDelegate?.onVehiclesUpdate!(data: data)
                    } else {
                        self.errorCall(value: json as! [String : Any])
                    }
                }else{
                    self.errorCall(value: RequestTimeOut().json)
                }
            }
        }
        
        func postFcmToken(parameter:[String:Any])
        {
            self.cloudHandlerObj.makeCloudRequest(subUrl: RequestURL.URL_BIKER_TOKEN.rawValue, parameter: parameter, isheaderAUTH: true, requestType: .post) { response in
                
                if let json = response?.result.value{
                    
                    let statuscode = response?.response?.statusCode
                    if (statuscode)! >= 200 && (statuscode)! < 300
                    {
                        print("token posted successfully")
                    }
                    else
                    {
                        self.errorCall(value: json as! [String : Any])
                    }
                }else{
                    self.errorCall(value: RequestTimeOut().json)
                }
                
            }
        }
        
        //TODO:Error Call Handling Method
        
        func errorCall(value:[String : Any]!){
            let ErrorModelObj = Mapper<ErrorModel>().map(JSON:value)
            let errorData:[String:Any] = [StringConstant.data:ErrorModelObj ?? " "]
            self.userManagerDelegate?.onFailure!(Error: errorData)
        }
    }
    
