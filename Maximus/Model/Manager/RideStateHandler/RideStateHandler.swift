//
//  RideStateHandler.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 25/07/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import ObjectMapper
//import AVFoundation


///TBT Updates
struct TBTRange {
    
    static let TEN          = 10
    static let HUNDRED      = 100
    static let FIVE_HUNDRED = 500
    static let THOUSAND     = 1000
}

class RideStateHandler: NSObject {
    
    
    enum MANAUVER_STATUS : Int {
        case NONE = 0
        case EOJ        //END of The journey
        case POI        //POI Reach
    }

    
    //MARK:- Properties
    static let sharedInstance = RideStateHandler()
    
    fileprivate var bikerID:NSInteger?
    fileprivate var rideType:RideTypes?
    fileprivate var rideStatus:RideStates?
    fileprivate var rideDetails:Rides?
    
    fileprivate var totalLegCompleted:Int = 0
    fileprivate var tBTRoute:String?//----To save Planned direction Route
    
    fileprivate let navigation = NavigationLocator.sharedInstance()
    fileprivate let sinkManager = SinkManager.sharedInstance
    
    fileprivate let legContext = LegContext()
    fileprivate let stepContext = StepContext()
    
    fileprivate var distanceToSend:Int = 0
    fileprivate var prevDistanceToSend:Int64 = 0
    
    //MARK:- Properties - Current Ride Daily Iternary
    fileprivate var arrMilestoneLocs:[Int] = []
    fileprivate var arrWavePointNames:[String] = []
    fileprivate var totalStops:Int = 0
    fileprivate var plannedWavePointsModel:PlannedWavePointsModel?
    
    fileprivate var firstPoiName:String = "Not Available"
    fileprivate var firstPoiLat:Double = 0.0
    fileprivate var firstPoiLong:Double = 0.0
    
    fileprivate var secondPoiName:String = "Not Available"
    fileprivate var secondPoiLat:Double = 0.0
    fileprivate var secondPoiLong:Double = 0.0
    
    fileprivate var thirdPoiName:String = "Not Available"
    fileprivate var thirdPoiLat:Double = 0.0
    fileprivate var thirdPoiLong:Double = 0.0
    
    fileprivate var startLocation:StartLocation?
    fileprivate var startAddress:String?
    fileprivate var endLocation:EndLocation?
    fileprivate var endAdress:String?
    fileprivate var pointsPlotted:String?
    fileprivate var plotLegsWithIndexes:[Int] = []
    fileprivate var headProgress = ProcessHeading()
    fileprivate var rideStartTime = Date()
    fileprivate var neAlert:NSNavigationAlert?

    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()

    //MARK:- Private Methods
    private override init() {
        
        super.init()
        
        self.bikerID = MyDefaults.getInt (key: DefaultKeys.KEY_BIKERID)
        
        
        if  let currentRide = Rides.getMyProgressRide(context: context) //Rides.getRideInProgressObj(context: context, type: .NONE)
        {
             self.rideDetails = currentRide
             self.rideStatus = RideStates.InProgress
            
            if (self.rideDetails?.planned)!{
                self.rideType = RideTypes.Planned
            }else {
                self.rideType = RideTypes.AdHoc
            }
            
            self.tBTRoute = currentRide.tbt_route
            self.setTotalStops()
        }

        self.navigation.navigationLocatorDelegate = self
    }
    
    //MARK:- Public Methods - Getters
    
    func getPlannedWaypointsLocations() -> [Int]{
        return arrMilestoneLocs
        
    }
    
    func getTotalStops() -> Int{
        return totalStops
        
    }
    
    func getPlottedIndexes() -> [Int]{
        
        return self.plotLegsWithIndexes
        
    }
    
    func getPlannedWayPointsModel() -> PlannedWavePointsModel{
        
        if let planWp = self.plannedWavePointsModel{
            
            return planWp
            
        }else{
            
            return PlannedWavePointsModel()//[WavePointsList]
        }
        
    }
    
    func getStartEndDailyDestinationLocations() -> [String:Any]{
        if startLocation != nil {
            let dictionary:[String:Any] = ["StartLocation"      :self.startLocation!,
                                           "StartLocationName"  :self.startAddress!,
                                           "EndLocation"        :self.endLocation!,
                                           "EndLocationName"    :self.endAdress!,
                                           "Points"             :self.pointsPlotted!]
            return dictionary
            
        }else{
            return [:]
        }
    }
    
    
    func getFirstPoiDetails() -> [String:Any]{
        let dictionary:[String:Any] = [StringConstant.FIRST_POI:self.firstPoiName,
                                       StringConstant.LATITUDE:self.firstPoiLat,
                                       StringConstant.LONGITUDE:self.firstPoiLong]
        return dictionary
    }
    
    func getSecondPoiDetails() -> [String:Any]{
        let dictionary:[String:Any] = [StringConstant.SECOND_POI:self.secondPoiName,
                                       StringConstant.LATITUDE:self.secondPoiLat,
                                       StringConstant.LONGITUDE:self.secondPoiLong]
        return dictionary
    }
    
    func getThirdPoiDetails() -> [String:Any]{
        let dictionary:[String:Any] = [StringConstant.THIRD_POI:self.thirdPoiName,
                                       StringConstant.LATITUDE:self.thirdPoiLat,
                                       StringConstant.LONGITUDE:self.thirdPoiLong]
        return dictionary
    }
    
    func setRideStartTime(rideStartTime: Date)  {
        
        let dateString = rideStartTime.dateToStringWithFormat()
        MyDefaults.setData(value: dateString, key: "TimeStamp")
    }
    
    func getRideStartTime() -> Date {
        if let dateString = MyDefaults.getData(key: "TimeStamp") as? String, dateString != "" {
        return dateString.convertToDate()
        }
        return Date()
    }
    
    //MARK:- Public Methods - Setters
    
    func setTotalStops() {
        
        if let tbt_route = self.tBTRoute {
            
            if let objroute:RouteDetails = Mapper<RouteDetails>().map(JSONString: tbt_route){
                if let route = objroute.routeList?[0] {
                    
                    let legs:[Legs] = route.legs!
                    if (legs.count > 1){
                        
                        self.totalStops = legs.count - 1
                        
                    }else{
                        self.totalStops = 0
                    }
                    
                }
            }
            
        }
    }
    
    
    //MARK: - Public Methods - Processing
    func processPOI() {
        
        self.plotLegsWithIndexes.removeAll()
        self.firstPoiName = "Not Available"
        self.secondPoiName = "Not Available"
        self.thirdPoiName = "Not Available"
        self.firstPoiLat = 0.0
        self.firstPoiLong = 0.0
        self.secondPoiLat = 0.0
        self.secondPoiLong = 0.0
        self.thirdPoiLat = 0.0
        self.thirdPoiLong = 0.0
        
        
        let tbt_route:String = self.getCurrentRideTbtRoute()
        if (tbt_route != ""){
            
            if let objroute:RouteDetails = Mapper<RouteDetails>().map(JSONString: tbt_route){
                if let route = objroute.routeList?[0] {
                    
                    let legs:[Legs] = route.legs!
                    
                    let points = route.totalPolylinePoints?.points
                    let arrMilestoneLocs = self.getPlannedWaypointsLocations()
                    
                    let legCompleted = self.getCurrentRideTotalLegCompleted()
                    
                    
                    var startMileStone:Int = 0
                    var endMileStone:Int = 0
                    
                    
                    
                    if(legs.count > 0){
                        if legCompleted >= endMileStone {
                            for index in 0..<arrMilestoneLocs.count{
                                
                                endMileStone = arrMilestoneLocs[index]
                                
                                if(index != 0) && (arrMilestoneLocs[index] >= legCompleted){
                                    
                                    let loc = arrMilestoneLocs[index - 1] + 1
                                    if (loc <= endMileStone){
                                        startMileStone = loc
                                    }
                                }else{
                                    startMileStone = 1
                                }
                                
                                if(arrMilestoneLocs[index] > legCompleted){
                                    
                                    break
                                }
                            }
                        }
                    }
                    
                    
                    //fragmented route
                    if legs.count >= endMileStone {
                        
                        var counter = 1
                        for index in 0..<legs.count{
                            let successful = (startMileStone...endMileStone).contains(counter)
                            print("Daily Iternary: counter      \(counter)")
                            print("Daily Iternary: successful        \(successful)")
                            counter = counter + 1
                            if (successful){
                                
                                plotLegsWithIndexes.append(index)
                            }
                        }
                        
                        
                        
                        let stIndex = startMileStone - 1
                        let endIndex = endMileStone - 1
                        
                        if (stIndex >= 0) && (stIndex < legs.count) && (endIndex >= 0) && (endIndex < legs.count){
                            
                            if let startLoc = legs[stIndex].startLocation{
                                self.startLocation = startLoc
                            }
                            
                            if let startAdd = legs[stIndex].startAddress{
                                self.startAddress = startAdd
                            }else{
                                self.startAddress = ""
                            }
                            
                            if let endLoc = legs[endIndex].endLocation{
                                self.endLocation = endLoc
                            }
                            
                            if let endAdd = legs[endIndex].endAddress{
                                self.endAdress = endAdd
                            }else{
                                self.endAdress = ""
                            }
                            
                            if let points = points{
                                self.pointsPlotted = points
                            }
                            
                            
                            
                        }else{
                            
                            self.startLocation = nil
                            self.startAddress = nil
                            self.endLocation = nil
                            self.endAdress = nil
                            self.pointsPlotted = nil
                            
                        }
                        
                    }
                    
                    
                    //update poi
                    if legs.count > legCompleted{
                        if legs.count > legCompleted + 1{
                            if  legCompleted+1 < endMileStone{
                                let leg:Legs = legs[legCompleted+1]
                                
                                self.firstPoiName = leg.startAddress ?? ""
                                
                                if arrWavePointNames.count > (legCompleted + 2){
                                    
                                    let name = arrWavePointNames[legCompleted + 2]
                                    if  name != ""{
                                        
                                        self.firstPoiName = name
                                        
                                    }
                                    
                                }
                                
                                
                                self.firstPoiLat = (leg.startLocation?.lat)!
                                self.firstPoiLong = (leg.startLocation?.lng)!
                            }
                        }
                        
                        
                        if legs.count > legCompleted + 2{
                            let leg:Legs = legs[legCompleted+2]
                            
                            if  legCompleted+2 < endMileStone{
                                
                                if let _ = leg.startAddress {
                                    self.secondPoiName = leg.startAddress!
                                }
                                
                                if arrWavePointNames.count > (legCompleted + 3){
                                    let name = arrWavePointNames[legCompleted + 3]
                                    if  name != ""{
                                        
                                        self.secondPoiName = name
                                        
                                    }
                                    
                                }
                                
                                self.secondPoiLat = (leg.startLocation?.lat)!
                                self.secondPoiLong = (leg.startLocation?.lng)!
                                
                                
                            }
                        }
                        
                        if legs.count > legCompleted + 3{
                            let leg:Legs = legs[legCompleted+3]
                            
                            if  legCompleted+3 < endMileStone{
                                
                                self.thirdPoiName = leg.startAddress ?? ""
                                
                                if arrWavePointNames.count > (legCompleted + 4){
                                    let name = arrWavePointNames[legCompleted + 4]
                                    if  name != ""{
                                        
                                        self.thirdPoiName = name
                                        
                                    }
                                    
                                }
                                self.thirdPoiLat = (leg.startLocation?.lat)!
                                self.thirdPoiLong = (leg.startLocation?.lng)!
                                
                                
                            }
                        }
                    }
                    
                }
            }
            
            
        }
    }
    
    
    func cacheCurrentRideDetails(){
        //save in userDefaults
        //LocalCacheData.shared.saveProgressRideDetailsData(value: (self.rideDetails?.toJSONString())!)
    }
    
    func deleteCachedRideDetails(){
        //deleteFrom userDefaults
        LocalCacheData.shared.deleteProgressRideDetailsData()
    }
    
    func setBikerId(bikerId:NSInteger){
        self.bikerID = bikerId
    }
    
    func getBikerId() -> NSInteger{
        if let bId = self.bikerID{
            return bId
        }else{
            return -1
        }
    }
    
    func setCurrentRideDetails(rideDetails:Rides,rideType:RideTypes,rideState:RideStates){
        
        self.rideDetails = rideDetails
        self.rideType = rideDetails.planned ? RideTypes.Planned : RideTypes.AdHoc
        self.rideStatus = rideState
        self.cacheCurrentRideDetails()
    }
    
    func setCurrentRideLegCompleted(){
        self.totalLegCompleted = self.totalLegCompleted + 1
    }
    
    func setCurrentRideTbtRoute(rideTbtRoute:String){
        self.tBTRoute = rideTbtRoute
        self.setTotalStops()
        MyDefaults.setData(value: self.tBTRoute!, key: DefaultKeys.KEY_CURRENT_RIDE_TBT_ROUTE)
        let rideObj = RideManager()
        rideObj.rideManagerDelegate = self
        rideObj.getPlannedWavepoints(rideID: (self.rideDetails?.rideID?.toInt())!)

    }
    
    func isAnyRideInProgress() -> Bool{
        
        if self.rideDetails != nil{
            return true
        }else{
            return false
        }
    }
    
    func clearCurrentRideDetails() {
        
        self.rideType = nil
        self.rideStatus = nil
        self.rideDetails = nil
        self.totalLegCompleted = 0
        self.tBTRoute = nil
        self.deleteCachedRideDetails()
    }
    
    func getCurrentRideId() -> NSInteger{
        
        if let currentRideDetails = Rides.getMyProgressRide(context: context){
            if let rideId = currentRideDetails.rideID{
                return rideId.toInt()!
            }else{
                return -1
            }
        }else{
            return -1
        }
    }
    
    func getCurrentRideType() -> RideTypes{
        if let rideType = self.rideType{
            return rideType
        }else{
            return RideTypes.None
        }
    }
    
    func getCurrentRideStatus() -> RideStates{
        if let rideStatus = self.rideStatus{
            return rideStatus
        }else{
            return RideStates.Finished
        }
    }
    
    func getCurrentRideDetails() -> Rides {
        if let currentRide = Rides.getMyProgressRide(context: context) {
            self.rideDetails = currentRide
            return currentRide
        }
        return self.rideDetails!
    }
    
    func getCurrentRideTotalLegCompleted() -> Int{
        return self.totalLegCompleted
    }
    
    func getCurrentRideTbtRoute() -> String{
        if let tbtRoute = self.tBTRoute{
            return tbtRoute
        }else{
            return ""
        }
    }
    
    //Get Status of navigtion Engine
//    func getStatusOFNavigationEngine()->NavigationStatus{
//        return self.navigation.getStatusOfNavigation()
//    }
    
    func setTbtRouteToNavigation(TBT_Route:String){
        self.navigation.directionFetcher(mDirectionResponse: TBT_Route)
    }
    
    func startNavigationEngine(){
        navigation.startNavigationEngine()
    }
    
    func resumeNavigationEngine(){
        navigation.resumeNavigationEngine()
        
    }
    
    func stopNavigationEngine(){
        self.navigation.stopNavigationEngine()
    }
    
    
    fileprivate var isShownAlert:Bool = false
    @available(iOS 8.0, *)
    func showDeviationAlert(distance: String ,navigator: NSNavigator?) {
        
        //To check that is already Shown Deviation alert
        if !isShownAlert {
            isShownAlert = true
            let yesAction = UIAlertAction.init(title: "YES", style: .default) { (action) -> Void in
                navigator?.getStatus()?.getDeviation()?.setRouting(true)
                self.isShownAlert = false
            }
            
            let noAction = UIAlertAction.init(title: "NO", style: .default) { (action) -> Void in
                navigator?.getStatus()?.getDeviation()?.setRouting(false)
                self.isShownAlert = false
            }
            
            let alertController = UIAlertController.init(title: "You are deviated by \(distance) !!", message: ToastMsgConstant.alertPermisReroute, preferredStyle: .alert)
            alertController.addAction(noAction)
            alertController.addAction(yesAction)
            
            UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showDeviationAlertwithoutNetwork(distance: String) {
        
        //To check that is already Shown Deviation alert
        if !isShownAlert {
            isShownAlert = true
            
            let okAction = UIAlertAction.init(title: "OK", style: .default) { (action) -> Void in
                self.isShownAlert = false
            }
            
            let alertController = UIAlertController.init(title: "You are deviated by \(distance) !!", message:ToastMsgConstant.alertReroute, preferredStyle: .alert)
            alertController.addAction(okAction)
            UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    func sendApprochingStopInfo(status: NSStatus?) {
        
        guard let _ =  status?.getCurrentLeg()?.getNext() else  {
            
            print("Last leg , reaching destination")
            
            if let _ = status?.getNextStep(){
                
            }else{
                if let distance = status?.getManeuver()?.getPoint()?.getDistance()?.getMeters(), distance < 500 {
                    //fire destination reaching
                    MaximusDM.sharedInstance.destinationApproaching()
                }
            }
            return
        }
        
        if let _ = status?.getNextStep(){
            
        }else{
            print("POI Reaching")
            if let distance = status?.getManeuver()?.getPoint()?.getDistance()?.getMeters(), distance < 500 {
                //fire poi reaching
                MaximusDM.sharedInstance.poiApproaching()
            }
        }
    }
    
}

extension RideStateHandler: NavigationLocatorDelegate {
    
    //MARK:- NavigationLocatorDelegate Methods
    func stateChanged(_ navigator: NSNavigator?, state: NSNavigatorState){
        print("@StateHandler: State: \(state.rawValue)")
        switch state {
        case .NAVIGATORSTATEREADY:
            break
        case .NAVIGATORSTATEACTIVE:
            ///Clear Old head Instructions
            ProcessHeading.sharedInstance().clearOldHeadingInstructions()
            MaximusDM.sharedInstance.navigatorBecameActive()
            break
        case .NAVIGATORSTATEPAUSED:
            break
        case .NAVIGATORSTATEROUTING:
            break
        case .NAVIGATORSTATENOROUTE:
            break
        case .NAVIGATORSTATENOLOCATION:
             MaximusDM.sharedInstance.sendWeakSignalCommand()///--Send weak signal command to DM
            break
        case .NAVIGATORSTATECOMPLETED:
            break
        case .NAVIGATORSTATEERROR:
            break
        }
    }

    /*****************************************************************************
     * Function :   legUpdate
     * Input    :   none
     * Output   :   legUpdates
     * Comment  :   Event call after every legUpdate
     *
     ****************************************************************************/
    
    func legUpdate(_ navigator: NSNavigator?, legProgress: NSLegProgress?){
        
        print("@StateHandler: Leg Update ")
        
        if let legType = legProgress?.getType(){
            
            switch legType {
                
            case NSProgressType.PROGRESSTYPECOMPLETED:
                legContext.changeStateToLegCompleted(legDetail: legProgress!)
                
            case NSProgressType.PROGRESSTYPESTARTED:
                legContext.changeStateToLegStarted(legDetail: legProgress!)
                
            case NSProgressType.PROGRESSTYPEINTERMEDIATE:
                legContext.changeStateToLegInProgress(legDetail: legProgress!)
                
            case .PROGRESSTYPESKIPPED:
                legContext.changeStateToLegSkipped(legDetail: legProgress!)
                break
            }
        }
    }
    /*****************************************************************************
     * Function :   stepUpdate
     * Input    :   none
     * Output   :   stepProgress
     * Comment  :   Event call after every stepprogress Update
     *
     ****************************************************************************/
    
    func stepUpdate(_ navigator: NSNavigator?, stepProgress: NSStepProgress?){
        
        if let stepProgressType = stepProgress?.getType(){
            
            switch stepProgressType {
            case NSProgressType.PROGRESSTYPESTARTED:
                
                stepContext.changeStateToStepStarted(stepDetail: stepProgress!)
            case NSProgressType.PROGRESSTYPEINTERMEDIATE:
                stepContext.changeStateToStepInProgress(stepDetail: stepProgress!)
                
            case NSProgressType.PROGRESSTYPECOMPLETED:
                self.distanceToSend = 0
                self.prevDistanceToSend = 0
                stepContext.changeStateToStepCompleted(stepDetail: stepProgress!)
                
            case .PROGRESSTYPESKIPPED:
                stepContext.changeStateToStepSkipped(stepDetail: stepProgress!)
                break
                //
            }
        }
        
    }
    
    /*****************************************************************************
     * Function :   maneuver
     * Input    :   none
     * Output   :   get the coming & Upcoming Turn
     * Comment  :   Event call after every Manuever Update
     *
     ****************************************************************************/
    func maneuver(_ navigator: NSNavigator?, maneuver: NSManeuver?){
       
        
        ////UI Updates on DMDispalyVC calls
        
        if (maneuver?.getStep()?.hasExitNumber())! {
            //Temporory showing the exit number of round Turn

            let exitNumber:Int16 = (maneuver?.getStep()?.getExitNumber())!
            
        }else{
        }
        print("@StateHandler: maneuver Called)")
        
        ///* 📢📢📢AUDIO TURN FUNCTIONALITY ⚠️⚠️⚠️*//
        /*        if let menuver = maneuver {
         let typecurrent = menuver.getType()
         let (turn,iconId) = turnIconData[typecurrent]
         print("@StateHandler: icon ID - \(iconId)")
         //Text to Speech code is here
         SpeechText.shared.say(sentence: turn)
         }
         */
        
    }
    
    
    /*****************************************************************************
     * Function :   update
     * Input    :   none
     * Output   :   get the update of navigation
     * Comment  :   Event call after every upadate happen of Navigation Side
     *
     ****************************************************************************/
    func update(_ navigator: NSNavigator?, status: NSStatus?) {
        
        self.sendApprochingStopInfo(status: status)
        
        //
        //* For Updating the Manuer over DM */
        print("@StateHandler: Update Called manuver available")

        if let menuver = status?.getManeuver() {
            
            print("@StateHandler: manuver available")
            var currentTurn = ""
            var nextTurn    = ""
            var currentIcon:Int?
            var nextIcon:Int?

            
            //**************** Current Turn Data *************************/
            
            let typecurrent = menuver.getType()
            let (iconId,turn) = turnIconData[typecurrent]!
            currentTurn = turn
            
            if currentTurn.lowercased().range(of:"head") != nil {
                ProcessHeading.sharedInstance().notifyHeading(headStatus: status!)
                //headProgress.notifyHeading(headStatus: status!)
            }else{
            
                if iconId == Int(ICON_NAV_ROUND_ABOUT_CURRENT_L.rawValue) {
                    
                    let hasExitNumber   = status?.getManeuver()?.getStep()?.hasExitNumber()//--check has exitNumber to current turn
                    let hasExitBearing  = status?.getManeuver()?.getStep()?.hasExitBearing()//--check has Bearing to current turn
                    
                    if (hasExitNumber! && hasExitBearing!) {
                        let exitNumber = Int((status?.getManeuver()?.getStep()?.getExitNumber())!)
                        let exitBearing  = Int((status?.getManeuver()?.getStep()?.getExitBearing())!)
                        
                        MaximusDM.sharedInstance.currentRoundAbout(exitNumber: exitNumber, angle: exitBearing ,iconID:iconId,isSink:false)
                        
                    }
                }else{
                    
                    currentIcon = iconId
                }
                
                
                
                //**************** Next Turn Data *************************/
                
                if let typeNext = menuver.getNext()?.getType() {
                    let iconId = nexrTurnIcon[typeNext]!
                    //for checking
                    let (_,nxtxtx) = turnIconData[typeNext]!
                    
                    print("@StateHandler: Current Turn: \(turn)")
                    print("@StateHandler: Next Turn: \(nxtxtx)")
                    nextTurn = nxtxtx
                    
                    if iconId == Int(ICON_NAV_ROUND_ABOUT_NEXT_S.rawValue) {
                        
                        let hasExitNumber = status?.getManeuver()?.getNext()?.getStep()?.hasExitNumber()//--check has exitNumber to current turn
                        let hasExitBearing = status?.getManeuver()?.getNext()?.getStep()?.hasExitBearing()//--check has Bearing to current turn
                        
                        if (hasExitNumber! && hasExitBearing!){
                            let exitNumber = Int((status?.getManeuver()?.getNext()?.getStep()?.getExitNumber())!)
                            let exitBearing = Int((status?.getManeuver()?.getNext()?.getStep()?.getExitBearing())!)
                            MaximusDM.sharedInstance.nextRoundAbout(exitNumber: exitNumber, angle: exitBearing, iconID:iconId,isSink:false)
                        }
                    }else {
                        nextIcon = iconId
                    }
                    
                }else{
                    let nextStatus = self.isNextENDofJourneyOrPOIReach(status: status!)
                    
                    switch nextStatus {
                    case .NONE:
                        print("None status of Manuever")
                        break
                    case .POI:
                        nextIcon = Int(ICON_ARRIVED.rawValue)
                        break
                    case .EOJ:
                        nextIcon = Int(ICON_JOURNEY_COMPLETE.rawValue)
                        break
                    }
                }
                
                
                //TESTING PURPOSE
                if currentTurn == "" {
                    currentTurn = "No Turn"
                }
                
                if nextTurn == "" {
                    nextTurn = "No Turn"
                }
                
                //* To Notify the App Screens About Updates */
                
                self.sinkManager.notifyObservers(notificationKey: self.sinkManager.SINK_NOTIFICATION_CURRENT_TURN, sender: self, data: ["Data":currentTurn])
                
                self.sinkManager.notifyObservers(notificationKey: self.sinkManager.SINK_NOTIFICATION_NEXT_TURN, sender: self, data: ["Data":nextTurn])

                //* To Notify to DM Screens About Updates */

                if let iconCurr = currentIcon {
                    self.sinkManager.notifyObservers(notificationKey: self.sinkManager.SINK_NOTIFICATION_CURRENT_MANUEVER, sender: self, data:["data":iconCurr])
                }
                if let iconNext = nextIcon {
                    self.sinkManager.notifyObservers(notificationKey: self.sinkManager.SINK_NOTIFICATION_NEXT_MANUEVER, sender: self, data:["data":iconNext])
                }
                
                if self.neAlert != nil {
                    let turn = neAlert?.getAttributes()?.getInteger("turn")
                    let angle = neAlert?.getAttributes()?.getDouble("angle")
                    
                    
                    if let alertType:NSNavigationAlertType = (neAlert?.getType()) {
                        print("@Alerts : Type: \(String(describing: alertType.hashValue)) Turn: \(turn ?? 55) and angle: \(angle ?? 111)")
                        
                        let processAlert = ProcessWindingRoadAlert(_alert: neAlert!)
                        processAlert.processAlert()
                    }
                }
                
                ///* Notify Remaining distnace of current step */
                if let distance = status?.getManeuver()?.getPoint()?.getDistance()?.getMeters() {
                    print("@StateHandler: Distance In Meters: \(distance)")
                    
                    if distance != self.prevDistanceToSend {
                        // broadcast distance
                        sinkManager.notifyObservers(notificationKey: sinkManager.SINK_NOTIFICATION_REMAINING_DISTANCE, sender: self, data: ["Data":distance])
                        self.prevDistanceToSend = distance
                    }
                    
                }

            }
        }
    }
    /*****************************************************************************
     * Function :   deviation
     * Input    :   none
     * Output   :   Deviation summary
     * Comment  :   Event call after every 500m distance after deviation
     *
     ****************************************************************************/
    func deviation(_ navigator: NSNavigator?, deviation: NSDeviation?) {
        print("@StateHandler: deviation called")
        let distance = deviation?.getPoint()?.getDistance()?.getMeters()
        var strDistance = ""
        if distance! < 1000 && distance! >= 0{
            strDistance = String(format:"%d m",distance!)
        }else{
            let dist = Double(distance!)/1000.0
            strDistance = String(format:"%.2f km",dist)
        }
        
        /*
        if NetworkCheck.isNetwork() {
            //Alert Call for deviation event
            self.showDeviationAlert(distance: strDistance ,navigator: navigator!)
        }else{
            //Alert Call for deviation event without Internet connection
            self.showDeviationAlertwithoutNetwork(distance: strDistance)
        }
         */
        
        /////Observer call for deviation detect
        if let objNavigator = navigator {
            sinkManager.notifyObservers(notificationKey: sinkManager.SINK_NOTIFICATION_DEVIATION_DETECTED, sender: self, data: ["data":objNavigator])
        }

    }
    /*****************************************************************************
     * Function :   routeChanged
     * Input    :   none
     * Output   :   Return Updated Route
     * Comment  :   Event call after route changed(Re-routing)
     *
     ****************************************************************************/
    func routeChanged(_ navigator: NSNavigator?, directions: NSDirections?, route: NSRoute?) {
        print("@StateHandler: Route Change called")
//        DispatchQueue.main.async{
//            UIApplication.topViewController()?.view.makeToast("📢📢 NavigationLocator Route change Called")
//        }
    }
    
    func alert(_ navigator: NSNavigator?, alert: NSNavigationAlert?) {
        
        self.neAlert = alert
        alert?.subscribe(self)
        
        let turn = alert?.getAttributes()?.getInteger("turn") //alert?.getAttributes()?.getInteger("turn")
        let angle = alert?.getAttributes()?.getDouble("angle") //alert?.getAttributes()?.getDouble("angle")
        
        if let alertType:NSNavigationAlertType = (alert?.getType()) {
            print("@Alerts : Type: \(String(describing: alertType.hashValue)) Turn: \(turn ?? 55) and angle: \(angle ?? 111)")
            
            switch alertType {
                
            case .NAVIGATIONALERTTYPESHARPBENDAHEAD: //Sharp Bend Ahead
                //                self.alert.subscribe(self)
                print("@Alerts : Sharp Bend Ahead")
                //send sharp brend as Alert
                if let turn:Int64 = (alert?.getAttributes()?.getInteger("turn"))!{
                    
                    switch turn {
                        
                    case 1:
                        //sharp left
                        print("@Alerts : Sharp left turn")
                        MaximusDM.sharedInstance.sendSharpBendAlerts(alertId: Int(LEFT_HAND_CURVE_AHEAD.rawValue), type: NON_URGENT_ALERT)
                        break
                        
                    case 2:
                        //sharp right
                        print("@Alerts : Sharp right turn")
                        MaximusDM.sharedInstance.sendSharpBendAlerts(alertId: Int(RIGHT_HAND_CURVE_AHEAD.rawValue), type: NON_URGENT_ALERT)
                        break
                        
                    case 3:
                        //hair pin left
                        print("@Alerts : Hair pin left")
                        MaximusDM.sharedInstance.sendSharpBendAlerts(alertId: Int(LEFT_HAIR_PIN_BEND_AHEAD.rawValue), type: NON_URGENT_ALERT)
                        break
                        
                    case 4:
                        //hair pin right
                        print("@Alerts :  Hair pin right")
                        MaximusDM.sharedInstance.sendSharpBendAlerts(alertId: Int(RIGHT_HAIR_PIN_BEND_AHEAD.rawValue), type: NON_URGENT_ALERT)
                        
                        break
                        
                    default:
                        break
                    }
                    
                }
                
            case .NAVIGATIONALERTTYPEWINDINGROADAHEAD:
                break
            case .NAVIGATIONALERTTYPEWINDINGROADBEGIN:
                break
            case .NAVIGATIONALERTTYPEWINDINGROADEND:
                break
            }
        }
    }
    
}

extension RideStateHandler: NSNavigationAlertEvent {
    
    func expired(_ navigator: NSNavigator?, alert: NSNavigationAlert?) {
        print("@Alerts : Sharp End alert expired")
        //sharp bend expired
        
        if let turn:Int64 = (alert?.getAttributes()?.getInteger("turn"))!{
            
            switch turn {
                
            case 1:
                //sharp left
                print("@Alerts : Sharp left turn")
                MaximusDM.sharedInstance.sendSharpBendExpired(alertId: Int(LEFT_HAND_CURVE_AHEAD.rawValue), type: NON_URGENT_ALERT)
                break
                
            case 2:
                //sharp right
                print("@Alerts : Sharp right turn")
                MaximusDM.sharedInstance.sendSharpBendExpired(alertId: Int(RIGHT_HAND_CURVE_AHEAD.rawValue), type: NON_URGENT_ALERT)
                break
                
            case 3:
                //hair pin left
                print("@Alerts : Hair pin left")
                MaximusDM.sharedInstance.sendSharpBendExpired(alertId: Int(LEFT_HAIR_PIN_BEND_AHEAD.rawValue), type: NON_URGENT_ALERT)
                break
                
            case 4:
                //hair pin right
                print("@Alerts :  Hair pin right")
                MaximusDM.sharedInstance.sendSharpBendExpired(alertId: Int(RIGHT_HAIR_PIN_BEND_AHEAD.rawValue), type: NON_URGENT_ALERT)
                
                break
                
            default:
                break
            }
            
        }
    }
    
}

extension RideStateHandler:RideManagerDelegate{
    
    
    func getWavePointsSuccess(data:[String:Any]){
        
        arrMilestoneLocs.removeAll()
        arrWavePointNames.removeAll()
        
        
        if let obj:PlannedWavePointsModel = data["data"] as? PlannedWavePointsModel {
            
            self.plannedWavePointsModel = obj
            if let wavePointList = obj.WavePointsList{
                if (wavePointList.count) > 0 {
                    var i = 0
                    for wavePoint in wavePointList{
                        
                        if(wavePoint.types?.contains(StringConstant.WAVEPOINT_TYPE_MILESTONE))!{
                            
                            arrMilestoneLocs.append(i-1)
                        }
                        i += 1
                        //arrWavePoints.append(wavePoint)
                        if let name = wavePoint.name{
                            arrWavePointNames.append(name)
                            
                        }else{
                            
                            arrWavePointNames.append("")
                        }
                    }
                    
                    arrMilestoneLocs.append(i-1)//add last destination
                }
            }
        }
        processPOI()
    }
    
    func onFailure(Error:[String:Any]){
        let errModel:ErrorModel = Error["data"] as! ErrorModel
        print(errModel.message ?? " ")
        
        
    }
    
    fileprivate func isNextENDofJourneyOrPOIReach(status:NSStatus) -> MANAUVER_STATUS{
        return self.checkisPOIorEOJ(leg: status.getNextLeg())
        
    }
    
    fileprivate func isCurrentENDofJourneyOrPOIReach(status:NSStatus) -> MANAUVER_STATUS{
        return self.checkisPOIorEOJ(leg: status.getCurrentLeg())
    }
    
    fileprivate func checkisPOIorEOJ(leg:NSLeg?) -> MANAUVER_STATUS{
        //Its Journey End
        
        //Journey END
        if  (leg == nil) {
            return .EOJ
        }
        ///POI Reach
        if  (leg != nil) {
            return .POI
        }
        
        return .NONE
    }
    

}
