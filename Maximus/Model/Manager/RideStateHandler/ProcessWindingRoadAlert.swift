//
//  ProcessWindingRoadAlert.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 03/04/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import Foundation



class ProcessWindingRoadAlert:NSObject {
    
    var alert:NSNavigationAlert?
    
    init(_alert: NSNavigationAlert) {
        super.init()
        self.alert = _alert
    }
    
    func processAlert(){
        
        let turn = self.alert?.getAttributes()?.getInteger("turn") //alert?.getAttributes()?.getInteger("turn")
        let angle = self.alert?.getAttributes()?.getDouble("angle") //alert?.getAttributes()?.getDouble("angle")
        
        if let alertType:NSNavigationAlertType = (alert?.getType()) {
        print("@Alerts : Type: \(String(describing: alertType.hashValue)) Turn: \(turn ?? 55) and angle: \(angle ?? 111)")
            
            switch alertType {
                
            case .NAVIGATIONALERTTYPESHARPBENDAHEAD: //Sharp Bend Ahead
                break
                
            case .NAVIGATIONALERTTYPEWINDINGROADAHEAD:
                //Winding road ahead
                print("@Alerts :  Winding road ahead")
                break
                
            case .NAVIGATIONALERTTYPEWINDINGROADBEGIN: // Winding road begin
                //send winding road command // send as manuever icon
                print("@Alerts : Sending winding road ahead ")
                MaximusDM.sharedInstance.windingRoadStarted()

                break
                
            case .NAVIGATIONALERTTYPEWINDINGROADEND:
                // Winding road End
                 print("@Alerts : Sending winding road end ")
                 MaximusDM.sharedInstance.windingRoadEnds()
                break
                
            }
        }
    }
    
}

