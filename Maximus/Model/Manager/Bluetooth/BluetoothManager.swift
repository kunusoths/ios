
//
//  BluetoothManager.swift
//  Maximus
//
//  Created by Sriram G on 03/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import CoreBluetooth

class BluetoothManager:NSObject
{
    var bleDevicesList = [Any]()
    var connectedPeripheral:CBPeripheral!
    var dmBatteyLevel:Int?
    var recivedCurrentImageInfo: [UInt8] = []
    var otaModeData:[String:Data]?

    
    ///Singleton instance of Class
    private static var privateShared : BluetoothManager?
    class func sharedInstance() -> BluetoothManager { // change class to final to prevent override
        guard let shared = privateShared else {
            privateShared = BluetoothManager()
            return privateShared!
        }
        return shared
    }
    
    override init() {
        super.init()
    }
    
    func configureBLEService(name: String){
        //Configure Bluetooth Service
        BluetoothServices.sharedInstance().configureBLEDelegateService(namePeripheral: name)
        BluetoothServices.sharedInstance().bleServiceDelegate = self
    }
    
    ///Shared Instance Released here
    class func destroyInstance() {
        privateShared = nil
        BluetoothServices.destroyInstance()
    }
    
    
    //MARK:Public Methods
    func startScanning(namePeripheral:String) {
        BluetoothServices.sharedInstance().startBLEscan(namePeripheral: namePeripheral)
    }
    /**Get the STATE of CentaralManager*/
    func getBLEState()-> BluetoothState {
        let status = BluetoothServices.sharedInstance().getPairingSatus()
        return status
    }
    
    func stopScanning() {
        BluetoothServices.sharedInstance().stopBLEScan()
    }
        
    func disconnectPeripheral() {
        BluetoothServices.sharedInstance().cancelPeripheralConnection()
    }
    
    func sendToBle(data: Data, char: BLEChar) {
        let _ = BluetoothServices.sharedInstance().writeToBleChar(infoData: data, typeOfChar: char)
        //BluetoothServices.sharedInstance().writeDataToBLE(data: data)
    }
    
    func setDmBatteryLevel(value:Int) {
        self.dmBatteyLevel = value
    }
    
    func getDMBatteryLevel() -> Int {
        
        return self.dmBatteyLevel ?? 0 // to be removed
    }
    
    func getConnectedPeripheral()->CBPeripheral? {
        
        guard let peripheral = BluetoothServices.sharedInstance().connectedPeripheral else {
            return nil
        }
        
        self.connectedPeripheral = BluetoothServices.sharedInstance().connectedPeripheral
        return peripheral
    }
    
    func setCurrentImageInfo(info:[UInt8]) {
        OTAWrapper.sharedInstace().saveCurrentImgInfo(info)
        self.recivedCurrentImageInfo = info
    }
    
    func getCurrentImageInfo() -> [UInt8] {
        return self.recivedCurrentImageInfo
    }
    
    //Get and set DM OTA mode
    
    func setOTAMode(data: Data) {
        print("In Mode Set")
        if data.hashValue == 188 {
            print("DM Mode is OTA Mode")
            self.otaModeData = [CURRENT_DM_MODE.DM_OTA_MODE : data]
        }else{
             print("DM Mode is APP Mode")
            self.otaModeData = [CURRENT_DM_MODE.DM_DEFAULT_MODE : data]
        }
    }
    
    func getOTAMode() -> [String:Data] {
        var defaultState = 5
        let defaultVal = Data(bytes: &defaultState, count: MemoryLayout<Int>.size)
        return otaModeData ?? [CURRENT_DM_MODE.DM_UNKNOWN_MODE: defaultVal]
    }
    
    
    
    func unpaireBLE() {
        BluetoothServices.sharedInstance().invalidateHeartBeat()
        BluetoothServices.sharedInstance().invalidateTimer()
        self.stopScanning()
        BluetoothServices.sharedInstance().removeSavedBLE()
        MyDefaults.removeForKey(key: "NONCE")
        self.disconnectPeripheral()
    }
}

extension BluetoothManager:BLEServiceDelegate {
    
    func notifyUpdatedBLEStatus(status:BluetoothState){
    
        print("@BLE:New Status:\(status)")
        SinkManager.sharedInstance.notifyObservers(notificationKey: SinkManager.sharedInstance.SINK_NOTIFICATION_BLUETOOTH_STATE_UPDATE, sender: self, data: ["data":status])
        
    }
}
