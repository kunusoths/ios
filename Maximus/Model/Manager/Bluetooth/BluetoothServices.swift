//
//  BluetoothServices.swift
//  Maximus
//
//  Created by Sriram G on 03/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import CoreBluetooth
import UIKit

/// RepeatingTimer mimics the API of DispatchSourceTimer but in a way that prevents
/// crashes that occur from calling resume multiple times on a timer that is
/// already resumed (noted by https://github.com/SiftScience/sift-ios/issues/52
class RepeatingTimer {
    
    let timeInterval: TimeInterval
    
    init(timeInterval: TimeInterval) {
        self.timeInterval = timeInterval
    }
    
    private lazy var timer: DispatchSourceTimer = {
        let t = DispatchSource.makeTimerSource()
        t.scheduleRepeating(deadline: .now() + self.timeInterval, interval: self.timeInterval)
//        t.schedule(deadline: .now() + self.timeInterval, repeating: self.timeInterval)
        t.setEventHandler(handler: { [weak self] in
            self?.eventHandler?()
        })
        return t
    }()
    
    var eventHandler: (() -> Void)?
    
    private enum State {
        case suspended
        case resumed
    }
    
    private var state: State = .suspended
    
    deinit {
        timer.setEventHandler {}
        timer.cancel()
        /*
         If the timer is suspended, calling cancel without resuming
         triggers a crash. This is documented here https://forums.developer.apple.com/thread/15902
         */
        resume()
        eventHandler = nil
    }
    
    func resume() {
        if state == .resumed {
            return
        }
        state = .resumed
        timer.resume()
    }
    
    func suspend() {
        if state == .suspended {
            return
        }
        state = .suspended
        timer.suspend()
    }
}
protocol BLEServiceDelegate {
    
    func notifyUpdatedBLEStatus(status:BluetoothState)
}

class BluetoothServices:NSObject
{
    
    fileprivate var centralManager:CBCentralManager!
    var connectedPeripheral:CBPeripheral! = nil
    
    var dmBatteryService:CBService! = nil
    var dmBatteryCharacteristic:CBCharacteristic?
    
    var dmDisplayService:CBService! = nil
    var dmVersionChar:CBCharacteristic?
    var dmBuildIdCahr:CBCharacteristic?
    var dmHarwareVersionChar:CBCharacteristic?
    
    var TXCharacteristic:CBCharacteristic?
    var RXCharacteristic:CBCharacteristic?
    var otaCRCChar:CBCharacteristic?

    var otaService:CBService?
    
    var otaBleImgChar:CBCharacteristic?
    var otaNewImgChar:CBCharacteristic?
    
    var otaNewImgTUContentChar:CBCharacteristic?
    var otaExpectedImgTUSeqChar:CBCharacteristic?
    
    var otaModeChar:CBCharacteristic?
    var counter: Int = 0
    
    /////New BT Pairing State Characteristic
    var set_pairing_TX_CHAR:CBCharacteristic?
    var set_pairing_RX_CHAR:CBCharacteristic?
    
    var ble_encryptionChar : CBCharacteristic?
    
    var bleServiceDelegate:BLEServiceDelegate?
    var data = NSMutableData()
    var otaResQueue = DispatchQueue(label: "com.maximus.otarespqueue")
    
    
    //////*************************************
    fileprivate var BT_STATUS:BluetoothState = .NONE
    fileprivate var searchDeviceName:String?
    fileprivate var foundperipheralDATA:[String:Any] = [:]
    
    /////**************************************
    var timer: Timer? = nil
    var seconds = 0
//    var heartBeatTimer: Timer!
    var heartBeatTimer: RepeatingTimer!
    var sentHCount = 0
    var recievedHCount = 0
    let timeInterval: TimeInterval = 10.0
    ///Singleton instance of Class
    private static var privateShared : BluetoothServices?
    class func sharedInstance() -> BluetoothServices { // change class to final to prevent override
        guard let shared = privateShared else {
            privateShared = BluetoothServices()
            return privateShared!
        }
        return shared
    }
    
    ///Shared Instance Released here
    class func destroyInstance() {
        privateShared = nil
    }
    
    override init()
    {
        super.init()
    }
    
    /*!
     * @param namePeripheral
     * Name of peripheral that we search
     *
     * @discussion
     * Initial level of configuration of Central manager .
     *
     */
    
    func configureBLEDelegateService(namePeripheral: String){
        
        ///Check if its already paired
        if self.isAlreadyPaired() {
            self.BT_STATUS = .DISCONNECT_P
        }
//        }else{
//            self.BT_STATUS = .NONE
//        }
        
        self.searchDeviceName = namePeripheral///-----Device Name to search
        //Configure BluetoothService
        let centralQueue = DispatchQueue(label:BLE_Strings.centralQueueName, attributes: .concurrent)
        centralManager = CBCentralManager(delegate: self, queue: centralQueue, options:[CBCentralManagerOptionShowPowerAlertKey: true] )
    }
    
    ///* Get current Bluetooth connection status */
    public func getPairingSatus()-> BluetoothState {
        return self.BT_STATUS
    }
    
    
    //    ///*  method to Start scanning BLE */
    func startBLEscan(namePeripheral:String){
        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Start BLE scan")
        searchDeviceName = namePeripheral/////-------Device Name that we try to search for
        centralManager?.scanForPeripherals(
            withServices:[PERIPHERAL_DISCOVERY_SERVICE], options: [CBCentralManagerScanOptionAllowDuplicatesKey:false] )
    }
    
    ///* method to Stop scanning BLE */
    func stopBLEScan(){
        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Stop BLE scan")
        centralManager?.stopScan()
    }
    
    /** Call this when things either go wrong, or you're done with the connection.
     *  This cancels any subscriptions if there are any, or straight disconnects if not.
     *  (didUpdateNotificationStateForCharacteristic will cancel the connection if a subscription is involved)
     */
    fileprivate func cleanup() {
        // Don't do anything if we're not connected
        guard connectedPeripheral?.state == .connected else {
            return
        }
        
        // See if we are subscribed to a characteristic on the peripheral
        guard let services = connectedPeripheral?.services else {
            cancelPeripheralConnection()
            return
        }
        
        for service in services {
            guard let characteristics = service.characteristics else {
                continue
            }
            
            for characteristic in characteristics {
                if characteristic.uuid.isEqual(KB_NUCLEO_TX_CHAR_UUID) || characteristic.uuid.isEqual(KB_NUCLEO_RX_CHAR_UUID) && characteristic.isNotifying {
                    connectedPeripheral?.setNotifyValue(false, for: characteristic)
                    // And we're done.
                    return
                }
            }
        }
    }
    
    ///Check is already peripheral name saved
    fileprivate func isAlreadyPaired()->Bool{
        
        let strName:String = MyDefaults.getData(key: DefaultKeys.KEY_BLE_CONNECTED) as! String
        
        if strName.isBlank {
            return false
        }else{
            return true
        }
    }
    
    
    /*!
     * @param data
     * data of type NSData
     *
     * @discussion
     * Convert NSData to NString
     *
     */
    
    func dataToStringConversion(data:Data)->NSString{
        //Convert NSData to NSString
        let convertedStr:NSString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)!
        print(convertedStr)
        return convertedStr
        
    }
    
    
    func notifyOtaExpectedImgTUSeqChar() {
        if connectedPeripheral != nil{
            self.connectedPeripheral.setNotifyValue(true, for: otaExpectedImgTUSeqChar!)
        }
    }
    
    
    func removeSavedBLE(){
        self.BT_STATUS = .NONE ///-----Set the BLE Status here
        // self.connectedPeripheral = nil ///---clear connected periferal locally
        self.manageCachedPeripheralName(isSave: false)
    }
    
    fileprivate func manageCachedPeripheralName(isSave:Bool){
        if isSave {
            
            print("@BLE:Save BLE name == true")
            if let advName:String = foundperipheralDATA[BLE_Strings.advNameKey] as? String {
                MyDefaults.setData(value:advName , key: DefaultKeys.KEY_BLE_CONNECTED) ///----Save prifpheral name locally
            }else{
                print("@BLE:No Adv name found !!!")
            }
        }else{
            print("@BLE:Save BLE name == false")
            MyDefaults.removeForKey(key:  DefaultKeys.KEY_BLE_CONNECTED)///---Delete prifpheral name which was saved
            searchDeviceName = ""
        }
    }
    
    // MARK:Public methods
    func cancelPeripheralConnection()
    {
        // If we've got this far, we're connected, but we're not subscribed, so we just disconnect
        if self.connectedPeripheral != nil{
            self.centralManager.cancelPeripheralConnection(self.connectedPeripheral)
        }
    }
    
    // MARK: Read value at  Characteristic
    
    func readDMBarttery() {
        
        if self.getPairingSatus() == .CONNECTED_P{
            connectedPeripheral.readValue(for: dmBatteryCharacteristic!)
            connectedPeripheral.readValue(for: dmVersionChar!)
        }
    }
    
    // called when connection with trust is done
    func readCharacteristicValues() {
        
        if self.getPairingSatus() == .CONNECTED_P{
            connectedPeripheral.readValue(for: dmBatteryCharacteristic!)
            connectedPeripheral.readValue(for: dmVersionChar!)
            connectedPeripheral.readValue(for: dmHarwareVersionChar!)
            connectedPeripheral.readValue(for: dmBuildIdCahr!)
            connectedPeripheral.readValue(for: set_pairing_TX_CHAR!)
        }
        
    }
    
    // MARK: Write Data on Characteristic
    
    func writeToBleChar(infoData: Data, typeOfChar: BLEChar) -> BleWriteData {

        if let bleChar: CBCharacteristic = getAffectedCharacteristic(typeOfChar: typeOfChar, data: infoData) {
            
            if !infoData.isEmpty {
                if self.centralManager != nil{
                    if typeOfChar == BLEChar.nonceChar && bleChar != nil {
                        self.connectedPeripheral.writeValue(infoData as Data, for: self.ble_encryptionChar!, type: .withoutResponse)
                        return .bleWriteSuccess
                    } else if  self.BT_STATUS == .CONNECTED_P && bleChar != nil {
                        //  print("--------- BLE:Writing to MODE CHAR : Success --------")
                      //  FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Writing to BLE success. H count:\(sentHCount)")

                        self.connectedPeripheral.writeValue(infoData as Data, for: bleChar, type: .withoutResponse)
                        return .bleWriteSuccess
                    }else{
                        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Write to BLE failed. H count:\(sentHCount)")
                        readRSSIValue()
                        Log.i(tag: "BluetoothService", message: "Writing data to BLE failed")
                        return .bleWriteFailure
                    }
                } else {
                    FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Write to BLE failed. H count:\(sentHCount)")
                    readRSSIValue()
                    Log.i(tag: "BluetoothService", message: "Writing data to BLE failed \(self.getPairingSatus())")
                    return  .bleWriteFailure
                }
            }
        }
        return .bleWriteFailure
    }
    
    func getAffectedCharacteristic(typeOfChar: BLEChar, data: Data) -> CBCharacteristic? {
        
        switch typeOfChar {
            
        case .otaModeChar:
            if let _ = self.otaModeChar {
                return self.otaModeChar!
            }
        case .otaNewImgChar:
            if let _ = self.otaNewImgChar {
                return self.otaNewImgChar!
            }
        case .otaNewImgTUContentChar:
            if let _ = self.otaNewImgTUContentChar {
                return self.otaNewImgTUContentChar!
            }
        case .nonceChar:
            if let _ = self.ble_encryptionChar {
                return self.ble_encryptionChar!
            }
        case .RXCharacteristic:
            if let _ = self.RXCharacteristic {
                let array = Array(data)
                if array.count > 6 {
                    if array[1] == 1 && array[6] == 1  {
                            self.invalidateHeartBeat()
                    }
                }
                return self.RXCharacteristic!
            }
            
        case .otaCRCChar:
            if let _ = self.otaCRCChar {
                return self.otaCRCChar
            }
        }
        
        return nil
    }
    
}


// MARK:central manager delegates
extension BluetoothServices:CBCentralManagerDelegate {
    
    /*****************************************************************************
     * Function :   centralManagerDidUpdateState
     * Input    :   none
     * Output   :   None
     * Comment  :   CentralManager Delegate event represents the current state of a CBManager
     @constant CBManagerStateUnknown       State unknown, update imminent.
     *  @constant CBManagerStateResetting     The connection with the system service was momentarily lost, update imminent.
     *  @constant CBManagerStateUnsupported   The platform doesn't support the Bluetooth Low Energy Central/Client role.
     *  @constant CBManagerStateUnauthorized  The application is not authorized to use the Bluetooth Low Energy role.
     *  @constant CBManagerStatePoweredOff    Bluetooth is currently powered off.
     *  @constant CBManagerStatePoweredOn     Bluetooth is currently powered on and available to use.
     *
     ****************************************************************************/
    public func centralManagerDidUpdateState(_ central: CBCentralManager)
    {
        print("\(#line) \(#function)")
        /** Store the BLE Current state*/
        sentHCount = 0
        switch central.state {
            
        case .poweredOn:
            print("Current Bluetooth State:  PoweredOn")
            FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "BLE powered ON. H count:\(sentHCount)")
            readRSSIValue()
            if self.isAlreadyPaired(){
                self.BT_STATUS = .CONNECT_BONP_NC
            }else if self.BT_STATUS == .NONE {
                self.BT_STATUS = .NONE
            } else {
                self.BT_STATUS = .CONNECT_BONNP_NC
            }
            if self.BT_STATUS == .DISCONNECT_P || self.BT_STATUS == .NONE || self.BT_STATUS == .DISCONNECT_BOP || self.BT_STATUS == .CONNECT_BONP_NC || self.BT_STATUS == .CONNECT_BONNP_NC {
                if self.searchDeviceName != nil {
                    self.startBLEscan(namePeripheral: self.searchDeviceName!)
                }
            }
            self.bleServiceDelegate?.notifyUpdatedBLEStatus(status: self.BT_STATUS)
            break;
        case .poweredOff:
            FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "BLE powered OFF. H count:\(sentHCount)")
            readRSSIValue()
            BacklightScheduler.sharedInstance().invalidateTimer()
            if self.isAlreadyPaired(){
                self.BT_STATUS = .DISCONNECT_BOP
            }else{
                self.BT_STATUS = .DISCONNECT_BONP
                self.searchDeviceName = ""///-----Device Name to search
            }
            self.stopBLEScan()
            cleanup()
            BluetoothManager.sharedInstance().disconnectPeripheral()
            self.bleServiceDelegate?.notifyUpdatedBLEStatus(status: self.BT_STATUS)
            print("@BLE:Current Bluetooth State:  PoweredOff")
            break;
        case .resetting:
            print("@BLE:Current Bluetooth State:  Resetting")
            break;
        case .unauthorized:
              print("@BLE:Current Bluetooth State:  Unauthorized")
        case .unknown:
            print("@BLE:Current Bluetooth State:  Unknown")
            break;
        case .unsupported:
            print("@BLE:Current Bluetooth State:  Unsupported")
            break;
        }
    }
    
    //    public func centralManager(_ central: CBCentralManager, willRestoreState dict: [String : Any])
    //    {
    //        print(#function, dict)
    //        print("willRestore State Called")
    //    }
    
    /*****************************************************************************
     * Function :   didDiscover
     * Input    :   none
     * Output   :   None
     * Comment  :   CentralManager Delegate event called when peripheral is discovered
     *
     ****************************************************************************/
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber)
    {
        print("@BLE:Discovered \(String(describing: peripheral.name)) at \(RSSI)")
        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Discovered \(String(describing: peripheral.name)) at \(RSSI)")
        
        if let name = searchDeviceName {
            if let advertisingname:String = advertisementData[CBAdvertisementDataLocalNameKey] as? String{
                if name == advertisingname {
                    if !(peripheral.state == .connected) {
                        //central.stopScan()///-----Stop looking for device
                        foundperipheralDATA[BLE_Strings.peripheralKey] = peripheral
                        foundperipheralDATA[BLE_Strings.advNameKey] = advertisingname
                        connectedPeripheral = peripheral ///---Saved connected periferal locally
                        central.connect(peripheral, options: nil)///---connect request call
                    }
                }
            }
        }else{
            print("search device name is not available")
        }
        
    }
    
    /*****************************************************************************
     * Function :   didConnect
     * Input    :   none
     * Output   :   None
     * Comment  :   CentralManager delegate event called when peripheral is connected successfully
     *
     ****************************************************************************/
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral){
        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Did connect to \(peripheral.name)")

        print("@BLE:didConnect \(String(describing: peripheral.name)) : State\(peripheral.state.rawValue)")
        central.stopScan()///-----Stop looking for device
        ///******************* Setting Up BLE status ******************
        if self.isAlreadyPaired() {
            self.BT_STATUS = .CONNECTED_NP ///-----Set the BLE Status here
        }
        data.length = 0
        connectedPeripheral = peripheral ///---Saved connected periferal locally
        // Make sure we get the discovery callbacks
        peripheral.delegate = self
        peripheral.discoverServices([BATTERY_SERVICE_UUID, DEV_INFO_SERVICE_UUID, DISP_SERVICE_UUID, OTA_SERVICE])
    }
    
    /*****************************************************************************
     * Function :   didFailToConnect
     * Input    :   none
     * Output   :   None
     * Comment  :   CentralManager delegate event called when peripheral fail to make connection
     *
     ****************************************************************************/
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?)
    {
        peripheral.readRSSI()
        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Failed to connect to \(peripheral). (\(error!.localizedDescription))")
        Log.i(tag: String(describing: BluetoothServices.self), message:"Failed to connect to \(peripheral). (\(error!.localizedDescription))")
        cleanup()
    }
    
    /*****************************************************************************
     * Function :   didDisconnectPeripheral
     * Input    :   none
     * Output   :   None
     * Comment  :   CentralManager delegate event represent the disconnection of peripheral bond
     *
     ****************************************************************************/
    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?){
        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "didDisconnectPeripheral BLE disconnected")
        BacklightScheduler.sharedInstance().invalidateTimer()
        readRSSIValue()
        Log.i(tag: String(describing: BluetoothServices.self), message:"@BLE:Disconnected")
        DispatchQueue.main.async {
            self.invalidateHeartBeat()
        }
        guard error == nil else {
            FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "didDisconnectPeripheral Error disconnecting services: \(error!.localizedDescription)")
            Log.i(tag: String(describing: BluetoothServices.self), message: "@BLE:Error disconnecting services: \(error!.localizedDescription)")
            
            ///get the cached BLE identifier which is already connected
            if error?.localizedDescription == ErrorCodeBLE.unknown {
                
                if isAlreadyPaired() {
                    self.BT_STATUS = .DISCONNECT_P ///-----Set the BLE Status here
                    if self.searchDeviceName != nil {
                        self.startBLEscan(namePeripheral: self.searchDeviceName!)
                    }
                    //----Start Scan againg
                }else{
                    self.BT_STATUS = .DISCONNECT_NP ///-----Set the BLE Status here
                }
            }
            else if  error?.localizedDescription == ErrorCodeBLE.connectionTimedOut  {
                
                if isAlreadyPaired() {
                    self.BT_STATUS = .DISCONNECT_P ///-----Set the BLE Status here
                    // We're disconnected, so start scanning again
                    if self.searchDeviceName != nil {
                        self.startBLEscan(namePeripheral: self.searchDeviceName!)
                    }//----Start Scan againg
                }else{
                    self.BT_STATUS = .DISCONNECT_NP ///-----Set the BLE Status here
                }
            }
            
            self.bleServiceDelegate?.notifyUpdatedBLEStatus(status:self.BT_STATUS)
            return
        }
        
        //Disconected due to Explicit user action to disconnect connection
        //Check if diconnection is not due to BT button off
        //Disconnected due to BT powerOff
        
        if isAlreadyPaired(){
            self.BT_STATUS = .DISCONNECT_P  ///-----Set the BLE Status here
            if self.searchDeviceName != nil {
                self.startBLEscan(namePeripheral: self.searchDeviceName!)
            }//----Start Scan againg
        }else{
            self.BT_STATUS = .DISCONNECT_NP ///-----Set the BLE Status here
        }
        ////Notify status
        self.bleServiceDelegate?.notifyUpdatedBLEStatus(status:self.BT_STATUS)
        
        
    }
}

extension BluetoothServices: CBPeripheralDelegate {
    // MARK: Peripheral Delegates
    
    /** The Transfer Service was discovered
     */
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        guard error == nil else {
            FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Error discovering services")
            print("@BLE:Error discovering services: \(error!.localizedDescription)")
            cleanup()
            return
        }
        
        guard let services = peripheral.services else {
            return
        }
        
        // Discover the characteristic we want...
        
        /// Loop through the newly filled peripheral.services array, just in case there's more than one.
        for service in services {
            
            switch service.uuid {
            case DISP_SERVICE_UUID:
                peripheral.discoverCharacteristics([KB_NUCLEO_TX_CHAR_UUID,KB_NUCLEO_RX_CHAR_UUID,SET_PAIRING_STATE_RX_CHAR,SET_PAIRING_STATE_TX_CHAR, BLE_ENCRYPTION_NONCE_CHAR], for: service)
                break
                
            case BATTERY_SERVICE_UUID:
                peripheral.discoverCharacteristics([BATTERY_VALUE_CHAR_UUID], for: service)
                break
                
            case DEV_INFO_SERVICE_UUID:
                peripheral.discoverCharacteristics([DM_FIRMAWARE_VERSION_CHAR, DM_BUID_ID_CHAR, DM_HARDWARE_VERSION_CHAR], for: service)
                break
                
            case OTA_SERVICE:
                peripheral.discoverCharacteristics([OTA_BLE_IMG_CHAR,OTA_NEW_IMG_CHAR,OTA_NEW_IMG_TU_CONTENT_CHAR,OTA_EXPECTED_IMG_TU_SEQ_CHAR,OTA_MODE_SET_CHAR, OTA_CRC_CHAR], for:service)
                break
                
            default :
                break
            }
            
        }
    }
    
    /** The Transfer characteristic was discovered.
     *  Once this has been found, we want to subscribe to it, which lets the peripheral know we want the data it contains
     */
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        // Deal with errors (if any)
        // print("Disconnected Periperal:\(peripheral)")
        guard error == nil else {
            print("@BLE:Error discovering services: \(error!.localizedDescription)")
            FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Error discovering characteristic")
            cleanup()
            return
        }
        
        guard let characteristics = service.characteristics else {
            return
        }
        
        // Again, we loop through the array, just in case.
        for characteristic in characteristics {
            // And check if it's the right one
            
            switch characteristic.uuid {
                
            case SET_PAIRING_STATE_RX_CHAR:
                self.set_pairing_RX_CHAR = characteristic
                peripheral.setNotifyValue(true, for: characteristic)
                seconds = 30
                timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
                timer?.fire()
                print("@BLE: Start timer of 30 sec")
                sentHCount = 0
                FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Heart beat counter:\(sentHCount)")
                Log.i(tag: String(describing: BluetoothServices.self), message: "@BLE:Discovered RX")
                FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Discovered RX")

                ///*************** Check status and Notify DM *****************
                if self.isAlreadyPaired() {
                    Thread.sleep(forTimeInterval: 2)
                    if let characteristic_tx = self.set_pairing_TX_CHAR {
                        self.notifyPairingState(pState: DM_PAIRSTATE.pair_P, characteristic:characteristic_tx,peripheral:peripheral)
                        Log.i(tag: String(describing: BluetoothServices.self), message: "@BLE:send P")
                        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Send P")

                    }
                }else{
                    if let characteristic_tx = self.set_pairing_TX_CHAR {
                        self.notifyPairingState(pState: DM_PAIRSTATE.notPair_N,characteristic:characteristic_tx,peripheral:peripheral)
                        Log.i(tag: String(describing: BluetoothServices.self), message: "@BLE:send N")
                        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Send N")

                    }
                }
                
                break
                
            case BLE_ENCRYPTION_NONCE_CHAR:
                self.ble_encryptionChar = characteristic
                peripheral.setNotifyValue(true, for: self.ble_encryptionChar!)
                
                break
                
            case SET_PAIRING_STATE_TX_CHAR:
                self.set_pairing_TX_CHAR = characteristic
                peripheral.setNotifyValue(true, for: characteristic)
                Log.i(tag: String(describing: BluetoothServices.self), message: "@BLE:Discovered TX")
                FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Discovered TX")
                break
                
                
            case KB_NUCLEO_TX_CHAR_UUID:
                self.TXCharacteristic = characteristic
                peripheral.setNotifyValue(true, for: characteristic)
                break
                
            case KB_NUCLEO_RX_CHAR_UUID:
                self.RXCharacteristic = characteristic;
                break
                
            case BATTERY_VALUE_CHAR_UUID:
                self.dmBatteryCharacteristic = characteristic
                peripheral.setNotifyValue(true, for: characteristic)
                break
                
            case DM_BUID_ID_CHAR:
                self.dmBuildIdCahr = characteristic
                break
                
            case DM_HARDWARE_VERSION_CHAR:
                self.dmHarwareVersionChar = characteristic
                break
                
            case DM_FIRMAWARE_VERSION_CHAR:
                self.dmVersionChar = characteristic
                break
                
            case OTA_BLE_IMG_CHAR:
                self.otaBleImgChar = characteristic
                peripheral.readValue(for: characteristic)
                break
                
            case OTA_NEW_IMG_CHAR:
                self.otaNewImgChar = characteristic
                break
                
            case OTA_NEW_IMG_TU_CONTENT_CHAR:
                self.otaNewImgTUContentChar = characteristic
                break
                
            case OTA_EXPECTED_IMG_TU_SEQ_CHAR:
                self.otaExpectedImgTUSeqChar = characteristic
                break
                
            case OTA_MODE_SET_CHAR:
                print("OTA mode char")
                self.otaModeChar = characteristic
                break
            
            case OTA_CRC_CHAR:
                print("OTA: Got OTA CRC characteristic")
                self.otaCRCChar = characteristic
                break
                
            default:
                break
            }
            
        }
    }
    /*****************************************************************************
     * Function :   notifyPairingState
     * Input    :   String & CBCharacteristic
     * Output   :   None
     * Comment  :  *********
     *
     ****************************************************************************/
    private func notifyPairingState(pState:String ,characteristic:CBCharacteristic,peripheral: CBPeripheral ){
        let pairingStateData:Data = pState.data(using: .utf8)!
        peripheral.writeValue(pairingStateData as Data, for: characteristic, type: .withResponse)
    }
    
    func peripheralDidUpdateName(_ peripheral: CBPeripheral) {
        
    }
    
    /*****************************************************************************
     * Function :   didUpdateValueFor
     * Input    :   none
     * Output   :   Getting Updates on characteristics
     * Comment  :   This callback lets us know more data has arrived via notification on the characteristic
     *
     ****************************************************************************/
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        if (error != nil) {
            FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Error reading characteristic.")
            print("@BLE:Error reading characteristics: \(characteristic.uuid)",error?.localizedDescription ?? "")
            return;
        }
        
        
        if (characteristic.value != nil) {
            
            switch characteristic.uuid {
            case KB_NUCLEO_TX_CHAR_UUID:
                if let data = characteristic.value {
                    CPWrapper.sharedInstance().cp_receive_payload(data)
                }
                break
                
            case OTA_BLE_IMG_CHAR:
                if let data = characteristic.value{
                    BluetoothManager.sharedInstance().setCurrentImageInfo(info: Array(data))
                }
                break
                
            case BATTERY_VALUE_CHAR_UUID:
                if let data = characteristic.value {
                    print("Setting DM Battery : \(data.hashValue)")
                    BluetoothManager.sharedInstance().setDmBatteryLevel(value: data.hashValue)
                }
                break
                
            case DM_BUID_ID_CHAR:
                if let data = characteristic.value {
                    let dataArray:[UInt8] = Array(data)
                    
                    if dataArray.count > 1 {
                        let result = (UInt16(dataArray[1]) << 8) + UInt16(dataArray[0])
                        print("DM Build id : \(result)")
                        AppDetailsHandler.sharedInstance.isValueChanged(changedValue: String(result), infoToChange: DMInfo.BuildNo)
                        
                    } else {
                        AppDetailsHandler.sharedInstance.isValueChanged(changedValue: "1", infoToChange: DMInfo.BuildNo)
                    }
                    self.counter = self.counter + 1
                }
                if self.counter == 3 {
                    if !AppDetailsHandler.sharedInstance.getSyncFlagStatus() {
                        let parameters = AppDetailsHandler.sharedInstance.getDmDetailsObj()
                        AppDetailsHandler.sharedInstance.sendDMDetails(details: parameters)
                    }
                }
                break
                
            case DM_HARDWARE_VERSION_CHAR:
                if let data = characteristic.value {
                    _ = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                    print("DM Hardware Version : \(data.hashValue)")
                }
                break
                
            case DM_FIRMAWARE_VERSION_CHAR:
                if let data = characteristic.value {
                    print("DM Version data \(data.hashValue)")
                    let dataArray:[UInt8] = Array(data)
                    
                    
                    if dataArray.count > 1 {
                        let result = (UInt16(dataArray[1]) << 8) + UInt16(dataArray[0])
                        let majorNumber = (0xfc00 & (UInt16(result))) >> 10 // + (0x000003e0 & (UInt32(result) >> 5))
                        let minorNumber = (0x03e0 & (UInt16(result))) >> 5
                        let patchNumber = (0x001f & UInt16(result))
                        
                        let dmFwVersion = "\(majorNumber)\(minorNumber)\(patchNumber)"
                        print("DM Firmaware version : \(dmFwVersion)")
                        MyDefaults.setInt(value: Int(dmFwVersion)!, key:DefaultKeys.KEY_DM_VERSION)
                        AppDetailsHandler.sharedInstance.isValueChanged(changedValue: dmFwVersion, infoToChange: DMInfo.FirmwareVersion)
                        
                    } else {
                        MyDefaults.setInt(value: 1, key: DefaultKeys.KEY_DM_VERSION)
                        AppDetailsHandler.sharedInstance.isValueChanged(changedValue: "1", infoToChange: DMInfo.FirmwareVersion)
                    }
                    self.counter = self.counter + 1
                }
                break
                
            case SET_PAIRING_STATE_RX_CHAR:
                
                if let data = characteristic.value {
                    print("@BLE:Set paring data \(data.hashValue)")
                    let strDATA = String(self.dataToStringConversion(data: data))
                    print("@BLE:strData\(strDATA)")
                    switch strDATA {
                    case DM_PAIRSTATE.error_E:
                        print("***E")
                        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Received E.")
                        ///******************* Setting Up BLE status ******************
                        self.BT_STATUS = .DISCONNECT_E ///-----Set the BLE Status here
                        invalidateTimer()
                        ////Notify status
                        self.bleServiceDelegate?.notifyUpdatedBLEStatus(status:self.BT_STATUS)
                        break
                    case DM_PAIRSTATE.ignore_I:
                        print("***I")
                        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Received I.")
                        invalidateTimer()
                        ////Notify status
                        self.bleServiceDelegate?.notifyUpdatedBLEStatus(status:self.BT_STATUS)
                        break
                    case DM_PAIRSTATE.notPair_N:
                        ///******************* Setting Up BLE status ******************
                        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Received N.")
                        print("***N")
                        if self.isAlreadyPaired() {
                            //disconnect update state
                            DispatchQueue.main.async {
                                let rootView = UIApplication.shared.keyWindow?.rootViewController
                                rootView?.view.makeToast("Please unpair your device from ‘My Support’ and try pairing again.")
                            }
                            self.BT_STATUS = .DISCONNECT_N ///-----Set the BLE Status here
                            BluetoothManager.sharedInstance().disconnectPeripheral()
                            timer?.invalidate()
                        } else {
                            self.BT_STATUS = .CONNECT_BOFFNP
                        }
                        ////Notify status
                        self.bleServiceDelegate?.notifyUpdatedBLEStatus(status:self.BT_STATUS)
                        break
                    case DM_PAIRSTATE.pair_P:
                        ///******************* Setting Up BLE status ******************
                        // for reconnect send nonce
                        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Received P.")
                        print("@BLE:***P")
                        self.BT_STATUS = .CONNECTED_P
                        let nonce  = MyDefaults.getDataType(key: "NONCE")
                        print("NONCE send: \(nonce)")
                        BluetoothManager.sharedInstance().sendToBle(data: nonce, char: BLEChar.nonceChar)
                        ////Notify status
                        self.bleServiceDelegate?.notifyUpdatedBLEStatus(status:self.BT_STATUS)
                        break
                    case DM_PAIRSTATE.other_O:
                        print("@BLE:***O")
                        ///******************* Setting Up BLE status ******************
                        self.BT_STATUS = .DISCONNECT_O ///-----Set the BLE Status here
                        //if paired already with someone else and invalid passkey show some UI
                        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Received O.")
                        invalidateTimer()
                        ////Notify status
                        self.bleServiceDelegate?.notifyUpdatedBLEStatus(status:self.BT_STATUS)
                        break
                    case DM_PAIRSTATE.nonce_S:
                        print("@BLE:***S")
                        self.BT_STATUS = .CONNECTED_P ///-----Set the BLE Status here
                        self.manageCachedPeripheralName(isSave: true)
                        self.connectedPeripheral = peripheral
                        self.connectedToDM()
                        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Connected")
                        ////Notify status
                        self.bleServiceDelegate?.notifyUpdatedBLEStatus(status:self.BT_STATUS)
                        break
                    case DM_PAIRSTATE.heartBeat_H:
                        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Received heart beat.")
                        print("@BLE:***H")
                        endHeartBeat()
                        self.BT_STATUS = .CONNECTED_P ///-----Set the BLE Status here
                        break
                    default:
                        break
                    }
                    
                    
                }
                break
                
            case SET_PAIRING_STATE_TX_CHAR:
                if let data = characteristic.value {
                    print("get pairing data \(data.hashValue)")
                }
                break
                
            case BLE_ENCRYPTION_NONCE_CHAR:
                if let data = characteristic.value {
                    print("get encryption data \(data.hashValue)")
                }
                break
                
            case OTA_MODE_SET_CHAR:
                if let data = characteristic.value {
                    print("Got value at OTA MODE CHAR: \(data.hashValue)")
                    BluetoothManager.sharedInstance().setOTAMode(data: data)
                    otaResQueue.async {
                        OTAWrapper.sharedInstace().otaGetModeStatusCB(data)
                    }
                }
                break
                
            case OTA_EXPECTED_IMG_TU_SEQ_CHAR:
                if let data = characteristic.value{
                    OTAWrapper.sharedInstace().otaWriteResponseCb(data)
                }
                break
                
            default:
                break
            }
            
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "RSSI vaue \(RSSI)")
    }
    //Called When DM is paired and Connected
    func connectedToDM(){
        invalidateTimer()
        self.connectedPeripheral.setNotifyValue(true, for: self.otaModeChar!)
        self.connectedPeripheral.readValue(for: self.otaModeChar!)
        self.readCharacteristicValues()
        //Thread.sleep(forTimeInterval: 5)
        readRSSIValue()
        BacklightScheduler.sharedInstance().calculateTimerInterval()
        startHeatbeatTimer()
    }
    
    func readRSSIValue(){
        if connectedPeripheral != nil {
            connectedPeripheral.readRSSI()
        }
    }
    
    func startHeatbeatTimer() {
        print("@BLE: Start heart beat timer")
        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Start heart beat time. H count: \(sentHCount)")
        readRSSIValue()
        heartBeatTimer = RepeatingTimer(timeInterval: 5.0)
        heartBeatTimer.eventHandler = {
            print("Timer Fired")
            if self.sentHCount >= 5 {
                FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Disconnect due to failure of heart beat, H count:\(self.sentHCount)")
                self.disconnectHeartBeat()
            } else {
                if let characteristic_tx = self.set_pairing_TX_CHAR {
                    self.notifyPairingState(pState: DM_PAIRSTATE.heartBeat_H, characteristic: characteristic_tx,peripheral:self.connectedPeripheral)
                    Log.i(tag: String(describing: BluetoothServices.self), message: "@BLE:send H")
                    FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Send H, H count:\(self.sentHCount)")
                    self.readRSSIValue()
                    self.sentHCount = self.sentHCount + 1
                }
            }
        }
        heartBeatTimer.resume()
//        DispatchQueue.main.async {
//            if self.heartBeatTimer == nil {
//                print("@BLE: Send H called")
//                self.heartBeatTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.updateHeartBeatTimer), userInfo: nil, repeats: true)
//            }
//        }
    }
    
    func invalidateHeartBeat() {
        print("@BLE: End heart beat timer\(sentHCount)")
        readRSSIValue()
        sentHCount = 0
        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "End heart beat time. H count: \(sentHCount)")
//        if heartBeatTimer != nil {
//            heartBeatTimer?.invalidate()
//            heartBeatTimer = nil
//        }
        if heartBeatTimer != nil {
            heartBeatTimer.suspend()
        }
    }
    
    @objc func updateHeartBeatTimer() {
        print("@BLE: updateHeartBeatTimer:\(sentHCount)")
        if sentHCount >= 5 {
            FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Disconnect due to failure of heart beat, H count:\(sentHCount)")
            disconnectHeartBeat()
        } else {
            if let characteristic_tx = self.set_pairing_TX_CHAR {
                self.notifyPairingState(pState: DM_PAIRSTATE.heartBeat_H, characteristic: characteristic_tx,peripheral:self.connectedPeripheral)
                Log.i(tag: String(describing: BluetoothServices.self), message: "@BLE:send H")
                FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Send H, H count:\(sentHCount)")
                readRSSIValue()
                sentHCount = sentHCount + 1
             }
        }
    }
    
    func endHeartBeat() {
        sentHCount = 0
    }
    
    func disconnectHeartBeat() {
        print("@BLE: Disconnect heart beat\(sentHCount)")
        readRSSIValue()
        if self.isAlreadyPaired(){
            self.BT_STATUS = .CONNECTED_NP
        }else{
            self.BT_STATUS = .DISCONNECT_BONP
        }
        DispatchQueue.main.async {
            self.invalidateHeartBeat()
        }
        ////Notify status
        self.bleServiceDelegate?.notifyUpdatedBLEStatus(status:self.BT_STATUS)
      //  BluetoothManager.sharedInstance().disconnectPeripheral()
    }
    
    func invalidateTimer() {
        print("@BLE: Ended timer of 30 sec")
        timer?.invalidate()
        timer = nil
    }
    
    func updateTimer() {
        if seconds <= 0 {
            disconnect()
            print("@BLE: start scanning again after 30 sec bonding error")
            if self.searchDeviceName != nil {
                self.startBLEscan(namePeripheral: self.searchDeviceName!)
            }
        }
        seconds = seconds - 1
    }
    
    func disconnect() {
        print("@BLE: Ended timer of 30 sec")
        timer?.invalidate()
        timer = nil
        BluetoothManager.sharedInstance().unpaireBLE()
    }
    
    /*****************************************************************************
     * Function :   peripheraldidUpdateNotificationStateForcharacteristic
     * Input    :   none
     * Output   :   subscribe/unsubscribe characteristic
     * Comment  :   The peripheral letting us know whether our subscribe/unsubscribe happened or not
     *
     ****************************************************************************/
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        
        print("Peripheral == \(peripheral) Characteristics == \(characteristic)  Error == \(error)")
        
        
    }
    func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]) {
        //
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: Error?) {
        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "Error discovering services")

        guard error == nil else {
            print("Error discovering services: error\(String(describing: error?.localizedDescription))")
            return
        }
        print("Message sent")
    }
    
}



