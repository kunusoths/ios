//
//  BluetoothConfiguration.swift
//  Maximus
//
//  Created by Admin on 22/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//
//<CBCharacteristic: 0x1702ba220, UUID = D3E43E24-BBA8-11E7-8F1A-0800200C9A66, properties = 0x12, value = <50>, notifying = YES>

import Foundation
import CoreBluetooth


let KB_NUCLEO_TX_CHAR_UUID       =  CBUUID(string: "1bc5d5a5-0200-6585-e611-f326401fb601")
let KB_NUCLEO_RX_CHAR_UUID       =  CBUUID(string: "1bc5d5a5-0200-7e9e-e611-b727a0d29940")
let KB_TX_CHAR_CCD_ID            =  CBUUID(string: "00002902-0000-1000-8000-00805f9b34fb")
let DISP_SERVICE_UUID            =  CBUUID(string: "1bc5d5a5-0200-c188-e611-f12680c921da")
let DEV_INFO_SERVICE_UUID        =  CBUUID(string: "0000180a-0000-1000-8000-00805f9b34fb")

let DM_BUID_ID_CHAR              =  CBUUID(string: "00002a25-0000-1000-8000-00805f9b34fb")
let DM_FIRMAWARE_VERSION_CHAR   =   CBUUID(string: "00002a26-0000-1000-8000-00805f9b34fb")
let DM_HARDWARE_VERSION_CHAR     =  CBUUID(string: "00002a27-0000-1000-8000-00805f9b34fb")

let BATTERY_SERVICE_UUID         =  CBUUID(string: "0000180f-0000-1000-8000-00805f9b34fb")
let BATTERY_VALUE_CHAR_UUID      =  CBUUID(string: "00002a19-0000-1000-8000-00805f9b34fb")

let OTA_BLE_IMG_CHAR             =  CBUUID(string: "122e8cc0-8508-11e3-baa7-0800200c9a66")
let OTA_NEW_IMG_CHAR             =  CBUUID(string: "210f99f0-8508-11e3-baa7-0800200c9a66")
let OTA_NEW_IMG_TU_CONTENT_CHAR  =  CBUUID(string: "2691aa80-8508-11e3-baa7-0800200c9a66")
let OTA_EXPECTED_IMG_TU_SEQ_CHAR =  CBUUID(string: "2bdc5760-8508-11e3-baa7-0800200c9a66")

let OTA_MODE_SET_CHAR            =  CBUUID(string: "b4fb1f10-8508-11e3-baa7-0800200c9a66")
let OTA_SERVICE                  =  CBUUID(string: "8a97f7c0-8506-11e3-baa7-0800200c9a66")

let PERIPHERAL_DISCOVERY_SERVICE =  CBUUID(string: "a5d5c51b")

let SET_PAIRING_STATE_TX_CHAR    =  CBUUID(string: "d3e43e23-bba8-11e7-8f1a-0800200c9a66")
let SET_PAIRING_STATE_RX_CHAR    =  CBUUID(string: "d3e43e24-bba8-11e7-8f1a-0800200c9a66")

let BLE_ENCRYPTION_NONCE_CHAR    =  CBUUID(string: "d3e43e22-bba8-11e7-8f1a-0800200c9a66")
let OTA_CRC_CHAR                 = CBUUID(string: "4524a090-3320-11e8-b467-0ed5f89f718b")

struct CURRENT_DM_MODE {
    static let DM_OTA_MODE = "OTA_MODE"
    static let DM_DEFAULT_MODE = "DEFAULT"
    static let DM_UNKNOWN_MODE = "UNKNOWN"
}


///* Explicitly handle States of BLUETOOTH States *///
enum BluetoothState : Int {
    case DISCONNECT_NP = 0  ///----Default state of BT connection
    case DISCONNECT_P       ///----BT Disconnected but app has pairing information
    case DISCONNECT_E       ///----BT Disconnected due to some Error on Hardware Side
    case DISCONNECT_N       ///----BT Disconnected due to Hardware do not have pairing Info
    case DISCONNECT_O       ///----BT Disconnected due to already connected with some Device
    case CONNECTED_P        ///----BT Connected and Authenticated on Hardware side as well
    case CONNECTED_NP       ///----BT Connected but not Authenticated on Hardware Side
    case DISCONNECT_BOP     ///----BT Disconnect due to BT off and app has pairing info
    case DISCONNECT_BONP    ///----BT Disconnect due to BT off and app does not have pairing info
    
    case DISCONNECT_BOFFP ///----BT off app is having paring info, connected
    case DISCONNECT_BOFFNP ///--- BT off app is not having paring info, connected
    case DISCONNECT_BOFFP_NC ///----BT off app is having paring info, not connected
    case DISCONNECT_BOFFNP_NC ///--- BT off app is not having paring info, not  connected
    case CONNECT_BOFFP ///----BT on app is having paring info, connected
    case CONNECT_BOFFNP ///--- BT on app is not having paring info, connected
    case CONNECT_BONP_NC ///----BT on app is having paring info, not connected
    case CONNECT_BONNP_NC ///--- BT on app is not having paring info, not  connected
    
    case NONE               ///----NO state
}



struct ErrorCodeBLE {
    static let unknown:String              = "Unknown error."
    static let connectionTimedOut:String   = "The connection has timed out unexpectedly."
}

struct ConstantBLEName {
    static let Name_DM:String              = "MaximusPro-"
}

struct BLE_Strings{

    static let advNameKey:String    = "Name"
    static let peripheralKey:String = "objPerpheral"
    static let centralQueueName:String     = "com.kpit.maximus.BLE_CentralQueue"

}

struct DM_PAIRSTATE{
    
    static let pair_P   :String     = "P"   ///-------Paired
    static let error_E  :String     = "E"   ///-------Error
    static let notPair_N:String     = "N"   ///-------Not have pair info
    static let ignore_I :String     = "I"   ///-------Ignore case
    static let other_O  :String     = "O"   ///-------Paired with other
    static let nonce_S : String = "S" ///------Nonce validated successfully
    static let heartBeat_H : String = "H" ///------Heart beat
}



