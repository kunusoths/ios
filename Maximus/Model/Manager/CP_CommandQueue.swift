//
//  CP_CommandQueue.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 12/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation

class CP_CommandQueue: NSObject {
    
    let commndQueue = OperationQueue() //DispatchQueue(label: "Comm_proto_queue", qos: .userInitiated)
    // let delay = 0.5 // 500 ms
    let serialQueue = DispatchQueue(label: "com.maximuspro.commandqueue")
    let backgroundQueue = DispatchQueue.global(qos: .background)
    
    override init() {
        
    }
    
    func addCommandsToQueue(block: Operation)  {
        let otaData:[String:Data] = BluetoothManager.sharedInstance().getOTAMode()
        if otaData.has(CURRENT_DM_MODE.DM_OTA_MODE) {
            //dont fire CP commands
            print("OTA MODE SET DISABLED CP")
        }else{
            print("Normal MODE SET Enabled CP")
            backgroundQueue.async{
                self.serialQueue.sync {
                    
                    self.commndQueue.addOperation(block)
                    Thread.sleep(forTimeInterval: 0.1)
                }
            }
        }
    }
    
    func removeAllOperations() {
        self.commndQueue.cancelAllOperations()
    }
}
