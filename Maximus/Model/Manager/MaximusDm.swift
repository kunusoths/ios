//
//  MaximusDm.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 01/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit


enum DM_SHOWN_STATE : Int {
    case NONE = 0
    case TBT
    case HEADING
    case DEVIATION
    case POI_REACHING
    case DESTINATION_REACHING
    case WEAK_GPS
    case LONG_STEP
}

enum WINDING_STATE : Int {
    case OTHER = 0
    case WINDING_STARTED
    case WINDING_END
}

enum DM_TBT_STATE : Int {
    case STOP = 0
    case START
    case NONE
}

protocol DMBatteryStateChangeProtocol:class{
    func onDMChargingStateChange(state: CP_DmChargingTypeDef)
}

class MaximusDM: NSObject{
    
    static let sharedInstance = MaximusDM()
    let sinkManager = SinkManager.sharedInstance
    
    let commandQueue = CP_CommandQueue()
    
    weak var dmChargingDelegate:DMBatteryStateChangeProtocol?
    
    ///Manuever Current & Next Variables
    fileprivate var prevCurrentIcon:Int = CP_CONSTANTS.ICON_DEFAULTVALUE
    fileprivate var prevNextIcon:Int = CP_CONSTANTS.ICON_DEFAULTVALUE
    
    ///State TBT & DM
    fileprivate var stateTBT:DM_TBT_STATE   = .STOP
    fileprivate var stateDM:DM_SHOWN_STATE  = .NONE
    fileprivate var windingState:WINDING_STATE = .OTHER
    
    ///Manuever Exit Number current & Next Variables
    fileprivate var oldexitNoCurrent       = 0
    fileprivate var oldexitNoNext          = 0
    
    fileprivate var oldobjRoundAboutCurent:RoundAboutExit = RoundAboutExit()
    fileprivate var oldobjRoundAboutNext:RoundAboutExit = RoundAboutExit()
    
    
    ///Leg Progress ETA Variables
    var mPrevLegETA:Int             = 0
    var mPrevRemainingLegDist:Int   = 0
    var travelTimeCounter:Int       = 60 //To send ETA for first time
    
    var isDMCharging:Bool?
    ///FLAG Sink Data
    //MARK:- Private Methods
    private override init() {
        super.init()
        CPWrapper.sharedInstance().delegate = self
        self.changeRegisterationForCommunicationProtocolUpdates(register: true)
        
    }
    
    deinit {
        self.changeRegisterationForCommunicationProtocolUpdates(register: false)
    }
    
    
    func changeRegisterationForCommunicationProtocolUpdates(register:Bool){
        
        if(register){
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_LEG_STARTED, selectorName: catchNotificationOnLegStarted)
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_LEG_PROGRESS_COMPLETED, selectorName: catchNotificationOnLegCompleted)
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_LEG_IN_PROGRESS, selectorName: catchNotificationOnLegInProgress)
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_SKIPPED, selectorName: catchNotificationOnLegInProgress)

            
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_STEP_STARTED, selectorName: catchNotificationOnStepStarted)
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_STEP_IN_PROGRESS, selectorName: catchNotificationOnStepIsInProgress)
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_STEP_SKIPPED, selectorName: catchNotificationOnStepSkipped)

            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_STEP_COMPLETED, selectorName: catchNotificationOnStepCompleted)
            
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_CURRENT_TURN, selectorName: catchCurrentTurn)
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_RIDE_ROUTE_FINISHED, selectorName: catchNotificationRideRouteFinished)
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_JOURNEY_STARTED, selectorName: catchJourneyStatredNotification)
            
            sinkManager.addObserver(observer: self, notificationKey: SinkManager.sharedInstance.SINK_NOTIFICATION_BLUETOOTH_STATE_UPDATE, selectorName: eventBLEStatusUpdate)
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_CURRENT_MANUEVER, selectorName: sendCurrentManueverIconToDM)
            
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_NEXT_MANUEVER, selectorName: sendNextManueverIconToDM)
            
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_REMAINING_DISTANCE, selectorName: catchRemaningDistance)
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_DEVIATION_DETECTED, selectorName: catchDeviationEvent)
            
            
            
        }else{
            
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_LEG_STARTED)
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_LEG_IN_PROGRESS)
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_LEG_PROGRESS_COMPLETED)
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_STEP_STARTED)
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_STEP_IN_PROGRESS)
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_STEP_COMPLETED)
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_STEP_SKIPPED)

            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_RIDE_ROUTE_FINISHED)
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_CURRENT_TURN)
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_JOURNEY_STARTED)
            sinkManager.removeObserver(observer: self, notificationKey: SinkManager.sharedInstance.SINK_NOTIFICATION_BLUETOOTH_STATE_UPDATE)
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_CURRENT_MANUEVER)
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_NEXT_MANUEVER)
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_DEVIATION_DETECTED)
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_REMAINING_DISTANCE)
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_SKIPPED)

        }
    }
    
    func setBatteryChargingStatus(status: Bool){
        self.isDMCharging = status
    }
    
    func getDMBatteryChargingStatus() -> Bool {
        return self.isDMCharging ?? false
    }
    
    
    // Communication Protocol Methods
    
    func startDMFWOTA() {
        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_firmwareUpdateStart()
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
        
    }
    
    // Call this method once BT is connected and paired
    func sendDateAndTimeToDM(){
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Sending Date and Time ----------")
        let todaysDate = Date()
        let calender = Calendar.current
        
        let components = calender.dateComponents([.hour,.minute], from: todaysDate)
        
        print(components)
        let day = calender.component(.day, from: todaysDate)
        let month = calender.component(.month, from: todaysDate)
        let year = calender.component(.year, from: todaysDate)
        let weekDay = calender.component(.weekday, from:todaysDate)
        
        var hours = calender.component(.hour, from: todaysDate)
        let minutes = calender.component(.minute, from: todaysDate)
        let seconds  = calender.component(.second, from: todaysDate)
        
        let formatString = DateFormatter.dateFormat(fromTemplate: "j", options: Constants.ZERO_VALUE, locale: Locale.current)! as String
        print(formatString)
        let hasAMPM = formatString.contains("a")
        
        var timeFormat:Int = 1
        
        var ampm = Constants.ZERO_VALUE
        
        
        if hasAMPM {
            //12 hrs
            timeFormat = Constants.ZERO_VALUE
            
            if hours >= 12{
                //pm
                ampm = 1
            }else{
                //am
                ampm = Constants.ZERO_VALUE
            }
            
            if hours > 12{
                hours = hours - 12
            }
        }else{
            //24 HRS
            timeFormat = 1
        }
        
        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_set_date_time(Int32(hours), andMIN: Int32(minutes), andSS: Int32(seconds), andDD: Int32(day), andMM: Int32(month), andYY: Int32(year - 2000), andAmPm: Int32(ampm), andTmeFormat: Int32(timeFormat), andWeekDay: Int32(weekDay))
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
        
        self.sendBackLightControlModeToDM()
    }
    
    
    
    
    /*****************************************************************************
     * Function :   updateJourneyEta
     * Input    :   Duration
     * Output   :
     * Comment  :   Send Updated ETA duration to DM
     *
     ****************************************************************************/
    
    func updateJourneyEta(duration: Int) {
        
        var hours = Constants.ZERO_VALUE
        
        if (duration > CP_CONSTANTS.HOUR_IN_SECONDS) {
            hours = duration/CP_CONSTANTS.HOUR_IN_SECONDS
        }else{
            hours = Constants.ZERO_VALUE
        }
        
        var minutes = Constants.ZERO_VALUE//(duration % 3600)/CP_CONSTANTS.SECOND_IN_MINUTE
        if (duration > CP_CONSTANTS.HOUR_IN_SECONDS){
            minutes = (duration % CP_CONSTANTS.HOUR_IN_SECONDS)/CP_CONSTANTS.SECONDS_IN_MINUTE
        }else{
            minutes = duration/CP_CONSTANTS.SECONDS_IN_MINUTE
        }
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Sending Journey ETA \(hours)h \(minutes)m")
        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_update_full_journey_eta(LIVE_FLAG.TWO, andhour: Int32(hours), andminute: Int32(minutes))
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
    }
    
    func updateCurrentManueverIcon(iconId: Int ,isSinkData:Bool)  {
        
        if (iconId != prevCurrentIcon || isSinkData == true){
            if (self.windingState != .WINDING_STARTED) {
                print("@MaximusDM :Winding state = \(self.windingState.rawValue) icon : \(iconId)")
               // Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Winding state = \(self.windingState.rawValue) icon : \(iconId)"")
                prevCurrentIcon = iconId
                let operationBlock = BlockOperation{
                    CPWrapper.sharedInstance().cp_update_current_manuever_icon(LIVE_FLAG.TWO, andiconID: Int32(iconId))
                }
                
                commandQueue.addCommandsToQueue(block: operationBlock)
            }
        }
    }
    
    func updateNextManueverIcon(iconId: Int, isSinkData:Bool)  {
        
        if (iconId != prevNextIcon || isSinkData == true) {
            prevNextIcon = iconId
            Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Next Manuever icon \(iconId)")
            let operationBlock = BlockOperation{
                CPWrapper.sharedInstance().cp_update_next_manuever_icon(LIVE_FLAG.TWO, andiconID: Int32(iconId))
            }
            commandQueue.addCommandsToQueue(block: operationBlock)
        }
        
    }
    
    func updateCurrentManueverDistance(distance: Int){
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Sending Current ManueverDistance: \(distance)")
        
        let (convertDistance,unit, scale) = self.getDistanceAndUnit(distance: Int64(distance))
        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_update_current_manuever_distance_unit(LIVE_FLAG.TWO, anddistance: Int32(convertDistance), andscaleType: scale, andunit: unit)
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
        
        
//        if distance >= CP_CONSTANTS.TEN_KM {
//            let distanceToSend:Double = (Double(distance)/1000)
//             Log.i(tag: MaximusClass.MAXIMUS_DM, message: "distance to send : \(distanceToSend)")
//            let operationBlock = BlockOperation{
//                CPWrapper.sharedInstance().cp_update_current_manuever_distance_unit(LIVE_FLAG.TWO, anddistance: Int32(distanceToSend), andscaleType: SCALE_TYPE.ZERO, andunit: DISTANCE_UNIT.KILOMETER)
//            }
//            commandQueue.addCommandsToQueue(block: operationBlock)
//        } else if (distance >= CP_CONSTANTS.ONE_KM)  {
//            let distanceToSend:Double = (Double(distance)/1000.0) * 10.0
//
//            let operationBlock = BlockOperation{
//                CPWrapper.sharedInstance().cp_update_current_manuever_distance_unit(LIVE_FLAG.TWO, anddistance: Int32(distanceToSend), andscaleType: SCALE_TYPE.TEN, andunit: DISTANCE_UNIT.KILOMETER)
//            }
//            commandQueue.addCommandsToQueue(block: operationBlock)
//        }else {
//            let distanceToSend = distance
//
//            let operationBlock = BlockOperation{
//                CPWrapper.sharedInstance().cp_update_current_manuever_distance_unit(LIVE_FLAG.TWO, anddistance: Int32(distanceToSend), andscaleType: SCALE_TYPE.TEN, andunit: DISTANCE_UNIT.METER)
//            }
//            commandQueue.addCommandsToQueue(block: operationBlock)
//        }
    }
    
    func sendSourceNameToCp(sourceName: String) {
        
        let arr = sourceName.components(separatedBy: ",")
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Sending Source Name: \(arr[Constants.ZERO_VALUE])")
        let trun_sourceName:String = self.getTruncatedString(name: arr[Constants.ZERO_VALUE] ,trun_length:15)
        //Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Truncated Source Name: \(trun_sourceName)")
        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_SourceName(LIVE_FLAG.TWO, andSourceName: trun_sourceName)
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
    }
    
    /*****************************************************************************
     * Function :   getTruncatedString
     * Input    :   String
     * Output   :   String
     * Comment  :   Truncating the Name & added two dots at the end for showing that it exceed limit of string
     *
     ****************************************************************************/
    
    func getTruncatedString(name:String, trun_length:Int)->String{
        var tempName:String = name
        if (tempName.length > trun_length) {
            tempName = name.substring(to:name.index(name.startIndex, offsetBy: trun_length))
            return tempName + ".."
        }else{
            return tempName
        }
    }
    
    func sendDestinationName(destinationName: String) {
        let destArr = destinationName.components(separatedBy: ",")
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Sending Destination  Name: \(destArr[Constants.ZERO_VALUE])")
        let trun_DestiName:String = self.getTruncatedString(name: destArr[Constants.ZERO_VALUE] ,trun_length:15)
        //Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Truncated Source Name: \(trun_DestiName)")
        let block = BlockOperation{
            CPWrapper.sharedInstance().cp_DestinationName(LIVE_FLAG.TWO, andDestinationName: trun_DestiName)
        }
        commandQueue.addCommandsToQueue(block: block)
    }
    
    func sendNextCityName(nextCity: String){
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Sending Next City  Name: \(nextCity)")
        let trun_NextCity:String = self.getTruncatedString(name:nextCity ,trun_length:11)
        //Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Truncated Source Name: \(trun_NextCity)")
        
        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_dailyTargatedDestinationName(LIVE_FLAG.TWO, andNextCity: trun_NextCity)
        }
        
        commandQueue.addCommandsToQueue(block: operationBlock)
    }
    
    func sendFullJourneyDistance(distance:Int64,unit:Int32, scale:Int32) {
        
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Sending Full Journey Distance: \(distance)")
        let operationBlock = BlockOperation{
            
            CPWrapper.sharedInstance().cp_FullJourneyDistance(LIVE_FLAG.TWO, andDistance:Int32(distance),andscaleType: scale, andunit: unit)
        }
        
        commandQueue.addCommandsToQueue(block: operationBlock)
    }
    
    func setJourney(journeyOnOff: Int) {
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Set Journey \(journeyOnOff)")
        
        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_setJourney(LIVE_FLAG.TWO, andJourneyOnOff: Int32(journeyOnOff))
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
    }
    
    func dailyJourneyStartStop(journeyStartStop: Int) {
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Daily Journey Start/Stop \(journeyStartStop)")
        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_dailyJourneyStartStop(LIVE_FLAG.TWO, andJourneyStartStop: Int32(journeyStartStop))
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
    }
    
    //MARK: Heading Instructions
    /*****************************************************************************
     Heading Instruction
     ****************************************************************************/
    
    func sendHeadingAngle(angle: Int){
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Sending heading angle : \(angle)")
        
        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_headingBearingAngle(LIVE_FLAG.TWO, andAngle: Int32(angle))
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
    }
    
    func tbtHeadingDirection(nextTurn: String){
        
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Sending heading : \(nextTurn)")
        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_tbtHEADINGDirection(LIVE_FLAG.TWO, andString:nextTurn)
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
    }
    
    
    func tbtHeadingDestination(destination: String) {
        let truncatedStr = self.getTruncatedString(name: destination, trun_length: 15)
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Sending heading  destination name: \(truncatedStr)")
        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_tbtHEADING_(to: LIVE_FLAG.TWO, andString: truncatedStr) //send string name
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
    }
    
    func tbtHeadingStart(startStatus: Int) {
        
        if (stateDM != .HEADING){
            Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Sending heading  Start/Stop Status: \(startStatus)")
            let operationBlock = BlockOperation{
                CPWrapper.sharedInstance().cp_tbtHEADINGStart(LIVE_FLAG.TWO, andSTARTStatus:CP_HeadingStatusTypeDef(rawValue: UInt32(startStatus)))
            }
            commandQueue.addCommandsToQueue(block: operationBlock)
            stateDM = .HEADING///----Update the State of DM shwon screen
        }
        
    }
    
    func tbtHeadingDistance(distance:Int) {
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Sending heading distance: \(distance)")
        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_tbtHEADINGDistance(LIVE_FLAG.TWO, anddistance:Int32(distance), andscaleType:SCALE_TYPE.TEN, andunit:DISTANCE_UNIT.METER)
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
    }
    
    /****************************************************************************/
    
    func tbtStartStop(startStop: Int) {
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "TBT Start/Stop \(startStop)")
        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "MaximusDM: TBT Start/Stop \(startStop)")

        if (startStop == Constants.ZERO_VALUE){
            stateTBT = .START
            stateDM  = .TBT
        }else{
            stateTBT = .STOP
            stateDM  = .NONE
        }
        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_tbtStartStop(LIVE_FLAG.TWO, andTBTStartStop: Int32(startStop))
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
    }
    
    func followStraightArrow() {
        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_followStraightRoad()
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
    }
    
    func tbtHideShowTbt(hideShow: Int) {
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "TBT Hide/Show \(hideShow)")
        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "MaximusDM: ----------- TBT Hide/Show \(hideShow) ----------")

        if (hideShow == Constants.ZERO_VALUE){
            stateTBT = .START
            stateDM  = .TBT
        }else{
            stateTBT = .STOP
            stateDM  = .NONE
        }
        
        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_tbtHideShow(LIVE_FLAG.TWO, andTBTHideShow: Int32(hideShow))
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
    }
    
    func setFullJourneyOnOff(fulljourneyState: CP_JourneyOnOffTypeDef) {
        
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Full Journey State : \(fulljourneyState)")
        self.sendFullJourneyCompletedRideTime()
        MyDefaults.setBoolData(value: false, key: DefaultKeys.KEY_JOIRNEY_COMPLETE)
        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_fullJourneyStartStop(1, andJourneyStartStop: Int32(fulljourneyState.rawValue))
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
        
    }
    
    
    func updateDailyJourneyUnit(distance:Int64) {
        
        if distance >= CP_CONSTANTS.TEN_KM {
            let distanceToSend:Double = (Double(distance)/1000)
            Log.i(tag: MaximusClass.MAXIMUS_DM, message: "distance to send : \(distanceToSend)")
            let operationBlock = BlockOperation{
                CPWrapper.sharedInstance().cp_update_daily_journey_eta_unit(LIVE_FLAG.TWO, anddistance: Int32(distanceToSend), andscaleType: SCALE_TYPE.ZERO, andunit: DISTANCE_UNIT.KILOMETER)
            }
            commandQueue.addCommandsToQueue(block: operationBlock)
        } else if (distance >= CP_CONSTANTS.ONE_KM)  {
            let distanceToSend:Double = (Double(distance)/1000.0) * 10.0
            
            let operationBlock = BlockOperation{
                CPWrapper.sharedInstance().cp_update_daily_journey_eta_unit(LIVE_FLAG.TWO, anddistance: Int32(distanceToSend), andscaleType: SCALE_TYPE.TEN, andunit: DISTANCE_UNIT.KILOMETER)
            }
            commandQueue.addCommandsToQueue(block: operationBlock)
        }else {
            let distanceToSend = distance
            
            let operationBlock = BlockOperation{
                CPWrapper.sharedInstance().cp_update_daily_journey_eta_unit(LIVE_FLAG.TWO, anddistance: Int32(distanceToSend), andscaleType: SCALE_TYPE.TEN, andunit: DISTANCE_UNIT.METER)
            }
            commandQueue.addCommandsToQueue(block: operationBlock)
        }
        
//        let operationBlock = BlockOperation{
//            //Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Sending daily journey eta distance")
//
//            CPWrapper.sharedInstance().cp_update_daily_journey_eta_unit(LIVE_FLAG.TWO, anddistance: Int32(convertDistance), andscaleType:SCALE_TYPE.TEN, andunit:Int32(unit))
//        }
//        commandQueue.addCommandsToQueue(block: operationBlock)
    }
    
    func updateDailyJourneyEta(hour:Int , minutes:Int, ampm:Int)  {
        //Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Sending daily journey eta ")
        
        let operationBlock = BlockOperation{
            
            CPWrapper.sharedInstance().cp_update_daily_journey_eta(LIVE_FLAG.TWO, andhour: Int32(hour), andminute:Int32(minutes), andAmPm: Int32(ampm) ,andTmeFormat: TIME_FORMAT.ZERO)
        }
        
        commandQueue.addCommandsToQueue(block: operationBlock)
    }
    
    func getTimeFromSeconds(duration:Int) -> (Int, Int, Int){
        var hours = Constants.ZERO_VALUE
        
        
        let days = Constants.ZERO_VALUE
        
        if (duration > CP_CONSTANTS.HOUR_IN_SECONDS) {
            hours = duration/CP_CONSTANTS.HOUR_IN_SECONDS
        }else{
            hours = Constants.ZERO_VALUE
        }
        
        var minutes = Constants.ZERO_VALUE//(duration % 3600)/CP_CONSTANTS.SECOND_IN_MINUTE
        
        if duration > CP_CONSTANTS.HOUR_IN_SECONDS{
            
            minutes = (duration % CP_CONSTANTS.HOUR_IN_SECONDS)/CP_CONSTANTS.SECONDS_IN_MINUTE
        }else{
            minutes = duration/CP_CONSTANTS.SECONDS_IN_MINUTE
        }
        
        return ( days, hours, minutes)
    }
    
    func syncJourneyDetails(){
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Sync Journey Details")
        let rideDetails = JourneyManager.sharedInstance.currentRideDetails()
        if rideDetails.planned {

            self.sendSourceNameToCp(sourceName: (rideDetails.startLocation_name)!)
            self.sendDestinationName(destinationName: (rideDetails.endLocation_name)!)
            
            self.updateJourneyEta(duration: Int(rideDetails.total_ride_time ?? "")!)
            
            let (convertDistance,unit, scale) = self.getDistanceAndUnit(distance: Int64(rideDetails.total_ride_distance ?? "")!)
                
            self.sendFullJourneyDistance(distance:convertDistance,unit:unit, scale: scale)
        }
    }
    
    // This function is called once BLE is paired
    func journeyStartSetUp(rideDetailsObj: Rides, totalDuration: Int, totalTime: Int){
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Journey Set Up")
        if rideDetailsObj.planned
        {
            
            Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Journey Set Up \(String(describing: rideDetailsObj.ride_title))")
            
            self.sendSourceNameToCp(sourceName: rideDetailsObj.startLocation_name ?? "")
            self.sendDestinationName(destinationName: rideDetailsObj.endLocation_name ?? "")
    
            
            self.updateJourneyEta(duration: totalTime)
            
            let (convertDistance,unit, scale) = self.getDistanceAndUnit(distance: Int64(totalDuration))
                
            self.sendFullJourneyDistance(distance:convertDistance,unit:unit, scale: scale)
            
            self.sendNextCityName(nextCity: "")
            
            self.setJourney(journeyOnOff: Int(JOURNEY_START.rawValue))
        }
    }
    
    
    func sendDailyCompletedRideTime(minutes:Int, hours:Int) {
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Sending ride time \(hours): \(minutes)")
        
        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_DailyCompletedRideTime(LIVE_FLAG.TWO, minutes: Int32(minutes), hours: Int32(hours))
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
        
    }
    
    //cp_FullJourneyCompletedRideTime
    
    func sendFullJourneyCompletedRideTime() {
        //
        
        let rideStartTime = RideStateHandler.sharedInstance.getRideStartTime()
        
        let timeNow = Date()
        let journeyDuration:Int = Int(timeNow.timeIntervalSince(rideStartTime)) //Int(Date().timeIntervalSince(rideStartTime))
        let (days, hours, minutes) =  self.getTimeFromSeconds(duration: journeyDuration)
        
        Log.i(tag: "Leg Completed", message: "Sending ride time \(days): \(hours): \(minutes)")
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Sending ride time \(days): \(hours): \(minutes)")
        
        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_FullJourneyCompletedRideTime(LIVE_FLAG.TWO, minutes: Int32(minutes), hours: Int32(hours), days: Int32(days))
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
    }
    
    // MARK:- RoundAbout Turns
    
    func currentRoundAbout(exitNumber:Int, angle:Int ,iconID:Int,isSink:Bool)  {
        
        
        if (exitNumber != self.oldobjRoundAboutCurent.exitNumber || isSink == true){
            
            Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Current Round about exit:\(exitNumber) angle:\(angle)")
            //flagCurrentRoundAbout = true
            oldexitNoCurrent = exitNumber
            
            self.oldobjRoundAboutCurent = RoundAboutExit(exitNumber: exitNumber, angle: angle, iconID: iconID)
            
            let operationBlockFirst = BlockOperation{
                CPWrapper.sharedInstance().cp_currentIconRoundAboutBearingInfo(LIVE_FLAG.TWO, exitNumer: Int32(exitNumber), andAngle: Int32(angle))
            }
            commandQueue.addCommandsToQueue(block: operationBlockFirst)
            
            ////Send Icon over DM
            let operationBlockSecond = BlockOperation{
                CPWrapper.sharedInstance().cp_update_current_manuever_icon(LIVE_FLAG.TWO, andiconID: Int32(iconID))
            }
            
            commandQueue.addCommandsToQueue(block: operationBlockSecond)
        }
    }
    
    func nextRoundAbout(exitNumber:Int, angle:Int ,iconID:Int ,isSink:Bool)  {
        
        if (exitNumber != self.oldobjRoundAboutNext.exitNumber || isSink == true){
            Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Next Round about exit:\(exitNumber) angle:\(angle)")
            
            oldexitNoNext = exitNumber
            self.oldobjRoundAboutNext = RoundAboutExit(exitNumber: exitNumber, angle: angle, iconID: iconID)
            
            let operationBlockFirst = BlockOperation{
                CPWrapper.sharedInstance().cp_nextIconRoundAboutBearingInfo(LIVE_FLAG.TWO, exitNumer: Int32(exitNumber), andAngle: Int32(angle))
                
            }
            commandQueue.addCommandsToQueue(block: operationBlockFirst)
            
            ////Send Icon over DM
            
            let operationBlockSecond = BlockOperation{
                CPWrapper.sharedInstance().cp_update_next_manuever_icon(LIVE_FLAG.TWO, andiconID: Int32(iconID))
            }
            commandQueue.addCommandsToQueue(block: operationBlockSecond)
        }
    }
    
    func catchNotificationJourneyRouteFinished(notification:Notification) -> Void{
        
        //self.tbtStartStop(startStop: Int(STOP.rawValue))
        
    }
    
    func catchNotificationRideRouteFinished(notification:Notification) -> Void{
        //stop journey
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Navigation completed")
       
       
        let state = NavigationLocator.sharedInstance().getStatusOfNavigation()
        switch state {
        case .NAVIGATORSTATEACTIVE,.NAVIGATORSTATEPAUSED,.NAVIGATORSTATEROUTING,.NAVIGATORSTATENOROUTE,.NAVIGATORSTATENOLOCATION:
            
                NavigationLocator.sharedInstance().stopNavigationEngine()
                self.setFullJourneyOnOff(fulljourneyState: JOURNEY_STOP)
                break
            case .NAVIGATORSTATECOMPLETED:
                 print("Navigation engine Already stopped")
                break
            case .NAVIGATORSTATEREADY:
                break
            case .NAVIGATORSTATEERROR:
                break
            }
        
        
        self.clearPreviousDefaults()///----Clear previous saved values
        
        ///State TBT & DM set to default
        stateTBT = .STOP
        stateDM  = .NONE
        
    }
    
    //MARK: Journey Start Notification
    // This function is called when planned ride is started
    func catchNotificationOnPlannedRideStart(notification:Notification) -> Void{
        
    }
    
    func catchJourneyStatredNotification(notification:Notification) -> Void{
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Full journey start")
    }
    
    //MARK: Leg States
    //This function called when a Leg is started
    func catchNotificationOnLegStarted(notification:Notification) -> Void{
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Leg Started-----------")
        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "---------- Leg Started --------------")

        if (stateTBT != .START){
            self.tbtStartStop(startStop: Int(START.rawValue))
            self.stateTBT = .START
        }
    }
    
    func catchNotificationOnLegInProgress(notification:Notification) -> Void{
        guard let legDetail:NSLegProgress = notification.userInfo!["data"] as! NSLegProgress? else { return }
        
        var distance:Int64 = 0
        if let dist =  legDetail.getDistanceRemaining()?.getMeters(){
            distance = dist
        }
        
        var duration:Int64 = 0
        if let dur = legDetail.getDurationRemaining()?.getSeconds() {
            duration = dur
        }
        self.updateETAEveryMinuite(duration: Int(duration), distnace: Int(distance))
    }

    
    //This function gets called on Leg complete
    func catchNotificationOnLegCompleted(notification:Notification) -> Void{
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Leg Completed -------------------")
        stateTBT = .STOP
    }
    
    func catchNotificationOnLegSkipped(notification:Notification) -> Void
    {
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Leg Skipped -------------------")
        stateTBT = .STOP
    }
    
    /*****************************************************************************
     * Function :   destination approaching
     * Input    :   distance
     * Output   :
     * Comment  :
     *
     ****************************************************************************/
    
    func destinationApproaching() {
        if stateDM != .DESTINATION_REACHING {
            Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Destination Approaching -------------------")
            self.stateDM = .DESTINATION_REACHING
            let operationBlockSecond = BlockOperation{
                CPWrapper.sharedInstance().cp_approachingDestination()
            }
            commandQueue.addCommandsToQueue(block: operationBlockSecond)
        }
    }
    
    /*****************************************************************************
     * Function :   poi approaching
     * Input    :   distance
     * Output   :
     * Comment  :
     *
     ****************************************************************************/
    
     func poiApproaching() {
        if stateDM != .POI_REACHING {
            Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Poi Approaching -------------------")
            self.stateDM = .POI_REACHING
            let operationBlockSecond = BlockOperation{
                CPWrapper.sharedInstance().cp_approachingPoi()
            }
            commandQueue.addCommandsToQueue(block: operationBlockSecond)
        }
    }
    
    /*****************************************************************************
     * Function :   updateETAEveryMinuite
     * Input    :   duration,distnace
     * Output   :
     * Comment  :
     *
     ****************************************************************************/
    
    fileprivate func updateETAEveryMinuite(duration:Int ,distnace:Int){
        
        /* broadcast LEG ETA at every x time interval*/
        
        if(mPrevLegETA <= 0){
            mPrevLegETA = 0;
        }
        else{
            
            let diffETA = mPrevLegETA - duration;
            if(diffETA <= 0){
                mPrevLegETA = 0;
            }
            else{
                mPrevLegETA = diffETA;
            }
        }
        
        travelTimeCounter = travelTimeCounter + mPrevLegETA
        
        if(travelTimeCounter >= 60){
            
            /* Send ETA */
            let (HRS ,min,timeAMPM) = self.getRemainingHRSAndMinutes(duration:Int64(duration),format:0)
            MaximusDM.sharedInstance.updateDailyJourneyEta(hour: HRS, minutes: min, ampm:Int(timeAMPM))
            
            if (distnace !=  mPrevRemainingLegDist) {
                /* Send remaining distance */
                MaximusDM.sharedInstance.updateDailyJourneyUnit(distance: Int64(distnace))
                
            }
            mPrevRemainingLegDist = distnace;
            
            mPrevLegETA = 0;
            travelTimeCounter = 0;
        }
        else {
            mPrevLegETA = duration;
        }
    }
    
    /*****************************************************************************
     * Function :   getDistanceAndUnit
     * Input    :   Distance
     * Output   :   converted Distance along with Unit
     * Comment  :
     *
     ****************************************************************************/
    
    fileprivate func getDistanceAndUnit(distance:Int64)->(Int64,Int32,Int32){
        
        if Int(distance) >= CP_CONSTANTS.TEN_KM {
            let dist:Double = (Double(distance)/1000.0)
            let scale = SCALE_TYPE.ZERO
            return (Int64(dist),DISTANCE_UNIT.KILOMETER, scale)
        }else
            if Int(distance) >= CP_CONSTANTS.ONE_KM{
                let dist:Double = (Double(distance)/1000.0) * 10
                let scale = SCALE_TYPE.TEN
                return (Int64(dist),DISTANCE_UNIT.KILOMETER, scale)
            }else{
                let scale = SCALE_TYPE.TEN
                return (distance,DISTANCE_UNIT.METER, scale)
        }
    }
    
    /*****************************************************************************
     * Function :   getRemainingHRSAndMinutes
     * Input    :   Duration & Time format AM or PM
     * Output   :   Hours, minutes & TimeFormat
     * Comment  :   Send Duration to method which will add it to current time and return hours and minuites
     *
     ****************************************************************************/
    fileprivate func getRemainingHRSAndMinutes(duration:Int64,format:Int32)->(Int,Int,Int32){
        let minuites = duration/60
        let date = Date().add(minutes: Int(minuites))
        let (hours,min,_) = date.getHourMinutesSecond()
        if format == 0 {
            //12 HRS FORMAT
            let (HRS,TIME_FORMAT) = self.TwelveHRSFormat(hours: hours)
            return (HRS,min,TIME_FORMAT)
        }else{
            //24 HRS FORMAT
            return (hours,min,1)
        }
    }
    /*****************************************************************************
     * Function :   TwelveHRSFormat
     * Input    :   Hours
     * Output   :   Time format & Hours
     * Comment  :   Time format like is AM or PM with Hours according to Format
     *
     ****************************************************************************/
    fileprivate func TwelveHRSFormat(hours:Int)->(Int,Int32){
        if hours > 12 {
            return (hours - 12,TIME_AM_PM.ONE)
        }else{
            return (hours,TIME_AM_PM.ZERO)
        }
    }
    
    
    // MARK: Step States
    //This function is called when a step is started
    func catchNotificationOnStepStarted(notification:Notification) -> Void{
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Step Started -------------------")
        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "MaximusDM: ----------- Step Started -------------")

        if self.stateDM != .TBT {
            self.tbtHideShowTbt(hideShow: Int(TBT_ON.rawValue))
        }
    }
    
    //This function is called when a step is in progress
    func catchNotificationOnStepIsInProgress(notification:Notification) -> Void {
        
        if self.stateDM != .TBT && self.stateDM != .LONG_STEP && self.stateDM != .DESTINATION_REACHING && self.stateDM != .POI_REACHING  {
            self.tbtHideShowTbt(hideShow: Int(TBT_ON.rawValue))
        }
        
        guard let stepRemainingDistancce:Int64 = notification.userInfo!["stepRemainingDistance"] as! Int64? else { return }
        
        if stepRemainingDistancce > CP_CONSTANTS.HALF_KM {
            
            if self.windingState != .WINDING_STARTED && self.stateDM != .LONG_STEP {
                self.stateDM = .LONG_STEP
            }
            
        }
        else
        {
            if stateDM == .LONG_STEP {
                Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Showing TBT remaining distance \(stepRemainingDistancce)")
                self.tbtHideShowTbt(hideShow: Int(TBT_ON.rawValue))
            }
            
        }
        
    }
    
    //This function is called once step is completed
    func catchNotificationOnStepCompleted(notification:Notification) -> Void{
        
        self.clearPreviousDefaults()///----Clear previous saved values
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Step Completed -------------")
        
    }
    
    //This function is called once step is skipped
    func catchNotificationOnStepSkipped(notification:Notification) -> Void{
        
        self.clearPreviousDefaults()///----Clear previous saved values
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Step Skipped -------------")
        
    }
    
    func clearPreviousDefaults(){
        ///Manuever Current & Next Variables clear
        prevCurrentIcon     = CP_CONSTANTS.ICON_DEFAULTVALUE
        prevNextIcon        = CP_CONSTANTS.ICON_DEFAULTVALUE
        
        ///Manuever Exit Number current & Next Variables clear
        oldexitNoCurrent       = 0
        oldexitNoNext          = 0
        self.oldobjRoundAboutCurent = RoundAboutExit()
        self.oldobjRoundAboutNext   = RoundAboutExit()
        self.stateTBT = .NONE
        self.stateDM = .NONE
        self.windingState = .OTHER
    }
    
    // MARK:Notification ON/OFF Bluetooth Events
    
    func eventBLEStatusUpdate(notification:Notification){
        let status:BluetoothState = notification.userInfo!["data"] as! BluetoothState
        
        if (status == .CONNECTED_P) {
            Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Connect")
            Thread.sleep(forTimeInterval: 3)
            self.clearPreviousDefaults()
            
            ////Send Date & Time once it connect to BT
            self.sendDateAndTimeToDM()
            ////Check navigation status and sink data with DM accordingly
            self.navigatorState()
        }else{
            //Set to default when disconnected BLE
            FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "---------------- MaximusDM: DisConncted -------------------")
            
            Log.i(tag: MaximusClass.MAXIMUS_DM, message: "DisConnect")
            self.prevCurrentIcon = CP_CONSTANTS.ICON_DEFAULTVALUE
            self.prevNextIcon = CP_CONSTANTS.ICON_DEFAULTVALUE
        }
    }
    
    func navigatorState() {
        
        switch NavigationLocator.sharedInstance().getStatusOfNavigation() {
        case .NAVIGATORSTATEACTIVE:
            print("MaximusDM: navigatorState NE Active ")
            DeviceLocationServiece.sharedInstance().startUpdatingLocations()
            break
            
        case .NAVIGATORSTATENOLOCATION:
            print(" MaximusDM: navigatorState NO Location ")
            self.sendWeakSignalCommand()
            break
            
        case .NAVIGATORSTATEREADY:
            break
            
        case .NAVIGATORSTATEPAUSED:
            break
        case .NAVIGATORSTATEROUTING:
            break
        case .NAVIGATORSTATENOROUTE:
            break
            
        case .NAVIGATORSTATECOMPLETED:
            break
        case .NAVIGATORSTATEERROR:
            break
        default:
            break
        }
    }
    
    
    //////********************************************
    // MARK:- TBT Commands
    func catchCurrentTurn(notification:Notification)->Void{
    }
    
    // MARK: Send Manuever icons to Dm
    func sendCurrentManueverIconToDM(notification:Notification) -> Void  {
        if let iconId:Int = notification.userInfo?["data"] as! Int? {
            
            self.updateCurrentManueverIcon(iconId: iconId, isSinkData:false)
        }
    }
    
    func sendNextManueverIconToDM(notification:Notification) -> Void  {
        if let iconId = notification.userInfo?["data"] as! Int? {
            self.updateNextManueverIcon(iconId: iconId, isSinkData:false)
        }
    }
    
    
    func catchRemaningDistance(notification:Notification) -> Void  {
        guard let distance:Int64 = notification.userInfo!["Data"] as! Int64? else { return }
        print("Distance In Meters: \(distance)")
        self.updateCurrentManueverDistance(distance: Int(distance))
    }
    
    //MARK: Deviation detected
    func catchDeviationEvent(notification:Notification) -> Void  {
        
        guard let navigator:NSNavigator = notification.userInfo!["data"] as! NSNavigator? else { return }
        
        guard let objDeviation:NSDeviation = navigator.getStatus()?.getDeviation() else { return }
        
        let (convertDistance,unit, scale) = self.getDistanceAndUnit(distance: (objDeviation.getPoint()?.getDistance()?.getMeters())!)
        
        /// Get relative angle from deviation
        var angle:Int32 = Int32(Constants.ZERO_VALUE)
        if (navigator.getStatus()?.hasRelativeBearing())!{
            angle = Int32((navigator.getStatus()?.getRelativeBearing())!)
        }
        
        let operationBlock1 = BlockOperation{
            CPWrapper.sharedInstance().cp_setDeviationAlertData(LIVE_FLAG.TWO, anddistance:Int32(convertDistance), andscaleType:scale, andunit:Int32(unit), andAngle:angle)
        }
        commandQueue.addCommandsToQueue(block: operationBlock1)
        
        let operationBlock2 = BlockOperation{
            CPWrapper.sharedInstance().cp_showDeviationAlert(Int32(START.rawValue))
        }
        commandQueue.addCommandsToQueue(block: operationBlock2)
        
        stateDM = .DEVIATION//------Update DM state Showing screen
    }
    
    //MARK: Winding Road
    func windingRoadStarted(){
        if self.windingState != .WINDING_STARTED {
            Log.i(tag: MaximusClass.MAXIMUS_DM, message:"Winding road starts ---------")
            self.updateCurrentManueverIcon(iconId: Int(ICON_NAV_WINDING_ROAD_L.rawValue), isSinkData: false)
            self.windingState = .WINDING_STARTED
        }
    }
    
    func windingRoadEnds() {
        if self.windingState == .WINDING_STARTED {
            Log.i(tag: MaximusClass.MAXIMUS_DM, message:"Winding road ends ---------")
            self.prevCurrentIcon = CP_CONSTANTS.ICON_DEFAULTVALUE
            self.windingState = .WINDING_END
        }
    }
    
    //MARK: Sharp turns and Heir pin turns alert
    
    func sendSharpBendAlerts(alertId: Int, type:CP_AlertTypeInfoTypeDef) {
        Log.i(tag: MaximusClass.MAXIMUS_DM, message:"Sharp Bend Begin ")
        CPWrapper.sharedInstance().cp_showNonUrgentIncomingAlert(Int32(alertId), andAlertType: type, andUUID: 0, andStructure: nil)
    }
    
    func sendSharpBendExpired(alertId: Int, type:CP_AlertTypeInfoTypeDef){
        Log.i(tag: MaximusClass.MAXIMUS_DM, message:"Sharp Bend Expired")
        CPWrapper.sharedInstance().cp_AlertEnd_Incoming(Int32(alertId), andType: type, andUUID: 0)
    }
    
    
    /****************************************************************************/
    //MARK: WEAK Signal
    
    func sendWeakSignalCommand(){
        
        self.stateDM = .WEAK_GPS
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "Weak GPS")

        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_WeakGPSSignal(CP_GpsStatusTypeDef(rawValue: 1))
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
        
        self.sendBackLightModeToDM()
    }
    
    // MARK: NAVIGATOR BECAME ACTIVE
    
    func navigatorBecameActive() {
        self.stateDM = .NONE
    }
    
    /****************************************************************************/
    //MARK: DM Backlight Commands
    func sendBackLightControlModeToDM(){
        
        let backLightManager = BacklightManager()
        
        let operationBlock = BlockOperation{
            CPWrapper.sharedInstance().cp_DMBacklightControlMode(CP_BacklightControlModeTypeDef(rawValue: backLightManager.getBackLightAutoMode()))
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
        
        self.sendBackLightModeToDM()
    }
    
    func sendBackLightModeToDM(){
        
        let backLightManager = BacklightManager()
        
        let operationBlock = BlockOperation{
            
            CPWrapper.sharedInstance().cp_DMBacklightMode(CP_BacklightModeTypeDef(rawValue: backLightManager.getBackLightMode()))
        }
        commandQueue.addCommandsToQueue(block: operationBlock)
    }
    
    
}




extension MaximusDM:cp_ProtocolDelegate{
    
    public func onDMKeyAlertClosed(_ alertID:UInt8, andType alertType: CP_AlertTypeInfoTypeDef){
        print(alertID)
        print(alertType)
        sinkManager.notifyObservers(notificationKey: sinkManager.ALERT_NOTIFICATION_CLOSE_ALERT_EVENT_FROM_DM, sender: self)
    }
    
    
    
    public func onAlertModeChange(_ alertMode: Bool){
        
        if alertMode{
            JourneyManager.sharedInstance.setAlertModeState(state: AlertModeStatus.On)
        }else{
            JourneyManager.sharedInstance.setAlertModeState(state: AlertModeStatus.Off)
        }
        
    }
    
    public func onDMKeyPressIncomingAlert(_ alertID: UInt8, andUUID uuid: Int32, andIsAccepted status: Bool) {
        var strStatus = AlertConstant.ALERT_RESPONSE_STATUS_UNKNOWN
        if status {
            strStatus = AlertConstant.ALERT_RESPONSE_STATUS_ACKNOWLEDGED
        }else{
            strStatus = AlertConstant.ALERT_RESPONSE_STATUS_DISMISSED
        }
        
        let alertDbManager = AlertDatabaseManager()
        let arrResult = alertDbManager.getAlertFromReceivedAlertTableWithId(Id:"\(uuid)")
        let alertsArray = arrResult as! [ReceiveAlert]
        
        if alertsArray.count > Constants.ZERO_VALUE{
            
            if let uuidDb = alertsArray.get(at: Constants.ZERO_VALUE)?.alertUUID{
                let alertContext = AlertContext()
                alertContext.changeStateToAttemptAck(responseId: strStatus,
                                                     generatedAlertId: String(uuid),
                                                     uuid: uuidDb)
            }
            
        }else{
            
            CPWrapper.sharedInstance().cp_AlertResponseSent_Incoming(12, andAlertType: CP_AlertTypeInfoTypeDef(rawValue: 2))
        }
    }
    
    public func onDMKeyOutgoingAlert(toApp alertID: UInt8, andType alertType: CP_AlertTypeInfoTypeDef) {
        
        JourneyManager.sharedInstance.setAlertGenerator(generator: AlertGenerator.DM)
        
        var showNotSent:Bool = true
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: "\(alertID) and \(alertType)")
        let journeyManager = JourneyManager.sharedInstance
        if journeyManager.isAnyRideInProgress() {
            
            if journeyManager.rideProgressType() == RideTypes.Planned{
                let rideDetails = journeyManager.currentRideDetails()
                
                if let rideJoiners:Int16 = rideDetails.joined_users{
                    if rideJoiners > 1{
                        
                        showNotSent = false
                        
                        if JourneyManager.sharedInstance.getAlertSendingType() == AlertSendingType.Unknown{
                            let navigationController = UINavigationController(rootViewController:MyAlertVC(alertCategory: alertType, AlertID:Int(alertID), isAutoGenerateAlert:true))
                            UIApplication.topViewController()?.presentVC(navigationController)
                        }else{
                            if JourneyManager.sharedInstance.getAlertSendingType() != AlertSendingType.AboutToSent{
                                JourneyManager.sharedInstance.setAlertSendingType(type: AlertSendingType.ReSent)
                            }
                            
                            let alertContext:AlertContext = AlertContext()
                            alertContext.changeStateToAlertGenerate(alertType: AlertApiManager().getTypeOfAlert(category: alertType, alertID: Int(alertID)))
                        }
                    }
                }
            }
        }
        
        
        if showNotSent{
            CPWrapper.sharedInstance().cp_AlertFailed_Outgoing(CP_AlertSentTypeDef(rawValue: 0))
        }
        
        
    }
    
    public func onChargingStatus(toAPP alertID: Int32, andType alertType: Int32) {
        
    }
    
    
    public func onDMKeyPressEvent(_ key_event: Int32, andsensor_data sensor_data: Int32) {
        
    }
    
    
    public func onCallBackData(_ commandType: Int32, andStatus status: Int32) {
        print("CP Wrapper callback:\(status)")
       BluetoothServices.sharedInstance().sentHCount = 0
       BluetoothServices.sharedInstance().startHeatbeatTimer()
    }
    
    
    public func onAckReceive(_ data: Data!, inMethod length: Int32) {
     //   DispatchQueue.main.asyncAfter(deadline: .now() + CP_CONSTANTS.BLE_DELAY){
            print("CP Wrapper \(Array(data))")
            BluetoothManager.sharedInstance().sendToBle(data: data, char: .RXCharacteristic)
     //   }
    }
    
    func returnData(_ data: String!, inMethod method: String!) {
        NSLog(data,method)
    }
    
    
    func  updateDmBatteryChargingStatus(_ status: CP_DmChargingTypeDef){
        dmChargingDelegate?.onDMChargingStateChange(state: status)
        
        if status == CHARGING_ON_USB_CONNECTED {
            self.setBatteryChargingStatus(status: true)
        }else{
            self.setBatteryChargingStatus(status: false)
        }
    }
    
}

//////CLASS ROUND_ABOUT DATA
class RoundAboutExit: NSObject {
    fileprivate var exitNumber:Int = 0
    fileprivate var angle:Int?
    fileprivate var iconID:Int?
    
    override init() {
        super.init()
        self.exitNumber = 0
    }
    
    init(exitNumber:Int ,angle:Int ,iconID:Int) {
        self.exitNumber  = exitNumber
        self.angle      = angle
        self.iconID     = iconID
    }
    
}

