//
//  AlertDatabaseManager.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 05/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class AlertDatabaseManager: NSObject {
    
    func getCategoryIdByName(category:String) -> UInt32{
    
        switch category {
        case AlertConstant.TYPE_NON_URGENT_STRING:
            return 1
        case AlertConstant.TYPE_URGENT_STRING:
            return 2
        case AlertConstant.TYPE_CRITICAL_STRING:
            return 3
        default:
            break
        }
        return 0
    }
    
    func getAlertIdByType(alertType:String) -> UInt32{
    
        switch alertType {
        case AlertConstant.ALERT_TYPE_ACCIDENT:
            return 0
        
        case AlertConstant.ALERT_TYPE_NO_FUEL:
            return 2
            
        case AlertConstant.ALERT_TYPE_PUNCTURE:
            
            return 1
            
        case AlertConstant.ALERT_TYPE_SEVERE_CONDITION:
            return 0
            
        default:
            return 100
        }
      
    }
    //MARK:- Sent ALert Methods
    func insertGeneratedAlertDetails(alertId:Int32, alertTriggeredById:Int32, alertRideId:Int32, alertShortName:String, alertCategory:String, alertPriority:Int32, alertStatus:String, alertHelpReceived:String, alertRespondantBikerName:String){
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        
        do {
            try db?.createTable(table: SendingAlert.self)
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        
        do {
            
            try db?.insertIntoTable(table: SQLTableType.SENDING_ALERT, queryString: DBQuery.QUERY_SENDING_ALERT_INSERT_INTO, data:SendingAlert(alertId: alertId, alertTriggeredById: alertTriggeredById, alertRideId: alertRideId, alertShortName: alertShortName, alertCategory: alertCategory, alertPriority: alertPriority, alertStatus: alertStatus, alertHelpReceived: alertHelpReceived, alertRespondantBikerName:alertRespondantBikerName))
            
        } catch {
            print(db?.errorMessage ?? "error Message while inserting a value intoTable")
        }
        
        
        
    }
    
    func updateSentAlertStatusWith(status:String ,havingAlertid:Int32){
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        
        do {
            try db?.createTable(table: SendingAlert.self)
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        
        do {
            
            try db?.updateIntoTable(table: SQLTableType.SENDING_ALERT, queryString:DBQuery.QUERY_UPDATE_SENDING_ALERT_STATUS, data:SendingAlert(alertId:havingAlertid, alertStatus:status))
            
        } catch {
            print(db?.errorMessage ?? "error Message while inserting a value intoTable")
        }
        
        
    }
    
    func updateSentAlertHelperBikerWith(name:String ,havingAlertid:Int32){
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        
        do {
            try db?.createTable(table: SendingAlert.self)
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        
        do {
            
            try db?.updateIntoTable(table: SQLTableType.SENDING_ALERT, queryString:DBQuery.QUERY_UPDATE_SENDING_ALERT_HELPER_BIKER, data:SendingAlert(alertId:havingAlertid, helperBiker:name))
            
        } catch {
            print(db?.errorMessage ?? "error Message while inserting a value intoTable")
        }
        
        
    }
    
    
    func getAlertsFromSentAlertTableWithStatus(status: String) ->[Any] {
        
        var arrResult:[Any]?
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("@AlertDB: Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        do {
            try db?.createTable(table: SendingAlert.self)
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        do {
            arrResult  = try db?.fetchAllTableRecordWithRespToField(table: SQLTableType.SENDING_ALERT, queryString: DBQuery.QUERY_SENDING_ALERT_FETCH_BY_STATUS, field: status)
            // arrResult = try db?.fetchAllTableRecord(table: SQLTableType.SENDING_ALERT, queryString: DBQuery.QUERY_SENDING_ALERT_FETCH_ALL_RECORDS)
            
            let arrr = arrResult as! [SendingAlert]
            print("arrResult route ",arrr.get(at: 0)?.alertId ?? "")
            
            
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        
        return arrResult!
        
    }
    
    
    
    //MARK:- Recieved Alert Methods
    
     func insertReceivedAlertDetails(alertId:Int32, alertRideId:Int32, alertUUID:String, alertStatus:String, alertOriginatedByBikerId:Int32, alertOriginatedByBikerName:String, alertShortName:String, alertCategory:String, alertPriority:Int32,  alertHelped:Int32, alertLocation:String){
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        
        do {
            try db?.createTable(table: ReceiveAlert.self)
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        
        do {
            
           try db?.insertIntoTable(table: SQLTableType.RECEIVE_ALERT, queryString: DBQuery.QUERY_RECEIVE_ALERT_INSERT_INTO, data:ReceiveAlert(alertId: alertId, alertRideId: alertRideId, alertUUID: alertUUID, alertStatus: alertStatus, alertOriginatedByBikerId: alertOriginatedByBikerId, alertOriginatedByBikerName: alertOriginatedByBikerName, alertShortName: alertShortName, alertCategory: alertCategory, alertPriority: alertPriority, alertHelped: alertHelped, alertLocation: alertLocation))
            
        } catch {
            print(db?.errorMessage ?? "error Message while inserting a value intoTable")
        }
        
        
        
    }

    
    func updateReceivedAlertStatusWith(status:String ,havingAlertid:Int32){
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        
        do {
            try db?.createTable(table: ReceiveAlert.self)
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        
        do {
            
            try db?.updateIntoTable(table: SQLTableType.RECEIVE_ALERT, queryString:DBQuery.QUERY_UPDATE_RECEIVE_ALERT_STATUS, data:ReceiveAlert(alertId: havingAlertid, alertStatus: status))
            
        } catch {
            print(db?.errorMessage ?? "error Message while inserting a value intoTable")
        }
        
        
    }
    
    func getAlertsFromReceivedAlertTableWithStatus(status: String) ->[Any] {
        
        var arrResult:[Any]?
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("@AlertDB: Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        do {
            try db?.createTable(table: ReceiveAlert.self)
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        do {
            arrResult  = try db?.fetchAllTableRecordWithRespToField(table: SQLTableType.RECEIVE_ALERT, queryString: DBQuery.QUERY_RECEIVE_ALERT_FETCH_BY_STATUS, field: status)
            
            //arrResult = try db?.fetchAllTableRecord(table: SQLTableType.SENDING_ALERT, queryString: DBQuery.QUERY_SENDING_ALERT_FETCH_ALL_RECORDS)
            
            let arrr = arrResult as! [ReceiveAlert]
            print("arrResult route ",arrr.get(at: 0)?.alertId ?? "")
            
            
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        
        return arrResult!
        
    }
    
    
    func getAlertFromReceivedAlertTableWithId(Id: String) ->[Any] {
        
        var arrResult:[Any]?
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("@AlertDB: Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        do {
            try db?.createTable(table: ReceiveAlert.self)
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        do {
            arrResult  = try db?.fetchAllTableRecordWithRespToField(table: SQLTableType.RECEIVE_ALERT, queryString: DBQuery.QUERY_RECEIVE_ALERT_FETCH_BY_ID, field: Id)
            
        
            let arrr = arrResult as! [ReceiveAlert]
            print("arrResult route ",arrr.get(at: 0)?.alertId ?? "")
            
            
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        
        return arrResult!
        
    }
    func getAlertFromSendAlertTableWithId(Id: String) ->[Any] {
        
        var arrResult:[Any]?
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("@AlertDB: Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        do {
            try db?.createTable(table: SendingAlert.self)
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        do {
            arrResult  = try db?.fetchAllTableRecordWithRespToField(table: SQLTableType.SENDING_ALERT, queryString: DBQuery.QUERY_SENDING_ALERT_FETCH_BY_ID, field: Id)
            
            
            let arrr = arrResult as! [SendingAlert]
            print("arrResult route ",arrr.get(at: 0)?.alertId ?? "")
            
            
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        
        return arrResult!
        
    }
    


    
}
