//
//  AlertApiManager.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 28/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

@objc protocol AlertApiManagerDelegate {
    
    @objc optional func generateAlertSuccess(alertObj:[String:Any])
    @objc optional func closeAlertSuccess(alertObj:[String:Any])
    @objc optional func alertRespondSuccess(message:String)
    @objc optional func alertDeliveredAckSuccess(message:String)
    @objc optional func onFailure(Error:[String:Any])
}


class AlertApiManager: NSObject {
    
    var alertManagerDelegate:AlertApiManagerDelegate?
    var cloudHandlerObj = CloudHandler()
    let sinkManager = SinkManager.sharedInstance
    
    
    func getTypeOfAlert(category:CP_AlertTypeInfoTypeDef, alertID:Int)->String{
        var name:String = ""
        switch category {
        case NON_URGENT_ALERT:
            name = self.getNONUrgentAlertNameByID(alertID: alertID)
            break
        case URGENT_ALERT:
            name = self.getUrgentAlertNameByID(alertID: alertID)
            break
        case CRITICAL_ALERT:
            name = self.getCriticalAlertNameByID(alertID: alertID)
            break
        default:
            break
        }
        return name
    }
    
    //---Temporary assign some name change it once get it from cloud⚠️⚠️⚠️❗️‼️‼️‼️‼️
    func getNONUrgentAlertNameByID(alertID:Int)->String{
        var name:String = ""
        switch alertID {
        case 0:
            name = "Incoming Call"
            break
        case 1:
            name = "Low Bike Battery"
            break
        case 2:
            name = "Low DM Battery"
            break
        case 3:
            name = "Low Fuel"
            break
        case 4:
            name = "Fuel pump not available"
            break
        case 5:
            name = "Deviation"
            break
        default:
            break
        }
        return name
    }
    
    func getUrgentAlertNameByID(alertID:Int)->String{
        var name:String = ""
        switch alertID {
        case 0:
            name = AlertConstant.ALERT_TYPE_SEVERE_CONDITION
            break
        case 1:
            name = AlertConstant.ALERT_TYPE_PUNCTURE
            break
        case 2:
            name = AlertConstant.ALERT_TYPE_NO_FUEL
            break
        default:
            break
        }
        return name
    }
    
    func getCriticalAlertNameByID(alertID:Int)->String{
        var name:String = ""
        switch alertID {
        case 0:
            name = AlertConstant.ALERT_TYPE_ACCIDENT
            break
        case 1:
            name = "Bike Theft"
            break
        default:
            break
        }
        return name
    }

    func generateALert(alertType:String)
    {
        
        let bikerId:Int = MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID)
        let suburl = "\(RequestURL.URL_BIKER.rawValue)\(bikerId)\(RequestURL.URL_ALERTS.rawValue)"
        
        let parameteres:[String:Any] = [AlertConstant.PARAM_ALERT_TYPE:alertType,
                                        AlertConstant.PARAM_ALERT_TRIGGERED_BY:bikerId]
        
        
        print("ALERTS LOG : Generate Alert Api : \(parameteres)")
        cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter:parameteres, isheaderAUTH: true, requestType: .post){ response in
            
            if let json = response?.result.value {
                print("ALERTS LOG:- GENERATED ALERT DETAILS Alert Api \(json)")
                
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let alertModel = Mapper<AlertApiResponseModel>().map(JSON:json as! [String : Any])!
                    
                    let alertContext:AlertContext = AlertContext()
                    alertContext.changeStateToAlertSend(alertDetail: alertModel)
                    
                    let data:[String:Any] = [StringConstant.data:alertModel]
                    self.alertManagerDelegate?.generateAlertSuccess!(alertObj: data)
                }
                else
                {
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    func closeGeneratedAlert(generatedAlertId:Int){
        
        LoadingHandler.shared.startloading(interactionEnable: true, view: (UIApplication.topViewController()?.view)!)
        
        let bikerId:Int = MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID)
        
        print("ALERTS LOG : close alert with id Alert Api= \(generatedAlertId)")
        
        let parameters = [AlertConstant.PARAM_ALERT_HELPER_BIKER_IDS:[0]]
        
        let suburl = "\(RequestURL.URL_BIKER.rawValue)\(bikerId)\(RequestURL.URL_ALERTS_SUB.rawValue)\(generatedAlertId)\(RequestURL.URL_CLOSE_ALERT.rawValue)"
        
        
        cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter:parameters, isheaderAUTH: true, requestType: .post){ response in
            
            DispatchQueue.main.async{
                LoadingHandler.shared.stoploading()
            }
            if let json = response?.result.value {
                print("ALERTS LOG:  Alert Api\(json)")
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let alertModel = Mapper<AlertApiResponseModel>().map(JSON:json as! [String : Any])!
                    
                    let alertContext:AlertContext = AlertContext()
                    alertContext.changeStateToAlertGeneratedClosed(alertId: "\(generatedAlertId)")
                    
                    let data:[String:Any] = [StringConstant.data:alertModel]
                    self.alertManagerDelegate?.closeAlertSuccess!(alertObj: data)
                }
                else
                {
                    
                    let ErrorModelObj = Mapper<ErrorModel>().map(JSON:json as! [String : Any])
                    
                    if (ErrorModelObj?.errorCode == 160006) && (ErrorModelObj?.description == "Alert is already closed"){
                        let alertContext:AlertContext = AlertContext()
                        alertContext.changeStateToAlertGeneratedClosed(alertId: "\(generatedAlertId)")
                        
                        let data:[String:Any] = [StringConstant.data:""]
                        self.alertManagerDelegate?.closeAlertSuccess!(alertObj: data)
                    }else{
                        
                        let errorData:[String:Any] = ["data":ErrorModelObj ?? " "]
                        self.alertManagerDelegate?.onFailure!(Error: errorData)
                    }
                    //self.errorCall(value: json as! [String : Any])
                }            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    
    func sentAlertDeliveryAckToSender(uuid:String){
    
        let parameters:[String:Any] = [AlertConstant.PARAM_ACK:"true",
                                       AlertConstant.PARAM_ALERT_UUID:uuid]
        
    
        print("ALERTS LOG = Sent ack Alert Api \(parameters)")
        let suburl = "\(RequestURL.URL_NOTIFY_ALERT_DELIVERED.rawValue)"
        
        cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter:parameters, isheaderAUTH: true, requestType: .put){ response in
            
            if let json = response?.result.value {
                print("ALERTS LOG:- Alert Api\(json)")
                
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    self.alertManagerDelegate?.alertDeliveredAckSuccess!(message: "Done")
                }
                else
                {
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    
    
    }
    func respondToReceivedAlert(responseId:Int, generatedAlertId:String, uuid:String){
        
        LoadingHandler.shared.startloading(interactionEnable: true, view: (UIApplication.topViewController()?.view)!)
        
        let bikerId:Int = MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID)
        let parameters:[String:Any] = [AlertConstant.PARAM_STATUS:responseId,
                                       AlertConstant.PARAM_ALERT_UUID:uuid]
        
        print("ALERTS LOG:- Respond to Alert Api param = \(parameters)")
        print("ALERTS LOG:- Respond to Alert Api generatedAlertId = \(generatedAlertId)")
        
        let suburl = "\(RequestURL.URL_BIKER.rawValue)\(bikerId)\(RequestURL.URL_ALERTS_SUB.rawValue)\(generatedAlertId)\(RequestURL.URL_RESPOND_TO_ALERT.rawValue)"
        
        
        cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter:parameters, isheaderAUTH: true, requestType: .post){ response in
            
            DispatchQueue.main.async{
                LoadingHandler.shared.stoploading()
            }
            
            if let json = response?.result.value {
                print("ALERTS LOG:- Alert Api\(json)")
                
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let alertModel = Mapper<AlertApiResponseModel>().map(JSON:json as! [String : Any])!
                    
                    if responseId == AlertConstant.ALERT_RESPONSE_STATUS_ACKNOWLEDGED{
                        let alertContext:AlertContext = AlertContext()
                        alertContext.changeStateToAlertAcknowledge(alertDetail: alertModel)
                        
                    }else{
                        
                        let alertContext:AlertContext = AlertContext()
                        alertContext.changeStateToAlertDiscard(alertId: Int32(alertModel.alert_id!))
                    }
                    self.alertManagerDelegate?.alertRespondSuccess!(message:"Done")
                }else {
                    let ErrorModelObj = Mapper<ErrorModel>().map(JSON:json as! [String : Any])
                    
                    if (ErrorModelObj?.errorCode == 160006) && (ErrorModelObj?.description == "Alert is already closed"){
                        let alertContext:AlertContext = AlertContext()
                        alertContext.changeStateToAlertStateReceivedClosed(alertId: generatedAlertId)
                       
                        self.alertManagerDelegate?.alertRespondSuccess!(message:"Alert is already closed")
                    }else{
                        
                        let errorData:[String:Any] = ["data":ErrorModelObj ?? " "]
                        self.alertManagerDelegate?.onFailure!(Error: errorData)
                    }
                }
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    
    
    func errorCall(value:[String : Any]!){
        let ErrorModelObj = Mapper<ErrorModel>().map(JSON:value)
        let errorData:[String:Any] = ["data":ErrorModelObj ?? " "]
        self.alertManagerDelegate?.onFailure!(Error: errorData)
    }
    
}
