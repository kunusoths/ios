//
//  ImageUploader.swift
//  Maximus
//
//  Created by Namdev Jagtap on 03/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class ImageUploader: NSObject {
   
    var cloudHandler = CloudHandler()
    
    func uploadImage(data:Data, url:String, successCallback: @escaping successData, failureCallback: @escaping errorData){
        
        let name = "imagefile"
        let fileName = "image.jpeg"
        let mimetype = "image/jpeg"
        
//        print("url", url)
        
        cloudHandler.uploadMultiPart(url: url, data: data, name: name, filename: fileName, mimetype: mimetype)
        {   response in
            
            if let json = response.result.value {
                let statusCode = response.response?.statusCode
                print("JSON ",json)
                if (statusCode)! >= 200 && (statusCode)! < 300
                {
                    let imageUpload = Mapper<ImageUploadModel>().map(JSON:json as! [String : Any])!
                    let data:[String:Any] = [StringConstant.data:imageUpload]
//                    self.imageUploadDelegate?.uploadImageSucess!(data: data)
                    successCallback(data)
                } else {
                    print("Image upload error: \(json as! [String : Any])")
//                    self.imageUploadDelegate?.uploadImageFailed!(Error: (json as! [String : Any]))
                    failureCallback((json as! [String : Any]))
                }
            }else{
//                self.imageUploadDelegate?.uploadImageFailed!(Error: RequestTimeOut().json)
                failureCallback(RequestTimeOut().json)
            }
        }
    }
    
//    //TODO:Error Call Handling Method
//    func errorCall(value:[String : Any]!){
//
//        let ErrorModelObj = Mapper<ErrorModel>().map(JSON:value)
//        let errorData:[String:Any] = [StringConstant.data:ErrorModelObj ?? " "]
//        self.imageUploadDelegate?.uploadImageFailed!(Error: errorData)
//
//    }
}
