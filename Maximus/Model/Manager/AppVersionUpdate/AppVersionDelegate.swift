
//
//  AppVersionDelegate.swift
//  Maximus
//
//  Created by Admin on 27/12/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//

import Foundation

/// MARK - UpdateType

/// `UpdateType` defines what kind of update is available
/// It is used as parameter if user wants to use
/// custom alert to inform the user about an update.
///
/// - major: Major release available: A.b.c.d
/// - minor: Minor release available: a.B.c.d
/// - patch: Patch release available: a.b.C.d
/// - revision: Revision release available: a.b.c.D
/// - unknown: No information available about the update
public enum UpdateType: String {
    case major
    case minor
    case patch
    case revision
    case unknown
}

// MARK: - AppversionDelegate Protocol

/// Delegate that handles all codepaths for appversion upon version check completion.
public protocol AppVersionDelegate: NSObjectProtocol {
    /// User presented with update dialog.
    func appversionDidShowUpdateDialog(alertType: AppVesrionManager.AlertType)

    /// User did click on button that launched App Store.app.
    func appversionUserDidLaunchAppStore()

    /// User did click on button that skips version update.
    func appversionUserDidSkipVersion()

    /// User did click on button that cancels update dialog.
    func appversionUserDidCancel()

    /// Appversion failed to perform version check (may return system-level error).
    func appversionDidFailVersionCheck(error: Error)

    /// Appversion performed version check and did not display alert.
    func appversionDidDetectNewVersionWithoutAlert(message: String, updateType: UpdateType)

    /// Appversion performed version check and latest version is installed.
    func appversionLatestVersionInstalled()
}

// MARK: - AppversionDelegate Protocol Extension

public extension AppVersionDelegate {

    func appversionDidShowUpdateDialog(alertType: AppVesrionManager.AlertType) {
        printMessage()
    }

    func appversionUserDidLaunchAppStore() {
        printMessage()
    }

    func appversionUserDidSkipVersion() {
        printMessage()
    }

    func appversionUserDidCancel() {
        printMessage()
    }

    func appversionDidFailVersionCheck(error: Error) {
        printMessage()
    }

    func appversionDidDetectNewVersionWithoutAlert(message: String, updateType: UpdateType) {
        printMessage()
    }

    func appversionLatestVersionInstalled() {
        printMessage()
    }

    private func printMessage(_ function: String = #function) {
        print("The default implementation of \(function) is being called. You can ignore this message if you do not care to implement this method in your `appversionDelegate` conforming structure.")
    }

}
