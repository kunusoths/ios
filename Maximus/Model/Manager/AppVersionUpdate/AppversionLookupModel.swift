//
//  AppversionLookupModel.swift
//  Maximus
//
//  Created by Admin on 27/12/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//



import Foundation

// MARK: - Model representing a selection of results from the iTunes Lookup API

struct AppversionLookupModel: Decodable {
    private enum CodingKeys: String, CodingKey {
        case results
    }

    let results: [Results]

    struct Results: Decodable {
        private enum CodingKeys: String, CodingKey {
            case appID = "trackId"
            case currentVersionReleaseDate
            case minimumOSVersion = "minimumOsVersion"
            case version
        }

        let appID: Int
        let currentVersionReleaseDate: String
        let minimumOSVersion: String
        let version: String
    }
}
