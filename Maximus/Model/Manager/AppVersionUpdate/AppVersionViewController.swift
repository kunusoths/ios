//
//  AppVersionViewController.swift
//  Maximus
//
//  Created by Admin on 27/12/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//


import Foundation
import UIKit

// MARK: - UIViewController Extension 

final class AppVersionViewController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle { return UIApplication.shared.statusBarStyle }
}
