
//
//  AppVersionAlertMessaging.swift
//  Maximus
//
//  Created by Admin on 27/12/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//

import Foundation

// MARK: -  Alert Messaging Customization

/// Allows the overriding of all the `UIAlertController` and `UIActionSheet` Strings to which AppversionDefaults.
///
/// - Warning: Overriding any of these keys will result in the loss of the built-in internationalization that appversion provides.
///
/// As AppversionAlertMessaging is a Struct, one _or_ more keys can be modified. Overriding only one string will result in the other keys retaining their default (and internationalizable) values.
public struct AppVersionAlertMessaging {

    public struct Constants {
        public static let nextTime      = "Remind Me Later"//"Next time"
        public static let skipVersion   = "Skip This Version"
        public static let updateMessage = "%@ %@ is now available. You have %@. Would you like to update it now?"
        public static let updateTitle   = "A new version of MaximusPro app is available!"
        public static let updateNow     = "Install Updates"//"Update"
    }

    let nextTimeButtonMessage: String
    let skipVersionButtonMessage: String
    let updateButtonMessage: String
    let updateMessage: String
    let updateTitle: String

    /// The public initializer
    ///
    /// - Parameters:
    ///   - title: The title field of the `UIAlertController`.
    ///   - message: The `message` field of the `UIAlertController`.
    ///   - updateButtonMessage: The `title` field of the Update Button `UIAlertAction`.
    ///   - nextTimeButtonMessage: The `title` field of the Next Time Button `UIAlertAction`.
    ///   - skipVersionButtonMessage: The `title` field of the Skip Button `UIAlertAction`.
    public init(updateTitle title: String = Constants.updateTitle,
                updateMessage message: String = Constants.updateMessage,
                updateButtonMessage: String = Constants.updateNow,
                nextTimeButtonMessage: String = Constants.nextTime,
                skipVersionButtonMessage: String = Constants.skipVersion) {
        self.updateTitle = title
        self.nextTimeButtonMessage = nextTimeButtonMessage
        self.updateButtonMessage = updateButtonMessage
        self.updateMessage = message
        self.skipVersionButtonMessage = skipVersionButtonMessage
    }

}
