//
//  SocialNetworkModel.swift
//  Maximus
//
//  Created by Admin on 16/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation

//******************** Social Network Responce Model **************
class SocialNetworkModel {
    var name        = String()
    var firstname   = String()
    var lastname    = String()
    var email       = String()
    var profilePic  = String()
    var socialID    = String()
    var socialAccesToken = String()
    var type        = String()
    
}

class RegistrationModel {
    var fullName = String()
    var email = String()
    var password = String()
}

enum SignUpModel {
    case SocialNetworkModel
    case RegistrationModel
}
