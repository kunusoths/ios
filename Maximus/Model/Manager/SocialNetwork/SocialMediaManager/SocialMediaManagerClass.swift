//
//  SocialMediaManagerClass.swift
//  Maximus
//
//  Created by Admin on 16/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import UIKit
import FBSDKLoginKit

//MARK: *********** Common Responce Call Back ****************
protocol SNSResponseDelegate {
    func onSocialSuccess(objModel:SocialNetworkModel)
    func onSocialFailure(response:Error)
}

//MARK: ***********  Social Network Service Common Actions ***********
protocol SNSActions {
    func setDelegate(delegate:SNSResponseDelegate)
    func login(viewcontroller:UIViewController)
    func logout()
}

//MARK: ************  FaceBook Class ***********
class FacebookClass : SNSActions {
    
    var snsResponseDelegate:SNSResponseDelegate?
    
    init(delegate:SNSResponseDelegate) {
        snsResponseDelegate = delegate
    }
    
    internal func setDelegate(delegate:SNSResponseDelegate ) {
        snsResponseDelegate = delegate
    }
    
    func login(viewcontroller:UIViewController) {
        
        FBSDKLoginManager().logIn(withReadPermissions: ["email","public_profile"], from: viewcontroller) { (result, err) in
            if err != nil {
                print("Custom FB Login failed:", err ?? "Data Missing")
                self.snsResponseDelegate?.onSocialFailure(response: err!)
                return
            }
            
            FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start { (connection, result, err) in
                
                if err != nil {
                    print("Failed to start graph request:", err ?? "Data Missing")
                    self.snsResponseDelegate?.onSocialFailure(response: err!)
                    return
                }
                
                print(FBSDKAccessToken.current().tokenString)
                
                let response = result as! [String : AnyObject]
                let pictureData:[String:Any] = response["picture"] as! [String: Any]
                let urlData:[String:Any] = pictureData["data"] as! [String: Any]

                let url = urlData["url"] as! String
   
                let objModel = SocialNetworkModel()
                objModel.firstname    = (response["first_name"] as? String) ?? ""
                objModel.socialID   = (response["id"] as? String) ?? ""
                objModel.lastname     = (response["last_name"] as? String) ?? ""
                objModel.name         = (response["name"] as? String) ?? ""
                
                
                objModel.socialAccesToken = FBSDKAccessToken.current().tokenString

                let pictureUrl          = "https://graph.facebook.com/"+objModel.socialID+"/picture" //pictDict?["data"] as? NSDictionary
                objModel.profilePic   = pictureUrl //(pictureUrl?["url"] as? String) ?? ""
                objModel.type         = "facebook"
               
                if let email:String = response["email"] as! String? {
                    objModel.email = email
                }else{
                    objModel.email = objModel.socialID+"@facebook.com"///Form Email from socialID
                }
                
                MyDefaults.setData(value: url, key: DefaultKeys.KEY_PROFILE_URL)
                MyDefaults.setData(value: objModel.firstname, key: DefaultKeys.KEY_FIRST_NAME)
                MyDefaults.setData(value: objModel.lastname, key: DefaultKeys.KEY_LAST_NAME)

                MyDefaults.setData(value: objModel.name, key: DefaultKeys.KEY_USERNAME)
                MyDefaults.setData(value: objModel.socialAccesToken, key: DefaultKeys.KEY_FBACCESSTOKEN)
                
                MyDefaults.setData(value: objModel.email, key: DefaultKeys.KEY_BIKER_EMAIL)
                
                self.snsResponseDelegate?.onSocialSuccess(objModel: objModel)
            }
        }
    }
    
//    objModel.socialAccesToken,
//    StringConstant.ID          :objModel.socialID,
//    StringConstant.username    :objModel.email]
    func logout() {
        let loginManager = FBSDKLoginManager()
        loginManager.logOut() // this is an instance function
        print("Facebook Logout Successful")
    }
}

//MARK: ************  Tweeter Class ***********
class TwitterClass : SNSActions {
    var snsResponseDelegate:SNSResponseDelegate?
    
    internal func setDelegate(delegate: SNSResponseDelegate) {
        snsResponseDelegate = delegate
    }
    
    func login(viewcontroller:UIViewController) {
        print("Tweeter Login Called")
    }
    
    func logout() {
        print( "Tweeter Logout called")
    }
}

//MARK: ************  Google Plus Class ***********
class GooglePlusClass : SNSActions {
    var snsResponseDelegate:SNSResponseDelegate?
    
    internal func setDelegate(delegate: SNSResponseDelegate) {
        snsResponseDelegate = delegate
    }
    
    func login(viewcontroller:UIViewController)  {
        print("GooglePlus Login Called")
    }
    
    func logout()  {
        print( "GooglePlus Logout Called")
    }
}

//MARK: *********** Email **********
class EmailClass: SNSActions {
    var snsResponseDelegate:SNSResponseDelegate?

    func setDelegate(delegate: SNSResponseDelegate) {
        snsResponseDelegate = delegate
    }
    
    func login(viewcontroller: UIViewController) {
        print("GooglePlus Login Called")
    }
    
    func logout() {
        print("GooglePlus Login Called")
    }
    
    init(delegate:SNSResponseDelegate) {
        snsResponseDelegate = delegate
    }
    
}

//MARK: ********** Created Enum For Constant Social Login Service  *********
enum Social {
    case facebook, twitter, googlePlus, email
}


//MARK: ********** Factory Design Pattern  ************
class SocialMediaFactory {
    
    static func SocialLoginService(for type:Social , delegate:SNSResponseDelegate) -> SNSActions? {
        
        switch type {
        case .facebook :
            return FacebookClass(delegate: delegate)
        case .twitter :
            return TwitterClass()
        case .googlePlus:
            return GooglePlusClass()
        case .email:
            return EmailClass(delegate: delegate)
        }
    }
}

