//
//  PlannedWavePointsModel.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 26/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import ObjectMapper
import GoogleMaps

class PlannedWavePointsModel: Mappable {
    var WavePointsList    :[WavePointsList]?
    
//    required init?(map: Map) {
//        
//    }
    
    init() {}
    required convenience init?(map: Map) { self.init() }
    

    func mapping(map: Map) {
        WavePointsList        <- map["WavePointsList"]
    }

}

class WavePointsList:Mappable
{
    var id          :intmax_t?
    var poi_id      :String?
    var name        :String?
    var location    :Location?
    var categories     :[String]?
    var route_order    :Int?
    var date_added     :String?
    var types          :[String]?
    var ride_id        :intmax_t?
    var distanceFromSource :CLLocationDistance?
    var cllLocation   :CLLocation?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        id              <- map["id"]
        poi_id          <- map["poi_id"]
        name            <- map["name"]
        location        <- map["location"]
        categories      <- map["categories"]
        route_order     <- map["route_order"]
        date_added      <- map["date_added"]
        types           <- map["types"]
        ride_id         <- map["ride_id"]
        distanceFromSource <- map["distanceFromSource"]
        cllLocation <- map["cllLocation"]
    }
}

