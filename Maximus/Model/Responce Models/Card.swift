/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct Card : Mappable {
	var score : Double?
	var sticky : Bool?
	var actions : [Actions]?
	var card_id : String?
	var visuals : Visuals?
	var biker_id : Int?
	var priority : Int?
	var item_type : String?
	var allow_dismiss : Bool?
	var date_generated : String?
	var date_published : String?
	var destination_id : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		score <- map["score"]
		sticky <- map["sticky"]
		actions <- map["actions"]
		card_id <- map["card_id"]
		visuals <- map["visuals"]
		biker_id <- map["biker_id"]
		priority <- map["priority"]
		item_type <- map["item_type"]
		allow_dismiss <- map["allow_dismiss"]
		date_generated <- map["date_generated"]
		date_published <- map["date_published"]
		destination_id <- map["destination_id"]
	}

}
