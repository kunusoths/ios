//
//  DeviceDataModel.swift
//  Maximus
//
//  Created by Admin on 21/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper

class DeviceDataResponse: NSObject,Mappable {
    
    var biker_id            : Int?
    var latest_data         : [LatestData]?
    
    
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        biker_id            <- map["biker_id"]
        latest_data         <- map["latest_data"]
    }
}

class  LatestData: NSObject,Mappable {
    
    var mode_of_operation           : Int?
    var ignition_status             : Int?
    var msg_type                    : String?
    var current_gsm_2_3_nb          : Int?
    var unit_io_status_byte_1       : Int?
    var unit_io_status_byte_2       : Int?
    var unit_io_status_byte_3       : Int?
    var unit_io_status_byte_4       : Int?
    var unit_id                     : Int?
    var unit_status_current_gsm_1nb : Int?
    var utc_time_sec                : Int?
    var raw_packet                  : String?
    var vehicle_movement_status     : Int?
    var unit_hw_ver                 : Int?
    var msg_num                     : Int?
    var ground_speed                : Int?
    var gps_validity                : Int?
    var altitude                    : Int?
    var trans_reason_data           : Int?
    var analog_ip_2                 : Int?
    var analog_ip_3                 : Int?
    var header_4                    : Int?
    var header_2                    : Int?
    var analog_ip_1                 : Int?
    var header_3                    : Int?
    var analog_ip_4                 : Int?
    var header_1                    : String?
    var multipurpose_field          : String?
    var unit_sw_ver                 : Int?
    var location_status             : Int?
    var packettimestamp             : String?
    var receivedtimestamp           : String?
    var latitude                    : Double?
    var error_detection_code        : Int?
    var utc_time_hour               : Int?
    var utc_time_min                : Int?
    var imei                        : String?
    var gps_date_year               : Int?
    var current_gsm_4_5_nb          : Int?
    var mileage_counter             : Int?
    var utc_date_month              : Int?
    var trans_reason                : Int?
    var utc_date_day                : Int?
    var num_staellites              : Int?
    var protocol_ver_id             : Int?
    var longitude                   : Double?
    var date_part                   : String?
    var gps_mode_1                  : Int?
    var gps_mode_2                  : Int?
    var comm_ctrl                   : Int?

   
    
    
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        mode_of_operation           <- map["mode_of_operation"]
        ignition_status             <- map["ignition_status"]
        msg_type                    <- map["msg_type"]
        current_gsm_2_3_nb          <- map["current_gsm_2_3_nb"]
        unit_io_status_byte_1       <- map["unit_io_status_byte_1"]
        unit_io_status_byte_2       <- map["unit_io_status_byte_2"]
        unit_io_status_byte_3       <- map["unit_io_status_byte_3"]
        unit_io_status_byte_4       <- map["unit_io_status_byte_4"]
        unit_id                     <- map["unit_id"]
        unit_status_current_gsm_1nb <- map["unit_status_current_gsm_1nb"]
        utc_time_sec                <- map["utc_time_sec"]
        raw_packet                  <- map["raw_packet"]
        vehicle_movement_status     <- map["vehicle_movement_status"]
        unit_hw_ver                 <- map["unit_hw_ver"]
        msg_num                     <- map["msg_num"]
        ground_speed                <- map["ground_speed"]
        gps_validity                <- map["gps_validity"]
        altitude                    <- map["altitude"]
        trans_reason_data           <- map["trans_reason_data"]
        analog_ip_2                 <- map["analog_ip_2"]
        analog_ip_3                 <- map["analog_ip_3"]
        header_4                    <- map["header_4"]
        header_2                    <- map["header_2"]
        analog_ip_1                 <- map["analog_ip_1"]
        header_3                    <- map["header_3"]
        analog_ip_4                 <- map["analog_ip_4"]
        header_1                    <- map["header_1"]
        multipurpose_field          <- map["multipurpose_field"]
        unit_sw_ver                 <- map["unit_sw_ver"]
        location_status             <- map["location_status"]
        packettimestamp             <- map["packettimestamp"]
        receivedtimestamp           <- map["receivedtimestamp"]
        latitude                    <- map["latitude"]
        error_detection_code        <- map["error_detection_code"]
        utc_time_hour               <- map["utc_time_hour"]
        utc_time_min                <- map["utc_time_min"]
        imei                        <- map["imei"]
        gps_date_year               <- map["gps_date_year"]
        header_1                    <- map["header_1"]
        current_gsm_4_5_nb          <- map["current_gsm_4_5_nb"]
        mileage_counter             <- map["mileage_counter"]
        utc_date_month              <- map["utc_date_month"]
        trans_reason                <- map["trans_reason"]
        utc_date_day                <- map["utc_date_day"]
        num_staellites              <- map["num_staellites"]
        protocol_ver_id             <- map["protocol_ver_id"]
        longitude                   <- map["longitude"]
        date_part                   <- map["date_part"]
        gps_mode_1                  <- map["gps_mode_1"]
        gps_mode_2                  <- map["gps_mode_2"]
        comm_ctrl                   <- map["comm_ctrl"]
    }
}
