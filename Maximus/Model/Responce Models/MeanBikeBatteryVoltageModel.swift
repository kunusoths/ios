//
//  MeanBikeBatteryVoltageModel.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 10/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper

class MeanBikeBatteryVoltageModel: Mappable {
    
    var mean_bike_battery_ignition_off_voltage:String
    var mean_bike_battery_ignition_on_voltage:String
    
    required init?(map: Map) {
        mean_bike_battery_ignition_off_voltage = "0.0"
        mean_bike_battery_ignition_on_voltage = "0.0"
    }
    
    func mapping(map: Map) {
        mean_bike_battery_ignition_off_voltage <- map["mean_bike_battery_ignition_off_voltage"]
        mean_bike_battery_ignition_on_voltage  <- map["mean_bike_battery_ignition_on_voltage"]
    }

}
