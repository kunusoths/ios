//
//  RideStatistics.swift
//  Maximus
//
//  Created by Sriram G on 22/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper

class RideStatisticsModel:Mappable
{
    var id                  :intmax_t?
    var BikerId             :intmax_t?
    var RideId              :intmax_t?
    var averageSpeed        :Double?
    var distanceTravelled   :Double?
    var topSpeed            :Double?
    var distance            :intmax_t?
    var timeDuration        :Int?
    var job_id              :intmax_t?
    var unit_id             :intmax_t?
    var last_processed_time :String?

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                  <- map["_id"]
        job_id              <- map["job_id"]
        unit_id             <- map["unit_id"]
        last_processed_time <- map["last_processed_time"]
        averageSpeed        <- map["average_speed"]
        BikerId             <- map["biker_id"]
        RideId              <- map["ride_id"]
        distance            <- map["distance"]
        timeDuration        <- map["time_duration"]
        distanceTravelled   <- map["distance_travelled"]
        topSpeed            <- map["top_speed"]
    }
}
