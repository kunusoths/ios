/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct ActionData : Mappable {
	var source : Source?
	var destination : Destination?
    var via_points : [ViaPoint]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		source <- map["source"]
		destination <- map["destination"]
        via_points <- map["via_points"]
	}

}

struct ViaPoint : Mappable {
    var lat : Double?
    var lng : Double?
    var city : String?
    var image : String?
    var order : Int?
    var state : String?
    var rating:Double?
    var category:String?
    var full_address:String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        lat <- map["lat"]
        lng <- map["lng"]
        city <- map["city"]
        image <- map["image"]
        order <- map["order"]
        state <- map["state"]
        rating <- map["rating"]
        category <- map["category"]
        full_address <- map["full_address"]
    }
}

