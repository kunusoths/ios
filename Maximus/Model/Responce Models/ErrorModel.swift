//
//  ErrorModel.swift
//  Maximus
//
//  Created by Sriram G on 27/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper

class ErrorModel:Mappable
{
    var description:String?
    var errorDescription:String?
    var errorCode:intmax_t?
    var fieldError:String?
    var message:String?
    var metadata:String?
    var authMetadata: String?
    var authenticationException: String?
    required init?(map: Map) {
        
         description = ""
        errorDescription = ""
         errorCode = 0
         fieldError = ""
         message = ""
         metadata = ""
        authMetadata = ""
        authenticationException = ""
    }
    
    func mapping(map: Map) {
        
        description         <- map["description"]
        errorDescription <- map["error_description"]
        errorCode           <- map["error_code"]
        fieldError          <- map["field_errors"]
        message             <- map["message"]
        metadata            <- map["metadata"]
        authMetadata <- map["authMetadata"]
        authenticationException <- map["AuthenticationException"]
    }
    
}
