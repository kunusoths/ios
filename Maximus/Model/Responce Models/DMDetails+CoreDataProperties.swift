//
//  DMDetails+CoreDataProperties.swift
//  
//
//  Created by Isha Ramdasi on 07/02/18.
//
//

import Foundation
import CoreData


extension DMDetails {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DMDetails> {
        return NSFetchRequest<DMDetails>(entityName: "DMDetails")
    }

    @NSManaged public var firmware_build_number: String?
    @NSManaged public var firmware_version: String?
    @NSManaged public var sr_no: String?
    @NSManaged public var isSynced: Bool

}
