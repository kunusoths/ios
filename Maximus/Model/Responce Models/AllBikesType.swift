//
//  File.swift
//  Maximus
//
//  Created by kpit on 25/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper



class BikeModelList: Mappable {
    /// This function can be used to validate JSON prior to mapping. Return nil to cancel mapping at this point
    
    
    var company         :String?
    var model           :[String]?
    
    required init?(map: Map) {
        
    }
    
    //mappable
    func mapping(map: Map) {
        company       <- map["company"]
        model         <- map["models"]
    }
}

class AllBikesList: Mappable {
    /// This function can be used to validate JSON prior to mapping. Return nil to cancel mapping at this point
    
//    var bikeList         :[AllBikesType]?
    var bikeList         :[BikeModelList]?
    
    required init?(map: Map) {
    
    }
    
    //mappable
    func mapping(map: Map) {
        bikeList         <- map["AllBikesList"]
    }
}


class AllBikesType: Mappable {
    /// This function can be used to validate JSON prior to mapping. Return nil to cancel mapping at this point
    
    var company         :String?
    var milage          :String?
    var model           :String?
    var id              :Int
    
    required init?(map: Map) {
        company     = ""
        milage      = ""
        model       = ""
        id          = 0
    }
    
    //mappable
    func mapping(map: Map) {
        company         <- map["company"]
        milage          <- map["milage"]
        model           <- map["model"]
        id              <- map["id"]
    }
}
