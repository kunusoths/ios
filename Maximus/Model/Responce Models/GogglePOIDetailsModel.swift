//
//  GogglePOIDetailsModel.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 19/06/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import ObjectMapper

class GogglePOIDetailsModel: Mappable {
    
    var result : GooglePOIDetail?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        result      <- map["result"]
    }
}

class GooglePOIDetail: Mappable{

    var formatted_address         :String?
    var formatted_phone_number    :String?
    var name                      :String?
    var opening_hours             :GoogleOpeningHoursModel?
    var rating                    :Double?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        formatted_address               <- map["formatted_address"]
        formatted_phone_number          <- map["formatted_phone_number"]
        name                            <- map["name"]
        opening_hours                   <- map["opening_hours"]
        rating                          <- map["rating"]
    }
}
class GoogleOpeningHoursModel: Mappable{

    var weekday_text :[String]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        weekday_text            <- map["weekday_text"]
    }

}
