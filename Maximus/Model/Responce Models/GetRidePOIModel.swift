//
//  GetRidePOIModel.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 23/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper
import GoogleMaps

class GetRidePOIModel: Mappable {
    var POIList    :[POIListResponse]?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        POIList        <- map["POIListResponse"]
    }
}

class POIListResponse:Mappable
{
    var wavePoint_id          :intmax_t?
    var name        :String? 
    var location    :Location?
    var categories     :[String]?
    var provider      :String?
    var poiId         :String?
    var phone_number  :String?
    var rating        :Double?
    var OpeninHours   :OpeningHours?
    var cllLocation   :CLLocation?
    var distanceFromSource :CLLocationDistance?
    var date_added : String?
    var types :[String]?
    
    
    
    init() {}
    required convenience init?(map: Map) { self.init() }



    func mapping(map: Map) {
        
        name            <- map["name"]
        location        <- map["location"]
        categories      <- map["categories"]
        provider        <- map["provider"]
        poiId           <- map["poi_id"]
        phone_number    <- map["phone_number"]
        rating          <- map["rating"]
        cllLocation     <- map["cllLoction"]
        distanceFromSource <- map["distanceFromSource"]
        date_added <- map["date_added"]
        types <- map["types"]
        wavePoint_id <- map["wavePoint_id"]
    }
}

class OpeningHours:Mappable{
    
    var weekday_text : [String]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        weekday_text            <- map["weekday_text"]
    }
}

class Location:Mappable
{
    var latitude            :Double?
    var longitude           :Double?
    var altitude            :Double?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        latitude                <- map["latitude"]
        longitude               <- map["longitude"]
        altitude                <- map["altitude"]
        
    }
}
