//
//  AppDetailsModel.swift
//  Maximus
//
//  Created by Isha Ramdasi on 07/02/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import ObjectMapper
class AppDetailsModel: NSObject, Mappable {
    var dm_firmware_build_number: String?
    var dm_firmware_version: String?
    var dm_state: String?
    var history: [DMHistory]?
    var id: Int?
    var passcode:String?
    var sku_code: String?
    var sr_no: String?
    var state_history: [StateHistory]?
    var tags: [String]?
    var unit_id: String?
    var vehicle_id: Int?
    var reason: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        dm_firmware_build_number                  <- map["dm_firmware_build_number"]
        dm_firmware_version                <- map["dm_firmware_version"]
        dm_state          <- map["dm_state"]
        history          <- map["history"]
        id               <- map["id"]
        passcode          <- map["passcode"]
        sku_code       <- map["sku_code"]
        sr_no  <- map["sr_no"]
        reason                  <- map["reason"]
        tags              <- map["tags"]
        unit_id              <- map["unit_id"]
        vehicle_id              <- map["vehicle_id"]
        state_history              <- map["state_history"]
    }
}

class DMHistory: Mappable {
    var dm_firmware_build_no: String?
    var dm_firmware_version: String?
    var message: String?
    var reason: String?
    var timestamp: String?
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        dm_firmware_build_no                  <- map["dm_firmware_build_no"]
        dm_firmware_version                <- map["dm_firmware_version"]
        message          <- map["message"]
        reason          <- map["reason"]
        timestamp               <- map["timestamp"]
    }
}

class StateHistory: Mappable {
    var current_state: String?
    var date_time: String?
    var previous_state: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        current_state                  <- map["current_state"]
        date_time                <- map["date_time"]
        previous_state          <- map["previous_state"]
    }
}

class SystemDetails: Mappable {
    var app_uuid: String?
    var app_version: String?
    var date_deregistered: String?
    var date_registered: String?
    var device_uuid: String?
    var extra_data: String?
    var make: String?
    var model: String?
    var os_security_patch_level: String?
    var os_type: String?
    var os_version: String?
    var ram: Int?
    var resolution: String?
    var sku: String?
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        app_uuid                  <- map["app_uuid"]
        app_version                <- map["app_version"]
        date_registered          <- map["date_registered"]
        device_uuid          <- map["device_uuid"]
        make          <- map["make"]
        model          <- map["model"]
        os_type          <- map["os_type"]
        os_version          <- map["os_version"]
    }
}
