//
//  RideBadgeInformation.swift
//  Maximus
//
//  Created by Sriram G on 22/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper


class HonorStickerResponse:Mappable
{
    var HonorBadgeList    :[HonorBadgeListResponse]?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        HonorBadgeList        <- map["HonorBadgeList"]
    }
}

class HonorBadgeListResponse:Mappable
{
    var badge_id        :Int?
    var biker_id        :Int?
    var category_id     :Int?
    var primary         :Bool?
    var results         :String?
    var ride_id         :Int?
    var timestamp       :String?
    var _id             :IdModel?
    var images          :[Images]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        badge_id        <- map["badge_id"]
        biker_id        <- map["biker_id"]
        category_id     <- map["category_id"]
        primary         <- map["primary"]
        results         <- map["results"]
        ride_id         <- map["ride_id"]
        timestamp       <- map["timestamp"]
        _id             <- map["_id"]
        images          <- map["images"]
    }
}

class IdModel:Mappable
{
    var counter             :Int?
    var date                :String?
    var machineIdentifier   :Int?
    var processIdentifier   :Int?
    var time                :Int?
    var timeSecond          :Int?
    var timestamp           :Int?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        counter                 <- map["counter"]
        date                    <- map["date"]
        machineIdentifier       <- map["machineIdentifier"]
        processIdentifier       <- map["processIdentifier"]
        time                    <- map["time"]
        timeSecond              <- map["timeSecond"]
        timestamp               <- map["timestamp"]
    }
}
