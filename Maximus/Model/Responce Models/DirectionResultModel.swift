//
//  DirectionResultModel.swift
//  Maximus
//
//  Created by Sriram G on 31/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper

class DirectionDetails:Mappable
{
    var directionResult:RouteDetails?
    
    required init?(map: Map) {
    }
    func mapping(map: Map){
        directionResult    <- map["directions_result"]
    }
}

class RouteDetails:Mappable
{
    
    var geocodedWaypoints   :[GeocodedWaypoints]?
    var routeList           : [RouteList]?
    var status              : String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map){
        geocodedWaypoints   <- map["geocoded_waypoints"]
        routeList           <- map["routes"]
        status              <- map["status"]

    }
}

class SingleRouteDetails:Mappable
{
    var route: String?
    var distance: NSInteger?
    var tbt_route: String?
    var id: NSInteger?
    var biker:[Biker]?
    var waypointList: [WavePointsList]?
    var users_joined: NSInteger?
    var planned: Bool?
    
    init() {
        
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map){
        route           <- map["route"]
        distance        <- map["distance"]
        tbt_route       <- map["tbt_route"]
        id <- map["id"]
        biker <- map["biker"]
        waypointList <- map["waypoints"]
        users_joined <- map["users_joined"]
        planned <- map["planned"]
    }
}

class GeocodedWaypoints:Mappable{
    
    var geocoderStatus:String?
    var placeId:String?
    var types:[String]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        geocoderStatus  <- map["geocoder_status"]
        placeId         <- map["place_id"]
        types           <- map["types"]
    }
}

class RouteList:Mappable
{
    var bounds:Bounds?
    var copyright:String?
    var legs:[Legs]?
    var totalPolylinePoints:OverviewPolylinePoints?
    var summary:String?
    var warnings:[Any]?
    var waypoints:[Any]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        bounds              <- map["bounds"]
        copyright           <- map["copyrights"]
        legs                <- map["legs"]
        totalPolylinePoints <- map["overview_polyline"]
        summary             <- map["summary"]
        warnings            <- map["warnings"]
        waypoints           <- map["waypoint_order"]
    }
}


class Bounds:Mappable
{
    var northeastObj:Northeast?
    var southeastObj:Southeast?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        northeastObj <- map["northeast"]
        southeastObj <- map["southwest"]
    }
}

class Northeast:Mappable
{
    var lat:Double?
    var lng:Double?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        lat <- map["lat"]
        lng <- map["lng"]
        
    }
}

class Southeast:Mappable
{
    var lat:Double?
    var lng:Double?
    
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        lat <- map["lat"]
        lng <- map["lng"]
    }
}

class Legs:Mappable
{
    var distance:Distance?
    var duration:Duration?
    var startLocation:StartLocation?
    var endLocation:EndLocation?
    var startAddress:String?
    var endAddress:String?
    var steps:[Steps]?
    var traffic_speed_entry:[Any]?
    var via_waypoint:[Any]?
    
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        distance        <- map["distance"]
        duration        <- map["duration"]
        startLocation   <- map["start_location"]
        endLocation     <- map["end_location"]
        startAddress    <- map["start_address"]
        endAddress      <- map["end_address"]
        steps           <- map["steps"]
        traffic_speed_entry <- map["traffic_speed_entry"]
        via_waypoint    <- map["via_waypoint"]
    }
}

class Distance:Mappable
{
    var text:String?
    var value:Int?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        text <- map["text"]
        value <- map["value"]
    }
}

class Duration:Mappable
{
    var text:String?
    var value:Int?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        text <- map["text"]
        value <- map["value"]
    }
}

class EndLocation:Mappable
{
    var lat:Double?
    var lng:Double?
    
    init() {}
    required convenience init?(map: Map) { self.init() }

    
    func mapping(map: Map) {
        lat <- map["lat"]
        lng <- map["lng"]
    }
}

class StartLocation:Mappable
{
    var lat:Double?
    var lng:Double?
    
    init() {}
    required convenience init?(map: Map) { self.init() }

    func mapping(map: Map) {
        lat <- map["lat"]
        lng <- map["lng"]
    }
}

class Steps: Mappable
{
    var stepDistance:StepDistance?
    var stepDuration:StepDuration?
    var stepEndLoc:StepEndLocation?
    var stepStartLoc:StepStartLocation?
    var polylinePoints:StepPolyline?
    var htmlInstruction:String?
    var travelMode:String?
    var maneuver:String?
    
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        stepDistance        <- map["distance"]
        stepDuration        <- map["duration"]
        stepStartLoc        <- map["start_location"]
        stepEndLoc          <- map["end_location"]
        htmlInstruction     <- map["html_instructions"]
        polylinePoints      <- map["polyline"]
        travelMode          <- map["travel_mode"]
        maneuver            <- map["maneuver"]
    }
}

class StepDistance:Mappable
{
    var text:String?
    var value:intmax_t?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        text <- map["text"]
        value <- map["value"]
    }
}

class StepDuration:Mappable
{
    var text:String?
    var value:intmax_t?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        text <- map["text"]
        value <- map["value"]
    }
}

class StepEndLocation:Mappable
{
    var lat:Double?
    var lng:Double?
    
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        lat <- map["lat"]
        lng <- map["lng"]
    }
}

class StepStartLocation:Mappable
{
    var lat:Double?
    var lng:Double?
    
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        lat <- map["lat"]
        lng <- map["lng"]
    }
}

class StepPolyline:Mappable
{
    var points:String?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        points <- map["points"]
    }
}

class OverviewPolylinePoints: Mappable
{
    var points:String?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        points <- map["points"]
    }
}
