//
//  RideUpdateResponseModel.swift
//  Maximus
//
//  Created by Isha Ramdasi on 09/03/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import ObjectMapper
import Foundation

class RideUpdateResponseModel: Mappable {
    
    
    var created_at:String?
    var biker_id:Int?
    var message:String?
    var uuid:String?
    var type:String?
    var ride_id:String?

    required init?(map: Map) {
        
    }
    

    func mapping(map: Map) {
        created_at                  <- map["created_at"]
        biker_id                <- map["biker_id"]
        message          <- map["message"]
        uuid          <- map["uuid"]
        type               <- map["type"]
        ride_id          <- map["ride_id"]
    }
}
