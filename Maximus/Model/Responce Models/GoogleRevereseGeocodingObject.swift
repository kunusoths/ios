//
//  GoogleRevereseGeocodingObject.swift
//  Maximus
//
//  Created by Abhilash G on 6/23/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper



class GoogleRevereseGeocodingObject: Mappable {
    
    var result : [GooglePOIDetail2]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        result      <- map["results"]
    }
}



class GooglePOIDetail2: Mappable{
    
    var formatted_address         :String?
    var types:[String]?

    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        formatted_address               <- map["formatted_address"]
        types   <- map["types"]
    }
}


