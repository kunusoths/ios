//
//  DynamicLinkShareModel.swift
//  Maximus
//
//  Created by Namdev Jagtap on 20/04/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import ObjectMapper

class DynamicLinkShareModel: Mappable {

    var previewLink:String?
    var shortLink:String?
    
    required init?(map: Map) {
        previewLink = ""
        shortLink = ""
    }
    
    func mapping(map: Map) {
        previewLink          <- map["previewLink"]
        shortLink            <- map["shortLink"]
    }
}
