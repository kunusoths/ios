//
//  ActiveDevicesModel.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 16/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper

class ActiveDevicesModel: Mappable {
    
    var active:Bool?
    var biker_id:Int?
    var cmPackageSKUCode:String?
    var cmPackageSerialNumber:String?
    var dmPackageSKUMode:String?
    var dmPackageSerialNumber:String?
    var isVirtualCm:Bool?
    var date_cm_mapped:String?
    var date_dm_mapped:String?
    var device_id:UInt?
    var device_unit_id:String?
    var display_module_id:UInt?
    var display_module_unit_id:String?
    var display_module_sr_no: String?
    var id:UInt?
    var reason:UInt?
    var vehicle_id:Int?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        active                  <- map["active"]
        biker_id                <- map["biker_id"]
        date_cm_mapped          <- map["date_cm_mapped"]
        date_dm_mapped          <- map["date_dm_mapped"]
        device_id               <- map["device_id"]
        device_unit_id          <- map["device_unit_id"]
        display_module_id       <- map["display_module_id"]
        display_module_unit_id   <- map["display_module_unit_id"]
        id                      <- map["id"]
        reason                  <- map["reason"]
        vehicle_id              <- map["vehicle_id"]
        cmPackageSKUCode        <- map["cm_package_sku_code"]
        cmPackageSerialNumber   <- map["cm_package_sr_no"]
        dmPackageSKUMode        <- map["dm_package_sku_code"]
        dmPackageSerialNumber   <- map["dm_package_sr_no"]
        isVirtualCm             <- map["virtual_cm"]
        display_module_sr_no  <- map["display_module_sr_no"]
    }
}

