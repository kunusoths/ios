//
//  ArmDisArmModel.swift
//  Maximus
//
//  Created by kpit on 10/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper

class ArmDisArmModel: Mappable{
    var message:String?
    var status:Int
    var vehicle_id: String?
    
    required init?(map: Map) {
        message = ""
        status = 1
        vehicle_id = ""
    }
    
    func mapping(map: Map) {
        message  <- map["message"]
        status   <- map["status"]
        vehicle_id <- map["vehicle_id"]
    }
    
}
