//
//  RideMemberList.swift
//  Maximus
//
//  Created by Namdev Jagtap on 03/07/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper

class RideMemberList:Mappable
    
{
    var rideMember : [RideMember]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        rideMember          <- map["rideMemberlist"]
    }
}



class RideMember:Mappable{
    var biker_id            : Int?
    var location            : Location?
    var location_timestamp  : String?
    var name                : String?
    var phone_no            : String?
    var profile_image_url   : String?
    var ride_member_id      : Int?
    var status              : Int?
    
    required init?(map: Map) {
        
    }
    // Mappable
    
    func mapping(map: Map) {
        
        biker_id                <- map["biker_id"]
        location                <- map["location"]
        location_timestamp      <- map["location_timestamp"]
        name                    <- map["name"]
        phone_no                <- map["phone_no"]
        profile_image_url       <- map["profile_image_url"]
        ride_member_id          <- map["ride_member_id"]
        status                  <- map["status"]
    }
    
}
