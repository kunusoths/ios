//
//  placeDetailsModel.swift
//  Maximus
//
//  Created by Sriram G on 29/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper

class PlaceDetails:Mappable
{
    var status:String?
    var predictions: [Predictions]?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        status      <- map["status"]
        predictions <- map["predictions"]
    }
}

class Predictions:Mappable
{
    var description:String?
    var id:String?
    var placeId:String?
    var structuredFormatting: StructuredFormatting?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
       description <- map["description"]
       id          <- map["id"]
       placeId     <- map["place_id"]
        structuredFormatting <- map["structured_formatting"]
    }
}

class StructuredFormatting: Mappable {
    var primarytext: String?
    var secondaryText: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        primarytext <- map["main_text"]
        secondaryText          <- map["secondary_text"]
    }
}
