//
//  StickerBoardModel.swift
//  Maximus
//
//  Created by Admin on 13/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper

class StickerBoardResponse:Mappable
{
    var id              :Int?
    var stickerboard    :[Stickerboard]?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                  <- map["_id"]
        stickerboard        <- map["stickerboard"]
    }
}

class Stickerboard:Mappable
{
    var count       :Int?
    var category_id :Int?
    var description :String?
    var title       :String?
    var level       :Int?
    var criteria    :[Criteria]?
    var images      :[Images]?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        count               <- map["count"]
        category_id         <- map["category_id"]
        description         <- map["description"]
        title               <- map["title"]
        level               <- map["level"]
        criteria            <- map["criteria"]
        images              <- map["images"]
    }
}

class Criteria:Mappable
{
    var and       :[And]?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        and               <- map["and"]
    }
}
class And:Mappable
{
    var operator_       :String?
    var operand_value   :Int?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        operator_               <- map["operator"]
        operand_value           <- map["operand_value"]
    }
}

class Images:Mappable
{
    var url         :String?
    var resolution  :String?
    var type        :String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        url             <- map["url"]
        resolution      <- map["resolution"]
        type            <- map["type"]
    }
}

