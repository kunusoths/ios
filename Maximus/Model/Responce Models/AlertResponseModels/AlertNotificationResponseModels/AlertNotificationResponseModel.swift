//
//  RecievedAlertNotificationModel.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 04/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import ObjectMapper

class AlertNotificationResponseModel: Mappable {
    
    var originator_biker_id       :String?
    var triggered_at              :String?
    var originator_biker_name     :String?
    var ride_id                   :String?
    var uuid                      :String?
    var type                      :String?
    var category                  :String?
    var aps                       :ReceivedAlertApsModel?
    var biker_id                  :String?
    var alert_priority            :String?
    var originator_location       :String?
    var alert_short_code          :String?
    var message                   :String?
    var gcm_message_id            :String?
    var alert_id                  :String?
    
    //Ack notification
    var respondent_biker_id       :String?
    var respondent_biker_name     :String?
    
    required init?(map: Map) {
    }
    
    
    func mapping(map: Map) {
        originator_biker_id                      <- map["originator_biker_id"]
        triggered_at                             <- map["triggered_at"]
        originator_biker_name                    <- map["originator_biker_name"]
        ride_id                                  <- map["ride_id"]
        uuid                                     <- map["uuid"]
        type                                     <- map["type"]
        category                                 <- map["category"]
        biker_id                                 <- map["biker_id"]
        aps                                      <- map["aps"]
        alert_priority                           <- map["alert_priority"]
        originator_location                      <- map["originator_location"]
        alert_short_code                         <- map["alert_short_code"]
        message                                  <- map["message"]
        gcm_message_id                           <- map["gcm.message_id"]
        alert_id                                 <- map["alert_id"]
        respondent_biker_id                      <- map["respondent_biker_id"]
        respondent_biker_name                    <- map["respondent_biker_name"]
        
    }
}

class ReceivedAlertApsModel: Mappable {
    
    var alert                        :String?
    var category                     :String?
    
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        alert                   <- map["alert"]
        category                <- map["category"]
    }
}



