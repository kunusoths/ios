//
//  AlertClusterResponseModel.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 06/10/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import ObjectMapper

class AlertClusterResponseModel: Mappable {
    
    var type                      :String?
    var message                   :String?
    var device_os                 :String?
    var token                     :String?
    var uuid                      :String?
    var data                      :ClusterData?
    var cluster_count             :Int?
    var cluster_radius            :Int?
    var cluster_index             :Int?
    var cluster_member_count      :Int?
    var direction                 :String?
    var generated_at              :String?
    var frequency_in_minutes      :Int?
    
    required init?(map: Map) {
    }
    
    
    func mapping(map: Map) {
        type                                <- map["type"]
        message                             <- map["message"]
        device_os                           <- map["device_os"]
        token                               <- map["token"]
        uuid                                <- map["uuid"]
        data                                <- map["data"]
        cluster_count                       <- map["cluster_count"]
        cluster_radius                      <- map["cluster_radius"]
        cluster_index                       <- map["cluster_index"]
        cluster_member_count                <- map["cluster_member_count"]
        direction                           <- map["direction"]
        generated_at                        <- map["generated_at"]
        frequency_in_minutes                <- map["frequency_in_minutes"]
    }
}

class ClusterData: Mappable {
    
    var biker_id                  :Int?
    var biker_name                :String?
    var ride_id                   :Int?
    var ride_member_id            :Int?
    var biker_location            :[Int]?
    
    required init?(map: Map) {
    }
    
    
    func mapping(map: Map) {
        biker_id                      <- map["biker_id"]
        biker_name                    <- map["biker_name"]
        ride_id                       <- map["ride_id"]
        ride_member_id                <- map["ride_member_id"]
        biker_location                <- map["biker_location"]
    }
}
