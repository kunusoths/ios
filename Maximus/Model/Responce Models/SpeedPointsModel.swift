//
//  SpeedPointsModel.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 03/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper

class SpeedPointsModel:Mappable
{
    var BikerId         :Int?
    var RideId          :Int?
    var SpeedList       : [SpeedData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        BikerId             <- map["biker_id"]
        RideId              <- map["ride_id"]
        SpeedList           <- map["speed_data"]
        
    }
}

class SpeedData: Mappable {
    
    var lat             :Double?
    var lng             :Double?
    var packettimestamp :String?
    var ground_speed    :Double?
    var max_ground_speed    :Double?
    var min_ground_speed    :Double?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        lat                 <- map["latitude"]
        lng                 <- map["longitude"]
        packettimestamp      <- map["packettimestamp"]
        ground_speed         <- map["ground_speed"]
        max_ground_speed     <- map["max_ground_speed"]
        min_ground_speed     <- map["min_ground_speed"]

    }
}
