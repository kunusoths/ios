//
//  OpenWheatherModel.swift
//  Maximus
//
//  Created by Abhilash G on 7/5/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper

class OpenWheather:Mappable
{
    var location:LatLong?
    var sys:Country?
    var weather:[Weather]?
    var main:Main?
    var name:String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        location    <- map["coord"]
        sys         <- map["sys"]
        main        <- map["main"]
        name        <- map["name"]
        weather     <- map["weather"]
    }

}

class LatLong:Mappable
{
    var lat: Double?
    var lon: Double?
    
    required init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        lat  <- map["lat"]
        lon  <- map["lon"]
    }
}

class Weather:Mappable
{
    var id:Int?
    var main:String?
    var description:String?
    var icon:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                  <- map["id"]
        main                <- map["main"]
        description         <- map["description"]
        icon                <- map["icon"]
    }
    
}

class Country:Mappable
{
    var name:    String?
    var sunrise: String?
    var sunset:  String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name        <- map["country"]
        sunrise     <- map["sunrise"]
        sunset      <- map["sunset"]
    }
}

class Main:Mappable
{
    var temperature:Double?
    var humidity:Double?
    var pressure:Double?
    var minTemperature:Double?
    var maxTemperature:Double?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        temperature         <- map["temp"]
        humidity            <- map["humidity"]
        pressure            <- map["pressure"]
        minTemperature      <- map["temp_min"]
        maxTemperature      <- map["temp_max"]
    }
}
