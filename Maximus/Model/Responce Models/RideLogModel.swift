//
//  RideLogModel.swift
//  Maximus
//
//  Created by Admin on 08/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper


class RideLogList:Mappable
{
    var rideLogList : [RideLogModel]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        rideLogList          <- map["rideLogList"]
    }
}


class RideLogModel:Mappable{
    
    var biker_id                : Int?
    var rideback_img_url        : String?
    var ride_admin_biker_id     : Int?
    var ride_finish_date        : String?
    var ride_id                 : Int?
    var ride_start_date         : String?
    var ride_status             : Int?
    var ride_title              : String?
    var achivementdata          : AchievementData?
    var ride_sort_by_date       : Date?
    var honored_badges          : [Honored_badges]?
    
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        biker_id            <- map["biker_id"]
        rideback_img_url    <- map["ride_background_image_url"]
        ride_admin_biker_id <- map["ride_admin_biker_id"]
        ride_finish_date    <- map["ride_finish_date"]
        ride_id             <- map["ride_id"]
        ride_start_date     <- map["ride_start_date"]
        ride_status         <- map["ride_status"]
        ride_title          <- map["ride_title"]
        achivementdata      <- map["achievement_data"]
        honored_badges      <- map["honored_badges"]
    }
}

class AchievementData:Mappable{
    
    var average_speed       : Double?
    var distance            : Int?
    var distance_travelled  : Double?
    var last_processed_time : String?
    var time_duration       : Int?
    var top_speed           : Double?
    var topspeedtimerange   : String?
    var ride_end_time       : String?
    var ride_start_time     : String?

    
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        average_speed           <- map["average_speed"]
        distance                <- map["distance"]
        last_processed_time     <- map["last_processed_time"]
        distance_travelled      <- map["distance_travelled"]
        time_duration           <- map["time_duration"]
        top_speed               <- map["top_speed"]
        topspeedtimerange       <- map["top_speed_time_range"]
        ride_end_time           <- map["ride_end_time"]
        ride_start_time         <- map["ride_start_time"]
    }
}

class Honored_badges:Mappable{
    
    var images       : [Images]?
    
    
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        images      <- map["images"]
    }
}

