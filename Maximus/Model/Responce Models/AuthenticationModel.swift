//
//  ResponceModel.swift
//  DJ LAILAK
//
//  Created by Admin on 27/07/16.
//  Copyright © 2016 AppsPlanet. All rights reserved.
//


import Foundation
import ObjectMapper


class AuthenticationModel: Mappable {
    
    //****   Get Acess Token ******
    var id                  : Int?
    var id_token            : String?
    var refresh_token  :String?
    
    required init?(map: Map) {
        id                  = 0
        id_token            = ""
        refresh_token = ""
    }
    
    // Mappable
    func mapping(map: Map) {
        id                  <- map["id"]
        id_token            <- map["id_token"]
        refresh_token    <- map["refresh_token"]
    }
}
