//
//  AlertDetailModel.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 28/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

//
//  AlertDetailModel.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 24/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import ObjectMapper

class AlertApiResponseModel: Mappable {
    
    //Generated Alert
    var id                        :Int?
    var date_triggered            :String?
    var date_received             :String?
    var date_closed               :String?
    var status                    :String?
    var help_received             :Bool?
    var alert_type                :AlertTypeModel?
    var biker_id                  :Int?
    var vehicle_id                :Int?
    var ride_id                   :Int?
    var triggered_by_id           :Int?
    
    //Acknowledged Alert - alert type is absent
    var alert_id                  :Int?
    var date_acked                :String?
    var date_dismissed            :String?
    var date_reached              :String?
    var helped                    :Int?
    
    
    required init?(map: Map) {
    }
    
    
    func mapping(map: Map) {
        id                      <- map["id"]
        date_triggered          <- map["date_triggered"]
        date_received           <- map["date_received"]
        date_closed             <- map["date_closed"]
        status                  <- map["status"]
        help_received           <- map["help_received"]
        alert_type              <- map["alert_type"]
        biker_id                <- map["biker_id"]
        vehicle_id              <- map["vehicle_id"]
        ride_id                 <- map["ride_id"]
        triggered_by_id         <- map["triggered_by_id"]
        
        alert_id                <- map["alert_id"]
        date_acked              <- map["date_acked"]
        date_dismissed          <- map["date_dismissed"]
        date_reached            <- map["date_reached"]
        helped                  <- map["helped"]
    }
}

class AlertTypeModel: Mappable {
    
    var id                        :Int?
    var short_name                :String?
    var priority                  :Int?
    var date_added                :String?
    var category                  :String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id                      <- map["id"]
        short_name              <- map["short_name"]
        priority                <- map["priority"]
        date_added              <- map["date_added"]
        category                <- map["category"]
    }
}
