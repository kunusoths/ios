//
//  RouteLatLngModel.swift
//  Maximus
//
//  Created by Sriram G on 22/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper

class RouteLatLngModel:Mappable
{
    var BikerId         :intmax_t?
    var RideId          :intmax_t?
    var LocationList    : [LocationData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        BikerId             <- map["biker_id"]
        RideId              <- map["ride_id"]
        LocationList        <- map["location_data"]
    }
}

class LocationData: Mappable {
    
    var lat:double_t?
    var lng:double_t?
    var packettimestamp:String?
    
    required init() {
        
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        lat                 <- map["latitude"]
        lng                 <- map["longitude"]
        packettimestamp     <- map["packettimestamp"]
    }
}
