//
//  CMRegistrationModel.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 18/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import ObjectMapper

class CMRegistrationModel: Mappable {
   
    var bike_voltage        : String?
    var device_voltage      : String?
    var ignition_status     : Int?
    var timestamp           : String?
    var welcome_message     : String?
    
    var description         : String?
    var error_code          : Int?
    var message             : String?
    
    required init?(map: Map) {
        bike_voltage         = ""
        device_voltage       = ""
        ignition_status      = 0
        timestamp            = ""
        welcome_message      = ""
        
        description          = ""
        error_code           = 0
        message              = ""
    }
    
    // Mappable
    func mapping(map: Map) {
        bike_voltage        <- map["bike_voltage"]
        device_voltage      <- map["device_voltage"]
        ignition_status     <- map["ignition_status"]
        timestamp           <- map["timestamp"]
        welcome_message     <- map["welcome_message"]
        description         <- map["description"]
        error_code          <- map["error_code"]
        message             <- map["message"]
    }
}
