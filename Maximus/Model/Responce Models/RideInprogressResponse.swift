//
//  RideInprogressResponse.swift
//  Maximus
//
//  Created by Admin on 16/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper


class RideInprogressModel:Mappable{
    
    var rideInprogressList      : [RideCreateResponse]?
    
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        rideInprogressList      <- map["rideInprogressList"]
    }
}
