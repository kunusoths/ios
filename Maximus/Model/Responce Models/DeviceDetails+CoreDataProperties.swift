//
//  DeviceDetails+CoreDataProperties.swift
//  
//
//  Created by Isha Ramdasi on 07/02/18.
//
//

import Foundation
import CoreData


extension DeviceDetails {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DeviceDetails> {
        return NSFetchRequest<DeviceDetails>(entityName: "DeviceDetails")
    }

    @NSManaged public var app_uuid: String?
    @NSManaged public var app_version: String?
    @NSManaged public var date_registered: NSDate?
    @NSManaged public var device_uuid: String?
    @NSManaged public var make: String?
    @NSManaged public var model: String?
    @NSManaged public var os_type: String?
    @NSManaged public var os_version: String?
    @NSManaged public var isSynced: Bool

}
