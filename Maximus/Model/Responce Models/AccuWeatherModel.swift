//
//  AccuWeatherModel.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 24/04/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import Foundation
import ObjectMapper

class AccuWeatherModel: Mappable {
    var Key : String?
    var Temperature : Temperature?
    var Link :String?
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        Key          <- map["Key"]
        Temperature  <- map["Temperature"]
        Link <- map["Link"]
    }
}


class Metric: Mappable {
    //C
    var Value : Float
    
    required init?(map: Map) {
        Value = 0
    }
    
    func mapping(map: Map) {
        Value          <- map["Value"]
    }
    
    
}

class Imperial: Mappable {
    //F
    var Value : Double
    
    required init?(map: Map) {
        Value = 0
    }
    
    func mapping(map: Map) {
        Value          <- map["Value"]
    }
    
    
}
class Temperature: Mappable {
    
    var Imperial : Imperial?
    var Metric : Metric?
    
    required init?(map: Map) {
       
    }
    
    func mapping(map: Map) {
        Imperial          <- map["Imperial"]
        Metric          <- map["Metric"]
    }
}

