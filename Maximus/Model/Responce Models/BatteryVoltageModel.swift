//
//  BatteryVoltageModel.swift
//  Maximus
//
//  Created by Sriram G on 21/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper

class BatteryVoltageModel:Mappable
{
    var BikerId:intmax_t?
    var BatteryVoltageList = [BatteryVoltageData]()
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        BikerId             <- map["biker_id"]
        BatteryVoltageList  <- map["battery_voltage_data"]
    }
}

class BatteryVoltageData: Mappable {
   
    var ignition_status:intmax_t?
    var analog_ip_2:double_t?
    var analog_ip_1:double_t?
    var packettimestamp:String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        ignition_status <- map["ignition_status"]
        analog_ip_2     <- map["analog_ip_2"]
        analog_ip_1     <- map["analog_ip_1"]
        packettimestamp <- map["packettimestamp"]
    }
}
