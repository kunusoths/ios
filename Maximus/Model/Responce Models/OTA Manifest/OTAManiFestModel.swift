//
//  OTAManiFestModel.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 05/11/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import ObjectMapper
import Foundation


class OTAManifest: Mappable {
    var otaObj:OTA?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        otaObj        <- map["ota_manifest"]

    }
    
}

class OTA: Mappable {
    public var displayModule : DisplayModuleModel?
    
    required  init?(map: Map) {

    }
    
    func mapping(map: Map) {
        displayModule             <- map["diasplay_module"]
    }
}

class DisplayModuleModel: Mappable {

    public var hwVersion : String?
    public var appversion : String?
    public var firmwareData : FirmWareDataModel?
    
    required init?(map: Map) {
        //
    }
    
    func mapping(map: Map) {
        hwVersion            <- map["hw_version"]
        appversion           <- map["app_version"]
        firmwareData         <- map["firmware"]

    }
}

class FirmWareDataModel: Mappable {
    public var type : String?
    public var version : String?
    public var filename : String?
    public var checksum : String?
    public var memory : FirmwareMemory?
    public var buildNo: String?
    
    required init?(map: Map) {
        //
    }
    
    func mapping(map: Map) {
        
        type               <- map["type"]
        version            <- map["version"]
        filename           <- map["filename"]
        checksum           <- map["checksum"]
        memory            <- map["memory"]
        buildNo            <- map["build"]
    }

}

class FirmwareMemory:Mappable {
    public var high : String?
    public var low : String?
    
    required init?(map: Map) {
        //
    }
    
    func mapping(map: Map) {
        //
        high <- map["high"]
        low  <- map["low"]
    }
}

class DMData:Mappable {
    public var type : String?
    public var version : String?
    public var filename : String?
    public var checksum : String?
    public var memory : FirmwareMemory?
    
    required init?(map: Map) {
        //
    }
    
    func mapping(map: Map) {
        //
    }
}


