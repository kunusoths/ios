//
//  RecommendedResponse.swift
//  Maximus
//
//  Created by APPLE on 01/10/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import ObjectMapper

class RecommendedResponse: Mappable {
    var id : Int?
    var card_id : String?
    var card : Card?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        card_id <- map["card_id"]
        card <- map["card"]
    }
}
