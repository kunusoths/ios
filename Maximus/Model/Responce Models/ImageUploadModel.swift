//
//  ImageUploadModel.swift
//  Maximus
//
//  Created by Namdev Jagtap on 03/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper

class ImageUploadModel: Mappable {
    
    var imgPath:String?
    var imgType:String?
    
    required init?(map: Map) {
        
        imgPath = ""
        imgType = ""
        
    }
    
    func mapping(map: Map) {
        
        imgPath         <- map["path"]
        imgType         <- map["type"]
    }
}
