//
//  RideCreateResponse.swift
//  Maximus
//
//  Created by Admin on 22/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import ObjectMapper

class RideCreateResponse: NSObject,Mappable {
    
    var admin_biker_id      : Int?
    var admin_profile_img   : String?
    var background_imag     : String?
    var date_finished       : String?
    var date_created        : String?
    var date_cancelled      : String?
    var date_started        : String?
    var Description         : String?
    var difficulty_level    : String?
    var distance            : Int?
    var duration            : Int?
    var end_location        : PositionData?
    var finish_date         : String?
    var id                  : Int?
    var planned             : Bool?
    var round_trip          : String?
    var route               : String?
    var start_date          : String?
    var start_location      : PositionData?
    var status              : Int?
    var title               : String?
    var type                : Int?
    var users_capacity      : String?
    var users_following     : String?
    var users_joined        : Int?
    var waypoints           : [Any]?
    var biker               : Biker?
    var bikergroup          : Bikergroup?
    var bikerstatus         : Int?
    var rideMembers         : [Ride_members]?
    var ride_sort_by_date   : Date?
    var public_ride         : Bool?
    var tbt_route : String?
    
    required override init() {
        
    }
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        admin_biker_id      <- map["admin_biker_id"]
        admin_profile_img   <- map["admin_biker_profile_image_url"]
        background_imag     <- map["background_image_url"]
        date_created        <- map["date_created"]
        date_finished       <- map["date_finished"]
        date_cancelled      <- map["date_cancelled"]
        date_started        <- map["date_started"]
        Description         <- map["description"]
        difficulty_level    <- map["difficulty_level"]
        distance            <- map["distance"]
        duration            <- map["duration"]
        end_location        <- map["end_location"]
        finish_date         <- map["finish_date"]
        id                  <- map["id"]
        planned             <- map["planned"]
        round_trip          <- map["round_trip"]
        route               <- map["route"]
        start_date          <- map["start_date"]
        start_location      <- map["start_location"]
        status              <- map["status"]
        title               <- map["title"]
        type                <- map["type"]
        users_capacity      <- map["users_capacity"]
        users_following     <- map["users_following"]
        users_joined        <- map["users_joined"]
        waypoints           <- map["waypoints"]
        biker               <- map["biker"]
        bikergroup          <- map["bikergroup"]
        bikerstatus         <- map["biker_status"]
        rideMembers         <- map["ride_members"]
        public_ride         <- map["public_ride"]
        tbt_route   <- map["tbt_route"]
    }
}


class Biker:Mappable{
    var access_level            : String?
    var address                 : String?
    var background_image_url    : String?
    var blood_group             : String?
    var first_name              : String?
    var emergency_contacts      : [EmergencyContacts]?
    var email1                  : String?
    var id                      : Int?
    var last_name               : String?
    var license_issued          : String?
    var license_number          : String?
    var license_valid_till      : String?
    var middle_name             : String?
    var phone_mobile1           : String?
    var profile_image_url       : String?
    var share_location          : Bool?
    var cm_paired               : Bool?
    var otp_verified            : Bool?
    var default_vehicle         : DefaultVehicle?
    var biker_status            : String?
    var email1_verified         : Bool?
    
    var login_method            : String?
    var user_name               : String?
    var auth_method             : Int?
    
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        access_level            <- map["access_level"]
        address                 <- map["address"]
        background_image_url    <- map["background_image_url"]
        blood_group             <- map["blood_group"]
        first_name              <- map["first_name"]
        emergency_contacts      <- map["emergency_contacts"]
        id                      <- map["id"]
        email1                  <- map["email1"]
        last_name               <- map["last_name"]
        license_issued          <- map["license_issued"]
        license_number          <- map["license_number"]
        license_valid_till      <- map["license_valid_till"]
        middle_name             <- map["middle_name"]
        phone_mobile1           <- map["phone_mobile1"]
        profile_image_url       <- map["profile_image_url"]
        share_location          <- map["share_location"]
        default_vehicle         <- map["default_vehicle"]
        cm_paired               <- map["cm_paired"]
        otp_verified            <- map["otp_verified"]
        biker_status            <- map["biker_status"]
        email1_verified         <- map["email1_verified"]
        auth_method             <- map["auth_method"]
    }
}

class DefaultVehicle:Mappable{
    var company         : String?
    var date_added      : String?
    var make            : Int?
    var model           : String?
    var id              : Int?
    var nick_name       : String?
    var odometer_reading: String?
    var device          : Device?
    var registration_month: Int?
    var registration_year: Int?

    
    
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        company             <- map["company"]
        date_added          <- map["date_added"]
        device              <- map["device"]
        make                <- map["make"]
        model               <- map["model"]
        id                  <- map["id"]
        nick_name           <- map["nick_name"]
        odometer_reading    <- map["odometer_reading"]
        registration_month  <- map["registration_month"]
        registration_year   <- map["registration_year"]
    }
}

class EmergencyContacts: Mappable{
    
    var name      : String?
    var phone     : String?

    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        name      <- map["name"]
        phone     <- map["phone"]
    }
}

class Bikergroup:Mappable{
    var admin_biker_id      : String?
    var bikergroup_icon_url : String?
    var date_created        : String?
    var description         : String?
    var id                  : Int?
    var title               : String?
    var type                : Int?
    var groupMembers        : [Group_members]?
    
    
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        admin_biker_id      <- map["admin_biker_id"]
        bikergroup_icon_url <- map["bikergroup_icon_url"]
        date_created        <- map["date_created"]
        description         <- map["description"]
        id                  <- map["id"]
        title               <- map["title"]
        type                <- map["type"]
        groupMembers        <- map["group_members"]
    }
}

class Group_members:Mappable {
    
    var date_declined   : String?
    var date_invited    : String?
    var date_joined     : String?
    var id              : Int?
    var status          : Int?
    var biker           : Biker?
    
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        date_declined   <- map["date_declined"]
        date_invited    <- map["date_invited"]
        date_joined     <- map["date_joined"]
        id              <- map["id"]
        status          <- map["status"]
        biker           <- map["biker"]
    }
}

class Ride_members:Mappable {
    
    var date_accepted   : String?
    var date_followed   : String?
    var date_invited    : String?
    var date_rejected   : String?
    var date_unjoined   : String?
    var id              : Int?
    var status          : Int?
    var vehicle         : String?
    var biker           : Biker?
    
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        date_accepted       <- map["date_accepted"]
        date_followed       <- map["date_followed"]
        date_invited        <- map["date_invited"]
        date_rejected       <- map["date_rejected"]
        date_unjoined       <- map["date_unjoined"]
        id                  <- map["id"]
        status              <- map["status"]
        vehicle             <- map["vehicle"]
        biker               <- map["biker"]
    }
}


//MARK: Ride Start Responce Model
class RideStartResponse:Mappable {

    var status          : String?
    
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
            status              <- map["status"]

    }
}

class Device:Mappable {
    
    var date_added      : String?
    var id              : Int?
    var imei            : String?
    var mobile_number   : String?
    var msisdn          : String?
    var serial_number   : String?
    var title           : String?
    var history         : [History]?
    
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        date_added          <- map["date_added"]
        id                  <- map["id"]
        imei                <- map["imei"]
        mobile_number       <- map["mobile_number"]
        msisdn              <- map["msisdn"]
        serial_number       <- map["serial_number"]
        title               <- map["title"]
        history             <- map["history"]
    }
}

class History:Mappable {
    
    var message          : String?
    var updated_at       : String?
    
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        message              <- map["message"]
        updated_at           <- map["updated_at"]
    }
}
//"start_location" =         {
//    latlong =             {
//        latitude = "18.5204303";
//        longitude = "73.8567437";
//    };
//    name = "Pune, Maharashtra, India";
//};
class PositionData:Mappable {
    
    var name          : String?
    var latlong       : LocationData?
    
    required init() {
        
    }
    
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        name              <- map["name"]
        latlong           <- map["latlong"]
    }
}
