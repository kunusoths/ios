//
//  CPWrapper.m
//  Maximus
//
//  Created by Namdev Jagtap on 13/07/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

#import "CPWrapper.h"

//struct communication_protocol_ind ind_callback = {
//
//};
//
//
//struct communication_protocol_cfm cfm_callback = {
//
//};
//
//struct transport_cb transport_callback = {
//
//    .try_send = try_send, };

CP_CommunicationProtocolHandlers handler =  {
    .callback_ind = callback_ind,
    .callback_cfm = cp_callback_cfm,
    .try_send = try_send
};

struct kbike_communication_protocol *session;

unsigned int src_id = SOURCE_ID_PHONE;
unsigned int session_id = 0xFF;

id bridgeInstance;

@interface CPWrapper()
{
    //    id<CommunicationProtocolDelegate> delegate1;
}
@end

@implementation CPWrapper
static CPWrapper *cpWrapperObj = nil;


+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        cpWrapperObj = [[CPWrapper alloc] init];
    });
    
    return cpWrapperObj;
    
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        bridgeInstance =self;
    }
    return self;
}

-(int) cp_initialize:(int)imtu andOmtu:(int)omtu
{
    int result = SESSION_FALSE;
    session = CP_NewSession(imtu, omtu, &handler);
    if (session!=NULL) {
        result = SESSION_TRUE;
    }
    return result;
}

- (void) cp_firmwareUpdateStart
{
    CP_DmFirmwareUpdateOtaTypeDef otaTypeDef;
    otaTypeDef.otaTypeID = OTA_ONLY_APP;
    CP_FirmwareUpdateStart(session, src_id, session_id, &otaTypeDef, sizeof(otaTypeDef));
}


- (Boolean) cp_set_date_time:(int)HH andMIN:(int)MIN andSS:(int)SS andDD:(int)DD andMM:(int)MM andYY:(int)YY andAmPm:(int)ampm andTmeFormat:(int)format andWeekDay:(int)weekDay
{
    NSLog(@"CPWrapper: Date & Time send");
    Boolean result = false;
    
    CP_CurrentDateTimeTypeDef dateTime;
    
    dateTime.liveFlag = 1;
    dateTime.hour = HH;
    dateTime.minute = MIN;
    dateTime.second = SS;
    dateTime.date = DD;
    dateTime.month = MM;
    dateTime.year = YY;
    dateTime.timeFormat = format;
    dateTime.TimeAmPm = ampm;
    dateTime.dayLightSaving = DAYLIGHTSAVING_NONE;
    dateTime.weekDayInfo = weekDay;
    dateTime.storeOperation = STORE_OPERATION_RESET;
    
    if(CP_CurrentDateTime(session, src_id, session_id, &dateTime, sizeof(CP_CurrentDateTimeTypeDef))){
        result = true;
    }
    
    return result;
}

- (void) releaseCommunicationProtocol
{
    end_connection(session);
}

- (void)cp_receive_payload:(NSData*)data{
    // unsigned char *buff;
    NSUInteger len = [data length];
    // buff = (char*)malloc(len);
    memcpy(session->inbuf, [data bytes], len);
    CP_ReceivePayload(session, len);
}

#pragma makr Journey Setup

-(void) CP_SourceName:(int)liveFlag andSourceName:(NSString*)strname{
    
    NSLog(@"CPWrapper: Source Name send:%@",strname);

    if (strname != NULL){
        NSString *name = [self nullTerminatedString:strname];
        
        //Cast Nsstring to bytes array
        NSData *dataSourceName = [name dataUsingEncoding:NSUTF8StringEncoding];
        const void *bytes = [dataSourceName bytes];
        
        CP_RouteSourceNameTypeDef sourceName;
        sourceName.liveFlag = liveFlag;
        sourceName.stringInfo.letterCount = (uint8_t)dataSourceName.length;
        //create a buufer of size structure and string length
        unsigned char *buff = (unsigned char *)malloc(sizeof(sourceName) + name.length);
        
        //first copy struct to buffer
        memcpy((void*)buff, &sourceName, sizeof(sourceName));
        
        //after structure append string to buffer
        memcpy((void*)(buff+sizeof(sourceName)),(void*)bytes, name.length);
        
        //send to communication protocol
        CP_SourceName(session, src_id, session_id, buff, sizeof(sourceName) + name.length);
        
        //free buffer
        free(buff);
    }
    
}

-(void) CP_DestinationName:(int)liveFlag andDestinationName:(NSString*)strname{
    NSLog(@"CPWrapper: Destination Name send:%@",strname);
    if (strname != NULL){
        NSString *name = [self nullTerminatedString:strname];
        
        //Cast Nsstring to bytes array
        NSData *nextCity = [name dataUsingEncoding:NSUTF8StringEncoding];
        const void *bytes_nextCity = [nextCity bytes];
        
        CP_RouteDestinationNameTypeDef stuct_nextCity;
        
        stuct_nextCity.liveFlag = liveFlag;
        stuct_nextCity.stringInfo.letterCount = (uint8_t)nextCity.length ;
        
        //create a buufer of size structure and string length
        unsigned char *buff_nextCity = (unsigned char *)malloc(sizeof(stuct_nextCity) + name.length);
        
        //first copy struct to buffer
        memcpy((void*)buff_nextCity, &stuct_nextCity, sizeof(stuct_nextCity));
        
        //after structure append string to buffer
        memcpy((void*)(buff_nextCity+sizeof(stuct_nextCity)),(void*)bytes_nextCity,name.length);
        
        CP_DestinationName(session, src_id, session_id, buff_nextCity, sizeof(stuct_nextCity) + name.length);
        
        free(buff_nextCity);
    }
}


-(void) cp_dailyTargatedDestinationName:(int)liveFlag andNextCity:(NSString*)strname{
    NSLog(@"CPWrapper: Daily targated Destination Name send:%@",strname);
    if (strname != NULL){
        NSString *name = [self nullTerminatedString:strname];
        
        //Cast Nsstring to bytes array
        NSData *dataDestinationName = [name dataUsingEncoding:NSUTF8StringEncoding];
        const void *bytes = [dataDestinationName bytes];
        
        CP_RouteDestinationNameTypeDef destinationName;
        destinationName.liveFlag = liveFlag;
        destinationName.stringInfo.letterCount = (uint8_t)dataDestinationName.length ;
        
        //create a buufer of size structure and string length
        unsigned char *buff = (unsigned char *)malloc(sizeof(destinationName) + name.length);
        
        //first copy struct to buffer
        memcpy((void*)buff, &destinationName, sizeof(destinationName));
        
        //after structure append string to buffer
        memcpy((void*)(buff+sizeof(destinationName)),(void*)bytes, name.length);
        
        CP_DailyTargatedDestinationName(session, src_id, session_id, buff, sizeof(destinationName) + name.length);
        
        free(buff);
    }
}



-(void) CP_FullJourneyDistance:(int)liveFlag  andDistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit{
    NSLog(@"CPWrapper: Full Journey Distance send: Dist-%d Scale-%d Unit-%d",distance,scaleType,unit);
    CP_FullJourneyDistanceTypeDef journeyDistance;
    journeyDistance.distance = (uint16_t)distance;
    journeyDistance.liveFlag = liveFlag;
    journeyDistance.scale =  scaleType;
    journeyDistance.unit = unit;
    
    CP_FullJourneyDistance(session, src_id, session_id, &journeyDistance, sizeof(journeyDistance));
}

#pragma mark VEHICAL INFORMATION
- (void) cp_update_vehicle_battery_voltage:(int)liveFlag andvoltage:(int)voltage andscaleType:(int)scaleType andunit:(int)unit
{
    CP_VehicleBatteryVoltageTypeDef batteryData;
    batteryData.liveFlag = liveFlag;
    batteryData.voltage = voltage;
    batteryData.scale = scaleType;
    batteryData.unit = unit;
    
    CP_VehicleBatteryVoltage(session, src_id, session_id, &batteryData, sizeof(batteryData));
    
}

- (void) cp_update_vehicle_fuel_level:(int)liveFlag andfuelLevel:(int)fuelLevel andscaleType:(int)scaleType andunit:(int)unit
{
    CP_BikeFuelLevelTypeDef bikefuelLevel;
    bikefuelLevel.liveFlag = liveFlag;
    bikefuelLevel.fuelLevel = fuelLevel;
    bikefuelLevel.scale = scaleType;
    bikefuelLevel.unit = unit;
    
    CP_BikeFuelLevel(session, src_id, session_id, &bikefuelLevel, sizeof(bikefuelLevel));
}

- (void) cp_update_vehicle_milage_distance:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit
{
    CP_BikeMileageDistanceTypeDef milage;
    milage.liveFlag = liveFlag;
    milage.distance = distance;
    milage.scale = scaleType;
    milage.unit = unit;
    
    CP_BikeMilageDistance(session, src_id, session_id, &milage, sizeof(milage));
}


- (void) cp_update_source_info:(int)liveFlag andstringLength:(int)stringLength andsourceName:(Byte[])sourceName
{
    
}

- (void) cp_update_destination_info:(int)liveFlag andstringLength:(int)stringLength anddestinationName:(Byte[])destinationName
{
    
}

#pragma mark JOURNEY ETA

- (void) cp_update_full_journey_eta:(int)liveFlag andhour:(int)hour andminute:(int)minute
{
    NSLog(@"CPWrapper: Full Journey ETA send: Hr:%d Min:%d",hour,minute);

    CP_FullJourneyEtaTypeDef fullJourneyData;
    fullJourneyData.liveFlag = liveFlag;
    fullJourneyData.hour = hour;
    fullJourneyData.minute = minute;
    
    CP_FullJourneyEta(session, src_id, session_id, &fullJourneyData, sizeof(fullJourneyData));
    
}

- (void) cp_update_full_journey_distance_unit:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit
{
    NSLog(@"CPWrapper: Full Journey ETA Distance send: Dist-%d Scale-%d Unit-%d",distance,scaleType,unit);

    CP_FullJourneyDistanceTypeDef fullJourneyDataUnit;
    fullJourneyDataUnit.liveFlag = liveFlag;
    fullJourneyDataUnit.distance = distance;
    fullJourneyDataUnit.scale = scaleType;
    fullJourneyDataUnit.unit = unit;
    
    CP_FullJourneyEta(session, src_id, session_id, &fullJourneyDataUnit, sizeof(fullJourneyDataUnit));
}


- (void) cp_update_total_traveled_distance:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit
{
    NSLog(@"CPWrapper: travalled Distance send: Dist-%d Scale-%d Unit-%d",distance,scaleType,unit);
    CP_TotalJourneyTravelledDistanceTypeDef totalTraveled;
    totalTraveled.liveFlag = liveFlag;
    totalTraveled.distance = distance;
    totalTraveled.scale = scaleType;
    totalTraveled.unit = unit;
    
    CP_TotalTravelledDistance(session, src_id, session_id, &totalTraveled, sizeof(totalTraveled));
    
}

- (void) cp_update_daily_traveled_distance:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit
{
    NSLog(@"CPWrapper: Daily travalled Distance send: Dist-%d Scale-%d Unit-%d",distance,scaleType,unit);
    CP_DailyTravelledDistanceTypeDef dailyTraveled;
    dailyTraveled.liveFlag = liveFlag;
    dailyTraveled.distance = distance;
    dailyTraveled.scale = scaleType;
    dailyTraveled.unit = unit;
    
    CP_DailyTravelledDistance(session, src_id, session_id, &dailyTraveled, sizeof(dailyTraveled));
}

- (void) cp_update_total_journey_remaining_distance:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit
{
    NSLog(@"CPWrapper: Total journey rem Distance send: Dist-%d Scale-%d Unit-%d",distance,scaleType,unit);
    CP_TotalJourneyTravelledDistanceTypeDef fullJourneyRemainingDist;
    fullJourneyRemainingDist.liveFlag = liveFlag;
    fullJourneyRemainingDist.distance = distance;
    fullJourneyRemainingDist.scale = scaleType;
    fullJourneyRemainingDist.unit = unit;
    
    CP_TotalTravelledDistance(session, src_id, session_id, &fullJourneyRemainingDist, sizeof(fullJourneyRemainingDist));
    
}

- (void) cp_update_total_journey_eta:(int)liveFlag andhour:(int)hour andminute:(int)minute
{
    NSLog(@"CPWrapper: Total journey ETA send: hr-%d mi-%d",hour,minute);

    CP_FullJourneyReachingTimeTypeDef fullJourneyETA;
    fullJourneyETA.liveFlag = liveFlag;
    fullJourneyETA.hour = hour;
    fullJourneyETA.minute = minute;
    
    CP_FullJourneyEta(session, src_id, session_id, &fullJourneyETA, sizeof(fullJourneyETA));
}

- (void) cp_update_total_journey_eta_unit:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit
{
    NSLog(@"CPWrapper: Total journey ETA Distance send: Dist-%d Scale-%d Unit-%d",distance,scaleType,unit);
    CP_FullJourneyRemainingDistanceTypeDef fullJourneyRemainingDistUnit;
    fullJourneyRemainingDistUnit.liveFlag = liveFlag;
    fullJourneyRemainingDistUnit.distance = distance;
    fullJourneyRemainingDistUnit.scale = scaleType;
    fullJourneyRemainingDistUnit.unit = unit;
    
    CP_FullJourneyDistance(session, src_id, session_id, &fullJourneyRemainingDistUnit, sizeof(fullJourneyRemainingDistUnit));
    
}

- (void) cp_update_daily_journey_eta:(int)liveFlag andhour:(int)hour andminute:(int)minute andAmPm:(int)ampm andTmeFormat:(int)format
{
    NSLog(@"CPWrapper: journey reaching time send");

    CP_DailyReachingTimeTypeDef dailyETA;
    dailyETA.liveFlag = liveFlag;
    dailyETA.hour = hour;
    dailyETA.minute = minute;
    dailyETA.TimeAmPm = ampm;
    dailyETA.timeFormat = format;
    CP_DailyJourneyReachingTime(session, src_id, session_id, &dailyETA, sizeof(dailyETA));
    //CP_DailyEta(session, src_id, session_id, &dailyETA, sizeof(dailyETA));
}


- (void) cp_update_daily_journey_eta_unit:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit
{
    NSLog(@"CPWrapper: Daily journey remaining distance send");

    CP_DailyJourneyRemainingDistanceTypeDef dailyJourneyRemainingDistUnit;
    dailyJourneyRemainingDistUnit.liveFlag = liveFlag;
    dailyJourneyRemainingDistUnit.distance = distance;
    dailyJourneyRemainingDistUnit.scale = scaleType;
    dailyJourneyRemainingDistUnit.unit = unit;
    
    CP_DailyJourneyRemainingDistance(session, session_id, session_id,  &dailyJourneyRemainingDistUnit,  sizeof(dailyJourneyRemainingDistUnit));
    //CP_DailyDistance(session, src_id, session_id, &dailyJourneyRemainingDistUnit, sizeof(dailyJourneyRemainingDistUnit));
}

#pragma mark POI ETA

- (void) cp_update_poi_eta:(int)liveFlag andhour:(int)hour andminute:(int)minute
{
    NSLog(@"CPWrapper: POI ETA send");

    CP_PoiEtaTypeDef poiETA;
    poiETA.liveFlag = liveFlag;
    poiETA.hour = hour;
    poiETA.minute = minute;
    
    CP_PoiEta(session, src_id, session_id, &poiETA, sizeof(poiETA));
    
}

- (void) cp_update_poi_distance_unit:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit
{
    NSLog(@"CPWrapper: POI Distance send");

    CP_PoiDistanceTypeDef poiETADistanceUnit;
    poiETADistanceUnit.liveFlag = liveFlag;
    poiETADistanceUnit.distance = distance;
    poiETADistanceUnit.scale = scaleType;
    poiETADistanceUnit.unit = unit;
    
    CP_PoiDistance(session, src_id, session_id, &poiETADistanceUnit, sizeof(poiETADistanceUnit));
    
}

#pragma mark DAILY ETA
- (void) cp_update_daily_eta:(int)liveFlag andhour:(int)hour andminute:(int)minute
{
    NSLog(@"CPWrapper: update Daily ETA send");
    CP_DailyRideEtaTypeDef dailyETA;
    dailyETA.liveFlag = liveFlag;
    dailyETA.hour = hour;
    dailyETA.minute = minute;
    
    CP_DailyEta(session, src_id, session_id, &dailyETA, sizeof(dailyETA));
}

- (void) cp_update_daily_eta_distance_unit:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit
{
    NSLog(@"CPWrapper: update Daily ETA Distance send");
    CP_DailyRideDistanceTypeDef dailyETADistanceUnit;
    dailyETADistanceUnit.liveFlag = liveFlag;
    dailyETADistanceUnit.distance = distance;
    dailyETADistanceUnit.scale = scaleType;
    dailyETADistanceUnit.unit = unit;
    
    CP_DailyDistance(session, src_id, session_id, &dailyETADistanceUnit, sizeof(dailyETADistanceUnit));
}

#pragma mark MENUEVER
- (void) cp_update_current_manuever_icon:(int)liveFlag andiconID:(int)iconID
{
    NSLog(@"CPWrapper: update Current manuever icon send");
    CP_CurrentManeuverIconTypeDef currentManeuverIcon;
    currentManeuverIcon.liveFlag = liveFlag;
    currentManeuverIcon.IconID = iconID;
    
    CP_CurrentManueverIcon(session, src_id, session_id, &currentManeuverIcon, sizeof(currentManeuverIcon));
}

- (void) cp_update_current_manuever_distance_unit:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit
{
    NSLog(@"CPWrapper: update Current manuever Distance send");
    CP_CurrentManeuverDistanceTypeDef currentManeuverIconDistanceUnit;
    currentManeuverIconDistanceUnit.liveFlag = liveFlag;
    currentManeuverIconDistanceUnit.distance = distance;
    currentManeuverIconDistanceUnit.scale = scaleType;
    currentManeuverIconDistanceUnit.unit = unit;
    
    CP_CurrentManueverDistance(session, src_id, session_id, &currentManeuverIconDistanceUnit, sizeof(currentManeuverIconDistanceUnit));
    
}

- (void) cp_update_next_manuever_icon:(int)liveFlag andiconID:(int)iconID
{
    NSLog(@"CPWrapper: update Next manuever icon send");
    CP_NextManeuverIconTypeDef nextManeuverIcon;
    nextManeuverIcon.liveFlag = liveFlag;
    nextManeuverIcon.IconID = iconID;
    
    CP_NextManueverIcon(session, src_id, session_id, &nextManeuverIcon, sizeof(nextManeuverIcon));
}

- (void) cp_update_next_manuever_distance_unit:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit
{
    NSLog(@"CPWrapper: update Next manuever Distance send");

    CP_NextManeuverDistanceTypeDef nextManeuverIconDistanceUnit;
    nextManeuverIconDistanceUnit.liveFlag = liveFlag;
    nextManeuverIconDistanceUnit.distance = distance;
    nextManeuverIconDistanceUnit.scale = scaleType;
    nextManeuverIconDistanceUnit.unit = unit;
    
    CP_NextManueverDistance(session, src_id, session_id, &nextManeuverIconDistanceUnit, sizeof(nextManeuverIconDistanceUnit));
}

#pragma mark Set Up Before Journey Start ------

//Set Journey On/Off
- (void) cp_setJourney:(int)liveFlag andJourneyOnOff:(int)journeyOnOff{
    
    NSLog(@"CPWrapper: Journey ON/OFF send : ON/OFF =%d",journeyOnOff);

    CP_SetJourneyTypeDef setJourneyType;
    setJourneyType.onOff = journeyOnOff;
    
    CP_SetJourney(session, src_id, session_id, &setJourneyType, sizeof(setJourneyType));
}

// Set Daily Journey Start/Stop
-(void) cp_dailyJourneyStartStop:(int)liveFlag andJourneyStartStop:(int)dailyJourneyStartStop{
    
    NSLog(@"CPWrapper: Daily Journey START/STOP send : START/STOP =%d",dailyJourneyStartStop);

    CP_DailyJourneyStartStopTypeDef struct_DailyJourneyStartStop;
    struct_DailyJourneyStartStop.onOff = dailyJourneyStartStop;
    
    CP_DailyJourneyStartStop(session, src_id, session_id, &struct_DailyJourneyStartStop, sizeof(struct_DailyJourneyStartStop));
}

// Set  Journey Start/Stop
-(void) cp_fullJourneyStartStop:(int)liveFlag andJourneyStartStop:(int)journeyStartStop{
    
    NSLog(@"CPWrapper: Full Journey START/STOP send : START/STOP =%d",journeyStartStop);

    CP_FullJourneyStartStopTypeDef fullJourneyStartStop;
    fullJourneyStartStop.onOff = journeyStartStop;
    
    CP_FullJourneyStartStop(session, src_id, session_id, &fullJourneyStartStop, sizeof(fullJourneyStartStop));
}



//Set TBT Start / Stop
-(void) cp_tbtStartStop:(int)liveFlag andTBTStartStop:(int)TBTStartStop{
    
    NSLog(@"CPWrapper: TBT START/STOP send : START/STOP =%d",TBTStartStop);

    CP_TbtStartStopTypeDef struct_tbtStartStopType;
    struct_tbtStartStopType.onOff = TBTStartStop;
    
    CP_TbtStartStop(session, src_id, session_id, &struct_tbtStartStopType, sizeof(struct_tbtStartStopType));
    
}

//Show / Hide TBT
-(void) cp_tbtHideShow:(int)liveFlag andTBTHideShow:(int)TBTHideShow{
    
    NSLog(@"CPWrapper: TBT Hide/Show send : Hide/Show =%d",TBTHideShow);

    CP_TbtHideShowTypeDef tbtHideShowType;
    tbtHideShowType.onOff = TBTHideShow;
    
    CP_TbtHideShow(session, src_id, session_id, &tbtHideShowType, sizeof(tbtHideShowType));
}
#pragma mark Straight Arrow events
-(void)cp_followStraightRoad {
    CP_FollowStraightRoad(session, src_id, session_id, 0, 0);
}

-(void)cp_approachingDestination {
    int zeroValue = 0;
    CP_ApproachingDestination(session, src_id, session_id, &zeroValue, sizeof(zeroValue));
}

-(void)cp_approachingPoi {
    int zeroValue = 0;
    CP_ApproachingPoi(session, src_id, session_id, &zeroValue, sizeof(zeroValue));
}


#pragma mark HEADING Events
-(void) cp_tbtHEADINGDirection:(int)liveFlag  andString:(NSString*)strname{

    NSLog(@"CPWrapper: Heading direction name send =%@",strname);

    if (strname != NULL){
        NSString *name = [self nullTerminatedString:strname];
        
        //Cast Nsstring to bytes array
        NSData *nameDATA = [name dataUsingEncoding:NSUTF8StringEncoding];
        const void *data_bytes = [nameDATA bytes];
        
        CP_HeadingToTypeDef heading;
        heading.liveFlag = liveFlag;
        heading.stringInfo.letterCount = (uint8_t)nameDATA.length ;
        
        //create a buufer of size structure and string length
        unsigned char *buff = (unsigned char *)malloc(sizeof(heading) + name.length);
        
        //first copy struct to buffer
        memcpy((void*)buff, &heading, sizeof(heading));
        
        //after structure append string to buffer
        memcpy((void*)(buff+sizeof(heading)),(void*)data_bytes,name.length);
        
        CP_SetHeadingDirection(session, src_id, session_id, buff, sizeof(heading) + name.length);
        free(buff);
    }
}

-(void) cp_tbtHEADINGStart:(int)liveFlag andSTARTStatus:(CP_HeadingStatusTypeDef)status{
   
    NSLog(@"CPWrapper: Heading START/STOP send ");

    CP_HeadingStartTypeDef heading;
    heading.startStop = status;
    CP_HeadingStart(session, src_id, session_id, &heading, sizeof(heading));
}

-(void) cp_tbtHEADINGDistance:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit{
    
    NSLog(@"CPWrapper: Heading Disatnce send: Dist-%d",distance);

    CP_HeadingDistanceTypeDef heading;
    heading.liveFlag    = liveFlag;
    heading.distance    = distance;
    heading.scale       = scaleType;
    heading.unit        = unit;
    
    CP_SetHeadingDistance(session, src_id, session_id, &heading, sizeof(heading));
}


-(void) cp_headingBearingAngle:(int)liveFlag andAngle:(int)angle{
    
    NSLog(@"CPWrapper: Heading Angle send: Angle-%d",angle);
    CP_BearingAngleTypeDef bearingAngle;
    bearingAngle.bearingAngleDegree = angle;
    
    CP_HeadingBearingAngle(session, src_id, session_id, &bearingAngle, sizeof(bearingAngle));
}

-(void) cp_DailyCompletedRideTime:(int)liveFlag minutes:(int)minutes hours:(int)hours{

    NSLog(@"CPWrapper: Daily complete ride time send");

    CP_DailyCompletedRideTimeTypeDef dailyCompleteRideTime;
    dailyCompleteRideTime.liveFlag = liveFlag;
    dailyCompleteRideTime.hour = hours;
    dailyCompleteRideTime.minute = minutes;
    
    CP_DailyCompletedRideTime(session, src_id, session_id, &dailyCompleteRideTime, sizeof(dailyCompleteRideTime));
}

-(void) cp_FullJourneyCompletedRideTime:(int)liveFlag minutes:(int)minutes hours:(int)hours days:(int) days{

    NSLog(@"CPWrapper: Full Journey complete ride time send");

    CP_FullJourneyCompletedRideTimeTypeDef fullJourneyCompleteRideTime;
    fullJourneyCompleteRideTime.liveFlag = liveFlag;
    fullJourneyCompleteRideTime.hour = hours;
    fullJourneyCompleteRideTime.minute = minutes;
    fullJourneyCompleteRideTime.days = days;
    
    CP_FullJourneyCompletedRideTime(session, src_id, session_id, &fullJourneyCompleteRideTime, sizeof(fullJourneyCompleteRideTime));
}

-(void) cp_tbtHEADING_To:(int)liveFlag andString:(NSString*)strname{
    
    NSLog(@"CPWrapper: Heading To place name:%@",strname);

    if (strname != NULL){
        NSString *name = [self nullTerminatedString:strname];
        
        //Cast Nsstring to bytes array
        NSData *nameDATA = [name dataUsingEncoding:NSUTF8StringEncoding];
        const void *data_bytes = [nameDATA bytes];
        
        CP_HeadingToTypeDef heading;
        heading.liveFlag = liveFlag;
        heading.stringInfo.letterCount = (uint8_t)nameDATA.length ;
        
        //create a buufer of size structure and string length
        unsigned char *buff = (unsigned char *)malloc(sizeof(heading) + name.length);
        
        //first copy struct to buffer
        memcpy((void*)buff, &heading, sizeof(heading));
        
        //after structure append string to buffer
        memcpy((void*)(buff+sizeof(heading)),(void*)data_bytes,name.length);
        
        CP_SetHeadingToDestination(session, src_id, session_id, buff, sizeof(heading) + name.length);
        free(buff);
    }
}

#pragma mark Round About Turns

-(void) cp_currentIconRoundAboutBearingInfo:(int)liveFlag exitNumer:(int)exitNumber andAngle:(int)angle{
    NSLog(@"CPWrapper: Current Roundabout Exit Send:%d",exitNumber);

    CP_RoundAboutBearingAngleToTypeDef info;
    info.exitNumber = exitNumber;
    info.bearingAngle.bearingAngleDegree = angle;
    
    CP_CurrentIconRoundAboutBearingInfo(session, src_id, session_id, &info, sizeof(info));
    
}

-(void) cp_nextIconRoundAboutBearingInfo:(int)liveFlag exitNumer:(int)exitNumber andAngle:(int)angle{
    NSLog(@"CPWrapper: Next Roundabout Exit Send:%d",exitNumber);

    CP_RoundAboutBearingAngleToTypeDef info;
    info.exitNumber = exitNumber;
    info.bearingAngle.bearingAngleDegree = angle;
    
    CP_NextIconRoundAboutBearingInfo(session, src_id, session_id, &info, sizeof(info));
    
}

#pragma mark Deviation calls
///#################   Deviation Alerts ####################

/*****************************************************************************
 * Function :   cp_setDeviationData
 * Input    :   angle,distance,liveFlag,scaleType & unit
 * Output   :   none
 * Comment  :   Set data over DM (Deviation info)
 *
 ****************************************************************************/
-(void)cp_setDeviationAlertData:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit andAngle:(int)angle{
    
    NSLog(@"CPWrapper: Devaiation Alert Data Send: distance:%d",distance);

    CP_RouteDeviationBearingAngleTypeDef deviationInfo;
    deviationInfo.liveFlag  = liveFlag;
    deviationInfo.distance  = distance;
    deviationInfo.scale     = scaleType;
    deviationInfo.unit      = unit;
    deviationInfo.bearingAngle.bearingAngleDegree = angle;
    
    CP_RouteDeviationBearingInfo(session, src_id, session_id, &deviationInfo, sizeof(deviationInfo));
}

/*****************************************************************************
 * Function :   cp_showDeviationAlert
 * Input    :   control
 * Output   :   none
 * Comment  :   To Show DM Screen (Deviation info)
 *
 ****************************************************************************/
-(void)cp_showDeviationAlert:(int)control{

    NSLog(@"CPWrapper: Devaiation Alert send, SHOW:%d",control);

    CP_RouteDeviationStartTypeDef deviationControl;
    deviationControl.startStop = control;
    
    CP_RouteDeviationStart(session, src_id, session_id, &deviationControl, sizeof(deviationControl));
}



#pragma mark Alert calls
///#################   Incoming Alerts ####################

/*****************************************************************************
 * Function :   cp_showNonUrgentIncomingAlert
 * Input    :   Sturcture object
 * Output   :   none
 * Comment  :   Send data over DM (Showing alert)
 *
 ****************************************************************************/
-(void)cp_showNonUrgentIncomingAlert:(int)alertID andAlertType:(CP_AlertTypeInfoTypeDef)type andUUID:(int)uuid andStructure:(id)object {
    
    CP_IncomingAlertTypeDef alert;
    alert.alertID = alertID;
    alert.alertType = type;
    alert.alertUuid = uuid;
    alert.alertPayloadSize = sizeof(object);
    alert.expirationType.expirationEventTypeDefinition = (ALERT_EXPIRY_ENABLE_APP_CONTROLLED_BIT | ALERT_EXPIRY_ENABLE_TIMER_CONTROLLED_BIT);
    //create a buufer of size structure and string length
    unsigned char *buff = (unsigned char *)malloc(sizeof(object)+sizeof(alert));
    
    //first copy struct to buffer
    memcpy((void*)buff, &alert, sizeof(alert));
    
    //after structure append string to buffer
    memcpy((void*)(buff+sizeof(alert)),(void*)&object,sizeof(object));
    
    CP_IncomingAlerts(session, src_id, session_id, buff, sizeof(alert)+sizeof(object));
}

/*****************************************************************************
 * Function :   cp_showUrgentIncomingAlert
 * Input    :   Sturcture object & name
 * Output   :   none
 * Comment  :   Send data over DM (Showing alert)
 *
 ****************************************************************************/

-(void)cp_showUrgentIncomingAlert:(int)alertID andAlertType:(CP_AlertTypeInfoTypeDef)type andUUID:(int)uuid andName:(NSString*)strname{
    
    NSString *name = [self nullTerminatedString:strname];
    
    //Cast Nsstring to bytes array
    NSData *nameDATA = [name dataUsingEncoding:NSUTF8StringEncoding];
    const void *data_bytes = [nameDATA bytes];
    
    CP_UserNameTypeDef objUsername;
    objUsername.liveFlag = 1;
    objUsername.stringInfo.letterCount = (uint8_t)nameDATA.length ;
    
    ///**** Incoming Structure ****///
    CP_IncomingAlertTypeDef alert;
    alert.alertID = alertID;
    alert.alertType = type;
    alert.alertUuid = uuid;
    alert.alertPayloadSize = sizeof(objUsername);
    
    //create a buufer of size structure and string length
    unsigned char *buff = (unsigned char *)malloc(sizeof(objUsername)+sizeof(alert)+name.length);
    
    //Copy first struct to buffer
    memcpy((void*)buff, &alert, sizeof(alert));
    //Copy first struct to buffer
    memcpy((void*)(buff+sizeof(alert)),(void*)&objUsername,sizeof(objUsername));
    //after structure append string to buffer
    memcpy((void*)(buff+sizeof(alert)+sizeof(objUsername)),(void*)data_bytes,name.length);
    
    //Call to alert Command
    CP_IncomingAlerts(session, src_id, session_id, buff, sizeof(alert)+sizeof(objUsername)+name.length);
    
}

/*****************************************************************************
 * Function :   CP_AlertResponse_Incoming
 * Input    :   Alert ID,Type,Status,UUID
 * Output   :   none
 * Comment  :   Send data over DM (Showing alert status)
 *
 ****************************************************************************/
-(void)CP_AlertResponse_Incoming:(int)alertID andAlertType:(CP_AlertTypeInfoTypeDef)type andUUID:(int)uuid {
    
    CP_OutgoingAlertInfoTypeDef outgoingAlert;
    outgoingAlert.alertType = type;
    outgoingAlert.alertID = alertID;
    
    //Call to alert Command
    CP_AlertResponse(session, src_id, session_id, &outgoingAlert, sizeof(outgoingAlert));
}

/*****************************************************************************
 * Function :   CP_AlertResponseSent_Incoming
 * Input    :   Alert ID,Type,Status,UUID
 * Output   :   none
 * Comment  :   Send data over DM (Showing alert status)
 *
 ****************************************************************************/
-(void)CP_AlertResponseSent_Incoming:(int)alertID andAlertType:(CP_AlertTypeInfoTypeDef)type{
    
    CP_OutgoingAlertInfoTypeDef outgoingAlert;
    outgoingAlert.alertType = type;
    outgoingAlert.alertID = alertID;
    
    //Call to alert Command
    CP_AlertResponseSent(session, src_id, session_id, &outgoingAlert, sizeof(outgoingAlert));
}

/*****************************************************************************
 * Function :   CP_AlertResponseFailed_Incoming
 * Input    :   Alert ID,Type,Status,UUID
 * Output   :   none
 * Comment  :   Send data over DM (Showing alert status)
 *
 ****************************************************************************/
-(void)CP_AlertResponseFailed_Incoming:(int)alertID andAlertType:(CP_AlertTypeInfoTypeDef)type {
    
    CP_OutgoingAlertInfoTypeDef outgoingAlert;
    outgoingAlert.alertType = type;
    outgoingAlert.alertID = alertID;
    
    //Call to alert Command
    CP_AlertResponseFailed(session, src_id, session_id, &outgoingAlert, sizeof(outgoingAlert));
}

/*****************************************************************************
 * Function :   CP_AlertEnd_Incoming
 * Input    :   Alert ID,Type,Status,UUID
 * Output   :   none
 * Comment  :   Send data over DM (Showing alert status)
 *
 ****************************************************************************/
-(void)CP_AlertEnd_Incoming:(int)alertID andType:(CP_AlertTypeInfoTypeDef)type andUUID:(int)uuid{
    
    CP_IncomingAlertClosureTypeDef alert;
    alert.alertID   = alertID;
    alert.alertType = type;
    alert.alertUuid = uuid;
    
    //Call to alert Command
    CP_IncomingAlertEnd(session, src_id, session_id, &alert, sizeof(alert));
}


///#################   OutGoing Alerts ####################

/*****************************************************************************
 * Function :   CP_OutgoingAlerts
 * Input    :   Alert ID,Type,Status,UUID
 * Output   :   none
 * Comment  :   Send data over DM (Showing alert status)
 *
 ****************************************************************************/
-(void)CP_OutgoingAlerts:(int)alertID andAlertType:(CP_AlertTypeInfoTypeDef)type{
    
    CP_OutgoingAlertInfoTypeDef outgoingAlert;
    outgoingAlert.alertType = type;
    outgoingAlert.alertID = alertID;
    
    //Call to alert Command
    CP_OutgoingAlerts(session, src_id, session_id, &outgoingAlert, sizeof(outgoingAlert));
}

/*****************************************************************************
 * Function :   CP_AlertFailed_Outgoing
 * Input    :   Alert ID,Type,Status,UUID
 * Output   :   none
 * Comment  :   Send data over DM (Showing alert status)
 *
 ****************************************************************************/
-(void)CP_AlertFailed_Outgoing:(CP_AlertSentTypeDef)status{
    
    CP_AlertSentStatusTypeDef sentAlert;
    sentAlert.alertSent = status;
    
    //Call to alert Command
    CP_AlertFailed(session, src_id, session_id, &sentAlert, sizeof(sentAlert));
}

/*****************************************************************************
 * Function :   CP_Alertsent_Outgoing
 * Input    :   Alert ID,Type,Status,UUID
 * Output   :   none
 * Comment  :   Send data over DM (Showing alert status)
 *
 ****************************************************************************/
-(void)CP_Alertsent_Outgoing:(int)alertID andStatus:(CP_AlertSentTypeDef)status andAlertType:(CP_AlertTypeInfoTypeDef)alertType
{
    
    ///**** Incoming Structure ****///
    CP_AlertSentStatusTypeDef sentAlert;
    sentAlert.alertSent = status;
    sentAlert.alertID = alertID;
    sentAlert.alertType = alertType;
    //Call to alert Command
    CP_Alertsent(session, src_id, session_id, &sentAlert, sizeof(sentAlert));
}
/*****************************************************************************
 * Function :   CP_AlertSentResponse_Outgoing
 * Input    :   Alert ID,Type,Status,UUID
 * Output   :   none
 * Comment  :   Send data over DM (Showing alert status)
 *
 ****************************************************************************/
-(void)CP_AlertSentResponse_Outgoing:(int)alertID andUUID:(int)uuid andStatus:(CP_ResponseTypeDef)status{
    
    CP_AlertSentResponseTypeDef response;
    response.alertUuid = uuid;
    response.sentResponse = status;
    
    //Call to alert Command
    CP_AlertSentResponse(session, src_id, session_id, &response, sizeof(response));
}

/*****************************************************************************
 * Function :   CP_AlertEnd_Outgoing
 * Input    :   Alert ID,Type,Status,UUID
 * Output   :   none
 * Comment  :   Send data over DM (Showing alert status)
 *
 ****************************************************************************/
-(void)CP_AlertEnd_Outgoing:(CP_AlertSentTypeDef)status{
    
    CP_AlertSentStatusTypeDef outgoingAlertStatus;
    outgoingAlertStatus.alertSent = status;
    
    //Call to alert Command
    CP_OutgoingAlertEnd(session, src_id, session_id, &outgoingAlertStatus, sizeof(outgoingAlertStatus));
}

/*****************************************************************************
 * Function :   CP_AlertNOHelpReceived_Outgoing
 * Input    :   Status
 * Output   :   none
 * Comment  :   Send data over DM (Showing alert status)
 *
 ****************************************************************************/

-(void)CP_AlertNOHelpReceived_Outgoing{
    CP_AlertSentStatusTypeDef outgoingAlertStatus;
    outgoingAlertStatus.alertSent = 0;
    
    //Call to alert Command
    CP_AlertSentNoHelpReceived(session, src_id, session_id, &outgoingAlertStatus, sizeof(outgoingAlertStatus));
}

#pragma DMBacklight command


/*****************************************************************************
 * Function :   CP_DMBacklightMode
 * Input    :   CP_BacklightModeTypeDef
 * Output   :   none
 * Comment  :
 *
 ****************************************************************************/
-(void)CP_DMBacklightMode:(CP_BacklightModeTypeDef)mode{
    
    CP_BacklightModeDataTypeDef blmode;
    blmode.blMode = mode;
    CP_SetBacklightMode(session, src_id, session_id, &blmode, sizeof(blmode));
}

/*****************************************************************************
 * Function :   CP_DMBacklightControlMode
 * Input    :   CP_BacklightControlModeTypeDef
 * Output   :   none
 * Comment  :
 *
 ****************************************************************************/
-(void)CP_DMBacklightControlMode:(CP_BacklightControlModeTypeDef)control_mode{
    
    CP_SetBacklightControlModeDataTypeDef blctrlmode;
    blctrlmode.blControlMode = control_mode;
    CP_SetBacklightMode(session, src_id, session_id, &blctrlmode, sizeof(blctrlmode));
    
}

/*****************************************************************************
 * Function :   CP_DMBacklightBrightNess
 * Input    :   CP_BrightnessLevelTypeDef ,CP_BrightnessLevelTypeDef
 * Output   :   none
 * Comment  :
 *
 ****************************************************************************/
-(void)CP_DMBacklightBrightNess:(CP_BrightnessLevelTypeDef)maxBrightnessTwilght NightMode:(CP_BrightnessLevelTypeDef)maxBrightnessNight  {
    
    CP_MaxBacklightBrightnessDataTypeDef maxBrightnessData;
    maxBrightnessData.maxBrightnessTwilightMode = maxBrightnessTwilght;
    maxBrightnessData.maxBrightnessNightMode = maxBrightnessNight;
    
    CP_SetMaxBacklightBrightness(session, src_id, session_id, &maxBrightnessData, sizeof(maxBrightnessData));
    
}

/*****************************************************************************
 * Function :   CP_DMBacklightOnOff
 * Input    :   CP_BacklightStatusTypeDef
 * Output   :   none
 * Comment  :
 *
 ****************************************************************************/
-(void)CP_DMBacklightOnOff:(CP_BacklightStatusTypeDef)on_off {
    
    CP_BacklightStatusInfoTypeDef on_off_control;
    on_off_control.backlightOnOff = on_off;
    CP_SetBacklightMode(session, src_id, session_id, &on_off_control, sizeof(on_off_control));
    
}

/*****************************************************************************
 * Function :   CP_WeakGPSSignal
 * Input    :   CP_GpsStatusTypeDef
 * Output   :   none
 * Comment  :
 *
 ****************************************************************************/
-(void)CP_WeakGPSSignal:(CP_GpsStatusTypeDef)gps_status {
    
    CP_GpsStatusInfoTypeDef gpsStatus;
    gpsStatus.statusInfo = gps_status;
    CP_GpsStatusInfo(session, src_id, session_id, &gpsStatus, sizeof(gpsStatus));
}


/*!
 * @param string
 * Actual string to convert as input
 *
 * @discussion
 * It will return string with adding '\0' at the end for creating NSstring as Null terminated.
 *
 */
-(NSString *)nullTerminatedString:(NSString *)string{
    NSString *str = [string stringByAppendingString:@"\0"];
    return str;
}

#pragma mark CALL BACKS

void callNativeMethod(void * data, int data_size)
{
    NSLog(@"callNativeMethod");
    
    NSData *nsData = [NSData dataWithBytes:data length:data_size];
    [bridgeInstance onAckReceiveSwiftAPI:nsData andLenght:data_size];
}

void callNativeMethodACK(int commandType, int status)
{
    NSLog(@"callNativeMethodACK");
    [bridgeInstance onCallBackSwiftAPI:commandType andStatus:status];
}

void  callNativeMethodInd ( void* data, int size) {
    NSLog(@"callback indication");
    [bridgeInstance onCallBackSwiftAPI:size andStatus:0];
}

- (void)onCallBackSwiftAPI:(int) commandType andStatus: (int)status
{
    NSLog(@"onCallBackSwiftAPI");
    [_delegate onCallBackData:commandType andStatus:status];
}


- (void)onAckReceiveSwiftAPI:(NSData *)data andLenght:(int) length
{
    NSLog(@"onAckReceiveSwiftAPI");
    [_delegate onAckReceive:data inMethod:length];
    
}

+ (void)Destructor{
    cpWrapperObj = nil;
}

/*****************************************************************************
 * Function :   onCallBackDMAlertbtnPress
 * Input    :   none
 * Output   :   Send data to UI
 * Comment  :   Called when DM side Key press
 ****************************************************************************/
-(void)onCallBackDMAlertbtnPress:(UInt8)alertID andType:(CP_AlertTypeInfoTypeDef)alertType{
    [_delegate onDMKeyOutgoingAlertToApp:alertID andType:alertType];
}

-(void)updateDMBatteryChagingState:(CP_DmChargingTypeDef) state{
    [_delegate updateDmBatteryChargingStatus:state];
}

-(void)onCallBackDMAlertbtnPress_Incoming:(int)uuid andStatus:(CP_ResponseTypeDef)status {
    
    if (status == 0){
        NSLog(@"Rejected");
        [_delegate onDMKeyPressIncomingAlert:0 andUUID:uuid andIsAccepted:NO];
    }else{
        NSLog(@"Accepted");
        [_delegate onDMKeyPressIncomingAlert:0 andUUID:uuid andIsAccepted:YES];
    }
}


/*****************************************************************************
 * Function :   onAlertModeEntryEvent
 * Input    :   none
 * Output   :   Send data to UI
 * Comment  :   Called when DM side Alert Mode Entered
 ****************************************************************************/


-(void)onAlertModeEntryEvent {
    [_delegate onAlertModeChange: YES];
}


/*****************************************************************************
 * Function :   onAlertModeExitEvent
 * Input    :   none
 * Output   :   Send data to UI
 * Comment  :   Called when DM side Alert Mode Exit
 ****************************************************************************/


-(void)onAlertModeExitEvent {
    [_delegate onAlertModeChange: NO];
    
}


/*****************************************************************************
 * Function :   onCallBackOutgoingAlertClosed
 * Input    :   none
 * Output   :   Send data to UI
 * Comment  :   Called when DM side double tap on No Response Received Screen
 ****************************************************************************/


-(void)onCallBackOutgoingAlertClosed:(UInt8)alertID andType:(CP_AlertTypeInfoTypeDef)alertType{
    
    [_delegate onDMKeyAlertClosed:alertID andType:alertType];
}


/*****************************************************************************
 * Function :   callBackon_cp_callback_ind
 * Input    :   none
 * Output   :   Send data to UI
 * Comment  :   This Method called when any event happens on DM Side
 ****************************************************************************/
void callBackon_cp_callback_ind(struct kbike_communication_protocol *session, uint8_t commandGroup, uint8_t commandId, void * data, uint32_t size){
    
    switch(commandGroup){
        case COMMUNICATION_PROTOCOL_CMD_GRP_DM_NEW_PARAMETERS:
            processBatteryCommand(commandId, data, size);
            break;
            
        case COMMUNICATION_PROTOCOL_CMD_GRP_ALERTS:
            processAlerts(commandId, data, size);
            break;
            
        case COMMUNICATION_PROTOCOL_CMD_GRP_DM_SYS_CONFIG_PARAMETERS:
            break;
            
        case COMMUNICATION_PROTOCOL_CMD_GRP_DM_NAVIGATION_CONTROL:
            break;
            
        case COMMUNICATION_PROTOCOL_CMD_GRP_DM_FIRMWARE_UPDATE:
            break;
            
        case COMMUNICATION_PROTOCOL_CMD_GRP_DM_HID:
            processKeyPress(commandId, data, size);
            break;
            
        default:
            break;
    }
}

/*!
 * @param commandId
 * Type of Event occured on DM side
 *
 * @param data
 * Contains data of Event
 *
 * @discussion
 * Called when the any Key press happens on DM side.
 *
 */
void processKeyPress(uint8_t commandId, void * data, uint32_t size){
    
    unsigned char dataReceived[size];
    unsigned char status;
    
    switch(commandId){
            
        case COMMUNICATION_PROTOCOL_CMD_KEY_PRESS:
            memcpy((void *) &dataReceived, data, size);
            status = *(dataReceived + 0);
            break;
            
        default:
            break;
    }
}

/*!
 * @param commandId
 * Type of Event occured on DM side
 *
 * @param data
 * Contains data of Event
 *
 * @discussion
 * Called when the Battery Status action generated from DM side.
 *
 */
void processBatteryCommand(uint8_t commandId, void * data, uint32_t size){
    unsigned char dataReceived[size];
    unsigned char status;
    CP_DmBatteryChargingStatusTypeDef dmChargingOnOff;
    
    switch(commandId){
            
        case COMMUNICATION_PROTOCOL_CMD_DM_BATTERY_CHARGING_ON_OFF:
            memcpy((void *) &dataReceived, data, sizeof(dmChargingOnOff));
            status = *(dataReceived + 0);
            [bridgeInstance updateDMBatteryChagingState:status];

            break;
            
        default:
            break;
    }
}



/*!
 * @param commandId
 * Type of Event occured on DM side
 *
 * @param data
 * Contains data of Event
 *
 * @discussion
 * Called when the Alert generated from DM side.
 *
 */

void processAlerts(uint8_t commandId, void * data, uint32_t size){
    
    switch(commandId){
            
        case COMMUNICATION_PROTOCOL_CMD_OUTGOING_ALERT:
        {
            CP_OutgoingAlertInfoTypeDef outgoingAlert;
            memcpy((void *) &outgoingAlert, data, size);
            [bridgeInstance onCallBackDMAlertbtnPress:outgoingAlert.alertID andType:outgoingAlert.alertType];
            break;
        }
            
        case COMMUNICATION_PROTOCOL_CMD_ALERT_RESPONSE:
        {
            CP_AlertResponseTypeDef incomingAlertRsp;
            memcpy((void *) &incomingAlertRsp, data, sizeof(incomingAlertRsp));
            [bridgeInstance onCallBackDMAlertbtnPress_Incoming:incomingAlertRsp.alertUuid andStatus:incomingAlertRsp.response];
            break;
        }
            
        case COMMUNICATION_PROTOCOL_CMD_OUTGOING_ALERT_MODE_ENTRY:{
            [bridgeInstance onAlertModeEntryEvent];
            break;
        }
            
        case COMMUNICATION_PROTOCOL_CMD_OUTGOING_ALERT_MODE_EXIT:{
            [bridgeInstance onAlertModeExitEvent];
            break;
        }
            
        case COMMUNICATION_PROTOCOL_CMD_OUTGOING_ALERT_CLOSED:{
            CP_OutgoingAlertInfoTypeDef outgoingAlert;
            memcpy((void *) &outgoingAlert, data, size);
            [bridgeInstance onCallBackOutgoingAlertClosed:outgoingAlert.alertID andType:outgoingAlert.alertType];
            break;
        }
            
        default:
            break;
    }
}
@end
