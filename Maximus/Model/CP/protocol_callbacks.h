#ifndef PROTOCOL_CALLBACKS_H
#define PROTOCOL_CALLBACKS_H

#include "cp.h"
#include <string.h>

#define  LOG_TAG   "communication-protocol"

/* Callback function prototypes */

void show_error(uint8_t status);

/**** Transport Callback ****/
bool try_send(void *, size_t );

/****** Newly Added callback *****/
//bool cp_callback_ind (struct kbike_communication_protocol *session);
bool callback_ind(struct kbike_communication_protocol *session, uint8_t commandGroup, uint8_t commandId, void * data, uint32_t size);

void cp_callback_cfm (struct kbike_communication_protocol *, uint8_t);

void communicateSensorData(int sensor_type, int sensor_data);
void communicateALSSensorData(int sensor_type, int sensor_data);
void communicateProximitySensorData(int sensor_type, int sensor_data);
void communicateAUXSensorData(int sensor_type, int sensor_data);


#endif //PROTOCOL_CALLBACKS_H
