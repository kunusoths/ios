//
//  CPWrapper.h
//  Maximus
//
//  Created by Namdev Jagtap on 13/07/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cp_if.h"
#import "cp.h"
#import "protocol_callbacks.h"

#define SESSION_FALSE   0
#define SESSION_TRUE    1

// Protocol definition starts here
@protocol cp_ProtocolDelegate
@required
- (void)returnData:(NSString*)data inMethod:(NSString*)method;
- (void)onCallBackData:(int)commandType andStatus:(int)status;
- (void)onAckReceive:(NSData *)data inMethod:(int)length;
- (void)onDMKeyPressIncomingAlert:(UInt8)alertID andUUID:(int)uuid andIsAccepted:(Boolean)status;
-(void)updateDmBatteryChargingStatus:(CP_DmChargingTypeDef) status;
@optional
- (void)onDMKeyPressEvent:(int)key_event andsensor_data:(int) sensor_data;
- (void)onDMKeyOutgoingAlertToApp:(UInt8)alertID andType:(CP_AlertTypeInfoTypeDef)alertType;

- (void)onChargingStatusToAPP:(int)alertID andType:(int)alertType;
- (void)onAlertModeChange:(Boolean)alertMode;
- (void)onDMKeyAlertClosed:(UInt8)alertID andType:(CP_AlertTypeInfoTypeDef)alertType;

//- (void)onSensorUpdate:(int)sensor_type andsensor_data:(int) sensor_data;
//- (void)onDMKeyPressEvent:(int)key_event;
//- (void)onDMKeyPressEvent:(int)key_event andsensor_data:(int) sensor_data;
@end
// Protocol Definition ends here


@interface CPWrapper : NSObject
{
    
}

@property (weak, nonatomic) id<cp_ProtocolDelegate> delegate;

+ (instancetype)sharedInstance;
+ (void)Destructor;


//- (void)onCallBack:(Byte[])data andLenght:(int) length;
//- (void)onAckReceive:(Byte[])data andLenght:(int) length;
-(NSString *)nullTerminatedString:(NSString *)string;

// DM Firmware Update Start
-(void) cp_firmwareUpdateStart;

//Source Name
-(void) CP_SourceName:(int)liveFlag andSourceName:(NSString*)name;

//Daily targated destination
-(void) cp_dailyTargatedDestinationName:(int)liveFlag andNextCity:(NSString*)name;

//Destination Name
-(void) CP_DestinationName:(int)liveFlag andDestinationName:(NSString*)name;

//Full Jopurney Distance
-(void) CP_FullJourneyDistance:(int)liveFlag  andDistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit;

//Initialize Communication Protocol
- (int)  cp_initialize:(int)imtu andOmtu:(int)omtu;

// Release Communication Protocol
- (void) releaseCommunicationProtocol;

//Set Date and Time on Display Module
- (Boolean) cp_set_date_time:(int)HH andMIN:(int)MIN andSS:(int)SS andDD:(int)DD andMM:(int)MM andYY:(int)YY andAmPm:(int)ampm andTmeFormat:(int)format andWeekDay:(int)weekDay;

- (void) cp_receive_payload:(NSData*)data;
- (void) cp_update_vehicle_battery_voltage:(int)liveFlag andvoltage:(int)voltage andscaleType:(int)scaleType andunit:(int)unit;
- (void) cp_update_source_info:(int)liveFlag andstringLength:(int)stringLength andsourceName:(Byte[])sourceName;
- (void) cp_update_destination_info:(int)liveFlag andstringLength:(int)stringLength anddestinationName:(Byte[])destinationName;
- (void) cp_update_full_journey_eta:(int)liveFlag andhour:(int)hour andminute:(int)minute;
- (void) cp_update_full_journey_distance_unit:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit;
- (void) cp_update_poi_eta:(int)liveFlag andhour:(int)hour andminute:(int)minute;
- (void) cp_update_poi_distance_unit:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit;
- (void) cp_update_daily_eta:(int)liveFlag andhour:(int)hour andminute:(int)minute;
- (void) cp_update_daily_eta_distance_unit:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit;

//Send current manuever icon to Display Module
- (void) cp_update_current_manuever_icon:(int)liveFlag andiconID:(int)iconID;

//Send current manuever distance to Display Module
- (void) cp_update_current_manuever_distance_unit:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit;

//Send next manuever icon to Display Module
- (void) cp_update_next_manuever_icon:(int)liveFlag andiconID:(int)iconID;

//Send next manuever distance to Display Module
- (void) cp_update_next_manuever_distance_unit:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit;

- (void) cp_update_vehicle_fuel_level:(int)liveFlag andfuelLevel:(int)fuelLevel andscaleType:(int)scaleType andunit:(int)unit;
- (void) cp_update_vehicle_milage_distance:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit;
- (void) cp_update_total_traveled_distance:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit;
- (void) cp_update_daily_traveled_distance:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit;
- (void) cp_update_total_journey_remaining_distance:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit;
- (void) cp_update_total_journey_eta:(int)liveFlag andhour:(int)hour andminute:(int)minute;
- (void) cp_update_total_journey_eta_unit:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit;
- (void) cp_update_daily_journey_eta:(int)liveFlag andhour:(int)hour andminute:(int)minute andAmPm:(int)ampm andTmeFormat:(int)format;
- (void) cp_update_daily_journey_eta_unit:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit;


/************************************************************************************
 * API - dm alerts and warnings Commands - START
 *  	Commands Supported:
 *     journey start stop
 *     daily journey startstop
 *     full journey start stop
 *     tbt enable and disable
 ***********************************************************************************/

//
-(void) cp_setJourney:(int)liveFlag andJourneyOnOff:(int)journeyOnOff ;

// Set Daily Journey Start/Stop
-(void) cp_dailyJourneyStartStop:(int)liveFlag andJourneyStartStop:(int)journeyStartStop;

// Set  Journey Start/Stop
-(void) cp_fullJourneyStartStop:(int)liveFlag andJourneyStartStop:(int)journeyStartStop;

//Set TBT Start / Stop
-(void) cp_tbtStartStop:(int)liveFlag andTBTStartStop:(int)TBTStartStop;

//Show / Hide TBT
-(void) cp_tbtHideShow:(int)liveFlag andTBTHideShow:(int)TBTHideShow;

//Straight Arrow
-(void)cp_followStraightRoad;
-(void)cp_approachingDestination;
-(void)cp_approachingPoi;

//Heading Instruction
-(void) cp_tbtHEADINGDirection:(int)liveFlag  andString:(NSString*)name;
-(void) cp_tbtHEADINGDistance:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit;
-(void) cp_tbtHEADING_To:(int)liveFlag andString:(NSString*)name;
-(void) cp_tbtHEADINGStart:(int)liveFlag andSTARTStatus:(CP_HeadingStatusTypeDef)status;
-(void) cp_headingBearingAngle:(int)liveFlag andAngle:(int) angle;

//Round About icons
-(void) cp_currentIconRoundAboutBearingInfo:(int)liveFlag exitNumer:(int)exitNumber andAngle:(int)angle;
-(void) cp_nextIconRoundAboutBearingInfo:(int)liveFlag exitNumer:(int)exitNumber andAngle:(int)angle;


//Alert

/////Incoming ALERTS
-(void)cp_showNonUrgentIncomingAlert:(int)alertID andAlertType:(CP_AlertTypeInfoTypeDef)type andUUID:(int)uuid  andStructure:(id)object;
-(void)cp_showUrgentIncomingAlert:(int)alertID andAlertType:(CP_AlertTypeInfoTypeDef)type andUUID:(int)uuid andName:(NSString*)name;
-(void)CP_AlertResponse_Incoming:(int)alertID andAlertType:(CP_AlertTypeInfoTypeDef)type andUUID:(int)uuid ;
-(void)CP_AlertResponseSent_Incoming:(int)alertID andAlertType:(CP_AlertTypeInfoTypeDef)type;
-(void)CP_AlertResponseFailed_Incoming:(int)alertID andAlertType:(CP_AlertTypeInfoTypeDef)type;
-(void)CP_AlertEnd_Incoming:(int)alertID andType:(CP_AlertTypeInfoTypeDef)type andUUID:(int)uuid;

/////Out Going ALERTS
-(void)CP_OutgoingAlerts:(int)alertID andAlertType:(CP_AlertTypeInfoTypeDef)type;
-(void)CP_AlertFailed_Outgoing:(CP_AlertSentTypeDef)status;
-(void)CP_Alertsent_Outgoing:(int)alertID andStatus:(CP_AlertSentTypeDef)status andAlertType:(CP_AlertTypeInfoTypeDef)alertType;
-(void)CP_AlertSentResponse_Outgoing:(int)alertID andUUID:(int)uuid andStatus:(CP_ResponseTypeDef)status;
-(void)CP_AlertEnd_Outgoing:(CP_AlertSentTypeDef)status;
-(void)CP_AlertNOHelpReceived_Outgoing;

//Time related commands
-(void) cp_DailyCompletedRideTime:(int) liveFlag minutes:(int)minutes hours:(int)hours;
-(void) cp_FullJourneyCompletedRideTime:(int)liveFlag minutes:(int)minutes hours:(int)hours days:(int) days;
//Deviation alert
-(void)cp_setDeviationAlertData:(int)liveFlag anddistance:(int)distance andscaleType:(int)scaleType andunit:(int)unit andAngle:(int)angle;
-(void)cp_showDeviationAlert:(int)control;
//DM Backling & Weak Signal Commands
-(void)CP_DMBacklightMode:(CP_BacklightModeTypeDef)mode;
-(void)CP_DMBacklightControlMode:(CP_BacklightControlModeTypeDef)control_mode;
-(void)CP_DMBacklightBrightNess:(CP_BrightnessLevelTypeDef)maxBrightnessTwilght NightMode:(CP_BrightnessLevelTypeDef)maxBrightnessNight;
-(void)CP_DMBacklightOnOff:(CP_BacklightStatusTypeDef)on_off;
-(void)CP_WeakGPSSignal:(CP_GpsStatusTypeDef)gps_status;
@end
