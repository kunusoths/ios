#include "protocol_callbacks.h"
#include <syslog.h>
#include <stdio.h>


#define ICON_SUCCESS "01"
#define ICON_FAIL "00"

#define TEXT_SUCCESS "11"
#define TEXT_FAIL "10"

#define LED_RED_SUCCESS "21"
#define LED_RED_FAIL "20"

#define RGB_SUCCESS "31"
#define RGB_FAIL "30"

#define OLED_SUCCESS "41"
#define OLED_FAIL "40"

#define BUZZER_SUCCESS "51"
#define BUZZER_FAIL "50"

#define FW_UPDATE_INIT_SUCCESS "61"
#define FW_UPDATE_INIT_FAIL "60"

#define SLEEP_MODE_SUCCESS "71"
#define SLEEP_MODE_FAIL "70"

#define SET_TEXT_COLOR_SUCCESS "81"
#define SET_TEXT_COLOR_FAIL "80"

/***** Externs ******/
extern void callNativeMethod(void * data, int data_size);
extern void callNativeMethodACK(void * data, int data_size);
extern void callNativeMethodInd(void * data, int data_size);

extern void callBackon_cp_callback_ind(struct kbike_communication_protocol *session, uint8_t commandGroup, uint8_t commandId, void * data, uint32_t size);

/************* Command Indication Callback implementation ************/
bool callback_ind(struct kbike_communication_protocol *session, uint8_t commandGroup, uint8_t commandId, void * data, uint32_t size){
    //printf("#################### cp_callback_ind Success ");
    callBackon_cp_callback_ind(session, commandGroup, commandId, data, size);
    callNativeMethodInd(data, size);
    return TRUE;
}


/************* Command Confirmation Callback implementation ************/
void cp_callback_cfm (struct kbike_communication_protocol *session, uint8_t status){
  
    int cmd_grp = session->msginfo.cmd_grp;
    int cmd_id = session->msginfo.cmd;
    if (status == COMMUNICATION_PROTOCOL_SUCCESS_ACK) {
        fprintf(stdout,"\n[cfm]-> cp_callback_cfm - success - Command Grp: %x Command ID: %x", cmd_grp, cmd_id);
        callNativeMethodACK(SLEEP_MODE_SUCCESS, 2);
    }
    else {
        fprintf(stdout,"\n[cfm]-> cp_callback_cfm - fail - Command Grp: %x Command ID: %x", cmd_grp, cmd_id);
        callNativeMethodACK(SLEEP_MODE_FAIL, 1);
    }
}

/************* Transport Callback Implementation ****************/
bool try_send(void *data, size_t len) {
    int i;
    unsigned  char* str = (unsigned char*)data;
    for(i = 0; i < len; i++)
    {
        fprintf(stdout," %x ",*(str + i));
    }
    
    callNativeMethod(data,(int)len);
    return TRUE;
}

void show_error(uint8_t status) {
	switch (status) {
	case COMMUNICATION_PROTOCOL_BAD_HEADER_FORMAT:
		//printf("Bad Header format\n");
		break;
	case COMMUNICATION_PROTOCOL_BAD_LENGTH:
		//printf("Bad packet length\n");
		break;
	case COMMUNICATION_PROTOCOL_INVALID_COMMAND:
		//printf("Invalid Command\n");
		break;
	case COMMUNICATION_PROTOCOL_NO_BUFFER_SPACE:
		//printf("Insufficient IN Buffer Space\n");
		break;
	default:
		break;
	}
}
