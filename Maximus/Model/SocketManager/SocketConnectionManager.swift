//
//  SocketConnectionManager.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 12/01/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import Foundation
import Alamofire

@objc protocol SocketManagerDelegate {
    @objc func streamOpened()
    @objc func receivedInputSteram(bytes: [UInt8])
    @objc func writeToSocketFailed()
}

class SocketConnectionManager: NSObject , StreamDelegate {
    
    static let sharedInstance = SocketConnectionManager()
    
    weak var socketManagerDelegate:SocketManagerDelegate?
    
    var readStream: Unmanaged<CFReadStream>?
    var writeStream:Unmanaged<CFWriteStream>?
    
    var inputStream:InputStream?
    var outputStream:OutputStream?
    
    var host : CFString = "iot.maximuspro.com" as CFString
    var port : UInt32 = 443
    
    func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
        //
        
        var buffer = [UInt8](repeating: 0, count: 28)
        switch (eventCode){
            
        case Stream.Event.openCompleted:
            print("SocketConnectionManager : Stream opened")
            //      socketManagerDelegate?.streamOpened(bytes)
            break
            
        case Stream.Event.hasBytesAvailable:
            print("SocketConnectionManager : HasBytesAvailable")
            var responseData:Data = Data()
            if aStream == inputStream{
                
                while (inputStream?.hasBytesAvailable)! {
                    let len = inputStream?.read(&buffer, maxLength: buffer.count)
                    if(len! > 0){
                        responseData.append(&buffer, count: buffer.count)
                        socketManagerDelegate?.receivedInputSteram(bytes: Array(responseData))
                    }
                }
            }
            break
            
        case Stream.Event.errorOccurred:
            print("SocketConnectionManager : ErrorOccurred : \(aStream.streamError.debugDescription)")
            closeConnection()
            
            break
            
        case Stream.Event.endEncountered:
            print("SocketConnectionManager :EndEncountered")
            
            aStream.close()
            aStream.remove(from: .current, forMode: .defaultRunLoopMode)
            break
            
        case Stream.Event.hasSpaceAvailable:
            print("SocketConnectionManager : Space Available")
            
        default: break
        }
    }
    
    
    func writeToSocket(data: [UInt8]) {
        
        if let outputstream:OutputStream = outputStream {
            print("SocketManager : Stream is not nil \(outputstream.hasSpaceAvailable)")
            
            if outputStream?.streamStatus == Stream.Status.open {
                print("SocketManager : writing packet")
                let result = outputstream.write(data , maxLength: data.count)
                print("Write Status : \(result)")
            }else{
                //self.closeConnection()
                print("SocketManager : Stream is not open, connecting to host ")
                self.connectToServer()
                Thread.sleep(forTimeInterval: 0.5)
                socketManagerDelegate?.writeToSocketFailed()
            }
            
        }else{
            print("SocketManager : Stream is nil, connecting to host ")
            self.connectToServer()
            Thread.sleep(forTimeInterval: 0.5)
            socketManagerDelegate?.writeToSocketFailed()
        }
        
    }
    
    func connectToServer() {
        print("Setting up connection url:\(host) and port : \(port)")
        
        if (NetworkReachabilityManager()?.isReachable)! {
            CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, host, port, &readStream, &writeStream)
            self.openConnection()
        }else{
            print("No Internet")
        }
    }
    
    func openConnection() {
        
        print("opening stream")
        outputStream = writeStream?.takeRetainedValue()
        inputStream = readStream?.takeRetainedValue()
        
        if outputStream != nil {
            outputStream?.delegate = self
            outputStream?.schedule(in: .current, forMode: .defaultRunLoopMode)
            outputStream?.open()
        }else{
            print("output stream is nil")
        }
        
        if inputStream != nil {
            inputStream?.delegate = self
            inputStream?.schedule(in: .current, forMode: .defaultRunLoopMode)
            inputStream?.open()
        }else{
            print("input stream is nil")
        }
    }
    
    func disconnectServer() {
        self.closeConnection()
    }
    
    func closeConnection () {
        if (NetworkReachabilityManager()?.isReachable)! {
            print("closing stream")
            
            if outputStream != nil {
                outputStream?.close()
                outputStream?.remove(from: .current, forMode: .defaultRunLoopMode)
                outputStream?.delegate = nil
                outputStream = nil
            }else{
                print("input stream is nil")
                
            }
            
            if outputStream != nil {
                inputStream?.close()
                inputStream?.remove(from: .current, forMode: .defaultRunLoopMode)
                inputStream?.delegate = nil
                inputStream = nil
            }else{
                print("output stream is nil")
                
            }
        }
    }
    
    
}

