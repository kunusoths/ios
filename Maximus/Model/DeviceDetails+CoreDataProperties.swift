//
//  DeviceDetails+CoreDataProperties.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 05/10/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//
//

import Foundation
import CoreData


extension DeviceDetails {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DeviceDetails> {
        return NSFetchRequest<DeviceDetails>(entityName: "DeviceDetails")
    }

    @NSManaged public var app_uuid: String?
    @NSManaged public var app_version: String?
    @NSManaged public var date_registered: String?
    @NSManaged public var device_uuid: String?
    @NSManaged public var isSynced: Bool
    @NSManaged public var make: String?
    @NSManaged public var model: String?
    @NSManaged public var os_type: String?
    @NSManaged public var os_version: String?
    @NSManaged public var ram: Int16
    @NSManaged public var resolution: String?

}
