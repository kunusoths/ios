//
//  AchievementDataObj+CoreDataProperties.swift
//  Maximus
//
//  Created by Isha Ramdasi on 26/06/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//
//

import Foundation
import CoreData
import CoreLocation

extension AchievementDataObj {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AchievementDataObj> {
        return NSFetchRequest<AchievementDataObj>(entityName: "AchievementDataObj")
    }

    @NSManaged public var average_speed: Double
    @NSManaged public var distance: Double
    @NSManaged public var topspeedtimerange: Double
    @NSManaged public var top_speed: Double
    @NSManaged public var time_duration: Double
    @NSManaged public var last_processed_time: NSDate?
    @NSManaged public var distance_travelled: Double
    @NSManaged public var ride_end_date: String?
    @NSManaged public var ride_start_date: String?
    @NSManaged public var lastGoodLocation: NSObject?
    @NSManaged public var rideLocations: NSObject?
    @NSManaged public var rideID: String?
}

extension AchievementDataObj {
    class func fetchStatsWithRideId(rideId: String, context: NSManagedObjectContext) -> AchievementDataObj? {
        do {
            let statsFetch = NSFetchRequest<AchievementDataObj>(entityName: "AchievementDataObj")
            statsFetch.predicate = NSPredicate(format: "rideID == %@", rideId)
        
            let achievement = try context.fetch(statsFetch)
            if achievement.count > 0{
                return achievement.first
            } else {
                print("achievement count is 0")
                // Achievemt object is nill for rideId create a new and return
                let achievementObj = AchievementDataObj(context: context)
                achievementObj.rideID = rideId
                return achievementObj
                
            }
        } catch {
            
        }
        return nil
    }
    
    class func updateStatsObj(distanceTravelled: Double, travelledTime: TimeInterval, averageSpeed: Double, topSpeed: Double, lastGoodLocation: CLLocation, rideLocations: [CLLocation], rideID: String, context: NSManagedObjectContext) {
        let achievementObj: AchievementDataObj?
        if let rideCard = Rides.fetchRideWithID(rideID: rideID, context: context) {
            if let achievement = rideCard.achievement {
                achievementObj = achievement
            } else {
                let achievement = AchievementDataObj(context: context)
                achievementObj = achievement
                achievement.rideID = rideID
            }
            
            achievementObj?.distance_travelled = distanceTravelled
            achievementObj?.time_duration = Double(travelledTime)
            achievementObj?.top_speed = topSpeed 
            achievementObj?.average_speed = averageSpeed
            
            let archivedLocations = NSKeyedArchiver.archivedData(withRootObject: rideLocations)
            achievementObj?.rideLocations = archivedLocations as NSObject
            
            let archivedLastLocation = NSKeyedArchiver.archivedData(withRootObject: lastGoodLocation)
            achievementObj?.lastGoodLocation = archivedLastLocation as NSObject
            rideCard.achievement = achievementObj
            
            do {
                try context.save()
            } catch {
                
            }
        }else {
            print("updateStatsObj rideId not present")
        }
        
        
    }
    
    class func updateLastPacketTime(time: NSDate, context: NSManagedObjectContext, rideID: String) {
        let statsFetch = NSFetchRequest<AchievementDataObj>(entityName: "AchievementDataObj")
        statsFetch.predicate = NSPredicate(format: "rideID == %@", rideID)
        
        do {
            let achievement = try context.fetch(statsFetch)
            if achievement.count > 0{
                achievement.first?.last_processed_time = time
            } else {
                print("achievement count is 0")
            }
            do {
                print("Updated last packet time:\(rideID)")
                try context.save()
            } catch {
                
            }
        } catch {
            
        }
    }
    
    class func updateRideLocation(rideID: String, rideLocation: [CLLocation], context: NSManagedObjectContext) {
        if let rideCard = Rides.fetchRideWithID(rideID: rideID, context: context) {
            let archivedLocations = NSKeyedArchiver.archivedData(withRootObject: rideLocation)
            rideCard.achievement?.rideLocations = archivedLocations as NSObject
            do {
                try context.save()
            } catch {
                
            }
        }
    }
}
