//
//  VCMPacket+CoreDataProperties.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 05/10/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//
//

import Foundation
import CoreData


extension VCMPacket {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<VCMPacket> {
        return NSFetchRequest<VCMPacket>(entityName: "VCMPacket")
    }

    @NSManaged public var packetData: NSObject?
    @NSManaged public var packetId: NSDecimalNumber?
    @NSManaged public var ridePacket: RidePacket?

}
