//
//  VehicleDetails+CoreDataProperties.swift
//  Maximus
//
//  Created by Isha Ramdasi on 05/06/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//
//

import Foundation
import CoreData


extension VehicleDetails {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<VehicleDetails> {
        return NSFetchRequest<VehicleDetails>(entityName: "VehicleDetails")
    }

    @NSManaged public var id: String?
    @NSManaged public var company: String?
    @NSManaged public var make: String?
    @NSManaged public var model: String?
    @NSManaged public var nickname: String?
    @NSManaged public var odometer_reading: String
    @NSManaged public var date_added: String?
    @NSManaged public var servicing_details: String?
    @NSManaged public var fuel_details: String?
    @NSManaged public var device_id: String?
    @NSManaged public var biker_id: String?
    @NSManaged public var registrationMonth: String?
    @NSManaged public var registrationYear: String?
    @NSManaged public var lost: Bool
    @NSManaged public var lost_at: String?
    @NSManaged public var isVirtualCm: Bool
    @NSManaged public var display_module_id:String?
    @NSManaged public var display_module_sr_no:String?
    @NSManaged public var device_unit_id:String?
}
