//
//  LegInProgress.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 13/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit



class LegInProgress: LegState {
    
    let legDetail:NSLegProgress
    let sinkManager = SinkManager.sharedInstance
    
    init(legDetail:NSLegProgress) {
        self.legDetail = legDetail
        self.notifyLegInProgress()
    }
    
    private func notifyLegInProgress(){

        self.sinkManager.notifyObservers(notificationKey: sinkManager.SINK_NOTIFICATION_LEG_IN_PROGRESS, sender: self ,data: ["data":legDetail])
        
    }
    
    func isLegStarted(context: LegContext) -> Bool{
        return false
    }
    
    func isLegInProgress(context: LegContext) -> Bool{
        return true
    }
    
    func isLegCompleted(context: LegContext) -> Bool{
        return false
    }
    
    func isLegSkipped(context: LegContext) -> Bool {
        return false
    }
}

