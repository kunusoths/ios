//
//  LegStarted.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 13/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class LegStarted: LegState {
    
    let legDetail:NSLegProgress
    let sinkManager = SinkManager.sharedInstance
    
    init(legDetail:NSLegProgress) {
        self.legDetail = legDetail
        self.notifyLegStarted()
        self.sendDataToDM()
    }
    
    
    private func notifyLegStarted(){
        self.sinkManager.notifyObservers(notificationKey: sinkManager.SINK_NOTIFICATION_LEG_STARTED, sender: self, data: ["data": legDetail])
    }
    
    private func sendDataToDM(){
        
        //MaximusDM.sharedInstance.tbtStartStop(startStop: Int(JOURNEY_START.rawValue))
        
        let distance = legDetail.getLeg()?.getDistance()?.getMeters()
        let duration = legDetail.getLeg()?.getDuration()?.getSeconds()
        let (HRS ,min,timeAMPM) = self.getRemainingHRSAndMinutes(duration:duration!,format:0)
        
        Log.i(tag: MaximusClass.MAXIMUS_DM, message: " Distance Send")
        MaximusDM.sharedInstance.updateDailyJourneyUnit(distance: distance!)
        MaximusDM.sharedInstance.updateDailyJourneyEta(hour: HRS, minutes: min, ampm:Int(timeAMPM))
    
    }
    
    
    /*****************************************************************************
     * Function :   getDistanceAndUnit
     * Input    :   Distance
     * Output   :   converted Distance along with Unit
     * Comment  :
     *
     ****************************************************************************/
    
    func getDistanceAndUnit(distance:Int64)->(Int64,Int32){
        if distance > 1000{
            let dist:Double = (Double(distance)/1000.0)
            let final_dist = Int64(dist * 10.0)
            return (final_dist,DISTANCE_UNIT.KILOMETER)
        }else{
            return (distance,DISTANCE_UNIT.METER)
        }
    }
    
    /*****************************************************************************
     * Function :   getRemainingHRSAndMinutes
     * Input    :   Duration & Time format AM or PM
     * Output   :   Hours, minutes & TimeFormat
     * Comment  :   Send Duration to method which will add it to current time and return hours and minuites
     *
     ****************************************************************************/
    func getRemainingHRSAndMinutes(duration:Int64,format:Int32)->(Int,Int,Int32){
        let minuites = duration/60
        let date = Date().add(minutes: Int(minuites))
        let (hours,min,_) = date.getHourMinutesSecond()
        if format == 0 {
            //12 HRS FORMAT
            let (HRS,TIME_FORMAT) = self.TwelveHRSFormat(hours: hours)
            return (HRS,min,TIME_FORMAT)
        }else{
            //24 HRS FORMAT
            return (hours,min,1)
        }
    }
    
    /*****************************************************************************
     * Function :   TwelveHRSFormat
     * Input    :   Hours
     * Output   :   Time format & Hours
     * Comment  :   Time format like is AM or PM with Hours according to Format
     *
     ****************************************************************************/
    func TwelveHRSFormat(hours:Int)->(Int,Int32){
        if hours > 12 {
            return (hours - 12,TIME_AM_PM.ONE)
        }else{
            return (hours,TIME_AM_PM.ZERO)
        }
    }
    
    func isLegStarted(context: LegContext) -> Bool{
        return true
    }
    
    func isLegInProgress(context: LegContext) -> Bool{
        return false
    }
    
    func isLegCompleted(context: LegContext) -> Bool{
        return false
    }
    
    func isLegSkipped(context: LegContext) -> Bool {
        return false
    }
}
