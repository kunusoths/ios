//
//  LegNotStarted.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 13/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class LegNotStarted: LegState {
    
    
    func isLegStarted(context: LegContext) -> Bool{
        return false
    }
    
    func isLegInProgress(context: LegContext) -> Bool{
        return false
        
    }
    
    func isLegCompleted(context: LegContext) -> Bool{
        return false
    }
    
    func isNextLegPresent(context: LegContext) -> Bool{
        return false
        
    }
    
    func isLegSkipped(context: LegContext) -> Bool {
        return false
    }
    
}
