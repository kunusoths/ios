//
//  LegContext.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 13/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation

final class LegContext {
    
    private var state: LegState = LegNotStarted()
    
    func changeStateToLegStarted(legDetail:NSLegProgress){
        state = LegStarted(legDetail: legDetail)
    }
    
    func changeStateToLegCompleted(legDetail:NSLegProgress){
        state = LegCompleted(legDetail: legDetail)
    }
    
    func changeStateToLegInProgress(legDetail:NSLegProgress){
        state = LegInProgress(legDetail: legDetail)
    }
    
    func changeStateToLegSkipped(legDetail:NSLegProgress){
        state = LegSkipped(legDetail: legDetail)
    }
}


protocol LegState {
    
    func isLegStarted(context: LegContext) -> Bool
    func isLegInProgress(context: LegContext) -> Bool
    func isLegCompleted(context: LegContext) -> Bool
    func isLegSkipped(context: LegContext) -> Bool
    
}
