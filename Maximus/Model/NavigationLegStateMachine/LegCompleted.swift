//
//  LegCompleted.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 13/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import ObjectMapper

class LegCompleted: LegState {
    
    let legDetail:NSLegProgress
    let sinkManager = SinkManager.sharedInstance
    let rideStateHandler = RideStateHandler.sharedInstance
    let maximusDm = MaximusDM.sharedInstance
    
    init(legDetail:NSLegProgress) {
        self.legDetail = legDetail
        self.sendDataToDm()
        self.currentLegComplete()
        
        if(self.isNextLegAbsent()){
            self.rideRouteFinished()
        }
    }
    
    private func isNextLegAbsent() -> Bool{
        if self.legDetail.getNextLeg() == nil{
            return true
        }
        return false
    }
    
    private func currentLegComplete(){
        
        //rideStateHandler.setCurrentRideLegCompleted()
        
//        DispatchQueue.main.async{
//            UIApplication.topViewController()?.view.makeToast("📢📢 You have arrived 🏁🏁")
//        }
        
        //rideStateHandler.processPOI()
        self.sinkManager.notifyObservers(notificationKey: sinkManager.SINK_NOTIFICATION_LEG_PROGRESS_COMPLETED, sender: self)
        print("@ :------------ Leg Completed && Moving towords next Leg-------------")
    }
    
    private func rideRouteFinished(){
//        DispatchQueue.main.async{
//            UIApplication.topViewController()?.view.makeToast("📢📢 Destination Reached 🏁🏁")
//        }
        
        ///Notify
        self.sinkManager.notifyObservers(notificationKey: sinkManager.SINK_NOTIFICATION_RIDE_ROUTE_FINISHED, sender: self)
        print("@LegCompletedStateMAchine:------------ Leg Completed && Destination Reach-------------")
        
    }
    
    private func sendDataToDm(){
        Log.i(tag: "Leg Completed", message: "Sending ride time ")

        if self.isNextLegAbsent(){
            //Ride Route Reached
            MyDefaults.setBoolData(value: true, key: DefaultKeys.IS_RIDE_COMPLETED)
            maximusDm.setFullJourneyOnOff(fulljourneyState: JOURNEY_STOP)
        }else{
            //POI Reached
            maximusDm.syncJourneyDetails()
            let duration:Int = Int((legDetail.getLeg()?.getDuration()?.getSeconds())!)
            let (_, hours, minutes) =  self.getTimeFromSeconds(duration: duration)
            MaximusDM.sharedInstance.sendDailyCompletedRideTime(minutes: minutes, hours: hours)
            maximusDm.dailyJourneyStartStop(journeyStartStop: Int(STOP.rawValue))
            
            if let nextPOI = legDetail.getNextLeg()?.getEndAddress() {
                maximusDm.sendNextCityName(nextCity: nextPOI)
            }
            
            maximusDm.setJourney(journeyOnOff: Int(TBT_ON.rawValue))
        }
    }
    
    func getTimeFromSeconds(duration:Int) -> (Int, Int, Int){
        var hours = 0
        
        let days = 0
        
        if duration > 3600 {
            hours = duration/3600
        }else{
            hours = 0
        }
        
        var minutes = 0//(duration % 3600)/60
    
        if duration > 3600{
            minutes = (duration % 3600)/60
        }else{
            minutes = duration/60
        }
        
        return ( days, hours, minutes)
    }
    
    func isLegStarted(context: LegContext) -> Bool{
        return false
    }
    
    func isLegInProgress(context: LegContext) -> Bool{
        return false
        
    }
    
    func isLegCompleted(context: LegContext) -> Bool{
        return true
    }
    
    func isLegSkipped(context: LegContext) -> Bool {
        return false
    }
        
}

