//
//  LegSkipped.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 07/09/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import Foundation


class LegSkipped: LegState {
    
    let legDetail:NSLegProgress
    let sinkManager = SinkManager.sharedInstance
    
    init(legDetail:NSLegProgress) {
        self.legDetail = legDetail
        self.notifyLegSkipped()
    }
    
    private func notifyLegSkipped(){
        
        self.sinkManager.notifyObservers(notificationKey: sinkManager.SINK_NOTIFICATION_SKIPPED, sender: self ,data: ["data":legDetail])
        
    }
    
    func isLegStarted(context: LegContext) -> Bool{
        return false
    }
    
    func isLegInProgress(context: LegContext) -> Bool{
        return false
    }
    
    func isLegCompleted(context: LegContext) -> Bool{
        return false
    }
    
    func isLegSkipped(context: LegContext) -> Bool {
        return true
    }
}
