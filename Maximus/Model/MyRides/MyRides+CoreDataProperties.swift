//
//  MyRides+CoreDataProperties.swift
//  Maximus
//
//  Created by Isha Ramdasi on 27/02/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//
//

import Foundation
import CoreData


extension MyRides {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MyRides> {
        return NSFetchRequest<MyRides>(entityName: "MyRides")
    }

    @NSManaged public var admin_biker_id: Int16
    @NSManaged public var admin_profile_image: String?
    @NSManaged public var background_image: String?
    @NSManaged public var biker_ID: Int16
    @NSManaged public var biker_status: Int16
    @NSManaged public var endLocation_lat: Double
    @NSManaged public var endLocation_long: Double
    @NSManaged public var endLocation_name: String?
    @NSManaged public var finish_date: String?
    @NSManaged public var joined_users: Int16
    @NSManaged public var planned: Bool
    @NSManaged public var public_ride: Bool
    @NSManaged public var ride_title: String?
    @NSManaged public var rideID: Int16
    @NSManaged public var round_trip: String?
    @NSManaged public var start_date: String?
    @NSManaged public var startLocation_lat: Double
    @NSManaged public var startLocation_long: Double
    @NSManaged public var startLocation_name: String?
    @NSManaged public var status: Int16
    @NSManaged public var type: Int16

}
