//
//  RideDetailsUpdateHandler.swift
//  Maximus
//
//  Created by Isha Ramdasi on 19/06/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import Foundation

class RideDetailsUpdateHandler: NSObject {
    static let rideObj = RideManager()

    class func updateRides(ridesToUpdate: [MyRides]) {
        for i in 0..<ridesToUpdate.count  {
             DispatchQueue.global(qos: .background).async {
                self.rideObj.getRideRouteById(rideID: Int(ridesToUpdate[i].rideID ?? "")!, bikerId:Int(ridesToUpdate[i].biker_ID ?? "")!)
                self.rideObj.getPlannedWavepoints(rideID: Int(ridesToUpdate[i].rideID ?? "")!)
            }
        }
    }
    
    func postPlannedWaypointsListSuccess(data: [String : Any]) {
        if let obj:PlannedWavePointsModel = data["data"] as? PlannedWavePointsModel {
            if let wavePointList = obj.WavePointsList{
              //  MyRides.addWaypointsInRide(rideID: String(rideDetails?.id ?? 0), context: context, obj: wavePointList)
            }
        }
    }
    
    func rideRouteSuccess(data: [String : Any]) {
        
    }
}
