//
//  RouteListObj+CoreDataProperties.swift
//  Maximus
//
//  Created by Isha Ramdasi on 19/06/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//
//

import Foundation
import CoreData


extension RouteListObj {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RouteListObj> {
        return NSFetchRequest<RouteListObj>(entityName: "RouteListObj")
    }

    @NSManaged public var lat: Double
    @NSManaged public var lng: Double

}
