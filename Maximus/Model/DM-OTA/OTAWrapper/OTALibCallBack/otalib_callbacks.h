//
//  otalib_callbacks.h
//  Maximus
//
//  Created by Abhilash Ghogale on 17/10/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

#ifndef otalib_callbacks_h
#define otalib_callbacks_h

#include <stdio.h>
#include "otafwu_if.h"

/* Callback function prototypes */

OTA_ReturnType ota_get_current_image_info(char* value);

OTA_ReturnType ota_update_status(OTA_Status status, float percentage);

OTA_ReturnType ota_init_seq_number();

OTA_ReturnType ota_write_new_content(char* value);

OTA_ReturnType ota_write_new_image(char* value);

OTA_ReturnType ota_request_status(char* value);
/* Callback function prototypes */
void updateOTAStatus(OTA_Status status);

#endif /* otalib_callbacks_h */
