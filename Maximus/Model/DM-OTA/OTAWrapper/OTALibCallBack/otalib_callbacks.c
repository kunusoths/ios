//
//  otalib_callbacks.c
//  Maximus
//
//  Created by Abhilash Ghogale on 17/10/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

#include "otalib_callbacks.h"

/***** Externs ******/
extern OTA_ReturnType handleOTAStatus(OTA_Status status);
extern OTA_ReturnType otaWriteNewImage(char * value);
extern OTA_ReturnType otaInitSeqNumber();
extern OTA_ReturnType otaWriteNewContent(char * value);
extern OTA_ReturnType getCurrentImageInfo(char * value);
extern OTA_ReturnType otaRequestStatus(char *value);

extern void updateFileComplete(float percentage);



/***** OTA Callbacks ******/

OTA_ReturnType ota_request_status(char* value)
{
    return otaRequestStatus(value);
}

OTA_ReturnType ota_update_status(OTA_Status status, float percentage)
{
    fprintf(stdout, "Update in callback \n");
    
    if (status == OTA_CHUNK_SUCCESS) {
        updateFileComplete(percentage);
    }
    return handleOTAStatus(status);
}

OTA_ReturnType ota_write_new_image(char* value)
{
    fprintf(stdout, "OTA new image %s \n", value);    
    return otaWriteNewImage(value);
}

OTA_ReturnType ota_get_current_image_info(char* value)
{
    fprintf(stdout, "Current Image Info \n %s", value);
    return getCurrentImageInfo(value);
}

OTA_ReturnType ota_init_seq_number()
{
    fprintf(stdout, "OTA Init Seq \n");
    return otaInitSeqNumber();
}

OTA_ReturnType ota_write_new_content(char* value)
{
    return otaWriteNewContent(value);
}

void updateOTAStatus(OTA_Status status)
{
    fprintf(stdout,"\n OTA Status update : %d", status);
}
