//
//  OTAWrapper.h
//  Maximus
//
//  Created by Abhilash Ghogale on 14/10/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "otafwu_if.h"
#import "otalib_callbacks.h"

@protocol OTAWrapperDelegate

@optional

-(OTA_ReturnType) ota_statue_ack:(OTA_Status) status;
-(OTA_ReturnType) ota_current_image_info:(char *) value;
-(OTA_ReturnType) ota_write_new_image_Data:(NSData *) infoData;
-(OTA_ReturnType) enableOTADescriptor;
-(OTA_ReturnType) write_new_content:(NSData *) newData;
-(OTA_ReturnType) ota_request_new_status:(NSData *) reqData;

-(void) ota_complete_status:(float) per;
@end


@interface OTAWrapper : NSObject

+(OTAWrapper *) sharedInstace;

@property (weak, nonatomic) id<OTAWrapperDelegate> delegate;

@property NSArray* infoArray;

-(OTA_Status) dmOTAValidateSpaceOnDM:(NSString*)filePath  andCrc:(uint32_t) crc;
-(void) saveCurrentImgInfo:(NSArray*) info;
-(void) otaWriteResponseCb:(NSData*) data;
-(void) otaGetModeStatusCB:(NSData*) data;
-(void) startUpgratde;
-(void) endOTASession;
-(void) createOTASession;
//OTA_GetModeStatusCB

@end

