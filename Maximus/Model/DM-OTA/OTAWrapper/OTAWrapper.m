//
//  OTAWrapper.m
//  Maximus
//
//  Created by Abhilash Ghogale on 14/10/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

#import "OTAWrapper.h"

//call back pointer functions
struct OTA_APP_API_CB  ota_callbacks = {
    .otaGetCurrentImageInfoAPI = ota_get_current_image_info,
    .otaWriteNewImageAPI = ota_write_new_image,
    .otaInitiateSeqNumberAPI = ota_init_seq_number,
    .otaWriteNewContentAPI = ota_write_new_content,
    .otaUpdateStatusCB = ota_update_status,
    .otaUpdateRequest = ota_request_status,
};


@interface OTAWrapper()

@end

@implementation OTAWrapper

@synthesize infoArray;

//Singleton class object
static OTAWrapper * _sharedInstance = nil;

//Class initializer
+(OTAWrapper *) sharedInstace{
    
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    //creating OTA Seassion
    return _sharedInstance;
}

#pragma mark Class Methods

//Start OTA

-(void) startUpgratde
{
    OTA_StartUpgrade();
}

//Create a New OTA Session
-(void) createOTASession{
    OTA_NewSession(&ota_callbacks);
}

-(void) endOTASession{
    OTA_AbortUpgrade();
}

//Save Current DM img info
-(void) saveCurrentImgInfo:(NSArray*) info {
    self.infoArray = info;
}

//valifate space DM
-(OTA_Status) dmOTAValidateSpaceOnDM:(NSString*)filePath  andCrc:(uint32_t) crc
{
    unsigned char *myPath = (unsigned char *)[filePath UTF8String];
    return OTA_ValidateImageSizeAndAvailableSpace(myPath, crc);
}

//OTA_WriteResponseCB
-(void) otaWriteResponseCb:(NSData*) data{
    unsigned char * myData= (unsigned char *)[data bytes];
    OTA_WriteResponseCB(myData);
}

//Get DM mode OTA/Fw
-(void) otaGetModeStatusCB:(NSData*) data
{
    unsigned char * myData= (unsigned char *)[data bytes];
    OTA_GetModeStatusCB(myData);
}

#pragma mark OTA lib call back listeners

-(OTA_ReturnType) updateOTAStatus:(OTA_Status) status
{
    NSLog(@"Update status to UI");
    return  [_delegate ota_statue_ack:status];
}

-(OTA_ReturnType)  updateCurrentImgageInfo:(char *) value
{
    NSLog(@"value %s", value);
    unsigned char cArray[16];
    int arrayCount = (int)[self.infoArray count] ;
    for (int i = 0; i<arrayCount; ++i) {
        cArray[i] = (uint8_t) [[self.infoArray objectAtIndex:(i)] integerValue] ;
    }
    
    memcpy(value, cArray, sizeof(cArray));
    NSLog(@"value %s", value);
    return OTA_RETURN_SUCCESS;
}

-(OTA_ReturnType) requestForStatus : (NSData *) data
{
    return [_delegate ota_request_new_status:data];
}

-(OTA_ReturnType) sendNewImageInfo :(NSData *)data
{
    return  [_delegate ota_write_new_image_Data:data];
}

-(OTA_ReturnType) enableOTADescriptor
{
    //enable descriptor
    [_delegate enableOTADescriptor];
    return OTA_RETURN_SUCCESS;
}

-(OTA_ReturnType) writeNewContent:(NSData *) newData
{
    [_delegate write_new_content:newData];
    return OTA_RETURN_SUCCESS;
}

-(void) updateFileCompleteStatus:(float) per
{
    [_delegate ota_complete_status:per];
}
#pragma mark CALL BACKS

void updateFileComplete(float per)
{
    
    NSLog(@" OTAWrapper : OTA Completed %f", per);
    [_sharedInstance updateFileCompleteStatus:per];
}

OTA_ReturnType handleOTAStatus(OTA_Status status)
{
    NSLog(@" OTAWrapper : OTA Status %u", status);
    return [_sharedInstance updateOTAStatus:status];
}

OTA_ReturnType otaWriteNewImage(char * value)
{
    NSLog(@"New Image %s", value);
    
    NSData *newImageData = [NSData dataWithBytes:value length:9];
    return [_sharedInstance sendNewImageInfo:newImageData];
    
}

OTA_ReturnType otaInitSeqNumber()
{
    NSLog(@"Init Seq Number");
    return [_sharedInstance enableOTADescriptor];
}

OTA_ReturnType otaWriteNewContent(char * value)
{
   // NSLog(@"Write New Content %c", *value);
    
    NSData *newConent = [NSData dataWithBytes:value length:20];
    return [_sharedInstance writeNewContent:newConent];
}

OTA_ReturnType getCurrentImageInfo(char* value)
{
    
    return [_sharedInstance updateCurrentImgageInfo:value];
}

OTA_ReturnType otaRequestStatus(char * value)
{
    NSData *requestData = [NSData dataWithBytes:value length:1];
    return [_sharedInstance requestForStatus:requestData];
}

@end

