//
//  OTAConstants.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 13/11/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//

import Foundation

struct OTAConstants {
    static let DM_BATTERY_THRESHOLD = 50
    static let PHONE_BATTERY_THRESHOLD = 50
    
}
