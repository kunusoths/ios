//
//  DMOTAManager.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 10/10/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

@objc protocol OTAManagerDelegate {
    
    @objc func updateOTACompleteStatus(per : Float)
    @objc func handleOtaError(err: [String:Any])
    @objc func preConditionSucess()
    @objc func otaSuccess()
    @objc func otaHasBeenStarted()
    
    @objc func phoneBatterySufficent()
    @objc func dmBatterySufficeint()
    @objc func bleConnected()
    @objc func noRideIsInProgress()
}

class DMOTAManager: NSObject {
    
    weak var otaDelegate:OTAManagerDelegate?
    
    let otaValidation = OTAValidatePrecondition()
    let otaProgress = OTAProgressState()
    
    let otaWrapper = OTAWrapper.sharedInstace()
    //
    
    override init() {
        super.init()
        otaValidation.otaValidationDelegate = self

    }
    
    func eventBLEStatusUpdate(notification:Notification){
        let status:BluetoothState = notification.userInfo!["data"] as! BluetoothState
        
        if status == .CONNECTED_P {
            
        }else{
            //BLE_DISCONNECTED
            print(" Ble Disconnected : Aborting Ota")
            OTAWrapper.sharedInstace().endOTASession()
            otaDelegate?.handleOtaError(err: ["error" : OTAErrorCases.BLE_DISCONNECTED])
        }
    }
    
    func startOTA()  {
        otaProgress.otaPeogressDelegate = self
        self.otaProgress.otaProcessStarted()
    }
    
    func validatePreConditions() {
        otaValidation.checkOTAPrecondition()
    }
    
}

extension DMOTAManager:OTAValidationDelegate{
    func connectedToDisplay() {
        
        otaDelegate?.bleConnected()
    }
    
    func phoneBatteryIsSufficent() {
        otaDelegate?.phoneBatterySufficent()
        
    }
    
    func displaBatteryIsSufficient() {
        otaDelegate?.dmBatterySufficeint()
    }
    
    func noRideIsInProgress() {
        //
        otaDelegate?.noRideIsInProgress()
    }
    
    
    func onPreconditionSucess (){
        //
        print("OTA Precondition Succeded")
        otaDelegate?.preConditionSucess()
        
    }
    
    func onValidationFailed(errCase: OTAErrorCases) {
        print("OTA Failed err : \(errCase)")
        otaDelegate?.handleOtaError(err: ["error":errCase])
    }
    
}

extension DMOTAManager:OTAProgressDelegate
{
    func otaStarted() {
        BluetoothServices.sharedInstance().invalidateHeartBeat()
        otaDelegate?.otaHasBeenStarted()
    }
    
    func otaCompletedSuccess() {
        otaDelegate?.otaSuccess()
    }
    
    func otaError(err: OTAErrorCases) {
        //update ui
        BluetoothServices.sharedInstance().startHeatbeatTimer()
        print(" OTA Error Occured \(err)")
        otaDelegate?.handleOtaError(err: ["error":err])
    }
    
    func otaCompleteStatus(value: Float) {
        //update to ui
        print("Ota Completed : \(value) %")
        otaDelegate?.updateOTACompleteStatus(per: value)
    }
    

    
    
}

