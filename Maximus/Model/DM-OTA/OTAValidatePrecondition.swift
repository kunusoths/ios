//
//  OTAValidatePrecondition.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 10/10/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import ObjectMapper

protocol OTAValidationDelegate:class {
    func onValidationFailed(errCase: OTAErrorCases)
    func connectedToDisplay()
    func phoneBatteryIsSufficent()
    func displaBatteryIsSufficient()
    func noRideIsInProgress()
    func onPreconditionSucess()
}

class OTAValidatePrecondition : DMBatteryStateChangeProtocol{

    let maximusDm = MaximusDM.sharedInstance
    weak var otaValidationDelegate: OTAValidationDelegate?
    let sinkManager = SinkManager.sharedInstance
    var phoneBatteryState: UIDeviceBatteryState  = UIDevice.current.batteryState
    
    init() {
        print("In OTAValidatePrecondition init ")
        BluetoothServices.sharedInstance().readDMBarttery()
        sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_BLUETOOTH_STATE_UPDATE, selectorName: self.eventBLEStatusUpdate)
        NotificationCenter.default.addObserver(self, selector: #selector(batteryStateDidChange), name: .UIDeviceBatteryStateDidChange, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(batteryLevelDidChange), name: .UIDeviceBatteryLevelDidChange, object: nil)
        maximusDm.dmChargingDelegate = self
    }
    
    deinit {
        print("In OTAValidatePrecondition deinit ")
        NotificationCenter.default.removeObserver(self)
    }

    //MARK : Notification on precondition changed
    
    
    func onDMChargingStateChange(state: CP_DmChargingTypeDef) {
        //self.checkOTAPrecondition()
    }
    
    func eventBLEStatusUpdate(notification:Notification){
       // self.checkOTAPrecondition()
    }
    
    @objc func batteryStateDidChange(notification: NSNotification){
        // The stage did change: plugged, unplugged, full charge...
        //self.checkOTAPrecondition()
    }
    
    @objc func batteryLevelDidChange(notification: NSNotification){
        // The battery's level did change (98%, 99%, ...)
        
    }
    
    
    func checkOTAPrecondition()  {
        
        let isBtConnected = isBleConnected()
        let isPhoneBatterySufficient = validatePhoneBattery()
        let isDMBatterySufficient = validateDisplayBattery()
        let isRideStopped = validatedIsRideInProgress()
        let dmSize = validateSpaceOnDm()
        
        if (isBtConnected && isPhoneBatterySufficient && isDMBatterySufficient &&  isRideStopped && dmSize){
            
           otaValidationDelegate?.onPreconditionSucess()
            
        }else{
            
           // otaValidationDelegate?onP
        }
    }
    
    func getCrc() -> UInt32 {
        
        var fileCrc:UInt32?
        do {
            if let file = Bundle.main.url(forResource: DefaultKeys.KEY_OTA_MANIFEST, withExtension: DefaultKeys.KEY_JSON) {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                
                let user = Mapper<OTAManifest>().map(JSON:json as! [String : Any])!
                let crc:String = user.otaObj?.displayModule?.firmwareData?.checksum ?? "00"
                fileCrc = UInt32(crc, radix: 16)
            }
        }catch {
            print("error in readind manifest file")
        }
        
        return fileCrc!
    }
    
    
    fileprivate func validateSpaceOnDm() -> Bool{
        
        let path = Bundle.main.path(forResource: "DisplayModule", ofType: "bin")
        let fileCrc = self.getCrc()
        print("File crc: \(fileCrc)")
        
        let otaStatus:OTA_Status  = OTAWrapper.sharedInstace().dmOTAValidateSpace(onDM: path, andCrc: fileCrc)
        
        var returnStatus:Bool  = false
        
        switch otaStatus {
        case OTA_PROCEED_SUFFICIENT_STORAGE:
            returnStatus = true
            break
            
        case OTA_CANCELLED_INSUFFICIENT_STORAGE:
            if self.isBleConnected(){
                otaValidationDelegate?.onValidationFailed(errCase: .INSUFFICIENT_SPACE_ON_DM)
            }
            break
            
        default:
            break
        }
        
        return returnStatus
    }
    
    fileprivate func isBleConnected() -> Bool{
        if BluetoothManager.sharedInstance().getBLEState() == .CONNECTED_P {
            otaValidationDelegate?.connectedToDisplay()
            return true
        }else{
            otaValidationDelegate?.onValidationFailed(errCase: .BLE_NOT_CONNECTED)
            return false
        }
        
    }
    
    fileprivate func validatePhoneBattery() -> Bool {
        
        let baterryLevel = UIDevice.current.batteryLevel * 100

        
        if baterryLevel >= 30 || phoneBatteryState == .charging {
            otaValidationDelegate?.phoneBatteryIsSufficent()
            return true
        }else{
            otaValidationDelegate?.onValidationFailed(errCase: .PHONE_BATTERY_LOW)
            return false
        }
    }
    
    fileprivate func validateDisplayBattery() -> Bool {
        let displayBatteryLevel = BluetoothManager.sharedInstance().getDMBatteryLevel()
        let isDmCharging = MaximusDM.sharedInstance.getDMBatteryChargingStatus()
        if self.isBleConnected() {
            if displayBatteryLevel >= 30 || isDmCharging {
                otaValidationDelegate?.displaBatteryIsSufficient()
                return true
            }else{
                otaValidationDelegate?.onValidationFailed(errCase: .DM_BATTERY_LOW)
                return false
            }
        }else{
            otaValidationDelegate?.onValidationFailed(errCase: .DM_BATTERY_LOW)
            return false
        }
    }
    
    fileprivate func validatedIsRideInProgress() -> Bool {
        let flagRideInProgress = JourneyManager.sharedInstance.isAnyRideInProgress()
        if flagRideInProgress{
            otaValidationDelegate?.onValidationFailed(errCase: .RIDE_IN_PROGRESS)
            return false
        }else{
            otaValidationDelegate?.noRideIsInProgress()
            return !flagRideInProgress
        }
    }

}

