//
//  OTAProgressState.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 10/10/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import ObjectMapper

protocol OTAProgressDelegate: class {
    
    func otaCompletedSuccess()
    func otaCompleteStatus(value: Float)
    func otaStarted()
    func otaError(err: OTAErrorCases)

}

class OTAProgressState {
    
    let otaWrapper = OTAWrapper.sharedInstace()
    weak var otaPeogressDelegate:OTAProgressDelegate?
    
    init() {
        print("In OTAProgressState init ")

    }
    
    deinit {
        print("In OTAProgressState deinit ")
    }
    
    func eventBLEStatusUpdate(notification:Notification){
        let status:BluetoothState = notification.userInfo!["data"] as! BluetoothState
        
        if status != .CONNECTED_P {
            
        }
        else{
            otaWrapper?.endOTASession()
            otaPeogressDelegate?.otaError(err: .BLE_DISCONNECTED)
        }
    }
    
    func otaProcessStarted(){
        print("Starting OTA")
        otaWrapper?.delegate = self
        
    }
    
    func getCRC() -> Data {
        
        var crcData = Data()
        
        do {
            if let file = Bundle.main.url(forResource: DefaultKeys.KEY_OTA_MANIFEST, withExtension: DefaultKeys.KEY_JSON) {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                
                let user = Mapper<OTAManifest>().map(JSON:json as! [String : Any])!
                let crc:String = user.otaObj?.displayModule?.firmwareData?.checksum ?? "00"
                let crcUint32 = UInt32(crc, radix: 16)
                crcData = self.uint32ToData(val: crcUint32!)
                return crcData
            }
        }catch {
            
        }
        
        return crcData
    }
    
    /*
     *function to convert Hex Uint32 value to Data
     */
    func uint32ToData(val: UInt32) -> Data{
        
        var array = [UInt8]()
        
        let cmId_1 = UInt8((val & 0x000000ff))
        array.append(cmId_1)
        let cmId_2 = UInt8(((val & 0x0000ff00) >> 8))
        array.append(cmId_2)
        let cmId_3 = UInt8(((val & 0x00ff0000) >> 16))
        array.append(cmId_3)
        let cmId_4 = UInt8(((val & 0xff000000)>>24))
        array.append(cmId_4)
        
        let data = Data(bytes: array, count: 4)
        return data
    }
    
}

extension OTAProgressState:OTAWrapperDelegate{
    
    func ota_statue_ack(_ status: OTA_Status) -> OTA_ReturnType{
        print(" OTA Status \(status)")
        switch status {
        
        case OTA_DM_FWU_MODE_SET:
            let crcData = self.getCRC()
            let _ = BluetoothServices.sharedInstance().writeToBleChar(infoData: crcData as Data, typeOfChar: .otaCRCChar)
            otaPeogressDelegate?.otaStarted()
            otaWrapper?.startUpgratde()
            break
            
        case OTA_CANCELLED_INSUFFICIENT_STORAGE:
            print("OTAProgress : Insufficient space on dm")
            otaPeogressDelegate?.otaError(err: .INSUFFICIENT_SPACE_ON_DM)
            break
            
        case OTA_COMPLETED:
            print("OTAProgress: File written Succesfully !!") 
            otaPeogressDelegate?.otaCompletedSuccess()
            break
            
        case OTA_CHUNK_SUCCESS:
            
            break
        
        case OTA_SUCCESS:
            break
            
        case OTA_ABORT:
            otaPeogressDelegate?.otaError(err: .OTA_ABORTED)
            
        case OTA_TIMEOUT_ERROR_ABORT:
            otaPeogressDelegate?.otaError(err: .OTA_TIME_OUT_ERROR)
            break
            
        case OTA_EXCEEDED_REPEATED_ATTEMPT_COUNT:
            otaPeogressDelegate?.otaError(err: .OTA_EXCEEDED_REPEATED_ATTEMPT_COUNT)
            break
            
        case OTA_DM_FLASH_ERROR:
            otaPeogressDelegate?.otaError(err: .OTA_DM_FLASH_ERROR)
            break
            
        case OTA_UNEXPECTED_END_OF_FILE_OCCURED:
            otaPeogressDelegate?.otaError(err: .OTA_UNEXPECTED_END_OF_FILE_OCCURED)
            break
            
        case OTA_ERROR_IN_READING_FILE:
            otaPeogressDelegate?.otaError(err: .OTA_ERROR_IN_READING_FILE)
            break
            
        case OTA_ERROR_IN_READING_IMAGE_PACKET:
            otaPeogressDelegate?.otaError(err: .OTA_ERROR_IN_READING_IMAGE_PACKET)
            break
        
        case OTA_DM_FULL_IMAGE_CHECKSUM_ERROR:
            otaPeogressDelegate?.otaError(err: .OTA_CHECKSUM_ERROR)
            
        default:
            break
        }
        
        return OTA_RETURN_SUCCESS
    }
    
    
    func ota_current_image_info(_ value: UnsafeMutablePointer<Int8>!) -> OTA_ReturnType {
        
        return OTA_RETURN_SUCCESS
    }
    
    func ota_write_new_image_Data(_ infoData: Data!) -> OTA_ReturnType {
        let _ = BluetoothServices.sharedInstance().writeToBleChar(infoData: infoData, typeOfChar: BLEChar.otaNewImgChar)
       // BluetoothServices.sharedInstance().writeImgInfoDataToBLE(infoData: infoData)
        
        return OTA_RETURN_SUCCESS
    }
    
    func enableOTADescriptor() -> OTA_ReturnType {
        BluetoothServices.sharedInstance().notifyOtaExpectedImgTUSeqChar()
        return OTA_RETURN_SUCCESS
    }
    
    func ota_complete_status(_ per:Float) {
        otaPeogressDelegate?.otaCompleteStatus(value: per)
    }
    
    func write_new_content(_ newData: Data!) -> OTA_ReturnType {
        let _ = BluetoothServices.sharedInstance().writeToBleChar(infoData: newData, typeOfChar: BLEChar.otaNewImgTUContentChar)
        return   OTA_RETURN_SUCCESS
        
    }
    
    func ota_request_new_status(_ reqData: Data!) -> OTA_ReturnType {
       let result = BluetoothServices.sharedInstance().writeToBleChar(infoData: reqData, typeOfChar: BLEChar.otaModeChar)
        return  result.rawValue == 1 ? OTA_RETURN_SUCCESS : OTA_RETURN_FAILURE
    }
}

