//
//  VirtualCMConstants.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 10/01/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import Foundation


struct VirtualCMPacket {
    
    // -------- System Code ------------
    static let SYSTEM_CODE_BYTE_1 = "M"
    static let SYSTEM_CODE_BYTE_2 = "C"
    static let SYSTEM_CODE_BYTE_3 = "G"
    static let SYSTEM_CODE_BYTE_4 = "p"
    
    static let MESSAGE_TYPE:UInt8 = 0
    
    static let COMM_CONTROLDIELD_BYTE_1:UInt8 = 0
    static let COMM_CONTROLDIELD_BYTE_2:UInt8 = 0
    
    static let UNIT_HW_NUMBER:UInt8 = 0
    static let UNIT_SW_NUMBER:UInt8 = 0
    
    static let PROTOCOL_VERSION_IDENTIFIER:UInt8 = 0
    static let BYTE_16:UInt8 = 0               //Unit’s status + Current GSM Operator (1st nibble)
    static let BYTE_17:UInt8 = 0               //Current GSM Operator (2nd and 3rd nibble)
    static let TX_REASON_DATA:UInt8 = 0        //Transmission Reason Specific Data
    static let TX_REASON:UInt8 = 44            //Transmission Reason
    static let UNITS_MODE_IGNITION_ON:UInt8 = 0  //Unit’s mode of operation
    static let UNITS_MODE_IGNITION_OFF:UInt8 = 1  //Unit’s mode of operation
    
    static let UNITS_IO_STATUS_1:UInt8   = 32
    static let UNITS_IO_STATUS_2:UInt8   = 192
    static let UNITS_IO_STATUS_3:UInt8   = 0
    static let UNITS_IO_STATUS_4:UInt8   = 0
    static let CURRENT_GSM_OPERATOR:UInt8 = 0
    
    static let ANALOG_INPUT_1:UInt8 = 120
    static let ANALOG_INPUT_2:UInt8 = 232
    static let ANALOG_INPUT_3:UInt8 = 0
    static let ANALOG_INPUT_4:UInt8 = 0
    
    static let MILEAGE_COUNTER_BYTE_1:UInt8 = 0
    static let MILEAGE_COUNTER_BYTE_2:UInt8 = 0
    static let MILEAGE_COUNTER_BYTE_3:UInt8 = 0
    
    static let MULTI_PERPOSE_FIELD_BYTE_1:UInt8 = 0
    static let MULTI_PERPOSE_FIELD_BYTE_2:UInt8 = 0
    static let MULTI_PERPOSE_FIELD_BYTE_3:UInt8 = 0
    static let MULTI_PERPOSE_FIELD_BYTE_4:UInt8 = 0
    static let MULTI_PERPOSE_FIELD_BYTE_5:UInt8 = 0
    static let MULTI_PERPOSE_FIELD_BYTE_6:UInt8 = 0
    
    static let LAST_GPS_FIX_BYTE_1:UInt8 = 50
    static let LAST_GPS_FIX_BYTE_2:UInt8 = 92
    
    static let LOCATION_STATUS:UInt8 = 0
    static let MODE_1:UInt8 = 4
    static let MODE_2:UInt8 = 2
    static let MODE_LOW:UInt8 = 0
    
    static let NUMBER_OF_SATELLITES:UInt8 = 0
    
}












