//
//  VCMPacketManager.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 10/01/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import Alamofire
import CoreData

@objc protocol VCMPacketManagerDelegate {
    
    @objc func allPacketsSent()
    @objc func writingFailed()
}


class VCMPacketManager: NSObject, SocketManagerDelegate {
    
    func streamOpened() {
    
    }
    
    static let sharedInstance = VCMPacketManager()
    weak var vcmDelegate:VCMPacketManagerDelegate?
    
    var vcmTimer: Timer?
    var syncDataTimer: Timer?
    let vcmTimeInterval:TimeInterval = 12
    let synDataTimeInterval:TimeInterval = 10 * 60
    
    var retryCount: Int? = nil
    
    private static var vcmPacket:VCMPacket?
    
    override init() {
        super.init()
        SocketConnectionManager.sharedInstance.socketManagerDelegate = self
        MyDefaults.setInt(value: 0, key: "MessgaeNumerator")
        retryCount = 0
    }
    
    var vcmPackets:[VCMPacket]?
    var rides:[RidePacket]!
    var messageNumerator:UInt8 = 0
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    
    //This method creates a packet as CM and add packet in Core Data
    func createAndSaveVCMPacket()  {
        let deviceLocation = DeviceLocationServiece.sharedInstance().getUserCurrentLocation()
        self.createVCMPacket(deviceLocation: deviceLocation)
    }
    
    // MARK:- Core Data mehods
    
    func addPacket(array: [UInt8]) {
        
        do {
            let currentRideId = JourneyManager.sharedInstance.currentRideDetails().rideID?.toInt()
            let currentRidePacketReq = NSFetchRequest<NSFetchRequestResult>(entityName: "RidePacket")
            
            currentRidePacketReq.predicate = NSPredicate(format: "rideId == %d", currentRideId ?? 0)
            
            if let currentRide = try context.fetch(currentRidePacketReq).first as? RidePacket{
                
                let vcmPacket = VCMPacket(context: context)
                let msgNumerator = MyDefaults.getInt(key: DefaultKeys.MESSAGE_NUMERATOR)
                vcmPacket.packetData = Data(bytes: array, count: 70) as NSObject
                vcmPacket.packetId = NSDecimalNumber(value: msgNumerator)
                //self.saveContext()
                print("VCM: Added packet Array \(vcmPacket)")
                currentRide.addToPacketArray(vcmPacket)                
                self.saveContext()
            }
        }catch{
            print("packet failed")
        }
    }
    
    func fetchRides() -> [RidePacket] {
        
        do{
            rides = try context.fetch(RidePacket.fetchRequest())
            print("VCM: no of Rides counts : \( rides?.count ?? 0)")
            if let count = rides?.count, count > 0 {
                return rides!
            }
        }catch{
            print("Fetching failed")
        }
        return []
    }
    
    func fetchCurrentRides() -> [RidePacket] {
        
        do{
            let currentRideId = JourneyManager.sharedInstance.currentRideDetails().rideID?.toInt()
            print("VCM: Current Ride \(currentRideId)")
            let currentRidePacketReq = NSFetchRequest<NSFetchRequestResult>(entityName: "RidePacket")
            
            currentRidePacketReq.predicate = NSPredicate(format: "rideId == %d", currentRideId ?? 0)
            
            if let currentRide = try context.fetch(currentRidePacketReq).first as? RidePacket {
                return [currentRide]
            }

        }catch{
            print("Fetching failed")
        }
        return []
    }
    
    func fetchPackets() -> [VCMPacket] {
        do{
            vcmPackets = try context.fetch(VCMPacket.fetchRequest())
            print("VCM: Pckt counts : \( vcmPackets?.count ?? 0)")
            if let count = vcmPackets?.count, count > 0 {
                return vcmPackets!
            }
        }catch{
            print("Fetching failed")
        }
        return []
    }
    
    func deleteRidePacket(packet: RidePacket) {
        print("VCM: deleting packet with id \(String(describing: packet.rideId))")
        context.delete(packet)
        do {
            try context.save()
        } catch {
            
        }
    }
    
    func deleteVCMPacket(packet: VCMPacket) {
        print("VCM: deleting packet with id \(String(describing: packet.packetId))")
        context.delete(packet)
        do {
            try context.save()
        } catch {
            
        }
    }

    
    func saveContext()  {
        do {
            try context.save()
        } catch {
            
        }
    }
    
    //Create Ride in CoreData
    func rideHasBeenStarted(rideId: Int) {
        //create RidePacket
        let ridePacket = RidePacket(context: context)
        ridePacket.rideId = Int32(rideId)
        self.saveContext()
        //
        self.startVCMTimer()
    }
    
    //This method starts timer
    func startVCMTimer() {
        let isCmVirtual = MyDefaults.getBoolData(key: DefaultKeys.IS_CM_VIRTUAL)
        if isCmVirtual {
            vcmTimer = Timer.scheduledTimer(timeInterval: vcmTimeInterval, target: self, selector: #selector(createAndSaveVCMPacket), userInfo: nil, repeats: true)
            syncDataTimer = Timer.scheduledTimer(timeInterval: synDataTimeInterval, target: self, selector: #selector(sendPacketToCloud), userInfo: nil, repeats: true)
        }
    }
    
    func invalidateVCMTimer() {
        self.vcmTimer?.invalidate()
        self.syncDataTimer?.invalidate()
    }
    
    // Create Packet of 70 bytes as per Communication Modulde
    func createVCMPacket(deviceLocation: CLLocation) {
        
        let element = "MCGP"
        let elementByte = element.utf8
        var checksum:UInt16 = 0
        var buffer = [UInt8](elementByte)
        var pktBytes: Array<UInt8> = Array()
        
        //bytes 1 to 4
        pktBytes.insert(buffer[0], at: 0)
        pktBytes.insert(buffer[1], at: 1)
        pktBytes.insert(buffer[2], at: 2)
        pktBytes.insert(buffer[3], at: 3)
        
        //byte 5
        pktBytes.insert(VirtualCMPacket.MESSAGE_TYPE, at: 4)
        
        
        let cmNumber:String = (BikerDetails.getBikerDetails(context: context)?.device_unit_id)!
        
        if let cmNumber32 = UInt32(cmNumber)
        {
            let cmId_1 = UInt8((cmNumber32 & 0x000000ff))
            let cmId_2 = UInt8(((cmNumber32 & 0x0000ff00) >> 8))
            let cmId_3 = UInt8(((cmNumber32 & 0x00ff0000) >> 16))
            let cmId_4 = UInt8(((cmNumber32 & 0xff000000)>>24))
            //bytes 6 to 9
            pktBytes.insert(cmId_1, at: 5)
            pktBytes.insert(cmId_2, at: 6)
            pktBytes.insert(cmId_3, at: 7)
            pktBytes.insert(cmId_4, at: 8)
        }
        
        
        // checksum = checksum + UInt16 (cmId_1 + cmId_2 + cmId_3 + cmId_4)
        //bytes 10 and 11
        pktBytes.insert(VirtualCMPacket.COMM_CONTROLDIELD_BYTE_1, at: 9)
        pktBytes.insert(VirtualCMPacket.COMM_CONTROLDIELD_BYTE_2 , at: 10)
        
        //byte 12 insert Message Numerator
        var msgNumerator = MyDefaults.getInt(key: DefaultKeys.MESSAGE_NUMERATOR)
        if msgNumerator == 255 {
            msgNumerator = 0
            MyDefaults.setInt(value: 0, key: DefaultKeys.MESSAGE_NUMERATOR)
        }
        
        pktBytes.insert(UInt8(msgNumerator), at: 11)
        msgNumerator = msgNumerator + 1
        MyDefaults.setInt(value: msgNumerator, key: DefaultKeys.MESSAGE_NUMERATOR)
        
        // byte 13 and 14
        pktBytes.insert(VirtualCMPacket.UNIT_HW_NUMBER, at: 12)
        pktBytes.insert(VirtualCMPacket.UNIT_SW_NUMBER, at: 13)
        
        // bytes 15
        pktBytes.insert(VirtualCMPacket.PROTOCOL_VERSION_IDENTIFIER, at: 14)
        
        // byte 16 and 17
        pktBytes.insert(VirtualCMPacket.BYTE_16, at: 15)
        pktBytes.insert(VirtualCMPacket.BYTE_17, at: 16)
        
        //bytes 18 and 19
        pktBytes.insert(VirtualCMPacket.TX_REASON_DATA, at: 17)
        pktBytes.insert(VirtualCMPacket.TX_REASON, at: 18)
        
        //byte 20
        if JourneyManager.sharedInstance.isAnyRideInProgress() {
            pktBytes.insert(VirtualCMPacket.UNITS_MODE_IGNITION_ON, at: 19)
        }else{
            pktBytes.insert(VirtualCMPacket.UNITS_MODE_IGNITION_OFF, at: 19)
            
        }
        //bytes 21 tp 24
        pktBytes.insert(VirtualCMPacket.UNITS_IO_STATUS_1, at: 20)
        pktBytes.insert(VirtualCMPacket.UNITS_IO_STATUS_2, at: 21)
        pktBytes.insert(VirtualCMPacket.UNITS_IO_STATUS_3, at: 22)
        pktBytes.insert(VirtualCMPacket.UNITS_IO_STATUS_4, at: 23)
        
        //byte 25
        pktBytes.insert(VirtualCMPacket.CURRENT_GSM_OPERATOR, at: 24)
        
        //byte 26 to 29
        pktBytes.insert(VirtualCMPacket.ANALOG_INPUT_1, at: 25)
        pktBytes.insert(VirtualCMPacket.ANALOG_INPUT_2, at: 26)
        pktBytes.insert(VirtualCMPacket.ANALOG_INPUT_3, at: 27)
        pktBytes.insert(VirtualCMPacket.ANALOG_INPUT_4, at: 28)
        
        //byte 30 and 32
        pktBytes.insert(VirtualCMPacket.MILEAGE_COUNTER_BYTE_1 , at: 29)
        pktBytes.insert(VirtualCMPacket.MILEAGE_COUNTER_BYTE_2 , at: 30)
        pktBytes.insert(VirtualCMPacket.MILEAGE_COUNTER_BYTE_3 , at: 31)
        
        //bytes 33 to 38
        pktBytes.insert(VirtualCMPacket.MULTI_PERPOSE_FIELD_BYTE_1, at: 32)
        pktBytes.insert(VirtualCMPacket.MULTI_PERPOSE_FIELD_BYTE_2, at: 33)
        pktBytes.insert(VirtualCMPacket.MULTI_PERPOSE_FIELD_BYTE_3, at: 34)
        pktBytes.insert(VirtualCMPacket.MULTI_PERPOSE_FIELD_BYTE_4, at: 35)
        pktBytes.insert(VirtualCMPacket.MULTI_PERPOSE_FIELD_BYTE_5, at: 36)
        pktBytes.insert(VirtualCMPacket.MULTI_PERPOSE_FIELD_BYTE_6, at: 37)
        
        //bytes 39 and 40
        pktBytes.insert(VirtualCMPacket.LAST_GPS_FIX_BYTE_1, at: 38)
        pktBytes.insert(VirtualCMPacket.LAST_GPS_FIX_BYTE_2, at: 39)
        
        //byte 41
        pktBytes.insert(VirtualCMPacket.LOCATION_STATUS, at: 40)
        
        //bytes 42 and 43
        
        if deviceLocation.horizontalAccuracy < 30 && deviceLocation.speed > 1.5 {
            pktBytes.insert(VirtualCMPacket.MODE_1, at: 41)
            pktBytes.insert(VirtualCMPacket.MODE_2, at: 42)
        }else{
            pktBytes.insert(VirtualCMPacket.MODE_LOW, at: 41)
            pktBytes.insert(VirtualCMPacket.MODE_LOW, at: 42)
        }
        
        pktBytes.insert(VirtualCMPacket.NUMBER_OF_SATELLITES, at: 43)
        
        //Append lattitude
        
        var lng = deviceLocation.coordinate.longitude
        
        lng = lng * (.pi / 180) * 100000000
        
        let lngInt = UInt32(bitPattern: Int32(lng))
        
        let lng_1 = UInt8((lngInt & 0x000000ff))
        let lng_2 = UInt8(((lngInt & 0x0000ff00) >> 8))
        let lng_3 = UInt8(((lngInt & 0x00ff0000) >> 16))
        let lng_4 = UInt8(((lngInt & 0xff000000)>>24))
        
        
        pktBytes.insert(lng_1, at: 44)
        pktBytes.insert(lng_2, at: 45)
        pktBytes.insert(lng_3, at: 46)
        pktBytes.insert(lng_4, at: 47)
        
        var lat = deviceLocation.coordinate.latitude
        
        lat = lat * (.pi / 180) * 100000000
        
        let latInt = UInt32(bitPattern: Int32(lat))
        
        let lat_1 = UInt8((latInt & 0x000000ff))
        let lat_2 = UInt8(((latInt & 0x0000ff00) >> 8))
        let lat_3 = UInt8(((latInt & 0x00ff0000) >> 16))
        let lat_4 = UInt8(((latInt & 0xff000000)>>24))
        
        pktBytes.insert(lat_1, at: 48)
        pktBytes.insert(lat_2, at: 49)
        pktBytes.insert(lat_3, at: 50)
        pktBytes.insert(lat_4, at: 51)
        
        let alt = Int32(deviceLocation.altitude * 100)
        let altitude =  UInt32(bitPattern: alt)
        
        let alt_1 = UInt8((altitude & 0x000000ff))
        let alt_2 = UInt8(((altitude & 0x0000ff00) >> 8))
        let alt_3 = UInt8(((altitude & 0x00ff0000) >> 16))
        let alt_4 = UInt8(((altitude & 0xff000000)>>24))
        
        pktBytes.insert(alt_1, at: 52)
        pktBytes.insert(alt_2, at: 53)
        pktBytes.insert(alt_3, at: 54)
        pktBytes.insert(alt_4, at: 55)
        
        let groundSpeed: UInt32?
        
        if deviceLocation.speed < 0 {
            groundSpeed = 0
        } else {
            
            groundSpeed = UInt32(deviceLocation.speed) * 100
        }
        
        let speed_1 = UInt8((groundSpeed! & 0x000000ff))
        let speed_2 = UInt8(((groundSpeed! & 0x0000ff00) >> 8))
        let speed_3 = UInt8(((groundSpeed! & 0x00ff0000) >> 16))
        let speed_4 = UInt8(((groundSpeed! & 0xff000000)>>24))
        
        pktBytes.insert(speed_1, at: 56)
        pktBytes.insert(speed_2, at: 57)
        pktBytes.insert(speed_3, at: 58)
        pktBytes.insert(speed_4, at: 59)
        
        
        // direction course
        let course: Double?
        if deviceLocation.course < 0 {
            course = 0
        } else {
            course = deviceLocation.course
        }
        let courseHex = UInt16(course! * (.pi / 180) * 1000)
        
        let course_1 = UInt8((courseHex & 0x000000ff))
        let course_2 = UInt8(((courseHex & 0x0000ff00) >> 8))
        
        
        pktBytes.insert(course_1, at: 60)
        pktBytes.insert(course_2, at: 61)
        
        //
        let today = Date()
        //append seconds
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MMM-dd HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        
        dateFormatter.dateFormat = "ss"
        let seconds = UInt8(dateFormatter.string(from: today))
        pktBytes.insert(seconds!, at: 62)
        
        dateFormatter.dateFormat = "mm"
        let minutes = UInt8(dateFormatter.string(from: today))
        pktBytes.insert(minutes!, at: 63)
        
        dateFormatter.dateFormat = "HH"
        let hours = UInt8(dateFormatter.string(from: today))
        pktBytes.insert(hours!, at: 64)
        
        dateFormatter.dateFormat = "dd"
        let day = UInt8(dateFormatter.string(from: today))
        pktBytes.insert(day!, at: 65)
        
        dateFormatter.dateFormat = "MM"
        let month = UInt8(dateFormatter.string(from: today))
        pktBytes.insert(month!, at: 66)
        
        dateFormatter.dateFormat = "yyyy"
        let year = UInt16(dateFormatter.string(from: today))
        
        let year_1 = UInt8((year! & 0x000000ff))
        let year_2 = UInt8(((year! & 0x0000ff00) >> 8))
        
        pktBytes.insert(year_1, at: 67)
        pktBytes.insert(year_2, at: 68)
        
        for i in 4 ... 68 {
            checksum = checksum + UInt16(pktBytes[i])
        }
        
        
        let checksum_2 = UInt8((checksum & 0x000000ff))
        pktBytes.insert(checksum_2, at: 69)
        
        self.addPacket(array: pktBytes)
        //SocketConnectionManager.sharedInstance.writeToSocket(data: pktBytes)
        
    }
    
    //Check whether VCM packets are present in  CoreData
    func checkVcmPacket()  {
        if let packets:[VCMPacket] = self.fetchPackets(),
            packets.count > 0 {
            print("VCM Packet count \(packets.count)")
            self.sendPacketToCloud()
            }else{
            print("VCM no packets")
        }
    }
    
    //Validate received packet response
    func validateReceivedPacket(packet: [UInt8]) -> Bool {
        
        if packet.count == 28 {
            let validPacketSignature = "MCGP"
            
            let firstFour = packet[0...3]
            
            if let encodedString: String = String(bytes: firstFour, encoding: .utf8){
                
                if validPacketSignature == encodedString && packet[27] == calculateChecksum(packet: packet) {
                    
                    return true
                }
            }
        }
        return false
    }
    
    //calculate checksum of received response packet
    func calculateChecksum(packet:[UInt8]) -> UInt8 {
        
        var checksum:Int = 0
        for i in 4...26 {
            checksum = checksum + Int(packet[i])
        }
        return UInt8(checksum % 256)
    }
    
    //Write VCM packet to Socket
    func sendPacketToCloud() {
        
        if (NetworkReachabilityManager()?.isReachable)! {
            
            var ride: RidePacket?
            ride = fetchRides().first
            
            print("VCM: Ride id : \(String(describing: ride?.rideId))")
            if let packets = ride?.packetArray?.allObjects as? [VCMPacket],
                
                packets.count > 0 {
                print("VCM: Sending packet \(Array(packets.first?.packetData as! Data)))")
                VCMPacketManager.vcmPacket = packets.first
                SocketConnectionManager.sharedInstance.writeToSocket(data: Array(packets.first?.packetData as! Data))
            }else{
                // save last packet rideId
                if let rideId:Int32 = ride?.rideId {
                    print(" VCM zero packet ride id \(Int(rideId))")
                    if JourneyManager.sharedInstance.currentRideId() != rideId {
                        
                        JourneyManager.sharedInstance.saveLastPacketSentTime(rideId: Int(rideId))
                        JourneyManager.sharedInstance.recalulateRideStats(rideId: Int(rideId))
                        print("VCM: Deleting ridepacket with ride id \(rideId)")
                        deleteRidePacket(packet: ride!)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 30, execute: {
                            print("Is main thread: \(Thread.current.isMainThread) id: \(rideId)")
                            JourneyManager.sharedInstance.recalulateRideStats(rideId: Int(rideId))
                            
                        })
                        self.checkVcmPacket()
                        
                    }else {
                        print("VCM : Ride id is \(rideId) and has packets \(ride?.packetArray?.count)")
                        //deleteRidePacket(packet: ride!)
                    }
                    
                    
                }
            }
            
        }else{
                print("No Internet")
        }
        
}
    
    // MARK:- SocketManagerDelegate Methods
    func receivedInputSteram(bytes: [UInt8]) {
        
        let result =  self.validateReceivedPacket(packet: bytes)
        print("VCM: Received inpit status \(result)")
        if result {
            if let _ = VCMPacketManager.vcmPacket {
                self.deleteVCMPacket(packet: VCMPacketManager.vcmPacket!)
                VCMPacketManager.vcmPacket = nil
            }
            self.sendPacketToCloud()
        }else{
            if let count = retryCount,
                count < 4 {
                print("VCM : Retry count")
                self.sendPacketToCloud()
            }
        }
        print("Result : \(result)")
        
    }
    
    func writeToSocketFailed() {
        print("VCM: writing failed")
        if let _ = retryCount {
            retryCount = retryCount! + 1
            if retryCount! < 4 {
              //  self.sendPacketToCloud()
                perform(#selector(sendPacketToCloud), with: nil, afterDelay: 1)
                retryCount = 0
            }
        }
        vcmDelegate?.writingFailed()
    }
    
}

