//
//  OfflineStats+CoreDataProperties.swift
//  Maximus
//
//  Created by Isha Ramdasi on 16/07/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//
//

import Foundation
import CoreData
import CoreLocation

extension OfflineStats {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<OfflineStats> {
        return NSFetchRequest<OfflineStats>(entityName: "OfflineStats")
    }

    @NSManaged public var averageSpeed: Double
    @NSManaged public var distanceTravelled: Double
    @NSManaged public var lastGoodLocation: NSObject?
    @NSManaged public var lastPacketTime: NSDate?
    @NSManaged public var rideId: Int64
    @NSManaged public var rideLocations: NSObject?
    @NSManaged public var timeTravelled: Int64
    @NSManaged public var topSpeed: Int64

}
extension OfflineStats {
    class func fetchOfflineStatsWithRideId(rideId: Int16, context: NSManagedObjectContext) -> OfflineStats? {
        do {
            let offlineStatsFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "OfflineStats")
            offlineStatsFetch.predicate = NSPredicate(format: "rideId == %d", rideId)
            
            if let offlineStats = try context.fetch(offlineStatsFetch).first as? OfflineStats {
                return offlineStats
            } else {
                print("Ride Id")
            }
        } catch {
            
        }
        return nil
    }
    
    class func updateOfflineStatsObj(distanceTravelled: Double, travelledTime: TimeInterval, averageSpeed: Double, topSpeed: Double, lastGoodLocation: CLLocation, rideLocations: [CLLocation], rideID: Int16, context: NSManagedObjectContext) {
        if let offlineStats = self.fetchOfflineStatsWithRideId(rideId: rideID, context: context) {
            offlineStats.distanceTravelled = distanceTravelled
            offlineStats.timeTravelled = Int64(travelledTime)
            offlineStats.topSpeed = Int64(topSpeed)
            offlineStats.averageSpeed = averageSpeed
            
            let archivedLocations = NSKeyedArchiver.archivedData(withRootObject: rideLocations)
            offlineStats.rideLocations = archivedLocations as NSObject
            
            let archivedLastLocation = NSKeyedArchiver.archivedData(withRootObject: lastGoodLocation)
            offlineStats.lastGoodLocation = archivedLastLocation as NSObject
            
            do {
                try context.save()
            } catch {
                
            }
        }
    }
    
    class func updateLastPacketTime(time: NSDate, context: NSManagedObjectContext, rideID: Int16) {
        if let offlineStats = self.fetchOfflineStatsWithRideId(rideId: rideID, context: context) {
            offlineStats.lastPacketTime = time
        }
        do {
            try context.save()
        } catch {
            
        }
    }
    
}
