//
//  RideMemberDetails+CoreDataProperties.swift
//  Maximus
//
//  Created by Isha Ramdasi on 21/06/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//
//

import Foundation
import CoreData


extension RideMemberDetails {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RideMemberDetails> {
        return NSFetchRequest<RideMemberDetails>(entityName: "RideMemberDetails")
    }

    @NSManaged public var biker_id: String?
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var location_timestamp: String?
    @NSManaged public var name: String?
    @NSManaged public var phone_no: String?
    @NSManaged public var profile_image_url: String?
    @NSManaged public var ride_member_id: String?
    @NSManaged public var status: Int16

}
