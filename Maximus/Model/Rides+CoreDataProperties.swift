//
//  Rides+CoreDataProperties.swift
//  Maximus
//
//  Created by Isha Ramdasi on 27/02/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//
//

import Foundation
import CoreData


extension Rides {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Rides> {
        return NSFetchRequest<Rides>(entityName: "Rides")
    }

    @NSManaged public var admin_biker_id: String?
    @NSManaged public var admin_profile_image: String?
    @NSManaged public var background_image: String?
    @NSManaged public var biker_ID: String?
    @NSManaged public var biker_status: Int16
    @NSManaged public var endLocation_lat: Double
    @NSManaged public var endLocation_long: Double
    @NSManaged public var endLocation_name: String?
    @NSManaged public var finish_date: String?
    @NSManaged public var joined_users: Int16
    @NSManaged public var planned: Bool
    @NSManaged public var public_ride: Bool
    @NSManaged public var ride_title: String?
    @NSManaged public var rideID: String?
    @NSManaged public var round_trip: String?
    @NSManaged public var start_date: String?
    @NSManaged public var startLocation_lat: Double
    @NSManaged public var startLocation_long: Double
    @NSManaged public var startLocation_name: String?
    @NSManaged public var status: Int16
    @NSManaged public var type: String?
    @NSManaged public var achievement: AchievementDataObj?
    @NSManaged public var waypoints: NSSet?
    @NSManaged public var locationDetails: NSSet?
    @NSManaged public var total_ride_distance: String?
    @NSManaged public var total_ride_time: String?
    @NSManaged public var routeList: NSObject?
    @NSManaged public var rideMemberList: NSSet?
    @NSManaged public var route: String?
    @NSManaged public var tbt_route: String?
    @NSManaged public var rideInProgressStatus: Int16
    @NSManaged public var honorBadge: NSObject?
    @NSManaged public var isSynced: Int16
}

// MARK: Generated accessors for waypoints
extension Rides {

    @objc(addWaypointsObject:)
    @NSManaged public func addToWaypoints(_ value: WaypointsDetails)

    @objc(removeWaypointsObject:)
    @NSManaged public func removeFromWaypoints(_ value: WaypointsDetails)

    @objc(addWaypoints:)
    @NSManaged public func addToWaypoints(_ values: NSSet)

    @objc(removeWaypoints:)
    @NSManaged public func removeFromWaypoints(_ values: NSSet)

}

// MARK: Generated accessors for locationDetails
extension Rides {

    @objc(addLocationDetailsObject:)
    @NSManaged public func addToLocationDetails(_ value: LocationDetails)

    @objc(removeLocationDetailsObject:)
    @NSManaged public func removeFromLocationDetails(_ value: LocationDetails)

    @objc(addLocationDetails:)
    @NSManaged public func addToLocationDetails(_ values: NSSet)

    @objc(removeLocationDetails:)
    @NSManaged public func removeFromLocationDetails(_ values: NSSet)

}

// MARK: Generated accessors for rideMemberList
extension Rides {
    
    @objc(addRideMemberListObject:)
    @NSManaged public func addToLocationDetails(_ value: RideMemberDetails)
    
    @objc(removeRideMemberListObject:)
    @NSManaged public func removeFromRideMemberList(_ value: RideMemberDetails)
    
    @objc(addRideMemberList:)
    @NSManaged public func addToRideMemberList(_ values: NSSet)
    
    @objc(removeRideMemberList:)
    @NSManaged public func removeFromRideMemberList(_ values: NSSet)
    
}

extension Rides {

    class func insertMyRideCard(rideCreateResponse: RideCreateResponse, context: NSManagedObjectContext, route: String, tbtRoute: String) {
        if let ride = Rides.fetchRideWithID(rideID: String(rideCreateResponse.id ?? 0), context: context) {
            if ride.biker_status != 7 {
                self.insertRideDetails(rideCreateResponse: rideCreateResponse, context: context, route: route, tbtRoute: tbtRoute)
            }
        } else {
            self.insertRideDetails(rideCreateResponse: rideCreateResponse, context: context, route: route, tbtRoute: tbtRoute)
        }
        
    }
    
    class func insertRideDetails(rideCreateResponse: RideCreateResponse, context: NSManagedObjectContext, route: String, tbtRoute: String) {
        var rideLogObj: Rides?

        if !isRideCardPresentInDB(rideId:String(rideCreateResponse.id ?? 0), context: context) {
            rideLogObj = Rides(context: context)
        } else {
            rideLogObj = self.fetchRideWithID(rideID: String(rideCreateResponse.id ?? 0), context: context)
        }
        print("Ride Card Title: \(rideCreateResponse.title ?? "")")
        rideLogObj?.biker_ID = String( rideCreateResponse.biker?.id ?? 0)
        rideLogObj?.admin_biker_id = String(rideCreateResponse.admin_biker_id ?? 0)
        rideLogObj?.finish_date = rideCreateResponse.finish_date
        rideLogObj?.rideID = String(rideCreateResponse.id ?? 0)
        rideLogObj?.start_date = rideCreateResponse.start_date
        rideLogObj?.status = (rideCreateResponse.status != nil)  ?  Int16(NSNumber(value: rideCreateResponse.status ?? 0)) : (rideLogObj?.status ?? 0)
        rideLogObj?.ride_title = rideCreateResponse.title
        rideLogObj?.background_image = rideCreateResponse.background_imag
        
        rideLogObj?.startLocation_lat =  (rideCreateResponse.start_location?.latlong?.lat == nil) ? (rideLogObj?.startLocation_lat ?? 0) : (rideCreateResponse.start_location?.latlong?.lat ?? 0)
        rideLogObj?.startLocation_long = (rideCreateResponse.start_location?.latlong?.lng == nil) ? (rideLogObj?.startLocation_long ?? 0) : (rideCreateResponse.start_location?.latlong?.lng ?? 0)
        rideLogObj?.startLocation_name = rideCreateResponse.start_location?.name
        rideLogObj?.endLocation_lat = (rideCreateResponse.end_location?.latlong?.lat == nil) ? (rideLogObj?.endLocation_lat ?? 0) : rideCreateResponse.end_location?.latlong?.lat ?? 0
        rideLogObj?.endLocation_long = (rideCreateResponse.end_location?.latlong?.lng == nil) ? (rideLogObj?.endLocation_long ?? 0) : rideCreateResponse.end_location?.latlong?.lng ?? 0
        rideLogObj?.endLocation_name = rideCreateResponse.end_location?.name
        rideLogObj?.public_ride = rideCreateResponse.public_ride!
        rideLogObj?.biker_status = (rideCreateResponse.bikerstatus != nil) ?  Int16(NSNumber(value: rideCreateResponse.bikerstatus ?? 0)) : (rideLogObj?.biker_status ?? 0)
        rideLogObj?.type =  String(rideCreateResponse.type!)
        rideLogObj?.planned = ((rideCreateResponse.planned == nil) ? rideLogObj?.planned : rideCreateResponse.planned)!
        rideLogObj?.round_trip = rideCreateResponse.round_trip
        rideLogObj?.joined_users = Int16(NSNumber(value: (rideCreateResponse.users_joined)!))
        rideLogObj?.rideInProgressStatus = Int(rideLogObj?.status ?? 0) == 1 ? Int16(NSNumber(value: RideInProgressStatus.inProgress)) : Int16(NSNumber(value: RideInProgressStatus.notStarted))
        rideLogObj?.route = route != "" ? route : rideLogObj?.route
        rideLogObj?.tbt_route = tbtRoute != "" ? tbtRoute : rideLogObj?.tbt_route
        rideLogObj?.total_ride_distance = rideLogObj?.total_ride_distance == "0" ? String(rideCreateResponse.distance ?? 0) : rideLogObj?.total_ride_distance
        rideLogObj?.total_ride_time = rideLogObj?.total_ride_time == "0" ?  String(rideCreateResponse.duration ?? 0) : rideLogObj?.total_ride_time
        rideLogObj?.isSynced =  rideLogObj?.isSynced ?? 0
        do {
            try context.save()
        } catch {
            print("failed to save ride card \(rideCreateResponse.id ?? 0)")
        }
    }
    class func insertAdhocRideCard(dummyRideID: String, dummyRideTitle: String, context: NSManagedObjectContext) {
        var rideLogObj: Rides?
        if !isRideCardPresentInDB(rideId:dummyRideID, context: context) {
            rideLogObj = Rides(context: context)
        } else {
            rideLogObj = self.fetchRideWithID(rideID: dummyRideID, context: context)
        }
        rideLogObj?.biker_ID = String(MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID))
        rideLogObj?.planned = false
        rideLogObj?.rideID = dummyRideID
        rideLogObj?.ride_title = dummyRideTitle
        rideLogObj?.status = 1
        rideLogObj?.biker_status = 6
        rideLogObj?.start_date = Date().dateToStringWithFormatTTTT()

        let achivement = AchievementDataObj(context: context)
        achivement.ride_start_date = Date().dateToStringWithFormat()
        
        rideLogObj?.achievement = achivement
        do {
            print("Inserted adhoc ride with id : \(dummyRideID)")
            try context.save()
        } catch {
            print("failed to save ad hoc ride \(dummyRideID)")
        }
    }
    
    class func insertRideLogDetails(rideLog: RideLogModel, context: NSManagedObjectContext) {
        var rideLogObj: Rides?
        var achievementObj : AchievementDataObj?
        print("Ride title \(rideLog.ride_title ?? "")")
        if !isRideCardPresentInDB(rideId: String(rideLog.ride_id ?? 0), context: context) {
            rideLogObj = Rides(context: context)
            achievementObj = AchievementDataObj(context: context)
            achievementObj?.rideID = String(rideLog.ride_id ?? 0)
        } else {
            rideLogObj = self.fetchRideWithID(rideID: String(rideLog.ride_id ?? 0), context: context)
            if rideLogObj?.achievement?.time_duration != 0.0 {
                achievementObj = rideLogObj?.achievement
            } else {
                achievementObj = AchievementDataObj.fetchStatsWithRideId(rideId: String(rideLog.ride_id ?? 0), context: context)
            }
            if achievementObj == nil {
                achievementObj = AchievementDataObj(context: context)
                achievementObj?.rideID = String(rideLog.ride_id ?? 0)
            }
        }
        if rideLog.achivementdata != nil {
            achievementObj?.average_speed = ((rideLog.achivementdata?.average_speed ?? 0) == 0) ? (achievementObj?.average_speed ?? 0) : rideLog.achivementdata?.average_speed ?? 0
            achievementObj?.distance = Double((rideLog.achivementdata?.distance ?? 0)!) == 0 ? achievementObj?.distance ?? 0 : Double(rideLog.achivementdata?.distance ?? 0)
            achievementObj?.distance_travelled = ((rideLog.achivementdata?.distance_travelled ?? 0) == 0) ? achievementObj?.distance_travelled ?? 0 : rideLog.achivementdata?.distance_travelled ?? 0

            achievementObj?.time_duration = ((rideLog.achivementdata?.time_duration ?? 0) == 0) ? Double(achievementObj?.time_duration ?? 0) : Double(rideLog.achivementdata?.time_duration ?? 0)
            achievementObj?.top_speed = ((rideLog.achivementdata?.top_speed ?? 0) == 0)  ? Double(achievementObj?.top_speed ?? 0)  : Double(rideLog.achivementdata?.top_speed ?? 0)
            achievementObj?.ride_end_date = rideLog.achivementdata?.ride_end_time
            achievementObj?.ride_start_date = rideLog.achivementdata?.ride_start_time
            if let topSpeedTime = Double(rideLog.achivementdata?.topspeedtimerange ?? "") {
                achievementObj?.topspeedtimerange = topSpeedTime
            }
        }
        
        rideLogObj?.achievement = achievementObj
        
        rideLogObj?.biker_ID = String(rideLog.biker_id ?? 0)
        rideLogObj?.admin_biker_id = String(rideLog.ride_admin_biker_id ?? 0)
        rideLogObj?.finish_date = rideLog.ride_finish_date
        rideLogObj?.rideID = String(rideLog.ride_id ?? 0)
        rideLogObj?.start_date = rideLog.ride_start_date
        rideLogObj?.status = Int16(NSNumber(value: rideLog.ride_status ?? 0))
        rideLogObj?.ride_title = rideLog.ride_title
        rideLogObj?.background_image = rideLog.rideback_img_url
        rideLogObj?.rideInProgressStatus = Int((rideLogObj?.status)!) == 1 ? Int16(NSNumber(value: RideInProgressStatus.inProgress)) : Int16(NSNumber(value: RideInProgressStatus.finished))
        rideLogObj?.total_ride_distance = rideLogObj?.total_ride_distance
        rideLogObj?.total_ride_time = rideLogObj?.total_ride_time
        rideLogObj?.biker_status = 7
        var array:[[String: String]] = []
        for j in 0..<(rideLog.honored_badges?.count ?? 0) {
            var badgeObj:[String: String] = [:]
            let badge = rideLog.honored_badges![j]
            badgeObj["resolution"] = badge.images?.first?.resolution
            badgeObj["type"] = badge.images?.first?.type
            badgeObj["url"] = badge.images?.first?.url
            array.append(badgeObj)
        }
        let archivedBadges = NSKeyedArchiver.archivedData(withRootObject: array)
        rideLogObj?.honorBadge = archivedBadges as NSObject
        do {
            try context.save()
        } catch {
            print("failed to save ridelog list")
        }
    }
    
    class func isRideCardPresentInDB(rideId: String, context: NSManagedObjectContext) -> Bool {
        let fetchRq = NSFetchRequest<Rides>(entityName: "Rides")
        let fetchpredicate = NSPredicate(format: "rideID == %@", rideId)
        fetchRq.predicate = fetchpredicate
        fetchRq.includesPropertyValues = true
        do {
            let rideLog = try context.fetch(fetchRq)
            if rideLog.count > 0{
                return true
            }
        } catch {
            
        }
        return false
    }
    
    class func deleteRide(rideID: String, context: NSManagedObjectContext) {
        do{
            let fetchRq = NSFetchRequest<Rides>(entityName: "Rides")
            fetchRq.predicate = NSPredicate(format: "rideID == %@", rideID)
            fetchRq.includesPropertyValues = true
            let rideCard = try context.fetch(fetchRq)
            if rideCard.count > 0 {
                context.delete(rideCard.first!)
            }
        } catch {
            
        }
        do {
            try context.save()
        } catch {
            
        }
    }
    
    class func deleteAllCompletedObjs(context: NSManagedObjectContext) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Rides")
        let predicate = NSPredicate(format: "status == 2")
        fetchRequest.predicate = predicate
        fetchRequest.includesPropertyValues = true
        do {
            if let ridesArray = try context.fetch(fetchRequest) as? [Rides] {
                for ride in ridesArray {
                    context.delete(ride)
                }
            }
        } catch {
            
        }
    }
    
    class func getNotCompletedMyRideObjs(context: NSManagedObjectContext) -> [Rides]{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Rides")
        let predicate = NSPredicate(format: "status != 1 && status != 2 && planned == true && (biker_status == \(RideMemberStatus.DEFAULT) || biker_status == \(RideMemberStatus.ACCEPTED))")
        fetchRequest.predicate = predicate
        fetchRequest.includesPropertyValues = true
        do {
            if let ridesArray = try context.fetch(fetchRequest) as? [Rides] {
               return ridesArray
            }
        } catch {
            
        }
        return []
    }
    
    class func getMyProgressRide(context: NSManagedObjectContext) -> Rides? {
        
        
        let fetchRequest = NSFetchRequest<Rides>(entityName: "Rides")
        fetchRequest.includesPropertyValues = true
        let predicate = NSPredicate(format: "biker_status == \(Int16(RideMemberStatus.STARTED)) && status == 1")

        fetchRequest.predicate = predicate
        do {
            let dataObj = try context.fetch(fetchRequest)
            if dataObj.count > 0 {
                return (dataObj.first)
            }
        } catch {
            
        }
        return nil
        
    }
    
    class func getRideInProgressObj(context: NSManagedObjectContext, type: RideType) -> Rides? {
        
        
        let fetchRequest = NSFetchRequest<Rides>(entityName: "Rides")
        fetchRequest.includesPropertyValues = true
        var predicate:NSPredicate! // = NSPredicate(format: "status == 1 && planned == true")

        if type == .PLANNED {
           predicate = NSPredicate(format: "biker_status == 6 && planned == true")
        }else{
            predicate = NSPredicate(format: "biker_status == 6")
        }
        
        fetchRequest.predicate = predicate
        do {
            let dataObj = try context.fetch(fetchRequest)
                if dataObj.count > 0 {
                return (dataObj.first)
            }
        } catch {
            
        }
        return nil
    }
    
    class func getRideInProgressObjArray(context: NSManagedObjectContext, type: RideType) -> [Rides] {
        
        
        let fetchRequest = NSFetchRequest<Rides>(entityName: "Rides")
        fetchRequest.includesPropertyValues = true
        
        if type == .PLANNED {
            let predicate = NSPredicate(format: "status == 1 && planned == true && (biker_status != \(RideMemberStatus.LEFT) && biker_status != \(RideMemberStatus.REJECTED) && biker_status != \(RideMemberStatus.FINISHED) && biker_status != \(RideMemberStatus.INVITED) )")
            fetchRequest.predicate = predicate
            do {
                let dataObj = try context.fetch(fetchRequest)
                if dataObj.count > 0 {
                    return dataObj
                }
            } catch {
                
            }
        }else{
            let predicate = NSPredicate(format: "status == 1")
            fetchRequest.predicate = predicate
            do {
                let dataObj = try context.fetch(fetchRequest)
                if dataObj.count > 0 {
                    return dataObj
                }
            } catch {
                
            }
        }
        
        
        return []
    }
    
    class func fetchRideWithID(rideID: String,context: NSManagedObjectContext) -> Rides? {
        let fetchRq = NSFetchRequest<Rides>(entityName: "Rides")
        let fetchpredicate = NSPredicate(format: "rideID == %@", rideID)
        fetchRq.predicate = fetchpredicate
        fetchRq.includesPropertyValues = true
        do {
            let rideLog = try context.fetch(fetchRq)
            if rideLog.count > 0{
                return rideLog.first!
            }
        } catch {
        }
        return nil
    }
    
    class func getAllCompletedRides(context: NSManagedObjectContext) -> [Rides]{
        let fetchRq = NSFetchRequest<Rides>(entityName: "Rides")
        let fetchpredicate = NSPredicate(format: "biker_status == 7")
        let sortDescriptor = NSSortDescriptor(key: "achievement.ride_end_date", ascending: false)
        fetchRq.predicate = fetchpredicate
        fetchRq.sortDescriptors = [sortDescriptor]
        fetchRq.includesPropertyValues = true
        do {
            let rideLog = try context.fetch(fetchRq)
            if rideLog.count > 0{
                return rideLog
            }
        } catch {
        }
        return []
    }
    
    class func getLastTenCompletedRides(context: NSManagedObjectContext) -> [Rides]{
        let fetchRq = NSFetchRequest<Rides>(entityName: "Rides")
        let fetchpredicate = NSPredicate(format: "biker_status == 7")
        fetchRq.predicate = fetchpredicate
        let sortDescriptor = NSSortDescriptor(key: "rideID", ascending: false)
        fetchRq.sortDescriptors = [sortDescriptor]
        fetchRq.fetchLimit = 10
        fetchRq.includesPropertyValues = true
        do {
            let rideLog = try context.fetch(fetchRq)
            if rideLog.count > 0{
                return rideLog
            }
        } catch {
        }
        return []
    }
    
    class func getAllRidesDetails(context: NSManagedObjectContext) -> [Rides] {
        do {
            return  try context.fetch(Rides.fetchRequest())
        } catch {
            
        }
        return []
    }
    
    class func updateRouteDetailsFor(rideID: String, context: NSManagedObjectContext, updatedDetails:RouteDetails, routeDetails: SingleRouteDetails) {
        if isRideCardPresentInDB(rideId: rideID, context: context) {
            if let rideDetails = fetchRideWithID(rideID: rideID, context: context) {            
                 let legs:[Legs] = (updatedDetails.routeList?.first?.legs)!
                var arrOfAllPoints:[String] = []
                
                if legs.count > 0{
                    
                    for leg in legs{
                        let steps:[Steps] = leg.steps!
                        if steps.count > 0{
                            for step in steps{
                                let polylinePoint:StepPolyline = step.polylinePoints!
                                if let point = polylinePoint.points{
                                    arrOfAllPoints.append(point)
                                }
                            }
                        }
                    }
                }
                let archivedBadges = NSKeyedArchiver.archivedData(withRootObject: arrOfAllPoints)
                rideDetails.routeList = archivedBadges as NSObject
                rideDetails.route = routeDetails.route
                rideDetails.tbt_route = routeDetails.tbt_route
                do {
                    try context.save()
                } catch {
                    
                }
            }
        }
    }
    
    class func addGraphDetails(rideID: String, context: NSManagedObjectContext, graphDetails: NSSet) {
        
        if isRideCardPresentInDB(rideId: rideID, context: context) {
            if let rideDetails = fetchRideWithID(rideID: rideID, context: context) {
                rideDetails.locationDetails = graphDetails
                do {
                    try context.save()
                } catch {
                    
                }
            }
        }
    }
    
    class func updateRideWithLatestBadge(rideID: String, context: NSManagedObjectContext, badgeArray: [[String: String]]) {
        if isRideCardPresentInDB(rideId: rideID, context: context) {
            if let rideDetails = fetchRideWithID(rideID: rideID, context: context) {
                let archivedBadges = NSKeyedArchiver.archivedData(withRootObject: badgeArray)
                rideDetails.honorBadge = archivedBadges as NSObject
                do {
                    try context.save()
                } catch {
                    
                }
            }
        }
    }
    
    class func updateDistanceAndDurationAsPerTBTRoute(rideID: String, context: NSManagedObjectContext, distance: Int, time: Int) {
        if isRideCardPresentInDB(rideId: rideID, context: context) {
            if let rideDetails = fetchRideWithID(rideID: rideID, context: context) {
                rideDetails.total_ride_time = String(time)
                rideDetails.total_ride_distance = String(distance)
                do {
                    try context.save()
                } catch {
                    
                }
            }
        }
    }
    
    class func addWaypointsInRide(rideID: String, context: NSManagedObjectContext, obj:[WavePointsList]) {
        var waypointArray:[WaypointsDetails] = []
        for i in 0..<obj.count {
            let wayPointList = WaypointsDetails(context: context)
            wayPointList.date_added = obj[i].date_added
            wayPointList.poi_id = obj[i].poi_id
            wayPointList.id = String(obj[i].id ?? 0)
            wayPointList.ride_id = String(obj[i].ride_id ?? 0)
            wayPointList.route_order = String(obj[i].route_order ?? 0)
            wayPointList.categories = obj[i].categories as NSObject?
            wayPointList.types = obj[i].types as NSObject?
            wayPointList.latitude = obj[i].location?.latitude ?? 0
            wayPointList.longitude = obj[i].location?.longitude ?? 0
            wayPointList.name = obj[i].name
            waypointArray.append(wayPointList)
        }
        if isRideCardPresentInDB(rideId: rideID, context: context) {
            if let rideDetails = fetchRideWithID(rideID: rideID, context: context) {
                rideDetails.waypoints = nil
                rideDetails.waypoints = NSSet(array: waypointArray)
            }
        }
        do {
            try context.save()
        } catch {
            
        }
    }
    
    class func addRideMemberDetails(rideID: String, context: NSManagedObjectContext, obj: [RideMember]) {
        var rideMembersArray: [RideMemberDetails] = []
        for i in 0..<obj.count {
            let rideMember = RideMemberDetails(context: context)
            rideMember.biker_id = String(obj[i].biker_id ?? 0)
            rideMember.latitude = obj[i].location?.latitude ?? 0
            rideMember.longitude = obj[i].location?.longitude ?? 0
            rideMember.location_timestamp = obj[i].location_timestamp
            rideMember.name = obj[i].name
            rideMember.phone_no = obj[i].phone_no
            rideMember.profile_image_url = obj[i].profile_image_url
            rideMember.ride_member_id = String(obj[i].ride_member_id ?? 0)
            rideMember.status = Int16(obj[i].status ?? 0)
            rideMembersArray.append(rideMember)
        }
        
        if isRideCardPresentInDB(rideId: rideID, context: context) {
            if let rideDetails = fetchRideWithID(rideID: rideID, context: context) {
                rideDetails.removeFromRideMemberList(rideDetails.rideMemberList!)
                rideDetails.rideMemberList = NSSet(array: rideMembersArray)
            }
            do {
                try context.save()
            } catch {
                
            }
        }       
    }
    
    class func getRideMembers(rideID: String, context: NSManagedObjectContext) -> [RideMemberDetails] {
        if isRideCardPresentInDB(rideId: rideID, context: context) {
            if let rideDetails = fetchRideWithID(rideID: rideID, context: context) {
                if let list = rideDetails.rideMemberList?.allObjects as? [RideMemberDetails] {
                    return list
                }
            }
        }
        return []
    }
    
    class func updateRideStatusAfterStartingRide(rideID: String, context: NSManagedObjectContext) {
        if isRideCardPresentInDB(rideId: rideID, context: context) {
            if let rideDetails = fetchRideWithID(rideID: rideID, context: context) {
                rideDetails.status = 1
                rideDetails.biker_status = 6
                
                let achivement = AchievementDataObj(context: context)
                achivement.ride_start_date = Date().dateToStringWithFormat()
                
                rideDetails.achievement = achivement
                
            }
            do {
                try context.save()
            } catch {
                print("Updating ride status failed \(rideID)")
            }
        }
    }
    
    class func setRideAsCompleted(rideID: String, context: NSManagedObjectContext) {
        print("CoreData : setRideAsCompleted \(rideID)")
        if isRideCardPresentInDB(rideId: rideID, context: context) {
            if let rideDetails = fetchRideWithID(rideID: rideID, context: context) {
                rideDetails.status = 2
                rideDetails.biker_status = 7
                rideDetails.finish_date = Date().dateToStringWithFormat()
                rideDetails.achievement?.ride_end_date = Date().dateToStringWithFormat()

                
                do {
                    print("CoreData : Updated Ride status \(rideID) ")
                    try context.save()
                } catch let error as NSError {
                    print("CoreData : Ride Status Stop failed \(rideID) \(error.localizedDescription) \(error.userInfo)")
                }
            }
            
        }
    }
    
    class func updateDummyRideID(oldRideId: String, newRideID: String, context: NSManagedObjectContext) {
        if isRideCardPresentInDB(rideId: oldRideId, context: context) {
            if let rideDetails = fetchRideWithID(rideID: oldRideId, context: context) {
                rideDetails.rideID = newRideID
            }
            do {
                try context.save()
            } catch {
                
            }
        }
    }
    
    class func updateRideTitle(title: String, rideID: String, context: NSManagedObjectContext) {
        if isRideCardPresentInDB(rideId: rideID, context: context) {
            if let rideDetails = fetchRideWithID(rideID: rideID, context: context) {
                rideDetails.ride_title = title
            }
            do {
                try context.save()
            } catch {
                
            }
        }
    }
    
    class func updateSyncFlag(rideID: String, context: NSManagedObjectContext) {
        if isRideCardPresentInDB(rideId: rideID, context: context) {
            if let rideDetails = fetchRideWithID(rideID: rideID, context: context) {
                rideDetails.isSynced = Int16(NSNumber(value: true))
                do {
                    try context.save()
                } catch {
                    
                }
            }
            
        }
    }
    
    class func getAllNotSyncedRides(context: NSManagedObjectContext) -> [Rides] {
        let fetchRq = NSFetchRequest<Rides>(entityName: "Rides")
        let fetchpredicate = NSPredicate(format: "isSynced == %d && planned == true", 0)
        fetchRq.predicate = fetchpredicate
        fetchRq.includesPropertyValues = true
        do {
            let rideLog = try context.fetch(fetchRq)
            if rideLog.count > 0{
                return rideLog
            }
        } catch {
        }
        return []
    }
}
