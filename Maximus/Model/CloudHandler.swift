//
//  CloudHandler.swift
//  Maximus
//
//  Created by Admin on 14/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import Alamofire

class CloudHandler
{
    
    let cloudManager = CloudManager()
    
    func makeAccuWeatherCloudRequest(subUrl:String,parameter:[String:Any],isheaderAUTH:Bool,requestType:HTTPMethod ,completionHandler: @escaping (DataResponse<Any>?) -> Void) {
        
        var headers = [String:String]()
        headers = RequestHeader.getHeadersWithouthAuth()
        cloudManager.makeCloudRequest(url: subUrl, parameter: parameter, requestType:requestType,   completionHandler: completionHandler)
    }
    
    
    func makeCloudRequest(subUrl:String,parameter:[String:Any],isheaderAUTH:Bool,requestType:HTTPMethod ,completionHandler: @escaping (DataResponse<Any>?) -> Void) {
        let strURL = "\(RequestURL.URL_BASE.rawValue)\(subUrl)"
        print("strURl \(strURL) ")
        if subUrl == RequestURL.URL_REGISTER.rawValue{
            self.cloudManager.makeCloudRequestWithClientCredentials(url: strURL, parameter: parameter, requestType:requestType,   completionHandler: completionHandler)
        }
        else{
            var url = strURL
            if subUrl == RequestURL.URL_AUTHENTICATION.rawValue{
                url = RequestURL.URL_OAUTH_ACCESS_TOKEN.rawValue
            }
            self.cloudManager.makeCloudRequest(url: url, parameter: parameter, requestType:requestType,   completionHandler: completionHandler)
        }
    }
    
    func makeCloudRequestForStringResponse(subUrl:String,parameter:[String:Any],isheaderAUTH:Bool,requestType:HTTPMethod ,completionHandler: @escaping (DataResponse<String>?) -> Void) {
        let strURL = "\(RequestURL.URL_BASE.rawValue)\(subUrl)"
        print("strURl \(strURL) ")
        let headers = [
            "Accept"        : "text/plain",
            "Content-Type"  : "application/json"
        ]
        if subUrl == RequestURL.URL_FORGOT_PASSWORD.rawValue{
            cloudManager.generateClientOauthAccessToken(url: strURL, parameter: parameter,header:headers, requestType: requestType, completionHandler: completionHandler)
        }else{
            cloudManager.makeCloudRequestForStringResponse(url: strURL, parameter: parameter, headers: headers, requestType:requestType,   completionHandler: completionHandler)
        }
    }
    
    
    func makeCloudGetWithCacheRequest(subUrl:String,parameter:[String:Any],isheaderAUTH:Bool,requestType:HTTPMethod ,completionHandler: @escaping (CachedURLResponse?,Int?) -> Void) {
        
        let strURL = "\(RequestURL.URL_BASE.rawValue)\(subUrl)"
        print("strURl ",strURL)
        var headers = [String:String]()
        if isheaderAUTH {
            headers = RequestHeader.getHeadersWithAuth()
        }else{
            headers = RequestHeader.getHeadersWithouthAuth()
        }
        
        cloudManager.makeCloudGetWithCacheRequest(url: strURL, parameter: parameter, headers: headers, requestType:requestType,   completionHandler: completionHandler)
    }
    
    
    func makeCloudPostRequestWithArray(subUrl:String,parameter:NSMutableArray,isheaderAUTH:Bool,requestType:HTTPMethod ,completionHandler: @escaping (DataResponse<Any>?) -> Void) {
        
        let strURL = "\(RequestURL.URL_BASE.rawValue)\(subUrl)"
        print("strURl ",strURL)
        var headers = [String:String]()
        if isheaderAUTH {
            headers = RequestHeader.getHeadersWithAuth()
        }else{
            headers = RequestHeader.getHeadersWithouthAuth()
        }
        
       cloudManager.makeCloudPostRequestWithArray(url: strURL, parameter: parameter, headers: headers, requestType:requestType,   completionHandler: completionHandler)
    }
    
    func uploadMultiPart(url:String, data:Data, name:String, filename:String, mimetype:String, completionHandler: @escaping (DataResponse<Any>)  -> Void){
        
        var headers = [String:String]()
        headers = RequestHeader.getHeaderForFormData()
        
        cloudManager.makeUploadRequest(url:url,data:data,name:name,filename:filename,mimetype:mimetype,headers:headers,completionHandler: completionHandler)
    }
    
    /*func fireBaseDynamicShortLinkRequest(subUrl:String,parameter:[String:Any],isheaderAUTH:Bool,requestType:HTTPMethod ,completionHandler: @escaping (DataResponse<Any>?) -> Void) {
        
        let strURL = "\(DynamicLinkShareURL.FIREBASE_URL.rawValue)\(subUrl)"
        
        print("Url ",strURL)
        var headers = [String:String]()
        if isheaderAUTH {
            headers = RequestHeader.getHeadersWithAuth()
        }else{
            headers = RequestHeader.getHeadersWithouthAuth()
        }
        print("header ",headers)
        CloudManager().makeCloudRequest(url: strURL, parameter: parameter, headers: headers, requestType:requestType,   completionHandler: completionHandler)
    }*/
    
    
}
