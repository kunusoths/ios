//
//  LocationDetails+CoreDataProperties.swift
//  Maximus
//
//  Created by Isha Ramdasi on 26/06/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//
//

import Foundation
import CoreData


extension LocationDetails {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<LocationDetails> {
        return NSFetchRequest<LocationDetails>(entityName: "LocationDetails")
    }
    
    @NSManaged public var max_ground_speed: Double
    @NSManaged public var min_ground_speed: Double
    @NSManaged public var ground_speed: Double
    @NSManaged public var packettimestamp: String?
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var rideID: String?
}

extension LocationDetails {
    class func fetchLocationDetailsOfRide(rideID: String, context: NSManagedObjectContext) -> Bool {
        let fetchRq = NSFetchRequest<LocationDetails>(entityName: "LocationDetails")
        fetchRq.predicate = NSPredicate(format: "rideID == %@", rideID)
        do {
             let  locationArray = try context.fetch(fetchRq)
             if locationArray.count > 0 {
                return true
             }
        } catch {
            
        }
        return false
    }
    
    class func fetchLocationDetailsOfRideID(rideID: String, context: NSManagedObjectContext) -> [LocationDetails] {
        let fetchRq = NSFetchRequest<LocationDetails>(entityName: "LocationDetails")
        fetchRq.predicate = NSPredicate(format: "rideID == %@", rideID)
        do {
            let  locationArray = try context.fetch(fetchRq)
            if locationArray.count > 0 {
                return locationArray
            }
        } catch {
            
        }
        return []
    }
}
