//
//  MyLocation.swift
//  Maximus
//
//  Created by Admin on 24/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import CoreLocation


//************ Class *******************
class Mylocation: NSLocation {
    
    var location:CLLocation? = nil
    
    init(loc:CLLocation) {
        self.location = loc
    }
    /** Name of the provider */
    public func getProvider()->String{
        return  StringConstant.GOOGLE
    }
    
    /** Meters / Second */
    public func getSpeed()->Double{
        return (location?.speed)!
    }
    
    public func hasSpeed()->Bool{
        return true
    }
    
    /** Degrees */
    public func getLatitude()->Double{
        return (location?.coordinate.latitude)!
    }
    
    public func getLongitude()->Double{
        return (location?.coordinate.longitude)!
    }
    
    /** Meters */
    public func getAltitude()->Double{
        return (location?.altitude)!
    }
    
    public func hasAltitude()->Bool{
        return true
    }
    
    /** Degrees */
    public func getBearing()->Double{
        return 0.0//(location?.getBearing())!
        
    }
    
    public func hasBearing()->Bool{
        return false//(location?.hasBearing())!
        
    }
    
    /** Meters */
    public func getAccuracy()->Double{
        return (location?.horizontalAccuracy)!
    }
    
    public func hasAccuracy()->Bool{
        if ((location?.horizontalAccuracy) != nil) {
            return true
        }else{
            return false
        }
    }
    

    /** Degrees */
    public func getBearingAccuracy()->Double{
        return 0.0//(location?.getBearingAccuracy())!
        
    }
    
    public func hasBearingAccuracy()->Bool{
        return false//(location?.hasBearingAccuracy())!
        
    }
    
    /** Meters / Second */
    public func getSpeedAccuracy()->Double{
        return (location?.speed)!
    }
    
    public func hasSpeedAccuracy()->Bool{
        return false//(location?.hasSpeedAccuracy())!
    }
    
    /** Meters */
    public func getVerticalAccuracy()->Double{
        return (location?.verticalAccuracy)!
    }
    
    public func hasVerticalAccuracy()->Bool{
        return true
    }
    
    /** Get the nanoseconds elapsed since boot */
    public func getElapsedTime()->Int64 {
        return Int64(self.getSystemElapsedTime())
    }
    
    /*****************************************************************************
     * Function :   getSystemElapsedTime
     * Input    :   none
     * Output   :   Elaspsed Time in Nano-seconds
     * Comment  :   To get the Elapsed Time since system boots
     *
     ****************************************************************************/
    
    func getSystemElapsedTime()->Double{
        ///* Manually Calculating Elappsed Time since System BOOT *////
        let info = ProcessInfo.processInfo
        let current = info.systemUptime
        let time = current - APP_BOOT_TIME //---In Seconds
        let nanoSeconds = time * 1000000000//--- 10^9 for Convert to NANO-Seconds
        // print("@NavigationLocator: NANOSECONDS = \(nanoSeconds)")
        return nanoSeconds
    }
}
