//
//  NavigationLocator.swift
//  Maximus
//
//  Created by Admin on 07/06/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import Alamofire
import CoreLocation



@objc protocol NavigationLocatorDelegate {
    
    @objc optional func stateChanged(_ navigator: NSNavigator?, state: NSNavigatorState)
    @objc optional func legUpdate(_ navigator: NSNavigator?, legProgress: NSLegProgress?)
    @objc optional func stepUpdate(_ navigator: NSNavigator?, stepProgress: NSStepProgress?)
    @objc optional func stepAdvice(_ navigator: NSNavigator?, stepProgress: NSStepProgress?, steps: [NSStep])
    @objc optional func maneuver(_ navigator: NSNavigator?, maneuver: NSManeuver?)
    @objc optional func update(_ navigator: NSNavigator?, status: NSStatus?)
    @objc optional func deviation(_ navigator: NSNavigator?, deviation: NSDeviation?)
    @objc optional func routeChanged(_ navigator: NSNavigator?, directions: NSDirections?, route: NSRoute?)
    ///*Testing Pusrpose Delegates to get the user current location & Rerouted Route*/
    @objc optional func updateCurrentLocation(location:CLLocation)
    @objc optional func reRoutedRoute(routeResponse:String)
    @objc optional func alert(_ navigator: NSNavigator?, alert: NSNavigationAlert?)

}


class NavigationLocator {
    
    var navigationLocatorDelegate:NavigationLocatorDelegate?
    fileprivate var index:Int = 0
    fileprivate var timerToGetLocation = Timer()
    fileprivate var navigationEngine = NSNavigationEngine.getInstance()
    fileprivate var navigator:NSNavigator?
    fileprivate var voiceNavigator: NSVoiceNavigator?
    fileprivate var speechManager = NSSpeechManager.getInstance()
    fileprivate var speechService = SpeechService.sharedInstance
    fileprivate var arrAllPoints:[NSLatLng]? = []
    fileprivate var _lastlocation:CLLocation? = nil
    fileprivate var _listeners:[NSLocationClient]?
    //fileprivate var navigationStatus:NavigationStatus = .Stop
    let deviceLocation = DeviceLocationServiece.sharedInstance()
    let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    
    ///* variable :navTestDelegate For Testing Purpose remove this if no Longer needed */
    var navTestDelegate:NavigationLocatorDelegate?
    
    init() {
    }
    
    ///Singleton instance of Class
    private static var privateShared : NavigationLocator?
    class func sharedInstance() -> NavigationLocator {
        guard let shared = privateShared else {
            privateShared = NavigationLocator()
            return privateShared!
        }
        return shared
    }
    
    ///Shared Instance Released here
    class func destroyInstance() {
        privateShared = nil
    }
    
    ///Temporay API call to  Get JSON data from google remove it if no longer needed
    //    func getJSONResponce(){
    //
    //        Alamofire.request("https://maps.googleapis.com/maps/api/directions/json?origin=pune&destination=lonavala&key=AIzaSyDqoox5zDORJT1oH34YuL5Vsr_nHaRrmlE").responseString { response in
    //
    //            print("@NL:Success: \(response.result.isSuccess)")
    //            print("@NL:Response String: \(response.result.value)")
    //            if let strresponse = response.result.value {
    //                self.directionFetcher(mDirectionResponse: strresponse)
    //            }
    //        }
    //    }
    
    /// Parser for Direction Json response
    func directionFetcher(mDirectionResponse:String){
        let directionsProvider = NSDirectionsProvider.getInstance(StringConstant.GOOGLE, key: Constants.APIKEY_TBT!)
        let directions:NSDirections = (directionsProvider?.parse(mDirectionResponse))!
        
        for route in  directions.getRoutes() {
            for legs in route.getLegs(){
                print(legs)
                for step in legs.getSteps() {
                    let encodedPoints = step.getPath()
                    let arrPoints = encodedPoints?.getPoints()
                    for point in arrPoints! {
                        arrAllPoints?.append(point)
                    }
                }
            }
        }
        self.initNavigationEngine(directionsProvider: directionsProvider!, directions: directions ,arrPoints:arrAllPoints!)
    }
    
    /// Initialise Navigation engine and Configuring the Basic componet
    func initNavigationEngine(directionsProvider:NSDirectionsProvider ,directions:NSDirections, arrPoints:[NSLatLng]){
        
        _listeners = [NSLocationClient]()
        navigationEngine =  NSNavigationEngine.getInstance()!
        directionsProvider.setNetworkService(self)
        
        // Create the location manager
        NSEnvironment.setFilesDir(documentsPath)
        NSEnvironment.setCacheFilesDir(documentsPath)
        
        // Create the location manager
        let locationManager = NSLocationManager.getInstance()
        
        // Register a location provider
        locationManager?.register(self)
        
        navigationEngine?.setLocationManager(locationManager)
        navigationEngine?.setNetworkService(self)
        navigationEngine?.setDirectionsProvider(directionsProvider)
        
        
        let avoid = [NSNumber]()
        let option = NSDirectionsOptions(alternatives: false, mode: .DIRECTIONSMODEDRIVING, waypoints: arrPoints, transitMode: .DIRECTIONSTRANSITMODEBUS, trafficModel: .DIRECTIONSTRAFFICMODELBESTGUESS, avoid: avoid , units: .DIRECTIONSUNITMETRIC, language: "", region: "")
        
        ///**************    WITH RESTORE FUNCTIONALITY *********************///
        
        ///--------------------------------------------------------------------
        /*
        //Get Saved Token to restore navigator object
       
        let strToken = LocalCacheData().getSavedNavigationToken()
        if !strToken.isBlank {
            
            
            if let objNavigator = navigationEngine?.restore(strToken) {
                navigator = objNavigator
                print("Restored Token: \(navigator.getIdentifier())")
                
            }else
            {
                // Create navigator
                navigator = (navigationEngine?.navigate(directions, route: directions.getRoutes().get(at: 0), options: option)!)!
            }
        }
        else {
            // Create navigator
            navigator = (navigationEngine?.navigate(directions, route: directions.getRoutes().get(at: 0), options: option)!)!
            
        }
        
        //Save Navigator in Nvaigation Engine
        navigationEngine?.save(navigator)
        // Subscribe to navigation events
        navigator.subscribe(self)
        
        let token = navigator.getIdentifier()
        print("Saved Token:\(token)")

        //Save Token String Locally
        LocalCacheData().saveNavigationToken(value: token)
        
        */
        ///--------------------------------------------------------------------

        ///**************    WITHOUT RESTORE FUNCTIONALITY *********************///

        ///--------------------------------------------------------------------

        // Create navigator
        navigator = (navigationEngine?.navigate(directions, route: directions.getRoutes().get(at: 0), options: option)!)!
        //Save Navigator in Nvaigation Engine
        navigationEngine?.save(navigator)
        
        //
        self.enableAudio()
        //self.disableAudioAttributes()
        voiceNavigator?.getAttributes()?.setBool(NSVoiceNavigatorEnabled, value: false)
        voiceNavigator?.getAttributes()?.setBool(NSVoiceNavigatorGuidance, value: false)
        voiceNavigator?.getAttributes()?.setBool(NSVoiceNavigatorHazardAlerts, value: false)
        voiceNavigator?.getAttributes()?.setBool(NSVoiceNavigatorProgressReports, value: false)
        voiceNavigator?.getAttributes()?.setBool(NSVoiceNavigatorAssistanceAlerts, value: false)
        
        // Subscribe to navigation events
        navigator?.subscribe(self)

        ///--------------------------------------------------------------------
        
        //Set Auto Re-Route Disable initially
        self.setAutomaticRouting(isON: true)
        
        
        if ISSimulationON {
            //Set up timer for Bench Testing Purpose
            timerToGetLocation = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.getNextLatLong), userInfo: nil, repeats: true)
        }
        else{
            ///Location Service Configuration
            deviceLocation.configureDeviceLocation()
            deviceLocation.deviceLocationDelegate = self
            //set up timer for Real Time TBT Navigation
            //timerToGetLocation = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.getUserLocationAndNotify), userInfo: nil, repeats: true)
        }
    }
    
    func enableAudio() {
        print("@VN Audio enabled")

        voiceNavigator = NSVoiceNavigator.getInstance(navigator)
        speechManager?.setSpeechService(speechService)
        speechManager?.add(voiceNavigator?.getUtteranceSource())
        
    }
    
    
    public func 	enableAudioAttributes() {
        print("@VN Audio Attributes enabled")
        voiceNavigator?.getAttributes()?.setBool(NSVoiceNavigatorEnabled, value: true)
        MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_AUDIO_STATUS)
        voiceNavigator?.getAttributes()?.setBool(NSVoiceNavigatorGuidance, value: true)
        voiceNavigator?.getAttributes()?.setBool(NSVoiceNavigatorHazardAlerts, value: true)
        voiceNavigator?.getAttributes()?.setBool(NSVoiceNavigatorProgressReports, value: true)
        voiceNavigator?.getAttributes()?.setBool(NSVoiceNavigatorAssistanceAlerts, value: true)
    }
    
    public func disableAudioAttributes() {
        print("@VN Audio Attributes disabled")
        MyDefaults.setBoolData(value: false, key:  DefaultKeys.KEY_AUDIO_STATUS)
        voiceNavigator?.getAttributes()?.setBool(NSVoiceNavigatorEnabled, value: false)
        voiceNavigator?.getAttributes()?.setBool(NSVoiceNavigatorGuidance, value: false)
        voiceNavigator?.getAttributes()?.setBool(NSVoiceNavigatorHazardAlerts, value: false)
        voiceNavigator?.getAttributes()?.setBool(NSVoiceNavigatorProgressReports, value: false)
        voiceNavigator?.getAttributes()?.setBool(NSVoiceNavigatorAssistanceAlerts, value: false)
    }
    
    public func getStatusOfNavigation()->NSNavigatorState {
        if let obj_Navigator = self.navigator {
            return obj_Navigator.getState()
        }else{
           return .NAVIGATORSTATEERROR
        }
    }
    
    /// Start the Navigation
    func startNavigationEngine() {
        // Start navigation
        navigator?.start()
        navigator?.getOptions()?.setDetectHazards(true)
        //self.navigationStatus = .Started
    }
    /// Stop the Navigation Engine
    func stopNavigationEngine() {
        // Stop navigation
        navigator?.stop()
        //self.navigationStatus = .Stop
        timerToGetLocation.invalidate() //---Stop Timer
        deviceLocation.stopUpdatingLocation()
        //NavigationLocator.destroyInstance()//----To Release Singleton Instance of Navigation
        LocalCacheData().deleteNavigationTokenKeyFromData()//----Delete Saved Token of Navigation Engine
    }
    
    /// Pasue Navigation Engine
    func pauseNavigationEngine() {
        // Pause navigation
        navigator?.pause()
        //self.navigationStatus = .Pause
    }
    
    /// Resume Navigation Engine
    func resumeNavigationEngine() {
        // Resume navigation
        navigator?.resume()
        //self.navigationStatus = .Resume
    }
    /// enable or Disable Re-routing
    func setAutomaticRouting(isON:Bool) {
        /// Set Auto Re-Route Enable or Disable
        navigator?.getOptions()?.setAutomaticRouting(isON)
    }
    
    
    ///* Temp method for on bench simulation of TBT through current route Array of lat & Long commment it if no longer neeeded*/
    @objc func getNextLatLong(){
        if ((arrAllPoints?.count)!-1 > index) {
            let point = self.arrAllPoints?[self.index]
            let location = CLLocation(latitude: (point?.latitude)!, longitude: (point?.longitude)!)
            deviceLocation.userLocation = location
            self.notify(location: location)
            self.index = self.index + 1
        }else{
            timerToGetLocation.invalidate()
            navigator?.stop()//--Stop Navigation Engine
            print("@NL:************ Array completed ************")
        }
    }
    
    ///* get User Current updated Location */
    @objc func getUserLocationAndNotify(){
        let loc = deviceLocation.getUserCurrentLocation()
        if loc.coordinate.latitude != 0 && loc.coordinate.longitude != 0 {
            self.notify(location: loc)
        }else{
            print("@NL:Lat long Nullll")
        }
    }
    
    
    func notify(location:CLLocation){
        // Preserve last location
        self._lastlocation = location
        self.navTestDelegate?.updateCurrentLocation!(location: location)
        // Create location input
        let mylocation = Mylocation(loc: location)
        
        // for asynchronous location updates
        for client in _listeners! {
            client.onLocationChanged(mylocation)
        }
    }
}


//MARK: Delegate calls of NSLocationProvider
extension NavigationLocator: NSLocationProvider {
    public func supportsBearing() -> Bool {
        print("@NL:Support Bearing")
        return false
    }
    
    func getName() -> String {
        print("@NL:get name")
        return "MyLocationProvider"
    }
    
    func getState() -> NSLocationProviderState {
        print("@NL:get state")
        return NSLocationProviderState.LPSAVAILABLE
    }
    
    func getLastKnownLocation() -> NSLocation? {
        print("@NL:get last Known location")
        return Mylocation(loc: _lastlocation!)
    }
    
    func supportsSpeed() -> Bool {
        print("@NL:supprt speed")
        return true
    }
    
    func supportsAltitude() -> Bool {
        print("@NL:support altitude")
        return true
    }
    
    func subscribe(_ listener: NSLocationClient?) -> String {
        print("@NL:subscribe")
        _listeners?.append(listener!)
        return "ALL"
    }
    
    func remove(_ listenerToken: String) {
        print("@NL:remove")
        _listeners?.removeAll()
    }
}

//MARK: Delegate calls of NSNetworkService
extension NavigationLocator: NSNetworkService {
    
    public func httpRequest(_ url: String, headers: [String : String], body: String, client: NSNetworkClient?) {
        
        if NetworkCheck.isNetwork() {
            print("@NL:Deviation URL: \(url)")
            FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "@NL:Deviation URL: \(url)")
            Alamofire.request(url).responseString { response in
                print("@NL:Deviation reroute Success: \(response.result.isSuccess)")
                print("@NL:Deviation reroute Response String: \(String(describing: response.result.value))")
                FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "@NL:Deviation status: \(response.response?.statusCode ?? 0) ")
                if let strresponse = response.result.value {
                    if (response.response?.statusCode)! >= 200 && (response.response?.statusCode)! < 300
                    {
                        
                        client?.httpResponse(Int16((response.response?.statusCode)!), headers: headers, body: strresponse)
                        //self.navTestDelegate?.reRoutedRoute!(routeResponse:strresponse)
                    }else{
                        client?.httpError(Int16((response.response?.statusCode)!), errorMessage: strresponse)
                    }
                }
            }
        }else{
            FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "No network ")
            client?.httpError(Int16(600), errorMessage: "No Network")
        }
        
    }
}


//MARK: Delegate calls of NSNavigationClient
extension NavigationLocator: NSNavigationClient {

    /*****************************************************************************
     * Function :   routeChanged
     * Input    :   none
     * Output   :   Return Updated Route
     * Comment  :   Event call after route changed(Re-routing)
     *
     ****************************************************************************/
    func routeChanged(_ navigator: NSNavigator?, directions: NSDirections?, route: NSRoute?) {
        print("@NL:- Route Changed called")
        
        if JourneyManager.sharedInstance.getAlertMode() == AlertModeStatus.Off{
            
        self.navigationLocatorDelegate?.routeChanged!(navigator, directions: directions, route: route)
        }
    }
    /*****************************************************************************
     * Function :   stateChanged
     * Input    :   none
     * Output   :   Naviagtion State change
     * Comment  :   Event call after Navigation state changes
     *
     ****************************************************************************/
    public func stateChanged(_ navigator: NSNavigator?, state: NSNavigatorState) {
        print("NL:- StateChanged")
        if JourneyManager.sharedInstance.getAlertMode() == AlertModeStatus.Off{
            
        self.navigationLocatorDelegate?.stateChanged!(navigator, state: state)
        }
    }
    /*****************************************************************************
     * Function :   legUpdate
     * Input    :   none
     * Output   :   legUpdates
     * Comment  :   Event call after every legUpdate
     *
     ****************************************************************************/
    func legUpdate(_ navigator: NSNavigator?, legProgress: NSLegProgress?) {
        print("NL:- Leg Updated")
        
        if JourneyManager.sharedInstance.getAlertMode() == AlertModeStatus.Off{
            
        self.navigationLocatorDelegate?.legUpdate!(navigator, legProgress: legProgress)
        }
    }
    /*****************************************************************************
     * Function :   stepUpdate
     * Input    :   none
     * Output   :   stepProgress
     * Comment  :   Event call after every stepprogress Update
     *
     ****************************************************************************/
    func stepUpdate(_ navigator: NSNavigator?, stepProgress: NSStepProgress?) {
        print("NL:- StepUpdated")
        
        if JourneyManager.sharedInstance.getAlertMode() == AlertModeStatus.Off{
            
        self.navigationLocatorDelegate?.stepUpdate!(navigator, stepProgress: stepProgress)
        }
    }
    /*****************************************************************************
     * Function :   update
     * Input    :   none
     * Output   :   get the update of navigation
     * Comment  :   Event call after every upadate happen of Navigation Side
     *
     ****************************************************************************/
    func update(_ navigator: NSNavigator?, status: NSStatus?) {
        print("NL:- update called")
        if JourneyManager.sharedInstance.getAlertMode() == AlertModeStatus.Off{
            
        self.navigationLocatorDelegate?.update!(navigator, status: status)
        }
    }
    /*****************************************************************************
     * Function :   maneuver
     * Input    :   none
     * Output   :   get the coming & Upcoming Turn
     * Comment  :   Event call after every Manuever Update
     *
     ****************************************************************************/
    func maneuver(_ navigator: NSNavigator?, maneuver: NSManeuver?) {
        print("NL:- maneuver called")
        print(maneuver?.getNext()?.getType().rawValue ?? "")
        print(maneuver?.getType().rawValue  ?? "")
        if JourneyManager.sharedInstance.getAlertMode() == AlertModeStatus.Off{
            
        self.navigationLocatorDelegate?.maneuver!(navigator, maneuver: maneuver)
        }
    }
    /*****************************************************************************
     * Function :   deviation
     * Input    :   none
     * Output   :   Deviation summary
     * Comment  :   Event call after every 500m distance after deviation
     *
     ****************************************************************************/
    
    func deviation(_ navigator: NSNavigator?, deviation: NSDeviation?) {
        print("NL:- deviation called")
        if JourneyManager.sharedInstance.getAlertMode() == AlertModeStatus.Off{
            
        self.navigationLocatorDelegate?.deviation!(navigator, deviation: deviation)
        }
    }
    
    func alert(_ navigator: NSNavigator?, alert: NSNavigationAlert?) {
        self.navigationLocatorDelegate?.alert!(navigator, alert: alert)
    }
}


//MARK: Delegate calls of DeviceLocationDelegate
extension NavigationLocator: DeviceLocationDelegate {
    
    func locationUpdateCall(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        print("@NL:location Update called")
        let userLocation:CLLocation = locations[0] as CLLocation
        //Moving Position of Biker
        self.notify(location: userLocation)
    }
}
