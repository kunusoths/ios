//
//  DeviceLocationServiece.swift
//  Maximus
//
//  Created by Admin on 22/06/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import CoreLocation

@objc protocol DeviceLocationDelegate {
    
    @objc optional func locationUpdateCall(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    @objc optional func locationServiceFail(error: Error)
}


class DeviceLocationServiece:NSObject,CLLocationManagerDelegate {
    
    var locationManager:CLLocationManager!
    var deviceLocationDelegate:DeviceLocationDelegate?
    var userLocation:CLLocation = CLLocation(latitude: 0, longitude: 0)
    
    
    ///Singleton instance of Class
    private static var privateShared : DeviceLocationServiece?
    class func sharedInstance() -> DeviceLocationServiece { // change class to final to prevent override
        guard let shared = privateShared else {
            privateShared = DeviceLocationServiece()
            return privateShared!
        }
        return shared
    }
    
    ///Shared Instance Released here
    class func destroyInstance() {
        privateShared = nil
    }
    
    
    
    override init() {
        super.init()
        locationManager = CLLocationManager()
       // locationManager.distanceFilter = 3
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.activityType = .automotiveNavigation
        self.configureDeviceLocation()
    }
    
    
    func startUpdatingLocations()  {
        locationManager.startUpdatingLocation()
    }
    
    /*****************************************************************************
     * Function :   configureDeviceLocation
     * Input    :   none
     * Output   :   none
     * Comment  :   configure the initial setup for start location service
     *
     ****************************************************************************/
    func configureDeviceLocation() {
        var isEnabled: Bool = false
        locationManager.delegate = self
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
                isEnabled = false
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                
                locationManager.startUpdatingLocation()
                isEnabled = true
            }
        }
        if !isEnabled {
            locationManager.requestAlwaysAuthorization()
        }
    }
    /*****************************************************************************
     * Function :   locationManager
     * Input    :   none
     * Output   :   none
     * Comment  :   call after every location Update
     *
     ****************************************************************************/
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.userLocation = locations[0] as CLLocation
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        let locationData = ["location": locations.last!]
        let sinkManager = SinkManager.sharedInstance
        sinkManager.notifyObservers(notificationKey: sinkManager.SINK_NOTIFICATION_LOCATION_UPDATE, sender: self, data: locationData)
        
        self.deviceLocationDelegate?.locationUpdateCall!(manager, didUpdateLocations: locations)
    }
    /*****************************************************************************
     * Function :   getUserCurrentLocation
     * Input    :   none
     * Output   :   current device location
     * Comment  :   get the device current location
     *
     ****************************************************************************/
    func getUserCurrentLocation()-> CLLocation {
        return userLocation
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("Location Manager Changed authorizaation \(status.rawValue)")
        switch status {
            
        case .authorizedAlways, .authorizedWhenInUse:
            print("Location Manager Changed authorizaation allowed ")
            self.startUpdatingLocations()
            
        case .denied, .notDetermined, .restricted:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Manager Error \(error.localizedDescription)")
    }
    
    /*****************************************************************************
     * Function :   stopUpdatingLocation
     * Input    :   none
     * Output   :   none
     * Comment  :   Call stopUpdatingLocation() to stop listening for location updates, other wise this function will be called every time when user location changes.
     *
     ****************************************************************************/
    func stopUpdatingLocation(){
        locationManager.stopUpdatingLocation()
        //DeviceLocationServiece.destroyInstance()//-----Release Instance here
    }
    
}
