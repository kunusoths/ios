//
//  RidePacket+CoreDataProperties.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 08/08/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//
//

import Foundation
import CoreData


extension RidePacket {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RidePacket> {
        return NSFetchRequest<RidePacket>(entityName: "RidePacket")
    }

    @NSManaged public var rideId: Int32
    @NSManaged public var packetArray: NSSet?

}

// MARK: Generated accessors for packetArray
extension RidePacket {

    @objc(addPacketArrayObject:)
    @NSManaged public func addToPacketArray(_ value: VCMPacket)

    @objc(removePacketArrayObject:)
    @NSManaged public func removeFromPacketArray(_ value: VCMPacket)

    @objc(addPacketArray:)
    @NSManaged public func addToPacketArray(_ values: NSSet)

    @objc(removePacketArray:)
    @NSManaged public func removeFromPacketArray(_ values: NSSet)

}

extension RidePacket {
    class func updateVCMPacketRideID(oldRideID: String, rideID: String, context: NSManagedObjectContext){
        print("Updating ride id old : \(oldRideID) new: \(rideID) ")
        let fetchRq = NSFetchRequest<RidePacket>(entityName: "RidePacket")
        let ride = Int32(oldRideID)
        let fetchpredicate = NSPredicate(format: "rideId == %d", ride!)
        fetchRq.predicate = fetchpredicate
        do {
            let rideRequest = try context.fetch(fetchRq)
            if rideRequest.count > 0 {
                rideRequest.first?.rideId = Int32(rideID)!
                do {
                    try context.save()
                } catch {
                    print("========================== Update Ride id failed ===============")
                }
            }
            
        } catch {
            
        }
    }
}



