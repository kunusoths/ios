//
//  MotionDetector.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 22/02/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import Foundation
import CoreMotion

enum Motion {
    case STATIONARY
    case AUTOMOTIVE
    case WALKING
    case OTHER
}

class MotionDetector: NSObject {
    
    static let shared = MotionDetector()
    var userMotion: Motion = .OTHER
    
    override init() {
        super.init()
        self.startUpdating()
    }
    
    let activityManager = CMMotionActivityManager()
    
    private func startTrackingActivityType() {
        activityManager.startActivityUpdates(to: OperationQueue.main) {
            [weak self] (activity: CMMotionActivity?) in
            
            guard let activity = activity else { return }
            DispatchQueue.main.async {
                
                if activity.walking {
                    print( "MotionDetector: Walking")
                    self?.userMotion = .WALKING
                 } else if activity.stationary {
                    print( "MotionDetector: stationary")
                    self?.userMotion = .STATIONARY

                } else if activity.running {
                    print( "MotionDetector: running")
                    self?.userMotion = .OTHER

                } else if activity.automotive {
                    print( "MotionDetector: automotive")
                    self?.userMotion = .AUTOMOTIVE

                    
                }
            }
        }
    }
    
    private func startUpdating() {
        if CMMotionActivityManager.isActivityAvailable() {
            startTrackingActivityType()
        }
        
    }
    
}
