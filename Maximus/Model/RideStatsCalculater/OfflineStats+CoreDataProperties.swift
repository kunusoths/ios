//
//  OfflineStats+CoreDataProperties.swift
//  
//
//  Created by Abhilash Ghogale on 22/02/18.
//
//

import Foundation
import CoreData


extension OfflineStats {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<OfflineStats> {
        return NSFetchRequest<OfflineStats>(entityName: "OfflineStats")
    }

    @NSManaged public var rideId: Int16
    @NSManaged public var distanceTravelled: Double
    @NSManaged public var timeTravelled: Int16
    @NSManaged public var averageSpeed: Double
    @NSManaged public var topSpeed: Int16
    @NSManaged public var rideLocations: NSObject?
    @NSManaged public var lastPacketTime: Date?
}

extension OfflineStats {
    class func fetchOfflineStatsWithRideId(rideId: Int16, context: NSManagedContext) -> OfflineStats? {
        do {
            let offlineStatsFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "OfflineStats")
            offlineStatsFetch.predicate = NSPredicate(format: "rideId == %d", rideId)
            
            if let offlineStats = try context.fetch(offlineStatsFetch).first as? OfflineStats {
                return offlineStats
            } else {
                print("Ride Id")
            }
        } catch {
            print("packet failed")
        }
        return nil
    }
}
