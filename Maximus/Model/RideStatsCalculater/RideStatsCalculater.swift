
//
//  RideStatsCalculater.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 23/01/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData

class RideStatsCalculater: NSObject {
    
    // in meters
    private var distanceTravelled:Double = 0
    
    // in seconds
    private var travelledTime:TimeInterval = 0
    private var averageSpeed:Double = 0
    
    // in m/s
    private var topSpeed:Double = 0
    private var lastGoodLocation:CLLocation = CLLocation(latitude: 0, longitude: 0)
    private var rideLocations:[CLLocation] = []
    var currentRideId = JourneyManager.sharedInstance.currentRideId()
    
    
    var statsTimer: Timer?
    let statsTimerTimeInterval:TimeInterval = 12
    
    var sinkManager = SinkManager.sharedInstance
    
    var deviceLocationService = DeviceLocationServiece.sharedInstance()
    
    let offlineStatsContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    
    override init() {
        print("RideStatsCalculater : Init")
        
        super.init()
        sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_RIDE_STARTED, selectorName: rideStarted)
        sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_RIDE_STOPPED, selectorName:rideStopped)
        sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_RIDE_INPROGRESS, selectorName:rideInProgress)
    }
    
    deinit {
        print("RideStatsCalculater : deinit")
        
    }
    
    
    func rideStarted(notification:Notification){
        print("RideStatsCalculater : Ride Started Creating CoreData object ")
        self.currentRideId = JourneyManager.sharedInstance.currentRideId()
        self.startStatsCalulaterTimer()
    }
    
    func rideInProgress(notification:Notification) {
        print("RideStatsCalculater : Ride in progress")
        
        self.restoreLocationData()
        self.startStatsCalulaterTimer()
    }
    
    
    func rideStopped(notification:Notification) {
        print("RideStatsCalculater : Ride Finished")
        
        //self.addLastPacketTime()
        sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_RIDE_STARTED)
        sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_RIDE_STOPPED)
        sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_RIDE_INPROGRESS)
        self.invalidateTimer()
    }
    
    func startStatsCalulaterTimer() {
        if self.statsTimer == nil {
            print("RideStatsCalculater : Timer Started")
            self.statsTimer = Timer.scheduledTimer(timeInterval: statsTimerTimeInterval, target: self, selector: #selector(onUserLocationUpdate), userInfo: nil, repeats: true)
        }
    }
    
    func invalidateTimer() {
        if self.statsTimer != nil {
            print("RideStatsCalculater : Timer invalidate")
            self.statsTimer?.invalidate()
        }
    }
    
    func saveContext() {
        do {
            try offlineStatsContext.save()
        } catch {
            
        }
    }
    
    func onUserLocationUpdate() {
        print("RideStatsCalculater : onUserLocationUpdate ")
        
        let newLocation:CLLocation = DeviceLocationServiece.sharedInstance().getUserCurrentLocation()
        
       if  newLocation.horizontalAccuracy < 30 && newLocation.speed > 1.5 {
        
            if !self.lastGoodLocationNotKnown()  {
                
                self.updateDistanceTravelled(newLocation: newLocation)
                self.updateTimeTravelled(newLocation: newLocation)
                self.updateAverageSpeed()
                self.updateTopSpeed(newLocation: newLocation)
                self.updateLocations(newLocation: newLocation)
                self.chacheRideData()
                
            }
            
            self.updateLastGoodLocation(newLocation: newLocation)
        
        }
        else{
        }
        
    }
    
    //
    func updateDistanceTravelled(newLocation: CLLocation){
        
        self.distanceTravelled += newLocation.distance(from: lastGoodLocation)
    }
    
    //
    func updateTimeTravelled(newLocation: CLLocation) {
        let timeStamp = newLocation.timestamp
        let diff = timeStamp.timeIntervalSince(lastGoodLocation.timestamp)
        
        if diff < 10 * 60 {
            self.travelledTime += diff
        }
    }
    
    //
    func updateAverageSpeed() {
        self.averageSpeed = (self.distanceTravelled / self.travelledTime)   //m/s
    }
    
    //
    func updateTopSpeed(newLocation: CLLocation) {
        
        let newSpeed = newLocation.speed
        if self.topSpeed < newSpeed {
            self.topSpeed = newSpeed
        }
        
    }
    
    //
    func updateLastGoodLocation(newLocation : CLLocation){
        self.lastGoodLocation = newLocation
    }
    
    func lastGoodLocationNotKnown() -> Bool {
        return (self.lastGoodLocation.coordinate.latitude == 0 && self.lastGoodLocation.coordinate.longitude == 0)
    }
    
    func updateLocations(newLocation: CLLocation) {
        self.rideLocations.append(newLocation)
    }
    
    //
    func chacheRideData() {
        
        let distance = self.distanceTravelled // m
        let speed = self.topSpeed  // m/s
        let avgSpeed = self.averageSpeed
        self.currentRideId = JourneyManager.sharedInstance.currentRideId()
        print("RideStatsCalculater : dist = \(distance) , speed = \(speed), avgSpeed = \(self.averageSpeed)")
        AchievementDataObj.updateStatsObj(distanceTravelled: (distance), travelledTime: self.travelledTime, averageSpeed: avgSpeed, topSpeed: speed, lastGoodLocation: self.lastGoodLocation, rideLocations: self.rideLocations, rideID: String(currentRideId), context: offlineStatsContext)
    }
    
    //
    func resetLocationData(notification:Notification)
    {
    }
    
    func restoreLocationData() {
        print("RideStatsCalculater : Restoring data")
        if let offlineStats = Rides.fetchRideWithID(rideID: currentRideId.toString(), context: offlineStatsContext)?.achievement {
            
            let distance = offlineStats.distance_travelled // km to mtrs
            let speed =  offlineStats.top_speed // cm/s to m/s
            let avgSpeed = offlineStats.average_speed
            
            self.distanceTravelled = distance
            self.travelledTime = TimeInterval(offlineStats.time_duration)
            
            self.topSpeed = speed
            self.averageSpeed = avgSpeed
            
            if let rideLocationsData = offlineStats.rideLocations{
                self.rideLocations = (NSKeyedUnarchiver.unarchiveObject(with: rideLocationsData as! Data) as? [CLLocation])!
            }
            
            if let lastGoodLocation = offlineStats.lastGoodLocation{
                self.lastGoodLocation = (NSKeyedUnarchiver.unarchiveObject(with: lastGoodLocation as! Data) as? CLLocation)!
            }
        }
    }
}
