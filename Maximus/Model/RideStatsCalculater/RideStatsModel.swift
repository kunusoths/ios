//
//  RideStatsModel.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 27/02/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import Foundation
import CoreLocation

class RideStatsModel:NSObject{
    
     var distanceTravelled:Double = 0.0
     var travelledTime:TimeInterval = 0
     var averageSpeed:Double = 00
     var topSpeed:Double = 0
     var lastGoodLocation:CLLocation = CLLocation(latitude: 0, longitude: 0)
     var rideLocations:[CLLocation] = []
    
}
