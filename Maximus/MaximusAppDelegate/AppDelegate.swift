    //
    //  AppDelegate.swift
    //  Maximus
    //
    //  Created by kpit on 07/02/17.
    //  Copyright © 2017 com.kpit.maximus. All rights reserved.
    //
    
    import UIKit
    import CoreData
    import FBSDKCoreKit
    import GoogleMaps
    import GooglePlaces
    import UserNotifications
    import ObjectMapper
    import Alamofire
    import Firebase
    import FirebaseInstanceID
    import FirebaseMessaging
    import FirebaseDynamicLinks
    //import NTPKit
    import Fabric
    import Crashlytics
    import SlideMenuControllerSwift

    var APP_BOOT_TIME:TimeInterval = 0
    
    
    extension URL {
        
        public var queryParameters: [String: String]? {
            guard let components = URLComponents(url: self, resolvingAgainstBaseURL: true), let queryItems = components.queryItems else {
                return nil
            }
            
            var parameters = [String: String]()
            for item in queryItems {
                parameters[item.name] = item.value
            }
            
            return parameters
        }

    }
    @available(iOS 10.0, *)
    @UIApplicationMain
    class AppDelegate: UIResponder, UIApplicationDelegate, ReloadMyRidesDelegate {
        /// The callback to handle data message received via FCM for devices running iOS 10 or above.
        /// The callback to handle data message received via FCM for devices running iOS 10 or above.
        var window: UIWindow?
        let gcmMessageIDKey = Constants.gcmMessageIDKey
        let customURLScheme = "Maximuspro"
        let userObject = UserManagerHandler()
        var isThroughMyRides: Bool = false
        var editRideNotification = false
        var rideManager: PlanRideViewModel?
        var rideViewModel: MyRidesViewModel?
        var rideRepo: RideRepository?
        var planRideRepo: PlanRideRepository?
        
        func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
            
           DeviceLocationServiece.sharedInstance().startUpdatingLocations()
            ///****************  FireBase Configuration ***************************
            FirebaseOptions.defaultOptions()?.deepLinkURLScheme = self.customURLScheme
            FirebaseApp.configure()
            
            ///****************  Fabric Configuration ***************************
            Fabric.with([Answers.self])
            // temporary sleep use main thread
            
            //Appversion check & show alert
            self.setupAppversion()
            
            // Override point for customization after application launch.
            FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
            //self.getTimeDifferenceBetweenNTPAndDeviceTime()
            /*Force logout*/
            //First get the nsObject by defining as an optional anyObject
            let nsObject: AnyObject? = Bundle.main.infoDictionary?[StringConstant.appVersion] as AnyObject
            
            //Then just cast the object as a String, but be careful, you may want to double check for nil
            if let version = nsObject as? String, version == "2.0.3" {
                if MyDefaults.getBoolData(key: DefaultKeys.KEY_ISLOGIN) {
                    if !MyDefaults.getBoolData(key: DefaultKeys.KEY_FORCED_LOGOUT) {
                        MyDefaults.clearDefaults()
                        deleteSqliteData()
                        MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_TUTORIAL_SEEN)
                        MyDefaults.setBoolData(value: false, key: DefaultKeys.KEY_ISLOGIN)
                        MyDefaults.setBoolData(value: false, key: DefaultKeys.KEY_RELOGIN)
                      
                    } else {
                        
                    }
                }
            }
            MyDefaults.setBoolData(value: false, key: DefaultKeys.KEY_Location_ToastMessageShown)
            MyDefaults.setBoolData(value: false, key: DefaultKeys.KEY_VirtualCm_Offline_ToastMessageShown)
            MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_EMAIL_VERIFY)
            
            AppDetailsHandler.sharedInstance.createAppDetailsObj()
            AppDetailsHandler.sharedInstance.createDMDetailsObj()
            ///*******  Store ALL GOOGLE Key Credentials inside of Keychain Store *************
            KeychainService.shared[KEY_GOOGLE.KEY_NAME_MAP_STAGING] = KEY_GOOGLE.APIKEY_STAGING_MAPS
            KeychainService.shared[KEY_GOOGLE.KEY_NAME_MAP_PRODUCTION] = KEY_GOOGLE.APIKEY_PRODUCTION_MAPS
            KeychainService.shared[KEY_GOOGLE.KEY_NAME_TBT_STAGING] = KEY_GOOGLE.APIKEY_STAGING_TBT
            KeychainService.shared[KEY_GOOGLE.KEY_NAME_TBT_PRODUCTION] = KEY_GOOGLE.APIKEY_PRODUCTION_TBT
            KeychainService.shared[KEY_GOOGLE.KEY_NAME_DYNAMICLINK_STAGING] = KEY_GOOGLE.APIKEY_STAGING_DYNAMICLINKS
            KeychainService.shared[KEY_GOOGLE.KEY_NAME_DYNAMICLINK_PRODUCTION] = KEY_GOOGLE.APIKEY_PRODUCTION_DYNAMICLINKS
            
            KeychainService.shared[KEY_OAUTH.KEY_NAME_OAUTH_CLIENT_SECRET] = KEY_OAUTH.APIKEY_OAUTH_CLIENT_SECRET
            
            ///***********************************************************************
            let info = ProcessInfo.processInfo
            APP_BOOT_TIME = info.systemUptime
            

            sleep(3)
            self.configureGA()
            
            UIApplication.shared.setStatusBarHidden(false, with: .slide)
            UIApplication.shared.applicationIconBadgeNumber = 0
            ///****************  Google Maps Configuration *****************************
            GMSServices.provideAPIKey(Constants.APIKEY_MAPS!)
            self.generateAndSaveFcmToken()
            self.registerForRemoteNotifications(application: application)
            self.configureNotification()
            self.authoriseLocalNotification()
            
            // [START add_token_refresh_observer]
            // Add observer for InstanceID token refresh callback.
            NotificationCenter.default.addObserver(self, selector:
                #selector(tokenRefreshNotification), name:
                NSNotification.Name.InstanceIDTokenRefresh, object: nil)
            
            // [END add_token_refresh_observer]
            
            GMSPlacesClient.provideAPIKey(Constants.APIKEY_MAPS!)
            
            // Initialize communication protocol
            CPWrapper.sharedInstance().cp_initialize(20, andOmtu: 20)
            
            
            self.window = UIWindow(frame: UIScreen.main.bounds)
            SlideMenuOptions.panGesturesEnabled = false
            let (navigation, vc) = self.getCurrentRootView()
            let navigationBarAppearance = UINavigationBar.appearance()
            navigationBarAppearance.titleTextAttributes = [NSFontAttributeName : UIFont(name: "Corbel-Bold", size: 18)!]
            navigationBarAppearance.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
            navigationBarAppearance.backgroundColor = AppTheme.THEME_ORANGE
            navigationBarAppearance.barTintColor = AppTheme.THEME_ORANGE
            navigationBarAppearance.setBackgroundImage(UIImage(), for: .default)
            navigationBarAppearance.shadowImage = UIImage()
            navigationBarAppearance.isTranslucent = false
            navigation.interactivePopGestureRecognizer?.isEnabled = false
            
            self.window?.rootViewController = vc
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                
                let context = AlertContext()
                context.changeStateToCheckAndShowAlertState()
            }

            if MyDefaults.getData(key: DefaultKeys.KEY_REQUEST_OBJ_ID) as? String == "" {
                MyDefaults.setData(value: "1", key: DefaultKeys.KEY_REQUEST_OBJ_ID)
            }
            let _ = MaximusDM.sharedInstance
            planRideRepo = PlanRideRepository()
            rideManager = PlanRideViewModel(planRideRepoDelegate: planRideRepo!)
            rideRepo = RideRepository()

            rideViewModel = MyRidesViewModel(rideRepoDelegate: rideRepo!)

            self.window?.makeKeyAndVisible()
            return true || FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        }
        
        func deleteSqliteData() {
            var db : DBManager?
            do {
                db = try DBManager.open(path: part1DbPath)
            } catch let message {
                print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
                print(message)
            }
            db?.deleteTable(table: SQLTableType.PLANNED_ROUTE, queryString: DBQuery.QUERY_PLANNED_ROUTE_DROP)
            db?.deleteTable(table: SQLTableType.POINT_OF_INTEREST, queryString: DBQuery.QUERY_POINT_OF_INTREST_DROP)
            db?.deleteTable(table: SQLTableType.RECEIVE_ALERT, queryString: DBQuery.QUERY_RECEIVE_ALERT_DROP)
            db?.deleteTable(table: SQLTableType.SENDING_ALERT, queryString: DBQuery.QUERY_SENDING_ALERT_DROP)
        }
        
        /*
        func getTimeDifferenceBetweenNTPAndDeviceTime() {
            let server = NTPServer.default
            let date = try? server.date()
            print("Server time: \(date ?? Date())")
            let deviceDate = Date()
            if let serverDate = date {
                let offset = serverDate.offset(from: deviceDate)
                MyDefaults.setInt(value: offset, key: DefaultKeys.KEY_SERVER_OFFSET_TIME)
            } else {
                MyDefaults.setInt(value: 0, key: DefaultKeys.KEY_SERVER_OFFSET_TIME)
            }
        }*/
        
        func configureGA() {
            guard let gai = GAI.sharedInstance() else {
                assert(false, "Google Analytics not configured correctly")
                return
            }
            // Optional: automatically report uncaught exceptions.
            gai.trackUncaughtExceptions = true
            
            // Optional: set Logger to VERBOSE for debug information.
            // Remove before app release.
            gai.logger.logLevel = .verbose
        }
        
        func getCurrentRootView()->(UINavigationController,UIViewController){
            
            var vc:UIViewController?
            var navigation:UINavigationController?
            if MyDefaults.getBoolData(key: DefaultKeys.KEY_RELOGIN) {
                navigation = UINavigationController(rootViewController: LoginViewController())
                let leftViewController = SideMenuVC()
                let slideMenuController = SlideMenuController(mainViewController: navigation!, leftMenuViewController: leftViewController)
                slideMenuController.changeLeftViewWidth((self.window?.bounds.size.width)!-60)
                vc = slideMenuController
            } else if MyDefaults.getBoolData(key: DefaultKeys.KEY_ISLOGIN){
                //send Updated Token to Cloud
                if MyDefaults.getData(key: DefaultKeys.KEY_REFRESH_TOKEN) as! String == "" {
                    self.authenticateWithServerWithFBData()//------
                }
                
                    if MyDefaults.getBoolData(key: DefaultKeys.KEY_CMREGISTER){
                        RideRequestHandler.requestHandlerSharedInstance.startExecutingRequestObj()
                        if(JourneyManager.sharedInstance.isAnyRideInProgress()){
                            let vc = RideInProgressContainer()
                            _ = RideStatsCalculater()
                            JourneyManager.sharedInstance.updateNavigationEngineState()
                            let isVirtualCM = MyDefaults.getBoolData(key: DefaultKeys.IS_CM_VIRTUAL)
                            if isVirtualCM {
                                VCMPacketManager.sharedInstance.startVCMTimer()
                            }
                            
                            SinkManager.sharedInstance.notifyObservers(notificationKey: SinkManager.sharedInstance.SINK_NOTIFICATION_RIDE_INPROGRESS, sender: self)
                          
                            vc.rideDetails = JourneyManager.sharedInstance.currentRideDetails()
                            navigation = UINavigationController(rootViewController: vc)
                        }else{
                            navigation = UINavigationController(rootViewController: HomeContainerVC())
                        }
                        
                        let leftViewController = SideMenuVC()
                        let slideMenuController = SlideMenuController(mainViewController: navigation!, leftMenuViewController: leftViewController)
                        slideMenuController.changeLeftViewWidth((self.window?.bounds.size.width)!-60)
                        vc = slideMenuController
                        
                    }else{
                        navigation = UINavigationController(rootViewController: WelcomeVC())
                        vc = navigation
                    }
            }else {
                
                if MyDefaults.getBoolData(key: DefaultKeys.KEY_TUTORIAL_SEEN){
                    navigation = UINavigationController(rootViewController: SignUpViewController())
                }else{
                    navigation = UINavigationController(rootViewController: TutorialsVC())
                }
                vc = navigation
            }
            
            
            /* get the cached BLE identifier which is already connected */
            let connectedBTName:String =  MyDefaults.getData(key: DefaultKeys.KEY_BLE_CONNECTED) as! String
            if (connectedBTName != "") {
                //Re-initialiszation to ask BLE permission again
                BluetoothManager.sharedInstance().configureBLEService(name: connectedBTName)
            }
            VCMPacketManager.sharedInstance.checkVcmPacket()
            //navigation = UINavigationController(rootViewController: SettingVC())
            //vc = SettingVC()
            return (navigation!, vc!)
        }
        
        // MARK:Register remote notification
        
        func generateAndSaveFcmToken()
        {
            if let token = InstanceID.instanceID().token()
            {
                MyDefaults.setData(value: token, key: DefaultKeys.FCM_TOKEN)
                print("Remote Notification Token\(token)")
            }
            else{
                print("Remote Notification Token empty")
            }
        }
        
        func registerForRemoteNotifications(application:UIApplication)
        {
            if #available(iOS 10.0, *) {
                // For iOS 10 display notification (sent via APNS)
                UNUserNotificationCenter.current().delegate = self
                
                let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                UNUserNotificationCenter.current().requestAuthorization(
                    options: authOptions,
                    completionHandler: {_, _ in })
                
                // For iOS 10 data message (sent via FCM)
                Messaging.messaging().delegate = self
                
            } else {
                let settings: UIUserNotificationSettings =
                    UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                application.registerUserNotificationSettings(settings)
            }
            application.registerForRemoteNotifications()
            
        }
        
        func configureNotification()
        {
            
            let actionIgnore = UNNotificationAction(identifier:NotificationConstant.actionIgnore, title: NotificationConstant.ignoreTitle, options: [])
            let actionCheck  = UNNotificationAction(identifier: NotificationConstant.actionCheck, title: NotificationConstant.checkTitle, options: [.foreground])
            
            let bikeTheftCategory = UNNotificationCategory(identifier: NotificationCategoyConstant.category_bikeTheft, actions: [actionIgnore, actionCheck], intentIdentifiers: [], options: [])
            
            let batteryVoltageCategory = UNNotificationCategory(identifier: NotificationCategoyConstant.category_batteryVoltage, actions: [actionIgnore, actionCheck], intentIdentifiers: [], options: [])
            
            let batteryVoltage_One_Category = UNNotificationCategory(identifier: NotificationCategoyConstant.category_batteryVoltage_One, actions: [actionIgnore, actionCheck], intentIdentifiers: [], options: [])
            
            let batteryVoltage_Two_Category = UNNotificationCategory(identifier: NotificationCategoyConstant.category_batteryVoltage_Two, actions: [actionIgnore, actionCheck], intentIdentifiers: [], options: [])
            
            let batteryVoltage_Three_Category = UNNotificationCategory(identifier: NotificationCategoyConstant.category_batteryVoltage_Three, actions: [actionIgnore, actionCheck], intentIdentifiers: [], options: [])
            
            let otpVerificationCategory = UNNotificationCategory(identifier: NotificationCategoyConstant.categoryotp, actions: [actionIgnore, actionCheck], intentIdentifiers: [], options: [])
            
            let contactVerficationCategory = UNNotificationCategory(identifier: NotificationCategoyConstant.categroy_emergencyContacts, actions: [actionIgnore, actionCheck], intentIdentifiers: [], options: [])
            
            let alertPunctureCategory = UNNotificationCategory(identifier: NotificationCategoyConstant.category_alert_puncture, actions: [], intentIdentifiers: [], options: [])
            
            let alertNoFuelCategory = UNNotificationCategory(identifier: NotificationCategoyConstant.category_alert_no_fuel, actions: [], intentIdentifiers: [], options: [])
            
            let alertAccidentCategory = UNNotificationCategory(identifier: NotificationCategoyConstant.category_alert_accident, actions: [], intentIdentifiers: [], options: [])
            
            let alertSevereConditionCategory = UNNotificationCategory(identifier: NotificationCategoyConstant.category_severe_condition, actions: [], intentIdentifiers: [], options: [])
            
            
            let alertResponseToOriginatorCategory = UNNotificationCategory(identifier: NotificationCategoyConstant.category_alert_response_to_origanator, actions: [], intentIdentifiers: [], options: [])
            
            let alertCloseCategory = UNNotificationCategory(identifier: NotificationCategoyConstant.category_alert_close, actions: [], intentIdentifiers: [], options: [])
            
            let alertDeliveredAckCategory = UNNotificationCategory(identifier: NotificationCategoyConstant.category_alert_delivered_to_receiver, actions: [], intentIdentifiers: [], options: [])
            
            let alertCluster = UNNotificationCategory(identifier: NotificationCategoyConstant.category_alert_ride_cluster_formation, actions: [], intentIdentifiers: [], options: [])
            
            let editRide = UNNotificationCategory(identifier: NotificationCategoyConstant.ride_update_message, actions: [], intentIdentifiers: [], options: [])
            
            UNUserNotificationCenter.current().setNotificationCategories([bikeTheftCategory, batteryVoltageCategory,otpVerificationCategory,contactVerficationCategory, batteryVoltage_One_Category, batteryVoltage_Two_Category, batteryVoltage_Three_Category, alertPunctureCategory, alertNoFuelCategory, alertAccidentCategory, alertSevereConditionCategory, alertResponseToOriginatorCategory, alertCloseCategory, alertDeliveredAckCategory, alertCluster,editRide])
            
        }
        
        // MARK:Receive and handle messages
        // [START receive_message]
        func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
            // If you are receiving a notification message while your app is in the background,
            // this callback will not be fired till the user taps on the notification launching the application.
            // todo Handle data of notification
            
            // Print message ID.
            if let messageID = userInfo[gcmMessageIDKey] {
                print("Message ID: \(messageID)")
            }
            
            // Print full message.
            //        print("payload \(userInfo)")
        }
        
        func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                         fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
            // If you are receiving a notification message while your app is in the background,
            // this callback will not be fired till the user taps on the notification launching the application.
            // todo Handle data of notification
            
            
            if (application.applicationState != UIApplicationState.active){
                var jsonString:String?
                
                print(userInfo)
                // Print message ID.
                if let messageID = userInfo[gcmMessageIDKey] {
                    print("Message ID: \(messageID)")
                }
                
                
                if let theJSONData = try? JSONSerialization.data(
                    withJSONObject: userInfo,
                    options: []) {
                    jsonString = String(data: theJSONData,
                                        encoding: .ascii)
                    print("JSON string = \(jsonString!)")
                }
                
                if let alertType:String = userInfo[AlertConstant.PARAM_TYPE] as! String?{
                    
                    if alertType == NotificationCategoyConstant.category_alert_puncture
                        || alertType == NotificationCategoyConstant.category_alert_no_fuel
                        || alertType == NotificationCategoyConstant.category_alert_accident
                        || alertType == NotificationCategoyConstant.category_severe_condition
                        || alertType == NotificationCategoyConstant.category_alert_response_to_origanator
                        || alertType == NotificationCategoyConstant.category_alert_close
                        || alertType == NotificationCategoyConstant.category_alert_delivered_to_receiver
                        || alertType == NotificationCategoyConstant.category_alert_ride_cluster_formation
                        || alertType == NotificationCategoyConstant.ride_update_message
                    {
                        if let jsonStr = jsonString {
                            handleAlertNotification(notificationType: alertType, jsonString: jsonStr)
                        }
                    }
                }
            }
            completionHandler(UIBackgroundFetchResult.newData)
        }
        // [END receive_message]
        
        // MARK:[START refresh_token]
        
        func tokenRefreshNotification(_ notification: Notification) {
            if let data = notification.userInfo as? [String: Any] {
                let accessToken = data[StringConstant.accessToken]
                MyDefaults.setData(value: accessToken as! String, key: DefaultKeys.KEY_TOKEN)
            }
            if let refreshedToken = InstanceID.instanceID().token() {
                MyDefaults.setData(value: refreshedToken, key: DefaultKeys.FCM_TOKEN)
                let userObject = UserManagerHandler()
                userObject.postFcmToken(parameter:["device_os":"ios", "token":refreshedToken])
                print("Refreshed InstanceID token: \(refreshedToken)")
            }
            
            // Connect to FCM since connection may have failed when attempted before having a token.
            connectToFcm()
        }
        
        // [END refresh_token]
        
        // MARK:Connect to fcm
        func connectToFcm() {
            // Won't connect since there is no token
            
            guard InstanceID.instanceID().token() != nil else {
                return;
            }
            
            // Disconnect previous FCM connection if it exists.
            Messaging.messaging().disconnect()
            
            Messaging.messaging().connect { (error) in
                if error != nil {
                    print("Unable to connect with FCM. \(error)")
                } else {
                    print("Connected to FCM.")
                }
            }
        }
        
        // MARK:Unable to register for notification
        func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
            print("Unable to register for remote notifications: \(error.localizedDescription)")
        }
        
        // MARK:swizzling is disabled
        
        // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
        // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
        // the InstanceID token.
        func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
            print("APNs token retrieved: \(deviceToken)")
            
            // With swizzling disabled you must set the APNs token here.
            //            FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
            //            FIRInstanceID.instanceID().setAPNSToken(deviceToken, type:FIRInstanceIDAPNSTokenType.unknown)
            //            FIRInstanceID.instanceID().setAPNSToken(deviceToken, type:FIRInstanceIDAPNSTokenType.prod)
        }
        
        
        func onNotificationClickNavigate(rootVc:UIViewController)
        {
            let navigationController = UINavigationController(rootViewController: rootVc)
            let mainViewController = navigationController
            let leftViewController = SideMenuVC()
            let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
            slideMenuController.changeLeftViewWidth((self.window?.bounds.size.width)!-60)
            self.window?.rootViewController = slideMenuController
        }
        
        // MARK:LOCAL NOTIFICATION
        
        func authoriseLocalNotification()
        {
            UNUserNotificationCenter.current().getNotificationSettings { (notificationSettings) in
                switch notificationSettings.authorizationStatus {
                case .notDetermined:
                    self.requestAuthorization(completionHandler: { (success) in
                        guard success else { return }
                        
                        self.scheduleEmergencyContactNotification()
                    })
                case .authorized:
                    self.scheduleEmergencyContactNotification()
                case .denied:
                    print("Application Not Allowed to Display emergency alerts")

                case .provisional:
                    break
                }
            }
            
        }
        
        private func requestAuthorization(completionHandler: @escaping (_ success: Bool) -> ()) {
            // Request Authorization
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
                if let error = error {
                    print("Request Authorization Failed (\(error), \(error.localizedDescription))")
                }
                
                completionHandler(success)
            }
        }
        
        func scheduleOtpNotification()
        {
            let notificationContent = UNMutableNotificationContent()
            notificationContent.title = NotificationConstant.Title_Maximus
            notificationContent.subtitle = ""
            notificationContent.body = NotificationConstant.otp_Notification_body
            notificationContent.categoryIdentifier = NotificationCategoyConstant.categoryotp
            notificationContent.userInfo = ["type":NotificationCategoyConstant.categoryotp]
            
            // Add Trigger
            let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 60, repeats: true)
            
            // Create Notification Request
            let notificationRequest = UNNotificationRequest(identifier: NotificationIdentifier.otpIdentifier, content: notificationContent, trigger: notificationTrigger)
            
            // Add Request to User Notification Center
            UNUserNotificationCenter.current().add(notificationRequest) { (error) in
                if let error = error {
                    print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
                }
            }
        }
        
        func scheduleEmergencyContactNotification()
        {
            let isEmergencyContactAdded = MyDefaults.getBoolData(key: DefaultKeys.KEY_EMERGENCYCONTACTADDED)
            if !isEmergencyContactAdded
            {
                let notificationContent = UNMutableNotificationContent()
                notificationContent.title = NotificationConstant.Title_Maximus
                notificationContent.subtitle = ""
                notificationContent.body = NotificationConstant.emergency_Notificatio_body
                notificationContent.sound = UNNotificationSound.default()
                notificationContent.categoryIdentifier = NotificationCategoyConstant.categroy_emergencyContacts
                notificationContent.userInfo = ["type":NotificationCategoyConstant.categroy_emergencyContacts]
                
                // Add Trigger repeate everysunday at 10 am
                let date = Date()
                let calendar = Calendar.current
                var components = DateComponents()
                components.hour = 10
                components.minute = 0
                components.weekday = 1 // sunday = 1 ... saturday = 7
                let components1 = calendar.dateComponents([.weekday,.hour,.minute], from: date)
                
                let notificationTrigger = UNCalendarNotificationTrigger(dateMatching: components1, repeats: true)
                let notificationRequest = UNNotificationRequest.init(identifier: NotificationIdentifier.contactIdentifier, content: notificationContent, trigger: notificationTrigger)
                
                let center = UNUserNotificationCenter.current()
                center.add(notificationRequest)
            }
        }
        
        func cancelNotification(identifierString:String)
        {
            UNUserNotificationCenter.current().getPendingNotificationRequests { (notificationRequests) in
                var identifiers: [String] = []
                for notification:UNNotificationRequest in notificationRequests {
                    if notification.identifier == NotificationIdentifier.otpIdentifier {
                        identifiers.append(notification.identifier)
                    }
                    else if notification.identifier == NotificationIdentifier.contactIdentifier{
                        identifiers.append(notification.identifier)
                    }
                }
                UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: identifiers)
            }
        }
        
        // MARK:UIApplication Delegates
        
        func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
            
            let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
            
            return handled
        }
        
      
        
        func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
            let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url)
            if let dynamicLink = dynamicLink {
                // Handle the deep link. For example, show the deep-linked content or
                // apply a promotional offer to the user's account.
                // [START_EXCLUDE]
                // In this sample, we just open an alert.
                
                let message = generateDynamicLinkMessage(dynamicLink)
                if #available(iOS 8.0, *) {
                    showDeepLinkAlertView(withMessage: message)
                } else {
                    // Fallback on earlier versions
                }
                // [END_EXCLUDE]
                return true
            }
            
            // [START_EXCLUDE silent]
            // Show the deep link that the app was called with.
            if #available(iOS 8.0, *) {
                showDeepLinkAlertView(withMessage: "openURL:\n\(url)")
            } else {
                // Fallback on earlier versions
            }
            // [END_EXCLUDE]
            return false
        }
        // [END openurl]
        
        
        // [START continueuseractivity]
        //        @available(iOS 8.0, *)
        func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
            
            let dynamicLinks = DynamicLinks.dynamicLinks()
            
            let handled = dynamicLinks.handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
                // [START_EXCLUDE]
                //                print("url ",dynamiclink?.url! as Any)
                
                if (error != nil) {
                    
                    return
                }
                let message = self.generateDynamicLinkMessage(dynamiclink!)
              
                
            
                if let params = dynamiclink?.url?.queryParameters{
                    if params.count > 0{
                        let rideId = params[DynamicLinkShareURL.DynamicRideID.rawValue]
                        MyDefaults.setInt (value:  (rideId?.toInt())!, key: DefaultKeys.KEY_RIDEID)
                        let rideManager = RideManager()
                        rideManager.rideManagerDelegate = self
                        rideManager.getRide(rideID: (rideId?.toInt())!)
                    }
                }
                
                
                
                MyDefaults.setData(value: message, key: DefaultKeys.KEY_DEEPLINK)
                //let  deepLink:[String] = message.removingPercentEncoding!.components(separatedBy: "=")
                /*if deepLink.count >= 4 {
                    let rideIds:[String] = (deepLink.get(at: deepLink.count-3)!).removingPercentEncoding!.components(separatedBy: "&")
                    
                    if rideIds.count > 0 {
                        if let rideId = rideIds[0].toInt(){
                            MyDefaults.setInt (value:  rideId, key: DefaultKeys.KEY_RIDEID)
                            let rideManager = RideManager()
                            rideManager.rideManagerDelegate = self
                            rideManager.getRide(rideID: rideId)
                        }
                    }
                }*/
                
                // [END_EXCLUDE]
            }
            
            // [START_EXCLUDE silent]
            if !handled {
                // Show the deep link URL from userActivity.
                let message = "continueUserActivity webPageURL:\n\(userActivity.webpageURL?.absoluteString ?? "")"
                showDeepLinkAlertView(withMessage: message)
            }
            // [END_EXCLUDE]
            return handled
        }
        // [END continueuseractivity]
        
        func generateDynamicLinkMessage(_ dynamicLink: DynamicLink) -> String {
            
            var message = ""
            if let link = dynamicLink.url?.absoluteString {
                message = link
            }
            return message
        }
        
        //        @available(iOS 8.0, *)
        func showDeepLinkAlertView(withMessage message: String) {
            let okAction = UIAlertAction.init(title: "OK", style: .default) { (action) -> Void in
                print("OK")
            }
            
            let alertController = UIAlertController.init(title: "Deep-link Data", message: message, preferredStyle: .alert)
            alertController.addAction(okAction)
            self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
        
        func applicationWillResignActive(_ application: UIApplication) {
            // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
            // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
            application.isIdleTimerDisabled = true
        }
        
        func applicationDidEnterBackground(_ application: UIApplication) {
            // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
            // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        }
        
        func applicationWillEnterForeground(_ application: UIApplication) {
            // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: StringConstant.LOCATION_SERVICE_ENABLED), object: nil)
            DeviceLocationServiece.sharedInstance().configureDeviceLocation()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateScreen") , object: nil, userInfo: nil)
        }
        
        func applicationDidBecomeActive(_ application: UIApplication) {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
            application.isIdleTimerDisabled = false
        }
        
        func applicationWillTerminate(_ application: UIApplication) {
            // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
            // Saves changes in the application's managed object context before the application terminates.
            Thread.sleep(forTimeInterval: 2.0)
            BluetoothManager.sharedInstance().disconnectPeripheral()
            CPWrapper.sharedInstance().releaseCommunicationProtocol()
            self.saveContext()
        }
        
        // MARK: - Core Data stack
        
        lazy var persistentContainer: NSPersistentContainer = {
            /*
             The persistent container for the application. This implementation
             creates and returns a container, having loaded the store for the
             application to it. This property is optional since there are legitimate
             error conditions that could cause the creation of the store to fail.
             */
            let container = NSPersistentContainer(name: "Maximus")
            let nsObject: AnyObject? = Bundle.main.infoDictionary?[StringConstant.appVersion] as AnyObject
            
            //Then just cast the object as a String, but be careful, you may want to double check for nil
          
//            let description = NSPersistentStoreDescription()
//
//            description.shouldInferMappingModelAutomatically = true
//            description.shouldMigrateStoreAutomatically = true
//            container.persistentStoreDescriptions = [description]
//            container.viewContext.automaticallyMergesChangesFromParent = true
            container.loadPersistentStores(completionHandler: { (storeDescription, error) in
                if let error = error as NSError? {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    print("core data migration persistent container error:\(error.userInfo)")
                 //   fatalError("Unresolved error \(error), \(error.userInfo)")
                }
            })
            return container
        }()
        
        // MARK: - Core Data Saving support
        
        func saveContext () {
            let context = persistentContainer.viewContext
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    print("Core data migration save context error:\(nserror.userInfo)")
//                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        }
    }
    
   
    
    extension AppDelegate:UNUserNotificationCenterDelegate
    {
        // This method will be called when app received push notifications in foreground
        func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
        {
            
            var jsonString:String?
            
            let userInfo = notification.request.content.userInfo
            // Print message ID.
            if let messageID = userInfo[gcmMessageIDKey] {
                print("Message ID: \(messageID)")
            }
            
            if let theJSONData = try? JSONSerialization.data(
                withJSONObject: userInfo,
                options: []) {
                jsonString = String(data: theJSONData,
                                    encoding: .ascii)
                print("JSON string = \(jsonString!)")
            }
            
            if notification.request.content.categoryIdentifier == NotificationCategoyConstant.category_alert_puncture
                || notification.request.content.categoryIdentifier == NotificationCategoyConstant.category_alert_no_fuel
                || notification.request.content.categoryIdentifier == NotificationCategoyConstant.category_alert_accident
                || notification.request.content.categoryIdentifier == NotificationCategoyConstant.category_severe_condition
                || notification.request.content.categoryIdentifier == NotificationCategoyConstant.category_alert_response_to_origanator
                || notification.request.content.categoryIdentifier == NotificationCategoyConstant.category_alert_close
                || notification.request.content.categoryIdentifier == NotificationCategoyConstant.category_alert_delivered_to_receiver
                || notification.request.content.categoryIdentifier == NotificationCategoyConstant.category_alert_ride_cluster_formation
                || notification.request.content.categoryIdentifier == NotificationCategoyConstant.ride_update_message
                
            {
                if let jsonStr = jsonString {
                    handleAlertNotification(notificationType: notification.request.content.categoryIdentifier, jsonString: jsonStr)
                }
            }
            completionHandler(
                [UNNotificationPresentationOptions.alert,
                 UNNotificationPresentationOptions.sound,
                 UNNotificationPresentationOptions.badge,
                 ])
        }
        
        
        func handleAlertNotification(notificationType: String, jsonString: String){
            
            print("ALERTS LOG: \(jsonString)")
            
            if notificationType == NotificationCategoyConstant.category_alert_puncture
                || notificationType == NotificationCategoyConstant.category_alert_no_fuel
                || notificationType == NotificationCategoyConstant.category_alert_accident
                || notificationType == NotificationCategoyConstant.category_severe_condition
            {
                if let receivedAlert = Mapper<AlertNotificationResponseModel>().map(JSONString: jsonString){
                    
                    let alertContext:AlertContext = AlertContext()
                    if JourneyManager.sharedInstance.getAlertMode() == AlertModeStatus.Off{
                        alertContext.changeStateToSendDeliveryACK(alertUuid: receivedAlert.uuid!)
                    }
                    alertContext.changeStateToAlertReceived(alertDetail: receivedAlert)
                    /*alertContext.changeStateToSendDeliveryACK(alertUuid: receivedAlert.uuid!)
                    alertContext.changeStateToAlertReceived(alertDetail: receivedAlert)*/
                }
            }
            else if notificationType == NotificationCategoyConstant.category_alert_response_to_origanator
            {
                if let receivedAlert = Mapper<AlertNotificationResponseModel>().map(JSONString: jsonString){
                    let alertContext:AlertContext = AlertContext()
                    alertContext.changeStateToAlertHelpOnWay(alertDetail: receivedAlert)
                }
            }
            else if notificationType == NotificationCategoyConstant.category_alert_close
            {
                if let receivedAlert = Mapper<AlertNotificationResponseModel>().map(JSONString: jsonString){
                    let alertContext:AlertContext = AlertContext()
                    alertContext.changeStateToAlertStateReceivedClosed(alertId: receivedAlert.alert_id!)
                }
            }else if notificationType == NotificationCategoyConstant.category_alert_delivered_to_receiver{
                
                if let receivedAlert = Mapper<AlertNotificationResponseModel>().map(JSONString: jsonString){
                    let alertContext:AlertContext = AlertContext()
                    
                     alertContext.changeStateToAlertDelivered(alertDetail: receivedAlert)
                    
                    
                }
            }else if notificationType == NotificationCategoyConstant.category_alert_ride_cluster_formation{
                if let receivedAlert = Mapper<AlertClusterResponseModel>().map(JSONString: jsonString){
                    let alertContext:AlertContext = AlertContext()
                    alertContext.changeStateToClusterAlert(alertDetail:receivedAlert)
                }
            } else if notificationType == NotificationCategoyConstant.ride_update_message {
                if let updateRide = Mapper<RideUpdateResponseModel>().map(JSONString: jsonString) {
                    editRideNotification = true
                    let rideID = Int(updateRide.ride_id!)
                    MyDefaults.setInt(value: Int(updateRide.ride_id ?? "")!, key: DefaultKeys.KEY_POI_RIDEID)
                    rideRepo?.repoDelegate = self
//                    rideRepo?.cachePOI(rideID: Int(updateRide.ride_id ?? "")!, request: "POICaching")
//                    rideManager?.getRideRouteById(rideID: rideID!, bikerID: MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID), type: "GetRideRouteById", successCallback: { (data) in
//                        self.saveRideSuccessCallback(data: data)
//                    }) { (error) in
//                        self.failure(data: error)
//                    }
//=======
                    rideRepo?.cachePOI(rideID: Int(updateRide.ride_id ?? "")!, request: "POICaching", successCallback: {responseData in
                        
                    }, failureCallback:{ failureData in
                        
                    })
//                    rideManager?.saveRideDelegate = self
                    rideManager?.getRideRouteById(rideID: rideID!, bikerID: MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID), type: "GetRideRouteById", successCallback: {responseData in
                        
                    }, failureCallback: {failureData in
                        
                    })
                }
            }
            
        }
        
        func saveRideSuccessCallback(data: Any) {
            if  let dict = data as? [String: Any], let request = dict["RequestType"] as? String, request == "GetRideRouteById"{
                let rideCard = dict["data"] as? SingleRouteDetails
                rideViewModel?.reloadDataDelegate = self
                rideViewModel?.getRideMembers(rideID: (rideCard?.id)!, type: "GetRideMembers", successCallback: {responseData in
                    
                }, failureCallback:{ failureData in
                    
                })
            }
        }
        
        func saveRideSuccess(data: Any) {
//            if  let dict = data as? [String: Any], let request = dict["RequestType"] as? String, request == "GetRideRouteById"{
//                let rideCard = dict["data"] as? SingleRouteDetails
//                rideViewModel?.reloadDataDelegate = self
//                rideViewModel?.getRideMembers(rideID: (rideCard?.id)!, type: "GetRideMembers")
//            }
        }
        
        func failure(data: [String : Any]) {
            
        }
        
        func reloadData(data: Any) {
            
        }
        
        func userNotificationCenter(_ center: UNUserNotificationCenter,
                                    didReceive response: UNNotificationResponse,
                                    withCompletionHandler completionHandler: @escaping () -> Void) {
            
            var jsonString:String?
            
            let userInfo = response.notification.request.content.userInfo
            // Print message ID.
            if let messageID = userInfo[gcmMessageIDKey]{
                print("Message ID: \(messageID)")
            }
            
            if let theJSONData = try? JSONSerialization.data(
                withJSONObject: userInfo,
                options: []) {
                jsonString = String(data: theJSONData,
                                    encoding: .ascii)
                print("JSON string = \(jsonString!)")
            }
            
            if response.actionIdentifier == UNNotificationDefaultActionIdentifier
            {
                print("default action handled")
                if response.notification.request.content.categoryIdentifier == NotificationCategoyConstant.category_bikeTheft
                {
                    if  userInfo["type"] as! String == NotificationCategoyConstant.category_bikeTheft
                    {
                        let theftVcObj = TheftAlertVC()
                        theftVcObj.message = userInfo["message"] as! String?
                        theftVcObj.urlString = userInfo["tracking_url"] as! String?
                        self.onNotificationClickNavigate(rootVc:theftVcObj)
                    }
                }
                else{   //handled default notification alerts for all dm alerts here.
                    if let jsonStr = jsonString {
                        handleAlertNotification(notificationType: response.notification.request.content.categoryIdentifier, jsonString: jsonStr)
                    }
                }
            }
            else if response.notification.request.content.categoryIdentifier == NotificationCategoyConstant.categoryotp
            {
                if response.actionIdentifier == NotificationConstant.actionIgnore{
                    print("ignoreOtp action tapped")
                }
                else if response.actionIdentifier == NotificationConstant.actionCheck
                {
                    print("checkOtpAction action tapped")
                    self.onNotificationClickNavigate(rootVc:EnterOTPVC())
                }
            }
            else if response.notification.request.content.categoryIdentifier == NotificationCategoyConstant.categroy_emergencyContacts
            {
                if response.actionIdentifier == NotificationConstant.actionIgnore{
                    print("ignoreOtp action tapped")
                }
                else if response.actionIdentifier == NotificationConstant.actionCheck
                {
                    print("checkOtpAction action tapped")
                    self.onNotificationClickNavigate(rootVc:ProfileVC())
                }
            }
            else if response.notification.request.content.categoryIdentifier == NotificationCategoyConstant.category_bikeTheft //TODO:DO NOT DELETE THE BELOW CODE
            {
                if response.actionIdentifier == NotificationConstant.actionCheck || response.actionIdentifier == UNNotificationDefaultActionIdentifier
                {
                    if  userInfo["type"] as! String == NotificationCategoyConstant.category_bikeTheft
                    {
                        let theftVcObj = TheftAlertVC()
                        theftVcObj.message = userInfo["message"] as! String?
                        theftVcObj.urlString = userInfo["tracking_url"] as! String?
                        
                        self.onNotificationClickNavigate(rootVc:theftVcObj)
                    }
                }
                else if response.actionIdentifier == NotificationConstant.actionIgnore
                {
                    print("bike theft ignore action tapped")
                }
            }
            else if response.notification.request.content.categoryIdentifier == NotificationCategoyConstant.category_batteryVoltage
            {
                if response.actionIdentifier == NotificationConstant.actionCheck
                {
                    if  userInfo["type"] as! String == NotificationCategoyConstant.category_batteryVoltage
                    {
                        self.onNotificationClickNavigate(rootVc:BatteryIgnitionOnVC())
                    }
                }
                else if response.actionIdentifier == NotificationConstant.actionIgnore{
                    print("battery voltage ignore action tapped")
                }
            }
            else if response.notification.request.content.categoryIdentifier == NotificationCategoyConstant.category_batteryVoltage_One || response.notification.request.content.categoryIdentifier == NotificationCategoyConstant.category_batteryVoltage_Two ||
                response.notification.request.content.categoryIdentifier == NotificationCategoyConstant.category_batteryVoltage_Three
            {
                if response.actionIdentifier == NotificationConstant.actionCheck
                {
                    if  userInfo["type"] as! String == NotificationCategoyConstant.category_batteryVoltage_One
                    {
                        self.onNotificationClickNavigate(rootVc:BatteryIgnitionOnVC())
                    }
                    else if userInfo["type"] as! String == NotificationCategoyConstant.category_batteryVoltage_Two{
                        
                        self.onNotificationClickNavigate(rootVc:BatteryIgnitionOnVC())
                    }
                    else if userInfo["type"] as! String == NotificationCategoyConstant.category_batteryVoltage_Three{
                        
                        self.onNotificationClickNavigate(rootVc:BatteryIgnitionOnVC())
                    }
                }
                else if response.actionIdentifier == NotificationConstant.actionIgnore{
                    print("battery voltage one ignore action tapped")
                }
            }
            
            
            completionHandler()
        }
        
    }
    
    extension AppDelegate:MessagingDelegate
    {
        #if(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        public func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage){
        print(remoteMessage.appData)
        }
        #endif
        
        public func application(received remoteMessage: MessagingRemoteMessage) {
            print(remoteMessage.appData)
            
        }
    }
    
    // MARK: - RideMAnager API Delegate Methods
    extension AppDelegate:RideManagerDelegate, RidesRepoCallback{
        
        func onGetRideDetail(data:[String:Any]) {
            if let rideData:RideCreateResponse = data["data"] as? RideCreateResponse {
                
                if editRideNotification {
                    let vc =  RideInProgressContainer()
                    //vc.rideDetails = rideData //Abhilash
                    if(rideData.planned)!{
                        
                        MyDefaults.setInt(value: rideData.id!, key: DefaultKeys.KEY_POI_RIDEID)
                        rideRepo?.repoDelegate = self
                        rideRepo?.cachePOI(rideID: rideData.id ?? 0, request: "POICaching", successCallback: {responseData in
                            
                        }, failureCallback:{ failureData in
                            
                        })
                    }
                    Constants.appDelegate?.isThroughMyRides = false
                    self.onNotificationClickNavigate(rootVc: vc)
                } else {
                    if var topController = UIApplication.shared.keyWindow?.rootViewController {
                        while let presentedViewController = topController.presentedViewController {
                            topController = presentedViewController
                        }
                        topController.view.isUserInteractionEnabled = false
                        // topController should now be your topmost view controller
                    }
                    
                    let vcPop = PopOver.instanceFromNib()
                    vcPop.bounds = (self.window?.bounds)!
                    vcPop.objRideCreate = rideData
                    vcPop.center = (self.window?.rootView().center)!
                    self.window?.rootView().addSubview(vcPop)
                }
            }
        }
        
        func repoSuccessCallback(data: Any) {
            
        }
        
        func leaveRideSuccess() {
            
        }
        func repoFailureCallback(error: [String : Any]) {
            let errModel:ErrorModel = error["data"] as! ErrorModel
            print(errModel.message ?? " ")
            self.window?.rootView().makeToast(errModel.description!)

        }
}
    
    // MARK: - Authentication API Delegate Methods
    extension AppDelegate: UserManagerDelegate {
        //Please Redesign this Token Expire Scenario
        func authenticateWithServerWithFBData(){
            userObject.userManagerDelegate = self

            if MyDefaults.getData(key: DefaultKeys.KEY_FBACCESSTOKEN) as! String == "" {
                let dic = [StringConstant.rememberMe          :true,
                           StringConstant.username    :MyDefaults.getData(key: DefaultKeys.KEY_BIKER_EMAIL),
                           StringConstant.password :KeychainService.shared["Password"]] as [String : Any]
                
                userObject.aunthenticateWithCloud(loginMethod:"username_password",objModel:dic)
            } else {
                let obFBdata = SocialNetworkModel()
                obFBdata.socialAccesToken = MyDefaults.getData(key: DefaultKeys.KEY_FBACCESSTOKEN) as! String
                obFBdata.socialID = MyDefaults.getData(key: DefaultKeys.KEY_FB_ID) as! String
                obFBdata.email = MyDefaults.getData(key: DefaultKeys.KEY_FB_EMAIL) as! String
                obFBdata.type = "facebook"
                
                let dic = [StringConstant.accessToken :obFBdata.socialAccesToken,
                           StringConstant.ID          :obFBdata.socialID,
                           StringConstant.username    :obFBdata.email]
                
                userObject.aunthenticateWithCloud(loginMethod:obFBdata.type,objModel:dic)
            }
        }
        
        
        //MARK: ************  Cloud Request Responce Delegate ***********
        func onSocialLoginAuthenticationSuccess(data:[String:Any]) {
            
            if let user:AuthenticationModel = data["data"] as? AuthenticationModel{
                MyDefaults.setData( value: user.id_token! , key: DefaultKeys.KEY_TOKEN)
                MyDefaults.setInt ( value: user.id!       , key: DefaultKeys.KEY_BIKERID)
            }
        }
    }
    
    extension AppDelegate: AppVersionDelegate
    {
        func setupAppversion() {
            let appVersion = AppVesrionManager.shared
            
            // Optional
            appVersion.delegate = self
            
            // Optional
            appVersion.debugEnabled = true
            
            // Optional - Change the name of your app. Useful if you have a long app name and want to display a shortened version in the update dialog (e.g., the UIAlertController).
            //        appVersion.appName = "Test App Name"
            
            // Optional - Change the various UIAlertController and UIAlertAction messaging. One or more values can be changes. If only a subset of values are changed, the defaults with which appVersion comes with will be used.
            //        appVersion.alertMessaging = AppVersionAlertMessaging(updateTitle: "New Fancy Title",
            //                                                   updateMessage: "New message goes here!",
            //                                                   updateButtonMessage: "Update Now, Plz!?",
            //                                                   nextTimeButtonMessage: "OK, next time it is!",
            //                                                   skipVersionButtonMessage: "Please don't push skip, please don't!")
            
            // Optional - Defaults to .Option
            //        appVersion.alertType = .option // or .force, .skip, .none
            
            // Optional - Can set differentiated Alerts for Major, Minor, Patch, and Revision Updates (Must be called AFTER appVersion.alertType, if you are using appVersion.alertType)
            appVersion.majorUpdateAlertType = .skip
            appVersion.minorUpdateAlertType = .skip
            appVersion.patchUpdateAlertType = .skip
            appVersion.revisionUpdateAlertType = .skip
            
            // Optional - Sets all messages to appear in Russian. appVersion supports many other languages, not just English and Russian.
            //        appVersion.forceLanguageLocalization = .russian
            
            // Optional - Set this variable if your app is not available in the U.S. App Store.
            appVersion.countryCode = "IN"
            
            // Optional - Set this variable if you would only like to show an alert if your app has been available on the store for a few days.
            // This default value is set to 1
            // To show the update immediately after Apple has updated their JSON, set this value to 0.
            //    appVersion.showAlertAfterCurrentVersionHasBeenReleasedForDays = 3
            
            // Optional (Only do this if you don't call checkVersion in didBecomeActive)
            appVersion.checkVersion(checkType: .immediately)
        }
        

        
        func appversionDidShowUpdateDialog(alertType: AppVesrionManager.AlertType) {
            print(#function, alertType)
        }
        
        func appversionUserDidCancel() {
            print(#function)
        }
        
        func appversionUserDidSkipVersion() {
            print(#function)
        }
        
        func appversionUserDidLaunchAppStore() {
            print(#function)
        }
        
        func appversionDidFailVersionCheck(error: Error) {
            print(#function, error)
        }
        
        func appversionLatestVersionInstalled() {
            print(#function, "Latest version of app is installed")
        }
        
        // This delegate method is only hit when alertType is initialized to .none
        func appversionDidDetectNewVersionWithoutAlert(message: String, updateType: UpdateType) {
            print(#function, "\(message).\nRelease type: \(updateType.rawValue.capitalized)")
        }
        
        func onFailure(Error: [String : Any]) {
            
        }
    }

    
