//
//  AppDetailsHandler.swift
//  Maximus
//
//  Created by Isha Ramdasi on 07/02/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import ObjectMapper

@objc protocol AppDetailsCallback {
    @objc optional func detailsSentSuccess(message: [String: Any])
}

class AppDetailsHandler: NSObject {

    var cloudHandlerObj = CloudHandler()
    weak var appDetailsDelegate: AppDetailsCallback?
    static let sharedInstance = AppDetailsHandler()
    
    func sendDMDetails(details: [String: Any]) {
        let suburl = "\(RequestURL.URL_SEND_DM_DETAILS.rawValue)"
        
        
        cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter:details, isheaderAUTH: true, requestType: .put){ response in
            
            if let json = response?.result.value {
                if let statusCode = response?.response?.statusCode {
                    if statusCode >= 200 && statusCode < 300 {
                        let user = Mapper<AppDetailsModel>().map(JSON:json as! [String : Any])!
                        let data:[String:Any] = [StringConstant.data:user]
                        self.updateDMDetailsInDB(data: data)
                    } else {
                        print("Sending data failed")
                    }
                }
                print("DM Details \(json)")
            }else{
                print("Request timed out")
            }
        }
    }
    
    func sendSystemDetails(rider: Int, details:[String: Any]) {
        let suburl = "\(RequestURL.URL_BIKER.rawValue)\(rider)\(RequestURL.URL_SEND_SYSTEM_DETAILS.rawValue)"
        cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter:details, isheaderAUTH: true, requestType: .post){ response in
            
            if let json = response?.result.value {
                if let statusCode = response?.response?.statusCode {
                    if statusCode >= 200 && statusCode < 300 {
                        let user = Mapper<SystemDetails>().map(JSON:json as! [String : Any])!
                        let data:[String:Any] = [StringConstant.data:user]
                        self.updateSystemDetailsInDB(data: data)
                    } else {
                        print("Sending data failed")
                    }
                }
               // print("Phone Details \(json)")
            }else{
                print("Request timed out")
            }
        }
    }
    
    func updateDMDetailsInDB(data: [String: Any]) {
        let dmDetails: DMDetails?
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
        do{
            let dmDetailsArray = try context.fetch(DMDetails.fetchRequest())
            if dmDetailsArray.count > 0 {
                let dmData = data["data"] as? AppDetailsModel
                dmDetails = dmDetailsArray.first as? DMDetails
                dmDetails?.isSynced = true
                dmDetails?.firmware_build_number = dmData?.dm_firmware_build_number
                dmDetails?.firmware_version = dmData?.dm_firmware_version
                dmDetails?.sr_no = dmData?.sr_no
                do {
                    try context.save()
                } catch {
                    
                }
            }
        }catch{
            print("Fetching failed")
        }
    }
    
    func updateSystemDetailsInDB(data: [String: Any]) {
        let deviceDetails: DeviceDetails?
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
        do{
            let deviceDetailsArray = try context.fetch(DeviceDetails.fetchRequest())
            if deviceDetailsArray.count > 0 {
                let systemData = data["data"] as? SystemDetails
                deviceDetails = deviceDetailsArray.first as? DeviceDetails
                deviceDetails?.isSynced = true
                deviceDetails?.app_uuid = systemData?.app_uuid
                deviceDetails?.app_version = systemData?.app_version
                deviceDetails?.date_registered = systemData?.date_registered
                deviceDetails?.device_uuid = systemData?.device_uuid
                deviceDetails?.make = systemData?.make
                deviceDetails?.model = systemData?.model
                deviceDetails?.os_type = systemData?.os_type
                deviceDetails?.os_version = systemData?.os_version
                if let ram = systemData?.ram {
                    deviceDetails?.ram = Int16(ram)
                }
                deviceDetails?.resolution = systemData?.resolution
                do {
                    try context.save()
                } catch {
                    
                }
            }
        }catch{
            print("Fetching failed")
            
        }
    }
    
    func createAppDetailsObj() {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
        do{
            let deviceDetailsArray = try context.fetch(DeviceDetails.fetchRequest())
            if deviceDetailsArray.count > 0 {
                let deviceDetails = deviceDetailsArray.first as? DeviceDetails
                deviceDetails?.app_version =  (Bundle.main.object(forInfoDictionaryKey: StringConstant.appVersion) as AnyObject) as? String
                deviceDetails?.os_version = UIDevice.current.systemVersion
                deviceDetails?.isSynced = false
                do {
                    try context.save()
                } catch {
                    
                }
            } else {
                let deviceDetails = DeviceDetails(context: context)
                deviceDetails.isSynced = false
                deviceDetails.app_version =  (Bundle.main.object(forInfoDictionaryKey: StringConstant.appVersion) as AnyObject) as? String
                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
                dateFormatter.timeZone = TimeZone.current
                deviceDetails.date_registered =  dateFormatter.string(from: Date())
                deviceDetails.device_uuid = UIDevice.current.identifierForVendor?.uuidString
                deviceDetails.app_uuid = (UIDevice.current.identifierForVendor?.uuidString)! + "-" + dateFormatter.string(from: Date())
                deviceDetails.make = "Apple"
                deviceDetails.model = UIDevice.modelName
                deviceDetails.ram = Int16(ceilf(Float(((Float(ProcessInfo.processInfo.physicalMemory) / 1024)/1024)/1024)))
                deviceDetails.os_type = "iOS"
                deviceDetails.os_version = UIDevice.current.systemVersion
                let screenBounds = UIScreen.main.bounds
                let screenScale = UIScreen.main.scale
                let screenSize = CGSize(width: screenBounds.size.width * screenScale, height: screenBounds.size.height * screenScale)
                deviceDetails.resolution = "\(screenSize)"
                do {
                    try context.save()
                } catch {
                    
                }
            }
        }catch{
            print("Fetching failed")
          
        }
    }
    
    func createDMDetailsObj() {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
        do{
            let array = try context.fetch(DMDetails.fetchRequest())
            if array.count > 0 {
                let dmDetailsObj = array.first as? DMDetails
                dmDetailsObj?.isSynced = false
                do {
                    try context.save()
                } catch {
                    
                }
            } else {
                let deviceDetails = DMDetails(context: context)
                let manisfestDetails = getDMDetailsFromManifest()
                deviceDetails.firmware_build_number = manisfestDetails.buildVersion
                deviceDetails.firmware_version = manisfestDetails.version
                deviceDetails.sr_no = ""
                deviceDetails.isSynced = false
                do {
                    try context.save()
                } catch {
                    
                }
            }
        }catch{
            print("Fetching failed")
            
        }
    }
    
    func getDMDetailsFromManifest() -> (buildVersion:String, version:String) {
        do {
            if let file = Bundle.main.url(forResource: DefaultKeys.KEY_OTA_MANIFEST, withExtension: DefaultKeys.KEY_JSON) {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    if let displayModule: [String: Any] = object[DefaultKeys.KEY_OTA_MANIFEST] as? [String : Any] {
                        if let firmware: [String: Any] = displayModule[DefaultKeys.KEY_DISPLAY_MODULE] as? [String : Any] {
                            if let version = firmware[DefaultKeys.KEY_FIRMWARE] as? [String: Any] {
                                if let versionNo: String = version[DefaultKeys.KEY_VERSION] as? String, let build = version["build"] as? String {
                                    return (build, versionNo)
                                }
                            }
                        }
                    }
                    print(object)
                } else if let object = json as? [Any] {
                    // json is an array
                    print(object)
                } else {
                    print("JSON is invalid")
                }
            }else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        return ("", "")
    }
    
    func getAppDetailsObj() -> [String: Any] {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
        do{
            let deviceDetailsArray = try context.fetch(DeviceDetails.fetchRequest())
            if deviceDetailsArray.count > 0 {
                let deviceDetails = deviceDetailsArray.first as? DeviceDetails
                let objToPost:[String: Any] = [ "app_uuid" : deviceDetails?.app_uuid ?? "",
                                  "app_version" : deviceDetails?.app_version ?? "" ,
                                  "date_registered" : deviceDetails?.date_registered ?? "" ,
                                  "device_uuid" : deviceDetails?.device_uuid ?? "",
                                  "make" : deviceDetails?.make ?? "",
                                  "model" : deviceDetails?.model ?? "",
                                  "os_type" : deviceDetails?.os_type ?? "",
                                  "os_version" : deviceDetails?.os_version ?? "",
                                  "resolution": deviceDetails?.resolution ?? "",
                                  "ram": deviceDetails?.ram ?? 0]
                return objToPost
            }
        } catch {
                
        }
        return [:]
    }
    
    func getDmDetailsObj() -> [String: Any] {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
        do{
            let dmDetailsArray = try context.fetch(DMDetails.fetchRequest())
            if dmDetailsArray.count > 0 {
                let dmObj = dmDetailsArray.first as? DMDetails
                let objToPost:[String: Any] = [ "firmware_build_number" : dmObj?.firmware_build_number ?? "",
                                                "firmware_version" : dmObj?.firmware_version ?? "" ,
                                                "sr_no" : dmObj?.sr_no ?? ""]
                return objToPost
            }
        } catch {
            
        }
        return [:]
    }
    
    func isValueChanged(changedValue: String, infoToChange: DMInfo ) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
        do{
            let dmDetailsArray = try context.fetch(DMDetails.fetchRequest())
            if dmDetailsArray.count > 0 {
                let dmObj = dmDetailsArray.first as? DMDetails
                if infoToChange == DMInfo.BuildNo {
                    if dmObj?.firmware_build_number != changedValue {
                        dmObj?.firmware_build_number = changedValue
                        dmObj?.isSynced = false
                    }
                }
                if infoToChange == DMInfo.FirmwareVersion {
                    if dmObj?.firmware_version != changedValue {
                        dmObj?.firmware_version = changedValue
                        dmObj?.isSynced = false
                    }
                }
                if infoToChange == DMInfo.SerialNo {
                    if dmObj?.sr_no != changedValue {
                        dmObj?.sr_no = changedValue
                        dmObj?.isSynced = false
                    }
                }
                do {
                    try context.save()
                } catch {
                    
                }
            }
        } catch {
        }
    }
    
    func getSyncFlagStatus() -> Bool {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
        do{
            let dmDetailsArray = try context.fetch(DMDetails.fetchRequest())
            if dmDetailsArray.count > 0 {
                let dmObj = dmDetailsArray.first as? DMDetails
                return (dmObj?.isSynced)!
            }
        } catch {
            
        }
        return false
    }
}

public extension UIDevice {
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
                switch identifier {
                case "iPod5,1":                                 return "iPod Touch 5"
                case "iPod7,1":                                 return "iPod Touch 6"
                case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
                case "iPhone4,1":                               return "iPhone 4s"
                case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
                case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
                case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
                case "iPhone7,2":                               return "iPhone 6"
                case "iPhone7,1":                               return "iPhone 6 Plus"
                case "iPhone8,1":                               return "iPhone 6s"
                case "iPhone8,2":                               return "iPhone 6s Plus"
                case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
                case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
                case "iPhone8,4":                               return "iPhone SE"
                case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
                case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
                case "iPhone10,3", "iPhone10,6":                return "iPhone X"
                case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
                case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
                case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
                case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
                case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
                case "iPad6,11", "iPad6,12":                    return "iPad 5"
                case "iPad7,5", "iPad7,6":                      return "iPad 6"
                case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
                case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
                case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
                case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
                case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
                case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
                case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
                case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
                case "AppleTV5,3":                              return "Apple TV"
                case "AppleTV6,2":                              return "Apple TV 4K"
                case "AudioAccessory1,1":                       return "HomePod"
                case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
                default:                                        return identifier
                }
            #elseif os(tvOS)
                switch identifier {
                case "AppleTV5,3": return "Apple TV 4"
                case "AppleTV6,2": return "Apple TV 4K"
                case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
                default: return identifier
                }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
}
