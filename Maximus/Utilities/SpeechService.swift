//
//  SpeechService.swift
//  Maximus
//
//  Created by APPLE on 24/09/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//
import Foundation
import UIKit
import AVFoundation
import CallKit

class SpeechService: NSObject, NSSpeechService, AVSpeechSynthesizerDelegate {
    
    static let sharedInstance = SpeechService()
    let synth = AVSpeechSynthesizer()
    let audioSession = AVAudioSession.sharedInstance()
    fileprivate var _listeners = [NSUtteranceProgress]()
    var utteranceID: String = ""
    
    override init() {
        super.init()
        synth.delegate = self
    }
    
    func say(sentence: String) {
        print("@VN \(sentence)")
        let audio = MyDefaults.getBoolData(key: DefaultKeys.KEY_AUDIO_STATUS)
        
        //if audio {
            do {
                let utterance = AVSpeechUtterance(string: sentence)
                // silent mode utterance
                try audioSession.setActive(true)
                if synth.isPaused{
                    synth.continueSpeaking()
                }
                print("@VN: Speaking..")
                synth.speak(utterance)
                
            } catch {
                print("@VN:Failed to parse!")
            }
        //}
    }
    
    //check if phone is available for audio alerts
    func isAvailable() -> Bool {
        if checkPhoneConnectionToHeadphone() && checkCallInProgress() {
            print("@VN: Is available")
            return true
        }
        print("@VN: Is not available")
        return false
    }
    
    func checkCallInProgress() -> Bool {
        // Returns whether or not the user is on a phone call
        for call in CXCallObserver().calls {
            if call.hasConnected || call.isOnHold || call.isOutgoing {
                print("@VN: call in progress")
                do {
                    try audioSession.setCategory(AVAudioSessionCategoryPlayback, with: AVAudioSessionCategoryOptions.mixWithOthers)
                    return true
                } catch{
                    print("@VNFailed to set mode!")
                }
            }
        }
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayback, with: AVAudioSessionCategoryOptions.duckOthers)
        } catch {
            
        }
        print("@VN: call not in progress")
        return true
    }
    
    func checkPhoneConnectionToHeadphone()-> Bool {
        //check if phone is connected to headset
        let availableInputArray = audioSession.currentRoute.outputs
        for port in  availableInputArray {
            print("port type: \(port.portType)")
            if port.portType == AVAudioSessionPortBluetoothHFP ||
                port.portType == AVAudioSessionPortHeadphones ||
                port.portType == AVAudioSessionPortBluetoothA2DP ||
                port.portType == AVAudioSessionPortBluetoothLE ||
                port.portType == AVAudioSessionPortUSBAudio ||
                port.portType == AVAudioSessionPortAirPlay ||
                port.portType == AVAudioSessionPortCarAudio {
                print("@VN: input connected")
                return true
            }
        }
        
        print("@VN: input not connected")
        return false
    }
    
    func isSpeaking() -> Bool {
        return true
    }
    
    func speak(_ text: String, utteranceId: String) {
        if isAvailable() {
            print("@VN: is available say..")
            self.utteranceID = utteranceId
            self.say(sentence: text)
        }
    }
    
    func stop(_ utteranceId: String) {
        do {
            print("@VN: stopping..")
            try audioSession.setActive(false)
            for listener in  _listeners {
                listener.onStop(self.utteranceID, interrupted: true)
                self.utteranceID = ""
            }
        } catch {
            print("@VN:Failed to stop!")
        }
    }
    
    func reset() {
        do {
            print("@VN: resetting..")
            try audioSession.setActive(false)
            
        } catch {
            print("@VN:Failed to reset!")
        }
    }
    
    func subscribe(_ listener: NSUtteranceProgress?) -> String {
        print("@VN:subscribe speech listners")
        _listeners.append(listener!)
        return "ALL"
    }
    
    func unsubscribe(_ token: String) {
        print("@VN:unsubscribe")
        _listeners.removeAll()
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        do {
            print("@VN: did finish..")
            try audioSession.setActive(false)
            for listner in _listeners {
                listner.onStop(self.utteranceID, interrupted: false)
                self.utteranceID = ""
            }
            
        } catch {
            print("@VN:Failed to finish!")
        }
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didPause utterance: AVSpeechUtterance) {
        print("VN: didPause")
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didContinue utterance: AVSpeechUtterance) {
        print("VN: didContinue")
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didCancel utterance: AVSpeechUtterance) {
        print("VN: didCancel")
        for listner in _listeners {
            listner.onStop(self.utteranceID, interrupted: true)
            self.utteranceID = ""
        }
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didStart utterance: AVSpeechUtterance) {
        print("VN: didStart")
    }

}
