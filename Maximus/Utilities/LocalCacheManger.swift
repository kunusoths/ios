//
//  LocalCacheManger.swift
//  Maximus
//
//  Created by Admin on 03/04/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation

class LocalCacheData {
    
    class var shared: LocalCacheData {
        struct Static {
            static let instance: LocalCacheData = LocalCacheData()
        }
        return Static.instance
    }
    
   //Storing Graph Data
    public func saveGraphData(value:String){
        MyDefaults.setData(value: value, key: DefaultKeys.KEY_GRAPHDATA)
    }
    
    public func getGraphData()->String{
        if(MyDefaults.isKeyPresentInUserDefaults(key:DefaultKeys.KEY_GRAPHDATA)){
            return MyDefaults.getData(key: DefaultKeys.KEY_GRAPHDATA) as! String
        }else{
            return ""
        }
    }

    ////Storing Current Progress RideDetails Data
    public func saveProgressRideDetailsData(value:String){
        MyDefaults.setData(value: value, key: DefaultKeys.KEY_RIDEDETAILS)
    }
    
    public func getProgressRideDetailsData()->String{
        if(MyDefaults.isKeyPresentInUserDefaults(key:DefaultKeys.KEY_RIDEDETAILS)){
            return MyDefaults.getData(key: DefaultKeys.KEY_RIDEDETAILS) as! String
        }else{
            return ""
        }
    }
    
    public func deleteProgressRideDetailsData(){
        MyDefaults.removeForKey(key:DefaultKeys.KEY_RIDEDETAILS)
    }

    ////Storing Navigation Token Locally Data
    public func saveNavigationToken(value:String){
        MyDefaults.setData(value: value, key: DefaultKeys.KEY_NAV_TOKEN)
    }
    
    public func getSavedNavigationToken()->String{
        if(MyDefaults.isKeyPresentInUserDefaults(key:DefaultKeys.KEY_NAV_TOKEN)){
            return MyDefaults.getData(key: DefaultKeys.KEY_NAV_TOKEN) as! String
        }else{
            return ""
        }
    }
    
    public func deleteNavigationTokenKeyFromData(){
        MyDefaults.removeForKey(key:DefaultKeys.KEY_NAV_TOKEN)
    }


}
