//
//  MydefaultsUtility.swift
//  Maximus
//
//  Created by Admin on 20/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation

//MARK:************ Default Methods ********

class MyDefaults: NSObject {
    
    class func setData(value:String , key:String){
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
    }
    
    class func getData(key:String)->AnyObject {
        if(MyDefaults.isKeyPresentInUserDefaults(key:key)){
            let defaults = UserDefaults.standard
            return defaults.value(forKey: key) as AnyObject
        }else{
            return "" as AnyObject
        }
    }
    
    class func setDataType(value:Data , key:String){
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
    }
    
    class func getDataType(key:String)->Data {
        if(MyDefaults.isKeyPresentInUserDefaults(key:key)){
            let defaults = UserDefaults.standard
            return defaults.value(forKey: key) as! Data
        }else{
            return Data()
        }
    }
    
    class func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    class func setInt(value:Int , key:String){
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
    }
    
    
    class func getInt(key:String)->Int {
        if(MyDefaults.isKeyPresentInUserDefaults(key:key)){
            let defaults = UserDefaults.standard
            return defaults.integer(forKey: key)
        } else{
            return 0
        }
    }
    
    class func setBoolData(value:Bool , key:String){
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
    }
    class func getBoolData(key:String)->Bool {
        let defaults = UserDefaults.standard
        return defaults.bool(forKey: key)
    }
    
    class func setDictionary(value:[String : Any] , key:String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
    }
    
    class func getDictionary(key:String)->[String : Any] {
        let defaults = UserDefaults.standard
        return defaults.value(forKey: key) as! [String : Any]
    }
    
    class func setArray(value: [Any], key: String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
    }
    
    class func getArray(key:String)->[Any] {
        let defaults = UserDefaults.standard
        return defaults.value(forKey: key) as! [Any]
    }
    
    class func clearDefaults(){
        let appDomain = Bundle.main.bundleIdentifier!
        let defaults = UserDefaults.standard
        defaults.removePersistentDomain(forName: appDomain)
    }
    
    class func removeForKey(key:String){
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: key)
    }
}
