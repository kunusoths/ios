//
//  GraphUtility.swift
//  Maximus
//
//  Created by Sriram G on 27/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import Charts

class GraphUtilit
{

    // MARK:Public Methods
    func initChart( linechart:LineChartView )
    {
        // disable chart description
        linechart.chartDescription?.enabled = false
        linechart.drawMarkers = true
        
        // removed the grid lines of chart
        linechart.leftAxis.drawGridLinesEnabled = false
        linechart.rightAxis.drawGridLinesEnabled = false
        linechart.xAxis.drawGridLinesEnabled = false
        
        linechart.dragEnabled = true
        linechart.setScaleEnabled(true)
        linechart.scaleYEnabled = false
        linechart.scaleXEnabled = true
        linechart.pinchZoomEnabled = true
        linechart.doubleTapToZoomEnabled = true
        
        linechart.zoom(scaleX: 0.0, scaleY: 1.0, x: 0, y: 0)
        linechart.animate(xAxisDuration: 1.4, easingOption: .easeInOutQuart)
        

        let marker:BalloonMarker = BalloonMarker(color: UIColor.white, font: UIFont(name: "Helvetica", size: 12)!, textColor: UIColor.black, insets: UIEdgeInsets(top: 7.0, left: 7.0, bottom: 7.0, right: 7.0))
        marker.minimumSize = CGSize(width: 75, height: 35.0)
        marker.chartView = linechart
        linechart.marker = marker
    }
    
    func addYAxis(linechart:LineChartView) -> (YAxis)
    {
        let yaxis:YAxis = linechart.leftAxis
        yaxis.labelTextColor = UIColor.white
        yaxis.axisMinimum = 0
        yaxis.drawGridLinesEnabled = false
        yaxis.granularityEnabled = true
        
        
        let rightAxis :YAxis = linechart.rightAxis
        rightAxis.enabled = false
        return yaxis;
    }
    
    func setLimitLine(val:double_t, color:UIColor, linechart:LineChartView)
    {
        let limitLine = ChartLimitLine(limit: val, label: "")
        limitLine.lineColor = color
        limitLine.lineWidth = 1
        self.addYAxis(linechart: linechart).addLimitLine(limitLine)
    }
        
    func setChart(lineChart:LineChartView, data:[ChartDataEntry] ,arrOfGradientPosition:[CGFloat])
    {
        
        let valueColors = [UIColor]()
        lineChart.xAxis.drawAxisLineEnabled = false
        lineChart.legend.enabled = false
        lineChart.xAxis.drawLabelsEnabled = false
        
        let lineChartDataSet = LineChartDataSet(values: data, label: "")
        lineChartDataSet.axisDependency = .left
        lineChartDataSet.setColor(UIColor.yellow.withAlphaComponent(1))
        lineChartDataSet.setCircleColor(UIColor.red)
        lineChartDataSet.lineWidth = 2.0
        lineChartDataSet.circleRadius = 10.0
        lineChartDataSet.fillAlpha = 65/255.0
        lineChartDataSet.fillColor = UIColor.black
        lineChartDataSet.highlightColor = UIColor.white
        lineChartDataSet.drawCirclesEnabled = false
        lineChartDataSet.mode = .cubicBezier
        lineChartDataSet.drawHorizontalHighlightIndicatorEnabled = true
        lineChartDataSet.fill = Fill.fillWithColor(UIColor.black)
        lineChartDataSet.drawFilledEnabled = false // Draw the Gradient
        lineChartDataSet.valueColors = valueColors
        lineChartDataSet.drawValuesEnabled = false
        lineChartDataSet.highlightEnabled = true
        lineChartDataSet.highlightColor = UIColor.white
        
        lineChart.data?.highlightEnabled = true//(lineChart.data?.isHighlightEnabled)!
        lineChartDataSet.drawGradientEnabled = true
        lineChartDataSet.colors = [UIColor.red,UIColor(red: 247.0/255.0, green: 107.0/255.0, blue: 28.0/255.0, alpha: 1.00), UIColor(red: 250.0/255.0, green: 217.0/255.0, blue: 97.0/255.0, alpha: 1.00)]//, UIColor(red: 69.0/255.0, green: 255.0/255.0, blue: 0.0/255.0, alpha: 1.00)]//, UIColor(red: 0.980, green: 0.090, blue: 0.157, alpha: 1.00)]
        lineChartDataSet.gradientPositions = arrOfGradientPosition//[25.0,35.0,45.0]
        
        var dataSets = [IChartDataSet]()
        dataSets.append(lineChartDataSet)
        
        let lineChartData = LineChartData(dataSets: dataSets)
        lineChart.data = lineChartData
        
    }
    
}
