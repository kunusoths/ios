//
//  Toast Constant.swift
//  Maximus
//
//  Created by Admin on 15/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class ToastPopUp {

    //Toast Alert
    class public func showNetworkPOPup( message:String , view:UIView)
    {
        var msg:String?
        if message.isBlank {
            msg = ToastMsgConstant.noMsgForNetwork
        }else {
            msg = message
        }
        
        let FONT_NAME:FontName  = .Roboto
        let FONT_TYPE:FontType  = .Medium
        let FONT_SIZE:CGFloat   = 13.0 //Font Size
        let textColor           = UIColor.white//Toast Font Color
    
        let label = UILabel(frame: CGRect.zero)
        label.textAlignment = NSTextAlignment.center
        label.text = msg
        label.font = UIFont.Font(FONT_NAME , type: FONT_TYPE , size: FONT_SIZE)
        label.adjustsFontSizeToFitWidth = true
        
        label.backgroundColor =  UIColor.red
        label.textColor = textColor
        
        label.sizeToFit()
        label.numberOfLines = 4

        label.frame = CGRect(x: 0, y:view.size.height - 40, width: (view.frame.size.width), height: 40)
        label.alpha = 1
        view.addSubview(label)
        
        var basketTopFrame: CGRect = label.frame;
        basketTopFrame.origin.y = view.size.height - 40;
        
        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.0, initialSpringVelocity: 0.0, options: [.beginFromCurrentState, .curveLinear, .allowUserInteraction], animations: { () -> Void in
            ///// Uncomment this if u want to hide & show status bar when toast appears
//            DispatchQueue.main.async(execute: {
//                if let window = UIApplication.shared.keyWindow {
//                    window.windowLevel = UIWindowLevelStatusBar + 1
//                }
//            })
            label.frame = basketTopFrame
        },  completion: {
            (value: Bool) in
            UIView.animate(withDuration: 1.0, delay: 1.5, usingSpringWithDamping: 0.0, initialSpringVelocity: 0.0, options: [.beginFromCurrentState, .curveLinear, .allowUserInteraction], animations: { () -> Void in
                label.alpha = 0
            },  completion: {
                (value: Bool) in
                ///// Uncomment this if u want to hide & show status bar when toast appears
//                DispatchQueue.main.async(execute: {
//                    if let window = UIApplication.shared.keyWindow {
//                        window.windowLevel = UIWindowLevelNormal
//                    }
//                })
                label.removeFromSuperview()
            })
        })
    }
}
