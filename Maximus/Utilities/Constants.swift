//
//  Constants.swift
//
//  Created by Admin on 14/11/16.
//  Copyright © 2016 Appsplanet. All rights reserved.
//

import UIKit
import CoreLocation

class KEY_GOOGLE:NSObject{
    
    ///Credentials
    
    ///Staging
    static let APIKEY_STAGING_MAPS            =    "AIzaSyA1eHoHwZcYLmw0f_q4Mwr1snFlqZ3mIMk"
    static let APIKEY_STAGING_TBT             =    "AIzaSyADH0uVUBDJeFj2aLqroQRFFZAWTfYoeVY"
    static let APIKEY_STAGING_DYNAMICLINKS    =    "AIzaSyCxK6o4zUaL_XoymOix4sysVMNpDo-uyuU"
    ///Production
    static let APIKEY_PRODUCTION_MAPS          =    "AIzaSyCvkKlnVSKVR7C8oWEDJDunMn5KmLiXzws"
    static let APIKEY_PRODUCTION_TBT           =    "AIzaSyDQvU3ZdmXN-440Ruy4_2ayMCe-EWWTHgI"
    static let APIKEY_PRODUCTION_DYNAMICLINKS  =    "AIzaSyDCEFi4csAQUpuBgmqbELYbK4tq8PpRpBk"
    
    ///Key names to store credentials in Keychain
    ///Staging
    static let KEY_NAME_MAP_STAGING           =   "com.maximuspro.apikeyDebugMap"
    static let KEY_NAME_TBT_STAGING           =   "com.maximuspro.apikeyDebugTBT"
    static let KEY_NAME_DYNAMICLINK_STAGING   =   "com.maximuspro.apikeyDebugDynamicLink"
    ///Production
    static let KEY_NAME_MAP_PRODUCTION         =   "com.maximuspro.apikeyReleseMap"
    static let KEY_NAME_TBT_PRODUCTION         =   "com.maximuspro.apikeyReleseTBT"
    static let KEY_NAME_DYNAMICLINK_PRODUCTION =   "com.maximuspro.apikeyReleseDynamicLink"
    
    
    
}

class KEY_OAUTH:NSObject{
    //oauth server client credentials
    static let APIKEY_OAUTH_CLIENT_SECRET = "05ef2dc2-a731-4e18-90db-787ba145fb1b"
    static let KEY_NAME_OAUTH_CLIENT_SECRET = "oauth_client_secret"
    
}

extension Constants {
    class func getAdHocRideTitle() -> String {
        
        let afternoon = 12; let evening = 16; let noon = 12;
        var rideTitle = ""
        let currentDate = Date()
        
        let formater = DateFormatter()
        formater.dateFormat = "dd-MM-yyyy"
        let str = formater.string(from: currentDate)
        
        let c = Calendar.current
        let hour = c.component(.hour, from: currentDate)
        
        switch hour {
        case _ where hour < noon :
            print("Morning Ride On")
            rideTitle = "Morning Ride On" + " " + str
            break
            
        case noon:
            print("Noon Ride On")
            rideTitle = "Noon Ride On" + " " + str
            
            break
            
        case afternoon...evening:
            print("Afternoon Ride On")
            rideTitle = "Afternoon Ride On" + " " + str
            
            break
            
        case _ where hour > evening:
            print("Evening Ride  On")
            rideTitle = "Evening Ride On" + " " + str
            
            break
            
        default:
            print("Ride On")
            rideTitle = "Ride On" + " " + str
            
            break
        }
        
        return rideTitle
    }
}

class Constants: NSObject {
    
    #if CLOUD_SERVER_STAGING
    ///For Debug
    static let APIKEY_MAPS              =    KeychainService.shared[KEY_GOOGLE.KEY_NAME_MAP_STAGING]
    static let APIKEY_TBT               =    KeychainService.shared[KEY_GOOGLE.KEY_NAME_TBT_STAGING]
    static let APIKEY_DYNAMICLINKS      =    KeychainService.shared[KEY_GOOGLE.KEY_NAME_DYNAMICLINK_STAGING]
    #elseif CLOUD_SERVER_PRODUCTION
    ///For Relese
    static let APIKEY_MAPS              =    KeychainService.shared[KEY_GOOGLE.KEY_NAME_MAP_PRODUCTION]
    static let APIKEY_TBT               =    KeychainService.shared[KEY_GOOGLE.KEY_NAME_TBT_PRODUCTION]
    static let APIKEY_DYNAMICLINKS      =    KeychainService.shared[KEY_GOOGLE.KEY_NAME_DYNAMICLINK_PRODUCTION]
    #endif

    ///Enable disable ble logs
    #if CLOUD_SERVER_STAGING
    ///For Debug
    static let BLE_LOGS              =    true
    #elseif CLOUD_SERVER_PRODUCTION
    ///For Relese
    static let BLE_LOGS              =    true
    #endif

    @available(iOS 10.0, *)
    static let appDelegate = UIApplication.shared.delegate as? AppDelegate
//    static let apiKey_map               = "AIzaSyBX_mFbCPc6ljzr4kObmP9DOnLu6oxKLvo"
 //   static let API_KEY_GMSPLACECLIENT   = "AIzaSyDwkdD9ELIn2zNLHP71Rp1fFfdgf018BFw"
//    static let API_KEY_GOOGLE           = "AIzaSyDqoox5zDORJT1oH34YuL5Vsr_nHaRrmlE"
    //static let API_KEY_GOOGLE           = "AIzaSyAzw7DacW2nCrPgMqjJzzAzyixxEKW9lac"//last appstore Live key
    
    static let apiKeyGooglePOIDetailsURL = "https://maps.googleapis.com/maps/api/place/details/json?placeid="
    static let apiKeyGooglePOIDetailsURLPART2 = "&sensor=false&key="
    static let gcmMessageIDKey = "gcm.message_id"
    static let theftAlertMessage = "Bike stolen! Help if you are nearby..."
    static let openWheatherApiKey = "2e85e843377cb1f624f1be31e45f52d0"
    static let ZERO_VALUE = 0

    enum UIUserInterfaceIdiom : Int
    {
        case Unspecified
        case Phone
        case Pad
    }
    
    struct ScreenSize
    {
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType
    {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPHONE_7          = IS_IPHONE_6
        static let IS_IPHONE_7P         = IS_IPHONE_6P
        
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
        static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
    }
    
    struct Version {
        static let SYS_VERSION_FLOAT = (UIDevice.current.systemVersion as NSString).floatValue
        static let iOS7 = (Version.SYS_VERSION_FLOAT < 8.0 && Version.SYS_VERSION_FLOAT >= 7.0)
        static let iOS8 = (Version.SYS_VERSION_FLOAT >= 8.0 && Version.SYS_VERSION_FLOAT < 9.0)
        static let iOS9 = (Version.SYS_VERSION_FLOAT >= 9.0 && Version.SYS_VERSION_FLOAT < 10.0)
        static let iOS10 = (Version.SYS_VERSION_FLOAT >= 10.0 && Version.SYS_VERSION_FLOAT < 11.0)
        static let iOS11 = (Version.SYS_VERSION_FLOAT >= 11)
    }
    
}

struct CP_CONSTANTS{
    static let ICON_DEFAULTVALUE = 555
    static let HOUR_IN_SECONDS = 3600
    static let SECONDS_IN_MINUTE = 60
    static let ONE_KM:Int = 1000
    static let TEN_KM:Int = 10000
    static let HALF_KM:Int64 = 500
    static let BLE_DELAY = 0.075
}
//MARK:*** Logger

struct MaximusClass{
    
    static let MAXIMUS_DM = "@MaximusDM "
}

class Log:NSObject{
    
    class func i(tag:String, message:String){
        FCMFileUploadManager.fcmManager.writeToLogFile(logToWrite: "\(Date()) ::\(tag) : \(message) ")
        print("\(Date()) " + tag + ": " + message )
    }
    
    class func i(tag:String, message:String, value:Any){
        print(tag + ": " + message, value)
    }
    
}

// MARK: ------- OTA VALIDATION ------

enum OTAErrorCases:Int {
    case BLE_NOT_CONNECTED = 0
    case PHONE_BATTERY_LOW 
    case DM_BATTERY_LOW
    case PHONE_DISPLAY_VERSION_MISMATCH
    case DM_OTA_UPDATED_NOT_ALLOWED
    case INSUFFICIENT_SPACE_ON_DM
    case RIDE_IN_PROGRESS
    case BLE_DISCONNECTED
    case OTA_ABORTED
    case OTA_TIME_OUT_ERROR
    case OTA_EXCEEDED_REPEATED_ATTEMPT_COUNT
    case OTA_DM_FLASH_ERROR
    case OTA_UNEXPECTED_END_OF_FILE_OCCURED
    case OTA_ERROR_IN_READING_FILE
    case OTA_ERROR_IN_READING_IMAGE_PACKET
    case OTA_CHECKSUM_ERROR
}

//MARK:************ Default Theme Color of APP ********

struct AppTheme {

    static let NAVBAR_COLOR     = UIColor (r:47.0 , g:55.0 , b:62.0, a:1.0)
    static let THEME_COLOR      = UIColor (r:18.0 , g:26.0 , b:33.0, a:1.0)
    static let THEME_ORANGE     = UIColor (r:232.0 , g:78.0 , b:39.0, a:1.0)
    static let THEME_ON_TINT    = UIColor (r:128.0 , g:0.0 , b:0.0, a:1.0)
    static let THEME_LIGHT_GRAY = UIColor (r:229.0 , g:229.0 , b:229.0, a:1.0)
    static let THEME_DARK_GRAY = UIColor(r: 155.0, g: 155.0, b: 155.0, a: 1.0)
    
    
}
//MARK:************ Default Keys ********

struct DefaultKeys {
    
    static let KEY_ISLOGIN      = "LoginSession"
    static let KEY_CMREGISTER   = "CMRegistered"
    static let KEY_DMREGISTER   = "DMRegistered"
    static let KEY_RIDEPROGRESS = "RideInProgress"
    static let KEY_OTPVERIFIED  = "OtpVerified"
    static let KEY_PHONENUMBER  = "PhoneNumber"
    static let KEY_RIDEID       = "RideId"
    static let KEY_RIDETITLE    = "RideTitle"
    static let KEY_TOKEN        = "TokenString"
    static let KEY_REFRESH_TOKEN = "RefreshTokenString"
    static let KEY_BIKERID      = "BikerId"
    static let KEY_FIRST_NAME   = "FirstName"
    static let KEY_LAST_NAME    = "LastName"
    static let KEY_USERNAME     = "UserName"
    static let KEY_PROFILE_URL  = "UserProfileUrl"
    static let FCM_TOKEN:String = "fcmToken"
    static let KEY_DefaultVehicleID = "defaultVehicleID"
    static let KEY_EMERGENCYCONTACTADDED = "emergencyContactAdded"
    static let KEY_TUTORIAL_SEEN = "TutorialSeen"
    static let KEY_FROMWHERE     = "FromWhere"
    static let KEY_SHARERIDETITLE    = "RideTitle"
    static let KEY_RIDEDETAILS = "rideDetails"
    static let KEY_CMSERIAL_NO = "registeredCMNumber"
    static let KEY_JOIRNEY_COMPLETE = "isJourneyCompleted"
    static let IS_CM_VIRTUAL     = "isCMVirtual"
    static let MESSAGE_NUMERATOR = "MessageNumerator"
    static let IS_RIDE_COMPLETED = "IsRideCompleted"
    static let KEY_BIKER_EMAIL = "BikerMail"
    static let KEY_AUDIO_STATUS = "AudioStatus"
    static let KEY_Location_ToastMessageShown = "locationToastMessageShown"
    static let KEY_VirtualCm_Offline_ToastMessageShown = "VirtualCmOfflineToastMessageShown"
    
    
    //Cache Keys
    static let KEY_RIDELOGDATA  = "rideLogDATA"
    static let KEY_STICKERDATA  = "stickerDATA"
    static let KEY_GRAPHDATA    = "graphDATA"
    static let KEY_MYPLANRIDEDATA  = "MyPlannedrideDATA"
    static let KEY_RECOMMENDEDATA  = "RecommendedDATA"
    static let KEY_POI_RIDEID       = "POiRideId"
    static let KEY_FBACCESSTOKEN  = "FBAccessToken"
    static let KEY_FB_ID            = "FB_ID"
    static let KEY_FB_EMAIL         = "FB_EMAIL"
    static let KEY_DEEPLINK         = "DeepLink"
    static let KEY_BLE_CONNECTED    = "ConnectedDevice"
    static let KEY_CURRENT_RIDE_TBT_ROUTE = "currentRideTbtRoute"
    
    //Navigation Token Key
    static let KEY_NAV_TOKEN = "com.Navigation.Token"
    
    //Upgrade DM
    static let KEY_DM_VERSION = "DMVersion"
    static let KEY_APP_VERSION = "AppVersion"
    static let KEY_UPDATE_SUCCESSFUL = "DMUpdatedSuccessfully"
    static let KEY_OTA_MANIFEST = "ota_manifest"
    static let KEY_DISPLAY_MODULE = "diasplay_module"
    static let KEY_FIRMWARE = "firmware"
    static let KEY_VERSION = "version"
    static let KEY_JSON = "json"
    static let KEY_BUNDLE_VERSION = "CFBundleShortVersionString"
    static let KEY_JSON_VERSION = "FWVersionInJson"
    
    //Offline start stop
    static let KEY_SERVER_OFFSET_TIME = "ServerTimeOffset"
    static let KEY_RELOGIN = "Relogin"
    static let KEY_FORCED_LOGOUT = "IsForcedLogotDone"
    static let KEY_REQUEST_OBJ_ID = "RequestObjID"
    
    static let KEY_EMAIL_VERIFY = "EmailVerfiedPopup"
}

enum fromViewcontroller: String {
    case CMPairedSuccessVC  = "CMPairedSuccessVC"
    case HomeVC             = "HomeVC"
    case ProfileVC          = "ProfileVC"
    case SettingsVC = "SettingVC"
}

//MARK:********** URL Constants *************
public enum RequestURL: String {
    
    #if CLOUD_SERVER_STAGING
    case URL_BASE = "https://api-beta.maximuspro.com/maximuspro-v2/api"//"https://api-beta.maximuspro.com/api/v1.3"///----For Beta Release
    //case URL_BASE             = "http://103.243.226.150:8085/api/v1"  //-------Production URL
    //case URL_BASE               = "https://api-beta.maximuspro.in/api/v1" //--------Staging URL(Teaser)
    //case URL_BASE               = "https://api-beta.maximuspro.in/api/v1.2" //-Staging URL(Development)
    //case URL_BASE               = "https://api-beta.maximuspro.com/api/v1.2"// google staging
    //case URL_BASE               = "https://api.maximuspro.com/api/v1.2"// First LIVE build
    #elseif CLOUD_SERVER_PRODUCTION
    case URL_BASE = "https://api.maximuspro.com/api/v1.3"///--------For Final Distrubution
    #endif
    
    case URL_TOS                = "http://maximuspro.com/legal/termsofservice/EULA"
    case URL_Privacy            = "http://maximuspro.com/legal/termsofservice/privacypolicy"
    case URL_AboutUS            = "http://www.maximuspro.com/"

    case URL_VEHICLES           = "/vehicles/"
    case URL_AUTHENTICATION     = "/authenticate"
    case URL_REGISTER           = "/register"
    case URL_DEFAULTIMG         = "/data/defaultimages"
    case URL_CM_REGISTRATION    = "/vehicles/map_device"//"/vehicles/validateCM"
    case URL_ACTIVE_DEVICE_MAPPING = "/active_device_mapping"
    case URL_GETOTP             = "/bikers/generate_otp"
    case URL_VALIDATEOTP        = "/bikers/validate_otp"
    case URL_RIDE               = "/rides/"
    case URL_START              = "/start"
    case URL_STOP               = "/finish"
    case URL_BATTERYVOLTAGE     = "/analytics/ride_stats/bikers/battery_voltage"//_dummy"
    case URL_MEANBIKEBATTERYVOLTAGE = "/mean_bike_battery_voltage"
    case URL_BIKES              = "/data/bikes"
    case URL_BIKER_OAUTH = "/biker"
    case URL_ACCOUNT = "/account"
    case URL_OAUTH_REGISTER = "https://api-beta.maximuspro.com/register-v2/api/register"
    case URL_OAUTH_ACCESS_TOKEN = "https://api-beta.maximuspro.com/auth/realms/jhipster/protocol/openid-connect/token"
    case URL_BIKER              = "/bikers/"
    case URL_BIKER_MYRIDES      = "bikers/"
    case URL_STATS              = "/stats"//_dummy"
    case URL_ROUTE              = "/route"//_dummy"
    case URL_SPEEDPOINT         = "/speed_points"//_dummy"
    case URL_BADGE              = "/honored_badges"//_dummy"
    case URL_RIDE_STATUS        = "/analytics/ride_stats/"
    case URL_BIKER_TOKEN        = "/bikers/tokens"
    case URL_RIDE_STATS_RECALCULATE = "/recalculate"
    case URL_RIDE_STATS         = "/ride_stats?page=0&size=1000&sort=startDate,asc"//_dummy"
    case URL_STICKERBOARD       = "/sticker_board"//_dummy"
    case URL_ANALYTICS_BIKERS   = "/analytics/bikers/"
    case URL_CURENT_RIDEPROGRES = "/inprogress"
    case URL_RIDE_BIKER         = "/rides/bikers/"
    case URL_DEVICE_DATA        = "/latest_device_data"
    case URL_ROUTING            = "/rides/routing"
    case URL_JOINED             = "/joined?page=0&size=1000&sort=startDate,asc"
    case URL_RECOMMENDED        = "/feed"
    case URL_CLONE              = "/clone"
    case URL_PROFILE_IMAGE      = "profile_image"
    case URL_VEHICLE            = "/vehicles"
    case URL_BIKE_MODEL         = "/bike_models"
    case URL_POI                = "/places_of_interest?radius=10000"
    case URL_PlANNED_WAVEPOINTS = "/planned_waypoints"
    case URL_PLANNED_WAVEPOINTS_LIST = "/planned_waypoints_list"
    case URL_JOIN               = "/join"
    case URL_REJECT             = "/reject"
    case URL_SUMMARY            = "summary?page=0&size=1000"
    case URL_ALERTS             = "/alerts"
    case URL_ALERTS_SUB         = "/alerts/"
    case URL_RESPOND_TO_ALERT   = "/response"
    case URL_CLOSE_ALERT        = "/close"
    case URL_NOTIFY_ALERT_DELIVERED = "/notifications/status"
    case URL_OPEN_WEATHER       = "http://api.openweathermap.org/data/2.5/weather?"
    case URL_LOCATION = "/locations"
    case URL_POI_POLYLINE = "/places_of_interest_polyline"
    case URL_FORGOT_PASSWORD = "/account/reset_password/init"
    case URL_LEAVE_RIDE = "/leave"
    case URL_SEND_DM_DETAILS = "/display-modules/update_firmware_version"
    case URL_SEND_SYSTEM_DETAILS = "/phone_devices/register"
    case URL_FEED = "feed"
}

//MARK: ********** JOIN TYPES *************
public enum JoinRideTypes: String
{
    case JOIN_SHARE_LINK            = "SHARED_LINK"
    case JOIN_RECOMMENDATION_LINK   = "RECOMMENDATION"
    case JOIN_INVITATION_LINK       = "INVITATION"
}


struct RideMemberStatus{
    static let DEFAULT      = 0
    static let INVITED      = 1
    static let ACCEPTED     = 2
    static let REJECTED     = 3
    static let LEFT         = 4
    static let FOLLOWED     = 5
    static let STARTED      = 6
    static let FINISHED     = 7
    
}

//MARK: ********** Poly Line *************
struct PolylineColorSize{
    static let WIDTH               = 5
    static let ORANGE_COLOR        = UIColor (r:232.0 , g:78.0 , b:39.0, a:1.0)
}

public enum POIType:Int { case Fuel = 0, Restaurent, Hotel, ViaPoint }

public struct POIData {
    let name: String
    let location: CLLocation
    let poiInfo: POIList?
    let poiType: POIType
    var order = 0
}

//MARK: ********** String Constants *************

struct StringConstant {
    
    static let oauth_client_id = "maximuspro-ios-client"
    static let token = "token"
    static let id_token = "id_token"
    static let refreshToken = "refresh_token"
    static let grantType = "grant_type"
    static let clientId = "client_id"
    static let clientSecret = "client_secret"
    static let request = "request"
    static let responseDataKey = "OAuthSwiftError.response.data"
    static let responseKey = "OAuthSwiftError.response"
    static let subject_token = "subject_token"
    static let subject_token_type = "subject_token_type"
    static let subject_issuer = "subject_issuer"
    static let tokentype_accesstoken = "urn:ietf:params:oauth:token-type:access_token"
    static let granttype_tokenexchange = "urn:ietf:params:oauth:grant-type:token-exchange"
    static let offline_access = "offline_access"
    static let scope = "scope"
    static let rememberMe = "remember_me"
    static let login = "login"
    static let email = "email"
    static let password = "password"
    static let firstName = "first_name"
    static let accessToken      = "access_token"
    static let ID               = "id"
    static let username         = "username"
    static let auth_method      = "auth_method"
    static let auth_metadata    = "auth_metadata"
    static let data             = "data"
    static let AllBikesList     = "AllBikesList"
    static let planned          = "planned"
    static let biker_id         = "biker_id"
    static let biker_id_recommendation_id = "biker_id recommendation_id"
    static let time_stamp = "time_stamp"
    static let end_date         = "end_date"
    static let start_date       = "start_date"
    static let rideBadgeList    = "HonorBadgeList"
    static let rideLogList      = "rideLogList"
    static let rideMemberlist   = "rideMemberlist"
    static let serial_number    = "serial_number"
    static let vehicle_id       = "vehicle_id"
    static let rideInprogresList = "rideInprogressList"
    static let rideDetailsVc    = "RideDetailsVC"
    static let shareRide        = "shared"
    static let armDisarmStatus  = "armDisarmStatus"
    static let getPOIList       = "POIListResponse"
    static let wavePointsList   = "WavePointsList"
    static let POI_CATEGORY_FUEL_PUMPS = "gas_station"
    static let POI_CATEGORY_RESTAURANTS = "restaurant"
    static let POI_CATEGORY_FOOD = "food"
    static let POI_CATEGORY_CAFE = "cafe"
    static let POI_CATEGORY_LODGING = "lodging"
    static let POI_TITLE = "poiTitle"
    static let POI_RATING = "rating"
    static let POI_SUBTITLE = "poiSubtitle"
    static let POI_LAT = "poiLat"
    static let POI_LONG = "poiLong"
    static let POI_TYPE = "poiType"
    static let POI_BUISSNESS_PHONE_NUMBER = "phone_number"
    static let POI_INFO = "poiInfo"
    static let POI_WHOLE_DATA = "poiWholeData"
    static let WAVEPOINT_TYPE_MILESTONE = "MILESTONE"
    static let VIA_POINT = "ROUTE_IDENTITY"
    static let PLACES_OF_INTEREST = "PLACES_OF_INTEREST"
    static let POI_START = "START"
    static let POI_DESTINATION = "DESTINATION"
    static let STARTING_POINT = "SEARCH LOCATION"
    static let VIAPOINT_TITLE = "ADD STOPS"
    static let NAME = "name"
    static let DATE_ADDED = "date_added"
    static let CATEGORIES = "categories"
    static let ALTITUDE = "altitude"
    static let LATITUDE = "latitude"
    static let LONGITUDE = "longitude"
    static let LOCATION = "location"
    static let TYPES = "types"
    static let POI_ID = "poi_id"
    static let MARKER_POI_TYPE = "type"
    static let ROUTE_ORDER = "route_order"
    static let FIRST_POI = "firstPoi"
    static let SECOND_POI = "secondPoi"
    static let THIRD_POI = "thirdPoi"
    static let GOOGLE = "google"
    static let LOCATION_SERVICE_ENABLED = "LocationServiceEnabled"
    static let VIA_POINTS_DETAILS = "via_points_details"
    static let SOURCE_DETAILS = "source_details"
    static let DESTINATION_DETAILS = "destination_details"
    static let MYLOCATION = "My Location"
    static let BIKELOCATION = "Bike Location"
    static let CHOOSE_SOURCE = "Source"
    static let CHOOSE_DESTINATION = "Destination"
    static let PUBLIC_RIDE = "public_ride"
    static let END_LOCATION = "end_location"
    static let START_LOCATION = "start_location"
    static let FINISH_DATE = "finish_date"
    static let VIAPOINTS = "viapoints"
    static let TITLE = "title"
    static let ROUTE = "route"
    static let MyRideType = "myRide"
    static let RecommendedType = "Recommended"
    static let LatLong = "latlong"
    static let appVersion  = "CFBundleShortVersionString"
    static let checklistTxtOnline = "To use MaximusPro and prepare for your ride,\n\n*   Turn on your phone’s bluetooth\n\n*   Switch on MaximusPro display module"
    static let checklistTxtOffline = "To use MaximusPro and prepare for your ride,\n\n*   Turn on your phone’s internet connection.\n\n*   Turn on your phone’s bluetooth\n\n*   Switch on MaximusPro display module"
    static let enter = "ENTER "
    static let keyPassword = "Password"
    static let facebook = "facebook"
    static let nameValidationExpression = "^[a-zA-Z\\ ]{0,64}$"
    static let emailValidationExpression = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    static let passwordValidationExpression = "'\"\"`&()|<>,% "
    static let notAllowedCharactersInPassword = "!@#$*"
    static let termsServiceFileName = "termsandservice"
    static let LicencesList = "Software component links"
    static let DMUsage = "DMUsage"
    static let DMInstallation = "DMInstallation"
    static let CMInstallation = "CMInstallation"
    static let Title = "Title"
    static let Description = "Description"
    static let Img = "Img"
    static let InstallationGuide = "InstallationGuide"
    static let firstTimeDataFetch = "FirstTimeDataFetch"
    static let isRideInProgress = "isRideInProgress"
    static let keyRequestType = "RequestType"
    static let bikerData = "bikerData"
    static let recommendedActionTypePlanRide = "plan_a_ride"
}
struct ImageNames {
    static let ImgStar = "star"
    static let ImgBikeHealth = "Bike Health_1"
    static let ImgTheft = "Theft_1"
}
struct ViewcontrollerNameConstant {
    static let aboutusScreen        = "ABOUT MAXIMUSPRO"
    static let TermsandCondScreen   = "Terms of Services"
    static let PlanRideInDetail = "PlanRideInDetail"
}

struct AlertConstant {
    
    //Defaults
   /* static let KEY_ALERT_STATE = "alertState"
    static let KEY_GENERATED_ALERT_TYPE = "keyGeneratedAlertType"
    static let KEY_GENERATED_ALERT_ID = "keyGeneratedAlertId"
    static let KEY_GENERATED_BIKER_ID = "keyGeneratedBikerId"
    static let KEY_HEPLER_BIKER_NAME = "keyHelperBikerName"
    static let KEY_BIKER_NAME = "keyBikerName"
    static let KEY_UUID = "keyUUID"*/
    
    
    //Strings
    static let TYPE_CRITICAL_STRING = "CRITICAL"
    static let TYPE_NON_URGENT_STRING = "NON-URGENT"
    static let TYPE_URGENT_STRING = "URGENT"
    static let TYPE_SEVERE_CONDITION_STRING = "SEVERE CONDITION"
    static let TYPE_PUNCTURE_STRING = "PUNCTURE"
    static let TYPE_NO_FUEL_STRING = "NO-FUEL"
    static let TYPE_ACCIDENT_STRING = "ACCIDENT"
    
    //Defaults state constants
    static let KEY_ALERT_STATE_CLOSE = 0
    static let KEY_ALERT_STATE_SENT = 1
    static let KEY_ALERT_STATE_HELP_ON_WAY = 2
    static let KEY_ALERT_STATE_ACKNOWLEDGED = 3
    static let KEY_ALERT_RECEIVED = 4
    
    //DB constants
    static let DB_STATE_RECEIVED = "Received"
    static let DB_STATE_CLOSED = "Closed"
    static let DB_STATE_DELIVERED = "Delivered"
    static let DB_STATE_ACKNOWLEDGED = "Acknowledged"
    static let DB_STATE_SENT = "Sent"
    static let DB_STATE_RESEND = "ReSend"
    static let DB_STATE_DISCARD = "Discard"
    static let DB_STATE_HELP_NOT_RECEIVED = "HelpNotReceived"
    static let DB_STATE_AUTO_CANCEL = "AutoCancel"
    
    //API constants
    static let ALERT_TYPE_PUNCTURE = "dm-alert-tyre-puncture"
    static let ALERT_TYPE_NO_FUEL = "dm-alert-no-fuel"
    static let ALERT_TYPE_ACCIDENT = "dm-alert-accident"
    static let ALERT_TYPE_SEVERE_CONDITION = "dm-alert-severe-condition"
    
    static let ALERT_RESPONSE_STATUS_UNKNOWN = -1
    static let ALERT_RESPONSE_STATUS_NOT_RESPONDED = 0
    static let ALERT_RESPONSE_STATUS_ACKNOWLEDGED = 1
    static let ALERT_RESPONSE_STATUS_DISMISSED = 2
    
    //API parameters
    static let PARAM_ALERT_TYPE = "alert_type_name"
    static let PARAM_ALERT_TRIGGERED_BY = "triggered_by_biker_id"
    static let PARAM_ALERT_HELPER_BIKER_IDS = "helper_biker_ids"
    static let PARAM_ALERT_UUID = "uuid"
    static let PARAM_STATUS = "status"
    static let PARAM_ACK = "acknowleded"
    static let PARAM_TYPE = "type"
    
    
}

//MARK: ********** Integer Constants *************
struct IntegerConstant {
    static let DEFAULT_TYPE = 0
    static let SHARED_RIDE = 4
    static let MAX_EMERGENCYCONTACT     = 3
}


//MARK: ******* DynamicLinkShare URL *********
public enum DynamicLinkShareURL: String {
    
    case DynamicRideID      = "mRideID"
    case DynamicLinkDomain  = "jex5z.app.goo.gl"
    case DeepLinkScheme     = "https"
    case DeepLinkHost       = "maximuspro.com"
    case DeepPath           = "/deeplinks"
    
    //case ApiKey             = "AIzaSyBz9RlzKFMhxp7e0aIjMyNE5V27TbwWELo"
    //case FIREBASE_URL       = "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key="
    //case DynamicLinkDomain  = "https://jex5z.app.goo.gl"
    //case DeepLinkUrl        = "https://maximuspro.com/deeplinks"
    //case iosBundleId        = "com.maximuspro.app"
}

//MARK: ******* Error Messages *********
public enum ErrorString: String {
    case RIDE_PRESENT               = "ERROR_RIDE_OTHER_STARTED_RIDE_PRESENT"
}

//MARK: ******* REQUEST TIMEOUT JSON ********
class RequestTimeOut {
    let json:[String:Any] = ["description":ToastMsgConstant.network,
                             "errorCode":1001,"fieldError":"","message":"Request timed out."]
}

struct NotificationConstant
{
    static let ignoreTitle       = "IGNORE"
    static let checkTitle        = "CHECK"
    static let actionIgnore      = "ignore"
    static let actionCheck       = "check"
    static let Title_Maximus     = "Maximus Pro"
    static let otp_Notification_body = "Please verify your phone number to start getting theft alerts and location tracking alerts."
    static let emergency_Notificatio_body = "Please add your emergency contacts."
    static let showEmailPopup = "showEmailPopup"
    static let ble_list_notification = "BLELSIT_NOTIFICATION"
    static let ble_connect_notification = "BLECONNECT_NOTIFICATION"
}

struct NotificationCategoyConstant
{
    static let category_bikeTheft             = "bike-theft-push"
    static let category_batteryVoltage        = "bike-battery-voltage-push"
    static let categoryotp                    = "otp_notificationCategory"
    static let categroy_emergencyContacts     = "emergencyContact_notificationCategory"
    static let category_batteryVoltage_One    = "bike-battery-voltage-severity-one-push"
    static let category_batteryVoltage_Two    = "bike-battery-voltage-severity-two-push"
    static let category_batteryVoltage_Three  = "bike-battery-voltage-severity-three-push"
    static let category_alert_puncture        = "dm-alert-tyre-puncture-push"
    static let category_alert_no_fuel         = "dm-alert-no-fuel-push"
    static let category_alert_accident        = "dm-alert-accident-push"
    static let category_severe_condition      = "dm-alert-severe-condition-push"
    static let category_alert_response_to_origanator = "dm-alert-response-push"
    static let category_alert_delivered_to_receiver = "dm-alert-delivered-push"
    static let category_alert_ride_cluster_formation = "ride-cluster-formation-alert-push"
    static let category_alert_close            =  "dm-alert-close-push"
    static let ride_update_message = "ride-update-message"
}

struct NotificationIdentifier
{
    static let otpIdentifier      =   "otp_notification"
    static let contactIdentifier  =   "emergencyContact_notification"
    static let upgardeDmIdentifier = "UpgradeDMNotification"
}

//MARK: SQLITE DATABASE

struct SQliteDBConstants {
    static let KEY_ID = "id"
    static let KEY_RIDE_ID = "poiRideID"
    static let KEY_POI_NAME = "poiName"
    static let KEY_POI_PROVIDER = "poiProvider";
    static let KEY_POI_PLACE_ID = "poiPlaceID";
    static let KEY_POI_VICINITY = "poiVicinity";
    static let KEY_POI_RATING = "poiRating";
    static let KEY_POI_LATITUDE = "poiLatitude";
    static let KEY_POI_LONGITUDE = "poiLongitude";
    static let KEY_POI_COSLAT = "poiCosLatitude";
    static let KEY_POI_SINLAT = "poiSinLatitude";
    static let KEY_POI_COSLNG = "poiCosLongitude";
    static let KEY_POI_SINLNG = "poiSinLongitude";
    static let KEY_POI_ALTITUDE = "poiAltitude";
    static let KEY_POI_CATEGORIES = "poiCategories";
    static let KEY_POI_OPENING_HOURS = "poiOpeningHours";
    static let KEY_POI_PHONE_NUMBER = "poiPhoneNumber";
    static let KEY_POI_ADDRESS = "poiAddress";
}



public enum SQlitePoiCatagoryType: String {
    case KEY_POI_RESTAURANT     = "restaurant"
    case KEY_POI_GAS_STATION    = "gas_station"
    case KEY_POI_POLICE         = "police"
    case KEY_POI_HOSPITAL       = "hospital"
    case KEY_POI_HOTEL          = "hotel"
    
}


struct SQlitePlannedRoutes {
    static let KEY_RIDE_ID          = "poiRideID"
    static let KEY_RIDE_ROUTE       = "poiRideRooute"
    static let KEY_RIDE_WAYPOINTS   = "poiRideWaypoints"
    static let KEY_RIDE_TBT_ROUTE   = "poiRideTBTRoute"
}

struct SQliteReceiveAlert {
    static let KEY_ALERT_ID         = "alertID"
    static let KEY_RIDE_ID          = "rideID"
    static let KEY_UUID             = "uuid"
    static let KEY_ALERT_STATUS     = "alertStatus"
    
    static let KEY_ORIGANATED_BIKER_ID  = "origantedBikerId"
    static let KEY_ORIGANATED_BIKER_NAME = "origantedBikerName"
    static let KEY_ALERT_SHORT_NAME = "alertShortName"
    static let KEY_CATEGORY         = "alertCategory"
    static let KEY_PRIORITY         = "alertPriority"
    
    static let KEY_HELPED       = "alertHelped"
    static let KEY_LOCATION         = "alertLocation"
}

struct SQliteSendingAlert {
    
    static let KEY_ALERT_ID         = "alertID"
    static let KEY_TRIGGERED_BY_ID  = "alertTriggeredById"
    static let KEY_RIDE_ID          = "alertRideId"
    
    static let KEY_ALERT_SHORT_NAME = "alertShortName"
    static let KEY_CATEGORY         = "alertCategory"
    static let KEY_PRIORITY         = "alertPriority"
    
    static let KEY_ALERT_STATUS     = "alertStatus"
    
    static let KEY_HELP_RECEIVED    = "alertHelpReceived"
    
    static let KEY_RESPONDANT_BIKER_NAME       = "alertRespondantBikerName"
    
}


////***  ICON Image Names **********////
struct ImageConstant {
    
    static let ferry            = "ferry"
    static let ferry_train      = "ferry train"
    static let fork_left        = "fork left"
    static let fork_right       =  "fork right"
    static let keep_left        = "keep left"
    static let keep_right       = "keep right"
    static let merge            = "merge"
    static let ramp_left        = "ramp left"
    static let ramp_right       = "ramp right"
    static let roundabout_left  = "roundabout left"
    static let roundabout_right = "roundabout right"
    static let straight         = "straight"
    static let turn_left        = "turn left"
    static let turn_right       =  "turn right"
    static let turn_sharp_left  = "turn sharp left"
    static let turn_sharp_right = "turn sharp right"
    static let turn_slight_left = "turn slight left"
    static let turn_slight_right = "turn slight right"
    static let uturn_left       = "uturn left"
    static let uturn_right      = "uturn right"
    static let headNorth        = "HEADNORTH"
    static let headEast         = "HEADEAST"
    static let headSouth        =  "HEADSOUTH"
    static let headWest         = "HEADWEST"
    static let headNorthEast    = "HEADNORTHEAST"
    static let headNorthWest    = "HEADNORTHWEST"
    static let headSouthEast    = "HEADSOUTHEAST"
    static let headSouthWest    = "HEADSOUTHWEST"
    static let headNorthNorthEast   = "HEADNORTHNORTHEAST"
    static let headNorthNorthWest   = "HEADNORTHNORTHWEST"
    static let headEastNorthEast    = "HEADEASTNORTHEAST"
    static let headEastSouthEast    =  "HEADEASTSOUTHEAST"
    static let headSouthSouthEast   = "HEADSOUTHSOUTHEAST"
    static let headSouthSouthWest   = "HEADSOUTHSOUTHWEST"
    static let headWestSouthWest    = "HEADWESTSOUTHWEST"
    static let headWestNorthWest    = "HEADWESTNORTHWEST"
    static let roundabout_Enter     = "roundabout Enter"
    static let roundabout_Exit_Left = "roundabout Exit left"
    static let roundabout_Exit_Right  = "roundabout Exit Right"

}

////***  ICON Image Names **********////
struct ToastMsgConstant {
    static let network          = "Not Connected to Internet."
    static let noMsgForNetwork  = "⚠️ No message found"
    static let failPairingMsg   = "Pairing Not Successful, Please try again  "
    static let alertcamPermMsg  = "MaximusPro would like to access camera."
    static let alertcamPermInfo = "Go to settings?"
    static let alertReroute     =  "Please check the Network connection!"
    static let alertPermisReroute = "Are you sure you want to Re-route...?"
    static let alertPermisTitleRemoveBT = "Forget from Settings"
    static let alertPermisRemoveBT = "To completely unpair the Display Module, please forget the device from Bluetooth Settings"
}
//setup installation
enum InstallationGuide : Int{
        case DMInstallation = 0
        case DMUsage
        case CMInstallation
}

func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    #if DEBUG
        Swift.print(items[0], separator:separator, terminator: terminator)
    #endif
}

// Plan ride
struct PlanRideDataSource {
    static let SourcePlace = "Source"
    static let ViaPoint = "ViaPoint"
    static let DestinationPlace = "Destination"
}

struct TableViewCells {
    //TableView Cell Identifiers

    static let SourceDestinationCellIdentifier = "SourceDestinationTableViewCell"
    static let ViaPointsCell = "ViaPointsTableViewCell"
    static let MarkerView = "ViaPointMarkerView"
    static let CustomMarkerView = "MarkerCustomView"
    static let TutorialCell = "TutorialsCell"
    static let TutorialCollectionCell = "TutorialsCollectionViewCell"
    static let InstallationGuideCell = "InstallationGuideCollectionViewCell"
    static let MyRideCell = "MyRideCell"
    static let RouteCell = "RouteCell"
    static let hamburgerMenuCell = "MenuCell"
    static let viaPointCell = "ViaPointsCell"
    static let settingsTableViewCell = "SettingsTableViewCell"
    static let settingsContactUsCollectionViewCell = "ContactUsCollectionViewCell"
    //Table View Header Identifiers
    static let hamburgerHeaderMenuCell = "MenuHeaderView"
    static let footerCell = "PlanRideFooterTableViewCell"
}

struct ScreenTitle {
    static let RideOn = "RIDE ON"
    static let RideDeatils = "RIDE DETAILS"
    static let Ride = "RIDE"
}



enum HamburgerMenus: String {
    case RIDE_ON = "keyPlanYourRide"
    case CURRENT_RIDE = "keyCurrentRide"
    case RIDE_LOG = "keyRIDELOG"
    case ACHIEVEMENTS = "keyACHIEVEMENTS"
    case MY_BIKE = "keyMYBIKE"
    case MY_SUPPORT = "keySUPPORT"
    func localizedString() -> String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
}

enum BLEChar: Int {
    case otaModeChar = 0
    case otaNewImgTUContentChar
    case otaNewImgChar
    case RXCharacteristic
    case nonceChar
    case otaCRCChar
}

enum BleWriteData: Int {
    case bleWriteFailure = 0
    case bleWriteSuccess
}

enum DMInfo: String {
    case BuildNo = "BuildNo"
    case FirmwareVersion = "FirmwareVersion"
    case SerialNo = "SerialNo"
}

struct RideInProgressStatus {
    static let notStarted = 0
    static let inProgress = 1
    static let finished = 2
    static let cancelled = 3
    static let deleted = 4
    static let aborted = 5
}

enum RequestType: String {
    case Create = "Create"
    case Start = "Start"
    case Stop = "Stop"
}

enum PlanRideFlowOptions: Int {
    case PlanRide = 0
    case EditRide
    case RecommendedRide
}

struct URLRequestType {
    static let GetPlannedRides = "GetPlannedRides"
    static let GetRideStats = "GetRideStats"
    static let GetRideRouteById = "GetRideRouteById"
    static let GetCompletedRideRoute = "GetCompletedRideRoute"
    static let GetRideMembers = "GetRideMembers"
    static let CreateRide = "CreateRide"
    static let GetRecommendedRides = "GetRecommendedRides"
    static let GetRideOfBiker = "GetRideOfBiker"
}
