//
//  LoadingSpinnerHandler.swift
//  Maximus
//
//  Created by Admin on 18/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import UIKit

class LoadingHandler {
    
    let instanceLoader: LoadingOverlay = LoadingOverlay()
    class var shared: LoadingHandler {
        struct Static {
            static let instance: LoadingHandler = LoadingHandler()
        }
        return Static.instance
    }
    
    public  func startloading(interactionEnable:Bool, view:UIView){
        self.instanceLoader.showOverlay(interactionEnable: interactionEnable ,view:view)
    }
    
    public  func stoploading(){
        self.instanceLoader.hideOverlayView()
    }
}
