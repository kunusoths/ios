//
//  MapTheme.swift
//  Maximus
//
//  Created by Admin on 14/11/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//

import Foundation
import GoogleMaps

class MapTheme: NSObject {
    static let shared = MapTheme()
    
    override init() {
        super.init()
        
    }
    
    
    //// Comment: Default Mapstyle is Standared so no need to set that.
    
    func setTheme(map: GMSMapView) {
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "Silver", withExtension: "json") {
                map.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
}
