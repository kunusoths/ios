//
//  PopUpUtility.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 14/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class PopUpUtility: NSObject {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    func showDefaultPopUpWith(message:String){
        let alert = UIAlertController(title: "MaximusPro", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
        
    }
}
