//
//  Analytics.swift
//  Maximus
//
//  Created by Isha Ramdasi on 19/03/19.
//  Copyright © 2019 com.maximuspro.com. All rights reserved.
//

import Foundation
import Crashlytics

class Analytics{
    class func logCustomEvent(withName eventName:String,customAttributes attributes:[String:Any]){
        var name = "staging_" + eventName
        #if CLOUD_SERVER_STAGING
            name = "staging_" + eventName
        #elseif CLOUD_SERVER_PRODUCTION
            name = "prod_" + eventName
        #endif
        Answers.logCustomEvent(withName: name, customAttributes: attributes)
    }
}
