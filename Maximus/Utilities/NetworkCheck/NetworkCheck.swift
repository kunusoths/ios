//
//  File.swift
//  Maximus
//
//  Created by Namdev Jagtap on 18/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import Alamofire

class NetworkCheck {
    
    //Wrapper for internet Check
   class func isNetwork()->Bool{
        if (NetworkReachabilityManager()?.isReachable)! {
            return true
        }else{
            return false
        }
    }
}
