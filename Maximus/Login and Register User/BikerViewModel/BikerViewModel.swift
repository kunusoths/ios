//
//  BikerViewModel.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 02/07/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import Foundation
import Crashlytics
import UIKit

@objc protocol BikerViewModelDelegate {
    
    func bikerRegistrationSuccess()
    func authenticationFail(data: [String : Any])
}

class BikerViewModel: NSObject , BikerRepositoryDelegate {

    private let bikerRepo = BikerRepository()
    weak var bikerDelegate: BikerViewModelDelegate?
    var loginMethod: LoginMethod = .NONE
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    let imgUploaderObj = ImageUploader()

    override init() {
        //
        print("BikerViewModel init ")
        super.init()
        bikerRepo.bikerRepoDelegate = self
    }
    
    deinit {
        print("BikerViewModel deinit ")
    }
    
    //MARK:- BikerRepository Calls
    
    func registerWithFacebook(viewController: UIViewController) {
        print("BikerViewModel Register with Facebook ")
        self.loginMethod = .FACEBOOK
        bikerRepo.loginWithFacebook(viewController: viewController)
    }
    
    func registerUser(loginMethod:LoginMethod, biker:[String:Any]) {
        print("BikerViewModel Register user ")
        self.loginMethod = loginMethod
        bikerRepo.registerUser(loginMethod: loginMethod, user: biker)
    }
    
    func authenticateUser(loginMethod:LoginMethod, biker:[String:Any]){
        // Call Repositoery method
        self.loginMethod = loginMethod
        bikerRepo.authenticateUser(loginMethod: loginMethod, user: biker)
    }
    
    func getBiker(bikerID: Int) {
        if NetworkCheck.isNetwork(){
        bikerRepo.repoGetBiker(bikerId: bikerID)
        }else{
            
        }
    }
    
    func getActiveDeviceMapping(vehicle: Int)  {
        bikerRepo.repoGetActiveDeviceMapping(vehicleID: vehicle)
    }
    
    func updateBikersDetails() {
        
        if MyDefaults.getData(key: DefaultKeys.KEY_FIRST_NAME) as? String == "" {
            MyDefaults.setData(value: MyDefaults.getData(key: DefaultKeys.KEY_USERNAME) as! String , key: DefaultKeys.KEY_FIRST_NAME)
        }
        
        if self.loginMethod == .FACEBOOK {

            let json : [String:Any] = [
            
                "id": (BikerDetails.getBikerDetails(context: context)?.biker_id?.toInt())! ,
                "first_name": MyDefaults.getData(key: DefaultKeys.KEY_FIRST_NAME),
                "last_name": MyDefaults.getData(key: DefaultKeys.KEY_LAST_NAME),
                "profile_url": (BikerDetails.getBikerDetails(context: context)?.profile_image_url)!,
                "zone_offset": TimeZone.current.abbreviation()!]
            print("Update Biker \(json)")
            self.bikerRepo.repoUpdateBiker(parameter: json)
        } else {
            let json : [String:Any] = [
                "id": BikerDetails.getBikerDetails(context: context)?.biker_id?.toInt() ?? 0 ,
                "first_name": BikerDetails.getBikerDetails(context: context)?.first_name ?? "",
                "last_Name": BikerDetails.getBikerDetails(context: context)?.last_name ?? "",
                "profile_url": BikerDetails.getBikerDetails(context: context)?.profile_image_url ?? "",
                "zone_offset": TimeZone.current.abbreviation()!]
            print("Update Biker \(json)")
            self.bikerRepo.repoUpdateBiker(parameter: json)
        }
    }
    
    //MARK:- BikerRepositoryDelegate methods
    
    
    func bikerRepoResponse(response: [String : Any]) {
        //
        
        let type:BikerAPI = response["type"] as! BikerAPI
        
        switch type {
        case .REGISTER_USER:
            let userData = response["bikerData"] as! [String:Any]
            self.repoRegisterUserSuccess(data: userData)
            break
            
        case .AUTHENTICATE_USER:
            let authData:AuthenticationModel = response["data"] as! AuthenticationModel
            self.repoAuthenticationSuccess(user: authData)
            break
            
        case .GET_BIKER:
            
            let biker:BikerDetails = response["bikerData"] as! BikerDetails
            self.repoGetBikerSuccess(biker: biker)
            break
        
        case .ACTIVE_DEVICE:
            
            let mapped:Bool = response["isMapped"] as! Bool
            let model:ActiveDevicesModel = response["data"] as! ActiveDevicesModel
            
            self.repoDeviceMapped(mapped: mapped, device: model)
            break
        
        case .UPDATE_BIKER:
            if let bikerDetails = BikerDetails.getBikerDetails(context: self.context),self.loginMethod == .FACEBOOK  {
                let urlData = response["data"] as! [String:String]
                if urlData["default_biker_profile_image_url"] != bikerDetails.profile_image_url{
                    //case when cloud profile picture url is not same as default profile picture url
                    MyDefaults.setData(value: bikerDetails.profile_image_url!, key: DefaultKeys.KEY_PROFILE_URL)
                }
                else{
                    //uploading image only happens when user logins with facebook first time
                    self.uploadImage()
                }
            }
             break
        default:
            break
        }
        
        
        
    }
    
    
    func repoRegisterUserSuccess(data: [String : Any]) {
        print("BikerViewModel Register Success ")
        if self.loginMethod == LoginMethod.FACEBOOK {
            self.authenticateUser(loginMethod: self.loginMethod, biker: data)
        }else{
            var authenticateParameter: [String: Any] = [:]
            authenticateParameter[StringConstant.rememberMe] = "true"
            authenticateParameter[StringConstant.username] = data[StringConstant.email]
            authenticateParameter[StringConstant.password] = data[StringConstant.password]
            self.authenticateUser(loginMethod: self.loginMethod, biker: authenticateParameter)
        }
    }
    
    func repoAuthenticationSuccess(user: AuthenticationModel) {
        
        MyDefaults.setInt(value: user.id!, key: DefaultKeys.KEY_BIKERID) // to be removed
        MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_ISLOGIN)//-----Save Login Session
        self.getBiker(bikerID: user.id!)
    }
    
    func uploadImage(){
        
        let url = MyDefaults.getData(key: DefaultKeys.KEY_PROFILE_URL) as! String
        let imageData =  UIImagePNGRepresentation(UIImage(urlString: url)!)
        
        let uploadUrl = "\(RequestURL.URL_BASE.rawValue)\(RequestURL.URL_BIKER.rawValue)\(RequestURL.URL_PROFILE_IMAGE.rawValue)"
        
        imgUploaderObj.uploadImage(data: imageData!, url: uploadUrl, successCallback: { (data) in
            //
            print("Profile Image uploaded response: \(data)")
        }) { (error) in
            //
            print("Profile Image upload failed  response: \(error)")
        }
    }
    
    func repoGetBikerSuccess(biker: BikerDetails) {
        let username = (biker.first_name ?? "") + " " + (biker.last_name ?? "")
        MyDefaults.setData(value: username, key: DefaultKeys.KEY_USERNAME)
        let strUserIDCrash = "Biker ID: " + String(describing: biker.biker_id)
        Crashlytics.sharedInstance().setUserIdentifier(strUserIDCrash)
        Crashlytics.sharedInstance().setUserEmail(biker.email)
        Crashlytics.sharedInstance().setUserName((MyDefaults.getData(key: DefaultKeys.KEY_USERNAME) as! String))
        self.getActiveDeviceMapping(vehicle: (biker.default_vehicle_id.toInt())!)
//        if MyDefaults.getData(key: DefaultKeys.KEY_PROFILE_URL) as? String == "" { // to be removed
        MyDefaults.setData(value: biker.profile_image_url!, key: DefaultKeys.KEY_PROFILE_URL)
//        }
        self.updateBikersDetails()
    }
    
    func repoDeviceMapped(mapped: Bool, device: ActiveDevicesModel) {
        
        if mapped {
            
            if device.device_unit_id != nil {
                MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_CMREGISTER)//-----Save CM Registration Status
                MyDefaults.setData( value: device.device_unit_id!, key: DefaultKeys.KEY_CMSERIAL_NO)
                if device.isVirtualCm != nil {
                    MyDefaults.setBoolData(value: device.isVirtualCm!, key: DefaultKeys.IS_CM_VIRTUAL)
                }
                
            }
            
            self.bikerDelegate?.bikerRegistrationSuccess()
            let contextt = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
            do{
                let dmDetailsArray = try contextt.fetch(DMDetails.fetchRequest())
                if dmDetailsArray.count > 0 {
                    if let dmObj = dmDetailsArray.first as? DMDetails {
                        dmObj.sr_no = device.display_module_sr_no
                        dmObj.isSynced = false
                        do {
                            try contextt.save()
                        } catch {
                            
                        }
                    }
                }
            } catch {
                
            }
        }else {
            MyDefaults.setBoolData(value: false, key: DefaultKeys.KEY_CMREGISTER)//-----Save CM Registration Status
            MyDefaults.setBoolData(value: false, key: DefaultKeys.KEY_DMREGISTER)//-----Save DM Registration Status
            self.bikerDelegate?.bikerRegistrationSuccess()
        }
        
    }
    
    func repoRegisterUserFail(data: [String : Any]) {
        print("BikerViewModel Register Uset Failed ")
        self.bikerDelegate?.authenticationFail(data: data)
        //
    }
    
    func repoAuthenticationFail(data: [String : Any]) {
        //
        self.bikerDelegate?.authenticationFail(data: data)
    }
    
}

