//
//  BikerRepository.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 02/07/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import Foundation
import CoreData
import UIKit

enum LoginMethod:String {
    case FACEBOOK = "facebook"
    case USERNAME_PASSWORD = "username_password"
    case NONE = "none"
}

enum AUTH_METHOD:String {
    case USERNAME_PASSWORD = "0"
    case FACEBOOK          = "1"
    case G_PLUS            = "2"
    case TWITTER           = "3"
    case UNKNOWN           = "999"
}


protocol BikerRepositoryDelegate:class {
    //
    func repoAuthenticationFail(data:[String:Any])
    func bikerRepoResponse(response:[String:Any])
}

class BikerRepository: NSObject, BikerAPIDelegate {
    
    let apiHnadler = BikerAPIHandler()
    
    weak var bikerRepoDelegate:BikerRepositoryDelegate?
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    var userLoginMethod:LoginMethod = .NONE
    
    override init() {
        super.init()
        print("BikerRepository init ")
        apiHnadler.bikerAPIDelegate = self
    }
    
    deinit {
         print("BikerRepository dinit ")
    }
    
    //MARK:- Facebook Login
    
    func loginWithFacebook(viewController: UIViewController) {
        let objFB = SocialMediaFactory.SocialLoginService(for: .facebook , delegate: self)
        objFB?.setDelegate(delegate: self)
        objFB?.login(viewcontroller: viewController)
    }
    
    
    //MARK:- Register and Authenticate User
    
    //Register a new user
    func registerUser(loginMethod:LoginMethod , user:[String: Any]) {
        self.userLoginMethod = loginMethod
        apiHnadler.callRegisterUser(loginMethod: loginMethod, user: user)
    }
    
    // Authenticate Existing user
    func authenticateUser(loginMethod:LoginMethod , user:[String: Any]) {
        self.userLoginMethod = loginMethod
        apiHnadler.callAuthenticateUser(loginMethod: loginMethod, user: user)
    }
    
    func repoGetBiker(bikerId: Int)  {
        if NetworkCheck.isNetwork() {
            self.apiHnadler.apiGetBiker(bikerID: bikerId)
        }else{
            //            self.bikerRepoDelegate?.repoGetBikerSuccess(biker: )
            if let bikerDetails = BikerDetails.getBikerDetails(context: context) {
                let bikerData:[String:Any] = ["bikerData" : bikerDetails,
                                              "type": BikerAPI.GET_BIKER]
                self.bikerRepoDelegate?.bikerRepoResponse(response: bikerData)
            }
            
        }
    }
    
    func repoGetActiveDeviceMapping(vehicleID: Int){
        self.apiHnadler.apiGetActiveDeviceMapping(vehicleID: vehicleID)
    }
    
    func repoUpdateBiker(parameter: [String: Any]){
        self.apiHnadler.updateBiker(parameter: parameter)
    }

    
    //MARK:- BikerAPIDelegate Methods
    
    func bikerAPISuccess(response: [String : Any]) {
        let type:BikerAPI = response["type"] as! BikerAPI
        
        switch type {
        case .REGISTER_USER:
            self.bikerRepoDelegate?.bikerRepoResponse(response: response)
            break
            
        case .AUTHENTICATE_USER:
            let authDetails: AuthenticationModel = response["data"] as! AuthenticationModel
            BikerDetails.createBiker(context: self.context, id: (authDetails.id?.toString())!)
            self.bikerRepoDelegate?.bikerRepoResponse(response: response)
            break
        
        case .GET_BIKER:
            let biker:Biker = response["data"] as! Biker
            if let id = biker.default_vehicle?.id {
                MyDefaults.setInt ( value: id, key: DefaultKeys.KEY_DefaultVehicleID)
            }
            BikerDetails.updateBiker(context: context, biker: biker)
            BikerDetails.addBikerVehicle(context:context, vehicle: biker.default_vehicle!)
            
            if let bikerDetails = BikerDetails.getBikerDetails(context: context) {
                if MyDefaults.getData(key: DefaultKeys.KEY_FBACCESSTOKEN) as! String != "" {
                    //calling this only in case of facebook user
                    self.apiHnadler.updateFbProfilePicToCloud()
                }
                let bikerData:[String:Any] = ["bikerData" : bikerDetails,
                                              "type": BikerAPI.GET_BIKER]
                self.bikerRepoDelegate?.bikerRepoResponse(response: bikerData)
            }
            break
            
        case .ACTIVE_DEVICE:
            let device:ActiveDevicesModel = response["data"] as! ActiveDevicesModel
            BikerDetails.udpateDeviceModel(context: context, activeDevice:device)
            self.bikerRepoDelegate?.bikerRepoResponse(response: response)
            break
        
        case .UPDATE_BIKER:
            if let biker:Biker = response["data"] as? Biker{
                BikerDetails.updateBiker(context: context, biker: biker)
            }else{
                self.bikerRepoDelegate?.bikerRepoResponse(response: response)
            }
            break
            
        default:
            break
        }
    }
    
    
    func authenticationFailed(data: [String : Any]) {
        self.bikerRepoDelegate?.repoAuthenticationFail(data: data)
    }
}


extension BikerRepository: SNSResponseDelegate {
    
    func onSocialSuccess(objModel: SocialNetworkModel) {
        //
        let dic = [StringConstant.accessToken :objModel.socialAccesToken,
                   StringConstant.ID          :objModel.socialID,
                   StringConstant.username    :objModel.email]
        self.authenticateUser(loginMethod: .FACEBOOK, user: dic)
    }
    
    func onSocialFailure(response: Error) {
        //
    }
    
}
