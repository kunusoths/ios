//
//  BikerAPIHandler.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 10/07/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import Foundation
import ObjectMapper


enum BikerAPI: String {
    
    case DEFAULT = "Default"
    case REGISTER_USER = "RegisterUser"
    case AUTHENTICATE_USER = "AuthenticateUser"
    case GET_BIKER = "GetBiker"
    case ACTIVE_DEVICE = "ActiveDevice"
    case UPDATE_BIKER = "UpdateBiker"
}

protocol BikerAPIDelegate: class {
    func authenticationFailed(data:[String:Any])
    func bikerAPISuccess(response: [String:Any])
    
}

class BikerAPIHandler {
    
    init() {
        print("BikerAPIHandler init ")
    }
    
    deinit {
         print("BikerAPIHandler Dinit ")
    }
    
    let cloudHandler = CloudHandler()
    
    //MARK:- Class Parameters
    weak var bikerAPIDelegate:BikerAPIDelegate?
    
    //MARK:- Class Methods
    func callRegisterUser(loginMethod: LoginMethod, user:[String: Any]) {
        print("Register Metadata: \(user) , \(loginMethod.rawValue)")
        let json:NSString = user.jsonEncode()
        let parameter:[String:Any] = [StringConstant.auth_method: loginMethod.rawValue,
                                      StringConstant.auth_metadata: json]
        
        let urlRegister =  RequestURL.URL_REGISTER.rawValue
        
        self.cloudHandler.makeCloudRequest(subUrl: urlRegister, parameter: parameter, isheaderAUTH: false, requestType: .post){ response in
            
            if let json = response?.result.value {
                print("Register Response : \(json)")
                let statusCode = response?.response?.statusCode
                
                if (statusCode)! >= 200 && (statusCode)! < 300
                {
                    if loginMethod == LoginMethod.FACEBOOK {
                        
                       // self.bikerAPIDelegate?.registerUserSuccess(loginMethod: loginMethod, data: user)
                        
                        let data:[String:Any] = ["type" : BikerAPI.REGISTER_USER,
                                                "loginMethod": loginMethod,
                                                "bikerData": user]
                        self.bikerAPIDelegate?.bikerAPISuccess(response: data)
                    }else{
                       // self.bikerAPIDelegate?.registerUserSuccess(loginMethod: loginMethod, data: user)
                        
                        let data:[String:Any] = ["type" : BikerAPI.REGISTER_USER,
                                                 "loginMethod": loginMethod,
                                                 "bikerData": user]
                        self.bikerAPIDelegate?.bikerAPISuccess(response: data)
                    }
                }
                else{
                    // handle user resgister fail with status code
                    self.errorCall(value: json as! [String : Any])
                   
                }
            }else{
                // handle result value is nil
                
                 self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    func callAuthenticateUser(loginMethod: LoginMethod, user:[String: Any]) {
        
        var parameter:[String:String] = [:]
        if loginMethod == .FACEBOOK {
            parameter = [StringConstant.subject_token_type: StringConstant.tokentype_accesstoken,
                         StringConstant.grantType:StringConstant.granttype_tokenexchange,
                         StringConstant.clientId:StringConstant.oauth_client_id,
                         StringConstant.clientSecret:KeychainService.shared[KEY_OAUTH.KEY_NAME_OAUTH_CLIENT_SECRET]!,
                         StringConstant.subject_token:MyDefaults.getData(key: DefaultKeys.KEY_FBACCESSTOKEN) as! String,
                         StringConstant.subject_issuer:loginMethod.rawValue,
                         StringConstant.scope:StringConstant.offline_access]
        }else{
            parameter = [StringConstant.auth_method: loginMethod.rawValue,
                         StringConstant.grantType:StringConstant.password,
                         StringConstant.clientId:StringConstant.oauth_client_id,
                         StringConstant.clientSecret:KeychainService.shared[KEY_OAUTH.KEY_NAME_OAUTH_CLIENT_SECRET]!,
                         StringConstant.username:user[StringConstant.username] as! String,
                         StringConstant.password:user[StringConstant.password] as! String,
                         StringConstant.scope:StringConstant.offline_access]
        }
        
        
        let urlAuthentication =  RequestURL.URL_AUTHENTICATION.rawValue
        
        self.cloudHandler.makeCloudRequest(subUrl: urlAuthentication, parameter: parameter, isheaderAUTH: false, requestType: .post){ response in
            
            if let json = response?.result.value,let statusCode = response?.response?.statusCode {
                
                if (statusCode) >= 200 && (statusCode) < 300
                {
                    var json1 = json as! [String : Any]
                    json1[StringConstant.id_token] = json1[StringConstant.accessToken]
                    json1[StringConstant.refreshToken] = json1[StringConstant.refreshToken]
                    let authModel = Mapper<AuthenticationModel>().map(JSON:json1)!
                    // self.bikerAPIDelegate?.authenticationSuccess(user: user)
                    MyDefaults.setData( value: json1[StringConstant.accessToken] as! String, key: DefaultKeys.KEY_TOKEN)
                    MyDefaults.setData(value: json1[StringConstant.refreshToken] as! String, key: DefaultKeys.KEY_REFRESH_TOKEN)
                    self.apiGetAccount()
                    self.apiGetBikerId(authModel)
                    
                }else if statusCode == 400 || statusCode == 401 {
                    self.errorCall(value: json as! [String : Any])
                }
            }else{
                //handle no response
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    //api for migration from 1.3 to 1.5 getAccount creates biker on cloud if not present
    func apiGetAccount(){
        let suburl = "\(RequestURL.URL_ACCOUNT.rawValue)"
        self.cloudHandler.makeCloudRequest(subUrl: suburl, parameter: [:], isheaderAUTH: true, requestType: .get) { (response) in
            if let json = response?.result.value {
                let statusCode = response?.response?.statusCode
                print("getAccount Sucess json : \(json)")
                if (statusCode)! >= 200 && (statusCode)! < 300
                {
                   //no need of handling success message
                    //this request is only for creating biker on cloud side
                } else {
                    // Failed
                }
            }else{
                //json absent
            }
        }
    }
    
     //api for getting bikerid with oauth access token
    func apiGetBikerId(_ authModel:AuthenticationModel){
        let suburl = "\(RequestURL.URL_BIKER_OAUTH.rawValue)"
        self.cloudHandler.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ response in
            
            if let json = response?.result.value {
                let statusCode = response?.response?.statusCode
                print("getBiker Sucess json : \(json)")
                if (statusCode)! >= 200 && (statusCode)! < 300
                {
                    let biker = Mapper<Biker>().map(JSON:json as! [String : Any])!
                    authModel.id = biker.id
                    // self.bikerAPIDelegate?.authenticationSuccess(user: user)
                    
                    let data:[String:Any] = ["type": BikerAPI.AUTHENTICATE_USER,
                                             StringConstant.data: authModel]
                    
                    self.bikerAPIDelegate?.bikerAPISuccess(response: data)
                    
                } else {
                    // Failed
                }
            }else{
                //json absent
            }
        }
    }
    
    
    func apiGetBiker(bikerID:Int)  {
        
        let suburl = "\(RequestURL.URL_BIKER_OAUTH.rawValue)"
        self.cloudHandler.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ response in
            
            if let json = response?.result.value {
                let statusCode = response?.response?.statusCode
                print("getBiker Sucess json : \(json)")
                if (statusCode)! >= 200 && (statusCode)! < 300
                {
                    let biker = Mapper<Biker>().map(JSON:json as! [String : Any])!
                    let data:[String:Any] = [StringConstant.data: biker,
                                             "type": BikerAPI.GET_BIKER]
                    self.bikerAPIDelegate?.bikerAPISuccess(response: data)
                    //self.bikerAPIDelegate?.getBikerSuccess(biker: biker)
                } else {
                    // Failed
                }
            }else{
                //json absent
            }
        }
    }
    
    //for updating facebook profile picture to cloud
    func updateFbProfilePicToCloud(){
        self.cloudHandler.makeCloudRequest(subUrl: RequestURL.URL_DEFAULTIMG.rawValue, parameter: [:], isheaderAUTH: true, requestType: .get) { (response) in
            if let json = response?.result.value {
                let statusCode = response?.response?.statusCode
                if (statusCode)! >= 200 && (statusCode)! < 300
                {
                    let dict = json as! [String:String]
                    let data:[String:Any] = ["data": dict,
                                             "type": BikerAPI.UPDATE_BIKER]
                    self.bikerAPIDelegate?.bikerAPISuccess(response: data)
                    
                    
                } else {
                    // Failed
                }
            }else{
                //json absent
            }
        }
    }
    
    func apiGetActiveDeviceMapping(vehicleID: Int){
        
        self.cloudHandler.makeCloudRequest(subUrl:"\(RequestURL.URL_VEHICLES.rawValue)\(vehicleID)\(RequestURL.URL_ACTIVE_DEVICE_MAPPING.rawValue)", parameter:["":""], isheaderAUTH: true, requestType: .get)
        {
            response in
            
            print(response?.request?.url ?? "")
            
            if let json = response?.result.value {
                print("ActiveDeviceMapping : \(json)")
                if response?.response?.statusCode == 200
                {
                    let userObj = Mapper<ActiveDevicesModel>().map(JSON:response?.result.value as! [String : Any])!
                    
                    let data:[String: Any] = ["type" : BikerAPI.ACTIVE_DEVICE,
                                              "data": userObj,
                                              "isMapped": true]
                    self.bikerAPIDelegate?.bikerAPISuccess(response: data)
                    //self.bikerAPIDelegate?.deviceIsMapped(mapped: true, device: userObj)
                }
                else
                {
                    let userObj = Mapper<ActiveDevicesModel>().map(JSON:response?.result.value as! [String : Any])!
                    let data:[String: Any] = ["type" : BikerAPI.ACTIVE_DEVICE,
                                              "data": userObj,
                                              "isMapped": true]
                    self.bikerAPIDelegate?.bikerAPISuccess(response: data)
                    //self.bikerAPIDelegate?.deviceIsMapped(mapped: false, device: userObj)
                }
            }else{
                
            }
        }
    }
    
    func updateBiker(parameter:[String:Any])  {
        
        self.cloudHandler.makeCloudRequest(subUrl: RequestURL.URL_BIKER.rawValue, parameter: parameter, isheaderAUTH: true, requestType: .put) { response in
            
            if let json = response?.result.value {
                let statusCode = response?.response?.statusCode
                print("Update Biker Res JSON \(json)")
                if (statusCode)! >= 200 && (statusCode)! < 300
                {
                    print("Update Biker Success")
                    let biker:Biker = Mapper<Biker>().map(JSON:json as! [String : Any])!
                    
                    let data:[String:Any] = ["data": biker,
                                            "type": BikerAPI.UPDATE_BIKER]
                    self.bikerAPIDelegate?.bikerAPISuccess(response: data)
                    
                } else {
                    //self.errorCall(value: json as! [String : Any])
                }
            }else{
                // self.errorCall(value: RequestTimeOut().json)
            }
            
        }
    }
    
    func errorCall(value:[String : Any]!){
        let ErrorModelObj = Mapper<ErrorModel>().map(JSON:value)
        let errorData:[String:Any] = [StringConstant.data:ErrorModelObj ?? " "]
        self.bikerAPIDelegate?.authenticationFailed(data: errorData)
    }
    
}
