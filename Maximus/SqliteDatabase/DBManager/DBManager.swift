//
//  DatabaseMainManager.swift
//  SQliteExample
//
//  Created by Namdev Jagtap on 06/06/17.
//  Copyright © 2017 Namadev Jagtap. All rights reserved.
//
import SQLite
import Foundation

//: # Making it Swift

protocol SQLTable {
    static var createStatement: String { get }
}

//: ## Errors
enum SQLiteError: Error {
    case OpenDatabase(message: String)
    case Prepare(message: String)
    case Step(message: String)
    case Bind(message: String)
}

//MARK:********** URL Constants *************
public enum SQLTableType: String {
    case POINT_OF_INTEREST   = "PointsOfInterests"
    case PLANNED_ROUTE       = "plannedRoute"
    case RECEIVE_ALERT       = "receiveAlerts"
    case SENDING_ALERT       = "sendingAlerts"
}


//: ## The Database Connection
class DBManager {
    
    
    let dbPointer: OpaquePointer?
    
    private init(dbPointer: OpaquePointer) {
        self.dbPointer = dbPointer
    }
    
    static func open(path: String) throws -> DBManager {
        var db: OpaquePointer? = nil
        
        if sqlite3_open(path, &db) == SQLITE_OK {
            return DBManager(dbPointer: db!)
        } else {
            defer {
                if db != nil {
                    sqlite3_close(db)
                }
            }
            
            if let message: String? = String(cString: sqlite3_errmsg(db)!) {
                throw SQLiteError.OpenDatabase(message: message!)
            } else {
                throw SQLiteError.OpenDatabase(message: "No error message provided from sqlite.")
            }
        }
    }
    
    
    var errorMessage: String {
        if let errorMessage: String? = String(cString: sqlite3_errmsg(dbPointer)!) {
            return errorMessage!
        } else {
            return "No error message provided from sqlite."
        }
    }
    
    func prepareStatement(sql: String) throws -> OpaquePointer {
        var statement: OpaquePointer? = nil
        
        
        guard sqlite3_prepare_v2(dbPointer, sql, Int32(sql.length), &statement, nil) == SQLITE_OK else {
            throw SQLiteError.Prepare(message: errorMessage)
        }
        
        // print(sql)
        return statement!
    }
    
    func createTable(table: SQLTable.Type) throws {
        // 1
        let createTableStatement = try prepareStatement(sql: table.createStatement)
        
        print(createTableStatement)
        print(createTableStatement.debugDescription)
        
        // 2
        defer {
            sqlite3_finalize(createTableStatement)
        }
        // 3
        guard sqlite3_step(createTableStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: errorMessage.description)
        }
        print("\(table) table created.")
    }
    
    //MARK: ******* INSERT *******
    
    func insertIntoTable(table:SQLTableType,queryString:String,data:Any){
        
        switch table {
        case SQLTableType.POINT_OF_INTEREST:
            DispatchQueue.global(qos: .background).async {
                do{
                    try PointsOfInterests().insertIntoTable(insertSql: queryString, data: data)
                }
                catch let message
                {
                    //print(message)
                }
            }
           
            break
            
        case SQLTableType.PLANNED_ROUTE:
            DispatchQueue.global(qos: .background).async {
                
                do{
                    try PlannedRoute().insertIntoTable(insertSql: queryString, data: data)
                }
                catch let message
                {
                    // print(message)
                }
            }
            break
            
        case SQLTableType.RECEIVE_ALERT:
            DispatchQueue.global(qos: .background).async {
                do{
                    try ReceiveAlert().insertIntoTable(insertSql: queryString, data: data)
                }
                catch let message
                {
                    //print(message)
                }
            }
            break
            
        case SQLTableType.SENDING_ALERT:
            DispatchQueue.global(qos: .background).async {
                do{
                    try SendingAlert().insertIntoTable(insertSql: queryString, data: data)
                }
                catch let message
                {
                    //print(message)
                }
            }
            break
            
        default:
            break
        }
    }
    
    
    
    //MARK: ******* DELETE *******
    
    func deleteTable(table:SQLTableType,queryString:String)  {
        
        switch table {
        case SQLTableType.POINT_OF_INTEREST:
            DispatchQueue.global(qos: .background).async {
                
                do{
                    try PointsOfInterests().deleteTable(deleteSql: queryString)
                }
                catch let message
                {
                    //print(message)
                }
            }
            break
        case SQLTableType.PLANNED_ROUTE:
            DispatchQueue.global(qos: .background).async {
                
                do{
                    try PlannedRoute().deleteTable(deleteSql: queryString)
                }
                catch let message
                {
                    //print(message)
                }
            }
            break
        case SQLTableType.RECEIVE_ALERT:
            DispatchQueue.global(qos: .background).async {
                do{
                    try PlannedRoute().deleteTable(deleteSql: queryString)
                }
                catch let message
                {
                    //print(message)
                }
            }
            break
        case SQLTableType.SENDING_ALERT:
            DispatchQueue.global(qos: .background).async {
                
                do{
                    try PlannedRoute().deleteTable(deleteSql: queryString)
                }
                catch let message
                {
                    //print(message)
                }
            }
            break
            
        default:
            break
            
        }
    }
    
    func deleteTableRecordWithRespID(table:SQLTableType,queryString:String,id: Int32)  {
        
        switch table {
        case SQLTableType.POINT_OF_INTEREST:
            DispatchQueue.global(qos: .background).async {
                
                do{
                    try PointsOfInterests().deleteAllPOIWithRespectiveRideID(deleteSql: queryString, rideId: id)
                }
                catch let message
                {
                    // print(message)
                }
            }
            break
        case SQLTableType.PLANNED_ROUTE:
            DispatchQueue.global(qos: .background).async {
                
                do{
                    try PlannedRoute().deleteRouteWithRespectiveRideID(deleteSql: queryString, rideId: id)
                }
                catch let message
                {
                    // print(message)
                }
            }
            break
            
        case SQLTableType.RECEIVE_ALERT:
            DispatchQueue.global(qos: .background).async {
                
                do{
                    try ReceiveAlert().deleteAllRecevieAlertsWithRespectiveAlertID(deleteSql: queryString, alertId: id)
                }
                catch let message
                {
                    //print(message)
                }
            }
            break
            
        case SQLTableType.SENDING_ALERT:
            DispatchQueue.global(qos: .background).async {
                
                do{
                    try SendingAlert().deleteAllSendingAlertsWithRespectiveAlertID(deleteSql: queryString, alertID: id)
                }
                catch let message
                {
                    // print(message)
                }
            }
            break
            
        default:
            break
            
        }
        
    }
    
    //MARK: ******* UPDATE *******
    
    func updateIntoTable(table:SQLTableType,queryString:String,data:Any) {
        
        switch table {
        case SQLTableType.POINT_OF_INTEREST:
            DispatchQueue.global(qos: .background).async {
                
                do{
                    let poi = data as! PointsOfInterests
                    //  print("placeID - ",poi.poiPlaceID)
                    try PointsOfInterests().insertIntoTable(insertSql: queryString, data: data)
                }
                catch let message
                {
                    //print(message)
                }
            }
            break
        case SQLTableType.PLANNED_ROUTE:
            DispatchQueue.global(qos: .background).async {
                
                do{
                    try PlannedRoute().updateIntoTable(insertSql: queryString, data: data)
                }
                catch let message
                {
                    //print(message)
                }
            }
            break
            
            
        case SQLTableType.RECEIVE_ALERT:
            DispatchQueue.global(qos: .background).async {
                
                do{
                    try ReceiveAlert().updateReceivedAlertStatus(insertSql: queryString, data: data)
                }
                catch let message
                {
                    //print(message)
                }
            }
            break
            
        case SQLTableType.SENDING_ALERT:
            DispatchQueue.global(qos: .background).async {
                
                do{
                    
                    if (DBQuery.QUERY_UPDATE_SENDING_ALERT_HELPER_BIKER == queryString){
                        try SendingAlert().updateSentAlertRespondant(insertSql: queryString, data: data)
                    }
                    else{
                        try SendingAlert().updateSentAlertStatus(insertSql: queryString, data: data)
                        
                    }
                    
                }
                catch let message
                {
                    //print(message)
                }
            }
            break
            
        default:
            break
        }
        
    }
    
    //MARK: ******* FETCH *******
    
    func fetchAllTableRecord(table:SQLTableType,queryString:String) ->[Any] {
        
        var arrResult = [Any]()
        
        switch table {
        case SQLTableType.POINT_OF_INTEREST:
            arrResult =  PointsOfInterests().fetchAllTableRecords(querySql: queryString)
            print("count ",arrResult.count)
            break
            
        case SQLTableType.PLANNED_ROUTE:
            arrResult =  PlannedRoute().fetchAllPlannedRouteRecords(querySql: queryString)
            //            let arrr = arrResult as! [PlannedRoute]
            //            print("count ",arrr.count)
            //            print("arrResult route ",arrr.get(at: 0)?.planRideRoute ?? "")
            break
            
        case SQLTableType.RECEIVE_ALERT:
            //arrResult =  ReceiveAlert().fetchAllReceiveAlertRecords(querySql: queryString)
            break
            
        case SQLTableType.SENDING_ALERT:
            arrResult =  SendingAlert().fetchAllSendingAlertRecords(querySql: queryString)
            break
            
        }
        return arrResult
    }
    func fetchDistinctRideID(table:SQLTableType,queryString:String) ->[Any] {
        
        var arrResult = [Any]()
        
        switch table {
        case SQLTableType.POINT_OF_INTEREST:
            arrResult =  PointsOfInterests().fetchDistinctRideID(querySql: queryString)
            print("count ",arrResult.count)
            break
        default:
            break
        }
        return arrResult
        
    }
    func fetchAllTableRecordWithRespID(table:SQLTableType,queryString:String,id: Int32) ->[Any] {
        
        var arrResult = [Any]()
        
        switch table {
        case SQLTableType.POINT_OF_INTEREST:
            arrResult =  PointsOfInterests().fetchAllTableRecordsByID(querySql: queryString, id: id)
            print("count ",arrResult.count)
            break
            
        case SQLTableType.PLANNED_ROUTE:
            arrResult =  PlannedRoute().fetchAllPlannedRouteRecordsByRouteID(querySql: queryString, id: id)
            let arrr = arrResult as! [PlannedRoute]
            print("count ",arrr.count)
            print("arrResult route ",arrr.get(at: 0)?.planRideRoute ?? "")
            break
            
        case SQLTableType.RECEIVE_ALERT:
            arrResult =  PointsOfInterests().fetchAllTableRecordsByID(querySql: queryString, id: id)
            print("count ",arrResult.count)
            break
            
        case SQLTableType.SENDING_ALERT:
            arrResult =  PlannedRoute().fetchAllPlannedRouteRecordsByRouteID(querySql: queryString, id: id)
            let arrr = arrResult as! [PlannedRoute]
            print("count ",arrr.count)
            print("arrResult route ",arrr.get(at: 0)?.planRideRoute ?? "")
            break
            
        }
        
        return arrResult
        
    }
    
    func fetchAllTableRecordWithRespToField(table:SQLTableType,queryString:String,field: String) ->[Any] {
        
        var arrResult = [Any]()
        
        switch table {
            //       case SQLTableType.POINT_OF_INTEREST:
            //            arrResult =  PointsOfInterests().fetchAllTableRecordsByID(querySql: queryString, id: id)
            //            print("count ",arrResult.count)
            //            break
            //
            //        case SQLTableType.PLANNED_ROUTE:
            //            arrResult =  PlannedRoute().fetchAllPlannedRouteRecordsByRouteID(querySql: queryString, id: id)
            //            let arrr = arrResult as! [PlannedRoute]
            //            print("count ",arrr.count)
            //            print("arrResult route ",arrr.get(at: 0)?.planRideRoute ?? "")
            //            break
        //
        case SQLTableType.RECEIVE_ALERT:
            do{
                arrResult = try ReceiveAlert().fetchAllReceivedAlertRecordsWithField(querySql: queryString, field: field)
                let arrr = arrResult as! [ReceiveAlert]
                print("count ",arrr.count)
                
            }
            catch let message
            {
               // print(message)
            }
            break
            
        case SQLTableType.SENDING_ALERT:
            do{
                arrResult = try SendingAlert().fetchAllSendingAlertRecordsWithField(querySql: queryString, field: field)
                let arrr = arrResult as! [SendingAlert]
                print("count ",arrr.count)
                
            }
            catch let message
            {
                //print(message)
            }
            break
            
        default:
            break
        }
        
        return arrResult
        
    }
    
    func fetchAllRecordsBYGrp(table:SQLTableType,queryString:String,id: Int32,catagory:String) ->[Any]  {
        var arrResult = [Any]()
        
        switch table {
        case SQLTableType.POINT_OF_INTEREST:
            arrResult =  PointsOfInterests().fetchAllTableRecordsByCatagory(querySql: queryString, id: id, catagory: catagory)
            print("count ",arrResult.count)
            
            break
        default:
            break
        }
        return arrResult
    }
    
    
    func fetchPoIByCriteriaFromDB(table:SQLTableType,queryString: String,id: Int32,catagory:String,lat:Double,longitude:Double,radius:Double) ->[Any] {
        
        let arrResult = [Any]()
        switch table {
        case SQLTableType.POINT_OF_INTEREST:
            let arrResult =  PointsOfInterests().fetchPoIByCriteriaFromDB(querySql: queryString, id: id, catagory: catagory, lat: lat, lang: longitude, radius: radius)
            print("count ",arrResult.count)
            return arrResult
        default:
            break
        }
        return arrResult
    }
    
    deinit {
        sqlite3_close(dbPointer)
    }
    
}
