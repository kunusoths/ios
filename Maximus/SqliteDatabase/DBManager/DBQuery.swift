//
//  DBQuery.swift
//  Maximus
//
//  Created by Namdev Jagtap on 07/06/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation


struct DBQuery {
    
    //MARK: ******* PointOfIntrest Query *******

    static let QUERY_POINT_OF_INTREST_INSERT_INTO = "INSERT INTO \(SQLTableType.POINT_OF_INTEREST.rawValue) (\(SQliteDBConstants.KEY_RIDE_ID),\(SQliteDBConstants.KEY_POI_PLACE_ID),\(SQliteDBConstants.KEY_POI_NAME),\(SQliteDBConstants.KEY_POI_PROVIDER),\(SQliteDBConstants.KEY_POI_VICINITY),\(SQliteDBConstants.KEY_POI_RATING),\(SQliteDBConstants.KEY_POI_LATITUDE),\(SQliteDBConstants.KEY_POI_LONGITUDE),\(SQliteDBConstants.KEY_POI_COSLAT),\(SQliteDBConstants.KEY_POI_COSLNG),\(SQliteDBConstants.KEY_POI_SINLAT),\(SQliteDBConstants.KEY_POI_SINLNG),\(SQliteDBConstants.KEY_POI_ALTITUDE),\(SQliteDBConstants.KEY_POI_CATEGORIES),\(SQliteDBConstants.KEY_POI_OPENING_HOURS),\(SQliteDBConstants.KEY_POI_PHONE_NUMBER),\(SQliteDBConstants.KEY_POI_ADDRESS)) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
    
    static let QUERY_POINT_OF_INTREST_DROP = "DROP TABLE \(SQLTableType.POINT_OF_INTEREST.rawValue)"
    static let QUERY_POINT_OF_INTREST_DELETE_ROW = "DELETE FROM \(SQLTableType.POINT_OF_INTEREST.rawValue) WHERE \(SQliteDBConstants.KEY_RIDE_ID) = ?"
    
    static let QUERY_FETCH_POI_ALL_RECORDS = "SELECT * FROM \(SQLTableType.POINT_OF_INTEREST.rawValue)"
    
    static let QUERY_FETCH_POI_BY_ID = "SELECT * FROM \(SQLTableType.POINT_OF_INTEREST.rawValue) WHERE \(SQliteDBConstants.KEY_RIDE_ID) = ? "
    
    static let QUERY_FETCH_POI_BY_CATEGORIES = "SELECT * FROM \(SQLTableType.POINT_OF_INTEREST.rawValue) WHERE \(SQliteDBConstants.KEY_RIDE_ID) = ? AND \(SQliteDBConstants.KEY_POI_CATEGORIES) LIKE '%?%' GROUP BY \(SQliteDBConstants.KEY_POI_PLACE_ID)"
    
    static let QUERY_FETCH_DISTINCT_POI_RideID = "SELECT DISTINCT \(SQliteDBConstants.KEY_RIDE_ID) FROM \(SQLTableType.POINT_OF_INTEREST.rawValue)"
    
    static let QUERY_FETCH_POI_BY_PLACE_ID = "SELECT \(SQliteDBConstants.KEY_POI_PLACE_ID) FROM \(SQLTableType.POINT_OF_INTEREST.rawValue)"

    static let QUERY_FETCH_IS_RIDEID = "SELECT * FROM \(SQLTableType.POINT_OF_INTEREST.rawValue) WHERE \(SQliteDBConstants.KEY_RIDE_ID) = ? LIMIT 0,1"
    
    
    //MARK: ******* PlannedRoute Query *******
    
    static let QUERY_PLANNED_ROUTE_INSERT_INTO = "INSERT INTO \(SQLTableType.PLANNED_ROUTE.rawValue) (\(SQlitePlannedRoutes.KEY_RIDE_ID),\(SQlitePlannedRoutes.KEY_RIDE_ROUTE),\(SQlitePlannedRoutes.KEY_RIDE_WAYPOINTS),\(SQlitePlannedRoutes.KEY_RIDE_TBT_ROUTE)) VALUES (?,?,?,?)"
    
    static let QUERY_PLANNED_ROUTE_DROP = "DROP TABLE \(SQLTableType.PLANNED_ROUTE.rawValue)"
    static let QUERY_PLANNED_ROUTE_DELETE_ROW = "DELETE FROM \(SQLTableType.PLANNED_ROUTE.rawValue) WHERE \(SQlitePlannedRoutes.KEY_RIDE_ID) = ?"
    
    static let QUERY_FETCH_PLANNED_ROUTE_ALL_RECORDS = "SELECT * FROM \(SQLTableType.PLANNED_ROUTE.rawValue)"
    
    static let QUERY_FETCH_PLANNED_ROUTE_BY_ID = "SELECT * FROM \(SQLTableType.PLANNED_ROUTE.rawValue) WHERE \(SQlitePlannedRoutes.KEY_RIDE_ID) = ? "

    static let QUERY_UPDATE_PLANNED_ROUTE = "UPDATE \(SQLTableType.PLANNED_ROUTE.rawValue) SET \(SQlitePlannedRoutes.KEY_RIDE_ROUTE) = ?, \(SQlitePlannedRoutes.KEY_RIDE_WAYPOINTS) = ?, \(SQlitePlannedRoutes.KEY_RIDE_TBT_ROUTE) = ? WHERE \(SQlitePlannedRoutes.KEY_RIDE_ID) = ?"
    
    static let QUERY_FETCH_IS_ROUTE = "SELECT * FROM \(SQLTableType.PLANNED_ROUTE.rawValue) WHERE \(SQliteDBConstants.KEY_RIDE_ID) = ? LIMIT 0,1"

    
    //MARK: ******** Receiving Alerts ********
    
    static let QUERY_RECEIVE_ALERT_INSERT_INTO = "INSERT INTO \(SQLTableType.RECEIVE_ALERT.rawValue) (\(SQliteReceiveAlert.KEY_ALERT_ID),\(SQliteReceiveAlert.KEY_RIDE_ID),\(SQliteReceiveAlert.KEY_UUID),\(SQliteReceiveAlert.KEY_ALERT_STATUS),\(SQliteReceiveAlert.KEY_ORIGANATED_BIKER_ID),\(SQliteReceiveAlert.KEY_ORIGANATED_BIKER_NAME),\(SQliteReceiveAlert.KEY_ALERT_SHORT_NAME),\(SQliteReceiveAlert.KEY_CATEGORY),\(SQliteReceiveAlert.KEY_PRIORITY),\(SQliteReceiveAlert.KEY_HELPED),\(SQliteReceiveAlert.KEY_LOCATION)) VALUES(?,?,?,?,?,?,?,?,?,?,?)"
    
   static let QUERY_RECEIVE_ALERT_DROP = "DROP TABLE \(SQLTableType.RECEIVE_ALERT.rawValue)"
    
    static let QUERY_RECEIVE_ALERT_DELET_ROW = " DELETE FROM \(SQLTableType.RECEIVE_ALERT.rawValue)WHERE \(SQliteReceiveAlert.KEY_ALERT_ID) = ?"
    
    static let QUERY_RECEIVE_ALERT_FETCH_ALL_RECORDS = "SELECT * FROM \(SQLTableType.RECEIVE_ALERT.rawValue)"
    
    static let QUERY_RECEIVE_ALERT_FETCH_BY_ID = "SELECT * FROM \(SQLTableType.RECEIVE_ALERT.rawValue) WHERE \(SQliteReceiveAlert.KEY_ALERT_ID) = ?"
    
    static let QUERY_RECEIVE_ALERT_FETCH_BY_STATUS = "SELECT * FROM \(SQLTableType.RECEIVE_ALERT.rawValue) WHERE \(SQliteReceiveAlert.KEY_ALERT_STATUS) = ?"
    
    static let QUERY_UPDATE_RECEIVE_ALERT_STATUS = "UPDATE \(SQLTableType.RECEIVE_ALERT.rawValue) SET \(SQliteReceiveAlert.KEY_ALERT_STATUS) = ? WHERE \(SQliteReceiveAlert.KEY_ALERT_ID) = ?"
    

    
    //MARK: ******* Sending Alerts ********
    
    
    //static let KEY_RESPONDANT_BIKER_NAME       = "alertRespondantBikerName"
    
    
    static let QUERY_SENDING_ALERT_INSERT_INTO = "INSERT INTO \(SQLTableType.SENDING_ALERT.rawValue) (\(SQliteSendingAlert.KEY_ALERT_ID),\(SQliteSendingAlert.KEY_TRIGGERED_BY_ID),\(SQliteSendingAlert.KEY_RIDE_ID),\(SQliteSendingAlert.KEY_ALERT_SHORT_NAME),\(SQliteSendingAlert.KEY_CATEGORY),\(SQliteSendingAlert.KEY_PRIORITY),\(SQliteSendingAlert.KEY_ALERT_STATUS),\(SQliteSendingAlert.KEY_HELP_RECEIVED),\(SQliteSendingAlert.KEY_RESPONDANT_BIKER_NAME)) VALUES(?,?,?,?,?,?,?,?,?)"
    
    static let QUERY_SENDING_ALERT_DROP = "DROP TABLE \(SQLTableType.SENDING_ALERT.rawValue)"
    
    static let QUERY_SENDING_ALERT_DELET_ROW = " DELETE FROM \(SQLTableType.SENDING_ALERT.rawValue)WHERE \(SQliteSendingAlert.KEY_ALERT_ID) = ?"
    
    static let QUERY_SENDING_ALERT_FETCH_ALL_RECORDS = "SELECT * FROM \(SQLTableType.SENDING_ALERT.rawValue)"
    
    static let QUERY_SENDING_ALERT_FETCH_BY_ID = "SELECT * FROM \(SQLTableType.SENDING_ALERT.rawValue) WHERE \(SQliteSendingAlert.KEY_ALERT_ID) = ?"
    
    static let QUERY_SENDING_ALERT_FETCH_BY_STATUS = "SELECT * FROM \(SQLTableType.SENDING_ALERT.rawValue) WHERE \(SQliteSendingAlert.KEY_ALERT_STATUS) = ?"
    
    
    static let QUERY_UPDATE_SENDING_ALERT_STATUS = "UPDATE \(SQLTableType.SENDING_ALERT.rawValue) SET \(SQliteSendingAlert.KEY_ALERT_STATUS) = ? WHERE \(SQliteSendingAlert.KEY_ALERT_ID) = ?"
    
    static let QUERY_UPDATE_SENDING_ALERT_HELPER_BIKER = "UPDATE \(SQLTableType.SENDING_ALERT.rawValue) SET \(SQliteSendingAlert.KEY_RESPONDANT_BIKER_NAME) = ? WHERE \(SQliteSendingAlert.KEY_ALERT_ID) = ?"
    

}
