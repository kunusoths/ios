//
//  DBHelper.swift
//  Maximus
//
//  Created by Namdev Jagtap on 06/06/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation

//private let fm = FileManager.default

let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
    .appendingPathComponent("MaximusDB.sqlite")


//let docsurl = try! fm.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)

private enum Database: String {
    case Part1
    var path: String {
        return  "\(fileURL.path)"
    }
}

public let part1DbPath = Database.Part1.path

private func destroyDatabase(db: Database) {
    do {
        if FileManager.default.fileExists(atPath: db.path) {
            try FileManager.default.removeItem(atPath: db.path)
        }
    } catch {
        print("Could not destroy \(db) Database file.")
    }
}

public func destroyPart1Database() {
    destroyDatabase(db: .Part1)
}
