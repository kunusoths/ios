//
//  PlannedRoute.swift
//  Maximus
//
//  Created by Namdev Jagtap on 21/06/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import SQLite


extension PlannedRoute: SQLTable {
    
    static var createStatement: String {
        return "CREATE TABLE \(SQLTableType.PLANNED_ROUTE.rawValue)(" +
            "\(SQlitePlannedRoutes.KEY_RIDE_ID) INTEGER PRIMARY KEY NOT NULL," +
            "\(SQlitePlannedRoutes.KEY_RIDE_ROUTE) TEXT," +
            "\(SQlitePlannedRoutes.KEY_RIDE_WAYPOINTS) TEXT," +
            "\(SQlitePlannedRoutes.KEY_RIDE_TBT_ROUTE) TEXT" +
        ");"
    }
}

class PlannedRoute : CustomDebugStringConvertible {

    let plannedRideID: Int32
    let planRideRoute:String
    let planRideWayPoints:String
    let planRideTBTRoute:String
    
    public init(id: Int32? = 0 ,planRideRoute:String? = "",planRideWayPoints:String? = "",planRideTBTRoute:String? = "")
    {
        self.plannedRideID = id!
        self.planRideRoute = planRideRoute!
        self.planRideWayPoints = planRideWayPoints!
        self.planRideTBTRoute = planRideTBTRoute!
    }
    
    //MARK: ******* INSERT *******
    func insertIntoTable(insertSql: String,data:Any) throws {
        
//        print("Data ",data)
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        let insertStatement = try db?.prepareStatement(sql: insertSql)//prepareStatement(sql: insertSql)
        defer {
            sqlite3_finalize(insertStatement)
        }
        
        let plannedRouteObj = data as! PlannedRoute
        
        guard
                sqlite3_bind_int(insertStatement, 1,plannedRouteObj.plannedRideID) == SQLITE_OK  &&
                sqlite3_bind_text(insertStatement, 2, plannedRouteObj.planRideRoute._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertStatement, 3, plannedRouteObj.planRideWayPoints._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertStatement, 4, plannedRouteObj.planRideTBTRoute._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK
            else {
                throw SQLiteError.Bind(message: (db?.errorMessage)!)
        }
        
        guard sqlite3_step(insertStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: (db?.errorMessage)!)
        }
        
       // print("Successfully inserted row.")
    }
    
    
    //MARK: ******* UPDATE *******
    func updateIntoTable(insertSql: String,data:Any) throws {
        
        print("Data ",data)
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        let insertStatement = try db?.prepareStatement(sql: insertSql)//prepareStatement(sql: insertSql)
        defer {
            sqlite3_finalize(insertStatement)
        }
        
        let plannedRouteObj = data as! PlannedRoute
        
        guard
            sqlite3_bind_int(insertStatement, 1,plannedRouteObj.plannedRideID) == SQLITE_OK  &&
                sqlite3_bind_text(insertStatement, 2, plannedRouteObj.planRideRoute._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertStatement, 3, plannedRouteObj.planRideWayPoints._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertStatement, 4, plannedRouteObj.planRideTBTRoute._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK
            else {
                throw SQLiteError.Bind(message: (db?.errorMessage)!)
        }
        
        guard sqlite3_step(insertStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: (db?.errorMessage)!)
        }
        
        print("Successfully Updated row.")
    }
    
    
    //MARK: ******* DELETE *******
    func deleteTable(deleteSql: String) throws {
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        let deleteStatement = try db?.prepareStatement(sql: deleteSql)
        defer {
            sqlite3_finalize(deleteStatement)
        }
        
        guard sqlite3_step(deleteStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: (db?.errorMessage)!)
        }
        
        print("Successfully table deleted")
    }
    
    func deleteRouteWithRespectiveRideID(deleteSql: String,rideId: Int32) throws {
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        let deleteStatement = try db?.prepareStatement(sql: deleteSql)
        defer {
            sqlite3_finalize(deleteStatement)
        }
        
        guard sqlite3_bind_int(deleteStatement, 1, rideId) == SQLITE_OK else {
            throw SQLiteError.Bind(message: (db?.errorMessage)!)
        }
        
        guard sqlite3_step(deleteStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: (db?.errorMessage)!)
        }
        
        print("Successfully deleted row.")
    }
    
    //MARK: ******* FETCH *******
    func fetchAllPlannedRouteRecords(querySql: String) -> [Any] {
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        
        guard let queryStatement = try? db?.prepareStatement(sql: querySql) else {
            return []
        }
        
        defer {
            sqlite3_finalize(queryStatement)
        }
        
        guard sqlite3_step(queryStatement) == SQLITE_ROW else {
            return []
        }
        
        var arrPlannedRoute = [PlannedRoute]()
        repeat {
            let rideid         = sqlite3_column_int(queryStatement, 0)
            let route      = sqlite3_column_text(queryStatement, 1)
            let wayPonts    = sqlite3_column_text(queryStatement, 2)
            let tbtRoute     = sqlite3_column_text(queryStatement, 3)
            
            let obj = PlannedRoute(id: rideid, planRideRoute: String(cString:route!) != nil ? String(cString:route!):"", planRideWayPoints: String(cString:wayPonts!) != nil ? String(cString:wayPonts!):"", planRideTBTRoute: String(cString:tbtRoute!) != nil ? String(cString:tbtRoute!):"")
            
            arrPlannedRoute.append(obj)
            
        } while (sqlite3_step(queryStatement) != SQLITE_DONE)
        
        return arrPlannedRoute
    }
    
    
    func fetchAllPlannedRouteRecordsByRouteID(querySql: String,id: Int32) -> [Any] {
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        print("querySql",querySql)
        
        guard let queryStatement = try? db?.prepareStatement(sql: querySql) else {
            return []
        }
        
        defer {
            sqlite3_finalize(queryStatement)
        }
        
        do {
            try sqlite3_bind_int(queryStatement, 1, id) == SQLITE_OK
            
        } catch let message {
            print(message)
        }
        
        guard sqlite3_step(queryStatement) == SQLITE_ROW else {
            return []
        }
        
        var arrPlannedRoute = [PlannedRoute]()
        repeat {
            let rideid         = sqlite3_column_int(queryStatement, 0)
            let route      = sqlite3_column_text(queryStatement, 1)
            let wayPonts    = sqlite3_column_text(queryStatement, 2)
            let tbtRoute     = sqlite3_column_text(queryStatement, 3)
            
            let obj = PlannedRoute(id: rideid, planRideRoute: String(cString:route!) != nil ? String(cString:route!):"", planRideWayPoints: String(cString:wayPonts!) != nil ? String(cString:wayPonts!):"", planRideTBTRoute: String(cString:tbtRoute!) != nil ? String(cString:tbtRoute!):"")
            
            arrPlannedRoute.append(obj)
            
        } while (sqlite3_step(queryStatement) != SQLITE_DONE)
        
        return arrPlannedRoute
    }
    
    public var debugDescription: String {
        return "\(plannedRideID) | \(planRideRoute)\(planRideWayPoints) | \(planRideTBTRoute)"
    }

}
