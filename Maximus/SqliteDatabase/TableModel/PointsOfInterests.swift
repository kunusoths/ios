//
//  PointsOfInterests.swift
//  Maximus
//
//  Created by Namdev Jagtap on 06/06/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation
import SQLite



extension PointsOfInterests: SQLTable {
    
    static var createStatement: String {
        return "CREATE TABLE \(SQLTableType.POINT_OF_INTEREST.rawValue)(" +
            "\(SQliteDBConstants.KEY_ID) INTEGER PRIMARY KEY NOT NULL," +
            "\(SQliteDBConstants.KEY_RIDE_ID) INT," +
            "\(SQliteDBConstants.KEY_POI_PLACE_ID) TEXT," +
            "\(SQliteDBConstants.KEY_POI_NAME) TEXT," +
            "\(SQliteDBConstants.KEY_POI_PROVIDER) TEXT," +
            "\(SQliteDBConstants.KEY_POI_VICINITY) TEXT," +
            "\(SQliteDBConstants.KEY_POI_RATING) REAL," +
            "\(SQliteDBConstants.KEY_POI_LATITUDE) REAL," +
            "\(SQliteDBConstants.KEY_POI_LONGITUDE) REAL," +
            "\(SQliteDBConstants.KEY_POI_COSLAT) REAL," +
            "\(SQliteDBConstants.KEY_POI_COSLNG) REAL," +
            "\(SQliteDBConstants.KEY_POI_SINLAT) REAL," +
            "\(SQliteDBConstants.KEY_POI_SINLNG) REAL," +
            "\(SQliteDBConstants.KEY_POI_ALTITUDE) REAL," +
            "\(SQliteDBConstants.KEY_POI_CATEGORIES) TEXT," +
            "\(SQliteDBConstants.KEY_POI_OPENING_HOURS) TEXT," +
            "\(SQliteDBConstants.KEY_POI_PHONE_NUMBER) TEXT," +
            "\(SQliteDBConstants.KEY_POI_ADDRESS) TEXT" +
        ");"
    }
}
class PointsOfInterests: CustomDebugStringConvertible {
    let id: Int32
    let poiRideID: Int32
    let poiPlaceID:String
    let poiProvider:String
    let poiName:String
    let poiVicinity:String
    let poiRating:Double
    let poiLatitude:Double
    let poiLongitude:Double
    let poiCosLatitude:Double
    let poiSinLatitude:Double
    let poiCosLongitude:Double
    let poiSinLongitude:Double
    let poiAltitude:Double
    let poiCategories:String
    let poiOpeningHours:String
    let poiPhoneNumber:String
    let poiAddress:String
    
    public init(id: Int32? = 0 ,poiRideID: Int32? = 3,poiPlaceID:String? = "",poiProvider:String? = "",poiName:String? = "",poiVicinity:String? = "",poiRating:Double? = 0,poiLatitude:Double? = 0,poiLongitude:Double? = 0,poiCosLatitude:Double? = 0,poiSinLatitude:Double? = 0,poiCosLongitude:Double? = 0,poiSinLongitude:Double? = 0,poiAltitude:Double? = 0,poiCategories:String? = "",poiOpeningHours:String? = "",poiPhoneNumber:String? = "",poiAddress:String? = "")
    {
        self.id = id!
        self.poiRideID = poiRideID!
        self.poiPlaceID = poiPlaceID!
        self.poiProvider = poiProvider!
        self.poiName = poiName!
        self.poiVicinity = poiVicinity!
        self.poiRating = poiRating!
        self.poiLatitude = poiLatitude!
        self.poiLongitude = poiLongitude!
        self.poiCosLatitude = poiCosLatitude!
        self.poiSinLatitude = poiSinLatitude!
        self.poiCosLongitude = poiCosLongitude!
        self.poiSinLongitude = poiSinLongitude!
        self.poiAltitude = poiAltitude!
        self.poiCategories = poiCategories!
        self.poiOpeningHours = poiOpeningHours!
        self.poiPhoneNumber = poiPhoneNumber!
        self.poiAddress = poiAddress!
    }
    
    
    func convertPartialDistanceToKm(result:Double) -> Double{
//        return Math.acos(result) * 6371;
       return acos(result) * 6371;
    }
    
    func deg2rad(deg:Double) -> Double {
        return (deg * M_PI / 180.0);
    }

    
    func sind(degrees: Double) -> Double {
//        return sin(degrees * M_PI / 180.0)
        return sin(degrees)
    }
    
    func cosd(degrees: Double) -> Double {
//        return cos(degrees * M_PI / 180.0)
        return cos(degrees)
    }
    
    //MARK: ******* INSERT *******
    func insertIntoTable(insertSql: String,data:Any) throws {
//        print("poi_id: \(data)")
        
            var db : DBManager?
            do {
                db = try DBManager.open(path: part1DbPath)
            } catch let message {
                print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
                print(message)
            }
            
            print("POIIII STARTED")
            sqlite3_exec(db?.dbPointer, "BEGIN TRANSACTION", nil, nil, nil)
            
            let insertStatement = try db?.prepareStatement(sql: insertSql)//prepareStatement(sql: insertSql)
            defer {
                sqlite3_finalize(insertStatement)
            }
        
            let poi_rideID = MyDefaults.getInt(key: DefaultKeys.KEY_POI_RIDEID)
            
            var poiLists:[PointsOfInterests] = [PointsOfInterests] ()
            let poiList = data as! [POIListResponse]//as! PointsOfInterests
        
        
        
            for poi in poiList{
                
                var catagory:String = ""
                
                for item in poi.categories! {
                    catagory += item
                    catagory += " "
                }
                let poiResponse = PointsOfInterests(id: 0,
                                                    poiRideID: Int32(poi_rideID) != nil ? Int32(poi_rideID):0,
                                                    poiPlaceID: poi.poiId != nil ? poi.poiId :"",
                                                    poiProvider: poi.provider != nil ? poi.provider :"Provider",
                                                    poiName: poi.name != nil ? poi.name :"",
                                                    poiVicinity: "vicinity",
                                                    poiRating: poi.rating != nil ?  poi.rating: 0,
                                                    poiLatitude: poi.location?.latitude != nil ? poi.location?.latitude: 0,
                                                    poiLongitude: poi.location?.longitude != nil ? poi.location?.longitude : 0,
                                                    poiCosLatitude: self.cosd(degrees: self.deg2rad(deg:(poi.location?.latitude)!)) != nil ? self.cosd(degrees: self.deg2rad(deg:(poi.location?.latitude)!)): 0,
                                                    poiSinLatitude: self.sind(degrees: self.deg2rad(deg:(poi.location?.latitude)!)) != nil ? self.sind(degrees: self.deg2rad(deg:(poi.location?.latitude)!)): 0,
                                                    poiCosLongitude: self.cosd(degrees: self.deg2rad(deg:(poi.location?.longitude)!)) != nil ? self.cosd(degrees: self.deg2rad(deg:(poi.location?.longitude)!)): 0,
                                                    poiSinLongitude: self.sind(degrees: self.deg2rad(deg:(poi.location?.longitude)!)) != nil ? self.sind(degrees: self.deg2rad(deg:(poi.location?.longitude)!)): 0,
                                                    poiAltitude: 1.1, poiCategories: catagory != nil ? catagory :"",
                                                    poiOpeningHours: "",
                                                    poiPhoneNumber: poi.phone_number != nil ?  poi.phone_number: "",
                                                    poiAddress: "address" != nil ? "address": "")
                poiLists.append(poiResponse)
            }
            
            
            
            for poi in poiLists{
                guard
                    
                    sqlite3_bind_int(insertStatement, 1,poi.poiRideID) == SQLITE_OK  &&
                        sqlite3_bind_text(insertStatement, 2, poi.poiPlaceID._bridgeToObjectiveC().utf8String, Int32(poi.poiPlaceID.length), nil) == SQLITE_OK &&
                        sqlite3_bind_text(insertStatement, 3, poi.poiName._bridgeToObjectiveC().utf8String, Int32(poi.poiName.length), nil) == SQLITE_OK &&
                        sqlite3_bind_text(insertStatement, 4, poi.poiProvider._bridgeToObjectiveC().utf8String, Int32(poi.poiProvider.length), nil) == SQLITE_OK &&
                        sqlite3_bind_text(insertStatement, 5, poi.poiVicinity._bridgeToObjectiveC().utf8String, Int32(poi.poiVicinity.length), nil) == SQLITE_OK &&
                        sqlite3_bind_double(insertStatement, 6, Double(poi.poiRating)) == SQLITE_OK &&
                        sqlite3_bind_double(insertStatement, 7, Double(poi.poiLatitude)) == SQLITE_OK &&
                        sqlite3_bind_double(insertStatement, 8, Double(poi.poiLongitude)) == SQLITE_OK &&
                        sqlite3_bind_double(insertStatement, 9, Double(poi.poiCosLatitude)) == SQLITE_OK &&
                        sqlite3_bind_double(insertStatement, 10, Double(poi.poiCosLongitude)) == SQLITE_OK &&
                        sqlite3_bind_double(insertStatement, 11, Double(poi.poiSinLatitude)) == SQLITE_OK &&
                        sqlite3_bind_double(insertStatement, 12, Double(poi.poiSinLongitude)) == SQLITE_OK &&
                        sqlite3_bind_double(insertStatement, 13, Double(poi.poiAltitude)) == SQLITE_OK &&
                        sqlite3_bind_text(insertStatement, 14, poi.poiCategories._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK &&
                        sqlite3_bind_text(insertStatement, 15, poi.poiOpeningHours._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK &&
                        sqlite3_bind_text(insertStatement, 16, poi.poiPhoneNumber._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK &&
                        sqlite3_bind_text(insertStatement, 17, poi.poiAddress._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK
                    else {
                        throw SQLiteError.Bind(message: (db?.errorMessage)!)
                }
                let result = sqlite3_step(insertStatement)
                
                if result == SQLITE_DONE{
                    
                }else{
                    throw SQLiteError.Step(message: (db?.errorMessage)!)
                }
                
                sqlite3_reset(insertStatement)
            }
            
            sqlite3_exec(db?.dbPointer, "COMMIT TRANSACTION", nil, nil, nil)
            print("POIIII Commited")
    }
    
    //MARK: ******* DELETE *******
    func deleteTable(deleteSql: String) throws {
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        let deleteStatement = try db?.prepareStatement(sql: deleteSql)
        defer {
            sqlite3_finalize(deleteStatement)
        }
        
        guard sqlite3_step(deleteStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: (db?.errorMessage)!)
        }
        
        print("Successfully table deleted")
    }
    
    func deleteAllPOIWithRespectiveRideID(deleteSql: String,rideId: Int32) throws {
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        let deleteStatement = try db?.prepareStatement(sql: deleteSql)
        defer {
            sqlite3_finalize(deleteStatement)
        }
        
        guard sqlite3_bind_int(deleteStatement, 1, rideId) == SQLITE_OK else {
            throw SQLiteError.Bind(message: (db?.errorMessage)!)
        }
        
        guard sqlite3_step(deleteStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: (db?.errorMessage)!)
        }
        
        print("Successfully deleted row.")
    }
    //MARK: ******* FETCH *******
    func fetchAllTableRecords(querySql: String) -> [PointsOfInterests] {
        
        //           let querySql = "SELECT * FROM Contact ORDER BY Id ASC;"
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        
        guard let queryStatement = try? db?.prepareStatement(sql: querySql) else {
            return []
        }
        
        defer {
            sqlite3_finalize(queryStatement)
        }
        
        guard sqlite3_step(queryStatement) == SQLITE_ROW else {
            return []
        }
        
        var pois = [PointsOfInterests]()
        repeat {
            let id              = sqlite3_column_int(queryStatement, 0)
            let rideId          = sqlite3_column_int(queryStatement, 1)
            let placeid         = sqlite3_column_text(queryStatement, 2)
            let poiName         = sqlite3_column_text(queryStatement, 3)
            let poiProvider     = sqlite3_column_text(queryStatement, 4)
            let poiVicinity     = sqlite3_column_text(queryStatement, 5)
            let poiRating       = sqlite3_column_double(queryStatement,6)
            let poiLatitude     = sqlite3_column_double(queryStatement,7)
            let poiLongitude    = sqlite3_column_double(queryStatement,8)
            let poiCosLatitude  = sqlite3_column_double(queryStatement,9)
            let poiCosLongitude = sqlite3_column_double(queryStatement,10)
            let poiSinLatitude  = sqlite3_column_double(queryStatement,11)
            let poiSinLongitude = sqlite3_column_double(queryStatement,12)
            let poiAltitude     = sqlite3_column_double(queryStatement,13)
            let poiCategories   = sqlite3_column_text(queryStatement, 14)
            let poiOpeningHours = sqlite3_column_text(queryStatement, 15)
            let poiPhoneNumber  = sqlite3_column_text(queryStatement, 16)
            let poiAddress      = sqlite3_column_text(queryStatement, 17)
           
            
            let objPoi = PointsOfInterests(id: id, poiRideID: rideId, poiPlaceID: String(cString: placeid!) != nil ? String(cString: placeid!):"", poiProvider: String(cString: poiProvider!) != nil ? String(cString: poiProvider!):"", poiName: String(cString: poiName!) != nil ? String(cString: poiName!):"", poiVicinity: String(cString: poiVicinity!) != nil ? String(cString: poiVicinity!):"", poiRating:Double(poiRating) != nil ? Double(poiRating):0  , poiLatitude: Double(poiLatitude) != nil ? Double(poiLatitude): 0, poiLongitude: Double(poiLongitude) != nil ? Double(poiLongitude):0, poiCosLatitude: Double(poiCosLatitude) != nil ? Double(poiCosLatitude):0, poiSinLatitude: Double(poiSinLatitude) != nil ? Double(poiSinLatitude):0, poiCosLongitude: Double(poiCosLongitude) != nil ? Double(poiCosLongitude):0, poiSinLongitude: Double(poiSinLongitude) != nil ? Double(poiSinLongitude):0, poiAltitude: Double(poiAltitude) != nil ? Double(poiAltitude):0, poiCategories: String(cString: poiCategories!) != nil ? String(cString: poiCategories!):"", poiOpeningHours: String(cString: poiOpeningHours!) != nil ? String(cString: poiOpeningHours!):"", poiPhoneNumber: String(cString: poiPhoneNumber!) != nil ? String(cString: poiPhoneNumber!):"", poiAddress: String(cString: poiAddress!) != nil ? String(cString: poiAddress!):"")
           
            pois.append(objPoi)
            
        } while (sqlite3_step(queryStatement) != SQLITE_DONE)
        
        return pois
    }
    
    func fetchDistinctRideID(querySql: String) -> [PointsOfInterests] {
        
        //                    let querySql = "SELECT * FROM Contact ORDER BY Id ASC;"
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        
        guard let queryStatement = try? db?.prepareStatement(sql: querySql) else {
            return []
        }
        
        defer {
            sqlite3_finalize(queryStatement)
        }
        
        guard sqlite3_step(queryStatement) == SQLITE_ROW else {
            return []
        }
        
        var pois = [PointsOfInterests]()
        repeat {
            let id              = sqlite3_column_int(queryStatement, 0)
            let rideId          = sqlite3_column_int(queryStatement, 1)
            let placeid         = sqlite3_column_text(queryStatement, 2)
            let poiName         = sqlite3_column_text(queryStatement, 3)
            let poiProvider     = sqlite3_column_text(queryStatement, 4)
            let poiVicinity     = sqlite3_column_text(queryStatement, 5)
            let poiRating       = sqlite3_column_double(queryStatement,6)
            let poiLatitude     = sqlite3_column_double(queryStatement,7)
            let poiLongitude    = sqlite3_column_double(queryStatement,8)
            let poiCosLatitude  = sqlite3_column_double(queryStatement,9)
            let poiCosLongitude = sqlite3_column_double(queryStatement,10)
            let poiSinLatitude  = sqlite3_column_double(queryStatement,11)
            let poiSinLongitude = sqlite3_column_double(queryStatement,12)
            let poiAltitude     = sqlite3_column_double(queryStatement,13)
            let poiCategories   = sqlite3_column_text(queryStatement, 14)
            let poiOpeningHours = sqlite3_column_text(queryStatement, 15)
            let poiPhoneNumber  = sqlite3_column_text(queryStatement, 16)
            let poiAddress      = sqlite3_column_text(queryStatement, 17)
            
             let objPoi = PointsOfInterests(id: id, poiRideID: rideId, poiPlaceID: String(cString: placeid!) != nil ? String(cString: placeid!):"", poiProvider: String(cString: poiProvider!) != nil ? String(cString: poiProvider!):"", poiName: String(cString: poiName!) != nil ? String(cString: poiName!):"", poiVicinity: String(cString: poiVicinity!) != nil ? String(cString: poiVicinity!):"", poiRating:Double(poiRating) != nil ? Double(poiRating):0  , poiLatitude: Double(poiLatitude) != nil ? Double(poiLatitude): 0, poiLongitude: Double(poiLongitude) != nil ? Double(poiLongitude):0, poiCosLatitude: Double(poiCosLatitude) != nil ? Double(poiCosLatitude):0, poiSinLatitude: Double(poiSinLatitude) != nil ? Double(poiSinLatitude):0, poiCosLongitude: Double(poiCosLongitude) != nil ? Double(poiCosLongitude):0, poiSinLongitude: Double(poiSinLongitude) != nil ? Double(poiSinLongitude):0, poiAltitude: Double(poiAltitude) != nil ? Double(poiAltitude):0, poiCategories: String(cString: poiCategories!) != nil ? String(cString: poiCategories!):"", poiOpeningHours: String(cString: poiOpeningHours!) != nil ? String(cString: poiOpeningHours!):"", poiPhoneNumber: String(cString: poiPhoneNumber!) != nil ? String(cString: poiPhoneNumber!):"", poiAddress: String(cString: poiAddress!) != nil ? String(cString: poiAddress!):"")
            
            pois.append(objPoi)
            
        } while (sqlite3_step(queryStatement) != SQLITE_DONE)
        
        return pois
    }
    
    
    func fetchAllTableRecordsByCatagory(querySql: String,id: Int32,catagory:String) -> [PointsOfInterests] {
        
       var sqlQuery =  "SELECT * FROM \(SQLTableType.POINT_OF_INTEREST.rawValue) WHERE \(SQliteDBConstants.KEY_RIDE_ID) = \(id) AND \(SQliteDBConstants.KEY_POI_CATEGORIES) LIKE '%\(catagory)%' GROUP BY \(SQliteDBConstants.KEY_POI_PLACE_ID);"
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        
        print("sqlQuery = ",sqlQuery)
        
        guard let queryStatement = try? db?.prepareStatement(sql: sqlQuery) else {
            return []
        }
        
        defer {
            
            print(queryStatement ?? "EMpty")
            sqlite3_finalize(queryStatement)
        }
//        do {
//            
//            let itemName = catagory as NSString
//            var res1 = false
//            res1 = sqlite3_bind_int(queryStatement, 1, id) == SQLITE_OK
//            
//            if (res1){
//            res1 = sqlite3_bind_text(queryStatement, 2, catagory, -1, nil) == SQLITE_OK
//            }
//        } catch let message {
//            print(message)
//        }
        
        guard sqlite3_step(queryStatement) == SQLITE_ROW else {
            return []
        }
        
        
        var pois = [PointsOfInterests]()
        repeat {
            let id              = sqlite3_column_int(queryStatement, 0)
            let rideId          = sqlite3_column_int(queryStatement, 1)
            let placeid         = sqlite3_column_text(queryStatement, 2)
            let poiName         = sqlite3_column_text(queryStatement, 3)
            let poiProvider     = sqlite3_column_text(queryStatement, 4)
            let poiVicinity     = sqlite3_column_text(queryStatement, 5)
            let poiRating       = sqlite3_column_double(queryStatement,6)
            let poiLatitude     = sqlite3_column_double(queryStatement,7)
            let poiLongitude    = sqlite3_column_double(queryStatement,8)
            let poiCosLatitude  = sqlite3_column_double(queryStatement,9)
            let poiCosLongitude = sqlite3_column_double(queryStatement,10)
            let poiSinLatitude  = sqlite3_column_double(queryStatement,11)
            let poiSinLongitude = sqlite3_column_double(queryStatement,12)
            let poiAltitude     = sqlite3_column_double(queryStatement,13)
            let poiCategories   = sqlite3_column_text(queryStatement, 14)
            let poiOpeningHours = sqlite3_column_text(queryStatement, 15)
            let poiPhoneNumber  = sqlite3_column_text(queryStatement, 16)
            let poiAddress      = sqlite3_column_text(queryStatement, 17)
            
            let objPoi = PointsOfInterests(id: id, poiRideID: rideId, poiPlaceID: String(cString: placeid!), poiProvider: String(cString: poiProvider!), poiName: String(cString: poiName!), poiVicinity: String(cString: poiVicinity!), poiRating:Double(poiRating), poiLatitude: Double(poiLatitude), poiLongitude: Double(poiLongitude), poiCosLatitude: Double(poiCosLatitude), poiSinLatitude: Double(poiSinLatitude), poiCosLongitude: Double(poiCosLongitude), poiSinLongitude: Double(poiSinLongitude), poiAltitude: Double(poiAltitude), poiCategories: String(cString: poiCategories!), poiOpeningHours: String(cString: poiOpeningHours!), poiPhoneNumber: String(cString: poiPhoneNumber!), poiAddress: String(cString: poiAddress!))
            
           // print("poi id ",objPoi.id, " cat ",objPoi.poiCategories)
            pois.append(objPoi)
            
        } while (sqlite3_step(queryStatement) != SQLITE_DONE)
        
        return pois
    }
    
    
    func fetchAllTableRecordsByID(querySql: String,id: Int32) -> [PointsOfInterests] {
        
        //                    let querySql = "SELECT * FROM Contact ORDER BY Id ASC;"
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        print("querySql",querySql)
        
        guard let queryStatement = try? db?.prepareStatement(sql: querySql) else {
            return []
        }
        
        defer {
            sqlite3_finalize(queryStatement)
        }

        do {
            try sqlite3_bind_int(queryStatement, 1, id) == SQLITE_OK
            
        } catch let message {
            print(message)
        }
        
        guard sqlite3_step(queryStatement) == SQLITE_ROW else {
            return []
        }
        
        var pois = [PointsOfInterests]()
        repeat {
            let id              = sqlite3_column_int(queryStatement, 0)
            let rideId          = sqlite3_column_int(queryStatement, 1)
            let placeid         = sqlite3_column_text(queryStatement, 2)
            let poiName         = sqlite3_column_text(queryStatement, 3)
            let poiProvider     = sqlite3_column_text(queryStatement, 4)
            let poiVicinity     = sqlite3_column_text(queryStatement, 5)
            let poiRating       = sqlite3_column_double(queryStatement,6)
            let poiLatitude     = sqlite3_column_double(queryStatement,7)
            let poiLongitude    = sqlite3_column_double(queryStatement,8)
            let poiCosLatitude  = sqlite3_column_double(queryStatement,9)
            let poiCosLongitude = sqlite3_column_double(queryStatement,10)
            let poiSinLatitude  = sqlite3_column_double(queryStatement,11)
            let poiSinLongitude = sqlite3_column_double(queryStatement,12)
            let poiAltitude     = sqlite3_column_double(queryStatement,13)
            let poiCategories   = sqlite3_column_text(queryStatement, 14)
            let poiOpeningHours = sqlite3_column_text(queryStatement, 15)
            let poiPhoneNumber  = sqlite3_column_text(queryStatement, 16)
            let poiAddress      = sqlite3_column_text(queryStatement, 17)
            
            let objPoi = PointsOfInterests(id: id, poiRideID: rideId, poiPlaceID: String(cString: placeid!), poiProvider: String(cString: poiProvider!), poiName: String(cString: poiName!), poiVicinity: String(cString: poiVicinity!), poiRating:Double(poiRating), poiLatitude: Double(poiLatitude), poiLongitude: Double(poiLongitude), poiCosLatitude: Double(poiCosLatitude), poiSinLatitude: Double(poiSinLatitude), poiCosLongitude: Double(poiCosLongitude), poiSinLongitude: Double(poiSinLongitude), poiAltitude: Double(poiAltitude), poiCategories: String(cString: poiCategories!), poiOpeningHours: String(cString: poiOpeningHours!), poiPhoneNumber: String(cString: poiPhoneNumber!), poiAddress: String(cString: poiAddress!))
            
            pois.append(objPoi)
            
        } while (sqlite3_step(queryStatement) != SQLITE_DONE)
        
        return pois
    }
    
    
    
    func fetchPoIByCriteriaFromDB(querySql: String,id: Int32,catagory:String,lat:Double,lang:Double,radius:Double) -> [Any] {
        
        let coslat =  cosd(degrees: deg2rad(deg: lat))
        let sinlat =  sind(degrees: deg2rad(deg: lat))
        let coslng =  cosd(degrees: deg2rad(deg: lang))
        let sinlng =  sind(degrees: deg2rad(deg: lang))
        
        let format = "(\(coslat) * \(SQliteDBConstants.KEY_POI_COSLAT) * (\(coslng) * \(SQliteDBConstants.KEY_POI_COSLNG) + \(sinlng) * \(SQliteDBConstants.KEY_POI_SINLNG)) + \(sinlat) * \(SQliteDBConstants.KEY_POI_SINLAT))"
        
        
      //  print("selection ",format)
        
        var sqlQuery =  "SELECT *,\(format) AS distance FROM \(SQLTableType.POINT_OF_INTEREST.rawValue) WHERE \(SQliteDBConstants.KEY_RIDE_ID) = \(id) AND \(SQliteDBConstants.KEY_POI_CATEGORIES) LIKE '%\(catagory)%' GROUP BY \(SQliteDBConstants.KEY_POI_PLACE_ID) ORDER BY distance DESC;"
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        
        guard let queryStatement = try? db?.prepareStatement(sql: sqlQuery) else {
            return []
        }
        
        defer {
            sqlite3_finalize(queryStatement)
        }

        guard sqlite3_step(queryStatement) == SQLITE_ROW else {
            return []
        }
        
        var pois = [PointsOfInterests]()
        repeat {
            let id              = sqlite3_column_int(queryStatement, 0)
            let rideId          = sqlite3_column_int(queryStatement, 1)
            let placeid         = sqlite3_column_text(queryStatement, 2)
            let poiName         = sqlite3_column_text(queryStatement, 3)
            let poiProvider     = sqlite3_column_text(queryStatement, 4)
            let poiVicinity     = sqlite3_column_text(queryStatement, 5)
            let poiRating       = sqlite3_column_double(queryStatement,6)
            let poiLatitude     = sqlite3_column_double(queryStatement,7)
            let poiLongitude    = sqlite3_column_double(queryStatement,8)
            let poiCosLatitude  = sqlite3_column_double(queryStatement,9)
            let poiCosLongitude = sqlite3_column_double(queryStatement,10)
            let poiSinLatitude  = sqlite3_column_double(queryStatement,11)
            let poiSinLongitude = sqlite3_column_double(queryStatement,12)
            let poiAltitude     = sqlite3_column_double(queryStatement,13)
            let poiCategories   = sqlite3_column_text(queryStatement, 14)
            let poiOpeningHours = sqlite3_column_text(queryStatement, 15)
            let poiPhoneNumber  = sqlite3_column_text(queryStatement, 16)
            let poiAddress      = sqlite3_column_text(queryStatement, 17)
            let aCos            = sqlite3_column_double(queryStatement, 18)

            
            let objPoi = PointsOfInterests(id: id, poiRideID: rideId, poiPlaceID: String(cString: placeid!), poiProvider: String(cString: poiProvider!), poiName: String(cString: poiName!), poiVicinity: String(cString: poiVicinity!), poiRating:Double(poiRating), poiLatitude: Double(poiLatitude), poiLongitude: Double(poiLongitude), poiCosLatitude: Double(poiCosLatitude), poiSinLatitude: Double(poiSinLatitude), poiCosLongitude: Double(poiCosLongitude), poiSinLongitude: Double(poiSinLongitude), poiAltitude: Double(poiAltitude), poiCategories: String(cString: poiCategories!), poiOpeningHours: String(cString: poiOpeningHours!), poiPhoneNumber: String(cString: poiPhoneNumber!), poiAddress: String(cString: poiAddress!))
            
//            print("(cString: placeid!) - ",String(cString: placeid!))

            let convertValue = convertPartialDistanceToKm(result: aCos)
            if convertValue <= radius {
               // print("id ",id," place ID- ",String(cString: placeid!))
                pois.append(objPoi)
            }
            
        } while (sqlite3_step(queryStatement) != SQLITE_DONE)
        
        return pois
    }

    
    
    public var debugDescription: String {
        return "ID: \(id) | POI ID: \(poiRideID) POI PROVIDER: \(poiProvider) | PlaceID: \(poiPlaceID) PoiName: \(poiName) | Vicinity: \(poiVicinity) Rating: \(poiRating) | \(poiLatitude)\(poiLongitude) | \(poiCosLatitude)\(poiSinLatitude) | \(poiCosLongitude)\(poiSinLongitude) | \(poiAltitude) Categories: \(poiCategories) | \(poiOpeningHours)\(poiPhoneNumber) | \(poiAddress)"
    }
    
}

