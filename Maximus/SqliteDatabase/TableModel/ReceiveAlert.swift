//
//  ReceiveAlert.swift
//  Maximus
//
//  Created by kpit on 22/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import SQLite

extension ReceiveAlert: SQLTable {
    static var createStatement: String {
        return "CREATE TABLE \(SQLTableType.RECEIVE_ALERT.rawValue)(" +
            "\(SQliteReceiveAlert.KEY_ALERT_ID) INTEGER PRIMARY KEY NOT NULL," +
            "\(SQliteReceiveAlert.KEY_RIDE_ID) INTEGER," +
            "\(SQliteReceiveAlert.KEY_UUID) TEXT," +
            "\(SQliteReceiveAlert.KEY_ALERT_STATUS) TEXT," +
            "\(SQliteReceiveAlert.KEY_ORIGANATED_BIKER_ID) INTEGER," +
            "\(SQliteReceiveAlert.KEY_ORIGANATED_BIKER_NAME) TEXT," +
            "\(SQliteReceiveAlert.KEY_ALERT_SHORT_NAME) TEXT," +
            "\(SQliteReceiveAlert.KEY_CATEGORY) TEXT," +
            "\(SQliteReceiveAlert.KEY_PRIORITY) INTEGER," +
            "\(SQliteReceiveAlert.KEY_HELPED) INTEGER," +
            "\(SQliteReceiveAlert.KEY_LOCATION) TEXT" +
        ");"
    }
}

class ReceiveAlert: CustomDebugStringConvertible {
    
    var alertId: Int32 = 0
    var alertRideId:Int32 = 0
    var alertUUID:String = ""
    var alertStatus:String = ""
    var alertOriginatedByBikerId:Int32 = 0
    var alertOriginatedByBikerName = ""
    var alertShortName = ""
    var alertCategory = ""
    var alertPriority:Int32 = 0
    var alertHelped:Int32 = 0
    var alertLocation:String = "[0.0,0.0]"
    let alertDbManager = AlertDatabaseManager()

    
    public init(alertId:Int32, alertStatus:String ){
        self.alertId = alertId
        self.alertStatus = alertStatus
    }
    
     public init(alertId:Int32? = 0, alertRideId:Int32 = 0, alertUUID:String = "none", alertStatus:String = "none", alertOriginatedByBikerId:Int32 = 0, alertOriginatedByBikerName:String = "none", alertShortName:String = "none", alertCategory:String = "none", alertPriority:Int32 = 0,alertHelped:Int32 = 0, alertLocation:String = "[0.0,0.0]"){
        
        self.alertId = alertId!
        self.alertRideId = alertRideId
        self.alertUUID = alertUUID
        self.alertStatus = alertStatus
        self.alertOriginatedByBikerId = alertOriginatedByBikerId
        self.alertOriginatedByBikerName = alertOriginatedByBikerName
        self.alertShortName = alertShortName
        self.alertCategory = alertCategory
        self.alertPriority = alertPriority
        self.alertHelped = alertHelped
        self.alertLocation = alertLocation
    }
    
    
    //MARK: ******* INSERT *******
    func insertIntoTable(insertSql: String,data:Any) throws {
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        let insertStatement = try db?.prepareStatement(sql: insertSql)//prepareStatement(sql: insertSql)
        defer {
            sqlite3_finalize(insertStatement)
        }
        
        
        let alerts = data as! ReceiveAlert
        //        if !alerts.alertType.hasPrefix("ChIJ") {
        //            print(alerts)
        //        }
        //
        
        guard sqlite3_bind_int(insertStatement, 1,alerts.alertId) == SQLITE_OK  &&
            sqlite3_bind_int(insertStatement, 2, alerts.alertRideId) == SQLITE_OK &&
            sqlite3_bind_text(insertStatement, 3, alerts.alertUUID._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK &&
            sqlite3_bind_text(insertStatement, 4, alerts.alertStatus._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK &&
            sqlite3_bind_int(insertStatement, 5,alerts.alertOriginatedByBikerId) == SQLITE_OK &&
            sqlite3_bind_text(insertStatement, 6, alerts.alertOriginatedByBikerName._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK &&
            sqlite3_bind_text(insertStatement, 7, alerts.alertShortName._bridgeToObjectiveC().utf8String, -1,nil) == SQLITE_OK &&
            sqlite3_bind_text(insertStatement, 8, alerts.alertCategory._bridgeToObjectiveC().utf8String, -1,nil) == SQLITE_OK &&
            sqlite3_bind_int(insertStatement, 9, alerts.alertPriority) == SQLITE_OK &&
            sqlite3_bind_int(insertStatement, 10, alerts.alertHelped) == SQLITE_OK &&
            sqlite3_bind_text(insertStatement, 11, alerts.alertLocation._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK
            else {
                throw SQLiteError.Bind(message: (db?.errorMessage)!)
        }
        
        guard sqlite3_step(insertStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: (db?.errorMessage)!)
        }
        
        print("Successfully inserted row Receiving alert.")
        //calling show received alert after inserting in local database
        let context = AlertContext()
        context.changeStateToShowReceivedAlert()
    }
    
    
    //MARK: ******** Update ********
    
    
    func updateReceivedAlertStatus(insertSql: String,data:Any) throws {
        
        print("Data ",data)
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        let insertStatement = try db?.prepareStatement(sql: insertSql)
        defer {
            sqlite3_finalize(insertStatement)
        }
        
        let alertsObj = data as! ReceiveAlert
        
        
        guard  sqlite3_bind_text(insertStatement, 1, alertsObj.alertStatus._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK  &&
            sqlite3_bind_int(insertStatement, 2,alertsObj.alertId) == SQLITE_OK
            else {
                throw SQLiteError.Bind(message: (db?.errorMessage)!)
        }
        
        guard sqlite3_step(insertStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: (db?.errorMessage)!)
        }
        
        print("Successfully Updated row Receiving Alert.")
        let arrResult = alertDbManager.getAlertsFromReceivedAlertTableWithStatus(status:AlertConstant.DB_STATE_RECEIVED)
        if arrResult.count != 0 && AlertStateShowAlert.alertUpdateToClosedState{
            //after updating database,showing alerts only when there are any open alerts in received state.
            let context = AlertContext()
            context.changeStateToCheckAndShowAlertState()
        }
        AlertStateShowAlert.alertUpdateToClosedState = true
    }
    
    //MARK: ******* DELETE *******
    func deleteTable(deleteSql: String) throws {
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        let deleteStatement = try db?.prepareStatement(sql: deleteSql)
        defer {
            sqlite3_finalize(deleteStatement)
        }
        
        guard sqlite3_step(deleteStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: (db?.errorMessage)!)
        }
        
        print("Successfully table deleted Receving Alert")
    }
    
    func deleteAllRecevieAlertsWithRespectiveAlertID(deleteSql: String,alertId: Int32) throws {
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        let deleteStatement = try db?.prepareStatement(sql: deleteSql)
        defer {
            sqlite3_finalize(deleteStatement)
        }
        
        guard sqlite3_bind_int(deleteStatement, 1, alertId) == SQLITE_OK else {
            throw SQLiteError.Bind(message: (db?.errorMessage)!)
        }
        
        guard sqlite3_step(deleteStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: (db?.errorMessage)!)
        }
        
        print("Successfully deleted row.")
    }
    
    //MARK: ******* FETCH *******
    
    func fetchAllReceivedAlertRecordsWithField(querySql: String, field:String) throws -> [Any] {
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        guard let queryStatement = try? db?.prepareStatement(sql: querySql) else {
            return []
        }
        
        defer {
            sqlite3_finalize(queryStatement)
        }
        
        
        guard  sqlite3_bind_text(queryStatement, 1,field._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK else {
            throw SQLiteError.Bind(message: (db?.errorMessage)!)
        }
        
        guard sqlite3_step(queryStatement) == SQLITE_ROW else {
            return []
        }
        
       
        
        var arrSendingAlert = [ReceiveAlert]()
        repeat {
            let alertID                     = sqlite3_column_int(queryStatement, 0)
            let alertRideId                 = sqlite3_column_int(queryStatement, 1)
            let alertUUID                   = sqlite3_column_text(queryStatement, 2)
            let alertStatus                 = sqlite3_column_text(queryStatement, 3)
            let alertOriginatedByBikerId    = sqlite3_column_int(queryStatement, 4)
            let alertOriginatedByBikerName  = sqlite3_column_text(queryStatement, 5)
            let alertShortName              = sqlite3_column_text(queryStatement, 6)
            let alertCategory               = sqlite3_column_text(queryStatement, 7)
            let alertPriority               = sqlite3_column_int(queryStatement, 8)
            let alertHelped                 = sqlite3_column_int(queryStatement, 9)
            let alertLocation               = sqlite3_column_text(queryStatement, 10)
            
            let obj = ReceiveAlert(alertId: alertID,
                                   alertRideId: alertRideId,
                                   alertUUID: String(cString:alertUUID!),
                                   alertStatus: String(cString:alertStatus!),
                                   alertOriginatedByBikerId: alertOriginatedByBikerId,
                                   alertOriginatedByBikerName: String(cString:alertOriginatedByBikerName!),
                                   alertShortName: String(cString:alertShortName!),
                                   alertCategory: String(cString:alertCategory!),
                                   alertPriority: alertPriority,
                                   alertHelped: alertHelped,
                                   alertLocation: String(cString:alertLocation!))
                
            arrSendingAlert.append(obj)
            
        } while (sqlite3_step(queryStatement) != SQLITE_DONE)
        
        return arrSendingAlert
    }

    
    
//    func fetchAllReceiveAlertRecords(querySql: String) -> [Any] {
//        
//        var db : DBManager?
//        do {
//            db = try DBManager.open(path: part1DbPath)
//        } catch let message {
//            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
//            print(message)
//        }
//        
//        
//        guard let queryStatement = try? db?.prepareStatement(sql: querySql) else {
//            return []
//        }
//        
//        defer {
//            sqlite3_finalize(queryStatement)
//        }
//        
//        guard sqlite3_step(queryStatement) == SQLITE_ROW else {
//            return []
//        }
//        
//        var arrReceiveAlert = [ReceiveAlert]()
//        repeat {
//            let alertID         = sqlite3_column_int(queryStatement, 0)
//            let alertType      = sqlite3_column_text(queryStatement, 1)
//            let alertName    = sqlite3_column_text(queryStatement, 2)
//            
//            
//            let obj = ReceiveAlert(id: alertID, alertType: String(cString:alertType!) != nil ? String(cString:alertType!):"", alertName: String(cString:alertName!) != nil ? String(cString:alertName!):"")
//            
//            arrReceiveAlert.append(obj)
//            
//        } while (sqlite3_step(queryStatement) != SQLITE_DONE)
//        
//        return arrReceiveAlert
//    }
    
    
    public var debugDescription: String {
       return "\(alertId) | \(alertRideId) | \(alertUUID) | \(alertStatus) | \(alertOriginatedByBikerId) | \(alertOriginatedByBikerName) | \(alertShortName) | \(alertCategory) | \(alertPriority) | \(alertHelped)"
    }
    
}


