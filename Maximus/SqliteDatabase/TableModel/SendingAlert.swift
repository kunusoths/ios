//
//  SendingAlert.swift
//  Maximus
//
//  Created by kpit on 23/08/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import SQLite

extension SendingAlert: SQLTable {
    
    static var createStatement: String {
        return "CREATE TABLE \(SQLTableType.SENDING_ALERT.rawValue)(" +
            "\(SQliteSendingAlert.KEY_ALERT_ID) INTEGER PRIMARY KEY NOT NULL," +
            "\(SQliteSendingAlert.KEY_TRIGGERED_BY_ID) INTEGER," +
            "\(SQliteSendingAlert.KEY_RIDE_ID) INTEGER," +
            "\(SQliteSendingAlert.KEY_ALERT_SHORT_NAME) TEXT," +
            "\(SQliteSendingAlert.KEY_CATEGORY) TEXT," +
            "\(SQliteSendingAlert.KEY_PRIORITY) INTEGER," +
            "\(SQliteSendingAlert.KEY_ALERT_STATUS) TEXT," +
            "\(SQliteSendingAlert.KEY_HELP_RECEIVED) TEXT," +
            "\(SQliteSendingAlert.KEY_RESPONDANT_BIKER_NAME) TEXT" +
        ");"
    }
}

class SendingAlert: CustomDebugStringConvertible {
    
    var alertId: Int32 = 0
    var alertTriggeredById: Int32 = 0
    var alertRideId: Int32 = 0
    var alertShortName:String = ""
    var alertCategory:String = ""
    var alertPriority:Int32 = 0
    var alertStatus:String = ""
    var alertHelpReceived:String = ""
    var alertRespondantBikerName:String = ""
    
    public init(alertId:Int32, alertStatus:String ){
        self.alertId = alertId
        self.alertStatus = alertStatus
    }
    
    public init(alertId:Int32, helperBiker:String ){
        self.alertId = alertId
        self.alertRespondantBikerName = helperBiker
    }

    
    public init(alertId: Int32? = 0 ,alertTriggeredById: Int32? = 0 ,alertRideId:Int32? = 0 ,alertShortName:String? = "none" ,alertCategory:String = "none",alertPriority:Int32 = 0 ,alertStatus:String = "none" ,alertHelpReceived:String = "none" ,alertRespondantBikerName:String = "none")
    {
        self.alertId = alertId!
        self.alertTriggeredById = alertTriggeredById!
        self.alertRideId = alertRideId!
        self.alertShortName = alertShortName!
        self.alertCategory   = alertCategory
        self.alertPriority = alertPriority
        self.alertStatus = alertStatus
        self.alertHelpReceived = alertHelpReceived
        self.alertRespondantBikerName = alertRespondantBikerName
    }
    
    
    //MARK: ******* INSERT *******
    func insertIntoTable(insertSql: String,data:Any) throws {
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        let insertStatement = try db?.prepareStatement(sql: insertSql)//prepareStatement(sql: insertSql)
        defer {
            sqlite3_finalize(insertStatement)
        }
        
        let alerts = data as! SendingAlert
        /*if !alerts.alertType.hasPrefix("ChIJ") {
         print(alerts)
         }*/
        //        print("poi_id: \(poi)")
        //        print("poi.poiPlaceID - ",poi.poiPlaceID)
         
        
      guard sqlite3_bind_int(insertStatement, 1,alerts.alertId) == SQLITE_OK  &&
            sqlite3_bind_int(insertStatement, 2, alerts.alertTriggeredById) == SQLITE_OK &&
            sqlite3_bind_int(insertStatement, 3, alerts.alertRideId) == SQLITE_OK &&
            sqlite3_bind_text(insertStatement, 4, alerts.alertShortName._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK &&
            sqlite3_bind_text(insertStatement, 5,alerts.alertCategory._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK &&
            sqlite3_bind_int(insertStatement, 6, alerts.alertPriority) == SQLITE_OK &&
            sqlite3_bind_text(insertStatement, 7, alerts.alertStatus._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK &&
            sqlite3_bind_text(insertStatement, 8, alerts.alertHelpReceived._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK &&
            sqlite3_bind_text(insertStatement, 9, alerts.alertRespondantBikerName._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK
            else {
                throw SQLiteError.Bind(message: (db?.errorMessage)!)
        }
        
        guard sqlite3_step(insertStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: (db?.errorMessage)!)
        }
        
        print("Successfully inserted row Sending Alert.")
    }
    
    
    //MARK: ******** Update ********
    func updateSentAlertStatus(insertSql: String,data:Any) throws {
        
        print("Data ",data)
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        let insertStatement = try db?.prepareStatement(sql: insertSql)//prepareStatement(sql: insertSql)
        defer {
            sqlite3_finalize(insertStatement)
        }
        
        let alertsObj = data as! SendingAlert
        

        guard  sqlite3_bind_text(insertStatement, 1, alertsObj.alertStatus._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK  &&
               sqlite3_bind_int(insertStatement, 2,alertsObj.alertId) == SQLITE_OK
               else {
                throw SQLiteError.Bind(message: (db?.errorMessage)!)
        }
        
        guard sqlite3_step(insertStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: (db?.errorMessage)!)
        }
        
        print("Successfully Updated row Sending Alert.")
    }
    
    func updateSentAlertRespondant(insertSql: String,data:Any) throws {
        
        print("Data ",data)
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        let insertStatement = try db?.prepareStatement(sql: insertSql)//prepareStatement(sql: insertSql)
        defer {
            sqlite3_finalize(insertStatement)
        }
        
        let alertsObj = data as! SendingAlert
        
        
        guard  sqlite3_bind_text(insertStatement, 1, alertsObj.alertRespondantBikerName._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK  &&
            sqlite3_bind_int(insertStatement, 2,alertsObj.alertId) == SQLITE_OK
            else {
                throw SQLiteError.Bind(message: (db?.errorMessage)!)
        }
        
        guard sqlite3_step(insertStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: (db?.errorMessage)!)
        }
        
        print("Successfully Updated row Sending Alert.")
    }

    
    
    
    
    //MARK: ******* DELETE *******
    func deleteTable(deleteSql: String) throws {
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        let deleteStatement = try db?.prepareStatement(sql: deleteSql)
        defer {
            sqlite3_finalize(deleteStatement)
        }
        
        guard sqlite3_step(deleteStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: (db?.errorMessage)!)
        }
        
        print("Successfully table deleted Sending Alert")
    }
    
    func deleteAllSendingAlertsWithRespectiveAlertID(deleteSql: String,alertID: Int32) throws {
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        let deleteStatement = try db?.prepareStatement(sql: deleteSql)
        defer {
            sqlite3_finalize(deleteStatement)
        }
        
        guard sqlite3_bind_int(deleteStatement, 1, alertID) == SQLITE_OK else {
            throw SQLiteError.Bind(message: (db?.errorMessage)!)
        }
        
        guard sqlite3_step(deleteStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: (db?.errorMessage)!)
        }
        
        print("Successfully deleted row.")
    }
    
    //MARK: ******* FETCH *******
    func fetchAllSendingAlertRecords(querySql: String) -> [Any] {
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        
        guard let queryStatement = try? db?.prepareStatement(sql: querySql) else {
            return []
        }
        
        defer {
            sqlite3_finalize(queryStatement)
        }
        
        guard sqlite3_step(queryStatement) == SQLITE_ROW else {
            return []
        }
        
        var arrSendingAlert = [SendingAlert]()
        repeat {
            let alertID                 = sqlite3_column_int(queryStatement, 0)
            let alertTriggeredById      = sqlite3_column_int(queryStatement, 1)
            let alertRideId             = sqlite3_column_int(queryStatement, 2)
            let alertShortName          = sqlite3_column_text(queryStatement, 3)
            let alertCategory   = sqlite3_column_text(queryStatement, 4)
            let alertPriority   = sqlite3_column_int(queryStatement, 5)
            let alertStatus     = sqlite3_column_text(queryStatement, 6)
            let alertHelpReceived  = sqlite3_column_text(queryStatement, 7)
            let alertRespondantBikerName  = sqlite3_column_text(queryStatement, 8)
            
            
            let obj = SendingAlert(alertId: alertID,
                                   alertTriggeredById: alertTriggeredById,
                                   alertRideId: alertRideId,
                                   alertShortName: String(cString:alertShortName!),
                                   alertCategory: String(cString:alertCategory!),
                                   alertPriority: alertPriority,
                                   alertStatus: String(cString:alertStatus!),
                                   alertHelpReceived: String(cString:alertHelpReceived!),
                                   alertRespondantBikerName: String(cString:alertRespondantBikerName!))
            arrSendingAlert.append(obj)
            
        } while (sqlite3_step(queryStatement) != SQLITE_DONE)
        
        return arrSendingAlert
    }
    
    
    func fetchAllSendingAlertRecordsWithField(querySql: String, field:String) throws -> [Any] {
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
       
        
        
        
        guard let queryStatement = try? db?.prepareStatement(sql: querySql) else {
            return []
        }
        
        defer {
            sqlite3_finalize(queryStatement)
        }
        
        
        guard  sqlite3_bind_text(queryStatement, 1,field._bridgeToObjectiveC().utf8String, -1, nil) == SQLITE_OK else {
            throw SQLiteError.Bind(message: (db?.errorMessage)!)
        }

        guard sqlite3_step(queryStatement) == SQLITE_ROW else {
            return []
        }
        
        var arrSendingAlert = [SendingAlert]()
        repeat {
            let alertID                 = sqlite3_column_int(queryStatement, 0)
            let alertTriggeredById      = sqlite3_column_int(queryStatement, 1)
            let alertRideId             = sqlite3_column_int(queryStatement, 2)
            let alertShortName          = sqlite3_column_text(queryStatement, 3)
            let alertCategory   = sqlite3_column_text(queryStatement, 4)
            let alertPriority   = sqlite3_column_int(queryStatement, 5)
            let alertStatus     = sqlite3_column_text(queryStatement, 6)
            let alertHelpReceived  = sqlite3_column_text(queryStatement, 7)
            let alertRespondantBikerName  = sqlite3_column_text(queryStatement, 8)
            
            
            let obj = SendingAlert(alertId: alertID,
                                   alertTriggeredById: alertTriggeredById,
                                   alertRideId: alertRideId,
                                   alertShortName: String(cString:alertShortName!),
                                   alertCategory: String(cString:alertCategory!),
                                   alertPriority: alertPriority,
                                   alertStatus: String(cString:alertStatus!),
                                   alertHelpReceived: String(cString:alertHelpReceived!),
                                   alertRespondantBikerName: String(cString:alertRespondantBikerName!))
            arrSendingAlert.append(obj)
            
        } while (sqlite3_step(queryStatement) != SQLITE_DONE)
        
        return arrSendingAlert
    }
    
    
    
    public var debugDescription: String {
        return "\(alertId) | \(alertTriggeredById) | \(alertRideId) | \(alertShortName) | \(alertCategory) | \(alertPriority) \(alertStatus) | \(alertHelpReceived) | \(alertRespondantBikerName)"
    }
    
}


