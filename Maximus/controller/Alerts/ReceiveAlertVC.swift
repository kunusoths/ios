//
//  ReceiveAlertVC.swift
//  Maximus
//
//  Created by kpit on 07/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class ReceiveAlertVC: UIViewController,UserManagerDelegate {
    
    @IBOutlet weak var btnDismiss: UIButton!
    @IBOutlet weak var btnIWantToHelp: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblBikerName: UILabel!
    @IBOutlet weak var lblAlertType: UILabel!
    var userObject = UserManagerHandler()
    var bikerName:String?
    var generatedAlertId:String?
    var generatedAlertType:String?
    var generatedBikerId:String?
    var uuid:String?
    let sinkManager = SinkManager.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar?.isHidden = false
        self.navigationItem.title = "ALERTS"
        navigationItem.hidesBackButton = true
        
        btnDismiss.layer.borderColor = UIColor(colorLiteralRed: 213/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor//red.cgColor
        
        btnDismiss.layer.borderWidth  = 1.0
        
        if let bikerId = generatedBikerId {
            userObject.userManagerDelegate = self
            userObject.getBiker(bikeID:bikerId.toInt()!)
        }
        
        sinkManager.addObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_CLOSED, selectorName: self.catchNotificationAlertClosed)
        sinkManager.addObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_DISCARDED, selectorName: self.catchNotificationAlertDiscarded)
        sinkManager.addObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_ANOTHER_ALERT_RECEIVED, selectorName: self.catchNotificationAlertAnother)
        
        sinkManager.addObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_ALERT_RESPONSE_FAILURE, selectorName: self.catchNotificationAlertResponseFailure)
        
        
        
        imgView.layer.cornerRadius = 63//imgView.frame.size.height/2
        imgView.clipsToBounds = true
        
        
        if let name = bikerName{
            lblBikerName.text = "\(name) needs help"
        }else{
            lblBikerName.text = "Rider needs help"
        }
        
        if let alertType = generatedAlertType{
            
            switch alertType {
            case AlertConstant.ALERT_TYPE_NO_FUEL:
                self.lblAlertType.text = AlertConstant.TYPE_NO_FUEL_STRING
                
            case AlertConstant.ALERT_TYPE_ACCIDENT:
                self.lblAlertType.text = AlertConstant.TYPE_ACCIDENT_STRING
                
            case AlertConstant.ALERT_TYPE_PUNCTURE:
                self.lblAlertType.text = AlertConstant.TYPE_PUNCTURE_STRING
                
            case AlertConstant.ALERT_TYPE_SEVERE_CONDITION:
                self.lblAlertType.text = AlertConstant.TYPE_SEVERE_CONDITION_STRING
            default:
                break
            }
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        sinkManager.removeObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_CLOSED)
        sinkManager.removeObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_DISCARDED)
        sinkManager.removeObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_ANOTHER_ALERT_RECEIVED)
       sinkManager.removeObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_ALERT_RESPONSE_FAILURE)
        
    }
    
    func catchNotificationAlertResponseFailure(notification:Notification) -> Void{
        btnIWantToHelp.isEnabled = true
        btnDismiss.isEnabled = true
        //self.dismiss(animated: true, completion: nil)
    }

    
    func catchNotificationAlertClosed(notification:Notification) -> Void{
       self.dismiss(animated: true, completion: nil)
    }
    
    func catchNotificationAlertDiscarded(notification:Notification) -> Void{
       self.dismiss(animated: true, completion: nil)
    }
    func catchNotificationAlertAnother(notification:Notification) -> Void{
        self.dismiss(animated: true, completion: nil)
    }

    
    
   @IBAction func btnIWantToHelpPressed(_ sender: Any) {
      if let alertId = generatedAlertId {
            if let alertUuid = uuid {
                
                btnIWantToHelp.isEnabled = false
                btnDismiss.isEnabled = false
                
                let alertContext = AlertContext()
                alertContext.changeStateToAttemptAck(responseId: AlertConstant.ALERT_RESPONSE_STATUS_ACKNOWLEDGED,
                                                     generatedAlertId: alertId,
                                                     uuid: alertUuid)
            }
        }
    }
    
    @IBAction func btnDismissPressed(_ sender: Any) {
        if let alertId = generatedAlertId {
            if let alertUuid = uuid {
                
                btnIWantToHelp.isEnabled = false
                btnDismiss.isEnabled = false
                
                let alertContext = AlertContext()
                alertContext.changeStateToAttemptAck(responseId: AlertConstant.ALERT_RESPONSE_STATUS_DISMISSED,
                                                     generatedAlertId: alertId,
                                                     uuid: alertUuid)
    
            }
        }
        
    }
    
    
    
    //MARK: - UserManager Delegates
    func onGetBikerSuccess(data:[String:Any]) {
        
        if let biker:Biker = data["data"] as? Biker {
            if let imageUrl = biker.profile_image_url{
                imgView.imageWithUrl(url: imageUrl)
                
            }
            //self.bikerData = biker
            //self.updateUI(biker: biker)
        }
    }
    
    
    func onFailure(Error: [String : Any]) {
        let errModel:ErrorModel = Error["data"] as! ErrorModel
        PopUpUtility().showDefaultPopUpWith(message: (errModel.description)!)
    }
    
    
}
