//
//  ResumeRideVC.swift
//  Maximus
//
//  Created by kpit on 07/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class ResumeRideVC: UIViewController,AlertApiManagerDelegate {
    
    let alertManager:AlertApiManager = AlertApiManager()
    let sinkManager = SinkManager.sharedInstance
    var generatedAlertId:String?
    var helperBikerName:String?
    
    @IBOutlet weak var lblHelperNames: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar?.isHidden = false
        self.navigationItem.title = "ALERTS"
        navigationItem.hidesBackButton = true
        alertManager.alertManagerDelegate = self
        if let bikerNames = helperBikerName{
            if bikerNames.characters.contains(","){
                var arrNames = bikerNames.split(",")
                if arrNames.count > 0{
                    lblHelperNames.text = "\(arrNames[0]) and buddies are on the way to help you."
                }
            }else{
                lblHelperNames.text = "\(bikerNames) is on the way to help you."
            }
        }
    }
    
    func showPopUp(message:String){
        let alert = UIAlertController(title: "MaximusPro", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func btnResumeRidePressed(_ sender: Any) {
        if let alertId = generatedAlertId {
            JourneyManager.sharedInstance.setAlertSendingType(type: AlertSendingType.Unknown)
            alertManager.closeGeneratedAlert(generatedAlertId: alertId.toInt()!)
        }
    }
    
    //MARK:- AlertAPIManager Delegates
    func closeAlertSuccess(alertObj: [String : Any]) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func onFailure(Error: [String : Any]) {
        let errModel:ErrorModel = Error["data"] as! ErrorModel
        self.showPopUp(message: (errModel.description)!)
    }
    
    
    
}
