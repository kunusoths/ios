//
//  OptOutVC.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 01/11/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import GoogleMaps

class OptOutVC: UIViewController,CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: GMSMapView!
    //@IBOutlet weak var lblTimeToReach: UILabel!
    @IBOutlet weak var lblKMS: UILabel!
    
    var rideID:Int?
    let sinkManager = SinkManager.sharedInstance
    var alertId:String?
    
    var markerCurrentLocation: GMSMarker!
    var markerOrigin: GMSMarker!
    var markerDestination: GMSMarker!
    var routePolyline: GMSPolyline!
    var mapTasks: GooglePlacesService!
    var locationManager:CLLocationManager?
    
    
    var startAddress = ""
    var endAddress = "'"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = CLLocationManager()
        self.view.backgroundColor = AppTheme.THEME_COLOR
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.title = "SOS LOCATION"
        
        self.mapView.setDefaultCameraPosition()
        
        self.mapTasks = GooglePlacesService()
        self.getCurrentLocation()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sinkManager.addObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_CLOSED, selectorName: self.catchNotificationAlertClosed)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
         sinkManager.removeObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_CLOSED)
   
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let alertId = self.alertId {
            let alerts:[ReceiveAlert] = AlertDatabaseManager().getAlertFromReceivedAlertTableWithId(Id: alertId) as! [ReceiveAlert]
            if(alerts.count > 0){
                
                let bikerLoc = alerts[0].alertLocation
                //print(bikerLoc)
                //let bikeLoc = "[19.0760,72.8777]"
                var arr = bikerLoc.split(",")
                if arr.count == 2 {
                    _ = String(arr[0].characters.removeFirst())
                    _ = String(arr[1].characters.removeLast())
                    self.getAddressForBikerLocation(latitude:arr[0],longitude:arr[1])
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    
    func catchNotificationAlertClosed(notification:Notification) -> Void{
        print("Alert Closed")
        // vwOptOutStrtNav.isHidden = true
        //self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:Private Methods
    
    @IBAction func btnOptOut(_ sender: UIButton) {
        
        let strTitle = "Are you sure, you don't wan't to help?"
        let alertController = UIAlertController(title: strTitle, message: "This action will dismiss the alert, and you won't be able to track your friend.", preferredStyle: UIAlertControllerStyle.alert)
      
        
        let doneAction = UIAlertAction(title: "Done", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
        
        let alertDbManager = AlertDatabaseManager()
        let arrResult = alertDbManager.getAlertsFromReceivedAlertTableWithStatus(status: AlertConstant.DB_STATE_ACKNOWLEDGED)
        let alertsArray = arrResult as! [ReceiveAlert]
        if alertsArray.count > 0{
            
            if let rideId = self.rideID{
                if let ackRideId = alertsArray.get(at: 0)?.alertRideId{
                    
                    if (Int32(rideId) == ackRideId){
                        
                        if let alertId = alertsArray.get(at: 0)?.alertId{
                            if let alertUUID = alertsArray.get(at: 0)?.alertUUID{

                        JourneyManager.sharedInstance.setAlertSendingType(type: AlertSendingType.OptingOut)
                        let alertContext:AlertContext = AlertContext()
                                alertContext.changeStateToAttemptAck(responseId: AlertConstant.ALERT_RESPONSE_STATUS_DISMISSED,
                                                                     generatedAlertId: "\(alertId)",
                                                                     uuid: alertUUID)

                            }
                        }
                    }
                }
            }
        }
        self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(doneAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            
        }
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func getCurrentLocation()  {
        
        self.locationManager?.requestAlwaysAuthorization()
        self.locationManager?.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager?.delegate = self
            locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager?.startUpdatingLocation()
            //mapView.isMyLocationEnabled = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            self.locationManager?.stopUpdatingLocation()
            self.getAddressForCurrentLocation(latitude: "\(location.coordinate.latitude)", longitude: "\(location.coordinate.longitude)")
        }
    }
    
    
    func getAddressForCurrentLocation(latitude:String,longitude:String){
        
        self.mapTasks.getAddressForLatLng(latitude: latitude, longitude: longitude){ json in
            if let result = json["results"] as? NSArray {
                if result.count > 0{
                if let details = result[0] as? NSDictionary{
                    if let address = details["formatted_address"] as? String{
                        self.startAddress = address
                        if self.startAddress != "" && self.endAddress != ""{
                            self.configureAndDrawMarkersRoute(origin:self.startAddress,destination:self.endAddress)
                        }
                    }
                }
            }
        }
        }
        
    }
    
    func getAddressForBikerLocation(latitude:String,longitude:String){
        
        self.mapTasks.getAddressForLatLng(latitude: latitude, longitude: longitude){ json in
            if let result = json["results"] as? NSArray {
                if result.count > 0{
                    if let details = result[0] as? NSDictionary{
                        if let address = details["formatted_address"] as? String{
                            self.endAddress = address
                            if self.startAddress != "" && self.endAddress != ""{
                                self.configureAndDrawMarkersRoute(origin:self.startAddress,destination:self.endAddress)
                            }
                            
                        }
                    }
                }
            }
        }
        
    }
    
    func configureAndDrawMarkersRoute(origin:String,destination:String) {
        // draw route
        self.mapTasks.getDirections(origin, destination: destination, waypoints: nil, travelMode: nil, completionHandler: { (status, success) -> Void in
            if success {
                self.configureMapAndMarkersForRoute()
                self.drawRoute()
            }
            else {
                print(status)
            }
        })
    }
    
    func configureMapAndMarkersForRoute() {
        mapView.camera = GMSCameraPosition.camera(withTarget: mapTasks.originCoordinate, zoom: 12.5)
        
        if markerOrigin != nil {
            markerOrigin.map = nil
        }
        if markerDestination != nil {
            markerDestination.map = nil
        }
        
        
        markerOrigin = GMSMarker(position: self.mapTasks.originCoordinate)
        markerOrigin.map = self.mapView
        //markerOrigin.icon =  UIImage(named: "map")
        markerOrigin.title = self.mapTasks.originAddress
        markerOrigin.icon = GMSMarker.markerImage(with: UIColor.green)
        
        
        markerDestination = GMSMarker(position: self.mapTasks.destinationCoordinate)
        markerDestination.map = self.mapView
        //markerDestination.icon =  UIImage(named: "map")
        markerDestination.title = self.mapTasks.destinationAddress
    }
    
    
    func drawRoute() {
        let route = mapTasks.overviewPolyline["points"] as! String
        let path: GMSPath = GMSPath(fromEncodedPath: route)!
        routePolyline = GMSPolyline(path: path)
        routePolyline.strokeWidth = 2.0
        routePolyline.geodesic = true
        routePolyline.map = mapView
        routePolyline.strokeColor = AppTheme.THEME_ORANGE
        let bounds = GMSCoordinateBounds(coordinate: mapTasks.originCoordinate, coordinate: mapTasks.destinationCoordinate)
        self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))
        //self.lblTimeToReach.text = mapTasks.totalDuration
        self.lblKMS.text = mapTasks.totalDistance
        
        print(mapTasks.totalDistance)
        print(mapTasks.totalDistanceInMeters)
        //print(mapTasks.totalDuration)
        //print(mapTasks.totalDurationInSeconds)
        
    }
}
