//
//  MyAlertVC.swift
//  Maximus
//
//  Created by kpit on 06/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class MyAlertVC: UIViewController {
    
    //MARK: IB Outlets
    @IBOutlet weak var btnAccident: UIButton!
    @IBOutlet weak var btnPuncture: UIButton!
    @IBOutlet weak var btnNoFuel: UIButton!
    @IBOutlet weak var btnSevereCondition: UIButton!
    @IBOutlet weak var btnSendAlert: UIButton!
    
    //MARK: Properties
    let color_highlited_alert = UIColor(r: 244, g: 67, b: 54)
    let color_Non_highlited_alert = UIColor(r: 35, g: 54, b: 67)
    let color_highlited_btn = UIColor(r: 255, g: 75, b: 0)
    
    let sinkManager = SinkManager.sharedInstance
    var alertType:String = ""
    var isAutoGenerateAlert:Bool = false
    var alertCategory:CP_AlertTypeInfoTypeDef?
    var alertID:Int?
    
    var alertSentStatus:Bool = true
    
    override func viewDidLayoutSubviews() {
        btnAccident.roundView()
        btnNoFuel.roundView()
        btnPuncture.roundView()
        btnSevereCondition.roundView()
    }
    
    //MARK: Contructors
    init(alertCategory:CP_AlertTypeInfoTypeDef ,AlertID:Int ,isAutoGenerateAlert:Bool)
    {
        super.init(nibName: nil, bundle: nil)
        self.isAutoGenerateAlert = isAutoGenerateAlert
        self.alertCategory = alertCategory
        self.alertID = AlertID
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navBar?.isHidden = false
        self.navigationItem.title = "ALERTS"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Arrow White"), style: .plain, target: self, action: #selector(backButtonAction))
        
        
        JourneyManager.sharedInstance.setAlertSendingType(type: AlertSendingType.AboutToSent)
        JourneyManager.sharedInstance.setAlertModeState(state: AlertModeStatus.On)
        
        //If auto generate Alert from DM side then call to that alert
        if self.isAutoGenerateAlert {
            
            self.alertType = AlertApiManager().getTypeOfAlert(category: self.alertCategory!, alertID: self.alertID!)
            
            switch self.alertType {
            case AlertConstant.ALERT_TYPE_SEVERE_CONDITION:
                self.btnSevereConditionAlertPressed(self)
                break
                
            case AlertConstant.ALERT_TYPE_PUNCTURE:
                self.btnPunctureAlertPressed(self)
                break
                
            case AlertConstant.ALERT_TYPE_NO_FUEL:
                self.btnNoFuelAlertPressed(self)
                break
                
            case AlertConstant.ALERT_TYPE_ACCIDENT:
                self.btnAccidentAlertPressed(self)
                break
                
            default:
                print("No Alert Type Found")
            }
            self.btnSendAlertPressed(self)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.btnSendAlert.isEnabled = true
        
        sinkManager.addObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_GENERATED_SUCESSFULLY, selectorName: self.catchNotificationGenerateSuccess)
        sinkManager.addObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_GENERATE_FAILURE, selectorName: self.catchNotificationGenerateFail)
        sinkManager.addObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_ACK_WAIT_TIMEDOUT, selectorName: self.catchNotificationAckWaitTimedOut)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        sinkManager.removeObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_GENERATED_SUCESSFULLY)
        sinkManager.removeObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_GENERATE_FAILURE)
        sinkManager.removeObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_ACK_WAIT_TIMEDOUT)
        
        DispatchQueue.main.async{
            LoadingHandler.shared.stoploading()
        }
        
    }
    
    func catchNotificationAckWaitTimedOut(notification:Notification) -> Void{
        self.view.makeToast("Alert Time Out.", duration: 1.0, position: .center)
        
        self.btnSendAlert.isEnabled = true
        self.navigationItem.leftBarButtonItem?.isEnabled = true
        alertSentStatus = false
        
        DispatchQueue.main.async{
            LoadingHandler.shared.stoploading()
        }
    }
    
    
    func catchNotificationGenerateSuccess(notification:Notification) -> Void{
        //self.btnSendAlert.isEnabled = true
        self.navigationItem.leftBarButtonItem?.isEnabled = true
        
    }
    
    func catchNotificationGenerateFail(notification:Notification) -> Void{
        self.btnSendAlert.isEnabled = true
        self.navigationItem.leftBarButtonItem?.isEnabled = true
        
         alertSentStatus = false
        
        DispatchQueue.main.async{
            LoadingHandler.shared.stoploading()
        }
        
    }
    
    
    
    
    
    //MARK: Private Methods
    func backButtonAction()
    {
        if alertSentStatus != false{
            JourneyManager.sharedInstance.setAlertModeState(state: AlertModeStatus.Off)
        }
        JourneyManager.sharedInstance.setAlertSendingType(type: AlertSendingType.Unknown)
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: IBAction Methods
    @IBAction func btnAccidentAlertPressed(_ sender: Any) {
        self.alertType = AlertConstant.ALERT_TYPE_ACCIDENT
        btnAccident.backgroundColor = color_highlited_alert
        btnSendAlert.backgroundColor = color_highlited_btn
        btnSendAlert.isUserInteractionEnabled = true
        btnPuncture.backgroundColor = color_Non_highlited_alert
        btnNoFuel.backgroundColor = color_Non_highlited_alert
        btnSevereCondition.backgroundColor = color_Non_highlited_alert
    }
    @IBAction func btnPunctureAlertPressed(_ sender: Any) {
        self.alertType = AlertConstant.ALERT_TYPE_PUNCTURE
        btnPuncture.backgroundColor = color_highlited_alert
        btnSendAlert.backgroundColor = color_highlited_btn
        btnSendAlert.isUserInteractionEnabled = true
        btnAccident.backgroundColor = color_Non_highlited_alert
        btnNoFuel.backgroundColor = color_Non_highlited_alert
        btnSevereCondition.backgroundColor = color_Non_highlited_alert
    }
    @IBAction func btnNoFuelAlertPressed(_ sender: Any) {
        self.alertType = AlertConstant.ALERT_TYPE_NO_FUEL
        btnNoFuel.backgroundColor = color_highlited_alert
        btnSendAlert.backgroundColor = color_highlited_btn
        btnSendAlert.isUserInteractionEnabled = true
        btnPuncture.backgroundColor = color_Non_highlited_alert
        btnAccident.backgroundColor = color_Non_highlited_alert
        btnSevereCondition.backgroundColor = color_Non_highlited_alert
    }
    @IBAction func btnSevereConditionAlertPressed(_ sender: Any) {
        self.alertType = AlertConstant.ALERT_TYPE_SEVERE_CONDITION
        btnSevereCondition.backgroundColor = color_highlited_alert
        btnSendAlert.backgroundColor = color_highlited_btn
        btnSendAlert.isUserInteractionEnabled = true
        btnPuncture.backgroundColor = color_Non_highlited_alert
        btnNoFuel.backgroundColor = color_Non_highlited_alert
        btnAccident.backgroundColor = color_Non_highlited_alert
    }
    
    @IBAction func btnSendAlertPressed(_ sender: Any) {
        if self.alertType.isBlank {
            PopUpUtility().showDefaultPopUpWith(message: "Please select alert type")
        }else{
            if NetworkCheck.isNetwork() {
                self.navigationItem.leftBarButtonItem?.isEnabled = false
                JourneyManager.sharedInstance.setAlertSendingType(type: AlertSendingType.Sent)
                
                if !self.isAutoGenerateAlert {
                    JourneyManager.sharedInstance.setAlertGenerator(generator: AlertGenerator.App)
                }
                
                btnSendAlert.isEnabled = false
                let alertContext:AlertContext = AlertContext()
                alertContext.changeStateToAlertGenerate(alertType: self.alertType)
            } else {
                self.view.makeToast(ToastMsgConstant.network)
            }
        }
    }
    
    
}
