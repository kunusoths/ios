//
//  AlertSentVC.swift
//  Maximus
//
//  Created by kpit on 07/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class AlertSentVC: UIViewController,AlertApiManagerDelegate  {
    
    var parentContainerVc:RideInProgressContainer?
    let alertManager:AlertApiManager = AlertApiManager()
    let sinkManager = SinkManager.sharedInstance
    var generatedAlertId:Int?
    //var timer:Timer?
    var alertType:String?
    
    var seconds = 30
    var timer = Timer()
    let formatter = NumberFormatter()
    
    
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnRetryAlert: UIButton!
    @IBOutlet weak var lblElapsedTime: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar?.isHidden = false
        self.navigationItem.title = "ALERTS"
        navigationItem.hidesBackButton = true
        alertManager.alertManagerDelegate = self
        
        formatter.maximumFractionDigits = 1
        formatter.roundingMode = .down
        formatter.numberStyle = .decimal
        formatter.alwaysShowsDecimalSeparator = true
        formatter.minimumFractionDigits = 1
        self.btnRetryAlert.isHidden = true
        lblStatus.text = "mins - Awaiting response..."
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sinkManager.addObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_TIMER_STARTED, selectorName: self.catchNotificationTimerStarted)
        sinkManager.addObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_RESPONSE_WAIT_TIMEDOUT, selectorName: self.catchNotificationResponseWaitTimeOut)
        sinkManager.addObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_CLOSE_ALERT_EVENT_FROM_DM, selectorName: self.catchNotificationCloseAlertEventFromDM)
        
        sinkManager.addObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_GENERATE_FAILURE, selectorName: self.catchNotificationAlertGenerateForRetryFail)
        
        btnRetryAlert.isEnabled = false
        
        self.runTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        sinkManager.removeObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_GENERATE_FAILURE)
        sinkManager.removeObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_TIMER_STARTED)
        sinkManager.removeObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_RESPONSE_WAIT_TIMEDOUT)
        sinkManager.removeObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_CLOSE_ALERT_EVENT_FROM_DM)
        
    }
    
    func runTimer() {
        seconds = 30
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(AlertSentVC.updateTimer)), userInfo: nil, repeats: true)
    }
    
    
    func updateTimer() {
        seconds -= 1
        if seconds >= 10{
        lblElapsedTime.text = "00.\(seconds)"
        
        }else{
        lblElapsedTime.text = "00.0\(seconds)"
        }
        
        if seconds == 0{
        timer.invalidate()
        
        }
    }
    
    func catchNotificationAlertGenerateForRetryFail(notification:Notification) -> Void{
        DispatchQueue.main.async{
            LoadingHandler.shared.stoploading()
        }
    }
    
    func catchNotificationCloseAlertEventFromDM(notification:Notification) -> Void{
        if let alertId = generatedAlertId {
            JourneyManager.sharedInstance.setAlertSendingType(type: AlertSendingType.Unknown)
            alertManager.closeGeneratedAlert(generatedAlertId: alertId)
        }
    }
    
    func catchNotificationResponseWaitTimeOut(notification:Notification) -> Void{
        self.btnRetryAlert.isHidden = false
        btnRetryAlert.isEnabled = true
        lblStatus.text = "mins - Try reaching out again?"
    }
    
    func catchNotificationTimerStarted(notification:Notification) -> Void{
        DispatchQueue.main.async{
            LoadingHandler.shared.stoploading()
        }
        self.btnRetryAlert.isHidden = true
        btnRetryAlert.isEnabled = false
        lblStatus.text = "mins - Awaiting response..."
        self.runTimer()
    }
    
    
    @IBAction func btnYesIAmFinePressed(_ sender: Any) {
        if let alertId = generatedAlertId {
            JourneyManager.sharedInstance.setAlertSendingType(type: AlertSendingType.Unknown)
            alertManager.closeGeneratedAlert(generatedAlertId: alertId)
        }
    }
    
    @IBAction func btnRetryClicked(_ sender: UIButton) {
        
        if let alertType = self.alertType {
            LoadingHandler.shared.startloading(interactionEnable: true, view: self.view)
            
            JourneyManager.sharedInstance.setAlertSendingType(type: AlertSendingType.ReSent)
            JourneyManager.sharedInstance.setAlertGenerator(generator: AlertGenerator.App)

            let alertContext:AlertContext = AlertContext()
            alertContext.changeStateToAlertGenerate(alertType: alertType)
        }
        
              
    }
    
    //MARK:- AlertAPIManager Delegates
    func closeAlertSuccess(alertObj: [String : Any]) {
        self.dissmissCurrentView()
    }
    
    func dissmissCurrentView(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let navigationController = UINavigationController(rootViewController: RideInProgressContainer())
        let mainViewController = navigationController
        let leftViewController = SideMenuVC()
        let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
        slideMenuController.changeLeftViewWidth(self.view.size.width-60)
        appDelegate.window?.rootViewController = slideMenuController
        
    }
    
    func onFailure(Error: [String : Any]) {
        let errModel:ErrorModel = Error["data"] as! ErrorModel
        if let errorMsg = errModel.description {
            if errorMsg == "Alert is already closed" {
                self.dissmissCurrentView()
            }
        }
        PopUpUtility().showDefaultPopUpWith(message: (errModel.description)!)
    }
}
