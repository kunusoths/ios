//
//  AboutUsVc.swift
//  Maximus
//
//  Created by kpit on 23/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class AboutUsVc: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.navigationBar.isTranslucent = false
        self.view.backgroundColor = AppTheme.THEME_COLOR
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Ham Menu"), style: .plain, target: self, action: #selector(menuTapped))
        self.navigationItem.title = "ABOUT"
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        
        self.automaticallyAdjustsScrollViewInsets = false
        
    }
    
    //MARK:Private Methods
    func menuTapped() {
        self.slideMenuController()?.openLeft()
    }
}
