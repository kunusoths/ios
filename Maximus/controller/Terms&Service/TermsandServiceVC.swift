//
//  Terms&ServiceVC.swift
//  Maximus
//
//  Created by kpit on09/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class TermsandServiceVC: UIViewController {

    @IBOutlet weak var txtView: UITextView!
    var isPrivacyViewLoaded = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navBar?.isHidden = false
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:UIImage(named: "Back Arrow White"), style: .plain, target: self, action: #selector(backTapped))
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.automaticallyAdjustsScrollViewInsets = false

        //loadContent()

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.txtView.setContentOffset(CGPoint.zero, animated: false)
    }
    
    func loadContent() {
        if isPrivacyViewLoaded {
            // load privacy content
        } else {
           // load terms content
        }
        
    }
    func backTapped() {
        self.popVC()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onAgreeClic(_ sender: Any) {
        print("accept tapped")
        self.popVC()
        dismiss(animated: true, completion: nil)
    }

}
