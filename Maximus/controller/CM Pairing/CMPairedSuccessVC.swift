//
//  CMPairedSuccessVC.swift
//  Maximus
//
//  Created by kpit on 23/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class CMPairedSuccessVC: UIViewController {
    
    @IBOutlet weak var lblSerialNumber: UILabel!
    @IBOutlet weak var imgVwPrevArrow: UIImageView!
    @IBOutlet weak var imgVwNextArrow: UIImageView!
    
    @IBOutlet weak var lblVerifyNo: UILabel!
    let successMsg = "Congratulations! Display "
    var serialNumber = ""
    let cmMessage = "\nis ready to assist you for a great ride"
    var bikerDetails:BikerDetails?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector:
            #selector(self.upgradeDM), name: NSNotification.Name(rawValue: NotificationIdentifier.upgardeDmIdentifier), object: nil)
        
        self.navigationItem.title = "DEVICE SETUP"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.bikerDetails = BikerDetails.getBikerDetails(context: context)
        
        let attributedString = NSMutableAttributedString(string:successMsg)
        //TODO: uncomment and resolve error
        let font:FontName = .Roboto
        let type:FontType = .Bold
        let size = 15
        let attrs = [NSFontAttributeName : UIFont.Font(font, type: type, size: CGFloat(size)), NSForegroundColorAttributeName: AppTheme.THEME_ORANGE] as [String : Any]
        let boldString = NSMutableAttributedString(string:serialNumber, attributes:attrs)
        attributedString.append(boldString)
        attributedString.append(NSMutableAttributedString(string:cmMessage))
        lblSerialNumber.attributedText = attributedString
        
    //    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationIdentifier.upgardeDmIdentifier), object: nil)

        let image = #imageLiteral(resourceName: "Next Arrow").withRenderingMode(.alwaysTemplate)
        imgVwNextArrow.image = image
        imgVwNextArrow.tintColor = UIColor.white
        
        let image1 = #imageLiteral(resourceName: "Prev Arrow").withRenderingMode(.alwaysTemplate)
        imgVwPrevArrow.image = image1
        imgVwPrevArrow.tintColor = UIColor.white

        if self.bikerDetails?.is_otp_verified ?? false {
            self.lblVerifyNo.text = "YOU ARE ALL SET TO HIT THE ROADS"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func btnVerifyNumberAction(_ sender: UIButton) {
       // let isOtpVerified = MyDefaults.getBoolData(key: DefaultKeys.KEY_OTPVERIFIED)//-----Get OTP Verification Status
        if self.bikerDetails?.is_otp_verified ?? false {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            if (appDelegate.window?.rootViewController is HomeContainerVC)
            {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationIdentifier.upgardeDmIdentifier), object: nil)
                self.dismissVC(completion: nil)
            }
            else if   MyDefaults.getData(key: DefaultKeys.KEY_FROMWHERE) as! String == fromViewcontroller.SettingsVC.rawValue {
                let navigationController = UINavigationController(rootViewController: SupportContainerViewController())
                let mainViewController = navigationController
                let leftViewController = SideMenuVC()
                let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
                slideMenuController.changeLeftViewWidth(self.view.size.width-60)
                appDelegate.window?.rootViewController = slideMenuController
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationIdentifier.upgardeDmIdentifier), object: nil)
                
            } else {
                let navigationController = UINavigationController(rootViewController: HomeContainerVC())
                let mainViewController = navigationController
                let leftViewController = SideMenuVC()
                let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
                slideMenuController.changeLeftViewWidth(self.view.size.width-60)
                appDelegate.window?.rootViewController = slideMenuController
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationIdentifier.upgardeDmIdentifier), object: nil)
            }
            
        }else {
            MyDefaults.setData(value: fromViewcontroller.CMPairedSuccessVC.rawValue, key: DefaultKeys.KEY_FROMWHERE)
            self.pushVC(PhoneVerificationVC())
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationIdentifier.upgardeDmIdentifier), object: nil)
        }
    }
    
    @IBAction func btnScanAgainAction(_ sender: UIButton) {
        //self.popVC()
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is DeviceSetUpVC {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
}
