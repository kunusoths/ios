//
//  DMDisplayVC.swift
//  Maximus
//
//  Created by Admin on 02/06/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import ObjectMapper

class DMDisplayVC: UIViewController,UICollectionViewDelegateFlowLayout{//,DeviceLocationDelegate {
    
    @IBOutlet weak var lblExitNumber: UILabel!
    //MARK:- Properties
    @IBOutlet weak var lblNextTurnName: UILabel!
    @IBOutlet weak var lblCurrentTurnName: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var pageController: UIPageControl!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var imgViewNextState: UIImageView!
    @IBOutlet weak var imgViewcurrentState: UIImageView!
    @IBOutlet weak var mapView: GMSMapView!
    
    let cellIdentifier = "DMDisplayCell"
    //let deviceLocation = DeviceLocationServiece.sharedInstance()
    let sinkManager = SinkManager.sharedInstance
    let navLocator = NavigationLocator.sharedInstance()
    var marker = GMSMarker()
    var flowlayout = UICollectionViewFlowLayout()
    var tbtDirectionRoute:String?
    var isCamera:Bool = false
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "BACK TO APPLICATION"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Arrow White"), style: .plain, target: self, action: #selector(backButtonAction))
        
        //Collection view cell registration
        let nib = UINib (nibName:cellIdentifier, bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: cellIdentifier)
        
        flowlayout.scrollDirection = UICollectionViewScrollDirection.horizontal
        flowlayout.minimumLineSpacing = 0
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isPagingEnabled = true
        collectionView.setCollectionViewLayout(flowlayout, animated: false)
        
        pageController.numberOfPages = 3
        pageController.currentPage = 0
        pageController.hidesForSinglePage = true
        
        //Temporary Route Draw for Test
        self.drawRouteOnMAP(directionResponse: tbtDirectionRoute!)
        /* Image */
        //        self.imgViewcurrentState.alpha = 1;
        //        UIView.animate(withDuration: 0.8,
        //                       delay:0.0,
        //                       options:[.autoreverse, .repeat],
        //                       animations: {
        //                        self.imgViewcurrentState.alpha = 0;
        //        }, completion: nil)
        //
        
        // deviceLocation.configureDeviceLocation()
        // deviceLocation.deviceLocationDelegate = self
        //set to get the location which is provided for Navigation
        navLocator.navTestDelegate = self
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Register journey notifications
        
        self.changeRegistrationStateForDMDisplayVC(register: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //Deregister journey notifications
        self.changeRegistrationStateForDMDisplayVC(register: false)
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func changeRegistrationStateForDMDisplayVC(register:Bool){
        
        if(register){
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_REMAINING_DISTANCE, selectorName:catchNotificationRemainingDistance)
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_CURRENT_TURN, selectorName: catchNotifcationCurrentTurn)
           
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_NEXT_TURN, selectorName: catchNotificationNextTurn)
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_RIDE_ROUTE_FINISHED, selectorName: catchNotificationRideRouteFinished)
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_DEVIATION_DETECTED, selectorName: catchNotificationDeviationDetected)
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_LEG_PROGRESS_COMPLETED, selectorName: catchNotificationLegFinished)
            sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_CURRENT_MANUEVER_INFO, selectorName: catchNotificationTurnExitNumber)
            
        }else{
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_REMAINING_DISTANCE)
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_CURRENT_TURN)
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_NEXT_TURN)
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_RIDE_ROUTE_FINISHED)
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_DEVIATION_DETECTED)
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_LEG_PROGRESS_COMPLETED)
            sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_CURRENT_MANUEVER_INFO)
        }
    }
    
    
    //MARK:- Private Methods
    func backButtonAction(){
        self.popVC()
        //self.dismissVC(completion: nil)
    }
    
    //    func locationUpdateCall(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
    //
    //        let userLocation = locations[0] as CLLocation
    //        //print("user latitude = \(userLocation.coordinate.latitude)")
    //        //print("user longitude = \(userLocation.coordinate.longitude)")
    //        // self.mapView.clear()
    //        let originPos = CLLocationCoordinate2DMake(Double(userLocation.coordinate.latitude),Double(userLocation.coordinate.longitude))
    //        marker.map = nil
    //        marker = GMSMarker(position: originPos)
    //        marker.title = "My Location"
    //        marker.map = self.mapView
    //        let resizedimage = UIImage.scaleTo(image: UIImage(named: "Map Pointer")!, w: 40.0, h: 44.0)
    //        marker.icon = resizedimage
    //
    //        if !isCamera {
    //            let camera = GMSCameraPosition.camera(withLatitude: Double(userLocation.coordinate.latitude), longitude: Double(userLocation.coordinate.longitude), zoom: 17.0)
    //            self.mapView?.animate(to: camera)
    //           // isCamera = true// for Disable Camera Trace current location
    //            isCamera = false// for Enable Camera Trace current location
    //
    //        }
    //    }
    
    
    //MARK:- RideManager Delegates
    func drawRouteOnMAP(directionResponse:String){
        if let obj = Mapper<RouteDetails>().map(JSONString: directionResponse) {
            
            if (obj.routeList?.count)! > 0 {
                self.loadPolyline(routeObj: (obj.routeList?[0])!)
            }
        }
    }
    ///*Random Color Generator*/
    func getRandomColor() -> UIColor{
        let randomRed:CGFloat = CGFloat(drand48())
        let randomGreen:CGFloat = CGFloat(drand48())
        let randomBlue:CGFloat = CGFloat(drand48())
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
    }
    
    func setCameraForLineWithPath(points:String)
    {
        let path: GMSPath = GMSPath(fromEncodedPath: points)!
        
        var bounds = GMSCoordinateBounds()
        for index in 0...path.count() {
            bounds = bounds.includingCoordinate(path.coordinate(at: index))
        }
        self.mapView.animate(with: GMSCameraUpdate.fit(bounds))
        
    }
    
    func loadPolyline(routeObj:RouteList){
        
        let color:UIColor = getRandomColor()
        var totalPoints:[String] = []
        let legs:[Legs] = routeObj.legs!
        
        let points = routeObj.totalPolylinePoints?.points
        
        
        if legs.count > 0{
            
            self.setCameraForLineWithPath(points: points!)
            
            for leg in legs{
                let steps:[Steps] = leg.steps!
                if steps.count > 0{
                    for step in steps{
                        let polylinePoint:StepPolyline = step.polylinePoints!
                        if let point = polylinePoint.points{
                            print(point)
                            
                            let path: GMSPath = GMSPath(fromEncodedPath: point)!
                            let routePolyline = GMSPolyline(path: path)
                            routePolyline.spans = [GMSStyleSpan(color: color)]
                            routePolyline.strokeWidth = 3.0
                            routePolyline.map = self.mapView
                            routePolyline.geodesic = true
                            totalPoints.append(point)
                        }
                    }
                }
            }
        }
    }
    
    //MARK:- Scroll View Methods
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let pageNumber = Int(targetContentOffset.pointee.x / view.frame.width)
        pageController.currentPage = pageNumber
    }
    
    //MARK:- Journey Notifications
    
    func catchNotificationRideRouteFinished(notification:Notification) -> Void{
        
        DispatchQueue.main.async{
            //self.view.makeToast("📢📢 Destination Reached 🏁🏁", duration: 3.0,position: .bottom)
            self.imgViewcurrentState.image  = nil
            self.imgViewNextState.image     = nil
            self.lblNextTurnName.text = ""
            self.lblCurrentTurnName.text = ""
            self.lblDistance.text = "0 m"
        }
    }
    
    func catchNotificationLegFinished(notification:Notification) -> Void{
        //        DispatchQueue.main.async{
        //        self.view.makeToast("📢📢 You have arrived 🏁🏁", duration: 3.0,position: .bottom)
        //        }
    }
    
    func catchNotificationTurnExitNumber(notification:Notification) -> Void{
        
        guard let exitNumber:Int16 = notification.userInfo!["Data"] as! Int16? else { return }
        print("@StateHandler: Exit Number\(exitNumber)")
        
        DispatchQueue.main.async{
            if exitNumber == 0 {
                self.lblExitNumber.text = ""
                self.lblExitNumber.isHidden = true
            }else{
                self.lblExitNumber.text = "Take the exit \(exitNumber) at the roundabout"
                self.lblExitNumber.isHidden = false
            }
        }
    }
    
    func catchNotificationRemainingDistance(notification:Notification) -> Void {
        
        guard let distance:Int64 = notification.userInfo!["Data"] as! Int64? else { return }
        print("Distance In Meters: \(distance)")
        DispatchQueue.main.async{
            if distance < 1000 && distance >= 0{
                self.lblDistance.text = String(distance) + " m"
                
            }else{
                let dist = Double(distance)/1000.0
                self.lblDistance.text = String(format:"%.2f km",dist)
                print("Distance In km: \(String(format:"%.2f km",dist))")
            }
        }
    }
    
    func catchNotifcationCurrentTurn(notification:Notification) -> Void{
        
        guard let currentTurn:String = notification.userInfo!["Data"] as! String? else { return }
        DispatchQueue.main.async{
            self.lblCurrentTurnName.text = currentTurn
            self.imgViewcurrentState.image  = UIImage(named: currentTurn)
        }
    }
    
    func catchNotificationNextTurn(notification:Notification) -> Void{
        
        guard let nextTurn:String = notification.userInfo!["Data"] as! String? else { return }
        DispatchQueue.main.async{
            self.lblNextTurnName.text = nextTurn
            self.imgViewNextState.image = UIImage(named: nextTurn)
        }
    }
    
    func catchNotificationDeviationDetected(notification:Notification) -> Void{
        print("DEviation Detected")
    }
    
}

//MARK:- CollectionView Delegate
extension DMDisplayVC : UICollectionViewDelegate , UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        return pageController.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! DMDisplayCell
        
        return cell
    }
}

extension DMDisplayVC : NavigationLocatorDelegate {
    
    func updateCurrentLocation(location:CLLocation){
        let userLocation = location
        //print("user latitude = \(userLocation.coordinate.latitude)")
        //print("user longitude = \(userLocation.coordinate.longitude)")
        // self.mapView.clear()
        let originPos = CLLocationCoordinate2DMake(Double(userLocation.coordinate.latitude),Double(userLocation.coordinate.longitude))
        self.marker.map = nil
        self.marker = GMSMarker(position: originPos)
        self.marker.title = "My Location"
        self.marker.map = self.mapView
        let resizedimage = UIImage.scaleTo(image: UIImage(named: "Map Pointer")!, w: 40.0, h: 44.0)
        self.marker.icon = resizedimage
        
        if !self.isCamera {
            let camera = GMSCameraPosition.camera(withLatitude: Double(userLocation.coordinate.latitude), longitude: Double(userLocation.coordinate.longitude), zoom: 17.0)
            self.mapView?.animate(to: camera)
            self.isCamera = true// for Disable Camera Trace current location
            //isCamera = false// for Enable Camera Trace current location
        }
        
    }
    
    func reRoutedRoute(routeResponse: String) {
        print("DM Reroute Called:\(routeResponse)")
        //let user = Mapper<RouteDetails>().map(JSONString: routeResponse)
        self.drawRouteOnMAP(directionResponse: routeResponse)
        self.view.makeToast("📢📢Re-Routed⚠️⚠️⚠️", duration: 3.0,position: .bottom)
    }
    
}
