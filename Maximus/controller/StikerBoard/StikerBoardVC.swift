//
//  StikerBoardVC.swift
//  Maximus
//
//  Created by Admin on 13/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import ObjectMapper

fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)

class StikerBoardVC: UIViewController {
    let cellIdentifier = "StikerBoardCell"
    var arrSticker:[Stickerboard]?
    let lodingIndicator = LoadingOverlay()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.backgroundColor = AppTheme.THEME_LIGHT_GRAY
        self.collectionView.backgroundColor = AppTheme.THEME_LIGHT_GRAY
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Ham Menu"), style: .plain, target: self, action: #selector(menuTapped))
        self.navigationItem.title = "MY ACHIEVEMENTS"
        self.navigationItem.leftBarButtonItem?.tintColor = .white
            self.navigationController!.navigationBar.isTranslucent = false
        let nib = UINib (nibName:cellIdentifier, bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: cellIdentifier)
        self.collectionView.backgroundView = nil
        
        let bikerID = MyDefaults.getInt (key: DefaultKeys.KEY_BIKERID)//-Get biker ID
        let rideStatsObj = RideStatsViewModel()
        rideStatsObj.rideStatsDelegate = self
        rideStatsObj.getStickerBoard(bikerID:bikerID, request: "GetStickerBoard")
    }
    
    //MARK:Private Methods
    func menuTapped() {
        self.slideMenuController()?.openLeft()
    }
    
    func settableBackgroundView() {
        
        let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: self.collectionView.bounds.size.width, height: self.collectionView.bounds.size.height))
        noDataLabel.text          = "No data available"
        noDataLabel.textColor     = UIColor.white
        noDataLabel.textAlignment = .center
        self.collectionView.backgroundView  = noDataLabel
    }
    
}
//MARK:ColectionView delegate methods
extension StikerBoardVC : UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = arrSticker?.count{
            return count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! StikerBoardCell
        let objStiker:Stickerboard = self.arrSticker![indexPath.row]
        cell.imgStickerIcon.image = nil
        cell.imgStickerIcon.tag = indexPath.row
        let imgHighResolutionObj:Images = objStiker.images![0]
        cell.imgStickerIcon.loadImageUsingCacheWithUrlString(urlString: imgHighResolutionObj.url!,indexPath: indexPath.row)
        
        return cell
    }
}
//MARK:ColectionView Flow Layout delegate methods
extension StikerBoardVC : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //2
        let itemsPerRow: CGFloat = 3
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        print("padding:\(paddingSpace)\n AvailabeWidth:\(availableWidth)\nFinalWidth:\(widthPerItem)")
        return CGSize(width: widthPerItem, height: widthPerItem)
        
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    // 4
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}

extension StikerBoardVC : RideStatsDelegate {
    
    func rideStatisticSuccess(data: Any) {
        if let dataDict = data as? [String: Any] {
            self.view.hideToastActivity()
            if let obj:StickerBoardResponse = dataDict["data"] as? StickerBoardResponse {
                self.sortAndSetSticerData(obj: obj)
            }
            if self.arrSticker?.count == 0 {
                self.settableBackgroundView()
            }
            DispatchQueue.main.async{
                self.collectionView.reloadData()
            }
        }
    }
    func sortAndSetSticerData(obj:StickerBoardResponse){
        var arrStick = obj.stickerboard! as [Stickerboard]?
        arrStick?.sort {
            return $0.count! > $1.count!
        }
        self.arrSticker = arrStick
    }
    
    func failure(data:[String:Any]){
        print(data)
        self.view.hideToastActivity()
        if let errModel:ErrorModel = data["data"] as? ErrorModel {
            print(errModel.message ?? " ")
            self.view.makeToast((errModel.description)!)
        } else {
            if let msg = data["description"] as? String {
              self.view.makeToast(msg)
            }
        }
        self.settableBackgroundView()
    }
}
