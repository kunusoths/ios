//
//  SideMenuVC.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 27/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class SideMenuVC: UIViewController {
   
    @IBOutlet weak var hamburgerTableView: UITableView!
    @IBOutlet weak var btnTermsAndConditions: UIButton!
    @IBOutlet weak var appVersionlbl: UILabel!
    
    let cellMenuReuseIdentifier = TableViewCells.hamburgerMenuCell
    let cellMenuHeaderViewReuseIdentifier = TableViewCells.hamburgerHeaderMenuCell
    
    var hamburgerSelectedIndex: IndexPath?
    var hamburgerHeaderView:MenuHeaderView?
    
    //Creating Arrays for intializing Table View Cells
    var hamburgerMenuArray:[HamburgerMenus] = [.RIDE_ON , .CURRENT_RIDE, .RIDE_LOG, .ACHIEVEMENTS, .MY_BIKE, .MY_SUPPORT]
    var hamburgerTouchPoint:CGPoint?
    var hamburgerMenuArrayImageName = ["Plan your ride","Current ride","Ridelog","ACHIEVEMENTS","My Bike","support"]
    var hamburgerMenuArrayActiveImageName = ["plan a ride - active","Current ride- active","ride log - active","ACHIEVEMENTS-active","my bike-active","support - active"]
    var hamburgerPreviousSelectedIndexPath:IndexPath = IndexPath(row: 0, section: 0)
    let sideContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    var rideDetails:Rides?
    
    // MARK: View Lifecycle methods
    override func viewWillAppear(_ animated: Bool) {
//        self.rideDetails = nil
        let bikerDetails = BikerDetails.getBikerDetails(context: sideContext)
        
       if MyDefaults.getBoolData(key: DefaultKeys.KEY_ISLOGIN) {
        hamburgerHeaderView?.imgVwUser.loadImageUsingCacheWithUrlString(urlString: (MyDefaults.getData(key: DefaultKeys.KEY_PROFILE_URL) as! String) ,  indexPath: 0)
            hamburgerHeaderView?.lblUserName.text = (bikerDetails?.first_name ?? "") + " " + (bikerDetails?.last_name ?? "")
        }
        
        self.hamburgerTableView.reloadData()

    }

    override func viewDidLayoutSubviews() {
        let topBarOffset = self.topLayoutGuide.length
        self.hamburgerTableView.top = self.view.top - topBarOffset
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //registering table view cell to table view
        let tableNib = UINib(nibName: cellMenuReuseIdentifier, bundle: nil)
        self.hamburgerTableView.register(tableNib , forCellReuseIdentifier: cellMenuReuseIdentifier)
        
        //registering table Header View to table view
        let headerNib = UINib.init(nibName: cellMenuHeaderViewReuseIdentifier, bundle: nil)
        self.hamburgerTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: cellMenuHeaderViewReuseIdentifier)
        self.hamburgerHeaderView = hamburgerTableView.dequeueReusableHeaderFooterView(withIdentifier: cellMenuHeaderViewReuseIdentifier) as! MenuHeaderView
        
        hamburgerSelectedIndex = IndexPath(row: 0, section: 0)
        
        let isVirtualCm = MyDefaults.getBoolData(key: DefaultKeys.IS_CM_VIRTUAL)
        // in case of VirtualCM removing MY RIDE Cell
        if isVirtualCm {
            hamburgerMenuArray = [.RIDE_ON , .CURRENT_RIDE, .RIDE_LOG, .ACHIEVEMENTS, .MY_SUPPORT]
            hamburgerMenuArrayImageName = ["Plan your ride","Current ride","Ridelog","ACHIEVEMENTS","support"]
            hamburgerMenuArrayActiveImageName = ["plan a ride - active","Current ride- active","ride log - active","ACHIEVEMENTS-active","support - active"]
        }
        
       self.displayAppVersion()
        self.hamburgerTableView.tableFooterView = UIView()
        self.btnTermsAndConditions.titleLabel?.font = UIFont(name: "Roboto-Bold", size: 15.0)
        self.appVersionlbl.font =  UIFont(name: "Roboto-Bold", size: 15.0)
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressTableCell(_:)))
        longPressGesture.minimumPressDuration = 0.1
        self.hamburgerTableView.addGestureRecognizer(longPressGesture)
        hamburgerTableView.delegate = self
        hamburgerTableView.dataSource  = self
     
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.se
     
    }
    
    
    //MARK:Private Methods
    //for deselecting previous selected index while selecting other index
    public func selectMenuAtSection(index: NSInteger, andDeslectSectionIndex: NSInteger){
        
        let indexPathDeselect:IndexPath = IndexPath(row: andDeslectSectionIndex, section: 0)
        self.hamburgerTableView.deselectRow(at: indexPathDeselect , animated: true)
        self.hamburgerTableView.delegate?.tableView!( self.hamburgerTableView, didDeselectRowAt: indexPathDeselect)
        
        let indexPath:IndexPath = IndexPath(row: index, section: 0)
        self.hamburgerTableView.selectRow(at: indexPath, animated: true, scrollPosition: UITableViewScrollPosition.none)
        self.hamburgerTableView.delegate?.tableView!( self.hamburgerTableView, didSelectRowAt: indexPath)
    }
    
    //for showing previous selected index while coming from another view
    public func showSelectedSideMenuIndex(index: NSInteger){
        
        let indexPath:IndexPath = IndexPath(row: index, section: 0)
        self.hamburgerTableView.selectRow(at: indexPath, animated: true, scrollPosition: UITableViewScrollPosition.none)
        hamburgerSelectedIndex = IndexPath(row: index, section: 0)
        
        let cell = hamburgerTableView.cellForRow(at: indexPath) as! MenuCell
       
        cell.lblMenuTitle.textColor = .white
        cell.backgroundColor = AppTheme.THEME_ORANGE
        cell.lblMenuTitle.font = UIFont(name: "Roboto-Medium", size: 16.0)
    }
    
   //long press table view cell
    func longPressTableCell(_ longPressGestureRecognizer: UILongPressGestureRecognizer) {
        if longPressGestureRecognizer.state == UIGestureRecognizerState.ended {
            let touchPoint = longPressGestureRecognizer.location(in: hamburgerTableView)
            if let indexPath = hamburgerTableView.indexPathForRow(at: touchPoint) {
                if let cell = hamburgerTableView.cellForRow(at: self.hamburgerPreviousSelectedIndexPath) as? MenuCell {
                    cell.lblMenuTitle.textColor = .black
                    cell.contentView.backgroundColor = .white
                    cell.imgVwMenu.image = UIImage(named:hamburgerMenuArrayImageName[self.hamburgerPreviousSelectedIndexPath.row])
                }
                if touchPoint == self.hamburgerTouchPoint {
                    hamburgerTableView.delegate?.tableView!(hamburgerTableView, didSelectRowAt: indexPath)
                }
             }
        } else if longPressGestureRecognizer.state == UIGestureRecognizerState.began {
            let touchPoint = longPressGestureRecognizer.location(in: hamburgerTableView)
            if let indexPath = hamburgerTableView.indexPathForRow(at: touchPoint) {
                self.hamburgerTouchPoint = touchPoint
                if let cell = hamburgerTableView.cellForRow(at: indexPath) as? MenuCell {
                    self.hamburgerPreviousSelectedIndexPath = indexPath
                    cell.lblMenuTitle.textColor = .white
                    cell.contentView.backgroundColor = AppTheme.THEME_ORANGE
                    cell.imgVwMenu.image = UIImage(named:hamburgerMenuArrayActiveImageName[indexPath.row])
                }
            }
        }
    }
    
    //To go to ProfileVC
    func showProfile(_ sender: UIView) {
        if let cell = hamburgerTableView.cellForRow(at: hamburgerSelectedIndex!) as? MenuCell {
            self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: ProfileVC()), close: true)
        }
    }
    
    //for setting app version
    func displayAppVersion() {
        let nsObject: AnyObject? = Bundle.main.object(forInfoDictionaryKey: StringConstant.appVersion) as AnyObject
        //Then just cast the object as a String, but be careful, you may want to double check for nil
        if let version = nsObject as? String {
            appVersionlbl.text = version
        }
    }
    
      //MARK: Button Actions
    @IBAction func termsAndConditionsButtonTapped(_ sender: Any) {
          //About T_CContainerVCViewController
        self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: TCContainerViewController()), close: true)
    }
}

 //MARK:- Tableview Delegate & DataSource Methods
extension SideMenuVC:UITableViewDelegate,UITableViewDataSource{
    
    // number of sections in table view
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.hamburgerMenuArray.count
    }
    
    // Header View for Section
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        hamburgerHeaderView?.lblUserName.textColor = .white
        hamburgerHeaderView?.lblUserName.font = UIFont(name: "Roboto-Medium", size: 18.0)
        hamburgerHeaderView?.imgVwUser.setCornerRadius(radius: (hamburgerHeaderView?.imgVwUser.frame.size.width)!/2)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showProfile(_:)))
        hamburgerHeaderView?.imgVwUser.addGestureRecognizer(tapGesture)
        
        return hamburgerHeaderView
    }
    
    //Header View height for Section
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 216
    }

    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellMenuReuseIdentifier, for: indexPath) as! MenuCell
        cell.lblMenuTitle.text = self.hamburgerMenuArray[indexPath.row].localizedString()
        self.getFirmwareVersionFromManifest()
        if indexPath.row == self.hamburgerMenuArray.count - 1 {
            if MyDefaults.getInt(key: DefaultKeys.KEY_JSON_VERSION) == MyDefaults.getInt(key: DefaultKeys.KEY_DM_VERSION)  || MyDefaults.getBoolData(key: DefaultKeys.KEY_UPDATE_SUCCESSFUL) || BluetoothManager.sharedInstance().getBLEState() == .DISCONNECT_NP ||  BluetoothManager.sharedInstance().getBLEState() == .DISCONNECT_E || BluetoothManager.sharedInstance().getBLEState() == .DISCONNECT_N || BluetoothManager.sharedInstance().getBLEState() == .DISCONNECT_BONP || BluetoothManager.sharedInstance().getBLEState() == .NONE  {
                cell.badgeCount.isHidden = true
            } else {
                cell.badgeCount.isHidden = false
                cell.badgeCount.text = "1"
            }
        }
        cell.lblMenuTitle.textColor = UIColor.black
        cell.lblMenuTitle.font = UIFont(name: "Roboto-Medium", size: 16.0)
        cell.selectionStyle = .none
        cell.backgroundColor = .white
        cell.contentView.backgroundColor = .white
        if indexPath.row == hamburgerSelectedIndex?.row {
            cell.backgroundColor = UIColor(red: 234/255.0, green: 234/255.0, blue: 234/255.0, alpha: 0.26)
        }
        cell.imgVwMenu.image = UIImage(named:hamburgerMenuArrayImageName[indexPath.row])
        cell.accessibilityIdentifier = cellMenuReuseIdentifier
        return cell
        
    }
    

    //method to draw a cell for a particular row
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cellToDisplay = cell as? MenuCell {
            if !cellToDisplay.badgeCount.isHidden {
                cellToDisplay.badgeCount.layer.cornerRadius = 10.0
                cellToDisplay.clipsToBounds = true
                cellToDisplay.layer.masksToBounds = true
            }
        }
    }
    
    // did deselect row method
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? MenuCell {
            
            cell.lblMenuTitle.textColor = UIColor.black
            cell.backgroundColor = UIColor.white
            cell.lblMenuTitle.font = UIFont(name: "Roboto-Medium", size: 16.0)
        }
    }
    
    
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        hamburgerSelectedIndex = indexPath
        let val:HamburgerMenus  = self.hamburgerMenuArray[indexPath.row]
      
        if let cell = tableView.cellForRow(at: indexPath) as? MenuCell {
            
            cell.lblMenuTitle.textColor = .white
            cell.imgVwMenu.image = UIImage(named:hamburgerMenuArrayActiveImageName[indexPath.row])
            cell.contentView.backgroundColor = AppTheme.THEME_ORANGE
            cell.lblMenuTitle.font = UIFont(name: "Roboto-Medium", size: 16.0)
        }
        switch  val {
        case .RIDE_ON:
            //Home
            self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: HomeContainerVC()), close: true)
        case .CURRENT_RIDE:
            //My Ride
            let vc = RideInProgressContainer()
            if let rideInfo = rideDetails{
                vc.rideDetails = rideInfo
                self.rideDetails = nil
            }
            self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: vc), close: true)
            
        case .RIDE_LOG:
            //Ride Log
            self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: RideLogVC()), close: true)
            
        case .ACHIEVEMENTS:
            //My Achievements
            self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: StikerBoardVC()), close: true)
            
        case .MY_BIKE:
            //My Bike
            self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: BatteryIgnitionOnVC()), close: true)
        case .MY_SUPPORT:
            //Support
            self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: SupportContainerViewController()), close: true)
        
            
        }
    }
}
