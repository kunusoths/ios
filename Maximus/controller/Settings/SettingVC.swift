//
//  SettingVC.swift
//  Maximus
//
//  Created by Admin on 03/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import CoreBluetooth
import ObjectMapper
import MessageUI
import Messages

//enumeration for tracking indexes in table view
enum SettingsTableViewIndexData: Int {
    case pairDisplayModuleIndex = 0, connectivityModuleIndex, upgradeDisplayModuleIndex, appVersionIndex, theftAlertIndex
}

//structure for storing data to intialize table view cell
struct SettingsTableViewCellData {
    
    var name : String
    var description : String
    var unPairButton = false
    var connectionStatusButton = false
    static var dMStatusImageName = NSLocalizedString("keyEmptyString", comment: "")
    static var cMStatusImageName = NSLocalizedString("keyEmptyString", comment: "")
    static var btnAddDMIsUserInteraction = false
    static var isCellTapInteraction = false
}

class SettingVC: UIViewController{
    
    
    @IBOutlet weak var settingsTableView: UITableView!
    @IBOutlet weak var contactUsCollectionView: UICollectionView!

    
    var settingsTableViewCellData = [
        SettingsTableViewCellData(name:NSLocalizedString("keyPairDisplayModule", comment: "") , description:NSLocalizedString("keyEmptyString", comment: ""),unPairButton:false,connectionStatusButton:false),
        SettingsTableViewCellData(name: NSLocalizedString("keyConnectivityModule", comment: ""), description:NSLocalizedString("keyEmptyString", comment: ""),unPairButton:true,connectionStatusButton:false),
        SettingsTableViewCellData(name:NSLocalizedString("keyUpgradeDisplayModule", comment: ""), description:NSLocalizedString("keyUpgradeDisplayModuleDescription", comment: ""),unPairButton:true,connectionStatusButton:true),
        SettingsTableViewCellData(name:NSLocalizedString("keyAppVersion", comment: "") , description:NSLocalizedString("keyEmptyString", comment: ""),unPairButton:true,connectionStatusButton:true),
        SettingsTableViewCellData(name:NSLocalizedString("keyTheftAlert", comment: "") , description:NSLocalizedString("keyEmptyString", comment: ""),unPairButton:true,connectionStatusButton:true),
        ]
    
    
    
    
    let contactUsCellSize = Constants.DeviceType.IS_IPHONE_5 ? 40 : 50
    
    let tableCellIdentifier = TableViewCells.settingsTableViewCell
    let collectionCellIdentifier = TableViewCells.settingsContactUsCollectionViewCell
    var imageArray: [UIImage] = []
    let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.newBackgroundContext()
    var isDMUpgradeMode = false
    var isDMUpgradeAvailable = false
    var serialNo = NSLocalizedString("keyEmptyString", comment: "")
    
     // MARK: View Lifecycle methods
    override func viewWillAppear(_ animated: Bool) {
        refreshUI()
        self.changeRegistrationStateForObserver(register: true)
        NotificationCenter.default.addObserver(self, selector:
            #selector(self.upgradeDM), name: NSNotification.Name(rawValue: NotificationIdentifier.upgardeDmIdentifier), object: nil)
        settingsTableView.reloadData()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //registering table view cell to table view
        self.settingsTableView.register(UINib(nibName: tableCellIdentifier, bundle: nil), forCellReuseIdentifier: tableCellIdentifier)
        
        settingsTableView.tableFooterView = UIView()
        settingsTableView.delegate = self
        settingsTableView.dataSource  = self

        settingsTableView.allowsMultipleSelection=false
        settingsTableView.separatorStyle = .none
        settingsTableView.backgroundColor = AppTheme.THEME_COLOR
        
        
        let nib = UINib (nibName: collectionCellIdentifier, bundle: nil)
        //registering collection view cell to collection view
        self.contactUsCollectionView.register(nib, forCellWithReuseIdentifier: collectionCellIdentifier)
        self.getDataSource()
        self.contactUsCollectionView.dataSource = self
        self.contactUsCollectionView.delegate = self
        let isVirtualCM = MyDefaults.getBoolData(key: DefaultKeys.IS_CM_VIRTUAL)
        isDMUpgradeAvailable = false
        
        //remove theft alert in table view if No CM Exists
        if isVirtualCM {
            settingsTableViewCellData.remove(at: SettingsTableViewIndexData.theftAlertIndex.rawValue)
        }
        
        
        self.view.backgroundColor = AppTheme.THEME_COLOR
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Ham Menu"), style: .plain, target: self, action: #selector(menuTapped))
        self.navigationItem.title = "LEGAL"
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.navigationController!.navigationBar.isTranslucent = false
        if self.isDMUpgradeMode {
            let dmOtaVc = DMOTAVC()
            self.pushVC(dmOtaVc)
        }
        self.displayAppVersion()
        NotificationCenter.default.addObserver(self, selector: #selector(refreshUI), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        settingsTableView.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationIdentifier.upgardeDmIdentifier), object: nil)
        // self.changeRegistrationStateForObserver(register: false)
        NotificationCenter.default.removeObserver(self)
    }
    
    deinit {
        self.changeRegistrationStateForObserver(register: false)
    }

    
    // MARK:Notification OFF Bluetooth Handlers
    func eventBLEStatusUpdate(notification:Notification){
        let status:BluetoothState = notification.userInfo!["data"] as! BluetoothState
        self.updateDMStatus(status: status)
        settingsTableView.reloadData()
      
    }

    func changeRegistrationStateForObserver(register:Bool){
        let sinkManager = SinkManager.sharedInstance
        if(register){
            sinkManager.addObserver(observer: self, notificationKey: SinkManager.sharedInstance.SINK_NOTIFICATION_BLUETOOTH_STATE_UPDATE, selectorName:eventBLEStatusUpdate)
        }else{
            sinkManager.removeObserver(observer: self, notificationKey: SinkManager.sharedInstance.SINK_NOTIFICATION_BLUETOOTH_STATE_UPDATE)
        }
    }
    
    
    //MARK:Private Methods
    func menuTapped() {
        self.slideMenuController()?.openLeft()
    }
    
    //Refreshs CM and DM Status
    func refreshUI() {
        self.updateCMStatus()
        self.updateDMStatus(status: BluetoothManager.sharedInstance().getBLEState())
    }
    
    func updateCMStatus(){
        //Configure Neuron Status
        let isCMRegisterd = MyDefaults.getBoolData(key: DefaultKeys.KEY_CMREGISTER)//-----Get CM Registration Status
        if isCMRegisterd {
            SettingsTableViewCellData.cMStatusImageName = "Connected"
            let serialNumber = BikerDetails.getBikerDetails(context: context!)?.device_unit_id //MyDefaults.getData(key: DefaultKeys.KEY_CMSERIAL_NO)
            settingsTableViewCellData[SettingsTableViewIndexData.connectivityModuleIndex.rawValue].description = serialNumber! + NSLocalizedString("keyConfiguredOnThisDevice", comment: "")
        }else{
            SettingsTableViewCellData.cMStatusImageName = ""
            settingsTableViewCellData[SettingsTableViewIndexData.connectivityModuleIndex.rawValue].description = NSLocalizedString("keyEmptyString", comment: "")
        }
    }
    
    func updateDMStatus(status:BluetoothState){
        self.setApplicationBadgeIcon()
        var name    = NSLocalizedString("keyEmptyString", comment: "")
        var imgName = NSLocalizedString("keyEmptyString", comment: "")
        
        switch status {
        case .CONNECTED_NP:
            //yellow
            print("CONNECTED_NP")
            imgName = "Paired & Not Connected"
            settingsTableViewCellData[SettingsTableViewIndexData.pairDisplayModuleIndex.rawValue].unPairButton = false
            SettingsTableViewCellData.btnAddDMIsUserInteraction = false
            DMTextUpdate(bleState: .CONNECTED_NP)
            break
        case .CONNECTED_P:
            //green
            imgName = "Connected" 
            let deviceName:String = MyDefaults.getData(key: DefaultKeys.KEY_BLE_CONNECTED) as! String
            if deviceName.isBlank {
                name = NSLocalizedString("keyEmptyString", comment: "")
            }
            else{
                name = String.localizedStringWithFormat(NSLocalizedString("keyDeviceNameConfigured", comment:""),deviceName)
            }
            DMTextUpdate(bleState: .CONNECTED_P)
            settingsTableViewCellData[SettingsTableViewIndexData.pairDisplayModuleIndex.rawValue].unPairButton = false
            SettingsTableViewCellData.btnAddDMIsUserInteraction = false
            print("CONNECTED_P")
            break
        case .DISCONNECT_NP:
            //plus
            displayModuleNotPaired()
            print("DISCONNECT_NP")
            imgName = "Add DM"
            SettingsTableViewCellData.btnAddDMIsUserInteraction = true
            settingsTableViewCellData[SettingsTableViewIndexData.pairDisplayModuleIndex.rawValue].unPairButton = true
            break
        case .DISCONNECT_P:
            //yellow
            imgName = "Paired & Not Connected"
            settingsTableViewCellData[SettingsTableViewIndexData.pairDisplayModuleIndex.rawValue].unPairButton = false
            SettingsTableViewCellData.btnAddDMIsUserInteraction = false
            print("DISCONNECT_P")
            DMTextUpdate(bleState: .DISCONNECT_P)
            break
        case .DISCONNECT_E:
            //plus
            displayModuleNotPaired()
            print("DISCONNECT_E")
            imgName = "Add DM"
            SettingsTableViewCellData.btnAddDMIsUserInteraction = true
            settingsTableViewCellData[SettingsTableViewIndexData.pairDisplayModuleIndex.rawValue].unPairButton = true
            break
        case .DISCONNECT_N:
            //plus
            print("DISCONNECT_N")
            displayModuleNotPaired()
            imgName = "Add DM"
            SettingsTableViewCellData.btnAddDMIsUserInteraction = true
            settingsTableViewCellData[SettingsTableViewIndexData.pairDisplayModuleIndex.rawValue].unPairButton = true
            
            break
        case .DISCONNECT_O:
            //yellow
            print("DISCONNECT_O")
            imgName = "Paired & Not Connected"
            settingsTableViewCellData[SettingsTableViewIndexData.pairDisplayModuleIndex.rawValue].unPairButton = false
            SettingsTableViewCellData.btnAddDMIsUserInteraction = false
            DMTextUpdate(bleState: .DISCONNECT_O)
            break
        case .DISCONNECT_BOP:
            //red
            imgName = "Bluetooth off"
            settingsTableViewCellData[SettingsTableViewIndexData.pairDisplayModuleIndex.rawValue].unPairButton = true
            SettingsTableViewCellData.btnAddDMIsUserInteraction = false
            print("DISCONNECT_BOP")
            DMTextUpdate(bleState: .DISCONNECT_BOP)
            break
        case .DISCONNECT_BONP:
            //plus
            displayModuleNotPaired()
            print("DISCONNECT_BONP")
            imgName = "Add DM"
            SettingsTableViewCellData.btnAddDMIsUserInteraction = true
            settingsTableViewCellData[SettingsTableViewIndexData.pairDisplayModuleIndex.rawValue].unPairButton = true
            break
        case .NONE:
            //plus
            displayModuleNotPaired()
            print("NONE")
            imgName = "Add DM"
            SettingsTableViewCellData.btnAddDMIsUserInteraction = true
            settingsTableViewCellData[SettingsTableViewIndexData.pairDisplayModuleIndex.rawValue].unPairButton = true
            break
        case .CONNECT_BONNP_NC:
            //plus
            displayModuleNotPaired()
           print("NONE")
            imgName = "Add DM"
            SettingsTableViewCellData.btnAddDMIsUserInteraction = true
            settingsTableViewCellData[SettingsTableViewIndexData.pairDisplayModuleIndex.rawValue].unPairButton = true
            break
        case .CONNECT_BONP_NC:
            //yellow
            imgName = "Paired & Not Connected"
            settingsTableViewCellData[SettingsTableViewIndexData.pairDisplayModuleIndex.rawValue].unPairButton = false
            SettingsTableViewCellData.btnAddDMIsUserInteraction = false
            print("DISCONNECT_P")
            DMTextUpdate(bleState: .DISCONNECT_P)
            break
        case .DISCONNECT_BOFFP:
            break
        case .DISCONNECT_BOFFNP:
            break
        case .DISCONNECT_BOFFP_NC:
            break
        case .DISCONNECT_BOFFNP_NC:
            break
        case .CONNECT_BOFFP:
            break
        case .CONNECT_BOFFNP:
            //plus
            displayModuleNotPaired()
            print(NSLocalizedString("keyStatusNONE", comment: ""))
            imgName = "Add DM"
            SettingsTableViewCellData.btnAddDMIsUserInteraction = true
            settingsTableViewCellData[SettingsTableViewIndexData.pairDisplayModuleIndex.rawValue].unPairButton = true
            break
        }
        
        //Updating DM Status Image and connected DM name
        settingsTableViewCellData[SettingsTableViewIndexData.pairDisplayModuleIndex.rawValue].description = name
        SettingsTableViewCellData.dMStatusImageName = imgName
    }
    
    //Updates whether it is ready for upgrade or not
    func DMTextUpdate(bleState:BluetoothState) {
        self.getFirmwareVersionFromManifest()
        let vesrion = getFirmwareVersion(version:  MyDefaults.getInt(key: DefaultKeys.KEY_JSON_VERSION))
        if MyDefaults.getInt(key: DefaultKeys.KEY_JSON_VERSION) == MyDefaults.getInt(key: DefaultKeys.KEY_DM_VERSION) || MyDefaults.getBoolData(key: DefaultKeys.KEY_UPDATE_SUCCESSFUL) {
            isDMUpgradeAvailable = false
            settingsTableViewCellData[SettingsTableViewIndexData.upgradeDisplayModuleIndex.rawValue].description = String.localizedStringWithFormat(NSLocalizedString("keyDisplayModuleVersionUpToDate", comment:""),vesrion)
        } else {
            isDMUpgradeAvailable = true
            switch bleState {
            case .DISCONNECT_P :
                //yellow
                settingsTableViewCellData[SettingsTableViewIndexData.upgradeDisplayModuleIndex.rawValue].description = String.localizedStringWithFormat(NSLocalizedString("keyDisplayModuleConnect", comment:""),vesrion)
                break
            case .DISCONNECT_O:
                //yellow
                settingsTableViewCellData[SettingsTableViewIndexData.upgradeDisplayModuleIndex.rawValue].description = String.localizedStringWithFormat(NSLocalizedString("keyDisplayModuleConnect", comment:""),vesrion)
                break
            case .CONNECTED_P:
                //green
                settingsTableViewCellData[SettingsTableViewIndexData.upgradeDisplayModuleIndex.rawValue].description = String.localizedStringWithFormat(NSLocalizedString("keyDisplayModuleProceed", comment:""),vesrion)
                break
            case .CONNECTED_NP:
                //yellow
                settingsTableViewCellData[SettingsTableViewIndexData.upgradeDisplayModuleIndex.rawValue].description = String.localizedStringWithFormat(NSLocalizedString("keyDisplayModuleConnect", comment:""),vesrion)
                
                break
            case .DISCONNECT_BOP:
                //red
                settingsTableViewCellData[SettingsTableViewIndexData.upgradeDisplayModuleIndex.rawValue].description = String.localizedStringWithFormat(NSLocalizedString("keyDisplayModulePair", comment:""),vesrion)
                
                break
            default:
                break
            }
        }
    }
    
    
    func getFirmwareVersion(version: Int)-> String {
        let vesrionString = String(version)
        var stringArray = Array(vesrionString)
        if stringArray.count > 1 {
            return ("\(stringArray[0]).\(stringArray[1]).\(stringArray[2])")
        }
        return NSLocalizedString("keyGetFirmWareVersion", comment: "")
    }
    
    
    func displayModuleNotPaired() {
        UIApplication.shared.applicationIconBadgeNumber = 0
        settingsTableViewCellData[SettingsTableViewIndexData.upgradeDisplayModuleIndex.rawValue].description = NSLocalizedString("keyDmNotPaired", comment: "")
    }
    
    //MARK: Button Actions
    
    func onUpgradeDmClicked() {
        if true {
            let dmOtaVc = DMOTAVC()
            self.pushVC(dmOtaVc)
        }
    }
    
    //Unpairing DM
   func btnUnpairAction() {
        SettingsTableViewCellData.btnAddDMIsUserInteraction = true
        settingsTableViewCellData[SettingsTableViewIndexData.pairDisplayModuleIndex.rawValue].unPairButton = true
        SettingsTableViewCellData.dMStatusImageName = "Add DM"
        BluetoothManager.sharedInstance().unpaireBLE()
        self.displayModuleNotPaired()
        settingsTableView.reloadData()
    }
    
    
    func displayAppVersion() {
        let nsObject: AnyObject? = Bundle.main.object(forInfoDictionaryKey: StringConstant.appVersion) as AnyObject
        //Then just cast the object as a String, but be careful, you may want to double check for nil
        if let version = nsObject as? String {
            settingsTableViewCellData[SettingsTableViewIndexData.appVersionIndex.rawValue].description = version
        }
    }
    
    //Adding DM
    func btnAddDMAction() {
        MyDefaults.setData(value: fromViewcontroller.SettingsVC.rawValue, key: DefaultKeys.KEY_FROMWHERE)
        let nav = UINavigationController(rootViewController: DeviceSetUpVC())
        self.presentVC(nav)
    }
    
    //MARK : Arm DisArm Button
    func btnTheftAlertAction() {
        let navigationController = UINavigationController(rootViewController:ArmDisarmVC())
        self.presentVC(navigationController)
    }
    
}

 //MARK:- Tableview Delegate & DataSource Methods
extension SettingVC :UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsTableViewCellData.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.backgroundColor = AppTheme.THEME_COLOR
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let tableViewCell = tableView.dequeueReusableCell(withIdentifier: tableCellIdentifier, for: indexPath) as! SettingsTableViewCell
        tableViewCell.nameLbl.text=settingsTableViewCellData[indexPath.row].name
        tableViewCell.descriptionLbl.text=settingsTableViewCellData[indexPath.row].description
        tableViewCell.unPairButton.isHidden=settingsTableViewCellData[indexPath.row].unPairButton
        tableViewCell.connectionStatusButton.isHidden=settingsTableViewCellData[indexPath.row].connectionStatusButton
        tableViewCell.isUserInteractionEnabled = true
        
        // disable selection Style for all cells execpt tappable cells
        if indexPath.row != SettingsTableViewIndexData.upgradeDisplayModuleIndex.rawValue || indexPath.row != SettingsTableViewIndexData.theftAlertIndex.rawValue {
            tableViewCell.selectionStyle = .none
        }
        
        if indexPath.row == SettingsTableViewIndexData.pairDisplayModuleIndex.rawValue {
            tableViewCell.connectionStatusButton.accessibilityIdentifier = "iv_add_dm"
            tableViewCell.connectionStatusButton.isUserInteractionEnabled = SettingsTableViewCellData.btnAddDMIsUserInteraction
            tableViewCell.unPairButton.accessibilityIdentifier = "btn_upairdm_mysupport"
            tableViewCell.connectionStatusButton.setImage(UIImage(named:SettingsTableViewCellData.dMStatusImageName), for: UIControlState.normal)
            tableViewCell.cellButtonTap.isHidden = true
            if tableViewCell.connectionStatusButton.currentImage != UIImage(named:"Connected"){
                SettingsTableViewCellData.isCellTapInteraction = false
            }
            else{
                SettingsTableViewCellData.isCellTapInteraction = true
            }
        }else if indexPath.row == SettingsTableViewIndexData.connectivityModuleIndex.rawValue {
            tableViewCell.descriptionLbl.accessibilityIdentifier = "lbl_cmdescription_mysupport"
            tableViewCell.connectionStatusButton.accessibilityIdentifier = "iv_cmstatus_mysupport"
            tableViewCell.connectionStatusButton.setImage(UIImage(named:SettingsTableViewCellData.cMStatusImageName), for: UIControlState.normal)
        }else if indexPath.row == SettingsTableViewIndexData.upgradeDisplayModuleIndex.rawValue{
            tableViewCell.isUserInteractionEnabled = true
            tableViewCell.cellButtonTap.accessibilityIdentifier = "btn_upgradedm_mysupport"
            if SettingsTableViewCellData.isCellTapInteraction {
                tableViewCell.cellButtonTap.isEnabled = true
            }
            else{
                tableViewCell.cellButtonTap.isEnabled = false
            }
        }else if indexPath.row == SettingsTableViewIndexData.theftAlertIndex.rawValue{
            tableViewCell.isUserInteractionEnabled = true
            tableViewCell.cellButtonTap.isEnabled = true
            tableViewCell.cellButtonTap.accessibilityIdentifier = "btn_theftalert_mysupport"
        }else if indexPath.row == SettingsTableViewIndexData.appVersionIndex.rawValue{
            tableViewCell.descriptionLbl.accessibilityIdentifier = "lbl_appversion_mysupport"
        }
        tableViewCell.delegate = self
        return tableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if indexPath.row == SettingsTableViewIndexData.upgradeDisplayModuleIndex.rawValue && SettingsTableViewCellData.isCellTapInteraction {
            tableView.cellForRow(at: indexPath)?.isUserInteractionEnabled = false
           SettingsTableViewCellData.isCellTapInteraction = false
        }
    }
    
}

//MARK:TableViewCell delegate methods
extension SettingVC:SettingsTableViewCellDelegate{
    
    // method to run when unpair button in table view cell is tapped
    func unPairButtonTapped(cell: SettingsTableViewCell, button: UIButton) {
        btnUnpairAction()
    }
    
    // method to run when Connection Status button in table view cell is tapped
    func connectionStatusButtonTapped(cell:SettingsTableViewCell ,button:UIButton){
        if let indexPath = settingsTableView.indexPath(for: cell){
            if indexPath.row == SettingsTableViewIndexData.pairDisplayModuleIndex.rawValue {
                btnAddDMAction()
            }
        }
    }
    
    // method to run when Table View Cell Button is Tapped
    func cellButtonTapped(cell: SettingsTableViewCell, button: UIButton) {
        if let indexPath = settingsTableView.indexPath(for: cell){
            if indexPath.row == SettingsTableViewIndexData.upgradeDisplayModuleIndex.rawValue {
        let ble_status = BluetoothManager.sharedInstance().getBLEState()
        if ble_status == .CONNECTED_P && isDMUpgradeAvailable  && SettingsTableViewCellData.isCellTapInteraction{
            onUpgradeDmClicked()
            SettingsTableViewCellData.isCellTapInteraction = false
                }
            }
            else if indexPath.row == SettingsTableViewIndexData.theftAlertIndex.rawValue {
                btnTheftAlertAction()
            }
        }
    }
}

    //MARK:ColectionView Flow Layout delegate,Data Source methods
extension SettingVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MFMailComposeViewControllerDelegate {
    
    //DataSource of Collection View
    func getDataSource() {
        imageArray.append(#imageLiteral(resourceName: "call"))
        imageArray.append(#imageLiteral(resourceName: "email"))
        imageArray.append(#imageLiteral(resourceName: "whatsapp"))
    }
    
    // number of items in Collection View
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: contactUsCellSize, height: contactUsCellSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat((collectionView.bounds.size.width - CGFloat(imageArray.count * contactUsCellSize) - CGFloat(contactUsCellSize))/2)
    }
    
    // creating a cell for each item in Collection View
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionCellIdentifier, for: indexPath) as! ContactUsCollectionViewCell
        cell.tag = indexPath.item
        switch cell.tag{
            case 0:
                cell.contactUsButton.accessibilityIdentifier = "cltn_call_mysupport"
                break
            case 1:
                cell.contactUsButton.accessibilityIdentifier = "cltn_email_mysupport"
                break
            case 2:
                cell.contactUsButton.accessibilityIdentifier = "cltn_whatsapp_mysupport"
                break
            default:
                break
        }
        cell.contactUsButton.setBackgroundImage(imageArray[indexPath.item], for: .normal)
        cell.contactUsButton.addTarget(self, action: #selector(contactUsEvent), for: .touchUpInside)
        return cell
    }
    
    func contactUsEvent(sender: UIButton) {
        
        if let collectionViewCell = sender.superview?.superview as? ContactUsCollectionViewCell {
            
            switch collectionViewCell.tag {
            case 0:
                //call button case in Collection View
                if let url = URL(string: NSLocalizedString("keyContactUsPhNo", comment: "")) {
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
                break
            case 1:
                //email button case in Collection View
                if MFMailComposeViewController.canSendMail() {
                    let mailController = MFMailComposeViewController()
                    mailController.mailComposeDelegate = self
                    mailController.setSubject(getDMSerialNo())
                    mailController.setToRecipients([NSLocalizedString("keyContactUsEmail", comment: "")])
                    mailController.navigationBar.tintColor = UIColor.white
                    self.presentVC(mailController)
                } else {
                    self.view.makeToast(NSLocalizedString("keyConfigureMailAccountMessage", comment: ""))
                }
                break
            case 2:
                //whatsapp button case in Collection View
                let msg = getDMSerialNo()
                let urlWhats = String.localizedStringWithFormat(NSLocalizedString("keyContactUsWhatsAppURL", comment: ""), msg)
                if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                    if let whatsappURL = NSURL(string: urlString) {
                        if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                            UIApplication.shared.open(whatsappURL as URL, options: [:], completionHandler: nil)
                        } else {
                            self.view.makeToast(NSLocalizedString("keyWhatsAppInstallMessage", comment: ""))
                        }
                    }
                }
                break
            default:
                break
            }
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case .cancelled:
            print(NSLocalizedString("keyMailCancelled", comment: ""))
            break
        case .saved:
            print(NSLocalizedString("keyMailSaved", comment: ""))
            break
        case .sent:
            print(NSLocalizedString("keyMailSent", comment: ""))
            break
        case .failed:
            print(NSLocalizedString("keyMailfailed", comment: ""))
            break
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func getDMSerialNo()-> String {
        do {
            if let deviceDetailsArray = try context?.fetch(DMDetails.fetchRequest()) as? [DMDetails] {
                if (deviceDetailsArray.count) > 0 {
                    let device = deviceDetailsArray.first
                    serialNo = device?.sr_no ?? NSLocalizedString("keyEmptyString", comment: "")
                    return serialNo == NSLocalizedString("keyEmptyString", comment: "") ? NSLocalizedString("keyNeedHelp", comment: "") : String.localizedStringWithFormat(NSLocalizedString("keyNeedHelpWithDMID", comment: ""), serialNo)
                }
            }
        } catch {
            
        }
        return NSLocalizedString("keyNeedHelp", comment: "") 
    }
}

