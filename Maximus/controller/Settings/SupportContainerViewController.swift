//
//  SupportContainerViewController.swift
//  Maximus
//
//  Created by Isha Ramdasi on 15/11/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//

import UIKit

class SupportContainerViewController: UIViewController {

    @IBOutlet weak var faqBtn: UIButton!
    @IBOutlet weak var installationBtn: UIButton!
    @IBOutlet weak var setupBtn: UIButton!
    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var containerView: UIView!
    var isDMUpgradeMode = false
     var bottomBorder:UIView?
    var installationGuide = SetupAndInstallationViewController()
    var setupMaximus = SettingVC()
    var faqVC = TermsAndCondVC()
    
    
    private var activeViewController: UIViewController? {
        didSet {
            removeInactiveViewController(inactiveViewController: oldValue)
            updateActiveViewController()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Ham Menu"), style: .plain, target: self, action: #selector(menuTapped))
        self.navigationItem.leftBarButtonItem?.tintColor = .white
       
        self.setupBtn.backgroundColor =  AppTheme.THEME_ORANGE
        self.setupBtn.titleLabel?.text = "Setup MaximusPro"
        self.setupBtn.titleLabel?.numberOfLines = 2
        self.setupBtn.titleLabel?.textAlignment = .center
        self.installationBtn.backgroundColor = AppTheme.THEME_ORANGE
        self.installationBtn.titleLabel?.text = "Installation & Usage Tutorial"
        self.installationBtn.titleLabel?.numberOfLines = 2
        self.installationBtn.titleLabel?.textAlignment = .center
        
        self.faqBtn.backgroundColor = AppTheme.THEME_ORANGE
        
       
        
        self.bottomBorder = UIView()
        self.bottomBorder?.backgroundColor = UIColor.white
        self.tabView.addSubview(bottomBorder!)
        updateTabClick(button: self.setupBtn)
        setupMaximus.isDMUpgradeMode = self.isDMUpgradeMode
        activeViewController = setupMaximus
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.navigationItem.title = "MY SUPPORT"
        self.navigationItem.titleView?.accessibilityIdentifier = "tv_actionbar_title"
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func menuTapped()
    {
        self.slideMenuController()?.openLeft()
    }
    
    func updateTabClick(button:UIButton){
        self.bottomBorder?.frame = CGRect(x: button.frame.x , y: 46, width: button.frame.width, height: 4)
    }
    
    @IBAction func onClickSetupBtn(_ sender: UIButton) {
        setupMaximus.isDMUpgradeMode = false
        activeViewController = setupMaximus
        updateTabClick(button: setupBtn)
    }
    
   
    @IBAction func onClickInstallationBtn(_ sender: UIButton) {
        activeViewController = installationGuide
        updateTabClick(button: installationBtn)
    }
    

    @IBAction func onClickFaqBtn(_ sender: UIButton) {
        faqVC.fileName = "FAQs_ app"
        faqVC.title = "FAQ's"
        activeViewController = faqVC
        updateTabClick(button: faqBtn)
    }
    
    // MARK : Handling VCs
    private func removeInactiveViewController(inactiveViewController: UIViewController?) {
        if let inActiveVC = inactiveViewController {
            // call before removing child view controller's view from hierarchy
            inActiveVC.willMove(toParentViewController: nil)
            
            inActiveVC.view.removeFromSuperview()
            
            // call after removing child view controller's view from hierarchy
            inActiveVC.removeFromParentViewController()
        }
    }
    
    private func updateActiveViewController() {
        if let activeVC = activeViewController {
            // call before adding child view controller's view as subview
            addChildViewController(activeVC)
            
            activeVC.view.frame = containerView.bounds
            containerView.addSubview(activeVC.view)
            
            // call before adding child view controller's view as subview
            activeVC.didMove(toParentViewController: self)
        }
    }
}
