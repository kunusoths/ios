//
//  FAQViewController.swift
//  Maximus
//
//  Created by kpit on 18/11/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//

import UIKit
import WebKit

class FAQViewController: UIViewController {

    // MARK: - class properties
    
    fileprivate let keyLoading = "loading"
    fileprivate let keyEstimateProgress = "estimatedProgress"
    
    
    @IBOutlet weak var txtView: UITextView!
    fileprivate var webView: WKWebView!
    @IBOutlet fileprivate weak var webViewContainer: UIView!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        viewConfigurations()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.txtView.setContentOffset(CGPoint.zero, animated: false)
        self.txtView.typingAttributes = [NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType]
    }

    
    
    // MARK: - private functions
    
    fileprivate func viewConfigurations() {
        
        
        //These are the changes for UnderTopBars & UnderBottomBars
        edgesForExtendedLayout = []
        extendedLayoutIncludesOpaqueBars = false
        automaticallyAdjustsScrollViewInsets = false
        
        webView = WKWebView()
        webView.navigationDelegate = self
        webViewContainer.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        let attributes: [NSLayoutAttribute] = [.top, .bottom, .right, .left]
        
        NSLayoutConstraint.activate(attributes.map {
            NSLayoutConstraint(item: webView, attribute: $0, relatedBy: .equal, toItem: webViewContainer, attribute: $0, multiplier: 1, constant: 0)
        })
        
        let urlWeb:URL?
        ////Remove it, once you get the working URL of T&C
        urlWeb = Bundle.main.url(forResource: "FAQs_app", withExtension: "docx")
        webView.contentMode = .scaleAspectFit
        webView.load(URLRequest(url: urlWeb!))
        
    }
}



extension FAQViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
            }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {

    }
    
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        if navigationAction.navigationType == WKNavigationType.linkActivated {
            
            let url = navigationAction.request.url
            let shared = UIApplication.shared
            
            if shared.canOpenURL(url!) {
                shared.openURL(url!)
            }
            
            decisionHandler(WKNavigationActionPolicy.cancel)
        }
        
        decisionHandler(.allow)
    }
}
