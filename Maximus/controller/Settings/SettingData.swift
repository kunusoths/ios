//
//  SettingData.swift
//  Maximus
//
//  Created by Admin on 03/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation

class SettingData: NSObject {
    
    var title:String?
    var status:String?
    
    init(title:String,status:String) {
        self.title  = title
        self.status = status
    }
}
