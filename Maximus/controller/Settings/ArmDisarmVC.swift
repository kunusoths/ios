//
//  ArmDisarmVC.swift
//  Maximus
//
//  Created by kpit on 10/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class ArmDisarmVC: UIViewController {
    
    @IBOutlet weak var lblTheftMsg: UILabel!
    @IBOutlet weak var imgTheft: UIImageView!
    @IBOutlet weak var switchTheftAlert: UISwitch!
    
    let theftAlertManager = TheftAlertManager()
    var vehicleID = 0
    var armState = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:UIImage(named: "Back Arrow White"), style: .plain, target: self, action: #selector(backAction))
        self.navigationItem.title = "THEFT ALERT SETTING"
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        //
        switchTheftAlert.addTarget(self, action: #selector(ArmDisarmVC.onSwitchValueChanged), for: .valueChanged)
        self.onSwitchTapped()
        //api call
        vehicleID = MyDefaults.getInt(key: DefaultKeys.KEY_DefaultVehicleID)
        theftAlertManager.theftAlertDelegate = self
        theftAlertManager.getArmStatus(subUrl: "/vehicles/\(vehicleID)/arm")
    }
    
    //MARK:Private Methods
    func menuTapped() {
        self.slideMenuController()?.openLeft()
    }
    
    func backAction() {
        self.dismiss(animated: true, completion: {
            if(self.switchTheftAlert.isOn)
            {
                self.theftAlertManager.postArmStatus(subUrl:"/vehicles/\(self.vehicleID)/arm")
            }else {
                self.theftAlertManager.postArmStatus(subUrl:"/vehicles/\(self.vehicleID)/disarm")
            }
        })
    }
    
    func onSwitchTapped() {
        let isCMRegisterd = MyDefaults.getBoolData(key: DefaultKeys.KEY_CMREGISTER)
        if isCMRegisterd {
            self.switchTheftAlert.isUserInteractionEnabled = true
        }else{
            self.switchTheftAlert.isUserInteractionEnabled = false
        }
    }
    
    func onSwitchValueChanged(){
        if(self.switchTheftAlert.isOn){
            self.updateArmStatus(status: 1)
            
        }else{
            self.updateArmStatus(status: 0)
            
        }
    }
    
    func updateArmStatus(status: Int)
    {
        if(status != 0){
            imgTheft.image = UIImage(named: "sad thief")
            lblTheftMsg.text = "You will start getting theft alerts if your bike moves without permission."
            self.switchTheftAlert.isOn = true
        }else{
            imgTheft.image = UIImage(named: "smiling thief")
            lblTheftMsg.text = "You will not get theft alerts if your bike moves without your permission."
            self.switchTheftAlert.isOn = false
        }
    }
    
}

extension ArmDisarmVC:TheftAlertDelegate{
    
    func onPostArmStatusSuccess(data: [String : Any]) {
        let armDisarmObj = data[StringConstant.armDisarmStatus] as! ArmDisArmModel
        self.updateArmStatus(status: armDisarmObj.status)
    }
    
    func onGetArmStatusSuccess(data: [String : Any]) {
        //
        let armDisarmObj = data[StringConstant.armDisarmStatus] as! ArmDisArmModel
        self.updateArmStatus(status: armDisarmObj.status)
    }
    
    func onFailure(Error:[String:Any]){
        self.view.hideToastActivity()
        let errModel:ErrorModel = Error["data"] as! ErrorModel
        print(errModel.message ?? " ")
        self.view.makeToast((errModel.description)!)
    }
    
}

