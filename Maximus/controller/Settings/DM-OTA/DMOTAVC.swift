//
//  DMOTAVC.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 06/10/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import ObjectMapper

class DMOTAVC: UIViewController {
    
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var progressViewOTA: UIProgressView!
    @IBOutlet weak var btnUpgradeDisplay: UIButton!
    
    @IBOutlet weak var updateView: UIView!
    @IBOutlet weak var vwOtaProgress: UIView!
    
    //Precondition labels
    @IBOutlet weak var lblDmBatreryStatus: UILabel!
    @IBOutlet weak var lblPhoneBatteryStatus: UILabel!
    @IBOutlet weak var lblBleConnectionStatus: UILabel!
    @IBOutlet weak var lblRideProgressStatus: UILabel!
    
    @IBOutlet weak var errorCodeLbl: UILabel!
    
    @IBOutlet weak var otaErrorView: UIView!
    @IBOutlet weak var vwOTASuccess: UIView!
    @IBOutlet weak var lblPercentageComplete: UILabel!
    
    @IBOutlet weak var imgConnection: UIImageView!
    @IBOutlet weak var imgPhonePowerState: UIImageView!
    @IBOutlet weak var imgDmPowerState: UIImageView!
    @IBOutlet weak var imgRideState: UIImageView!
    
    @IBOutlet weak var otaErrorImg: UIImageView!
    @IBOutlet weak var otaInProgressView: UIView!
    var isOTACompleted = false
    var isOTAInProgress = false
    
    var isPreconditionSucceeded = false
    let otaManager = DMOTAManager()
    var otaQueue = DispatchQueue(label: "maximus.ota.queue")
    
    let sinkManager = SinkManager.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = AppTheme.THEME_COLOR
        self.navigationItem.title = "UPGRADE DISPLAY MODULE"
        self.navigationItem.titleView?.accessibilityIdentifier = "tv_actionbar_title"
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Arrow White"), style: .plain, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItem?.accessibilityIdentifier = "btn_upgradedm_back"
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.navigationController!.navigationBar.isTranslucent = false
        
        self.btnUpgradeDisplay.backgroundColor = AppTheme.THEME_ORANGE
        self.btnDone.backgroundColor = AppTheme.THEME_ORANGE
        
        // To get phones battery level in precondition validation
        UIDevice.current.isBatteryMonitoringEnabled = true

        OTAWrapper.sharedInstace().createOTASession()
        otaManager.otaDelegate = self
        otaManager.validatePreConditions()
        
        self.view.backgroundColor = AppTheme.THEME_COLOR
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sinkManager.addObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_BLUETOOTH_STATE_UPDATE, selectorName: self.eventBLEStatusUpdate)
        self.btnUpgradeDisplay.backgroundColor = AppTheme.THEME_ORANGE
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        OTAWrapper.sharedInstace().endOTASession()
        sinkManager.removeObserver(observer: self, notificationKey: sinkManager.SINK_NOTIFICATION_BLUETOOTH_STATE_UPDATE)
    }
    
    
    func backAction() {
        self.popVC()
    }
    
    func eventBLEStatusUpdate(notification:Notification){
        let status:BluetoothState = notification.userInfo!["data"] as! BluetoothState
        
        if status != .CONNECTED_P {
            if isOTAInProgress{
                self.handleOtaError(err: ["error":  OTAErrorCases.BLE_DISCONNECTED])
                OTAWrapper.sharedInstace().endOTASession()
            }
        }
    }
    
    fileprivate func startOTAProcess() {
        if !self.isPreconditionSucceeded{
            self.view.makeToast("Unable to Proceed. Please check Preconditions !!")
            
        }else{
            BluetoothServices.sharedInstance().invalidateHeartBeat()
            self.view.makeToastActivity(.center, isUserInteraction: false)

            self.otaQueue.async {
                if self.isPreconditionSucceeded {
                    
                    let otaData:[String:Data] = BluetoothManager.sharedInstance().getOTAMode()
                    self.otaManager.startOTA()
                    self.otaManager.otaDelegate = self
                    
                    if otaData.has(CURRENT_DM_MODE.DM_OTA_MODE){
                        let otaModeData:Data = otaData[CURRENT_DM_MODE.DM_OTA_MODE]!
                        OTAWrapper.sharedInstace().otaGetModeStatusCB(otaModeData)
                        
                    }else{
                        CPWrapper.sharedInstance().cp_firmwareUpdateStart()
                    }
                    
                }
                
            }
        }
    }
    
    func showUpgradeAlert() {
        // Create the alert controller
        let message = "Now your upgrade is going to start. Please stay connected to Bluetooth. It may take a while. Do you want to continue ?"
        let alertController = UIAlertController(title: "Upgrade Display Module", message: message, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Continue", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.startOTAProcess()
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            self.popVC()
        }
        okAction.setAccessibilityIdentifier(accessabilityIdentifier: "btn_continue_upgradedmpopup")
        cancelAction.setAccessibilityIdentifier(accessabilityIdentifier: "btn_cancel_upgradedmpopup")
        alertController.view.accessibilityIdentifier = "vw_upgradedmpopup"
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: {
            alertController.applyAccessibilityIdentifiers()
        })
    }
    
    @IBAction func onHomeClick(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func onDmUpgradeClick(_ sender: Any) {
            self.showUpgradeAlert()
    }
}

extension DMOTAVC:OTAManagerDelegate {
    func phoneBatterySufficent() {
        self.lblPhoneBatteryStatus.text = "Your phone is adequately charged or is getting charged"
        self.imgPhonePowerState.image = #imageLiteral(resourceName: "Tickmark Selected")
    }
    
    func dmBatterySufficeint() {
        self.lblDmBatreryStatus.text = "Your Display Module battery is adequately charged or is getting charged"
        self.imgDmPowerState.image = #imageLiteral(resourceName: "Tickmark Selected")
    }
    
    func bleConnected() {
        DispatchQueue.main.async {
            self.lblBleConnectionStatus.text = "Your phone is connected and paired."
            self.imgConnection.image = #imageLiteral(resourceName: "Tickmark Selected")
        }

    }
    
    func noRideIsInProgress() {
        self.lblRideProgressStatus.text = "Currently there are no active rides"
        self.imgRideState.image = #imageLiteral(resourceName: "Tickmark Selected")
    }
    
    func otaHasBeenStarted() {
        DispatchQueue.main.async {
            self.view.hideToastActivity()
            self.vwOtaProgress.isHidden = false
            self.otaInProgressView.isHidden = false
            self.updateView.isHidden = true
            self.navigationItem.leftBarButtonItem?.tintColor = .clear
            self.navigationItem.leftBarButtonItem?.isEnabled = false
        }
    }
    
    func otaSuccess() {
        DispatchQueue.main.async {
            
            self.vwOtaProgress.isHidden = true
            self.otaInProgressView.isHidden = true
            self.updateView.isHidden = true
            self.vwOTASuccess.isHidden = false
            self.isOTACompleted = true
            self.isOTAInProgress = false
            self.getFirmwareVersionFromManifest()
            MyDefaults.setInt(value: MyDefaults.getInt(key: DefaultKeys.KEY_JSON_VERSION), key: DefaultKeys.KEY_DM_VERSION)
            var defaultState = 5
            let defaultVal = Data(bytes: &defaultState, count: MemoryLayout<Int>.size)
            // var val = [CURRENT_DM_MODE.DM_DEFAULT_MODE:defaultVal]
            
            BluetoothManager.sharedInstance().setOTAMode(data: defaultVal)
            MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_UPDATE_SUCCESSFUL)
            self.btnUpgradeDisplay.backgroundColor = UIColor(r: 155.0, g: 155.0, b: 155.0, a: 1.0)
            UIApplication.shared.applicationIconBadgeNumber = 0
            let dmObj = AppDetailsHandler.sharedInstance.getDmDetailsObj()
            AppDetailsHandler.sharedInstance.sendDMDetails(details: dmObj)
        }
    }
    
    
    func handleOtaError(err: [String : Any]) {
        //
        //self.btnUpgradeDisplay.isUserInteractionEnabled = false
        MyDefaults.setBoolData(value: false, key: DefaultKeys.KEY_UPDATE_SUCCESSFUL)
        DispatchQueue.main.async {
            self.btnUpgradeDisplay.isUserInteractionEnabled = false
        }
        
        self.isPreconditionSucceeded = false
        let errorCase:OTAErrorCases = err["error"] as! OTAErrorCases
        var errMessage = ""
        
        switch errorCase {
        case .BLE_NOT_CONNECTED:
            errMessage = "Turn on Bluetooth on your phone and pair with Display Module."
            self.lblBleConnectionStatus.text = errMessage
            self.imgConnection.image = #imageLiteral(resourceName: "Red Tickmark")
            break
            
        case .DM_BATTERY_LOW:
            self.lblDmBatreryStatus.text = "Your Display Module is neither charging nor is it adequately charged."
            self.imgDmPowerState.image = #imageLiteral(resourceName: "Red Tickmark")
            break
            
        case .INSUFFICIENT_SPACE_ON_DM:
            errMessage = "Something did not work as expected.\nError code 10.\nPlease call MaximusPro Customer Support @1800-212-5359(Toll free)"
            self.showOTAAbortAlert(errMessage: "10")
            break
            
        case .PHONE_BATTERY_LOW:
            self.lblPhoneBatteryStatus.text = "Your phone is neither charging nor is it adequately charged."
            self.imgPhonePowerState.image = #imageLiteral(resourceName: "Red Tickmark")
            
        case .RIDE_IN_PROGRESS:
            self.lblRideProgressStatus.text = "Make sure you stop the ride in progress."
            self.imgRideState.image = #imageLiteral(resourceName: "Red Tickmark")
            break
            
        case .BLE_DISCONNECTED:
            errMessage = "Disconnected from bluetooth. Please try again later"
            showOTAAbortAlert(errMessage: "Error : Bluetooth Disconnected")
            break
            
        case .OTA_ABORTED:
            errMessage = "Something did not work as expected.\nError code 60.\nPlease restart your Application and Display Module and retry."
            showOTAAbortAlert(errMessage: "Error Code : 60")
            break
            
        case .OTA_TIME_OUT_ERROR:
            errMessage = "Something did not work as expected.\nError code 60.\nPlease restart your Application and Display Module and retry."
            showOTAAbortAlert(errMessage: "Error Code : 60")
            break
        case .OTA_EXCEEDED_REPEATED_ATTEMPT_COUNT:
            errMessage = "Something did not work as expected.\nError code 70.\nPlease restart your Application and Display Module and retry."
            showOTAAbortAlert(errMessage: "Error Code : 70")
            break
        case .OTA_DM_FLASH_ERROR:
            errMessage = "Something did not work as expected.\nError code 20.\nPlease call MaximusPro Customer Support @1800-212-5359(Toll free)"
            showOTAAbortAlert(errMessage: "Error Code : 20")
            break
        case .OTA_UNEXPECTED_END_OF_FILE_OCCURED:
            errMessage = "Something did not work as expected.\nError code 40.\nPlease restart your Application and Display Module and retry."
            showOTAAbortAlert(errMessage: "Error Code : 40")
            break
        case .OTA_ERROR_IN_READING_FILE:
            errMessage = "Something did not work as expected.\nError code 50.\nPlease restart your Application and Display Module and retry."
            showOTAAbortAlert(errMessage: "Error Code : 50")
            break
        case .OTA_ERROR_IN_READING_IMAGE_PACKET:
            errMessage = "Something did not work as expected.\nError code 80.\nPlease restart your Application and Display Module and retry."
            showOTAAbortAlert(errMessage: "Error Code : 80")
            break
        
        case .OTA_CHECKSUM_ERROR:
            errMessage = "Something did not work as expected.\nError code 100.\nPlease restart your Application and Display Module and retry."
            showOTAAbortAlert(errMessage: "Error Code : 100")
            
            
        default:
            break
        }
        
        
        return
    }
    
    func showOTAAbortAlert(errMessage: String) {
        DispatchQueue.main.async {
            self.otaErrorImg.isHidden = false
            self.otaErrorView.isHidden = false
            self.errorCodeLbl.text = errMessage
            self.vwOtaProgress.isHidden = false
            self.otaInProgressView.isHidden = true
            self.updateView.isHidden = true
            self.vwOTASuccess.isHidden = true
            self.lblPercentageComplete.isHidden = true
            self.progressViewOTA.isHidden = true
        }
        

        MyDefaults.setBoolData(value: false, key: DefaultKeys.KEY_UPDATE_SUCCESSFUL)
    }
    
    func otaAbortAction(action: UIAlertAction) {
        //Use action.title
        self.popVC()
    }
    
    func preConditionSucess() {
        //
        self.isPreconditionSucceeded = true
        DispatchQueue.main.async {
            self.btnUpgradeDisplay.isUserInteractionEnabled = true
        }
    }
    
    
    func updateOTACompleteStatus(per : Float)
    {
        DispatchQueue.main.async {
            self.isOTAInProgress = true
            self.progressViewOTA.progress = (per/100)
            let value = Int(per)
            self.lblPercentageComplete.text = "\(value)%"
            
        }
        
    }
    
    
}
