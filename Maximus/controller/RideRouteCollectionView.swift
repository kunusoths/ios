//
//  RideRouteCollectionView.swift
//  Maximus
//
//  Created by Isha Ramdasi on 13/07/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

protocol PassWaypointsDataToController: class {
    func passData(category:SQlitePoiCatagoryType, arrOfWavepoints: [PointsOfInterests])
    func setMarkerToNil()
}

fileprivate let sectionInsets = UIEdgeInsets(top: 0.0, left: 10.0, bottom: 0.0, right: 10.0)

class RideRouteCollectionView: UICollectionView {
    let cellIdentifier = "PlanRideRouteCell"
    var isPlotRest = true
    var isPlotHosp = true
    var isPlotPolice = true
    var isPlotFuel = true
    var isPlotHotel = true
    var poiRideID = Int()
    var arrMarkersWayPointsRest = [GMSMarker]()
    var arrMarkersWayPointsPolice = [GMSMarker]()
    var arrMarkersWayPointsFuel = [GMSMarker]()
    var arrMarkersWayPointsHosp = [GMSMarker]()
    var arrMarkersWayPointsHotel = [GMSMarker]()
    
    var markerCurrentSelected = GMSMarker()
    weak var passDataDelegate: PassWaypointsDataToController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        self.dataSource = self
        self.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(resetSelectedWaypointValue(_:)), name: NSNotification.Name(rawValue: "SelectedWaypoint"), object: nil)
    }
}
    extension RideRouteCollectionView : UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
        //1
        func collectionView(_ collectionView: UICollectionView,
                            layout collectionViewLayout: UICollectionViewLayout,
                            sizeForItemAt indexPath: IndexPath) -> CGSize {
            //2
            let itemsPerRow: CGFloat = 3
            let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
            let availableWidth = collectionView.frame.width - paddingSpace
            let widthPerItem = availableWidth / itemsPerRow
            print("padding:\(paddingSpace)\n AvailabeWidth:\(availableWidth)\nFinalWidth:\(widthPerItem)")
            return CGSize(width: widthPerItem.rounded(.towardZero), height: 51)
            
        }
        
        //3
        func collectionView(_ collectionView: UICollectionView,
                            layout collectionViewLayout: UICollectionViewLayout,
                            insetForSectionAt section: Int) -> UIEdgeInsets {
            return sectionInsets
        }
        
        // 4
        func collectionView(_ collectionView: UICollectionView,
                            layout collectionViewLayout: UICollectionViewLayout,
                            minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return sectionInsets.left
        }
        
        //MARK:- CollectionView Delegates
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            print(" indexPath ",indexPath.row)
            let cell = collectionView.cellForItem(at: indexPath) as! PlanRideRouteCell
            
            switch indexPath.row {
            case 0:
                if isPlotFuel {
                    cell.imgViewPOI.image = #imageLiteral(resourceName: "Fuel Pump_Actionbar_Selected")
                    cell.selectedPOIView.backgroundColor = UIColor(r: 255, g: 75, b: 0)
                    cell.lblSelectPOI.textColor  =  UIColor(r: 255, g: 75, b: 0)
                    self.fetchPOIFromDB(catagory: SQlitePoiCatagoryType.KEY_POI_GAS_STATION)
                }
                else{
                    cell.imgViewPOI.image = #imageLiteral(resourceName: "Fuel Pump_Actionbar_Unselected")
                    cell.selectedPOIView.backgroundColor = UIColor.clear
                    cell.lblSelectPOI.textColor  =  UIColor.white
                    removePlotWayPoints(catagoryType:SQlitePoiCatagoryType.KEY_POI_GAS_STATION)
                    markerCurrentSelected.map = nil
                    self.passDataDelegate?.setMarkerToNil()
                }
                break
            case 1:
                if isPlotHosp {
                    cell.imgViewPOI.image = #imageLiteral(resourceName: "Hospital_Actionbar_Selected")
                    cell.selectedPOIView.backgroundColor = UIColor(r: 255, g: 75, b: 0)
                    self.fetchPOIFromDB(catagory: SQlitePoiCatagoryType.KEY_POI_HOSPITAL)
                    cell.lblSelectPOI.textColor  =  UIColor(r: 255, g: 75, b: 0)
                }
                else{
                    cell.imgViewPOI.image = #imageLiteral(resourceName: "Hospital_Actionbar_Unselected")
                    cell.selectedPOIView.backgroundColor = UIColor.clear
                    cell.lblSelectPOI.textColor  =  UIColor.white
                    removePlotWayPoints(catagoryType:SQlitePoiCatagoryType.KEY_POI_HOSPITAL)
                    self.passDataDelegate?.setMarkerToNil()
                    markerCurrentSelected.map = nil
                }
                break
            case 2:
                if isPlotPolice {
                    cell.imgViewPOI.image = #imageLiteral(resourceName: "Police Station_Actionbar_Selected")
                    cell.selectedPOIView.backgroundColor = UIColor(r: 255, g: 75, b: 0)
                    cell.lblSelectPOI.textColor  =  UIColor(r: 255, g: 75, b: 0)
                    self.fetchPOIFromDB(catagory: SQlitePoiCatagoryType.KEY_POI_POLICE)
                    
                }
                else{
                    cell.imgViewPOI.image = #imageLiteral(resourceName: "Police Station_Actionbar_Unselected")
                    cell.selectedPOIView.backgroundColor = UIColor.clear
                    cell.lblSelectPOI.textColor  =  UIColor.white
                    removePlotWayPoints(catagoryType:SQlitePoiCatagoryType.KEY_POI_POLICE)
                    markerCurrentSelected.map = nil
                    self.passDataDelegate?.setMarkerToNil()
                }
                break
            default:
                break
            }
            
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 3
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! PlanRideRouteCell
            
            if(indexPath.row == 0){
                cell.imgViewPOI.image = UIImage(named: "Fuel Pump_Actionbar_Unselected")
                cell.lblSelectPOI.text = "Fuel Pump"
            }else if(indexPath.row == 1){
                cell.imgViewPOI.image = UIImage(named: "Hospital_Actionbar_Unselected")
                cell.lblSelectPOI.text = "Hospital"
            }else{
                cell.imgViewPOI.image = UIImage(named: "Police Station_Actionbar_Unselected")
                cell.lblSelectPOI.text = "Police Station"
            }
            
            //cell.btnSelectPOI.setCornerRadius(radius: 20)
            return cell
        }
        
        //MARK:POI Fetch
        func fetchPOIFromDB(catagory:SQlitePoiCatagoryType)  {
            let deviceLocation = DeviceLocationServiece.sharedInstance()
            let userLoc:CLLocation = deviceLocation.getUserCurrentLocation()
            
            var db : DBManager?
            do {
                db = try DBManager.open(path: part1DbPath)
            } catch let message {
                print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
                print(message)
            }
            
            do {
                
                let arrWaypoints = try db?.fetchPoIByCriteriaFromDB(table: SQLTableType.POINT_OF_INTEREST, queryString: DBQuery.QUERY_FETCH_POI_BY_CATEGORIES, id: Int32(poiRideID), catagory: catagory.rawValue, lat: userLoc.coordinate.latitude, longitude: userLoc.coordinate.longitude, radius: 10)
                
                self.passDataDelegate?.passData(category: catagory, arrOfWavepoints: arrWaypoints! as! [PointsOfInterests])
                
            } catch {
                print(db?.errorMessage ?? "error Message while Fetching record from Table")
            }
            
            // deviceLocation.stopUpdatingLocation()
            
        }
        
        //MARK:POI Remove
        func removePlotWayPoints(catagoryType:SQlitePoiCatagoryType)  {
            
            switch catagoryType {
            case SQlitePoiCatagoryType.KEY_POI_RESTAURANT:
                isPlotRest = true
                for removeMarker in arrMarkersWayPointsRest {
                    removeMarker.map = nil
                }
                break
            case SQlitePoiCatagoryType.KEY_POI_GAS_STATION:
                isPlotFuel = true
                for removeMarker in arrMarkersWayPointsFuel {
                    removeMarker.map = nil
                }
                break
            case SQlitePoiCatagoryType.KEY_POI_HOTEL:
                isPlotHotel = true
                for removeMarker in arrMarkersWayPointsHotel {
                    removeMarker.map = nil
                }
                break
            case SQlitePoiCatagoryType.KEY_POI_POLICE:
                isPlotPolice = true
                for removeMarker in arrMarkersWayPointsPolice {
                    removeMarker.map = nil
                }
                break
            case SQlitePoiCatagoryType.KEY_POI_HOSPITAL:
                isPlotHosp = true
                for removeMarker in arrMarkersWayPointsHosp {
                    removeMarker.map = nil
                }
                break
            }
}
        
        func resetSelectedWaypointValue(_ notification: Notification) {
            if let data = notification.userInfo as? [String: Any] {
                if let selectedCategory = data["category"] as? SQlitePoiCatagoryType, data["isSelected"] as? Bool == true {
                    self.removePlotWayPoints(catagoryType: selectedCategory)
                }
                if let category = data["category"] as? SQlitePoiCatagoryType {
                    switch category {
                    case SQlitePoiCatagoryType.KEY_POI_GAS_STATION:
                        isPlotFuel = false
                        break
                    case SQlitePoiCatagoryType.KEY_POI_HOSPITAL:
                        isPlotHosp = false
                        break
                    case SQlitePoiCatagoryType.KEY_POI_HOTEL:
                        isPlotHotel = false
                        break
                    case SQlitePoiCatagoryType.KEY_POI_POLICE:
                        isPlotPolice = false
                        break
                    case SQlitePoiCatagoryType.KEY_POI_RESTAURANT:
                        isPlotRest = false
                        break
                    }
                }
            }
        }
}
