//
//  POIImageView.swift
//  Maximus
//
//  Created by Isha Ramdasi on 12/07/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit

class POIImageView: UIImageView {

    var poiImage: UIImage? {
        set {
            image = newValue
        }
        get {
            return self.image
        }
    }
    
    
    
    
    override init(image: UIImage?) {
        super.init(image: image)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
