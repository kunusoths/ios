//
//  PlanRideInDetail.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 19/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//
//getAllplannedwavepoints
import UIKit
import ObjectMapper
import GoogleMaps
import GooglePlaces

extension Int {
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
}

extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}

let imageSize: CGFloat = 25.0
let heightOfMarkerView:CGFloat = 150
fileprivate let sectionInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)

class PlanRideInDetail: UIViewController , GMSMapViewDelegate, UpdateMapView {
    
    
    //MARK:- Properties
    //MARK:- IBOutlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet var collectionViewPOI: PlanRideInDetailCollectionView!
    @IBOutlet weak var viewMarkerInfo: UIView!
    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblMarkerInfoTitle: UILabel!
    @IBOutlet weak var lblOpenNow: UILabel!
    @IBOutlet weak var lblBuissnessPhoneNumber: UILabel!
    @IBOutlet weak var viewStarRating: CosmosView!
    @IBOutlet weak var imgViewContact: UIImageView!
    @IBOutlet weak var imgViewNoRating: UIImageView!
    @IBOutlet weak var lblNoRatings: UILabel!
    @IBOutlet weak var imagviewBusinessHours: UIImageView!
    @IBOutlet weak var widthConstraintBtnADD: NSLayoutConstraint!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnRemove: UIButton!
    
    
    //MARK:- Constants
    let cellIdentifier = "PlanRideInDetailCell"
    var selectedPOi:POIType = .ViaPoint
    var selectedMarker:GMSMarker!
    let POI_TYPE_FUELPUMPS = 0
    let POI_TYPE_RESTAURANTS = 1
    let POI_TYPE_HOTELS = 2
    var CURRENET_POI_SELECTED = -1
    var PREV_POI_SELECTED = -1
    var createdRideId:Int?
    //POI Selected,Unselected Arrays
    var arrSelectedFuelPumps:[POIList] = []
    var arrSelectedRestaurants:[POIList] = []
    var arrSelectedHotels:[POIList] = []
    
    var arrUnselectedFuelPumps:[POIList] = []
    var arrUnselectedRestaurants:[POIList] = []
    var arrUnselectedHotels:[POIList] = []
    
    //Markers poi selected, unselected array
    var arrMarkersSelectedPOIs = [GMSMarker]()
    var arrMarkersUnSelectedPOIs = [GMSMarker]()
    var markerCurrentSelected = GMSMarker()
    
    var selectedPOIIndex:NSInteger = 0
    var selectedPOI:POIList?
    var poiAlreadySelected:Bool = false
    //Wavepoint list and markers array
    var arrPreviousSelectedPOIS:[POIList] = []
    var sourceLocation:CLLocation?
    var bounds = GMSCoordinateBounds()
    var routeDetails: RouteList?
  //  var directionResult:DirectionDetails?
    var sourceName = ""
    var destinationName = ""
    var distance: String?
    var isEditMode: PlanRideFlowOptions = PlanRideFlowOptions(rawValue: 0)!
    let previousSelectedType = -1
    let planRideRepo = PlanRideRepository()
    var planRideViewModel: PlanRideViewModel?
    var isMarkerViewOpen = false
    var recommendedObj: RecommendedResponse?
    private var clusterManager: GMUClusterManager!
    let dataManager = PlanRideDataManager.shared()
    var selectedPoiType = 0
    //MARK:- Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
       // self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Arrow White"), style: .plain, target: self, action: #selector(menuTapped))
        self.navigationItem.title = StringConstant.VIAPOINT_TITLE //ScreenTitle.RideDeatils
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(onDoneButtonClick))
        self.navigationItem.rightBarButtonItem?.accessibilityIdentifier = "btn_navigationbar_done"
        self.navigationItem.rightBarButtonItem?.tintColor = .white
        self.mapView.accessibilityElementsHidden = false

        viewMarkerInfo.isHidden =  true
        collectionViewPOI.updateViewDelegate = self
        collectionViewPOI.poiDelegate = self
        //API calls
        LoadingHandler.shared.startloading(interactionEnable: true, view: self.view)
        collectionViewPOI.isUserInteractionEnabled =  false
        planRideViewModel = PlanRideViewModel(planRideRepoDelegate: planRideRepo)
        if let polyLine = dataManager.routeObj?.totalPolylinePoints?.points {
            planRideViewModel?.getPOIWithPolyline(polyline: polyLine , request: "GetPOIWithPolyline", successCallback: { (data) in
                self.savePoiWithPolyline(data: data)
            }) { (error) in
                LoadingHandler.shared.stoploading()
                self.failure(data: error)
            }
        } else {
            self.view.makeToast("Route not present")
        }
        self.mapView.setDefaultCameraPosition()
        self.mapView.padding = UIEdgeInsetsMake(80, 0, 0, 0)
        if let route = dataManager.routeObj {
              self.loadPolyline(route: route)
        }
        //self.loadPolyline(route: dataManager.routeObj!)
        mapView.delegate = self
        self.addSelectedMarkers()
        //SET MAP style to night mode and default camera
        MapTheme.shared.setTheme(map: mapView)///---Set Google map theme color
//        let iconGenerator = GMUDefaultClusterIconGenerator()
//        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
//        let renderer = GMUDefaultClusterRenderer(mapView: mapView, clusterIconGenerator: iconGenerator)
//        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm, renderer: renderer)
//
//        // Register self to listen to both GMUClusterManagerDelegate and GMSMapViewDelegate events.
//        clusterManager.setDelegate(self, mapDelegate: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- Private Methods
    func menuTapped() {
        self.popVC()
    }
    
    func addSelectedMarkers() {
        self.arrMarkersSelectedPOIs = dataManager.selectedStopArray
        self.arrMarkersSelectedPOIs = dataManager.getAddedStopMarkers()
        dataManager.removeAllSelectedStopsFromAllStops()
//        self.arrSelectedFuelPumps = arrPreviousSelectedPOIS.sorted({ 0.})
    }
    
    func onDoneButtonClick() {
        if dataManager.viaPointsArray.count > 21 {
            self.view.makeToast(NSLocalizedString("keyEnter21Stops", comment: ""))
        } else {
            if let viewController = self.navigationController?.viewControllers {
                let views:[UIViewController] = viewController
                
                var planRideVc:PlanRide?
                
                for view in views {
                    if view.isKind(of: PlanRide.self) {
                        planRideVc = (view as! PlanRide)
                    }
                }
                planRideVc?.doUpdatePolyline = true
                self.navigationController?.popToViewController(planRideVc!, animated: true)
            }
        }
    }
    
    func updateMarkerView(isMarkerViewHidden: Bool, showMarkerInfo: Bool) {
        markerCurrentSelected.map = nil
        showMarkerInfoView(showView: false)
        if(!isMarkerViewHidden) || isMarkerViewOpen{
            self.showMarkerInfoView(showView: false)
        }
       
    }
    
    func hideBottomPreview(toShow: Bool) {
    }
    
    func selectedPOIType(currentSelectedPOI: Int, previousSelectedPOI: Int) {
        self.CURRENET_POI_SELECTED = currentSelectedPOI
        self.PREV_POI_SELECTED = previousSelectedPOI
    }
    
    func plotSelectedPOI() {
        var markerIcon = #imageLiteral(resourceName: "Waypoint")
        if arrPreviousSelectedPOIS.count > 0 {
            for i in 0 ..< arrPreviousSelectedPOIS.count {
                let markerInfo:NSMutableDictionary = [:]
                if((arrPreviousSelectedPOIS[i].poiListResponse.types?.contains(StringConstant.PLACES_OF_INTEREST))! || (arrPreviousSelectedPOIS[i].poiListResponse.types?.contains(StringConstant.WAVEPOINT_TYPE_MILESTONE))!){
                    if((arrPreviousSelectedPOIS[i].poiListResponse.types?.contains(StringConstant.WAVEPOINT_TYPE_MILESTONE))!){
                        markerIcon = #imageLiteral(resourceName: "Waypoint")
                    }else if let categories = arrPreviousSelectedPOIS[i].poiListResponse.categories {
                        
                        if (categories.contains(StringConstant.POI_CATEGORY_FUEL_PUMPS)){
                            markerIcon = #imageLiteral(resourceName: "Petrol Small")
                            markerInfo[StringConstant.POI_TYPE] = POI_TYPE_FUELPUMPS
                        }
                        else if (categories.contains(StringConstant.POI_CATEGORY_RESTAURANTS)){
                            markerIcon = #imageLiteral(resourceName: "Restaurants Small")
                            markerInfo[StringConstant.POI_TYPE] = POI_TYPE_RESTAURANTS
                        }
                        else if (categories.contains(StringConstant.POI_CATEGORY_LODGING)){
                            markerIcon = #imageLiteral(resourceName: "Hotel Small")
                            markerInfo[StringConstant.POI_TYPE] = POI_TYPE_HOTELS
                        }
                    }
                    
                    let marker = GMSMarker()
                    marker.position = CLLocationCoordinate2D(latitude: (arrPreviousSelectedPOIS[i].poiListResponse.location?.latitude)!, longitude: (arrPreviousSelectedPOIS[i].poiListResponse.location?.longitude)!)
                    let resizedimage = UIImage.scaleTo(image: markerIcon, w: 40.0, h: 44.0)
                    marker.icon = resizedimage
                    //Set Marker Info Data
                    markerInfo[StringConstant.POI_TITLE] = arrPreviousSelectedPOIS[i].poiListResponse.name
                    markerInfo[StringConstant.POI_LAT] = (arrPreviousSelectedPOIS[i].poiListResponse.location?.latitude)!
                    markerInfo[StringConstant.POI_LONG] = (arrPreviousSelectedPOIS[i].poiListResponse.location?.longitude)!
                    markerInfo[StringConstant.MARKER_POI_TYPE] = StringConstant.VIA_POINT
                    markerInfo[StringConstant.POI_WHOLE_DATA] = arrPreviousSelectedPOIS[i]
                    marker.userData = markerInfo
                    //Info data setting done
                    
                    marker.map = self.mapView
                    bounds = bounds.includingCoordinate(marker.position)
                    arrMarkersSelectedPOIs.append(marker)
                    collectionViewPOI.arrMarkersSelectedPOIs.append(marker)
                }
            }
        }
    }
    
    
    func showMarkerInfoView(showView:Bool) {
        print(self.viewMarkerInfo.size.height)
        if(showView){
            if(self.viewMarkerInfo.isHidden){
                self.bottomViewConstraint.constant += heightOfMarkerView
                self.viewMarkerInfo.isHidden = false
            }
            
        }else{
            if(!self.viewMarkerInfo.isHidden){
                self.bottomViewConstraint.constant -= heightOfMarkerView
                self.viewMarkerInfo.isHidden = true
            }
        }
    }
    
    //MARK:- MapView Delagates
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        LoadingHandler.shared.startloading(interactionEnable: false, view: self.view)
        selectedMarker = marker
       // if(marker != markerCurrentSelected){// && CURRENET_POI_SELECTED >= 0){
            var tappedPOI:GMSMarker?
            viewStarRating.rating = 0//-default set to zero & Initially UserInteraction is disabled
            markerCurrentSelected.map = nil
            selectedPOIIndex = -1
        let allRouteMArkers = dataManager.getAllPoiMarkers()
        let allSelectedMarkers = dataManager.getAddedStopMarkers()
            // get the selected markers index in route poi
            // check whther it is already selected or not and show card with add or remove button
            
            //check tapped marker is present in add route pois
            
            for poi in allRouteMArkers {
                if poi.position.latitude == marker.position.latitude
                    && poi.position.longitude == marker.position.longitude {
                    poiAlreadySelected = false
                    selectedPOIIndex =  allRouteMArkers.index(where: {$0.position.latitude == marker.position.latitude && $0.position.longitude == marker.position.longitude})!
                    tappedPOI = allRouteMArkers[selectedPOIIndex]
                    self.showPOICard(poi: tappedPOI!, isSelected: false)
                }
            }
            
            for poi in allSelectedMarkers {
                if poi.position.latitude == marker.position.latitude
                    && poi.position.longitude == marker.position.longitude {
                    poiAlreadySelected = false
                    selectedPOIIndex =  allSelectedMarkers.index(where: {$0.position.latitude == marker.position.latitude && $0.position.longitude == marker.position.longitude})!
                    tappedPOI = allSelectedMarkers[selectedPOIIndex]
                    self.showPOICard(poi: tappedPOI!, isSelected: true)
                }
            }
            
        //}
        return true
    }
    
    
    func showPOICard(poi:GMSMarker, isSelected: Bool)  {
        //
        
        guard let poiInfo = poi.userData as? NSMutableDictionary else { return }
        guard let poiData = poiInfo[StringConstant.POI_INFO] as? POIData else { return }
        
        
        if isSelected{
            btnRemove.isHidden = false
            widthConstraintBtnADD.constant = 15
            btnAdd.isHidden = true
        }else{
            btnAdd.isHidden = false
            widthConstraintBtnADD.constant = 69
            btnRemove.isHidden = true
        }
        
        lblMarkerInfoTitle.text = poiData.name
        imagviewBusinessHours.isHidden = true
        lblOpenNow.text = ""
        imgViewContact.isHidden = true
        lblBuissnessPhoneNumber.text = ""
        viewStarRating.isHidden = true
        imgViewNoRating.isHidden = true
        lblNoRatings.isHidden = true
        
        if let data = poiData.poiInfo {
            selectedPOI = data
            planRideViewModel?.getPOIDetails(poiID: data.poiListResponse.poiId!, request: "GetPOIDetails", successCallback: { (data) in
                self.savePOIDetails(data: data)
            }) { (error) in
                self.failure(data: error)
            }
        }
       // selectedPOI = markerInfo[StringConstant.POI_WHOLE_DATA] as? POIList
        self.showMarkerInfoView(showView: true)
        markerCurrentSelected.icon = GMSMarker.markerImage(with: UIColor.red)
        markerCurrentSelected.userData = poi.userData
        markerCurrentSelected.map = self.mapView
        markerCurrentSelected.position =  poiData.location.coordinate
        
    }
    
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        if let mrkrData: Int = marker.userData as? Int {
            if mrkrData == 0  || mrkrData == 1  {
                if let customInfoWindow = Bundle.main.loadNibNamed("MarkerCustomView", owner: self, options: nil)?.first as? MarkerCustomView {
                    customInfoWindow.locationLbl.text = self.distance! + " km"
                    return customInfoWindow
                }
            }
        }
        return nil
        
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        return nil
    }
    //MARK:- Draw Ride Route
    
    func loadPolyline(route:RouteList)
    {
        let routeObj:RouteList = route
        
        let legs:[Legs] = routeObj.legs!
        var totalDistanceInMeters: Int = 0
        
        if legs.count > 0{
            for leg in legs{
                totalDistanceInMeters += (leg.distance?.value)!
                let steps:[Steps] = leg.steps!
                if steps.count > 0{
                    for step in steps{
                        let polylinePoint:StepPolyline = step.polylinePoints!
                        if let point = polylinePoint.points{
                            let path: GMSPath = GMSPath(fromEncodedPath: point)!
                            let routePolyline = GMSPolyline(path: path)
                            routePolyline.spans = [GMSStyleSpan(color:AppTheme.THEME_ORANGE)]
                            routePolyline.strokeWidth = 3.0
                            routePolyline.map = self.mapView
                            routePolyline.geodesic = true
                        }
                    }
                }
            }
        }
        
        let doubleMeter:Double = Double(totalDistanceInMeters)
        let distanceInKilometers: Int = Int(doubleMeter / 1000)
        
        self.distance = String(distanceInKilometers)
        
        var json:[String: Any] = [:]
        json[StringConstant.VIA_POINTS_DETAILS] = dataManager.viaPointsArray
        sourceLocation = CLLocation(latitude: CLLocationDegrees((routeObj.legs?.first?.startLocation?.lat)!), longitude: CLLocationDegrees((routeObj.legs?.first?.startLocation?.lng)!))
        bounds = mapView.plotPolyLineWithPath(firstRideLeg: (routeObj.legs?.first)!, lastRideLeg: (routeObj.legs?.last)!, totalLegs: routeObj.legs!, poiArray: dataManager.viaPointsArray, jsonObject: dataManager.routeData, bounds: bounds)
    }
    
    func selectPOIofType(POI_TYPE:NSInteger) {
        
        let poiMarker = dataManager.getAllPoiMarkers()[selectedPOIIndex] //arrMarkersUnSelectedPOIs[selectedPOIIndex]
        
        if let index = self.arrMarkersUnSelectedPOIs.index(where: {$0.position.latitude == selectedMarker.position.latitude && $0.position.longitude == selectedMarker.position.longitude}) {
            arrMarkersUnSelectedPOIs.remove(at: index)
        }
        let paramToSend = [StringConstant.biker_id_recommendation_id: "\(MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID)) \(RecommendedRideVC.recommendedObj?.card_id ?? "")"]
        switch POI_TYPE {
        case POI_TYPE_FUELPUMPS:
            let poi = dataManager.arrRoutePOIs[selectedPOIIndex]
            poi.poiListResponse.types = [StringConstant.PLACES_OF_INTEREST]
            poi.poiListResponse.cllLocation = CLLocation(latitude: CLLocationDegrees((poi.poiListResponse.location?.latitude ?? 0)), longitude: CLLocationDegrees((poi.poiListResponse.location?.longitude ?? 0)))
            self.selectedMarker.icon = UIImage.scaleTo(image: UIImage(named: "Petrol Small Selected")!, w: 25, h: 25)
            Analytics.logCustomEvent(withName: "add_fuel_tap", customAttributes: paramToSend)
            dataManager.addPOI(poi: poi, poiType: .Fuel)
            poiMarker.accessibilityLabel = "mrk_selected_fuelstation"
            
        case POI_TYPE_HOTELS:
            let poi = dataManager.arrRoutePOIs[selectedPOIIndex]
            poi.poiListResponse.types = [StringConstant.PLACES_OF_INTEREST]
            poi.poiListResponse.cllLocation = CLLocation(latitude: CLLocationDegrees((poi.poiListResponse.location?.latitude ?? 0)), longitude: CLLocationDegrees((poi.poiListResponse.location?.longitude ?? 0)))
            self.selectedMarker.icon = UIImage.scaleTo(image: UIImage(named: "Hotel Small Selected")!, w: 25, h: 25)
            Analytics.logCustomEvent(withName: "add_hotels_tap", customAttributes: paramToSend)
            dataManager.addPOI(poi: poi,poiType: .Hotel)
          //  arrSelectedHotels.append(poi)
            poiMarker.icon = #imageLiteral(resourceName: "Hotel Small Selected")
            poiMarker.accessibilityLabel = "mrk_selected_hotels"
        case POI_TYPE_RESTAURANTS:
            let poi = dataManager.arrRoutePOIs[selectedPOIIndex]
            poi.poiListResponse.types = [StringConstant.PLACES_OF_INTEREST]
            poi.poiListResponse.cllLocation = CLLocation(latitude: CLLocationDegrees((poi.poiListResponse.location?.latitude ?? 0)), longitude: CLLocationDegrees((poi.poiListResponse.location?.longitude ?? 0)))
            dataManager.addPOI(poi: poi, poiType: .Restaurent)
            Analytics.logCustomEvent(withName: "add_restaurants_tap", customAttributes: paramToSend)
            self.selectedMarker.icon = UIImage.scaleTo(image: UIImage(named: "Resturant Small Selected")!, w: 25, h: 25)
            poiMarker.accessibilityLabel = "mrk_selected_restaurants"
        default:
            break
        }
        
        dataManager.arrRoutePOIs.remove(at: selectedPOIIndex)
        dataManager.selectedStopArray.append(poiMarker)
        markerCurrentSelected.map = nil
        self.view.hideToastActivity()
    }
    
    
    func deSelectPOIofType(POI_TYPE:NSInteger) {
        
        let poiMarker = dataManager.getAddedStopMarkers()[selectedPOIIndex]//arrMarkersSelectedPOIs[selectedPOIIndex]
        dataManager.removeViaPoint(at: selectedPOIIndex)
        let x:NSMutableDictionary = poiMarker.userData as! NSMutableDictionary
        let poiData:POIData = x[StringConstant.POI_INFO] as! POIData
        
        arrMarkersUnSelectedPOIs.append(selectedMarker)
        
        let type = x[StringConstant.POI_TYPE] as! POIType
        
        if type == .ViaPoint {
            self.selectedMarker.map = nil
        }else{
            dataManager.arrRoutePOIs.append(poiData.poiInfo!)
        }
        
        self.selectedMarker.icon = UIImage.scaleTo(image: #imageLiteral(resourceName: "Hotel Small"), w: imageSize, h: imageSize)
        
        switch POI_TYPE {
        case POI_TYPE_FUELPUMPS:
            if self.selectedPOi == .Fuel {
                self.selectedMarker.icon  = UIImage.scaleTo(image: #imageLiteral(resourceName: "Petrol Small"), w: imageSize, h: imageSize)
            } else {
                self.selectedMarker.map = nil
                
            }
            
        case POI_TYPE_HOTELS:
            if self.selectedPOi == .Hotel {
                self.selectedMarker.icon  = UIImage.scaleTo(image: #imageLiteral(resourceName: "Hotel Small"), w: imageSize, h: imageSize)
            } else {
                self.selectedMarker.map = nil
                
            }
            
        case POI_TYPE_RESTAURANTS:
            if self.selectedPOi == .Restaurent {
                self.selectedMarker.icon  = UIImage.scaleTo(image: #imageLiteral(resourceName: "Restaurants Small"), w: imageSize, h: imageSize)
            } else {
                self.selectedMarker.map = nil
                
            }
        default:break
        }
        
        markerCurrentSelected.map = nil
        self.view.hideToastActivity()
        //        }
    }
    
    
    //MARK:- Action Methods
    @IBAction func btnClosePreviewAction(_ sender: UIButton) {
        CURRENET_POI_SELECTED = PREV_POI_SELECTED
    //    setPreviewMode(setMode: false)
        
        if let cell = collectionViewPOI.cellForItem(at: IndexPath(row: 3, section: 0)) {
            let planRideCell = cell as! PlanRideInDetailCell
            planRideCell.btnSelectPOI.setBackgroundImage(#imageLiteral(resourceName: "TickMark Grey"), for: .normal)
            
        }
        
    }
    
    @IBAction func closeMarkerInfo(_ sender: Any) {
        markerCurrentSelected.map = nil
        self.showMarkerInfoView(showView: false)
    }
    

    @IBAction func btnAddPOIAction(_ sender: UIButton) {
        if dataManager.viaPointsArray.count >= 21 {
            self.view.makeToast(NSLocalizedString("keyMaxStopCountReached", comment: ""))
            self.showMarkerInfoView(showView: false)
            return
        }
        if(poiAlreadySelected){
            self.view.makeToast("POI Already Selected")
        }
        else{
            if(selectedPOIIndex >= 0) {
                self.view.makeToastActivity(.center, isUserInteraction: false)
                self.showMarkerInfoView(showView: false)
                if let markerInfo = markerCurrentSelected.userData as? NSMutableDictionary {
                    if let poiType = markerInfo[StringConstant.POI_TYPE] as? POIType {
                        self.selectPOIofType(POI_TYPE: poiType.rawValue)
                        isMarkerViewOpen = true
                    }
                }
            }
        }
    }
    
    @IBAction func btnRemovePOIAction(_ sender: UIButton) {
     //   if(poiAlreadySelected){
            if(selectedPOIIndex >= 0){
                self.view.makeToastActivity(.center, isUserInteraction: false)
                self.showMarkerInfoView(showView: false)
                if let markerInfo = markerCurrentSelected.userData as? NSMutableDictionary {
                    if  let poiType = markerInfo[StringConstant.POI_TYPE] as? POIType {
                        self.deSelectPOIofType(POI_TYPE: poiType.rawValue)
                        isMarkerViewOpen = false
                    }
                }
            }
    }
    
    func showMarkerOnMap(marker: [GMSMarker], bounds: GMSCoordinateBounds, poiType: NSInteger, poiList:[POIList]) {
        
        for poimarker in marker {
            poimarker.map = mapView
        }
        
        if poiType != -4 {
            arrMarkersUnSelectedPOIs = marker
            self.bounds = bounds
        }
    }
}

extension PlanRideInDetail {
    
    func savePoiWithPolyline(data: Any) {
        if let dict = data as? [String: Any], dict["RequestType"] as? String == "GetPOIWithPolyline", let poiData = dict["data"] as? GetRidePOIModel {
            self.getPOIWithPolyline(data: poiData)
        }
    }
    func savePOIDetails(data: Any) {
        if let dict = data as? [String: Any], dict["RequestType"] as? String == "GetPOIDetails", let poiData = dict["data"] as? GogglePOIDetailsModel {
            self.poiDetailsSuccess(data: poiData)
        }
    }
    
    func savePlannedWaypoints(data: Any) {
//        if let dict = data as? [String: Any], dict["RequestType"] as? String == "GetPlannedWavepoints", let poiData = dict["data"] as? PlannedWavePointsModel {
//            //self.plannedWavepointsSuccess(data: poiData)
//        }
    }
    
    
    func getPOIWithPolyline(data: GetRidePOIModel) {
        arrUnselectedFuelPumps.removeAll()
        arrUnselectedRestaurants.removeAll()
        arrUnselectedHotels.removeAll()
//        collectionViewPOI.arrUnselectedHotels.removeAll()
//        collectionViewPOI.arrUnselectedRestaurants.removeAll()
//        collectionViewPOI.arrUnselectedFuelPumps.removeAll()
        
        dataManager.saveAllPOIs(ridePoiModel: data)
        self.collectionViewPOI.selectedPOI = selectedPoiType
        self.collectionViewPOI.reloadData()
        self.didSelectPOI(poiType: POIButtonType(rawValue: selectedPoiType) ?? .Hotel)
        if let rideID = createdRideId, rideID != 0 {
            planRideViewModel?.getPlannedWavepoints(rideID: rideID, request: "GetPlannedWavepoints", successCallback: { (data) in
                self.savePlannedWaypoints(data: data)
            }) { (error) in
                self.failure(data: error)
            }
        } else {
            collectionViewPOI.isUserInteractionEnabled =  true
       //     self.loadPolyline(route: dataManager.routeObj!)
            LoadingHandler.shared.stoploading()
        }
    }
    
    func poiDetailsSuccess(data: GogglePOIDetailsModel) {
        LoadingHandler.shared.stoploading()
        viewStarRating.rating = 0//-default set to zero & Initially UserInteraction is disabled
        
        lblMarkerInfoTitle.text = data.result?.name
        imgViewContact.isHidden = false
        imagviewBusinessHours.isHidden = false
        
        
        if let buissnessPhoneNumber = data.result?.formatted_phone_number{
            lblBuissnessPhoneNumber.text = buissnessPhoneNumber
        }else{
            lblBuissnessPhoneNumber.text = "Contact Details not available"
        }
        
        if let rating = data.result?.rating {
            viewStarRating.isHidden = false
            lblNoRatings.isHidden = true
            imgViewNoRating.isHidden = true
            viewStarRating.rating = rating//set exact Rating
        }else{
            viewStarRating.isHidden = true
            lblNoRatings.isHidden = false
            imgViewNoRating.isHidden = false
        }
        
        if let openHours = data.result?.opening_hours?.weekday_text {
            let today:String = Date().dayOfWeek()!
            if(openHours.count > 0){
                for day in openHours{
                    if day.contains(today){
                        lblOpenNow.text = day
                        break
                    }
                }
            }
        }
        
        if lblOpenNow.text == "" {
            if let result = data.result{
                lblOpenNow.text = result.formatted_address ?? ""
            }
            imagviewBusinessHours.image = #imageLiteral(resourceName: "Address POI Business Card")
        }else{
            imagviewBusinessHours.image = #imageLiteral(resourceName: "Business Hours")
        }
        lblOpenNow.fitHeight()
    }
    
    func plannedWavepointsSuccess(data: PlannedWavePointsModel) {
        var arrWavePoints: [WavePointsList] = []
        if let wavePointList = data.WavePointsList{
            if (wavePointList.count) > 0 {
                for wavePoint in wavePointList{
                    arrWavePoints.append(wavePoint)
                }
            }
        }
        for obj in arrWavePoints {
            let poiObj = POIListResponse()
            poiObj.cllLocation = CLLocation(latitude: (obj.location?.latitude)!, longitude: (obj.location?.longitude)!)
            poiObj.location = obj.location
            poiObj.name = obj.name
            poiObj.poiId = obj.poi_id
            poiObj.categories = obj.categories
            poiObj.types = obj.types
            if let categories = obj.categories{
                if categories.count > 0{
                    if categories.contains(StringConstant.POI_CATEGORY_FUEL_PUMPS){
                        let poiListObj:POIList = POIList(position: CLLocationCoordinate2D(latitude: (poiObj.location?.latitude)!, longitude: (poiObj.location?.longitude)!), response: poiObj)
                        arrSelectedFuelPumps.append(poiListObj)
                        collectionViewPOI.arrSelectedFuelPumps.append(poiListObj)
                    }
                    if categories.contains(StringConstant.POI_CATEGORY_RESTAURANTS){
                        let poiListObj:POIList = POIList(position: CLLocationCoordinate2D(latitude: (poiObj.location?.latitude)!, longitude: (poiObj.location?.longitude)!), response: poiObj)
                        arrSelectedRestaurants.append(poiListObj)
                        collectionViewPOI.arrSelectedRestaurants.append(poiListObj)
                    }
                    if categories.contains(StringConstant.POI_CATEGORY_LODGING){
                        let poiListObj:POIList = POIList(position: CLLocationCoordinate2D(latitude: (poiObj.location?.latitude)!, longitude: (poiObj.location?.longitude)!), response: poiObj)
                        arrSelectedHotels.append(poiListObj)
                        collectionViewPOI.arrSelectedHotels.append(poiListObj)
                    }
                }
            }
            let poiListObj:POIList = POIList(position: CLLocationCoordinate2D(latitude: (poiObj.location?.latitude)!, longitude: (poiObj.location?.longitude)!), response: poiObj)
            arrPreviousSelectedPOIS.append(poiListObj)
          //  collectionViewPOI.arrPreviousSelectedPOIS.append(poiListObj)
        }
        
        if self.arrPreviousSelectedPOIS.count > 0  {
            DispatchQueue.main.async {
                self.plotSelectedPOI()
            }
        }
        
        collectionViewPOI.isUserInteractionEnabled =  true
        self.loadPolyline(route: routeDetails!)
        LoadingHandler.shared.stoploading()
    }
    
    func failure(data: [String : Any]) {
        self.view.hideToastActivity()
        LoadingHandler.shared.stoploading()
        if let errModel:ErrorModel = data["data"] as? ErrorModel {
            print(errModel.message ?? " ")
            self.view.makeToast((errModel.description)!)
        } else {
            if let descriptionString = data["description"] as? String {
                self.view.makeToast(descriptionString)
            }
        }
    }
    // MARK: - GMUClusterManagerDelegate
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,
                                                 zoom: mapView.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        DispatchQueue.main.async {
            self.mapView.moveCamera(update)
        }
        return false
    }
}

extension PlanRideInDetail:POICollectionDelegates {
    
    func didSelectPOI(poiType: POIButtonType) {
       // self.mapView.clear()
        self.selectedPOi = POIType(rawValue: poiType.rawValue)!
        
        for marker in arrMarkersUnSelectedPOIs {
            marker.map = nil
        }
        let poiFuel:[GMSMarker] = dataManager.getPOIMarkerArray(ofType: POIType(rawValue: poiType.rawValue)!)
        arrMarkersUnSelectedPOIs = poiFuel
        
        //show selected category pois
        for poi in poiFuel {
            poi.map = self.mapView
            switch poiType{
                case POIButtonType.FuelStation:
                    poi.accessibilityLabel = "mrk_addstops_fuelstation"
                case POIButtonType.Hotel:
                    poi.accessibilityLabel = "mrk_addstops_hotels"
                case POIButtonType.Restaurent:
                    poi.accessibilityLabel = "mrk_addstops_restaurants"
               default:break
            }
        }

    }

}

