//
//  MarkerCustomView.swift
//  Maximus
//
//  Created by kpit on 06/11/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class MarkerCustomView: UIView {
    @IBOutlet weak var locationLbl: UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    class func instanceFromNib() -> MarkerCustomView {
        return (Bundle.main.loadNibNamed("MarkerCustomView", owner: self, options: nil)?.first as? MarkerCustomView)!
       // return UINib(nibName: "PopOver", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PopOver
    }
}
