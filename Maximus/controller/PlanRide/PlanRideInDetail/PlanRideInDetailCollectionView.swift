//
//  PlanRideInDetailCollectionView.swift
//  Maximus
//
//  Created by Isha Ramdasi on 10/07/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import GoogleMaps
import ObjectMapper
import GooglePlaces
import CoreLocation

class POIList: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var poiListResponse: POIListResponse
    init(position: CLLocationCoordinate2D, response: POIListResponse) {
        self.position = position
        self.poiListResponse = response
    }
    
}

protocol UpdateMapView: class {
    func updateMarkerView(isMarkerViewHidden: Bool, showMarkerInfo: Bool)
    func showMarkerOnMap(marker: [GMSMarker], bounds: GMSCoordinateBounds, poiType: NSInteger, poiList:[POIList])
    func hideBottomPreview(toShow: Bool)
    func selectedPOIType(currentSelectedPOI: Int, previousSelectedPOI: Int)
}

enum POIButtonType:Int
{
    case FuelStation = 0
    case Restaurent
    case Hotel
    
}

protocol POICollectionDelegates: class {
    func didSelectPOI(poiType :POIButtonType)
}

fileprivate let sectionInsets = UIEdgeInsets(top: 0.0, left: 10.0, bottom: 0.0, right: 10.0)

class PlanRideInDetailCollectionView: UICollectionView, UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout  {
    
    var arrSelectedFuelPumps:[POIList] = []
    var arrSelectedRestaurants:[POIList] = []
    var arrSelectedHotels:[POIList] = []
    
    
    let POI_TYPE_FUELPUMPS = 0
    let POI_TYPE_RESTAURANTS = 1
    let POI_TYPE_HOTELS = 2
    var CURRENET_POI_SELECTED = -1
    var PREV_POI_SELECTED = -1
    
    var arrMarkersSelectedPOIs = [GMSMarker]()
    var arrMarkersUnSelectedPOIs = [GMSMarker]()
    var markerCurrentSelected = GMSMarker()
    weak var updateViewDelegate: UpdateMapView?
    weak var poiDelegate: POICollectionDelegates?
    let cellIdentifier = "PlanRideInDetailCell"
    var mapBounds = GMSCoordinateBounds()
    var selectedPOI = 4

    override func awakeFromNib() {
        super.awakeFromNib()
        self.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        self.dataSource = self
        self.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(getArrOfSelectedPOIS(_:)), name: NSNotification.Name(rawValue: "SelectedPOIS"), object: nil)
    }
    
    //MARK:- CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        markerCurrentSelected.map = nil
        self.updateViewDelegate?.updateMarkerView(isMarkerViewHidden: true, showMarkerInfo: false)
        
        var changeImage:UIImage?
        
        if let cell = collectionView.cellForItem(at: indexPath) {
            let planRideCell = cell as! PlanRideInDetailCell
            let paramToSend = [StringConstant.biker_id_recommendation_id: "\(MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID)) \(RecommendedRideVC.recommendedObj?.card_id ?? "")"]
            if(indexPath.row == 0) {//Fuel Pump Selected
                Analytics.logCustomEvent(withName: "search_fuel_tap", customAttributes: paramToSend)
                self.poiDelegate?.didSelectPOI(poiType: .FuelStation)
//                CURRENET_POI_SELECTED = POI_TYPE_FUELPUMPS
//                PREV_POI_SELECTED = CURRENET_POI_SELECTED
                self.checkAndResetPOISelectorOfType(selectorType: POI_TYPE_RESTAURANTS)
                self.checkAndResetPOISelectorOfType(selectorType: POI_TYPE_HOTELS)
                
                if planRideCell.btnSelectPOI.currentBackgroundImage == #imageLiteral(resourceName: "Petrol Pump Grey") {
                    //Selected petrol pump
                    changeImage = #imageLiteral(resourceName: "Petrol Pump Selected")
                  //  self.clearUnSelectedPOIMarkers()
                   // self.plotPOIOfType(POIType: POI_TYPE_FUELPUMPS)
                }else{
                    //Selected Petrol pump when already in selected mode
                    if(arrSelectedFuelPumps.count>0){
                        //If already selection is because some poi are selected than clear other pois & re-plot unselected petrol pumps
                      //  self.clearUnSelectedPOIMarkers()
                        //self.plotPOIOfType(POIType: POI_TYPE_FUELPUMPS)
                    }
                }
            }else if(indexPath.row == 1) {//Restaurant Selected
                Analytics.logCustomEvent(withName: "search_restaurants_tap", customAttributes: paramToSend)
                self.poiDelegate?.didSelectPOI(poiType: .Restaurent)
//                CURRENET_POI_SELECTED = POI_TYPE_RESTAURANTS
//                PREV_POI_SELECTED = CURRENET_POI_SELECTED
                self.checkAndResetPOISelectorOfType(selectorType: POI_TYPE_FUELPUMPS)
                self.checkAndResetPOISelectorOfType(selectorType: POI_TYPE_HOTELS)
                
                if planRideCell.btnSelectPOI.currentBackgroundImage == #imageLiteral(resourceName: "Restaurant Grey"){
                    changeImage = #imageLiteral(resourceName: "Restaurant Selected")
//                    self.clearUnSelectedPOIMarkers()
//                    self.plotPOIOfType(POIType: POI_TYPE_RESTAURANTS)
                }else{

//                    if(arrSelectedRestaurants.count>0){
//                        self.clearUnSelectedPOIMarkers()
//                        self.plotPOIOfType(POIType: POI_TYPE_RESTAURANTS)
//                    }
                }
            }else if(indexPath.row == 2) {//Hotel Selected
                Analytics.logCustomEvent(withName: "search_hotels_tap", customAttributes: paramToSend)
                self.poiDelegate?.didSelectPOI(poiType: .Hotel)
                CURRENET_POI_SELECTED = POI_TYPE_HOTELS
                PREV_POI_SELECTED = CURRENET_POI_SELECTED
                self.checkAndResetPOISelectorOfType(selectorType: POI_TYPE_FUELPUMPS)
                self.checkAndResetPOISelectorOfType(selectorType: POI_TYPE_RESTAURANTS)
                
                if planRideCell.btnSelectPOI.currentBackgroundImage == #imageLiteral(resourceName: "Hotels Grey"){
                    changeImage = #imageLiteral(resourceName: "Hotel Selected")
                 //   self.clearUnSelectedPOIMarkers()
                    //self.plotPOIOfType(POIType: POI_TYPE_HOTELS)
                }else{
                    if(arrSelectedHotels.count>0){
//                        self.clearUnSelectedPOIMarkers()
//                        self.plotPOIOfType(POIType: POI_TYPE_HOTELS)
                    }
                }
            }
            if let newImage = changeImage{
                planRideCell.btnSelectPOI.setBackgroundImage(newImage, for: .normal)
            }
        }
        self.updateViewDelegate?.selectedPOIType(currentSelectedPOI: CURRENET_POI_SELECTED, previousSelectedPOI: PREV_POI_SELECTED)
    }
    
    
    
    @available(iOS 6.0, *)
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as!
        PlanRideInDetailCell
        collectionView.accessibilityIdentifier = "cltn_planride_addpois"
        if(indexPath.row == 0){
            cell.btnSelectPOI.setBackgroundImage(#imageLiteral(resourceName: "Petrol Pump Grey"), for: .normal)
            if indexPath.row == selectedPOI {
               cell.btnSelectPOI.setBackgroundImage( #imageLiteral(resourceName: "Petrol Pump Selected"), for: .normal)
            }
            cell.btnSelectPOI.accessibilityIdentifier = "btn_fuelstation"
        }else if(indexPath.row == 1){
            cell.btnSelectPOI.setBackgroundImage(#imageLiteral(resourceName: "Restaurant Grey"), for: .normal)
            if  indexPath.row == selectedPOI {
               cell.btnSelectPOI.setBackgroundImage( #imageLiteral(resourceName: "Restaurant Selected"), for: .normal)
            }
            cell.btnSelectPOI.accessibilityIdentifier = "btn_restaurants"
        }else if(indexPath.row == 2){
            cell.btnSelectPOI.setBackgroundImage(#imageLiteral(resourceName: "Hotels Grey"), for: .normal)
            if indexPath.row == selectedPOI {
               cell.btnSelectPOI.setBackgroundImage( #imageLiteral(resourceName: "Hotel Selected"), for: .normal)
            }
            cell.btnSelectPOI.accessibilityIdentifier = "btn_hotels"
        }
    
        cell.btnSelectPOI.setCornerRadius(radius: 22)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 3
    }
    
    //MARK:- CollectionView FlowLayout Delegates
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //2
        return CGSize(width: 48, height: 48)
        
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    // 4
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }

}

extension PlanRideInDetailCollectionView {
    
    
    func checkAndResetPOISelectorOfType(selectorType:Int){
        switch selectorType {
        case POI_TYPE_FUELPUMPS:
            if(arrSelectedFuelPumps.count == 0){
                if let cell = self.cellForItem(at: IndexPath(row: 0, section: 0)) {
                    let planRideCell = cell as! PlanRideInDetailCell
                    planRideCell.btnSelectPOI.setBackgroundImage(#imageLiteral(resourceName: "Petrol Pump Grey"), for: .normal)
                }
            }
        case POI_TYPE_RESTAURANTS:
            if(arrSelectedRestaurants.count == 0){
                if let cell = self.cellForItem(at: IndexPath(row: 1, section: 0)) {
                    let planRideCell = cell as! PlanRideInDetailCell
                    planRideCell.btnSelectPOI.setBackgroundImage(#imageLiteral(resourceName: "Restaurant Grey"), for: .normal)
                }
            }
        case POI_TYPE_HOTELS:
            if(arrSelectedHotels.count == 0){
                if let cell = self.cellForItem(at: IndexPath(row: 2, section: 0)) {
                    let planRideCell = cell as! PlanRideInDetailCell
                    planRideCell.btnSelectPOI.setBackgroundImage(#imageLiteral(resourceName: "Hotels Grey"), for: .normal)
                }
            }
        default: break
        }
    }
    
    func getArrOfSelectedPOIS(_ notification: Notification) {
        if let data = notification.userInfo as? [String: Any] {
            arrMarkersSelectedPOIs = (data["data"] as? [GMSMarker])!
        }
    }
}
