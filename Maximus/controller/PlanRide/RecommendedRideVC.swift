//
//  RecommendedRideVC.swift
//  Maximus
//
//  Created by kpit on 12/04/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import ObjectMapper

class RecommendedRideVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let cellReuseIdentifier = TableViewCells.MyRideCell
    var arrOfRides = [RecommendedResponse]()
    var shareRideTitle = ""
    let analyticsRepo = AnalyticsRepository()
    var analyticsViewModel:AnalyticsViewModel?
    var pageNo = 0
    let size = 20
    var isLastPage = false
    static var recommendedObj:RecommendedResponse?
    @IBOutlet weak var noRecommendationView: UIView!
    
    @IBOutlet weak var noInternetView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        analyticsViewModel = AnalyticsViewModel()
        tableView.register(UINib(nibName: cellReuseIdentifier, bundle: nil), forCellReuseIdentifier:cellReuseIdentifier)
        self.navigationItem.leftBarButtonItem?.tintColor = .white

        self.tableView.backgroundColor = .white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.topViewController?.navigationItem.title = NSLocalizedString("keyRideOn", comment: "")
        if self.tableView.numberOfRows(inSection: 0) == 0{
            self.view.makeToastActivity(.center, isUserInteraction: true)
            analyticsViewModel?.getRecommendedRides(bikerID:MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID) , requestType: URLRequestType.GetRecommendedRides, pageNo: pageNo, size: size, successCallback: { (data) in
                self.tableView.isHidden = false
                self.analyticsViewModelSuccessCallback(data: data)
            }, failureCallback: { (errorData) in
                self.tableView.isHidden = true
                self.analyticsViewModelFailureCallback(data: errorData)
            })
        }
    }
    
    
}


extension RecommendedRideVC {
    /*
     Set background view to no data available when no response from recommendation API
     */
    func settableBackgroundView() {
        self.noRecommendationView.isHidden = false
    }
}
//MARK:- Tableview Delegate & DataSource Methods
extension RecommendedRideVC : UITableViewDelegate ,UITableViewDataSource {
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrOfRides.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:MyRideCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! MyRideCell
        if (arrOfRides.count - 2 == indexPath.row) {
            pageNo = pageNo + 1
            if NetworkCheck.isNetwork(), !isLastPage {
                analyticsViewModel?.getRecommendedRides(bikerID:MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID) , requestType: URLRequestType.GetRecommendedRides, pageNo: pageNo, size: size, successCallback: { (data) in
                    self.analyticsViewModelSuccessCallback(data: data)
                }, failureCallback: { (errorData) in
                    self.analyticsViewModelFailureCallback(data: errorData)
                })
            }
        }
        cell.imgViewRide.tag = indexPath.row
        cell.recommendedObj = arrOfRides[indexPath.row]
        print("***\(indexPath.row)")
        return cell
    }
    
    
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        PlanRideTableViewController.sourceAddressSelected = true
        if NetworkCheck.isNetwork() {
            let objRidedata = arrOfRides[indexPath.row]
            for i in 0..<(objRidedata.card?.actions?.count)! {
                if let actionType = objRidedata.card?.actions![i].action_type, actionType == StringConstant.recommendedActionTypePlanRide {
                    var paramToSend = [StringConstant.biker_id_recommendation_id: ((objRidedata.card?.biker_id)?.toString())! + " " + objRidedata.card_id! as! NSString] as [String : Any]
                    Analytics.logCustomEvent(withName: "recommendation_card_tap", customAttributes: paramToSend)
                    paramToSend = [((objRidedata.card?.biker_id)?.toString()): objRidedata.card?.item_type! as! NSString] as! [String : Any]
                    Analytics.logCustomEvent(withName: "recommendation_card_tap", customAttributes: paramToSend)
                    paramToSend = [objRidedata.card?.item_type: ((objRidedata.card?.biker_id)?.toString())] as! [String : Any]
                    Analytics.logCustomEvent(withName: "recommendation_card_tap", customAttributes: paramToSend)
                    showPlanaRideFlow(rideData: (objRidedata))
                }
            }
        } else {
            self.view.makeToast(ToastMsgConstant.network)
        }
        
    }
    
    /*
     Show Plan a ride flow when tapped on recommendation card of type "plan_a_ride"
     @rideData: Actions
     */
    func showPlanaRideFlow(rideData: RecommendedResponse) {
        let planRideVC = PlanRide()
        let dataManager = PlanRideDataManager.shared()
        
        if let wayPointArray = rideData.card?.actions?.first?.action_data?.via_points {
            for i in 0..<wayPointArray.count where i<21 {
                let dict = wayPointArray[i] as! ViaPoint
      
                let poiListResp = POIListResponse()
                poiListResp.name = dict.full_address
                poiListResp.cllLocation = CLLocation(latitude: dict.lat!, longitude: dict.lng!)
                
                poiListResp.categories = [StringConstant.VIA_POINT]
                let poiList = POIList(position: CLLocationCoordinate2D(latitude: dict.lat!, longitude: dict.lng!), response: poiListResp)
                
              dataManager.addPOI(poi: poiList, poiType: .ViaPoint)
            }
        }
        
        dataManager.routeData[PlanRideDataSource.ViaPoint] = dataManager.viaPointsArray
//        planRideVC.autocompleteSearchitems[PlanRideDataSource.ViaPoint] = viaPointArray
        var startObj: [String:Any] = [:]
        startObj[StringConstant.NAME] = rideData.card?.actions?.first?.action_data?.source?.place_name
        var latLong:[String: Any] = [:]
        latLong[StringConstant.LATITUDE] = rideData.card?.actions?.first?.action_data?.source?.lat
        latLong[StringConstant.LONGITUDE] = rideData.card?.actions?.first?.action_data?.source?.lng
        startObj[StringConstant.LatLong] = latLong
        
    //    planRideVC.autocompleteSearchitems[PlanRideDataSource.SourcePlace] = startObj
        dataManager.routeData[PlanRideDataSource.SourcePlace] = startObj
        var destObj: [String:Any] = [:]
        destObj[StringConstant.NAME] = rideData.card?.actions?.first?.action_data?.destination?.place_name
        latLong[StringConstant.LATITUDE] = rideData.card?.actions?.first?.action_data?.destination?.lat
        latLong[StringConstant.LONGITUDE] = rideData.card?.actions?.first?.action_data?.destination?.lng
        destObj[StringConstant.LatLong] = latLong
  //      planRideVC.autocompleteSearchitems[PlanRideDataSource.DestinationPlace] = destObj
        dataManager.routeData[PlanRideDataSource.DestinationPlace] = destObj
        planRideVC.rideID = 0
        planRideVC.planMode = PlanRideFlowOptions(rawValue: PlanRideFlowOptions.RecommendedRide.rawValue)!
        planRideVC.recommendedObj = rideData
        RecommendedRideVC.recommendedObj = rideData
        self.view.hideToastActivity()
        self.pushVC(planRideVC)
    }
}

extension RecommendedRideVC {
    //Successful response from recommendation API
    func analyticsViewModelSuccessCallback(data: Any) {
        self.noRecommendationView.isHidden = true
        self.noInternetView.isHidden = true
        self.view.hideToastActivity()
        if let dict = data as? [String: Any], dict[StringConstant.keyRequestType] as? String == URLRequestType.GetRecommendedRides {
            if let obj:[RecommendedResponse] = dict[StringConstant.data] as? [RecommendedResponse] {
                isLastPage = obj.count == 0 ? true : false
                if obj.count != 0 {
                    arrOfRides.append(contentsOf: obj)
                }
                if (obj.count) > 0 {
                }else if arrOfRides.count <= 0 {
                    self.settableBackgroundView()
                }
                DispatchQueue.main.async{
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    //Failure response from recommendation API
    func analyticsViewModelFailureCallback(data: [String : Any]) {
        self.view.hideToastActivity()
        if let errDescription = data["description"] as? String {
            self.view.makeToast(errDescription)
            if errDescription == ToastMsgConstant.network {
                self.noInternetView.isHidden = false
            }
        } else {
            if let errModel:ErrorModel = data[StringConstant.data] as? ErrorModel{
                print(errModel.message ?? " ")
                self.view.makeToast((errModel.description)!)
                if arrOfRides.count == 0 {
                    self.settableBackgroundView()
                }
            } else if let msg = data[StringConstant.data] as? String {
                if msg == " " {
                    self.noInternetView.isHidden = false
                }
            }
        }
    }
}
