//
//  MyRidesVC.swift
//  Maximus
//
//  Created by kpit on 12/04/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import ObjectMapper
import CoreData
import CoreLocation

class MyRidesVC: UIViewController {
    
    @IBOutlet weak var noRidesView: UIView!
    @IBOutlet weak var tableViewMyRide: UITableView!
    
    let cellReuseIdentifier = "MyRideCell"
    var arrOfRides = [Rides]()
    var rideInProgressCard = [Rides]()
    var shareRideTitle = ""
    let rideObj = RideManager()
    var currentRideID = 0
    var selectedRow: Int?
    var selectedSection: Int?
    var dataSourceArray: [[String: Any]] = [[:]]
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    var page: Int = 0
    let batchSize: Int = 20
    var isRideClone = false
    var isMakeMine = false
    var totalDistanceInMeters: Int = 0
    var totalTime: Int = 0
    let bikerID = MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID)
    let rideRepo = RideRepository()
    var rideViewModel: MyRidesViewModel?
    var presentDayRides = [Rides]()
    var pastDayRides = [Rides]()
    var futureDayRides = [Rides]()
    var selectedRideCard: Rides?
    var cloneRideSuccessData: [String: Any]?
    var isLastPage = false
    let planRideRepo = PlanRideRepository()
    var planRideModel: PlanRideViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.tableViewMyRide.register(UINib(nibName: cellReuseIdentifier, bundle: nil), forCellReuseIdentifier:cellReuseIdentifier)
        self.tableViewMyRide.backgroundColor = .white
        self.automaticallyAdjustsScrollViewInsets = true;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.topViewController?.navigationItem.title = "RIDE ON"
        tableViewMyRide.allowsSelection = true
        rideObj.rideManagerDelegate = self
        //rideViewModel.reloadDataDelegate = self
      //  rideViewModel.getAllRidesOfBiker(request: "GetPlannedRides", pageNo: page, bikerID: bikerID, size: 20)
//        planRideModel.saveRideDelegate = self
        planRideModel = PlanRideViewModel(planRideRepoDelegate: planRideRepo)
        rideViewModel = MyRidesViewModel(rideRepoDelegate: rideRepo)
        self.view.makeToastActivity(.center, isUserInteraction: true)
        rideViewModel?.getAllRidesOfBiker(request: "GetPlannedRides", pageNo: page, bikerID: bikerID, size: 20, successCallback: {responseData in
            self.reloadData(data: responseData)
            self.view.hideToastActivity()
        }, failureCallback:{ failureData in
            self.failure(data: failureData)
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func compareTodaysDateWithRideStartDate(){
        let todaysDays = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let strTodaysDate = dateFormatter.string(from: todaysDays as Date)
        presentDayRides = []
        futureDayRides = []
        pastDayRides = []
        for ride in arrOfRides {
            let rideDate = ride.start_date?.getFormatedDate()
            let strRideDate = dateFormatter.string(from: rideDate!)
            
            let dateRideStartDate = dateFormatter.date(from: strRideDate) ?? Date() as Date
            let dateTodaysDate = dateFormatter.date(from: strTodaysDate) ?? Date() as Date
            if(dateTodaysDate > dateRideStartDate){
                //past
                pastDayRides.append(ride)
                pastDayRides = pastDayRides.sorted(by: {$0.start_date! > $1.start_date!})
            }else if dateTodaysDate == dateRideStartDate {
                //present
                presentDayRides.append(ride)
                presentDayRides = presentDayRides.sorted(by: {$0.start_date! > $1.start_date!})
            } else {
                //future
                futureDayRides.append(ride)
                futureDayRides = futureDayRides.sorted(by: {$0.start_date! > $1.start_date!})
            }
        }
        arrOfRides = []
        arrOfRides = presentDayRides + futureDayRides + pastDayRides
    }
    
    func sortAndSetData(arrayLogs:[Rides]) {
      //  arrLogList.append(contentsOf: arrayLogs)
        var rideIdArray = Set<Int>()
        var unique = [Rides]()
        for message in arrOfRides {
            if let rideID = message.rideID {
                if !rideIdArray.contains(Int(rideID)!) {
                    unique.append(message)
                    rideIdArray.insert(Int(rideID)!)
                }
            }
        }
        arrOfRides = unique.sorted(by: { $0.rideID! > $1.rideID! })
    }
}

extension MyRidesVC: RideManagerDelegate {

    func cloneRideSuccess(data: [String : Any]) {
        self.view.hideToastActivity()
        self.tableViewMyRide.allowsSelection = true
        if let rideData:RideCreateResponse = data["data"] as? RideCreateResponse {
            let alert = UIAlertController.init(title: "", message: "New Ride has been created. Do you want to edit the ride details?", preferredStyle: .alert)
            let yesAction = UIAlertAction.init(title: "Yes", style: .default, handler: { (action) in
                self.view.makeToastActivity(.center, isUserInteraction: false)
                self.currentRideID = rideData.id!
                self.rideViewModel?.editRide(rideID: String(rideData.id!), type: "GetRideDetails", successCallback: {responseData in
                    self.reloadData(data: responseData)
                }, failureCallback:{ failureData in
                    self.failure(data: failureData)
                })
            })
            let noAction = UIAlertAction.init(title: "No", style: .cancel, handler: { (action) in
                self.cloneRideSuccess()
            })
            alert.addAction(yesAction)
            alert.addAction(noAction)
            self.presentVC(alert)
        }
    }
    
    func settableBackgroundView() {
        self.noRidesView.isHidden = false
    }
    
    func onFailure(Error:[String:Any]){
        self.view.hideToastActivity()
        let errModel:ErrorModel = Error["data"] as! ErrorModel
        print(errModel.message ?? " ")
        self.view.makeToast((errModel.description)!)
        if arrOfRides.count == 0 {
            self.settableBackgroundView()
        } else {
            self.noRidesView.isHidden = true
        }
    }
    
    func leaveRideSuccess() {
        selectedRow = -1
        selectedSection = -1
        page = 0
        arrOfRides = Rides.getNotCompletedMyRideObjs(context: context)
        reloadMyRides()
        self.view.hideToastActivity()
        rideViewModel?.getAllRidesOfBiker(request: "GetPlannedRides", pageNo: page, bikerID: bikerID, size: 20, successCallback: {responseData in
            self.reloadData(data: responseData)
        }, failureCallback:{ failureData in
            self.failure(data: failureData)
        })
    }
    
    func cloneRideSuccess() {
        selectedRow = -1
        selectedSection = -1
        page = 0
        rideViewModel?.getAllRidesOfBiker(request: "GetPlannedRides", pageNo: page, bikerID: bikerID, size: 20, successCallback: {responseData in
            self.reloadData(data: responseData)
        }, failureCallback:{ failureData in
            self.failure(data: failureData)
        })
    }
}
//MARK:- Tableview Delegate & DataSource Methods
extension MyRidesVC : UITableViewDelegate ,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if rideInProgressCard.count > 0 {
            return 2
        }
        return 1
    }
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if rideInProgressCard.count > 0 {
            if section == 0 {
                return rideInProgressCard.count
            }
        }
        return self.arrOfRides.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:MyRideCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! MyRideCell
        cell.delegate = self
        if indexPath.section == 0 && rideInProgressCard.count > 0 {
            cell.objRideData = rideInProgressCard[indexPath.row]
            cell.btnJoinedRiders.tag = indexPath.row
        } else {
            cell.objRideData = arrOfRides[indexPath.row]
            cell.btnJoinedRiders.tag = indexPath.row
        }
        if (arrOfRides.count - 2 == indexPath.row) {
            page = page + 1
            if NetworkCheck.isNetwork(), !isLastPage {
                rideViewModel?.getAllRidesOfBiker(request: "GetPlannedRides", pageNo: page, bikerID: bikerID, size: 20, successCallback: {responseData in
                    self.reloadData(data: responseData)
                }, failureCallback:{ failureData in
                    self.failure(data: failureData)
                })
            }
        }
        //else {
            if indexPath.section == self.selectedSection && indexPath.row == selectedRow {
                cell.baseView.isHidden = false
                cell.applyBlurEffect(view: cell.baseView)
            } else {
                cell.baseView.isHidden = true
                cell.removeBlurEffect()
            }
      //  }
        if let title = cell.objRideData?.ride_title {
            MyDefaults.setData (value: title, key: DefaultKeys.KEY_SHARERIDETITLE)
        }
        cell.isUserInteractionEnabled = true
        cell.imgViewRide.tag = indexPath.row
        cell.btnJoinedRiders.addTarget(self, action: #selector(btnGangAction), for: .touchUpInside)
        cell.tag = indexPath.row
        return cell
    }
    
    
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isRideClone = false
        isMakeMine = false
        if rideInProgressCard.count > 0 && indexPath.section == 0 {
            self.view.makeToastActivity(.center, isUserInteraction: false)
            if let indexObj = rideInProgressCard.index(where: { ($0.rideID! == rideInProgressCard[indexPath.row].rideID!)}) {
                let objRidedata = rideInProgressCard[indexObj]
                selectedRideCard = objRidedata
                planRideModel?.getRideRouteById(rideID: (objRidedata.rideID?.toInt())!, bikerID: (objRidedata.biker_ID?.toInt())!, type: "GetRideRouteById", successCallback: { (data) in
                    self.saveRideRouteResponse(data: data)
                }) { (error) in
                    self.failure(data: error)
                }
            }
        } else {
            if let indexObj = arrOfRides.index(where: { ($0.rideID! == arrOfRides[indexPath.row].rideID!)}) {
                self.view.makeToastActivity(.center, isUserInteraction: false)
                let objRidedata = arrOfRides[indexObj]
                selectedRideCard = objRidedata
                planRideModel?.getRideRouteById(rideID: (objRidedata.rideID?.toInt())!, bikerID: (objRidedata.biker_ID?.toInt())!, type: "GetRideRouteById", successCallback: { (data) in
                    self.saveRideRouteResponse(data: data)
                }) { (error) in
                    self.failure(data: error)
                }
            }
        }
        MyDefaults.setBoolData(value: false, key: "isPOICached")
    }
    
    func shareRide(rideDetails: Rides) {
        tableViewMyRide.allowsSelection = true
        if NetworkCheck.isNetwork() {
            let dynamicLink = DynamiclinkShare()
            dynamicLink.dynamicLinkDelegate = self
            
            let objRideData: Rides = rideDetails
            
            if let title = objRideData.ride_title {
                shareRideTitle = title
            }
            
            if let id = objRideData.rideID {
                self.view.makeToastActivity(.center ,isUserInteraction:false)
                dynamicLink.buldDynamicShortUrl(rideID:Int(id)!)
            }
            selectedRow = -1
            selectedSection = -1
            tableViewMyRide.reloadData()
        } else {
            self.view.makeToast(ToastMsgConstant.network)
        }
    }
    
    func btnGangAction(sender: UIButton) {
        let objRidedata: Rides?
        isRideClone = false
        Constants.appDelegate?.isThroughMyRides = true
        let vc:SideMenuVC =  self.slideMenuController()?.leftViewController as! SideMenuVC
        if let tappableView1 = sender.superview?.superview?.superview as? MyRideCell {
            let indexPath = tableViewMyRide?.indexPath(for: tappableView1)
            if indexPath?.section == 0 && rideInProgressCard.count > 0 {
                if let indexObj = rideInProgressCard.index(where: { ($0.rideID! == rideInProgressCard[(indexPath?.row)!].rideID!)}) {
                    objRidedata = rideInProgressCard[indexObj]
                    vc.rideDetails = objRidedata
                }
            } else {
                if let indexObj = arrOfRides.index(where: { ($0.rideID! == arrOfRides[(indexPath?.row)!].rideID!)}) {
                     objRidedata = arrOfRides[indexObj]
                    vc.rideDetails = objRidedata
                }
            }
            MyDefaults.setBoolData(value: false, key: "isPOICached")
            self.view.makeToastActivity(.center, isUserInteraction: false)
            vc.selectMenuAtSection(index: 1, andDeslectSectionIndex: 0)
        }
    }
}

extension MyRidesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return dataSourceArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let dict = dataSourceArray[indexPath.section]
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyRidesCollectionViewCell", for: indexPath) as? MyRidesCollectionViewCell {
            let data = dict["dict"] as? Rides
           
            let img: UIImage = dict["ButtonImg"] as! UIImage
            cell.actionButton.setImage(img, for: .normal)
            cell.actionLbl.text = dict["LblTitle"] as? String
            cell.actionButton.tag = indexPath.section
            if data?.status == 1 && (cell.actionLbl.text == "SHARE" || cell.actionLbl.text == "MAKE MINE"){
                if cell.actionLbl.text == "SHARE" {
                    cell.actionButton.setImage(#imageLiteral(resourceName: "shareGray"), for: .normal)
                }
                if cell.actionLbl.text == "MAKE MINE" {
                    cell.actionButton.setImage(#imageLiteral(resourceName: "makeMineGray"), for: .normal)
                }
                cell.actionButton.isUserInteractionEnabled = false
                cell.actionLbl.textColor = UIColor.darkGray
            } else {
                cell.actionButton.isUserInteractionEnabled = true
                cell.actionLbl.textColor = UIColor.black
            }
            cell.actionButton.addTarget(self, action: #selector(setActionsToButtons), for: .touchUpInside)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width/CGFloat(dataSourceArray.count)
        let height = width
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: -30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func setActionsToButtons(sender: UIButton) {
        let rideObj = dataSourceArray[sender.tag]
        switch sender.tag {
        case 0:
            // share
            if let myride = rideObj["dict"] as? Rides {
                self.shareRide(rideDetails: myride)
            }
            
        case 1:
            if let obj = rideObj["dict"] as? Rides {
                if let type = rideObj["Type"] as? String, let option = rideObj["LblTitle"] as? String, option == "MAKE MINE" && type == "RIDER" {
                    //clone
                     self.cloneRide(rideCard: obj)
                } else if let type = rideObj["Type"] as? String, type == "RIDER"{
                    //leave
                    if let id = obj.rideID, let name = obj.ride_title {
                        self.leaveRide(rideID: Int(id)!, rideName: name)
                    }
                } else {
                    //edit
                    if let id = obj.rideID {
                        currentRideID = Int(id)!
                        if NetworkCheck.isNetwork() {
                            self.view.makeToastActivity(.center, isUserInteraction: false)
                            rideViewModel?.editRide(rideID: id, type: "GetRideDetails", successCallback: {responseData in
                                self.reloadData(data: responseData)
                            }, failureCallback:{ failureData in
                                self.failure(data: failureData)
                            })
                        } else {
                            self.view.makeToast(ToastMsgConstant.network)
                        }
                       
                    }
                }
            }
        case 2:
            if let type = rideObj["Type"] as? String, let option = rideObj["LblTitle"] as? String, option == "LEAVE" && type == "RIDER" {
                if let obj = rideObj["dict"] as? Rides, let rideId = obj.rideID, let name = obj.ride_title {
                    self.leaveRide(rideID: Int(rideId)!, rideName: name)
                }
            } else {
                if let obj = rideObj["dict"] as? Rides {
                    self.cloneRide(rideCard: obj)
                }
            }
        default:
            break
        }
    }
    
    func getNumberofActions(cardObj: Rides) {
        dataSourceArray = []
        dataSourceArray = (rideViewModel?.getDataSourceForMyRidesActions(rideCard: cardObj))!
        reloadCollectionView()
    }
}

extension MyRidesVC:DynamiclinkShareDelegate, EditRideDelegate{
   
    func shareShortLink(link:String)  {
        
        let myDynamicLink = link;
        let msg = "Hi, \((MyDefaults.getData(key: DefaultKeys.KEY_USERNAME) as? String)!)\(" has invited you to ")\(shareRideTitle). click to check it out " + myDynamicLink
        let shareSheet = UIActivityViewController(activityItems: [ msg ], applicationActivities: nil)
        shareSheet.popoverPresentationController?.sourceView = self.view
        self.present(shareSheet, animated: true, completion: nil)
        
    }
    // MARK: - API Delegate Methods
    func onDynamicShortLink(data:[String:URL]) {
        
        if let data1 = data["data"] {
            self.shareShortLink(link: data1.absoluteString)
            self.view.hideToastActivity()
        }
    }
    
    func onDynamicShortLinkFailure(Error:String){
        print("MyRidesVC:\(Error)")
        self.view.hideToastActivity()
    }
    
    func leaveRide(rideID: Int, rideName: String) {
        if NetworkCheck.isNetwork() {
            let alert = UIAlertController.init(title: "", message: "Leave \(rideName) group?", preferredStyle: .alert)
            let iWillRide = UIAlertAction.init(title: "I will Ride", style: .cancel, handler: { (action) in
            })
            let leaveRide = UIAlertAction.init(title: "Leave Ride", style: .default, handler: { (action) in
                self.currentRideID = rideID
                self.rideViewModel?.leaveRide(rideID: rideID, type: "LeaveRide", successCallback: {responseData in
                    self.reloadData(data: responseData)
                }, failureCallback:{ failureData in
                    self.failure(data: failureData)
                })
            })
            alert.addAction(iWillRide)
            alert.addAction(leaveRide)
            self.presentVC(alert)
        } else {
            self.view.makeToast(ToastMsgConstant.network)
        }
    }
    
    func showDetailsOnCard(row: Int, rideObj: Rides, section: Int) {
        tableViewMyRide.allowsSelection = false
        let finalSection = rideInProgressCard.count > 0 ? section : 0
        let indexPath = NSIndexPath(row: row, section: finalSection)
        if let cell:MyRideCell = tableViewMyRide.cellForRow(at: indexPath as IndexPath) as? MyRideCell {
            self.selectedRow = row
            self.selectedSection = finalSection
            cell.baseView.isHidden = false
            tableViewMyRide.reloadData()
            getNumberofActions(cardObj: rideObj)
            cell.adminCollectionView.delegate = self
            cell.adminCollectionView.dataSource = self
        }
    }
    
    func reloadCollectionView() {
        let indexPath = NSIndexPath(row: selectedRow!, section: selectedSection!)
        if let cell:MyRideCell = tableViewMyRide.cellForRow(at: indexPath as IndexPath) as? MyRideCell {
            cell.adminCollectionView.delegate = self
            cell.adminCollectionView.dataSource = self
            cell.adminCollectionView.reloadData()
        }
    }
    
    func closeOverlayView() {
        selectedSection = -1
        selectedRow = -1
        tableViewMyRide.allowsSelection = true
    }
    
    func cloneRide(rideCard: Rides) {
        if NetworkCheck.isNetwork() {
            if !MyDefaults.getBoolData(key: "IsClonePopupShown") {
                RecommendedRideVC.recommendedObj = nil
                let alert = UIAlertController.init(title: "", message: "This feature enables you to clone the ride", preferredStyle: .alert)
                let okAction = UIAlertAction.init(title: "Ok", style: .default) { (ok) in
                    self.view.makeToastActivity(.center, isUserInteraction: false)
                    if NetworkCheck.isNetwork() {
                        self.isRideClone = true
                        self.rideViewModel?.cloneRide(rideID: Int(rideCard.rideID!)!, data: ["shared" : (rideCard.joined_users > 1)], type: "CloneRide", successCallback: {responseData in
                            self.reloadData(data: responseData)
                        }, failureCallback:{ failureData in
                            self.failure(data: failureData)
                        })
                        MyDefaults.setBoolData(value: true, key: "IsClonePopupShown")
                    } else {
                        self.view.makeToast(ToastMsgConstant.network)
                    }
                }
                alert.addAction(okAction)
                self.presentVC(alert)
            } else {
                self.view.makeToastActivity(.center, isUserInteraction: false)
                isRideClone = true
                self.rideViewModel?.cloneRide(rideID: Int(rideCard.rideID!)!, data: ["shared" : (rideCard.joined_users > 1)], type: "CloneRide", successCallback: {responseData in
                    self.reloadData(data: responseData)
                }, failureCallback:{ failureData in
                    self.failure(data: failureData)
                })
            }
        } else {
            self.view.makeToast(ToastMsgConstant.network)
        }
    }
}

extension MyRidesVC: ReloadMyRidesDelegate {
    
    func reloadData(data: Any) {
        if  let dict = data as? [String: Any] , let request = dict["RequestType"] as? String, request == "GetPlannedRides",let arr = dict["data"] as? [Rides] {
            isLastPage = (dict["isLastPage"] as? Bool)!
            arrOfRides = []
            arrOfRides = arr
            reloadMyRides()
        } else if let dict = data as? [String: Any] , let request = dict["RequestType"] as? String, request == "GetRideDetails" , let editRideResponse = dict["data"] as? RideCreateResponse {
                    self.editRide(rideData: editRideResponse)
        } else if let dict = data as? [String: Any], let request = dict["RequestType"] as? String, request == "CloneRide" {
            let objRidedata = dict["data"] as? RideCreateResponse
            planRideModel?.getRideRouteById(rideID: (objRidedata?.id)!, bikerID: (objRidedata?.admin_biker_id)!, type: "GetRideRouteById", successCallback: { (data) in
                self.saveRideRouteResponse(data: data)
            }) { (error) in
                self.failure(data: error)
            }
            cloneRideSuccessData = dict
        } else if let dict = data as? [String: Any], let request = dict["RequestType"] as? String, request == "LeaveRide" {
            self.leaveRideSuccess()
        } else if let dict = data as? [String: Any], let request = dict["RequestType"] as? String, request == "GetRideMembers",  !isRideClone {
            self.addMemberListSuccess()
        } else if let rides = data as? [Rides] {
            arrOfRides = rides
            self.noRidesView.isHidden = true
            if arrOfRides.count > 0 || rideInProgressCard.count > 0{
                self.tableViewMyRide.reloadData()
            } else {
                DispatchQueue.main.async{
                    self.settableBackgroundView()
                }
            }
        }else if !isRideClone {
            //access denied temporary handling
            if let rideData = data as? [Rides] {
                if arrOfRides.count == rideData.count {
                    isLastPage = true
                } else {
                    isLastPage = false
                }
                arrOfRides = []
                arrOfRides = rideData
                reloadMyRides()
            } else {
                self.addMemberListSuccess()
            }
        }
    }
    
    func reloadMyRides() {
        compareTodaysDateWithRideStartDate()
        rideInProgressCard = Rides.getRideInProgressObjArray(context: context, type: .PLANNED)
        if arrOfRides.count < 10 && !isLastPage {
            page = page + 1
            rideViewModel?.getAllRidesOfBiker(request: "GetPlannedRides", pageNo: page, bikerID: bikerID, size: 10, successCallback: {responseData in
                self.reloadData(data: responseData)
            }, failureCallback:{ failureData in
                self.failure(data: failureData)
            })
        } else {
            self.tableViewMyRide.reloadData()
        }
        
        self.view.hideToastActivity()
        self.noRidesView.isHidden = true
        if arrOfRides.count > 0 || rideInProgressCard.count > 0{
            self.tableViewMyRide.reloadData()
        } else {
            DispatchQueue.main.async{
                self.settableBackgroundView()
            }
        }
        
    }
    
    
    func editRide(rideData: RideCreateResponse) {
        let planRideVC = PlanRide()
        var viaPointArray:[Any] = []
        let dataManager = PlanRideDataManager.shared()
        RecommendedRideVC.recommendedObj = nil
        PlanRideTableViewController.sourceAddressSelected = true
        if let wayPointArray = rideData.waypoints {
            for i in 0..<wayPointArray.count {
                let dict = wayPointArray[i] as? [String: Any]
                if let type: [Any] = dict?["types"] as? [Any], (type.first as? String == StringConstant.VIA_POINT) ||
                    (type.first as? String == StringConstant.PLACES_OF_INTEREST)
                    {
                    
                    let poiRes:POIListResponse = Mapper<POIListResponse>().map(JSON:dict!)!
                    var poiType:POIType = .Fuel
                    if poiRes.categories?.contains(StringConstant.POI_CATEGORY_FUEL_PUMPS) ?? false {
                        //
                    }else if poiRes.categories?.contains(StringConstant.POI_CATEGORY_RESTAURANTS) ?? false {
                        poiType = .Restaurent
                    }else if poiRes.categories?.contains(StringConstant.POI_CATEGORY_LODGING) ?? false {
                        poiType = .Hotel
                    }else {
                        poiType = .ViaPoint
                    }
                    dataManager.addPOI(poi: POIList(position: CLLocationCoordinate2D(latitude: (poiRes.location?.latitude!)!, longitude: (poiRes.location?.longitude!)!), response: poiRes), poiType: poiType)
                    viaPointArray.append(dict!)
                }
            }
        }
        dataManager.routeData[PlanRideDataSource.ViaPoint] = dataManager.viaPointsArray
        
        var startObj: [String:Any] = [:]
        startObj["name"] = rideData.start_location?.name
        var latLong:[String: Any] = [:]
        latLong["latitude"] = rideData.start_location?.latlong?.lat
        latLong["longitude"] = rideData.start_location?.latlong?.lng
        startObj["latlong"] = latLong
        dataManager.addSource(data: startObj)
      //  planRideVC.autocompleteSearchitems[PlanRideDataSource.SourcePlace] = startObj
        
        var destObj: [String:Any] = [:]
        destObj["name"] = rideData.end_location?.name
        latLong["latitude"] = rideData.end_location?.latlong?.lat
        latLong["longitude"] = rideData.end_location?.latlong?.lng
        destObj["latlong"] = latLong
        dataManager.addDestination(data: destObj)
      //  planRideVC.autocompleteSearchitems[PlanRideDataSource.DestinationPlace] = destObj
        planRideVC.rideID = currentRideID
        planRideVC.planMode = PlanRideFlowOptions(rawValue: PlanRideFlowOptions.EditRide.rawValue)!
        self.view.hideToastActivity()
        self.pushVC(planRideVC)
        selectedSection = -1
        selectedRow = -1
    }
    
    func addMemberListSuccess() {
        self.view.hideToastActivity()
        Constants.appDelegate?.isThroughMyRides = false
        Constants.appDelegate?.editRideNotification = false
        if let vc:SideMenuVC =  self.slideMenuController()?.leftViewController as? SideMenuVC {
            vc.rideDetails = selectedRideCard
            vc.selectMenuAtSection(index: 1, andDeslectSectionIndex: 0)
        }
    }
    
    func failure(data: [String : Any]) {
        self.view.hideToastActivity()
        if let errModel:ErrorModel = data["data"] as? ErrorModel {
            print(errModel.message ?? " ")
            self.view.makeToast((errModel.description)!)
            if arrOfRides.count == 0 {
                self.settableBackgroundView()
            } else {
                self.noRidesView.isHidden = true
            }
        } else if data["RequestType"] as? String == "GetRideMembers" || data["RequestType"] as? String == "GetRideRouteById"{
            if isRideClone {
                cloneRideSuccess(data: cloneRideSuccessData!)
            } else  {
                addMemberListSuccess()
            }
        } else if  data["RequestType"] as? String == "LeaveRide"{
            let description = data["data"] as? [String: Any]
            self.view.makeToast(description!["description"] as! String)
        }else if  data["RequestType"] as? String == "CloneRide"{
            let description = data["data"] as? [String: Any]
            self.view.makeToast(description!["description"] as! String)
        }
        else {
            if data["RequestType"] as? String == "SaveRide" {
                rideViewModel?.getRideMembers(rideID: Int((selectedRideCard?.rideID)!)!, type: "GetRideMembers", successCallback: {responseData in
                    self.reloadData(data: responseData)
                }, failureCallback:{ failureData in
                    self.failure(data: failureData)
                })
            } else if !isRideClone{
                addMemberListSuccess()
            }
//            self.view.makeToast(data["description"] as! String)
        }
    }
    
    func saveRideRouteResponse(data: Any) {
        if  let dict = data as? [String: Any], let request = dict["RequestType"] as? String, request == "GetRideRouteById"{
            let rideCard = dict["data"] as? SingleRouteDetails
            rideViewModel?.getRideMembers(rideID: (rideCard?.id)!, type: "GetRideMembers", successCallback: {responseData in
                self.reloadData(data: responseData)
            }, failureCallback:{failureData in
                self.failure(data: failureData)
            })
            if isRideClone {
                cloneRideSuccess(data: cloneRideSuccessData!)
            }
        }
    }
    
    func saveRideSuccess(data: Any) {
        if  let dict = data as? [String: Any], let request = dict["RequestType"] as? String, request == "GetRideRouteById"{
            let rideCard = dict["data"] as? SingleRouteDetails
            rideViewModel?.getRideMembers(rideID: (rideCard?.id)!, type: "GetRideMembers", successCallback: {responseData in
                self.reloadData(data: responseData)
            }, failureCallback:{ failureData in
                self.failure(data: failureData)
            })
            if isRideClone {
                cloneRideSuccess(data: cloneRideSuccessData!)
            }
        } else if !isRideClone{
            addMemberListSuccess()
            isRideClone = false
        }
    }
}
