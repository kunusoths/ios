//
//  ViaPointsTableViewCell.swift
//  Maximus
//
//  Created by Isha Ramdasi on 12/12/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//

import UIKit

protocol RemoveViaPointDelegate:class {
    func removeViaPoint(row: Int)
    func didTapRow(row: Int)
}

class ViaPointsTableViewCell: UITableViewCell {
    @IBOutlet weak var placeNameLbl: UILabel!
    
    @IBOutlet weak var tappableView: UIView!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var removeViaPointBtn: UIButton!
    @IBOutlet weak var imgViaPoint: UIImageView!
    weak var removeDelegate: RemoveViaPointDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if Constants.DeviceType.IS_IPHONE_5{
            cellView.frame.size = CGSize(width: 238.0, height: 54.0)
            imgViaPoint.x = 11.0
            placeNameLbl.x = 45.0
            tappableView.frame.size = CGSize(width: 210.0, height: 56.0)
            removeViaPointBtn.x = 212.0
        }
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didSelectRow(_:)))
        tappableView.addGestureRecognizer(tapGesture)
        self.cellView.addBorder(width: 2, color: .lightGray)
        self.cellView.layer.cornerRadius = 5
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(viaPointsList: [String: Any], row: Int) -> ViaPointsTableViewCell {
        self.isUserInteractionEnabled = ((viaPointsList[PlanRideDataSource.DestinationPlace] as?  [String: Any]) != nil) && ((viaPointsList[PlanRideDataSource.SourcePlace] as? [String: Any]) != nil) ? true : false
        self.tappableView.tag = row
        if let viaPointsArray:[Any] = viaPointsList[PlanRideDataSource.ViaPoint] as? [Any] {
           
                if viaPointsArray.count > 0{
                    let predicationsArray = viaPointsList[PlanRideDataSource.ViaPoint] as? [Any]
                    if let placeDetailsObj:POIData =  predicationsArray![row] as? POIData {
                        let delimiter = ","
                        
                        switch placeDetailsObj.poiType {
                        case .Fuel:
                            self.imgViaPoint.image = UIImage(named: "Petrol Small")
                            break
                         
                        case .Hotel:
                            self.imgViaPoint.image = UIImage(named: "Hotel Small")
                            break
                        
                        case .Restaurent:
                            self.imgViaPoint.image = UIImage(named: "Restaurants Small")
                            break
                            
                        case .ViaPoint:
                            self.imgViaPoint.image = UIImage(named: "Via-point-in-map")
                            break
                        }
                        
                        var stringList = (placeDetailsObj.name).components(separatedBy: delimiter)
                        self.placeNameLbl.text = stringList[0]
                    }
                    self.placeNameLbl.textColor = UIColor.darkGray
                }
        }
        return self
    }
    
    @IBAction func removeViaPoint(_ sender: UIButton) {
        if let buttonCell = sender.superview?.superview?.superview as? ViaPointsTableViewCell {
            let tableView: UITableView?
            if Constants.Version.iOS10 {
                tableView = buttonCell.superview?.superview as? UITableView
            } else {
                tableView = buttonCell.superview as? UITableView
            }
            let indexPath = tableView?.indexPath(for: buttonCell)
                let rowOfCell = indexPath?.row
                self.removeDelegate?.removeViaPoint(row: rowOfCell!)
        }
    }
    
    func didSelectRow(_ sender: UITapGestureRecognizer) {
        if let tappableView1 = sender.view?.superview?.superview as? ViaPointsTableViewCell {
            let tableView: UITableView?
            if Constants.Version.iOS10 {
                tableView = tappableView1.superview?.superview as? UITableView
            } else {
                tableView = tappableView1.superview as? UITableView
            }
            let indexPath = tableView?.indexPath(for: tappableView1)
            let rowOfCell = indexPath?.row
            self.removeDelegate?.didTapRow(row: rowOfCell!)
        }
        
    }
}
