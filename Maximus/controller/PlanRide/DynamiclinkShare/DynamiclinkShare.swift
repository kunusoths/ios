//
//  DynamiclinkShare.swift
//  Maximus
//
//  Created by Namdev Jagtap on 19/04/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

import Alamofire
import ObjectMapper
import FirebaseDynamicLinks


@objc protocol DynamiclinkShareDelegate {
    @objc optional func onDynamicShortLink(data:[String:URL])
    @objc optional func onDynamicShortLinkFailure(Error:String)
    
}

extension URLComponents {
    init(scheme: String, host: String, path: String, queryItems: [URLQueryItem]) {
        self.init()
        self.scheme = scheme
        self.host = host
        self.path = path
        self.queryItems = queryItems
    }
}


class DynamiclinkShare: NSObject {
    
    
    var dynamicLinkDelegate:DynamiclinkShareDelegate?
    var cloudHandlerObj = CloudHandler()
    
    func buldDynamicShortUrl(rideID:Int)  {
        
        if let url = URLComponents(scheme: DynamicLinkShareURL.DeepLinkScheme.rawValue,
                                   host: DynamicLinkShareURL.DeepLinkHost.rawValue,
                                   path: DynamicLinkShareURL.DeepPath.rawValue,
                                   queryItems: [URLQueryItem(name: DynamicLinkShareURL.DynamicRideID.rawValue, value: "\(rideID)")]).url {//,
            /*URLQueryItem(name: "apn", value: Bundle.main.bundleIdentifier!),
             URLQueryItem(name: "ibi", value: Bundle.main.bundleIdentifier!)])*/
            
            let components = DynamicLinkComponents(link: url, domain: DynamicLinkShareURL.DynamicLinkDomain.rawValue)
            
            
            if let bundleID = Bundle.main.bundleIdentifier {
                // iOS params
                let iOSParams = DynamicLinkIOSParameters(bundleID: bundleID)
                iOSParams.customScheme = "Maximuspro"
                iOSParams.appStoreID = "1309188922"
                components.iOSParameters = iOSParams
                // iTunesConnect params
                let appStoreParams = DynamicLinkItunesConnectAnalyticsParameters()
                components.iTunesConnectParameters = appStoreParams
            }
            
            if let packageName = Bundle.main.bundleIdentifier {
                // Android params
                let androidParams = DynamicLinkAndroidParameters(packageName: packageName)
                components.androidParameters = androidParams
            }
              
            let options = DynamicLinkComponentsOptions()
            options.pathLength = .unguessable
            components.options = options
            
            components.shorten { (shortURL, warnings, error) in
                // Handle shortURL.
                if let error = error {
                    print(error.localizedDescription)
                    self.dynamicLinkDelegate?.onDynamicShortLinkFailure!(Error: error.localizedDescription)
                    return
                }
                
                print(shortURL ?? "")
                
                if let shortUrl = shortURL {
                    let data:[String:Any] = [StringConstant.data:shortUrl]
                    self.dynamicLinkDelegate?.onDynamicShortLink!(data: data as! [String : URL])
                }
                
            }
            
            /*self.cloudHandlerObj.fireBaseDynamicShortLinkRequest(subUrl:Constants.APIKEY_DYNAMICLINKS!, parameter: parameter, isheaderAUTH: false, requestType: .post) { response in
             
             if let json = response?.result.value {
             let statusCode = response?.response?.statusCode
             print("statusCode ",statusCode ?? 0,"JSON ",json)
             if (statusCode)! >= 200 && (statusCode)! < 300
             {
             let shortLink = Mapper<DynamicLinkShareModel>().map(JSON:json as! [String : Any])!
             let data:[String:Any] = [StringConstant.data:shortLink]
             self.dynamicLinkDelegate?.onDynamicShortLink!(data: data)
             } else {
             
             self.errorCall(value: json as! [String : Any])
             }
             }
             else{
             self.errorCall(value: RequestTimeOut().json)
             }
             
             }*/
        }
    }
    
    //TODO:Error Call Handling Method
    /* func errorCall(value:[String : Any]!){
     let ErrorModelObj = Mapper<ErrorModel>().map(JSON:value)
     let errorData:[String:Any] = [StringConstant.data:ErrorModelObj ?? " "]
     self.dynamicLinkDelegate?.onDynamicShortLinkFailure!(Error: errorData)
     }*/
}
