//
//  PlanRideTableViewController.swift
//  Maximus
//
//  Created by Isha Ramdasi on 09/07/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces

protocol LoadNextView: class {
    func pushController(controller: ViaPointsListViewController)
    func findRouteByApi(jsonObj: [String: Any], bounds: GMSCoordinateBounds, autoCompleteResults: [String: Any])
    func passSelectedSourceDestination(data: [String: Any])
    func reorderFinished()
    func notConnectedToInternet()
}

class PlanRideTableViewController: LPRTableView, HeaderTableViewCellDelegate, RemoveViaPointDelegate,PassSelectedPlaceDelegate, PlanRideFooterTableViewCellDelegate {
    
    var autocompleteSearchitems: [String: Any] = [:]
    var jsonObject = [String:Any]()
    var planRideBounds: GMSCoordinateBounds?
    var sourceAddress: String?
    static var sourceAddressSelected: Bool = false
    var markerPosition = CLLocationCoordinate2D()
    weak var pushControllerDelegate: LoadNextView?
    var isListExpanded: Bool = false
    let rideDataManager = PlanRideDataManager.shared()
    var distance: String = ""
    var isPlanModeSMTRecommended = false
    override func awakeFromNib() {
        super.awakeFromNib()
        planRideBounds = GMSCoordinateBounds()
        
        self.minimumPressDuration = 0.3
        self.longPressReorderDelegate = self
        self.register(UINib.init(nibName: TableViewCells.footerCell, bundle: nil), forHeaderFooterViewReuseIdentifier: TableViewCells.footerCell)
        self.register(UINib.init(nibName: TableViewCells.SourceDestinationCellIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: TableViewCells.SourceDestinationCellIdentifier)
        self.register(UINib(nibName: TableViewCells.ViaPointsCell, bundle: nil), forCellReuseIdentifier: TableViewCells.ViaPointsCell)
        self.dataSource = self
        self.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(getSourceName(_:)), name: NSNotification.Name(rawValue: "GetSourceName"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(getDistance(_:)), name: NSNotification.Name(rawValue: "GetDistance"), object: nil)
    }
        
   
    
    func didSelectUserHeaderTableViewCell(Selected: Int, UserHeader: SourceDestinationTableViewCell) {
        
        if !NetworkCheck.isNetwork() {
            self.pushControllerDelegate?.notConnectedToInternet()
            return
        }
        
        self.isUserInteractionEnabled = false
        let viapointsList = ViaPointsListViewController()
        viapointsList.selectedSection = PlanRideDataSource.SourcePlace
        let paramToSend = [StringConstant.biker_id_recommendation_id: "\(MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID)) \(RecommendedRideVC.recommendedObj?.card_id ?? "")"]
        Analytics.logCustomEvent(withName: "starting_location_change", customAttributes: paramToSend)
        viapointsList.passDelegate = self
        self.pushControllerDelegate?.pushController(controller: viapointsList)
        rideDataManager.selectedDestinationFirstTime = false
    }
    
    func didSelectUserFooterTableViewCell(Selected: Int, UserHeader: SourceDestinationTableViewCell) {
        if !NetworkCheck.isNetwork() {
             self.pushControllerDelegate?.notConnectedToInternet()
            return
        }
        
        self.isUserInteractionEnabled = false
        let viapointsList = ViaPointsListViewController()
        viapointsList.selectedSection = PlanRideDataSource.DestinationPlace
        viapointsList.passDelegate = self
        if !rideDataManager.selectedDestinationFirstTime {
            self.pushControllerDelegate?.pushController(controller: viapointsList)
        }
        rideDataManager.selectedDestinationFirstTime = false

    }
    
    func removeViaPoint(row: Int) {
        if !NetworkCheck.isNetwork() {
            self.pushControllerDelegate?.notConnectedToInternet()
            return
        }
        if rideDataManager.viaPointsArray.count > 0, rideDataManager.viaPointsArray.count > row {
            rideDataManager.removeViaPoint(at: row)

            jsonObject[StringConstant.VIA_POINTS_DETAILS] = PlanRideHandler.planRideHandlerObj.setViaPointList(dataArray: rideDataManager.viaPointsArray)
            let paramToSend = ["\(StringConstant.biker_id_recommendation_id)" : "\(MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID)) \(RecommendedRideVC.recommendedObj?.card_id ?? "")"]
            Analytics.logCustomEvent(withName: "remove_added_stop", customAttributes: paramToSend)
            findRoute(json: jsonObject)
        }
    }
    
    func didTapRow(row: Int) {
        let viaPointArray = rideDataManager.routeData[PlanRideDataSource.ViaPoint] as? [Any]
        if  let arrCount = viaPointArray?.count,  row >= arrCount {
            self.isUserInteractionEnabled = false
            let viapointsList = ViaPointsListViewController()
            viapointsList.selectedSection = PlanRideDataSource.ViaPoint
           // viapointsList.placeList = autocompleteSearchitems
            viapointsList.passDelegate = self
            viapointsList.selectedRow = row
            self.pushControllerDelegate?.pushController(controller: viapointsList)
        }
    }
    
    func passSelectedPlaceForViaPoint(data: [Any], completion: (Bool) -> Void) {
        jsonObject[StringConstant.VIA_POINTS_DETAILS] = PlanRideHandler.planRideHandlerObj.setViaPointList(dataArray: rideDataManager.viaPointsArray)
        self.pushControllerDelegate?.passSelectedSourceDestination(data: rideDataManager.routeData)
        findRoute(json: jsonObject)
    }
    
    func passSelectedPlace(data: [String : Any], locationType: LocationType) {
        switch locationType {
        case .source:
            //  autocompleteSearchitems[PlanRideDataSource.SourcePlace] = data
            sourceAddress = ""
            PlanRideTableViewController.sourceAddressSelected = true
            //jsonObject[StringConstant.SOURCE_DETAILS] = data
            rideDataManager.addSource(data: data)
            
            if let srcLoc = data["latlong"] as? [String:Any], let lat = srcLoc["latitude"] as? Double, let lng = srcLoc["longitude"] as? Double {
                self.planRideBounds = planRideBounds?.includingCoordinate(CLLocationCoordinate2D.init(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lng)))
                self.markerPosition = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lng))
            }
            break
            
        case .destination:
            //  autocompleteSearchitems[PlanRideDataSource.DestinationPlace] = data
            //jsonObject[StringConstant.DESTINATION_DETAILS] = data
            rideDataManager.addDestination(data: data)
            if let srcLoc = data["latlong"] as? [String:Any], let lat = srcLoc["latitude"] as? Double, let lng = srcLoc["longitude"] as? Double {
                self.planRideBounds = planRideBounds?.includingCoordinate(CLLocationCoordinate2D.init(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lng)))
            }
            break
        default:
            break
        }
        self.pushControllerDelegate?.passSelectedSourceDestination(data: rideDataManager.routeData)
        findRoute(json: jsonObject)
    }
    
    func findRoute(json: [String:Any]) {
        do{
            var placeJson:[String:Any] = [:]
            if rideDataManager.isSourceAdded() {
                placeJson[StringConstant.SOURCE_DETAILS] = rideDataManager.routeData[PlanRideDataSource.SourcePlace]
            }
            
            if rideDataManager.isDestinationAdded() {
                placeJson[StringConstant.DESTINATION_DETAILS] = rideDataManager.routeData[PlanRideDataSource.DestinationPlace]
            }
            
            placeJson[StringConstant.VIA_POINTS_DETAILS] = PlanRideHandler.planRideHandlerObj.setViaPointList(dataArray: rideDataManager.viaPointsArray)
            print("Find route json object : \(placeJson)")
            if (placeJson[StringConstant.SOURCE_DETAILS] != nil) && (placeJson[StringConstant.DESTINATION_DETAILS] != nil) {
                self.pushControllerDelegate?.findRouteByApi(jsonObj: placeJson, bounds: planRideBounds!, autoCompleteResults: autocompleteSearchitems)
            }
        }
    }
    
}

// MARK: LongPressReorder Table View Delegate Methods
extension PlanRideTableViewController: LPRTableViewDelegate {
    //called when reoeder process is ended by LongPressReorder Table View
    func reorderEnded(initialIndex: IndexPath, finalIndex: IndexPath) {
        let initiaRow = self.initialIndexPath!.row
        let idxRow = finalIndex.row
        if idxRow <= rideDataManager.viaPointsArray.count  && initiaRow <= rideDataManager.viaPointsArray.count {
            let viaPoint = rideDataManager.viaPointsArray[initiaRow]
            rideDataManager.viaPointsArray.remove(at: initiaRow)
            rideDataManager.viaPointsArray.insert(viaPoint, at: idxRow)
            rideDataManager.routeData[PlanRideDataSource.ViaPoint] = rideDataManager.viaPointsArray
            jsonObject[StringConstant.VIA_POINTS_DETAILS] = PlanRideHandler.planRideHandlerObj.setViaPointList(dataArray: rideDataManager.viaPointsArray)
            findRoute(json: jsonObject)
        }
    }
}

extension PlanRideTableViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: UITableView datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let viaPoints = rideDataManager.routeData[PlanRideDataSource.ViaPoint] as? [Any], viaPoints.count > 0 && isListExpanded {
            return viaPoints.count  //< 10 ? (1 + viaPoints.count) : viaPoints.count
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell =  tableView.dequeueReusableHeaderFooterView(withIdentifier: TableViewCells.SourceDestinationCellIdentifier) as? SourceDestinationTableViewCell
        cell?.headerDelegate = self
        cell?.tag = 0
        cell?.imgView.image = #imageLiteral(resourceName: "From")
        if let selectedSource = rideDataManager.routeData[PlanRideDataSource.SourcePlace] as?  [String: Any]  {
            let delimiter = ","
            var stringList = (selectedSource[StringConstant.NAME] as? String)?.components(separatedBy: delimiter)
            cell?.textLbl.text = (self.sourceAddress == "" ) ?  stringList?[0] : StringConstant.MYLOCATION
            cell?.textLbl.textColor = UIColor.darkGray
        } else {
            cell?.textLbl.text = self.sourceAddress != "" ? StringConstant.MYLOCATION : StringConstant.CHOOSE_SOURCE
            cell?.textLbl.textColor =  self.sourceAddress != "" ? UIColor.lightGray :UIColor.lightGray
        }
        if self.isPlanModeSMTRecommended{
            cell?.isUserInteractionEnabled = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell =  tableView.dequeueReusableHeaderFooterView(withIdentifier: TableViewCells.footerCell) as?  PlanRideFooterTableViewCell
        if let selectedDestination = rideDataManager.routeData[PlanRideDataSource.DestinationPlace] as?  [String: Any] {

            let delimiter = ","
            var stringList = (selectedDestination[StringConstant.NAME] as? String)?.components(separatedBy: delimiter)
            cell?.textLbl.text = stringList?[0]
            cell?.textLbl.textColor = UIColor.darkGray
            cell?.distanceLbl.text = self.distance == "" ? "" : "Approx \(self.distance) Kms"
        } else {
            cell?.textLbl.text = StringConstant.CHOOSE_DESTINATION
            cell?.textLbl.textColor = UIColor.lightGray
            cell?.distanceLbl.text = ""
        }
        cell?.planRideFooterDelegate = self
        cell?.tag = 1
        if self.isPlanModeSMTRecommended{
            cell?.isUserInteractionEnabled = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return setTableRowHeight()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return setTableRowHeight()
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier:TableViewCells.ViaPointsCell, for: indexPath) as? ViaPointsTableViewCell {
            if self.isPlanModeSMTRecommended{
                cell.removeViaPointBtn.isHidden = true
            }
            cell.removeDelegate = self
            return cell.configureCell(viaPointsList: rideDataManager.routeData, row: indexPath.row)
        }
        return UITableViewCell()
    }
    
    func reloadTableData() {
        self.reloadData()
    }
    // MARK: UITableView delegates
    func setTableRowHeight() -> CGFloat {
        return CGFloat(45)
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func getSourceName(_ notification : Notification) {
        if let data = notification.userInfo as? [String: Any] {
            sourceAddress = data["sourceName"] as? String
            PlanRideTableViewController.sourceAddressSelected = true
            reloadData()
        }
    }
    
    func didSelectFooterTableViewCell(Selected: Int, UserHeader: PlanRideFooterTableViewCell) {
        if !NetworkCheck.isNetwork() {
            self.pushControllerDelegate?.notConnectedToInternet()
            return
        }
        self.isUserInteractionEnabled = false
        let viapointsList = ViaPointsListViewController()
        viapointsList.selectedSection = PlanRideDataSource.DestinationPlace
        viapointsList.passDelegate = self
        self.pushControllerDelegate?.pushController(controller: viapointsList)
    }

    func getDistance(_ notification: Notification) {
        if let distance = notification.userInfo!["Distance"] as? String{
            self.distance = distance
        }
    }
}
