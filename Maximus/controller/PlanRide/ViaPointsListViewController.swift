//
//  ViaPointsListViewController.swift
//  Maximus
//
//  Created by Isha Ramdasi on 12/12/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//

import UIKit
import ObjectMapper
import GoogleMaps
import GooglePlaces

typealias CompletionHandler = (_ success:Bool) -> Void

protocol PassSelectedPlaceDelegate: class {
    func passSelectedPlace(data:[String: Any], locationType: LocationType)
    func passSelectedPlaceForViaPoint(data: [Any], completion: CompletionHandler)
}

protocol PassYourLocationAsSourceDelegate: class {
    func passYourLocationAsSource()
}
class ViaPointsListViewController: UIViewController, RideHandlerDelegate  {
    
    @IBOutlet weak var dataTblView: UITableView!
    @IBOutlet weak var poiCollection: PlanRideInDetailCollectionView!
    @IBOutlet weak var pickAndChooseView: UIView!
    
    @IBOutlet weak var myLocationSuggestionVw: UIView!
    @IBOutlet weak var btnYourLocation: UIButton!
    
    @IBOutlet weak var myLocationViewTopConstraint: NSLayoutConstraint!
    
    weak var passDelegate: PassSelectedPlaceDelegate?
    static weak var passYourLocationDelegate:PassYourLocationAsSourceDelegate?
    var filteredPlaceList = [Any]()
    var searchController: UISearchController!
    var selectedSection: String = ""
    var selectedRow: Int = 0
    var rideHandler = PlanRideHandler()
    var viaPointsArray: [Any] = []
    var selectedSource = ""
    var selectedDestination = ""

    let delimiter = ","
    let dataManager = PlanRideDataManager.shared()
    let celIIdentifier = "ViaPointsTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataTblView.register(UINib(nibName: celIIdentifier, bundle: nil), forCellReuseIdentifier: celIIdentifier)
        self.dataTblView.tableFooterView = UIView()
        self.poiCollection.poiDelegate = self
        dataTblView.dataSource = self
        dataTblView.delegate = self
        viaPointsArray = dataManager.routeData[PlanRideDataSource.ViaPoint] as! [Any]
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Arrow White"), style: .plain, target: self, action: #selector(popView))
        self.navigationItem.leftBarButtonItem?.accessibilityIdentifier = "btn_planride_back"
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.poiCollection.accessibilityIdentifier = "cltn_planride_addpois"
        initializeSearchBar()
        setViewTitle()
        
        if selectedSection != PlanRideDataSource.ViaPoint {
            searchController.searchBar.becomeFirstResponder()
            //hide poi view
            self.pickAndChooseView.isHidden = true
//            self.poiCollection.isHidden = true
        }else{
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.dataTblView.isUserInteractionEnabled = true
        searchController.isActive = true
        searchController.searchBar.becomeFirstResponder()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        searchController.searchBar.endEditing(true)
        searchController.isActive = false
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showPoiScreen(_ sender: UIButton) {
        
//        let vc = PlanRideInDetail()
//        self.pushVC(vc)
    }
    
    @IBAction func yourLocationBtnTapped(_ sender: Any) {
        if NetworkCheck.isNetwork(){
            if CLLocationManager.locationServicesEnabled(){
                ViaPointsListViewController.passYourLocationDelegate!.passYourLocationAsSource()
                filteredPlaceList.removeAll()
                self.popView()
            }else{
                searchController.searchBar.endEditing(true)
                self.view.makeToast(NSLocalizedString("keyEnableLocationServices", comment: ""))
            }
        }
        else{
            searchController.searchBar.endEditing(true)
            self.view.makeToast(ToastMsgConstant.network)
        }
        
    }
    
    
    func findPlaceWithPlaceName(placeName:String)
    {
       //  self.view.makeToastActivity(.center, isUserInteraction: false)
        let googleObj = GooglePlacesService()
        googleObj.findPlaceName(placeName: placeName) { response in
            
            if let json = response.result.value
            {
                if (response.response?.statusCode)! >= 200 && (response.response?.statusCode)! < 300
                {
             //       self.view.hideToastActivity()
                    let placeResult = Mapper<PlaceDetails>().map(JSON:json as! [String : Any])!
                    if let places:[Predictions] = placeResult.predictions {
                        self.filteredPlaceList = places
                    }
                }
                else
                {
               //     self.view.hideToastActivity()
                    self.errorCall(value: json as! [String : Any])
                }
            }
            
            DispatchQueue.main.async{
                self.dataTblView.reloadData()
            }
        }
    }
    
    func locationDataFailed(obj: [String : Any]) {
        self.view.hideToastActivity()
        self.searchController.searchBar.resignFirstResponder()
        self.dataTblView.isUserInteractionEnabled = true
        self.view.makeToast("There is problem with suggetion. Please try selecting other")
    }
    
    func errorCall(value:[String : Any]!)
    {
        self.view.hideToastActivity()
        let ErrorModelObj = Mapper<ErrorModel>().map(JSON:value)
        let errorData:[String:Any] = ["data":ErrorModelObj ?? " "]
        print(errorData)
        self.view.makeToast(errorData.description)
    }
    
    
}

extension ViaPointsListViewController {
    func formDataSourceForSource(forRow row: Int) {
        dataManager.routeChanged()
        if filteredPlaceList.count > 0 {
            if let placeObj = filteredPlaceList[row] as? Predictions {
                if let source = dataManager.routeData[PlanRideDataSource.SourcePlace]  as? [String: Any] {
                    selectedSource = source[StringConstant.NAME] as! String
                }
                if let destination = dataManager.routeData[PlanRideDataSource.DestinationPlace] as? [String: Any] {
                    selectedDestination = destination[StringConstant.NAME] as! String
                }
                if !self.isStopAlreadyAdded(stop: placeObj){
                    getRideObj(placeObj: placeObj, type: .source)
                }
            }
        }
        
    }
    
    func formDataSourceForDestination(forRow row: Int) {
        dataManager.routeChanged()
        if filteredPlaceList.count > 0 {
            if let placeObj = filteredPlaceList[row] as? Predictions {
                //if let source = dataManager.routeData[PlanRideDataSource.SourcePlace]  as? [String: Any] {
                if let source = dataManager.routeData[PlanRideDataSource.SourcePlace] as? [String: Any] {
                    selectedSource = source[StringConstant.NAME] as! String
                }
                if let destination = dataManager.routeData[PlanRideDataSource.DestinationPlace] as? [String: Any] {
                    selectedDestination = destination[StringConstant.NAME] as! String
                }
                if !self.isStopAlreadyAdded(stop: placeObj){
                    getRideObj(placeObj: placeObj, type: .destination)
                }
            }
        }
    }
    
    func getRideObj(placeObj: Predictions, type: LocationType) {
        rideHandler.rideHandlerDelegate = self
        if NetworkCheck.isNetwork() {
            if let placeID = placeObj.placeId{
                rideHandler.findLocationWithPlaceId(placeId: placeID, type: type, address: placeObj.description!)
            }
        } else {
            self.view.makeToast(ToastMsgConstant.network)
            DispatchQueue.main.async {
                self.dataTblView.isUserInteractionEnabled = true
                self.view.hideToastActivity()
            }
        }
    }
    
    func formDataSourceForViaPoints(forRow row: Int) {
        var viaPointsArray = dataManager.routeData[PlanRideDataSource.ViaPoint] as? [[String: Any]]
        if let source = dataManager.routeData[PlanRideDataSource.SourcePlace]  as? [String: Any] {
            selectedSource = source[StringConstant.NAME] as! String
        }
        if let destination = dataManager.routeData[PlanRideDataSource.DestinationPlace] as? [String: Any] {
            selectedDestination = destination[StringConstant.NAME] as! String
        }
        
        if let count = viaPointsArray?.count, count > 0 {
            if selectedRow > (count-1), filteredPlaceList.count > 0 {
                if let placeObj = filteredPlaceList[row] as? Predictions {
                    if !self.isStopAlreadyAdded(stop: placeObj) {
                        getRideObj(placeObj: placeObj, type: .viaPoints)
                    }
                }
            } else {
                viaPointsArray?.removeSubrange(selectedRow..<count)
                if filteredPlaceList.count > 0 {
                    if let placeObj = filteredPlaceList[row] as? Predictions {
                        if !self.isStopAlreadyAdded(stop: placeObj){
                            getRideObj(placeObj: placeObj, type: .viaPoints)
                        }
                    }
                }
            }
        } else {
            if filteredPlaceList.count > 0 {
                if let placeObj = filteredPlaceList[row] as? Predictions {
                    if !self.isStopAlreadyAdded(stop: placeObj) {
                        let paramToSend = [StringConstant.biker_id_recommendation_id: "\(MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID)) \(RecommendedRideVC.recommendedObj?.card_id ?? "")"]
                        Analytics.logCustomEvent(withName: "added_place_tap", customAttributes: paramToSend)
                        getRideObj(placeObj: placeObj, type: .viaPoints)
                    }
                }
            }
        }
    }
    
    func returnJsonObj(obj: [String : Any], type: LocationType) {
        print("json object:::\(obj)")
//        self.view.hideToastActivity()
        switch type {
        case .source:
          //  dataManager.routeData[PlanRideDataSource.SourcePlace] = obj
            dataManager.addSource(data: obj)
            var stringList = (obj[StringConstant.NAME] as? String)?.components(separatedBy: delimiter)
            searchController.searchBar.placeholder = stringList![0]
            self.passDelegate?.passSelectedPlace(data: obj, locationType: type)
            break
        case .destination:
           // dataManager.routeData[PlanRideDataSource.DestinationPlace] = obj
            dataManager.addDestination(data: obj)
            var stringList = (obj[StringConstant.NAME] as? String)?.components(separatedBy: delimiter)
            searchController.searchBar.placeholder = stringList![0]
            self.passDelegate?.passSelectedPlace(data: obj, locationType: type)
            break
        case .viaPoints:
            dataManager.addViaPoint(data: obj)
            self.passDelegate?.passSelectedPlaceForViaPoint(data: dataManager.viaPointsArray, completion: { (isSucess) in
            })
            break
        }
        filteredPlaceList.removeAll()
        self.popView()
    }
    
    func isStopAlreadyAdded(stop: Predictions) -> Bool {
        let stopAddress = stop.description
        var message: String = ""
        var selectedOption = selectedSection
        let selectedPlace = (stopAddress?.components(separatedBy: ",").first ?? "")
        if selectedSection == PlanRideDataSource.ViaPoint {
            selectedOption = "stop"
        }
        //check with source
        
        if let source:[String:Any] = dataManager.routeData[PlanRideDataSource.SourcePlace] as? [String:Any]{
            let name = source["name"] as! String
            
            if name == stopAddress {
                //stop already added show pop up
              message = String(format: NSLocalizedString("keyAlreadySelectedSource", comment: ""), selectedPlace,selectedOption)
                showAlertWithMsg(message: message)
                return true
            }
        }
        //check with destination
        if let destination:[String:Any] = dataManager.routeData[PlanRideDataSource.DestinationPlace] as? [String:Any]{
            let name = destination["name"] as! String
            
            if name == stopAddress {
                //stop already added show pop up
                message = String(format: NSLocalizedString("keyAlreadySelectedSource", comment: ""), selectedPlace,selectedOption)
                showAlertWithMsg(message: message)
                return true
            }
        }
        
        //check with pois
        for poi in dataManager.viaPointsArray {
            
            if poi.name == stopAddress {
                message = String(format: NSLocalizedString("keyAlreadySelectedViaPoint", comment: ""), selectedPlace,selectedOption)
                showAlertWithMsg(message: message)
                return true
            }
        }
        
        return false
    }
    
    
    func showAlertWithMsg(message: String) {
        DispatchQueue.main.async {
            self.dataTblView.isUserInteractionEnabled = true
            self.view.hideToastActivity()
        }
        let alert = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
        let ok = UIAlertAction.init(title: NSLocalizedString("keyOk", comment: ""), style: .default, handler: nil)
        alert.addAction(ok)
        self.presentVC(alert)
    }
}

extension ViaPointsListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredPlaceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: TableViewCells.viaPointCell)
        cell.accessibilityIdentifier = TableViewCells.viaPointCell
        if filteredPlaceList.count > 0{
            if let placeDetailsObj:Predictions = filteredPlaceList[indexPath.row] as? Predictions {
                cell.textLabel?.text = placeDetailsObj.structuredFormatting?.primarytext
                cell.detailTextLabel?.text = placeDetailsObj.structuredFormatting?.secondaryText
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        DispatchQueue.main.async {
            self.dataTblView.isUserInteractionEnabled = false
            self.view.makeToastActivity(.center, isUserInteraction: false)
        }
        
        if selectedSection == PlanRideDataSource.SourcePlace {
            formDataSourceForSource(forRow: indexPath.row)
        } else if selectedSection == PlanRideDataSource.ViaPoint {
            formDataSourceForViaPoints(forRow: indexPath.row)
            dataManager.selectedDestinationFirstTime = false
        } else {
            formDataSourceForDestination(forRow: indexPath.row)
        }
    }
}

extension ViaPointsListViewController: UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate  {
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    func didPresentSearchController(_ searchController1: UISearchController) {
        searchController1.searchBar.becomeFirstResponder()
        if self.selectedSection != PlanRideDataSource.ViaPoint {
            self.pickAndChooseView.isHidden = true
        }
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            self.pickAndChooseView.isHidden = (searchText == "" && self.selectedSection == PlanRideDataSource.ViaPoint) ? false : true
            self.myLocationSuggestionVw.isHidden = (searchText == "" && self.selectedSection == PlanRideDataSource.SourcePlace && CLLocationManager.locationServicesEnabled()) ? false : true
            filteredPlaceList.removeAll()
            self.findPlaceWithPlaceName(placeName: searchText)
            dataTblView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.pickAndChooseView.isHidden = self.selectedSection == PlanRideDataSource.ViaPoint ? false : true
        self.myLocationSuggestionVw.isHidden = (self.selectedSection == PlanRideDataSource.SourcePlace && CLLocationManager.locationServicesEnabled()) ? false : true
        filteredPlaceList.removeAll()
        dataTblView.reloadData()
    }
    
    func setViewTitle() {
        var titleString = StringConstant.enter
        switch selectedSection {
        case PlanRideDataSource.SourcePlace:
            titleString = StringConstant.STARTING_POINT
            if let selectedSource = dataManager.routeData[PlanRideDataSource.SourcePlace] as?  [String: Any]  {
                var stringList = (selectedSource[StringConstant.NAME] as? String)?.components(separatedBy: delimiter)
                searchController.searchBar.placeholder = (PlanRideTableViewController.sourceAddressSelected) ? stringList![0] : StringConstant.MYLOCATION
                self.myLocationSuggestionVw.isHidden = (self.selectedSection == PlanRideDataSource.SourcePlace && CLLocationManager.locationServicesEnabled()) ? false : true
            }
            break
        case PlanRideDataSource.ViaPoint:
            titleString =  StringConstant.VIAPOINT_TITLE
            break
        case PlanRideDataSource.DestinationPlace:
            titleString =  StringConstant.STARTING_POINT
            if let selectedDestination = dataManager.routeData[PlanRideDataSource.DestinationPlace] as?  [String: Any]  {
                if  var stringList = (selectedDestination[StringConstant.NAME] as? String)?.components(separatedBy: delimiter), stringList.count > 0 {
                searchController.searchBar.placeholder = stringList[0]
                }
            }
            break
        default:
            break
        }
        self.navigationItem.title = titleString
    }
    
    func popView() {
        searchController.searchBar.endEditing(true)
        searchController.isActive = false

        if !dataManager.isDestinationAdded() {
            self.dismiss(animated: false, completion: nil)
        } else {
            if dataManager.selectedDestinationFirstTime{
                self.dismiss(animated: false, completion: nil)
                dataManager.selectedDestinationFirstTime = false
            }
            else{
                self.view.hideToastActivity()
                self.popVC()
            }
        }

    }
    
    func initializeSearchBar() {
        // Initializing with searchResultsController set to nil means that
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.delegate = self
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.showsCancelButton = false
        searchController.searchBar.setShowsCancelButton(false, animated: false)
        searchController.searchBar.restorationIdentifier = "src_planride_searchlocation"
        searchController.searchBar.accessibilityIdentifier = "src_planride_searchlocation"
        dataTblView.tableHeaderView = searchController.searchBar
        if #available(iOS 11, *) {
            // iOS 11 (or newer) Swift code
        } else {
            // iOS 10 or older code
            myLocationViewTopConstraint.constant -= 10
        }
        self.dataTblView.reloadData()
    }
    
}

extension ViaPointsListViewController:POICollectionDelegates {
    func didSelectPOI(poiType: POIButtonType) {
        //
        let addPoisVc = PlanRideInDetail()
        searchController.searchBar.endEditing(true)
        searchController.isActive = false
        self.pickAndChooseView.isHidden = false
        addPoisVc.selectedPoiType = poiType.rawValue
        self.pushVC(addPoisVc)
    }
    
    
}
