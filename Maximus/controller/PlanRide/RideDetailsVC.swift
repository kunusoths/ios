//
//  RideDetailsVC.swift
//  Maximus
//
//  Created by kpit on 03/04/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire

class RideDetailsVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    // MARK: - UI Components
    
    @IBOutlet weak var btnSaveRide: UIButton!
    @IBOutlet weak var tfStartDate: UITextField!
    @IBOutlet weak var tfEndDate: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var tfRideTitle: UITextField!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgRidePicture: UIImageView!
    @IBOutlet weak var lblRideSourceDestination: UILabel!
    @IBOutlet weak var switchMakeRidePrivate: UISwitch!
    @IBOutlet weak var camView: UIView!
    
    // MARK: - Public Variables
    
    let imagePicker = UIImagePickerController()
    var buttonclicked = 1
    var startDate = NSDate()
    var strStartDate: String = ""
    var strEndDate: String = ""
    var routeDetails: RouteList?
    var directionResult:RouteDetails?
    var sourceName = ""
    var destinationName = ""
    var rideType = ""
    var createdRideId:Int?
    var viaPointArray: [Any]?
    var arrayOfSelectedPOI: [POIList]?
    var isEditMode: PlanRideFlowOptions = PlanRideFlowOptions(rawValue: 0)!
    var createdRide: RideCreateResponse?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    let planRideRepo = PlanRideRepository()
    var planRideModel: PlanRideViewModel?
    var rideModel: MyRidesViewModel?
    let rideRepo = RideRepository()
    var recommendedObj: RecommendedResponse?
    let rideDataManager = PlanRideDataManager.shared()    // MARK: - Lifecycle Delegates
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rideModel = MyRidesViewModel(rideRepoDelegate: rideRepo)
        planRideModel = PlanRideViewModel(planRideRepoDelegate: planRideRepo)
        self.navigationItem.title = ScreenTitle.RideDeatils
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:#imageLiteral(resourceName: "Back Arrow White"), style: .plain, target: self, action: #selector(backTapped))
        self.navigationItem.leftBarButtonItem?.accessibilityIdentifier = "btn_ridedetails_back"
        
        let radius = self.imgProfile.frame.width / 2
        self.switchMakeRidePrivate.thumbTintColor = AppTheme.THEME_ORANGE
        self.switchMakeRidePrivate.onTintColor = AppTheme.THEME_ON_TINT
        self.btnSaveRide.backgroundColor = AppTheme.THEME_ORANGE
        if isEditMode.rawValue == PlanRideFlowOptions.EditRide.rawValue {
            self.btnSaveRide.setTitle("UPDATE RIDE", for: .normal)
        } else if isEditMode.rawValue == PlanRideFlowOptions.RecommendedRide.rawValue {
            self.tfRideTitle.text = "Ride to \(destinationName.capitalizedFirst())"
            if let latitude = recommendedObj?.card?.actions?.first?.action_data?.destination?.lat, let longitude = recommendedObj?.card?.actions?.first?.action_data?.destination?.lng, let endLocation = recommendedObj?.card?.visuals?.end_location?.first {
                let url = "https://maps.googleapis.com/maps/api/staticmap?center=\(latitude),\(longitude)&zoom=13&size=800x400&maptype=roadmap&markers=color:0xFE2ABC%7Clabel:\(endLocation)%7C\(latitude),\(longitude)&key=\(KEY_GOOGLE.APIKEY_STAGING_MAPS)"
                self.imgRidePicture.loadImageUsingCacheWithUrlString(urlString: url,indexPath: self.imgRidePicture.tag)
            }
        }
        self.imgProfile.setCornerRadius(radius: radius)
        imgProfile.imageWithUrl(url: BikerDetails.getBikerDetails(context: context)?.profile_image_url ?? "")
        self.navigationItem.leftBarButtonItem?.tintColor = .white

        var startCityName = ""
        let arrWords = sourceName.components(separatedBy: ",")
        startCityName = arrWords[0]
        //Slice Name and get City Name from it
        var endCityName = ""
        let arrWordsdest = destinationName.components(separatedBy: ",")
        endCityName = arrWordsdest[0]
//        rideModel.reloadDataDelegate = self
        lblRideSourceDestination.text = "\(startCityName) to \(endCityName)"
        self.datePickerView.backgroundColor = UIColor .lightGray
        self.datePicker.accessibilityIdentifier = "dtp_ridedetails"
        isEditMode.rawValue == PlanRideFlowOptions.EditRide.rawValue ? setUIForEditRide() : setDefaultStartEndDate(srtDate: datePicker.date, endDate: datePicker.date)
    }
    
    func setUIForEditRide() {
        // for recommended since no ride id in response
        if createdRideId != 0 {
            rideModel?.editRide(rideID: String(createdRideId ?? 0), type: "GetRideDetails", successCallback: {responseData in
                self.reloadData(data: responseData)
            }, failureCallback:{ failureData in
                self.failure(data: failureData)
            })
        }
    }
    
    func setDefaultStartEndDate(srtDate: Date,endDate: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        dateFormat.timeZone = TimeZone.current
        dateFormat.locale = Locale(identifier: "en_US_POSIX")
        startDate = srtDate as NSDate
        tfStartDate.text = dateFormatter.string(from: srtDate)
        strStartDate = dateFormat.string(from: srtDate) as String
        
        tfEndDate.text = dateFormatter.string(from: endDate)
        strEndDate = dateFormat.string(from: endDate) as String
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func backTapped() {
        self.view.endEditing(true)
        self.popVC()
    }
    
    // MARK: - Text Field Delegates
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true
        }
        if (textField.text?.length)! <= 24 {
            return true
        }
        return false
    }
    
    // MARK: - Button Actions
    
    @IBAction func onCameraClick(_ sender: Any) {
        self.view.endEditing(true)
        showCameraOptions()
    }
    
    @IBAction func onDoneButtonClick(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")

        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        dateFormat.timeZone = TimeZone.current
        dateFormat.locale = Locale(identifier: "en_US_POSIX")

        switch buttonclicked {
        case 2:
            startDate = datePicker.date as NSDate
            tfStartDate.text = dateFormatter.string(from: datePicker.date)
            strStartDate = dateFormat.string(from: datePicker.date) as String
            break
            
        case 3:
            tfEndDate.text = dateFormatter.string(from: datePicker.date)
            strEndDate = dateFormat.string(from: datePicker.date) as String
            break
            
        default:
            break
            
        }
        
        datePickerView.isHidden = true
    }
    
    @IBAction func onSaveRideClick(_ sender: Any) {
        
        if (tfRideTitle.text?.isEmpty)!{
            self.view.makeToast(NSLocalizedString("keyEnterRideTitle", comment: ""))
            return
        }
        
        if (tfStartDate.text?.isEmpty)!{
            self.view.makeToast(NSLocalizedString("keyEnterStartDate", comment: ""))
            return
        }
        
        if (tfEndDate.text?.isEmpty)!{
            self.view.makeToast(NSLocalizedString("keyEnterEndDate", comment: ""))
            return
        }
        isEditMode.rawValue == PlanRideFlowOptions.EditRide.rawValue ? checkDataThatIsChanged() : saveRide()
    }
    
    @IBAction func onStartRideClick(_ sender: Any) {
        tfRideTitle.endEditing(true)
        buttonclicked = 2
        let todaysDate = NSDate()
        self.datePicker.minimumDate = todaysDate as Date
        self.datePicker.date =  todaysDate as Date
        self.datePickerView.isHidden = false
        self.tfEndDate.text = ""
    }
    
    @IBAction func onEndRideClicked(_ sender: Any) {
        tfRideTitle.endEditing(true)
        if(tfStartDate.text?.isEmpty)!{
            self.view.makeToast(NSLocalizedString("keyEnterStartDateFirst", comment: "" ))
            return
        }
        buttonclicked = 3
        self.datePicker.minimumDate = (startDate as Date)
        self.datePicker.date =  startDate as Date
        self.datePickerView.isHidden = false
        
    }
    
    // MARK: - Private Methods
    
    func showCameraOptions()  {
        let alertController = UIAlertController(title: nil, message: nil , preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: NSLocalizedString("keyTakePhoto", comment: ""), style: .default){
            (result : UIAlertAction) -> Void in
            self.openCamera()
        }
        let libraryAction = UIAlertAction(title:NSLocalizedString("keyChooseFromGallery", comment: ""), style: .default){
            (result : UIAlertAction) -> Void in
            self.openPhotoLibrary()
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("keyCancel", comment: ""), style: .cancel, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(libraryAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func openCamera(){
        self.imagePicker.sourceType = .camera
        self.imagePicker.allowsEditing = true
        self.imagePicker.delegate = self
        self.present(imagePicker, animated: true)
    }
    
    func openPhotoLibrary() {
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.allowsEditing = true
        self.imagePicker.delegate = self
        self.present(imagePicker, animated: true)
    }
    
    func uploadImage(image:UIImage, rideId:Int)  {
        if let imageData = UIImageJPEGRepresentation(image, 0.2){
         let uploadUrl = "\(RequestURL.URL_BASE.rawValue)/rides/\(rideId)/rideIcon"
            planRideModel?.uploadImage(data: imageData, url: uploadUrl, request: "UploadImage", successCallback: { (data) in
                self.view.hideToastActivity()
                self.pushPlanRideInDetail(rideID: self.createdRideId!)
            }) { (error) in
                self.failure(data: error)
            }
        }
    }
    
    // MARK: - Image Picker Delegate Method
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    
        if let  image = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            print(image.size)
            let croppedImage = info[UIImagePickerControllerEditedImage] as? UIImage
            print(croppedImage ?? "")
            let cropRect = (info[UIImagePickerControllerCropRect]! as AnyObject).cgRectValue
            print(cropRect?.size ?? "")
            let _ = UIImage.cropImage(image: image, toRect: CGRect.init(x: 0, y: 0, w: self.imgRidePicture.bounds.width, h: self.imgRidePicture.bounds.width))
            print(image.size)
            self.imgRidePicture.image = image
            
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - API calls
    
    func saveRide()
    {
       
        var ride = rideDataManager.getSaveRideJson(rideRideTitle: self.tfRideTitle.text ?? "Ride", strStartDate: strStartDate, strEndDate: strEndDate)
        self.view.makeToastActivity(.center ,isUserInteraction:false)
        if let recommendedObj = recommendedObj{
            ride["card_id"] = recommendedObj.card_id
        }
        print("json:\(ride)")
        self.view.makeToastActivity(.center ,isUserInteraction:false)
        isEditMode.rawValue == PlanRideFlowOptions.EditRide.rawValue ? planRideModel?.createRide(parameter: ride, rqstType: .put, request: "CreateRide", successCallback: { (data) in
            self.saveCreateRideData(data: data)
        }) { (error) in
            self.failure(data: error)
            } : planRideModel?.createRide(parameter: ride, rqstType: .post,request: "CreateRide", successCallback: { (data) in
                self.saveCreateRideData(data: data)
            }) { (error) in
                self.failure(data: error)
        }
    }
    
    func formJsonArray(jsonArray: NSMutableArray) -> NSMutableArray{
        var i = 0
        for poi in arrayOfSelectedPOI! {
            print(poi)
            
            let jsonObject: NSMutableDictionary = NSMutableDictionary()
            jsonObject.setValue(poi.poiListResponse.name, forKey: StringConstant.NAME)
            let date = Date()
            jsonObject.setValue(date.dateToStringWithFormatTTTT(), forKey: StringConstant.DATE_ADDED)
            
            
            let dic = poi.poiListResponse.categories
            jsonObject.setValue(dic, forKey: StringConstant.CATEGORIES)
            
            let dic2 = [StringConstant.ALTITUDE : poi.poiListResponse.cllLocation?.altitude, StringConstant.LATITUDE : poi.poiListResponse.cllLocation?.coordinate.latitude, StringConstant.LONGITUDE : poi.poiListResponse.cllLocation?.coordinate.longitude]
            jsonObject.setValue(dic2, forKey: StringConstant.LOCATION)
            
            if let poiTypes = poi.poiListResponse.types{
                jsonObject.setValue(poiTypes, forKey:StringConstant.TYPES )
            }else{
                let poitype = [StringConstant.PLACES_OF_INTEREST]
                jsonObject.setValue(poitype, forKey:StringConstant.TYPES )
            }
            
            jsonObject.setValue(poi.poiListResponse.poiId, forKey: StringConstant.POI_ID)
            if let db_id = poi.poiListResponse.wavePoint_id{
                jsonObject.setValue(db_id, forKey: StringConstant.ID)
            }
            
            jsonObject.setValue(i+1, forKey: StringConstant.ROUTE_ORDER)
            jsonArray.add(jsonObject)
            i = i+1
        }
        return jsonArray
    }
    
    func checkDataThatIsChanged() {
        var directionResult: String = ""
        if let result = self.directionResult {
            directionResult = result.toJSONString()!
        }
        if isEditMode.rawValue == PlanRideFlowOptions.EditRide.rawValue {
            if !self.compareDate(firstdate: startDate as Date, secondDate: NSDate() as Date, unit: .day) {
                self.view.makeToast("Start date cannot be a past date.")
                return
            }
        }
        var updatedRide: [String: Any] = [StringConstant.start_date : strStartDate,
        StringConstant.FINISH_DATE : strEndDate]
        //date changed by default
        if createdRide?.title != tfRideTitle.text {
            //title changed
            updatedRide[StringConstant.TITLE] = tfRideTitle.text
        }
        if createdRide?.start_location?.latlong?.lat != routeDetails?.legs?[0].startLocation?.lat {
            //source changed
            let sourceLatLong = [StringConstant.LATITUDE : routeDetails?.legs?[0].startLocation?.lat,
                                 StringConstant.LONGITUDE : routeDetails?.legs?[0].startLocation?.lng]
            
            let sourceDic = [StringConstant.LatLong : sourceLatLong,
                             StringConstant.NAME: sourceName] as [String : Any]
            updatedRide[StringConstant.START_LOCATION] = sourceDic
            if directionResult != "" {
                updatedRide[StringConstant.ROUTE] = directionResult
            }
        }
        if createdRide?.end_location?.latlong?.lat != routeDetails?.legs?.last?.endLocation?.lat {
            //destination changed
            let destinationLatLong = [StringConstant.LATITUDE : routeDetails?.legs?.last?.endLocation?.lat,
                                      StringConstant.LONGITUDE :  routeDetails?.legs?.last?.endLocation?.lng]
            
            let destinationDic = [StringConstant.LatLong : destinationLatLong,
                                  StringConstant.NAME : destinationName] as [String : Any]
            updatedRide[StringConstant.END_LOCATION] = destinationDic
            if directionResult != "" {
                updatedRide[StringConstant.ROUTE] = directionResult
            }
        }
        var viapointArray: NSMutableArray = NSMutableArray()
        viapointArray = formJsonArray(jsonArray: viapointArray)
        var pastWaypoint = createdRide?.waypoints?.filter({ (($0 as! [String: Any])["types"]) as! [String] != ["START"]})
        pastWaypoint = pastWaypoint?.filter({ (($0 as! [String: Any])["types"]) as! [String] != ["DESTINATION"]})
        if let count = pastWaypoint?.count, count > 0, viapointArray.count != (pastWaypoint?.count)! {
            //viapoint list changed
            updatedRide[StringConstant.VIAPOINTS] = viapointArray
            if directionResult != "" {
                updatedRide[StringConstant.ROUTE] = directionResult
            }
        } else if (createdRide?.distance != getTotalDistanceOfCurrentRide() && updatedRide[StringConstant.START_LOCATION] == nil && updatedRide[StringConstant.END_LOCATION] == nil) {
             updatedRide[StringConstant.VIAPOINTS] = viapointArray
            if directionResult != "" {
                updatedRide[StringConstant.ROUTE] = directionResult
            }
        }else {
            var counter: Int = 0
            if viapointArray.count > 0 {
                for i in 0..<viapointArray.count {
                    if let count = createdRide?.waypoints?.count {
                    for j in 0..<count {
                        let newDict: [String: Any] = viapointArray[i] as! [String: Any]
                        let dict: [String: Any] = createdRide?.waypoints![j] as! [String : Any]
                        
                        if ((dict[StringConstant.NAME] as? String) == (newDict[StringConstant.NAME] as? String)) && ((dict[StringConstant.ROUTE_ORDER] as? Int) == (newDict[StringConstant.ROUTE_ORDER] as? Int)) {
                            //via point changed
                            counter = counter + 1
                        }
                    }
                    }
                }
            }
            if counter < viapointArray.count {
                updatedRide[StringConstant.VIAPOINTS] = viapointArray
                if directionResult != "" {
                    updatedRide[StringConstant.ROUTE] = directionResult
                }
            }
        }

        if isEditMode.rawValue == PlanRideFlowOptions.EditRide.rawValue {
            updatedRide["id"] = createdRideId!
        }
        
        print("json:\(updatedRide)")
        self.view.makeToastActivity(.center ,isUserInteraction:false)
        planRideModel?.createRide(parameter: updatedRide, rqstType: .put, request: "CreateRide", successCallback: { (data) in
            self.saveCreateRideData(data: data)
        }) { (error) in
            self.failure(data: error)
        }
    }
    
    func getTotalDistanceOfCurrentRide() -> Int {
        var totalDistanceInMeters: Int = 0
        if let legs: [Legs] = (self.directionResult?.routeList?.first?.legs) {
            for leg in legs {
                totalDistanceInMeters += (leg.distance?.value)!
            }
        }
        return totalDistanceInMeters
    }
    
    func compareDate(firstdate: Date, secondDate: Date, unit: CFCalendarUnit) -> Bool {
        let order = NSCalendar.current.compare(firstdate, to: secondDate, toGranularity: .day)
        switch order {
        case .orderedAscending:
            return false
        case .orderedSame:
            return true
        case .orderedDescending:
            return true
        }
    }
}

extension RideDetailsVC:ReloadMyRidesDelegate {
    
    func pushPlanRideInDetail(rideID: Int) {
        let vc = HomeContainerVC()
        vc.previousVc = StringConstant.rideDetailsVc
        self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: vc), close: true)
    }
    
    func saveCreateRideData(data: Any) {
        if let dict = data as? [String: Any], dict["RequestType"] as? String == "CreateRide", let rideData = dict["data"] as? RideCreateResponse {
            let rideid = rideData.id
            MyDefaults.setInt(value: rideid!, key: DefaultKeys.KEY_POI_RIDEID)
            createdRideId = rideData.id
            print("Ride Id \(String(describing: rideData.id))")
            planRideModel?.saveRide(rideResponse: rideData, type: "SaveRide", successCallback: { (data) in
                self.rideModel?.getRideMembers(rideID: self.createdRideId!, type: "GetRideMembers", successCallback: {responseData in
                    PlanRideDataManager.destoy()
                    if self.imgRidePicture.image != nil{
                        self.uploadImage(image: self.imgRidePicture.image!, rideId: self.createdRideId!)
                    }else{
                        self.view.hideToastActivity()
                        let vc = HomeContainerVC()
                        vc.previousVc = StringConstant.rideDetailsVc
                        self.pushVC(vc)
                    }
                }, failureCallback: {failureData in
                    self.failure(data: failureData)
                })
            }) { (error) in
                self.failure(data: error)
            }
          //  planRideModel.saveRide(rideResponse: rideData)
        } else if let dict = data as? [String: Any], dict["RequestType"] as? String == "UploadImage" {
            self.view.hideToastActivity()
            self.pushPlanRideInDetail(rideID: createdRideId!)
        } else {
            rideModel?.getRideMembers(rideID: createdRideId!, type: "GetRideMembers", successCallback: {responseData in
                if self.imgRidePicture.image != nil{
                    self.uploadImage(image: self.imgRidePicture.image!, rideId: self.createdRideId!)
                }else{
                    self.view.hideToastActivity()
                    let vc = HomeContainerVC()
                    vc.previousVc = StringConstant.rideDetailsVc
                    self.pushVC(vc)
                }
            }, failureCallback:{ failureData in
                self.failure(data: failureData)
            })
        }
    }
    

    func reloadData(data: Any) {
        if let dict = data as? [String: Any], dict["RequestType"] as? String == "GetRideDetails", let rideData = dict["data"] as? RideCreateResponse {
                self.createdRide = rideData
                DispatchQueue.main.async {
                    self.tfRideTitle.text = rideData.title
                    if let url = rideData.background_imag {
                        self.imgRidePicture.loadImageUsingCacheWithUrlString(urlString: url,indexPath: self.imgRidePicture.tag)
                    }
                    self.setDefaultStartEndDate(srtDate: (rideData.start_date?.getFormatedDate())!, endDate: (rideData.finish_date?.getFormatedDate())!)
                }
        } else {
            if imgRidePicture.image != nil{
                self.uploadImage(image: self.imgRidePicture.image!, rideId: createdRideId!)
            }else{
                self.view.hideToastActivity()
                let vc = HomeContainerVC()
                vc.previousVc = StringConstant.rideDetailsVc
                self.pushVC(vc)
            }
        }
    }
    
    func failure(data: [String : Any]) {
        self.view.hideToastActivity()
        if let errModel:ErrorModel = data["data"] as? ErrorModel {
            print(errModel.message ?? " ")
            self.view.makeToast((errModel.description)!)
        } else if data["description"] as? String == "Image Upload Failed" {
            self.view.makeToast(NSLocalizedString("keyImageUpload", comment: ""))
            self.pushPlanRideInDetail(rideID: createdRideId!)
        } else if let errDict = data["data"] as? [String: Any] {
            if let descriptionString = errDict["description"] as? String {
                self.view.makeToast(descriptionString)
            }
        } else {
            if let descriptionString = data["description"] as? String {
                self.view.makeToast(descriptionString)
            }
        }
    }
}
