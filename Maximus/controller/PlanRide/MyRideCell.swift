//
//  MyRideCell.swift
//  Maximus
//
//  Created by Admin on 14/04/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import ObjectMapper

@objc protocol EditRideDelegate {
    func showDetailsOnCard(row: Int, rideObj: Rides, section:Int)
    func closeOverlayView()
}

class MyRideCell: UITableViewCell {
    @IBOutlet weak var maincontentView: UIView!
    @IBOutlet weak var lblRideFromAndTo: UILabel!
    @IBOutlet weak var lblRideTitle: UILabel!
    @IBOutlet weak var imgViewRide: UIImageView!
    @IBOutlet weak var lblRideDate: UILabel!
    @IBOutlet weak var btnStartRide: UIButton!
    @IBOutlet weak var btnJoinedRiders: UIButton!
    @IBOutlet weak var settingsBtn: UIButton!    
    @IBOutlet weak var adminCollectionView: UICollectionView!
    @IBOutlet weak var baseView: UIView!
    
    @IBOutlet weak var smtIconImgView: UIImageView!
    
    let planRideRepo = PlanRideRepository()
    var planRideModel: PlanRideViewModel?
    var selectedRow: Int?
    weak var delegate: EditRideDelegate?
    var routeJason = [String:Any]()
    var objRideData: Rides? {
        didSet {
            lblRideTitle.text = objRideData?.ride_title
            if let url = objRideData?.background_image {
                self.imgViewRide.loadImageUsingCacheWithUrlString(urlString: url,indexPath: self.imgViewRide.tag)
            }
            //Slice Name and get City Name from it
            var startCityName:String = ""
            if let strName = objRideData?.startLocation_name {
                let arrWords = strName.components(separatedBy: ",")
                startCityName = arrWords[0]
            }
            //Slice Name and get City Name from it
            var endCityName:String = ""
            if let strName = objRideData?.endLocation_name {
                let arrWords = strName.components(separatedBy: ",")
                endCityName = arrWords[0]
            }
            
            var strStartDate:String = ""
            if let dateString:String = objRideData?.start_date {
                let date = dateString.getFormatedDate()
                strStartDate = self.getStringDate(strDateFormat: "dd MMM", date: date)
            }
            
            var strEndDate:String = ""
            if let dateString:String = objRideData?.finish_date {
                let date = dateString.getFormatedDate()
                strEndDate = self.getStringDate(strDateFormat: "dd MMM yyyy", date: date)
            }
            
            if let joinedRiders = objRideData?.joined_users {
                btnJoinedRiders.setTitle("\(joinedRiders)", for: .normal)
            }
            
            if let status = objRideData?.status{
                if status == 1{
                    settingsBtn.isHidden = (objRideData?.biker_status == Int16(RideMemberStatus.STARTED) || objRideData?.biker_status == 1) || (objRideData?.admin_biker_id == String(MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID))) ? true : false
                    self.btnStartRide.isHidden = false
                    self.btnStartRide.setTitle("RIDE IN PROGRESS", for: .normal)
                    self.btnStartRide.titleLabel?.textColor = AppTheme.THEME_ORANGE
                    self.btnStartRide.isUserInteractionEnabled = false
                }else  {
                    self.btnStartRide.isHidden = true
                    settingsBtn.isHidden = false
                }
            }
            
            let strStartToEnd = startCityName+" to "+endCityName
            let strStartDateToEndDate = strStartDate+" to "+strEndDate
            lblRideFromAndTo.text = strStartToEnd
            lblRideDate.text = strStartDateToEndDate
            print("\(strStartToEnd)\(strStartDate)\(strEndDate)")
        }
    }
    //polyline:
//    let url = "https://maps.googleapis.com/maps/api/staticmap?&size=800x400&maptype=roadmap&markers=color:0xFE2ABC%7Clabel:\(endLocation)%7C\(end_lat),\(end_lng)&markers=color:0xFE2ABC%7Clabel:\(startLocation)%7C\(start_lat),\(start_lng)&path=color:0x0000ff%7Cenc:\(polyline)&key=\(KEY_GOOGLE.APIKEY_STAGING_MAPS)"
    
    var recommendedObj: RecommendedResponse? {
        didSet {
            if let latitude = recommendedObj?.card?.actions?.first?.action_data?.destination?.lat, let longitude = recommendedObj?.card?.actions?.first?.action_data?.destination?.lng, let endLocation = recommendedObj?.card?.visuals?.end_location?.first {
                let url = "https://maps.googleapis.com/maps/api/staticmap?center=\(latitude),\(longitude)&zoom=13&size=800x400&maptype=roadmap&key=\(KEY_GOOGLE.APIKEY_STAGING_MAPS)"
                if let rec_type = recommendedObj?.card?.item_type {
                     //check recommendation type
                    if rec_type == "smt-routes" {
                        //show smt logo and load img url
                        self.smtIconImgView.isHidden = false
                        self.smtIconImgView.image = UIImage(named: "rec_smt_logo")
                        if let imageUrl = recommendedObj?.card?.visuals?.image?.first{
                            self.imgViewRide.loadImageUsingCacheWithUrlString(urlString: imageUrl,indexPath: self.imgViewRide.tag)
                        }
                        else{
                            self.imgViewRide.loadImageUsingCacheWithUrlString(urlString: url,indexPath: self.imgViewRide.tag)
                        }
                    }else {
                        self.smtIconImgView.isHidden = true
                        self.imgViewRide.loadImageUsingCacheWithUrlString(urlString: url,indexPath: self.imgViewRide.tag)
                    }
                }
            }
            
            lblRideTitle.text = recommendedObj?.card?.visuals?.title ?? ""
            #if CLOUD_SERVER_STAGING
            lblRideFromAndTo.text = (recommendedObj?.card?.date_published)! + " " + "Score:" + String(recommendedObj?.card?.score ?? 0)
            #endif
            let distance = (recommendedObj?.card?.visuals?.distance?.value)?.rounded()
            lblRideDate.text = "~ \(distance ?? 0) \(recommendedObj?.card?.visuals?.distance?.unit ?? "")"
            lblRideDate.textAlignment = .right
            btnStartRide.isHidden = true
            settingsBtn.isHidden = true
            btnJoinedRiders.isHidden = true
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let nib = UINib (nibName:"MyRidesCollectionViewCell", bundle: nil)
        self.adminCollectionView.register(nib, forCellWithReuseIdentifier: "MyRidesCollectionViewCell")
        self.contentView.backgroundColor = UIColor.white
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func getStringDate(strDateFormat:String ,date:Date)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = strDateFormat
        return dateFormatter.string(from: date)
    }
    
    
    @IBAction func onClickSettingsButton(_ sender: UIButton) {
        if let status = objRideData?.status {
            if status == 1 {
                self.adminCollectionView.isHidden = status == RideMemberStatus.INVITED ? false : true
                self.baseView.isHidden = true
                self.removeBlurEffect()
                self.delegate?.showDetailsOnCard(row: self.tag, rideObj: objRideData!, section:0)
            } else {
                self.delegate?.showDetailsOnCard(row: self.tag, rideObj: objRideData!, section:1)
                self.adminCollectionView.isHidden = false
                self.baseView.isHidden = false
                self.applyBlurEffect(view: self.baseView)
            }
        }
    }
    
    @IBAction func closeOverlayView(_ sender: UIButton) {
        self.baseView.isHidden = true
        self.adminCollectionView.isHidden = true
        removeBlurEffect()
        self.delegate?.closeOverlayView()
    }
    
    func applyBlurEffect(view: UIView) {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        var frame = view.frame
        frame.origin.x = self.maincontentView.frame.origin.x
        frame.origin.y = self.maincontentView.frame.origin.y
        blurEffectView.frame = frame
        blurEffectView.sizeToFit()
        self.addSubview(blurEffectView)
        view.backgroundColor = .clear
        blurEffectView.contentView.addSubview(view)
    }
    
    func removeBlurEffect() {
        for subview in self.subviews {
            if subview is UIVisualEffectView {
                subview.removeFromSuperview()
                self.addSubview(baseView)
            }
        }
    }
    
    func formRouteJsonObj() -> [String: Any] {
        var startObj: [String:Any] = [:]
        startObj[StringConstant.NAME] = recommendedObj?.card?.actions?.first?.action_data?.source?.place_name
        var latLong:[String: Any] = [:]
        latLong[StringConstant.LATITUDE] = recommendedObj?.card?.actions?.first?.action_data?.source?.lat
        latLong[StringConstant.LONGITUDE] = recommendedObj?.card?.actions?.first?.action_data?.source?.lng
        startObj[StringConstant.LatLong] = latLong
        routeJason[StringConstant.SOURCE_DETAILS] = startObj
        //    planRideVC.autocompleteSearchitems[PlanRideDataSource.SourcePlace] = startObj
        var destObj: [String:Any] = [:]
        destObj[StringConstant.NAME] = recommendedObj?.card?.actions?.first?.action_data?.destination?.place_name
        latLong[StringConstant.LATITUDE] = recommendedObj?.card?.actions?.first?.action_data?.destination?.lat
        latLong[StringConstant.LONGITUDE] = recommendedObj?.card?.actions?.first?.action_data?.destination?.lng
        destObj[StringConstant.LatLong] = latLong
        routeJason[StringConstant.DESTINATION_DETAILS] = destObj
        return routeJason
    }
    
}
