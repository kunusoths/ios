//
//  AdhocQuickRideHandler.swift
//  Maximus
//
//  Created by APPLE on 22/10/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import Foundation

class AdhocQuickRideHandler: NSObject {
    let journeyManager = JourneyManager.sharedInstance
    static let adhocQuickRideHandler = AdhocQuickRideHandler()
    
    func checkIfAnyRideInProgress() -> Bool {
        if journeyManager.isAnyRideInProgress() {
            return true
        }
        return false
    }
    
    func checkIfRideCanBeStarted() -> Bool {
        
        return false
    }
}
