//
//  SourceDestinationTableViewCell.swift
//  Maximus
//
//  Created by Isha Ramdasi on 13/12/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//

import UIKit
@objc protocol HeaderTableViewCellDelegate {
    func didSelectUserHeaderTableViewCell(Selected: Int, UserHeader: SourceDestinationTableViewCell)
}

@objc protocol FooterTableViewCellDelegate {
    func didSelectUserFooterTableViewCell(Selected: Int, UserHeader: SourceDestinationTableViewCell)
}

class SourceDestinationTableViewCell: UITableViewHeaderFooterView {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var textLbl: UILabel!
    
    @IBOutlet weak var buttonTap: UIButton!
    weak var headerDelegate: HeaderTableViewCellDelegate?
    weak var footerDelegate: FooterTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if Constants.DeviceType.IS_IPHONE_5{
            self.buttonTap.frame.size = CGSize(width: 238.0, height: 40.0)
            self.textLbl.frame.size = CGSize(width: 180.0, height: 21.0)
        }
        self.buttonTap.addBorder(width: 2, color: .lightGray)
        self.buttonTap.layer.cornerRadius = 5
    }

    
   
    
   @IBAction func selectedHeader(sender: Any) {
        if self.tag == 0 {
            headerDelegate?.didSelectUserHeaderTableViewCell(Selected: 0, UserHeader: self)
        } else {
            footerDelegate?.didSelectUserFooterTableViewCell(Selected: 1, UserHeader: self)
        }
    }
}

