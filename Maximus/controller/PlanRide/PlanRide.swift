//
//  PlanRide.swift
//  Maximus
//
//  Created by Sriram G on 27/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import CoreLocation
import ObjectMapper
import GoogleMaps
import GooglePlaces
import Alamofire

enum LocationType {
    case source, destination, viaPoints
}

enum PlanMode {
    case normal, edit, clone, none
}

let buttonRadius = 20.0
var initialHeight:CGFloat = 50
let rowHeight: CGFloat = 45
let btnHeight: CGFloat = 10
let offset: CGFloat = 70
let heightConstant: CGFloat = CGFloat((rowHeight * 2) + btnHeight + offset)
let topOffset:CGFloat = 40.0
let bottomOffset:CGFloat = 90.0

class PlanRide: UIViewController,UITextFieldDelegate, GMSMapViewDelegate
{
    //MARK:- Properties
    @IBOutlet weak var routeView: UIView!
    
    @IBOutlet weak var btnUpdateRide: UIButton!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet var routeTableView:PlanRideTableViewController!

    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionViewRoute: PlanRideCollectionView!
    @IBOutlet weak var mapViewPlan: GMSMapView!
    @IBOutlet weak var btnRouteNext: UIButton!
    @IBOutlet weak var recenterBtn: UIButton!
    
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var btnRideNow: UIButton!
    @IBOutlet weak var btnAddStop: UIButton!
    @IBOutlet weak var btnView: UIView!
    
    let bikeMarker = GMSMarker()
    let currentLocationMarker = GMSMarker()
    let cellIdentifier = "RouteCell"
    let cellReuseIdentifier = "LocationListCellTableViewCell"
    
    let bikerID = MyDefaults.getInt (key: DefaultKeys.KEY_BIKERID)
    var selectedCollectionRow = 0
    var routeJason = [String:Any]()
 //   var autocompleteSearchitems:[String: Any] = [:]
    var planRideBounds: GMSCoordinateBounds?
    var markerPosition = CLLocationCoordinate2D()
    var passedYourLocationAsSource:Bool = false
    
    var routeInfoItems = [String:Any]()
    var directionData:RouteDetails?
    var sourceAddress: String?
    var mapTasks: GooglePlacesService!
    var distance: String?
    var isListExpanded = false
    var planMode: PlanRideFlowOptions = PlanRideFlowOptions(rawValue: 0)!
    var rideID = 0
    var routeList: RouteList?
    var isClickedForEditRide = false
    let planRideRepo = PlanRideRepository()
    var planRideModel: PlanRideViewModel?
    var recommendedObj: RecommendedResponse?
    var progressView: UIProgressView?
    var timer: Timer?
    var zoomLevel: Float = 10.0
    let rideRepo = RideRepository()
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    var bikerDetails:BikerDetails!
    let rideDataManager = PlanRideDataManager.shared()
    var doUpdatePolyline = false
    var locationViewModel:LocationServicesViewModel?
    let repo = LocationRepository()
    let sinkManager = SinkManager.sharedInstance
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        getDataSource()
        registerCells()
        self.setNavigationBar()
        self.bikerDetails = BikerDetails.getBikerDetails(context: context)
        self.mapViewPlan.delegate = self
        collectionViewRoute.loadRoutesDelegate  = self
        MapTheme.shared.setTheme(map: mapViewPlan)///---Set Google map theme color
        self.view.backgroundColor = AppTheme.NAVBAR_COLOR
        self.mapTasks = GooglePlacesService()
        self.locationViewModel = LocationServicesViewModel(locationRepo: repo)

        ViaPointsListViewController.passYourLocationDelegate = self
        planRideModel = PlanRideViewModel(planRideRepoDelegate: planRideRepo)
        if Constants.BLE_LOGS {
            if !MyDefaults.getBoolData(key: "LogFileCreated") {
                FCMFileUploadManager.fcmManager.createBluetoothLogsFile(riderID: rideID)
            }
            FCMFileUploadManager.fcmManager.uploadBleLogs()
        }
        if !AdhocQuickRideHandler.adhocQuickRideHandler.checkIfAnyRideInProgress() {
            //Before Start check CM is registered or not
            let isCMRegisterd = MyDefaults.getBoolData(key: DefaultKeys.KEY_CMREGISTER)//-----Get CM Registration Status
            if isCMRegisterd {
                self.btnRideNow.addLongPressGesture(target: self, action: #selector(self.longTap))
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        
        self.btnAddStop.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.5).cgColor
        self.collectionViewRoute.clipsToBounds = false
        self.collectionViewRoute.layer.masksToBounds = false
        self.collectionViewRoute.layer.shadowColor = UIColor(red:0.73, green:0.73, blue:0.73, alpha:0.5).cgColor
        self.recenterBtn.layer.shadowColor = UIColor(red:0.05, green:0.05, blue:0.05, alpha:0.5).cgColor
        self.btnRouteNext.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.5).cgColor
        self.btnRideNow.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.5).cgColor
        
        if passedYourLocationAsSource && CLLocationManager.locationServicesEnabled() {
            self.view.makeToastActivity(.center, isUserInteraction: false)
            passedYourLocationAsSource = false
        }
    
        isClickedForEditRide = false
        btnRouteNext.isUserInteractionEnabled = true
        routeTableView.isUserInteractionEnabled = true
        addControls()
        self.navigationController?.topViewController?.navigationItem.title = ScreenTitle.RideOn
        self.navigationItem.leftBarButtonItem?.accessibilityIdentifier = "btn_planride_back"
        NotificationCenter.default.addObserver(self, selector: #selector(userLocation), name: NSNotification.Name(rawValue: StringConstant.LOCATION_SERVICE_ENABLED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getLocationOnMap), name: NSNotification.Name(rawValue: sinkManager.SINK_NOTIFICATION_LOCATION_UPDATE), object: nil)
        getLocationOnMap()

        if doUpdatePolyline {
            self.passSelectedPlaceForViaPoint(data: []) { (done) in
                //
                doUpdatePolyline = false
            }
        }
        self.mapViewPlan.padding = UIEdgeInsetsMake(0, 0, 60, 0)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Helper Methods
    //Get data source for edit mode or normal mode to find and draw route
    func getDataSource() {
        if planMode.rawValue == PlanRideFlowOptions.PlanRide.rawValue {
            self.callPlaceName()
            self.view.makeToastActivity(.center, isUserInteraction: false)
            
            self.updateAddStopButton()
            
            rideDataManager.selectedDestinationFirstTime = false
            
            routeTableView.pushControllerDelegate = self
            //self.routeTableView.didSelectUserFooterTableViewCell(Selected: 0, UserHeader: SourceDestinationTableViewCell())
        } else {
            //self.routeTableView.contentInset = UIEdgeInsetsMake(-10, 0, -10, 0)
            self.sourceAddress = ""
            routeTableView.sourceAddress = ""
            routeJason[StringConstant.SOURCE_DETAILS] = rideDataManager.routeData[PlanRideDataSource.SourcePlace]
            routeJason[StringConstant.DESTINATION_DETAILS] = rideDataManager.routeData[PlanRideDataSource.DestinationPlace]
            let arrayObject: NSMutableArray = NSMutableArray()
            let dataArray = rideDataManager.formJsonArray(jsonArray: arrayObject)
            routeJason[StringConstant.VIA_POINTS_DETAILS] = dataArray
            routeTableView.pushControllerDelegate = self
            //PlanRideHandler.planRideHandlerObj.setViaPointList(dataArray: dataArray)
            if planMode.rawValue == PlanRideFlowOptions.EditRide.rawValue {
                self.btnRideNow.isHidden = true
                self.btnUpdateRide.isHidden = false
                self.btnRouteNext.isHidden = true
                self.updateAddStopButton()
                planRideModel = PlanRideViewModel(planRideRepoDelegate: planRideRepo)
                planRideModel?.getRoutesFromGoogleApi(jsonObj: routeJason, type: "GetRoutesForLatLng", successCallback: { (data) in
                    //
                    self.selectedCollectionRow = 0
                    self.collectionViewRoute.selectedCollectionRow = 0
                    self.saveRouteResponce(data: data)
                    self.updateAddStopButton()
                }, failureCallback: { (err) in
                    //
                    self.failure(data: err)
                })
            } else if planMode.rawValue == PlanRideFlowOptions.RecommendedRide.rawValue {
                self.view.makeToastActivity(.center, isUserInteraction: false)
                if let rec_type = recommendedObj?.card?.item_type,rec_type == "smt-routes" {
                    self.routeTableView.longPressReorderEnabled = false
                    self.btnRouteNext.isHidden = true
                    self.btnAddStop.isHidden = true
                    self.routeTableView.isPlanModeSMTRecommended = true
                    self.collectionViewRoute.isHidden = true
                }
                planRideModel = PlanRideViewModel(planRideRepoDelegate: planRideRepo)
                planRideModel?.getRoutesFromGoogleApi(jsonObj: routeJason, type: "GetRoutesForLatLng", successCallback: { (data) in
                    self.selectedCollectionRow = 0
                    self.collectionViewRoute.selectedCollectionRow = 0
                    self.saveRouteResponce(data: data)
                }, failureCallback: { (error) in
                    self.failure(data: error)
                })
            }
        }
    }
    
    //
    func passSelectedSourceDestination( data: [String: Any]) {
        
        if  rideDataManager.isSourceAdded() && rideDataManager.isDestinationAdded(){
    //        self.btnAddStop.isEnabled = true
        }
        
    }
    
    //Register table and collection view cells
    func registerCells() {
        routeTableView.autocompleteSearchitems = rideDataManager.routeData//self.autocompleteSearchitems
        routeTableView.jsonObject = self.routeJason
        
        collectionViewRoute.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        self.collectionViewHeightConstraint.constant = 0
        self.collectionViewRoute.isHidden = true
    }
    
    //Set Current location on Map
    func getLocationOnMap() {
        if let srcArray = rideDataManager.routeData[PlanRideDataSource.SourcePlace] as? [String:Any],
            let destArray = rideDataManager.routeData[PlanRideDataSource.DestinationPlace] as? [String:Any],
            srcArray.count > 0 && destArray .count > 0 {
        } else {
            if let srcArray = rideDataManager.routeData[PlanRideDataSource.SourcePlace] as? [String:Any],
                let destArray = rideDataManager.routeData[PlanRideDataSource.DestinationPlace] as? [String:Any],
                srcArray.count > 0 && destArray .count == 0 {
                self.onRecentreClick("")
            }else if let srcArray = rideDataManager.routeData[PlanRideDataSource.SourcePlace] as? [Any],
                let destArray = rideDataManager.routeData[PlanRideDataSource.DestinationPlace] as? [String:Any],
                srcArray.count == 0 && destArray .count > 0 {
                if !CLLocationManager.locationServicesEnabled() {
                    self.view.makeToast(NSLocalizedString("keyEnableLocationServices", comment: ""))
                }else{
                    self.plotCurrentLocation()
                }
            } else if let srcArray = rideDataManager.routeData[PlanRideDataSource.SourcePlace] as? [String:Any],
                let destArray = rideDataManager.routeData[PlanRideDataSource.DestinationPlace] as? [String:Any],
                srcArray.count > 0 && destArray .count > 0 {
                self.onRecentreClick("")
            } else{
                self.plotCurrentLocation()
            }
        }
    }
    
    //MARK: Long Tap gesture call
    func longTap(sender : UIGestureRecognizer){
        
        if sender.state == .ended {
            // Call when Release long Tap press button
            if !NetworkCheck.isNetwork() {
                self.view.makeToast(NSLocalizedString("keyNoInternet", comment: ""))
                return
            }
            if(progressView?.progress == 1.0){
                DispatchQueue.main.async {
                    self.progressView?.setProgress(0.0, animated: false)
                }
                let isOtpVerified = self.bikerDetails.is_otp_verified ////-----Get OTP Verification Status
                if isOtpVerified || MyDefaults.getBoolData(key: DefaultKeys.KEY_OTPVERIFIED){
                    
                    if (!JourneyManager.sharedInstance.isAnyRideInProgress()){
                        if rideDataManager.isDestinationAdded() {
                            //quick ride
                            if NetworkCheck.isNetwork() {
                                if !CLLocationManager.locationServicesEnabled(){
                                    self.view.makeToast(NSLocalizedString("keyEnableLocationServices", comment: ""))
                                }
                                self.showLegalAlert()
                            } else {
                                self.view.makeToast(ToastMsgConstant.network)
                            }
                        } else {
                            if !CLLocationManager.locationServicesEnabled(){
                                self.view.makeToast(NSLocalizedString("keyEnableLocationServices", comment: ""))
                            }
                            self.showLegalAlert()
                        }
                    }else{
                        self.view.makeToast("Stop the Current Ride to start a new one.", duration: 0.5, position: .bottom)
                    }
                    
                }else{
                    self.showAlertForOtp()
                }
                
            }
            else{
                DispatchQueue.main.async {
                    self.progressView?.setProgress(0.0, animated: true)
                }
            }
            timer?.invalidate()
        }
        // Call when Begin long Tap press button
        if sender.state == .began {
            
            if !NetworkCheck.isNetwork() {
                 self.view.makeToast(NSLocalizedString("keyNoInternet", comment: ""))
                return
            }
            
            if rideDataManager.isDestinationAdded() && rideDataManager.isSourceAdded() {
                if rideDataManager.routeDetailsObj == nil {
                    self.view.makeToast(NSLocalizedString("keySelectValidRoute", comment: ""))
                    return
                }
                DispatchQueue.main.async {
                    self.progressView?.setProgress(0.0, animated: true)
                }
                
                    
                timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(self.updateProgress), userInfo: nil, repeats: true)
            } else {
                if !rideDataManager.isSourceAdded() {
                    self.view.makeToast(NSLocalizedString("keyEnterStartPoint", comment: ""))
                    return
                }
                
                if !rideDataManager.isDestinationAdded() {
                    self.view.makeToast(NSLocalizedString("keyEnterEndPoint", comment: ""))
                    return
                }
                
            }
        }
    }
    
    func showLegalAlert() {
        let alertController = UIAlertController(title: "Legal Notice", message: "WARNING: Please keep your eyes on the road while riding. Do not use the app or phone while riding.\n Legal Notice:It is your responsibility to observe safe driving practices. You are responsible to secure and use your product in a manner that will not cause accidents, personal injury or any damage to yourself or anyone around you.\nFor more information visit maximuspro.com/legal/termsofservice", preferredStyle: UIAlertControllerStyle.alert)
        let yesAction = UIAlertAction(title: "I Agree", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
           // self.view.makeToastActivity(.center ,isUserInteraction:false)
            ViaPointsListViewController.passYourLocationDelegate = nil
            if let data = self.rideDataManager.routeData[PlanRideDataSource.DestinationPlace] as? [String: Any], data.count > 0 {
                    self.saveQuickRide()
                }
            
        }
        alertController.addAction(yesAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func onAddStopClick(_ sender: Any) {
        if NetworkCheck.isNetwork() {
            let paramToSend = [StringConstant.biker_id_recommendation_id: "\(MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID)) \(RecommendedRideVC.recommendedObj?.card_id ?? "")"]
            Analytics.logCustomEvent(withName: "add_stops_tap", customAttributes: paramToSend)
            if rideDataManager.viaPointsArray.count > 21 {
                self.view.makeToast(NSLocalizedString("keyEnter21Stops", comment: ""))
            } else {
                let vc = ViaPointsListViewController()
                vc.passDelegate = self
                vc.selectedSection = PlanRideDataSource.ViaPoint
                //vc.placeList = autocompleteSearchitems
                self.pushVC(vc)
            }
        } else {
      //      self.view.makeToast(NSLocalizedString("keyNoInternet", comment: ""))
            self.view.makeToast(NSLocalizedString("keyNoInternet", comment: ""), duration: 1, position: .bottom)
        }
    }
    
    //MARK:- Add Progress Bar Progress
    func addControls() {
        // Create Progress View Control
        progressView = UIProgressView(progressViewStyle: UIProgressViewStyle.default)
        //progressView?.center = self.view.center
        progressView?.frame = CGRect(x: 0, y: self.btnRideNow.frame.origin.y - 12, width: self.view.frame.width, height: 10)
        //        progressView?.progressImage = UIImage(named: "Progress Bar")
        progressView?.progressImage = UIImage(named: "Start Green Line")
        progressView!.trackTintColor = UIColor.clear
        progressView?.barHeight = 6
        for subview in view.subviews {
            if subview is UIProgressView {
                progressView?.removeFromSuperview()
            }
        }
       // view.addSubview(progressView!)
        self.btnView.addSubview(progressView!)
    }
    //Update progress bar
    func updateProgress() {
        DispatchQueue.main.async {
            let frameChange: () -> () = {
                self.progressView?.progress = (self.progressView?.progress)! < 1.0 ? (self.progressView?.progress)! + 0.01 : 1.0
            }
            
            if #available(iOS 10.0, *) {
                UIViewPropertyAnimator(duration: 2.0, curve: .easeInOut, animations: frameChange)
                    .startAnimation()
            } else {
                UIView.animate(withDuration: 2.0,
                               delay: 0,
                               options: [UIViewAnimationOptions.curveEaseInOut],
                               animations: frameChange,
                               completion: nil);
            }
        }
    }
    
    func showAlertForOtp(){
        let strTitle = ""
        let alertController = UIAlertController(title: strTitle, message: "Please verify mobile number to start using MaximusPro.", preferredStyle: UIAlertControllerStyle.alert)
        let yesAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("OK")
            MyDefaults.setData(value: fromViewcontroller.HomeVC.rawValue, key: DefaultKeys.KEY_FROMWHERE)
            let navigationController = UINavigationController(rootViewController: PhoneVerificationVC())
            self.presentVC(navigationController)
        }
        alertController.addAction(yesAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func showAlert() {
        //Show Alert for CM registration
        let strTitle = "Your MaximusPro is not registered. To register, please pair Display Module with your phone by scanning the QR code on the Display Module. Make sure Bluetooth of your phone is turned ON"
        let alertController = UIAlertController(title: strTitle, message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        let yesAction = UIAlertAction(title: "YES", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("YES")
            
            let navigationController = UINavigationController(rootViewController: DeviceSetUpVC())
            self.presentVC(navigationController)
        }
        
        let laterAction = UIAlertAction(title: "LATER", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("LATER")
        }
        let buyAction = UIAlertAction(title: "BUY NOW", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("buy Now")
            if let amazonUrl = URL(string: NSLocalizedString("keyAmazonUrl", comment: "")) {
                UIApplication.shared.open(amazonUrl, options: [:]) { (status) in
                    print("Open amazon with status \(status)")
                }
            }
        }
        // buyAction.isEnabled = false
        alertController.addAction(yesAction)
        alertController.addAction(laterAction)
        alertController.addAction(buyAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func saveQuickRide()
    {
        var rideTitle = ""
        if let dest = rideDataManager.routeData[PlanRideDataSource.DestinationPlace] as? [String:Any] {
            if let destination = dest["name"] as? String {
                let delimiter = ","
                var stringList = destination.components(separatedBy: delimiter)
                rideTitle = stringList[0] + " on \(Date().quickRideDateFormat())"
            }
        }
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        dateFormat.timeZone = TimeZone.current
        dateFormat.locale = Locale(identifier: "en_US_POSIX")
        let startDate = Date()
        let strStartDate = dateFormat.string(from: startDate) as String
        let strEndDate = dateFormat.string(from: startDate) as String
        
        var ride = rideDataManager.getSaveRideJson(rideRideTitle: rideTitle, strStartDate: strStartDate, strEndDate: strEndDate)
        if let recommendedObj = recommendedObj{
            ride["card_id"] = recommendedObj.card_id
        }

        print("json:\(ride)")
        self.view.makeToastActivity(.center ,isUserInteraction:false)
        planRideModel?.createRide(parameter: ride, rqstType: .post,request: "CreateRide", successCallback: { (data) in
                self.handleQuickRideSaveResponse(data: data)
            let paramToSend = ["\(StringConstant.biker_id_recommendation_id) Stops" : "\(MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID)) \(RecommendedRideVC.recommendedObj?.card_id ?? "") \(self.rideDataManager.getArrOfSelectedPOIs().count)"]
            Analytics.logCustomEvent(withName: "ride_now_tap", customAttributes: paramToSend)
            if let rec_type = self.recommendedObj?.card?.item_type {
                //check recommendation type
                if rec_type == "smt-routes" {
                    let paramToSend = ["\(MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID))" : "\(RecommendedRideVC.recommendedObj?.card_id ?? "")"]
                    Analytics.logCustomEvent(withName: "ride_now_SMT", customAttributes: paramToSend)
                }
            }

            }) { (error) in
                self.failure(data: error)
        }
    }
    

    func handleQuickRideSaveResponse(data: Any) {
        if let dict = data as? [String: Any], dict["RequestType"] as? String == "CreateRide", let rideData = dict["data"] as? RideCreateResponse {
            let rideid = rideData.id
            MyDefaults.setInt(value: rideid!, key: DefaultKeys.KEY_POI_RIDEID)
            print("Ride Id \(String(describing: rideData.id))")
            planRideModel?.saveRide(rideResponse: rideData, type: "SaveRide", successCallback: { (data) in
                self.view.hideToastActivity()

                JourneyManager.sharedInstance.isQuickRide = true
                let vc:SideMenuVC =  self.slideMenuController()?.leftViewController as! SideMenuVC
                vc.rideDetails = Rides.fetchRideWithID(rideID: (rideData.id?.toString())!, context: self.context)
                vc.selectMenuAtSection(index: 1, andDeslectSectionIndex: 0)
                
            }) { (error) in
                self.failure(data: error)
            }
        }
    }
    
    //Customize Navigation bar
    func setNavigationBar() {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem =  UIBarButtonItem(image:#imageLiteral(resourceName: "Prev Arrow"), style: .plain, target: self, action: #selector(menuTapped))
        self.navigationItem.leftBarButtonItem?.accessibilityIdentifier = "btn_hamb_menu"
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.navigationController!.navigationBar.isTranslucent = false
        self.navigationController?.topViewController?.navigationItem.title = ScreenTitle.Ride
    }
    
    //Plot current location of phone
    func plotCurrentLocation() {
        let isVirtualCm = MyDefaults.getBoolData(key: DefaultKeys.IS_CM_VIRTUAL)
        if !isVirtualCm {
            //cm location
            self.locationViewModel?.getDeviceLocation(bikerID: bikerID, requestType: "GetCurrentDeviceLocation", handler: { (data, errData) in
                if let err = errData {
                    print("Error \(err)")
                    self.locationFailure(data: err)
                }
                
                if let resData = data {
                    print("res data \(resData)")
                    self.locationSuccess(data: resData)
                }
                
            })
        } else {
            if CLLocationManager.locationServicesEnabled() {
                let deviceLocation = DeviceLocationServiece.sharedInstance().getUserCurrentLocation()
                self.plotCurrentLocationOfDevice(latitude: Double(deviceLocation.coordinate.latitude), longitude: Double(deviceLocation.coordinate.longitude))
            }
        }
        
    }
    
    //Hamburger menu tapped
    func menuTapped()
    {
        //if isEditMode.rawValue != PlanRideFlowOptions.PlanRide.rawValue {
            let alert = UIAlertController.init(title: NSLocalizedString("keyEditModePopupTitle", comment: ""), message: NSLocalizedString("keyEditModePopup", comment: ""), preferredStyle: .alert)
            let yes = UIAlertAction.init(title: NSLocalizedString("keyYes", comment: ""), style: .default, handler: { (action) in
                PlanRideDataManager.destoy()
                let views:[UIViewController] = (self.navigationController?.viewControllers)!
                var popvc:HomeContainerVC?
                for view in views {
                    if view.isKind(of: HomeContainerVC.self) {
                        //                planRideVc = (view as! PlanRide)
                        popvc = view as? HomeContainerVC
                    }
                }
                if let popvc = popvc {
                    ViaPointsListViewController.passYourLocationDelegate = nil
                    self.navigationController?.popToViewController(popvc, animated: false)
                }
            })

            let no = UIAlertAction.init(title: NSLocalizedString("keyNo", comment: ""), style: .cancel, handler: { (action) in            })
            alert.addAction(yes)
            alert.addAction(no)
            self.presentVC(alert)
//        } else {
//            self.slideMenuController()?.openLeft()
//        }
    }
    
    // MARK: IBAction handlers

    @IBAction func onUpdateRideClick(_ sender: Any) {
        if NetworkCheck.isNetwork() {
            self.pushSaveRideScreen()
            ViaPointsListViewController.passYourLocationDelegate = nil
        } else {
            self.view.makeToast(ToastMsgConstant.network)
        }
    }
    
    @IBAction func onClickRideNow(_ sender: UIButton) {
        //start adhoc or quick ride
        if (JourneyManager.sharedInstance.isAnyRideInProgress())
        {
            self.view.makeToast("Stop the Current Ride to start a new one.", duration: 0.5,position: .bottom)
            return
        }
        
        if MyDefaults.getBoolData(key: DefaultKeys.KEY_CMREGISTER){
            self.view.makeToast(NSLocalizedString("keyTap&Hold", comment: ""))
        }else{
            self.showAlert()//--Show Alert for CM registration
        }
    }
    
    @IBAction func onRecentreClick(_ sender: Any) {
        mapViewPlan.settings.compassButton = true
        if CLLocationManager.locationServicesEnabled() {
                if (NetworkReachabilityManager()?.isReachable)! {
                    if let isValid = self.planRideBounds?.isValid, isValid {
                        if  let srcArray = rideDataManager.routeData[PlanRideDataSource.SourcePlace] as? [String:Any],
                            let destArray = rideDataManager.routeData[PlanRideDataSource.DestinationPlace] as? [String:Any],
                            srcArray.count > 0 && destArray.count > 0 {
                            let camera = self.mapViewPlan.camera(for: planRideBounds!, insets: UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0))
                            self.mapViewPlan.camera = camera!
                        }else{
                            self.mapViewPlan.animate(with: GMSCameraUpdate.setTarget(self.markerPosition, zoom:10))
                        }
                    } else {
                        self.mapViewPlan.animate(with: GMSCameraUpdate.setTarget(self.markerPosition, zoom:10))
                    }
                } else {
                    if let _ = self.planRideBounds {
                        //route bounds
                        let camera = self.mapViewPlan.camera(for: planRideBounds!, insets: UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0))
                        self.mapViewPlan.camera = camera!
                    } else {
                        //get current location
                        plotCurrentLocation()
                    }
                }
            } else {
//                self.mapViewPlan.setDefaultCameraPosition()
                self.showLocationTurnOnPopup()
        }
    }
    
    @IBAction func expandCollapseRouteView(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            isListExpanded = true
            expandList()
            self.routeTableView.isListExpanded = true
            self.routeTableView.reloadTableData()
            sender.setImage(#imageLiteral(resourceName: "arrowPullUp"), for: .normal)
        } else {
            isListExpanded = false
            collapseList()
            self.routeTableView.isListExpanded = false
            self.routeTableView.reloadTableData()
            sender.setImage(#imageLiteral(resourceName: "arrowPullDown"), for: .normal)
        }
        DispatchQueue.main.async {
            if let _ = self.planRideBounds {
                let camera = self.mapViewPlan.camera(for: self.planRideBounds!, insets: UIEdgeInsets.init(top: topOffset, left: 0, bottom: bottomOffset, right: 0))
                self.mapViewPlan.camera = camera!
                //self.mapViewPlan.animate(with: GMSCameraUpdate.fit(self.planRideBounds!))
            }
        }
        self.showCollectionView()
    }
    
    @objc func expandList() {
        self.heightConstraint.constant = adjustStopListHeight()
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func collapseList() {
        self.heightConstraint.constant = CGFloat(heightConstant)
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
        
    }
    
    @IBAction func btnRouteNextClicked(_ sender: UIButton) {
        if ((((rideDataManager.routeData[PlanRideDataSource.SourcePlace] as?  [String: Any]) == nil)) || self.sourceAddress == "") && (((rideDataManager.routeData[PlanRideDataSource.DestinationPlace] as?  [String: Any]) == nil)) {
            self.view.makeToast(NSLocalizedString("keyEnterSourceAndDestination", comment: ""))
        }
        if ((rideDataManager.routeData[PlanRideDataSource.SourcePlace] as?  [String: Any]) != nil) || self.sourceAddress != ""  {
            
        } else {
            self.view.makeToast(NSLocalizedString("keyEnterStartPoint", comment: ""))
            return
        }
        
        if (((rideDataManager.routeData[PlanRideDataSource.DestinationPlace] as?  [String: Any]) == nil)){
            self.view.makeToast(NSLocalizedString("keyEnterEndPoint", comment: ""))
            return
        }
        
        btnRouteNext.isUserInteractionEnabled = false
        if planMode.rawValue == PlanRideFlowOptions.EditRide.rawValue || planMode.rawValue == PlanRideFlowOptions.RecommendedRide.rawValue {
            self.view.makeToastActivity(.center, isUserInteraction: false)
            isClickedForEditRide = true
            findRoute(json: routeJason)
        } else if planMode.rawValue == PlanRideFlowOptions.PlanRide.rawValue {
            if NetworkCheck.isNetwork() {
               // pushPlanInDetails()
                pushSaveRideScreen()
            } else {
                btnRouteNext.isUserInteractionEnabled = true
                self.view.makeToast(ToastMsgConstant.network)
            }
        }
    }

    
    func pushSaveRideScreen() {
        let arrAllSelectedPOIS = rideDataManager.getArrOfSelectedPOIs() //getArrOfSelectedPOIs()
        let paramToSend = ["\(StringConstant.biker_id_recommendation_id) Stops" : "\(MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID)) \(RecommendedRideVC.recommendedObj?.card_id ?? "") \(rideDataManager.getArrOfSelectedPOIs().count)"]
        Analytics.logCustomEvent(withName: "save_ride_tap", customAttributes: paramToSend)
        if arrAllSelectedPOIS.count > 21 {
            self.view.makeToast("You cannot add more than 21 way points.")
        } else {
            let keyString = String(format: "route:%d", selectedCollectionRow)
            let vc = RideDetailsVC()
            if let jsonObj = routeJason[StringConstant.VIA_POINTS_DETAILS] as? [Any] {
                vc.viaPointArray = jsonObj
            }
            vc.directionResult = rideDataManager.routeDetailsObj
            vc.arrayOfSelectedPOI = arrAllSelectedPOIS
            if let selectedSource = rideDataManager.routeData[PlanRideDataSource.SourcePlace] as?  [String: Any] {
                vc.sourceName = selectedSource[StringConstant.NAME] as? String ?? ""
            }
            if let selectedDestination = rideDataManager.routeData[PlanRideDataSource.DestinationPlace] as?  [String: Any] {
                vc.destinationName = selectedDestination[StringConstant.NAME] as? String ?? ""
            }
            vc.isEditMode = PlanRideFlowOptions(rawValue: self.planMode.rawValue)!
            vc.createdRideId = rideID
            vc.recommendedObj = self.recommendedObj
            if routeInfoItems.count > 0{
                if let selectedRouteObj = routeInfoItems[keyString], let routeObj = selectedRouteObj as? RouteList {
                    vc.routeDetails = routeObj
                }
                self.pushVC(vc)
            } else {
                btnRouteNext.isUserInteractionEnabled = true
                self.view.makeToast(NSLocalizedString("keySelectValidRoute", comment: ""))
                return
            }
        }
    }
    
    // MARK: Private methods
    
    func updateAddStopButton(){
        if rideDataManager.isDestinationAdded() && rideDataManager.isSourceAdded() && rideDataManager.routeDetailsObj != nil && rideDataManager.viaPointsArray.count < 21 {
            self.btnAddStop.isEnabled = true
        } else {
            self.btnAddStop.isEnabled = false
            self.btnAddStop.setImage(UIImage(named: "Add stop inactive"), for: .disabled)
        }
    }
    
    func userLocation() {
        if CLLocationManager.locationServicesEnabled() {
            recenterBtn.setBackgroundImage(#imageLiteral(resourceName: "recenter"), for: .normal)
            if  let srcArray = rideDataManager.routeData[PlanRideDataSource.SourcePlace] as? [String:Any],
                let destArray = rideDataManager.routeData[PlanRideDataSource.DestinationPlace] as? [String:Any],
                srcArray.count > 0 && destArray.count > 0 {
            }  else if let srcArray = rideDataManager.routeData[PlanRideDataSource.SourcePlace] as? [Any], srcArray.count == 0  {
                callPlaceName()
            }
            else {
                if NetworkCheck.isNetwork() {
                    plotCurrentLocation()
                } else {
                    callPlaceName()
                }
            }
        }else {
            recenterBtn.setBackgroundImage(#imageLiteral(resourceName: "recenter_offline"), for: .normal)
        }
    }
    
    //Get current location using google places and reload table with latest location
    fileprivate func getCurrentLocationFromGoogle(_ place: GMSPlace) {
        DispatchQueue.main.async {
            
            //
            
            var latlong = [String: Any]()
            latlong[StringConstant.LATITUDE]     = place.coordinate.latitude
            latlong[StringConstant.LONGITUDE]    = place.coordinate.longitude
            
            var sourceDict:[String: Any] = [:]
            sourceDict[StringConstant.NAME] = place.formattedAddress
            sourceDict[StringConstant.LatLong]   = latlong
            
            self.rideDataManager.addSource(data: sourceDict)
            
            //  self.autocompleteSearchitems[PlanRideDataSource.SourcePlace] = sourceDict
            self.sourceAddress = sourceDict[StringConstant.NAME] as? String
            self.routeTableView.sourceAddress = StringConstant.CHOOSE_SOURCE
            PlanRideTableViewController.sourceAddressSelected = false
            self.routeJason[StringConstant.SOURCE_DETAILS] = sourceDict
            self.routeTableView.autocompleteSearchitems = self.rideDataManager.routeData
            self.routeTableView.jsonObject = self.routeJason
            self.routeTableView.reloadData()
            self.planRideBounds = GMSCoordinateBounds()
            if let srcArray = self.rideDataManager.routeData[PlanRideDataSource.SourcePlace] as? [String: Any], let destArray = self.rideDataManager.routeData[PlanRideDataSource.DestinationPlace] as? [String: Any],srcArray.count > 0 && destArray.count > 0 {
               // self.view.makeToastActivity(.center, isUserInteraction: false)
                self.routeJason[StringConstant.DESTINATION_DETAILS] = self.rideDataManager.routeData[PlanRideDataSource.DestinationPlace]
                self.findRouteByApi(jsonObj: self.routeJason, bounds: self.planRideBounds!, autoCompleteResults: self.rideDataManager.routeData)
            }else{
                self.view.hideToastActivity()
            }
        }
    }
    
    func callPlaceName(){
        
       // defer {
             self.view.hideToastActivity()
     //   }
        
        GMSPlacesClient.shared().currentPlace(callback: { (placeLikelihoodList, error) -> Void in
            if let error = error {
                self.view.hideToastActivity()
                if !CLLocationManager.locationServicesEnabled() {
                    self.recenterBtn.setBackgroundImage(#imageLiteral(resourceName: "recenter_offline"), for: .normal)
                }
                self.mapViewPlan.setDefaultCameraPosition()
                print("Pick Place error: \(error.localizedDescription)")
                self.sourceAddress = ""
                var data: [String: Any] = [:]
                data["sourceName"] = self.sourceAddress
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GetSourceName"), object: nil, userInfo: data)
                if self.passedYourLocationAsSource {
                    self.view.makeToast(NSLocalizedString("keyEnableLocationServices", comment: ""))
                    self.passedYourLocationAsSource = false
                }
                return
            }
            self.recenterBtn.setBackgroundImage(#imageLiteral(resourceName: "recenter"), for: .normal)
            if let placeLikelihoodList = placeLikelihoodList {
                let place = placeLikelihoodList.likelihoods.first?.place
                if let gmsPlace = place//, !self.rideDataManager.isSourceAdded()
                {
                    
                    self.getCurrentLocationFromGoogle(gmsPlace)
                }
            }
        })
    }
    
    func adjustStopListHeight() -> CGFloat {
        let window = UIApplication.shared.keyWindow
        var topPadding: CGFloat = 0
        if #available(iOS 11.0, *) {
            topPadding = (window?.safeAreaInsets.top)!
        } else {
            // Fallback on earlier versions
        }
        if ((CGFloat(self.rideDataManager.viaPointsArray.count) * rowHeight) + heightConstant) > (self.view.frame.height - recenterBtn.frame.size.height - 50) {
            return self.view.frame.height// - topPadding - recenterBtn.frame.size.height
        }
        return rideDataManager.viaPointsArray.count == 0 ? CGFloat(heightConstant): CGFloat((CGFloat(self.rideDataManager.viaPointsArray.count) * rowHeight) + (heightConstant))
    }

    //Find route using source destination and via points
    func findRoute(json: [String:Any]) {
        do{
            let json:NSString = routeJason.jsonEncode()
            if isListExpanded {
                expandList()
            self.routeView.makeToastActivity(.center, isUserInteraction: false)
            }else {
                self.view.makeToastActivity(.center, isUserInteraction: false)
            }
            // you can now cast it with the right type
            print(json)
            if (routeJason[StringConstant.SOURCE_DETAILS] != nil) && (routeJason[StringConstant.DESTINATION_DETAILS] != nil) {
                planRideModel?.getRoutesFromGoogleApi(jsonObj: routeJason, type: "GetRoutesForLatLng", successCallback: { (data) in
                    self.selectedCollectionRow = 0
                    self.collectionViewRoute.selectedCollectionRow = 0
                    self.saveRouteResponce(data: data)
                    self.updateAddStopButton()
                    if self.rideDataManager.viaPointsArray.count >= 21 {
                        self.view.makeToast(NSLocalizedString("keyMaxStopCountReached", comment: ""))
                    }
        
                    if self.isListExpanded {
                        self.routeView.hideToastActivity()
                    }else {
                        self.view.hideToastActivity()
                    }
                    
                }, failureCallback: { (error) in
                    if self.isListExpanded {
                        self.routeView.hideToastActivity()
                    }else {
                        self.view.hideToastActivity()
                    }
                    self.failure(data: error)
                })
                routeTableView.reloadData()
            }
        }
    }
    
    //Error callback
    func errorCall(value:[String : Any]!)
    {
        self.view.hideToastActivity()
        let ErrorModelObj = Mapper<ErrorModel>().map(JSON:value)
        let errorData:[String:Any] = ["data":ErrorModelObj ?? " "]
        print(errorData)
        
    }
    
    //Load polyline for found route
    func loadPolyline(key:String)
    {
        self.mapViewPlan.clear()
        
        let routeObj:RouteList?
        if (routeInfoItems.count == 1 &&  planMode.rawValue != PlanRideFlowOptions.PlanRide.rawValue) {
            routeObj = routeList
        } else {
            routeObj = routeInfoItems[key] as? RouteList
        }
        self.rideDataManager.routeObj = routeObj
        var totalDistanceInMeters: Int = 0
        if let legs:[Legs] = routeObj?.legs {
            if legs.count > 0{
                for leg in legs{
                    totalDistanceInMeters += (leg.distance?.value)!
                    let steps:[Steps] = leg.steps!
                    if steps.count > 0{
                        for step in steps{
                            let polylinePoint:StepPolyline = step.polylinePoints!
                            if let point = polylinePoint.points{
                                print(point)
                                let path: GMSPath = GMSPath(fromEncodedPath: point)!
                                let routePolyline = GMSPolyline(path: path)
                                routePolyline.spans = [GMSStyleSpan(color:AppTheme.THEME_ORANGE)]
                                routePolyline.strokeWidth = 3.0
                                routePolyline.map = self.mapViewPlan
                                routePolyline.geodesic = true
                            }
                        }
                    }
                }
            }
        }
        
        let doubleMeter:Double = Double(totalDistanceInMeters)
        let distanceInKilometers: Int = Int(doubleMeter / 1000)        
        self.distance = String(distanceInKilometers) + " km"
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GetDistance"), object: nil, userInfo: ["Distance":String(distanceInKilometers)])
        self.routeTableView.reloadData()
        planRideBounds = self.mapViewPlan.plotPolyLineWithPath(firstRideLeg: (routeObj?.legs?.first)!, lastRideLeg: (routeObj?.legs?.last)!, totalLegs: (routeObj?.legs)!, poiArray:rideDataManager.viaPointsArray, jsonObject: rideDataManager.routeData, bounds: planRideBounds ?? GMSCoordinateBounds())
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        if let mrkrData: Int = marker.userData as? Int {
            if mrkrData == 0  || mrkrData == 1  {
                if let customInfoWindow = Bundle.main.loadNibNamed(TableViewCells.CustomMarkerView, owner: self, options: nil)?.first as? MarkerCustomView {
                    customInfoWindow.locationLbl.text =  self.distance
                    return customInfoWindow
                }
            }
        }
        return nil
    }
    //Save selected route out of many routes
    func saveSelectedRoute(index:String) {
        if routeInfoItems.count >= 1{
            self.directionData?.routeList?.removeAll()
            self.directionData?.routeList?.append(routeInfoItems[index] as! RouteList)
            rideDataManager.routeObj = routeInfoItems[index] as? RouteList
            
        } else if planMode.rawValue == PlanRideFlowOptions.EditRide.rawValue || planMode.rawValue == PlanRideFlowOptions.RecommendedRide.rawValue {
            self.directionData?.routeList?.removeAll()
            self.directionData?.routeList?.append(routeList!)
             rideDataManager.routeObj = routeList!
        }
    }
    
}

    //MARK:Ridemanager Delegates
    
extension PlanRide: LoadSelectedRoute {
    
    func showCollectionView() {
        if routeInfoItems.count >= 1 {
            loadPolyline(key: "route:\(selectedCollectionRow)")
        }
        if self.routeInfoItems.count > 1 && !isListExpanded {
            self.collectionViewHeightConstraint.constant = initialHeight
            self.collectionViewRoute.isHidden = false
            self.collectionViewRoute.reloadData()
        } else {
            self.collectionViewHeightConstraint.constant = 0
            self.collectionViewRoute.isHidden = true
        }
    }
    
    func loadRoutes(selectedRoute: Int) {
        selectedCollectionRow = selectedRoute
        self.mapViewPlan.clear()
        let keyString = String(format: "route:%d", selectedRoute)
        self.loadPolyline(key:keyString)
        self.saveSelectedRoute(index: keyString)
    }
    

}

extension PlanRide: LoadNextView {
    
    func notConnectedToInternet() {
        self.view.makeToast(NSLocalizedString("keyNoInternet", comment: ""), duration: 1, position: .bottom)

    }
    
    func reorderFinished() {
        if isListExpanded {
            self.routeTableView.makeToastActivity(.center, isUserInteraction: false)
        }else {
            self.view.makeToastActivity(.center, isUserInteraction: false)
        }
    }
    
    func failure(data: [String : Any]) {
        self.view.hideToastActivity()
        btnRouteNext.isUserInteractionEnabled = true
        
        if let errModel:ErrorModel = data[StringConstant.data] as? ErrorModel {
            // no routes found 10006
            if errModel.errorCode == 10006 {
                showNoRoutesAlert()
            } else {
                self.view.makeToast((errModel.description ?? ""))
            }
        } else {
            if let descriptionString = data["description"] as? String {
                if descriptionString == "ZERO_RESULTS" {
                    self.view.makeToast("No route is available for this stop.")
                    //clear map and collection view on zero results
                    self.collectionViewRoute.routeInfoItems = [:]
                    self.routeInfoItems = [:]
                    self.mapViewPlan.clear()
                    self.plotCurrentLocation()
                    self.showCollectionView()
                }
                else if descriptionString == "" || !NetworkCheck.isNetwork(){
                    self.view.makeToast(ToastMsgConstant.network)
                }else {
                    self.view.makeToast(descriptionString)
                }
                
            }
//            else if let errMsg = data["description"] as? String, errMsg == "ZERO_RESULTS" {
//                 showNoRoutesAlert()
//            }
        }
    }
    
    func showNoRoutesAlert() {
        let alertController = UIAlertController(title: nil, message: NSLocalizedString("keySelectValidRoute", comment: "") , preferredStyle: .alert)
        let okAction = UIAlertAction(title: NSLocalizedString("keyOk", comment: ""), style: .cancel, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func pushController(controller: ViaPointsListViewController) {
        self.pushVC(controller)
    }
    
        func findRouteByApi(jsonObj: [String : Any], bounds: GMSCoordinateBounds, autoCompleteResults: [String: Any]) {
//            self.view.makeToastActivity(.center, isUserInteraction: false)
            if isListExpanded {
                expandList()
                self.routeTableView.superview!.makeToastActivity(.center, isUserInteraction: false)
            }else {
                self.view.makeToastActivity(.center, isUserInteraction: false)
            }
            planRideBounds = bounds
            self.routeJason = jsonObj
            self.mapViewPlan.clear()
            self.plotCurrentLocation()
           // self.autocompleteSearchitems = autoCompleteResults
            planRideModel?.getRoutesFromGoogleApi(jsonObj: routeJason, type: "GetRoutesForLatLng", successCallback: { (data) in
                self.selectedCollectionRow = 0
                self.collectionViewRoute.selectedCollectionRow = 0
                if self.isListExpanded {
                    self.routeTableView.superview!.hideToastActivity()
                }else {
                    self.view.hideToastActivity()
                }
                self.saveRouteResponce(data: data)
                self.updateAddStopButton()

            }, failureCallback: { (error) in
                if self.isListExpanded {
                    self.routeTableView.superview!.hideToastActivity()
                }else {
                    self.view.hideToastActivity()
                }
                self.failure(data: error)
            })
        }
    
    func saveRouteResponce(data: Any) {
        let dataDict = data as? [String: Any]
        if let routeDetailsObj:RouteDetails = dataDict![StringConstant.data] as? RouteDetails {
            rideDataManager.routeDetailsObj = routeDetailsObj
            if (routeDetailsObj.routeList?.count)! > 0 {
                self.routeInfoItems.removeAll()
                let count = routeDetailsObj.routeList?.count
                for index in 0..<count!{
                     let routeObj:RouteList =  routeDetailsObj.routeList![index]
                        let keyString = String(format: "route:%d", index)
                        routeInfoItems[keyString] = routeObj
                    
                }
                if count == 1 {
                    routeList = (routeDetailsObj.routeList![0])
                }
            }
            self.directionData = routeDetailsObj
            let keyString = String(format: "route:%d", self.selectedCollectionRow)
            self.saveSelectedRoute(index: keyString)
            //self.selectedCollectionRow = 0
            
            if !isListExpanded {
                showCollectionView()
            }
            self.loadPolyline(key: "route:0")
            self.view.hideToastActivity()
            if isClickedForEditRide {
                self.view.hideToastActivity()
                pushSaveRideScreen()
                isClickedForEditRide = false
            }
           postDataNotification()
        } else if let objdirectionL:SingleRouteDetails = dataDict!["data"] as? SingleRouteDetails {
            if let objroute:RouteDetails = Mapper<RouteDetails>().map(JSONString: (objdirectionL.tbt_route)!) {
                if let route = objroute.routeList?[0] {
                    routeList = route
                    self.saveSelectedRoute(index: "route:0")
                    self.selectedCollectionRow = 0
                    self.routeInfoItems["route:0"] = route
                    self.loadPolyline(key: "route: 0")
                }
            }
            if !isListExpanded {
                showCollectionView()
            }
           postDataNotification()
        } else  if let obj:DeviceDataResponse = dataDict![StringConstant.data] as? DeviceDataResponse {
            if (obj.latest_data?.count)! > 0 {
                let objlatest:LatestData  = (obj.latest_data![0]) as LatestData
                self.plotCurrentLocationOfDevice(latitude: objlatest.latitude!, longitude: objlatest.longitude!)
            }
        }
    }
    
    func postDataNotification() {
        var dataDict:[String: Any] = [:]
        dataDict["RouteInfoItems"] = routeInfoItems
        dataDict["SelectedCollectionRow"] = selectedCollectionRow
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PassSelectedData"), object: nil, userInfo: dataDict)
    }
    
    func plotCurrentLocationOfDevice(latitude: Double, longitude: Double) {
        currentLocationMarker.map = nil
        let originPos = CLLocationCoordinate2DMake(latitude,longitude)
        currentLocationMarker.position = originPos
        currentLocationMarker.title = StringConstant.MYLOCATION
        currentLocationMarker.map = self.mapViewPlan
        let resizedimage = UIImage.scaleTo(image: UIImage(named: "Source")!, w: 20.0, h: 20.0)
        currentLocationMarker.icon = resizedimage
        if rideDataManager.routeObj == nil {
            self.planRideBounds = GMSCoordinateBounds()
            self.planRideBounds = planRideBounds?.includingCoordinate(originPos)
        }
        self.markerPosition = originPos
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 10.0)
        self.mapViewPlan?.animate(to: camera)
    }
    
    func locationSuccess(data: Any) {
        if let deviceResponse = data as? [String: Any], let dict = deviceResponse["data"] as? DeviceDataResponse{
            bikeMarker.map = nil
            bikeMarker.position = CLLocationCoordinate2D(latitude: dict.latest_data?.first?.latitude ?? 0, longitude: dict.latest_data?.first?.longitude ?? 0)
            bikeMarker.map = self.mapViewPlan
            bikeMarker.title = StringConstant.BIKELOCATION
            let resizedimage = UIImage.scaleTo(image: UIImage(named: "Map Pointer")!, w: 40, h: 44)
            bikeMarker.icon = resizedimage
            let deviceLocation = DeviceLocationServiece.sharedInstance().getUserCurrentLocation()
            self.plotCurrentLocationOfDevice(latitude: Double(deviceLocation.coordinate.latitude), longitude: Double(deviceLocation.coordinate.longitude))
        }
    }
    
    func locationFailure(data: [String : Any]) {
        if let description = data["data"] as? String, data["RequestType"] as? String == "GetCurrentDeviceLocation" {
            bikeMarker.map = nil
            self.view.makeToast(description)
            let deviceLocation = DeviceLocationServiece.sharedInstance().getUserCurrentLocation()
            self.plotCurrentLocationOfDevice(latitude: Double(deviceLocation.coordinate.latitude), longitude: Double(deviceLocation.coordinate.longitude))
        }
    }
}


//PassSelectedPlaceDelegate

extension PlanRide: PassSelectedPlaceDelegate,PassYourLocationAsSourceDelegate{
    func passYourLocationAsSource(){
        passedYourLocationAsSource = true
        callPlaceName()
    }
    
    func passSelectedPlace(data: [String : Any], locationType: LocationType) {
        //
        
        
        
    }
    
    func passSelectedPlaceForViaPoint(data: [Any], completion: (Bool) -> Void) {
        //
        routeJason[StringConstant.VIA_POINTS_DETAILS] = PlanRideHandler.planRideHandlerObj.setViaPointList(dataArray: rideDataManager.viaPointsArray)
        self.routeTableView.autocompleteSearchitems = rideDataManager.routeData //self.autocompleteSearchitems
        self.routeTableView.jsonObject = self.routeJason
        DispatchQueue.main.async {
            self.routeTableView.reloadTableData()
        }
        findRoute(json: routeJason)
        
        defer {
            completion(true)
        }
    }
    

}

