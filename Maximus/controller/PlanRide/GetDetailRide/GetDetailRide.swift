//
//  GetDetailRide.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 18/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

extension UISlider {
    var thumbCenterX: CGFloat {
        let trackRect = self.trackRect(forBounds: frame)
        let thumbRect = self.thumbRect(forBounds: bounds, trackRect: trackRect, value: value)
        return thumbRect.midX
    }
}

class GetDetailRide: UIViewController, RideManagerDelegate {
    
    //MARK:- Properties
    @IBOutlet weak var vwSlider: UIView!
    @IBOutlet weak var lblSliderValue: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var imgVwNextArrow: UIImageView!
    @IBOutlet weak var lblMinKM: UILabel!
    @IBOutlet weak var lblMaxKM: UILabel!
    
    var prevSliderValue = 0
    var createdRideId:Int?
    var totalRideDistance:Int?
    let rideManagerObj = RideManager()
    
    var arrWayPoints:[WavePointsList] = []
    var arrStartEnd:[WavePointsList] = []
    
    var wayPointJsonStr:String = ""
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Ham Menu"), style: .plain, target: self, action: #selector(menuTapped))
        self.navigationItem.title = "RIDE DETAILS"
        
        let image = #imageLiteral(resourceName: "Next Arrow").withRenderingMode(.alwaysTemplate)
        imgVwNextArrow.image = image
        imgVwNextArrow.tintColor = UIColor.white
        
        prevSliderValue = Int(slider.value)
        let toKm = CGFloat(totalRideDistance! / 1000)
        self.slider.maximumValue = Float(toKm)
        self.navigationItem.leftBarButtonItem?.tintColor = .white

        self.lblMinKM.text = "\(Int(self.slider.minimumValue)) KM"
        self.lblMaxKM.text = "\(Int(self.slider.maximumValue)) KM"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private Methods
    func menuTapped() {
        self.slideMenuController()?.openLeft()
    }
    
    func pushHomeContainer()  {
        let vc = HomeContainerVC()
        vc.previousVc = StringConstant.rideDetailsVc
        self.pushVC(vc)
    }
    
    
    //MARK:- Action Methods
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        //let changeOrigin:Float = Float(sender.value) - Float(prevSliderValue)
        let sliderPointerViewMidPoint = vwSlider.frame.width/2
        vwSlider.frame.origin.x = slider.thumbCenterX - sliderPointerViewMidPoint//CGFloat(changeOrigin * sliderLenghtPerPoint)
        lblSliderValue.text = "\(currentValue)KM"
        
    }
    
    @IBAction func btnSkipThisStageAction(_ sender: UIButton) {
        
        //        let rideID = MyDefaults.getInt(key: DefaultKeys.KEY_POI_RIDEID)
        //        print("poiId ",rideID)
        //        rideManagerObj.rideManagerDelegate = self
        //        rideManagerObj.getPOI(rideID: rideID)
        
        //        rideManagerObj.getPlannedWaypoints(rideID: rideID) {
        //            response in
        //            if let json = response?.result.value {
        //                print("wayPoints - ",json)
        //                self.wayPointJsonStr = self.jsonToString(json: json as AnyObject)
        //                self.getRideByID(rideID: rideID)
        //            }
        //        }
         self.view.makeToastActivity(.center ,isUserInteraction:false)
        self.pushHomeContainer()
    }
    
    
    
    @IBAction func btnGetDailyPlanAction(_ sender: UIButton) {
        
        let parameter = [ "id" : createdRideId!,
                          "milestone_distance" : self.slider.value * 1000
            ] as [String : Any]
        print("json",parameter)
        rideManagerObj.rideManagerDelegate = self
        self.view.makeToastActivity(.center, isUserInteraction: false)
        rideManagerObj.putDailyTarget(parameter: parameter)
        
    }
    
    func deg2rad(deg:Double) -> Double {
        return (deg * M_PI / 180.0);
    }
    
    func sind(degrees: Double) -> Double {
        return sin(degrees)
    }
    
    func cosd(degrees: Double) -> Double {
        return cos(degrees)
    }
    
    
    func getPOISuccess(data:[String:Any]){
        
        print("POI Response", data)
        
        var db : DBManager?
        do {
            db = try DBManager.open(path: part1DbPath)
        } catch let message {
            print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
            print(message)
        }
        
        
        do {
            try db?.createTable(table: PointsOfInterests.self)
        } catch {
            print(db?.errorMessage ?? "error Message while creating a Table")
        }
        
        let poi_rideID = MyDefaults.getInt(key: DefaultKeys.KEY_POI_RIDEID)
        
        if let obj:GetRidePOIModel = data["data"] as? GetRidePOIModel {
            if (obj.POIList?.count)! > 0 {
                
                if let poiList = obj.POIList{
                    do {
                            
                            try db?.insertIntoTable(table: SQLTableType.POINT_OF_INTEREST, queryString: DBQuery.QUERY_POINT_OF_INTREST_INSERT_INTO, data:poiList)
                            
                        } catch {
                            print(db?.errorMessage ?? "error inserting")
                        }
                    }
                
            }else{
                
            }
        }
        LoadingHandler.shared.stoploading()
        self.pushHomeContainer()
        
    }
   

    func jsonToString(json: AnyObject) -> String{
        
        var convertedString : String = ""
        do {
            
            
            let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            convertedString = String(data: data1, encoding: String.Encoding.utf8)! // the data will be converted to the string
            print("converted string - ",convertedString ) // <-- here is ur string
            
        } catch let myJSONError {
            print(myJSONError)
        }
        return convertedString
    }
    
    
    func getDailyTargetSuccess(data: [String:Any]){
        self.view.hideToastActivity()
        let vc = PlanRideInDetail()
        vc.createdRideId = self.createdRideId
        self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: vc), close: true)
    }
    
    func onFailure(Error:[String:Any]){
        self.view.hideToastActivity()
        let errModel:ErrorModel = Error["data"] as! ErrorModel
        print(errModel.message ?? " ")
        self.view.makeToast((errModel.description)!)
    }
}
