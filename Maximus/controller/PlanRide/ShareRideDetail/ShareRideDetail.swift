//
//  ShareRideDetail.swift
//  Maximus
//
//  Created by Namdev Jagtap on 05/07/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import ObjectMapper
import GoogleMaps
import GooglePlaces

class ShareRideDetail: UIViewController,GMSMapViewDelegate{

    @IBOutlet weak var lblDates: UILabel!
    @IBOutlet weak var lblSourceDestination: UILabel!
    
    @IBOutlet weak var mapView: GMSMapView!
    let rideManagerObj = RideManager()
    var sharedRideId:Int?
    var sourceLocation:CLLocation?
    var rideDetails:RideCreateResponse?

    //Wavepoint list and markers array
    var arrWavePoints:[WavePointsList] = []
    var arrMarkersWavePoints = [GMSMarker]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        MapTheme.shared.setTheme(map: mapView)///---Set Google map theme color

        self.navigationItem.leftBarButtonItem?.tintColor = .white

        mapView.delegate = self
        
        if let rideInfo = rideDetails{
            
            sharedRideId = rideInfo.id
            var startLocation = ""
            var endLocation = ""

            if let source = rideInfo.start_location?.name {
                startLocation = source
            }
            if let destination = rideInfo.end_location?.name {
                endLocation = destination
            }
            
            lblSourceDestination.text = "\(startLocation) to \(endLocation)"
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "E, d MMM yyyy"
            
            let rideStartDate = rideInfo.start_date?.getFormatedDate()
            let strtDt = dateFormatter.string(from: rideStartDate ?? Date())
            
            let endDate = rideInfo.finish_date?.getFormatedDate()
            let endDt = dateFormatter.string(from: endDate ?? Date())
            
            lblDates.text = "\(strtDt) to \(endDt)"
            self.navigationItem.title = rideInfo.title

        }

        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Arrow White"), style: .plain, target: self, action: #selector(backButtonAction))

        // Do any additional setup after loading the view.
        rideManagerObj.rideManagerDelegate = self
        LoadingHandler.shared.startloading(interactionEnable: true, view: self.view)
        rideManagerObj.getRideRouteById(rideID: sharedRideId! ,bikerId:0)
        rideManagerObj.getPlannedWavepoints(rideID: sharedRideId!)

    }
    
    func backButtonAction(){
        self.dismissVC(completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:- Draw Ride Route
    
    func loadPolyline(route:RouteList)
    {
        
        var totalPoints:[String] = []
        let routeObj:RouteList = route
        
        let legs:[Legs] = routeObj.legs!
        
        if legs.count > 0{
            for leg in legs{
                let steps:[Steps] = leg.steps!
                if steps.count > 0{
                    for step in steps{
                        let polylinePoint:StepPolyline = step.polylinePoints!
                        if let point = polylinePoint.points{
                            print(point)
                            
                            let path: GMSPath = GMSPath(fromEncodedPath: point)!
                            let routePolyline = GMSPolyline(path: path)
                            routePolyline.spans = [GMSStyleSpan(color: AppTheme.THEME_ORANGE)]
                            routePolyline.strokeWidth = 5.0
                            routePolyline.map = self.mapView
                            routePolyline.geodesic = true
                            totalPoints.append(point)
                        }
                    }
                }
            }
        }
        
        var points:String = ""
        if(totalPoints.count > 0){
            points = totalPoints.joined(separator: "")//totalPoints as String
        }else{
            points = (routeObj.totalPolylinePoints?.points)!
        }
        self.plotPolyLineWithPath(startLoc: (routeObj.legs?[0].startLocation!)!, endLoc: (routeObj.legs?[0].endLocation!)!, points: points)
    }
    
    func plotPolyLineWithPath(startLoc:StartLocation, endLoc:EndLocation,points:String)
    {
        //self.mapView.clear()
        
        sourceLocation = CLLocation(latitude:(startLoc.lat)! , longitude: (startLoc.lng)!)
        
        //Source Marker
        let sourceMarker = GMSMarker()
        sourceMarker.position = CLLocationCoordinate2DMake((startLoc.lat)!, (startLoc.lng)!)
        sourceMarker.title = "Starting point"
        sourceMarker.map = self.mapView
        sourceMarker.icon = UIImage(named: "From")// #imageLiteral(resourceName: "Source")//GMSMarker.markerImage(with: UIColor.green)
        sourceMarker.tracksViewChanges = true
        
        //Destination Marker
        let destiMarker = GMSMarker()
        destiMarker.position = CLLocationCoordinate2DMake((endLoc.lat)!, (endLoc.lng)!)
        destiMarker.title = "End point"
        destiMarker.map = self.mapView
        destiMarker.icon = #imageLiteral(resourceName: "Destination")//GMSMarker.markerImage(with: UIColor.red)
        destiMarker.tracksViewChanges = true
        
        
        var bounds = GMSCoordinateBounds()
        bounds = bounds.includingCoordinate(sourceMarker.position)
        bounds = bounds.includingCoordinate(destiMarker.position)
        self.mapView.animate(with: GMSCameraUpdate.fit(bounds))
    }
    
    func plotWavePoints(){
        var markerIcon = #imageLiteral(resourceName: "Waypoint")
        if arrWavePoints.count > 0{
            for wavePoint in arrWavePoints{
                
                if((wavePoint.types?.contains(StringConstant.PLACES_OF_INTEREST))! || (wavePoint.types?.contains(StringConstant.WAVEPOINT_TYPE_MILESTONE))!){
                    if let categories = wavePoint.categories {
                        
                        if (categories.contains(StringConstant.POI_CATEGORY_FUEL_PUMPS)){
                            markerIcon = #imageLiteral(resourceName: "Petrol Small Selected")
                        }
                        else if (categories.contains(StringConstant.POI_CATEGORY_RESTAURANTS)){
                            markerIcon = #imageLiteral(resourceName: "Resturant Small Selected")
                        }
                        else if (categories.contains(StringConstant.POI_CATEGORY_LODGING)){
                            markerIcon = #imageLiteral(resourceName: "Hotel Small Selected")
                        }
                    } else{
                        markerIcon = #imageLiteral(resourceName: "Waypoint")
                    }
                    
                    let marker = GMSMarker()
                    marker.position = CLLocationCoordinate2D(latitude: (wavePoint.location?.latitude)!, longitude: (wavePoint.location?.longitude)!)
                    marker.icon = markerIcon
                    //Set Marker Info Data
                    let markerInfo:NSMutableDictionary = [:]
                    markerInfo[StringConstant.POI_TITLE] = wavePoint.name
                    markerInfo[StringConstant.POI_LAT] = (wavePoint.location?.latitude)!
                    markerInfo[StringConstant.POI_LONG] = (wavePoint.location?.longitude)!
                    marker.userData = markerInfo
                    //Info data setting done
                    
                    marker.map = self.mapView
                    arrMarkersWavePoints.append(marker)
                }
            }
            LoadingHandler.shared.stoploading()

            /*if(arrMarkersWavePoints.count>0){
             self.fitAllMarkers(markerList: arrMarkersWavePoints)
             }*/
        }
    }

}


extension ShareRideDetail:RideManagerDelegate
{
    
    //MARK:- RideManager Delegates
    func sendRouteDirectionDetails(directionResult:[String:Any])
    {

        if let objdirectionL:SingleRouteDetails = directionResult["data"] as? SingleRouteDetails {
            if let objroute:RouteDetails = Mapper<RouteDetails>().map(JSONString: (objdirectionL.route)!) {
                if let route = objroute.routeList?[0] {
                    self.loadPolyline(route: route)
                }
            }
        }
    }
    
    func getWavePointsSuccess(data:[String:Any]){
        
        if let obj:PlannedWavePointsModel = data["data"] as? PlannedWavePointsModel {
            if let wavePointList = obj.WavePointsList{
                if (wavePointList.count) > 0 {
                    for wavePoint in wavePointList{
                        // if(wavePoint.types?.contains(StringConstant.WAVEPOINT_TYPE_MILESTONE))!{
                        arrWavePoints.append(wavePoint)
                        //}
                    }
                    self.plotWavePoints()
                }
            }
        }
        
    }

    func onFailure(Error:[String:Any]){
        LoadingHandler.shared.stoploading()
        let errModel:ErrorModel = Error["data"] as! ErrorModel
        print(errModel.message ?? " ")
        self.view.makeToast((errModel.description)!)
    }
}
