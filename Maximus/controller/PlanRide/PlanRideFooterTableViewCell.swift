//
//  PlanRideFooterTableViewCell.swift
//  Maximus
//
//  Created by APPLE on 29/11/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
@objc protocol PlanRideFooterTableViewCellDelegate {
    func didSelectFooterTableViewCell(Selected: Int, UserHeader: PlanRideFooterTableViewCell)
}

class PlanRideFooterTableViewCell: UITableViewHeaderFooterView {
    @IBOutlet weak var textLbl: UILabel!
    
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var Tapbtn: UIButton!
    weak var planRideFooterDelegate: PlanRideFooterTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        if Constants.DeviceType.IS_IPHONE_5{
            self.contentView.frame.size = CGSize(width: 238.0, height: 60.0)
            self.Tapbtn.frame.size = CGSize(width:238.0,height:41)
        }
        self.Tapbtn.addBorder(width: 2, color: .lightGray)
        self.Tapbtn.layer.cornerRadius = 5
        // Initialization code
    }

    
    @IBAction func selectedFooter(sender: Any) {
        planRideFooterDelegate?.didSelectFooterTableViewCell(Selected: 1, UserHeader: self)
        
    }
}
