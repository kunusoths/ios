//
//  PlanRideDataManager.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 15/11/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps

//Singleton class to manage Plan Ride Data



class PlanRideDataManager {
    
    // Shred instance of PlanRideDataManager
    private static var  instance : PlanRideDataManager?
    //Private initializer
    
    class func shared() -> PlanRideDataManager {
        
        guard let mShared = instance else {
            instance = PlanRideDataManager()
            return instance!
        }
        
        return mShared
    }
    
    
    private init() {
        self.addDataSource()
    }
    
    class func destoy() {
    instance = nil
    }
    
    // MARK:-  properties
    
    // route data has kesy ["source" , "destination" , "via points" ]
    var routeData:[String: Any] = [:]
    var routeDetailsObj:RouteDetails?
    var routeObj:RouteList? // = RouteDetails()
    var viaPointsArray: [POIData] = []
    
    var selectedStopArray: [GMSMarker] = []
    
    var arrRoutePOIs:[POIList] = []
    
    var arrAllFuelPumps:[POIList] = []
    var arrAllHotels:[POIList] = []
    var arrAllRestaurents:[POIList] = []
    
    var arrSelectedFuelPumps:[POIList] = []
    var arrSelectedHotels:[POIList] = []
    var arrSelectedRestaurents:[POIList] = []
    var selectedDestinationFirstTime:Bool = false

    
    func addDataSource() {
        routeData[PlanRideDataSource.SourcePlace] = []
        routeData[PlanRideDataSource.DestinationPlace] = []
        routeData[PlanRideDataSource.ViaPoint] = []
    }
    
    // MARK:- Methods
    
    func addSource(data: [String: Any]) {
        routeData[PlanRideDataSource.SourcePlace] = data
    }
    
    func isSourceAdded() -> Bool {
        if  let srcArray = self.routeData[PlanRideDataSource.SourcePlace] as? [String:Any],
            srcArray.count > 0{
            return true
            }
        return false
    }
    
    func isDestinationAdded() -> Bool {
        
        if  let destArray = self.routeData[PlanRideDataSource.DestinationPlace] as? [String:Any],
            destArray.count > 0{
            return true
        }
        return false
    }
    
    // call this function when Source/Destination is changed
    func routeChanged(){
        self.routeDetailsObj = nil
    }
    
    func addDestination(data: [String:Any]) {
        routeData[PlanRideDataSource.DestinationPlace] = data
    }
    
    
    func removeAllSelectedStopsFromAllStops() {
        let allRoutePOIs = self.arrRoutePOIs
        let selectedViaPoint = self.viaPointsArray
        var selectedPOIIndex:Int?
        for selectedPoi in selectedViaPoint {
            
            if let selectedPOIIndex =  allRoutePOIs.index(where: {$0.position.latitude == selectedPoi.location.coordinate.latitude && $0.position.longitude == selectedPoi.location.coordinate.longitude}) {
                self.arrRoutePOIs.remove(at: selectedPOIIndex)
                
            }
            
        }
    }
    
    func getAllPoiMarkers() -> [GMSMarker] {
        
        var allPoiMarkers:[GMSMarker] = []
        
        for poi in arrRoutePOIs {
            let stop = GMSMarker()
            stop.position.latitude = poi.position.latitude
            stop.position.longitude = poi.position.longitude
            
            let markerInfo:NSMutableDictionary = [:]
            markerInfo[StringConstant.POI_TITLE] = poi.poiListResponse.name
            markerInfo[StringConstant.POI_LAT] = poi.position.latitude
            markerInfo[StringConstant.POI_LONG] = poi.position.longitude
            let poiInfo = POIData(name: poi.poiListResponse.name ?? "poi", location: CLLocation(latitude: (poi.poiListResponse.location?.latitude)!, longitude: (poi.poiListResponse.location?.longitude)!), poiInfo: poi, poiType: POIType.ViaPoint , order: 0)
            markerInfo[StringConstant.POI_INFO] = poiInfo
            markerInfo[StringConstant.POI_WHOLE_DATA] = poi
            
            if (poi.poiListResponse.categories?.contains(StringConstant.POI_CATEGORY_RESTAURANTS))! {
                markerInfo[StringConstant.POI_TYPE] = POIType.Restaurent
            }
            if (poi.poiListResponse.categories?.contains(StringConstant.POI_CATEGORY_LODGING))! {
                markerInfo[StringConstant.POI_TYPE] = POIType.Hotel
            }
            if (poi.poiListResponse.categories?.contains(StringConstant.POI_CATEGORY_FUEL_PUMPS))! {
                markerInfo[StringConstant.POI_TYPE] = POIType.Fuel
            }
            
            //categories.contains(StringConstant.POI_CATEGORY_FUEL_PUMPS)
            
            stop.userData = markerInfo
            
            allPoiMarkers.append(stop)
        }
        
        return allPoiMarkers
    }
    
    func getAddedStopMarkers() -> [GMSMarker] {
        
        var addedStops:[GMSMarker] = []
        
        for poi in viaPointsArray {
            let stop = GMSMarker()
            stop.position.latitude = poi.location.coordinate.latitude
            stop.position.longitude = poi.location.coordinate.longitude
            
            let markerInfo:NSMutableDictionary = [:]
            markerInfo[StringConstant.POI_TITLE] = poi.name
            markerInfo[StringConstant.POI_LAT] = poi.location.coordinate.latitude
            markerInfo[StringConstant.POI_LONG] = poi.location.coordinate.longitude
            markerInfo[StringConstant.POI_TYPE] = poi.poiType
            markerInfo[StringConstant.POI_BUISSNESS_PHONE_NUMBER] = ""
            markerInfo[StringConstant.POI_INFO] = poi
            
            stop.userData = markerInfo
            
            addedStops.append(stop)
        }
        
        return addedStops
    }
    
    
    func addViaPoint(data: [String:Any]) {
        
        let name = data["name"]  as! String
        let loc:[String:Any] = data["location"] as! [String:Any]
        
        let lat = loc["latitude"] as! Double
        let lng = loc["longitude"] as! Double
        
        let poiListResp = POIListResponse()
        poiListResp.name = name
        poiListResp.cllLocation = CLLocation(latitude: lat, longitude: lng)
        
        if let poiId:String = data["poi_id"] as? String {
            poiListResp.poiId = poiId
        }
        
        poiListResp.categories = [StringConstant.VIA_POINT]
        let poiList = POIList(position: CLLocationCoordinate2D(latitude: lat, longitude: lng), response: poiListResp)
        
        let viaPoint = POIData(name: name, location: CLLocation(latitude: lat, longitude: lng), poiInfo: poiList, poiType: .ViaPoint, order: 0)
        
        viaPointsArray.append(viaPoint)
        routeData[PlanRideDataSource.ViaPoint] = viaPointsArray
    }
    
    func removeViaPoint(at index: Int){
        self.viaPointsArray.remove(at: index)
         routeData[PlanRideDataSource.ViaPoint] = viaPointsArray
    }
    
    func addPOI(poi : POIList, poiType:POIType)  {
        
        let locDic:[String:Double]?
        
        if let location = poi.poiListResponse.cllLocation {
            locDic = ["latitude" : location.coordinate.latitude ,
                      "longitude" : location.coordinate.longitude]
        } else {
            locDic = ["latitude" : poi.position.latitude ,
                      "longitude" : poi.position.longitude]
        }
        
        //["latitude" : (poi.poiListResponse.cllLocation?.coordinate.latitude)! ,
        //   "longitude" : (poi.poiListResponse.cllLocation?.coordinate.longitude)!]
        
        let _:[String:Any] = ["name" : poi.poiListResponse.name!,
                              "location" : locDic!,
                              "type" : "POI"]
        
        let location = CLLocation(latitude: poi.position.latitude , longitude: poi.position.longitude)
        let poi = POIData(name: poi.poiListResponse.name ?? "", location: location, poiInfo: poi, poiType: poiType, order: 0)
        viaPointsArray.append(poi)
        
        routeData[PlanRideDataSource.ViaPoint] = viaPointsArray
    }
    
    func clearRoureDetails() {
        self.routeData = [:]
        self.viaPointsArray.removeAll()
    }
    
    
    func getSaveRideJson(rideRideTitle: String , strStartDate: String, strEndDate: String) -> [String:Any] {
        
        let sourceLatLong = [StringConstant.LATITUDE : routeObj?.legs?.first?.startLocation?.lat ?? 00,
                             StringConstant.LONGITUDE :routeObj?.legs?.first?.startLocation?.lng ?? 00
        ]
        
        let sourceDic = [StringConstant.LatLong : sourceLatLong,
                         StringConstant.NAME: (routeData[PlanRideDataSource.SourcePlace] as? [String: Any])![StringConstant.NAME]!]
        
        let destinationLatLong = [StringConstant.LATITUDE : routeObj?.legs?.last?.endLocation?.lat ?? 00 ,
                                  StringConstant.LONGITUDE :  routeObj?.legs?.last?.endLocation?.lng ?? 00]
        
        let destinationDic = [StringConstant.LatLong : destinationLatLong,
                              StringConstant.NAME : (routeData[PlanRideDataSource.DestinationPlace] as? [String: Any])![StringConstant.NAME]!]
        

        
        var jsonArrayObject: NSMutableArray = NSMutableArray()
        jsonArrayObject = formJsonArray(jsonArray: jsonArrayObject)
        var ride:[String : Any] = [StringConstant.PUBLIC_RIDE :false,
                    StringConstant.END_LOCATION : destinationDic,
                    StringConstant.START_LOCATION : sourceDic,
                    StringConstant.start_date : strStartDate,
                    StringConstant.FINISH_DATE : strEndDate,
                    StringConstant.VIAPOINTS: jsonArrayObject,
                    StringConstant.TITLE : rideRideTitle,
                    StringConstant.MARKER_POI_TYPE : IntegerConstant.DEFAULT_TYPE,
                    StringConstant.planned : true
            ] as [String : Any]
        
        self.routeDetailsObj?.routeList?.removeAll()
        self.routeDetailsObj?.routeList?.append(self.routeObj!)
        ride[StringConstant.ROUTE] = self.routeDetailsObj?.toJSONString()!
        ride["tbt_route"] = self.routeDetailsObj?.toJSONString()!
        
        return ride
        
    }
    
    
    func formJsonArray(jsonArray: NSMutableArray) -> NSMutableArray {
        var i = 0
        let arrayOfSelectedPOI = getArrOfSelectedPOIs()
        for poi in arrayOfSelectedPOI {
            print(poi)
            
            let jsonObject: NSMutableDictionary = NSMutableDictionary()
            jsonObject.setValue(poi.poiListResponse.name, forKey: StringConstant.NAME)
            let date = Date()
            jsonObject.setValue(date.dateToStringWithFormatTTTT(), forKey: StringConstant.DATE_ADDED)
            
            
            let dic = poi.poiListResponse.categories
            jsonObject.setValue(dic, forKey: StringConstant.CATEGORIES)
            
            let dic2 = [StringConstant.LATITUDE : poi.poiListResponse.cllLocation?.coordinate.latitude, StringConstant.LONGITUDE : poi.poiListResponse.cllLocation?.coordinate.longitude]
            jsonObject.setValue(dic2, forKey: StringConstant.LOCATION)
            
            if let poiTypes = poi.poiListResponse.types{
                jsonObject.setValue(poiTypes, forKey:StringConstant.TYPES )
            }else{
                let poitype = [StringConstant.PLACES_OF_INTEREST]
                jsonObject.setValue(poitype, forKey:StringConstant.TYPES )
            }
            
            jsonObject.setValue(poi.poiListResponse.poiId, forKey: StringConstant.POI_ID)
            if let db_id = poi.poiListResponse.wavePoint_id{
                jsonObject.setValue(db_id, forKey: StringConstant.ID)
            }
            
            jsonObject.setValue(i+1, forKey: StringConstant.ROUTE_ORDER)
            jsonArray.add(jsonObject)
            i = i+1
        }
        return jsonArray
    }
    
    
    func getArrOfSelectedPOIs() -> [POIList] {
        
        //check if via points are add if added remove added pois except via points from aaRouteMakers
        //        let x = self.arrRoutePOIs.
        
        var arrAllSelectedPOIS: [POIList] = []
        for wayPoint in viaPointsArray {
            let poi = POIListResponse()
            poi.name = wayPoint.name
            
            if wayPoint.poiType == .ViaPoint {
                 poi.types = [StringConstant.VIA_POINT]
            }else {
                 poi.types = [StringConstant.PLACES_OF_INTEREST]
            }
            
           
            poi.cllLocation = wayPoint.location
            if let poiInfo = wayPoint.poiInfo {
                poi.categories = poiInfo.poiListResponse.categories
                poi.poiId = poiInfo.poiListResponse.poiId ?? ""
            }
            let poiListObj = POIList(position: wayPoint.location.coordinate, response: poi)
            arrAllSelectedPOIS.append(poiListObj)
        }
        
        return arrAllSelectedPOIS
    }
    
    
    func saveAllPOIs(ridePoiModel : GetRidePOIModel) {
        self.arrRoutePOIs.removeAll()
        self.arrAllFuelPumps.removeAll()
        self.arrAllRestaurents.removeAll()
        self.arrAllHotels.removeAll()
        
        if (ridePoiModel.POIList?.count)! > 0 {
            if let poiList = ridePoiModel.POIList{
                
                for poi in poiList{
                    arrRoutePOIs.append(POIList(position: CLLocationCoordinate2D(latitude: (poi.location?.latitude)!, longitude: (poi.location?.longitude)!), response: poi))
                    
//                    let x = poiList.sorted({$0.})
                    if let categories = poi.categories{
                        if categories.count > 0{
                            if categories.contains(StringConstant.POI_CATEGORY_FUEL_PUMPS){
                                let poiListObj:POIList = POIList(position: CLLocationCoordinate2D(latitude: (poi.location?.latitude)!, longitude: (poi.location?.longitude)!), response: poi)
                                self.arrAllFuelPumps.append(poiListObj)
                            }
                            if categories.contains(StringConstant.POI_CATEGORY_RESTAURANTS){
                                let poiListObj:POIList = POIList(position: CLLocationCoordinate2D(latitude: (poi.location?.latitude)!, longitude: (poi.location?.longitude)!), response: poi)
                                self.arrAllRestaurents.append(poiListObj)
                            }
                            if categories.contains(StringConstant.POI_CATEGORY_LODGING){
                                let poiListObj:POIList = POIList(position: CLLocationCoordinate2D(latitude: (poi.location?.latitude)!, longitude: (poi.location?.longitude)!), response: poi)
                                self.arrAllHotels.append(poiListObj)
                            }
                        }
                    }
                }
            }
            
        }
        
        
    }
    
    
    func getPOIMarkerArray(ofType type: POIType) -> [GMSMarker] {
        
        var markerArray:[GMSMarker] = []
        var pois:[POIList] = []
        var icon:UIImage!
        switch type {
        case .Fuel:
            
            icon = UIImage.scaleTo(image: #imageLiteral(resourceName: "Petrol Small"), w: imageSize, h: imageSize)
            pois = arrRoutePOIs.filter{($0.poiListResponse.categories?.contains(StringConstant.POI_CATEGORY_FUEL_PUMPS))!} //self.arrAllFuelPumps
            break
        
        case .Hotel:
            icon = UIImage.scaleTo(image: #imageLiteral(resourceName: "Hotel Small"), w: imageSize, h: imageSize)
            pois = arrRoutePOIs.filter{($0.poiListResponse.categories?.contains(StringConstant.POI_CATEGORY_LODGING))!} //self.arrAllHotels
            break
        
        case .Restaurent:
           icon =  UIImage.scaleTo(image: #imageLiteral(resourceName: "Restaurants Small"), w: imageSize, h: imageSize)
           pois = arrRoutePOIs.filter{($0.poiListResponse.categories?.contains(StringConstant.POI_CATEGORY_RESTAURANTS))!} //arrAllRestaurents
            break
        default:
            break
        }
        
        for poi in pois{
            
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: (poi.poiListResponse.location?.latitude)!, longitude: (poi.poiListResponse.location?.longitude)!)
            marker.icon = icon!
            
            //Set Marker Info Data
            let markerInfo:NSMutableDictionary = [:]
            markerInfo[StringConstant.POI_TITLE] = poi.poiListResponse.name
            markerInfo[StringConstant.POI_LAT] = (poi.poiListResponse.location?.latitude)!
            markerInfo[StringConstant.POI_LONG] = (poi.poiListResponse.location?.longitude)!
            markerInfo[StringConstant.POI_TYPE] = type
            markerInfo[StringConstant.POI_BUISSNESS_PHONE_NUMBER] = poi.poiListResponse.phone_number
            markerInfo[StringConstant.POI_WHOLE_DATA] = poi
            
            let poiInfo = POIData(name: poi.poiListResponse.name ?? "poi", location: CLLocation(latitude: (poi.poiListResponse.location?.latitude)!, longitude: (poi.poiListResponse.location?.longitude)!), poiInfo: poi, poiType:type , order: 0)
            markerInfo[StringConstant.POI_INFO] = poiInfo
            
            marker.userData = markerInfo
            //Info data setting done
            
            //mapBounds = mapBounds.includingCoordinate(marker.position)
            markerArray.append(marker)
        }
        
        
       return markerArray
    }
    
    
}
