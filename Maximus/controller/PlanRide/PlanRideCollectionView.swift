//
//  PlanRideCollectionView.swift
//  Maximus
//
//  Created by Isha Ramdasi on 11/07/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit

protocol LoadSelectedRoute: class {
    func loadRoutes(selectedRoute: Int)
}

class PlanRideCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource {
    var routeInfoItems = [String:Any]()
    var selectedCollectionRow = 0
    let cellIdentifier = "RouteCell"
    weak var loadRoutesDelegate: LoadSelectedRoute?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        self.dataSource = self
        self.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(passData(_:)), name: NSNotification.Name(rawValue: "PassSelectedData"), object: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        print("\ndid Deselect index \(indexPath.row)")
        if let cell = self.cellForItem(at: indexPath) as? RouteCell {
            cell.backgroundColor = .clear//AppTheme.NAVBAR_COLOR
            cell.btnRouteAction.setTitleColor(UIColor.black, for: .normal)
        }
        
        collectionView.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("\ndid select index \(indexPath.row)")
        if let cell = self.cellForItem(at: NSIndexPath(row: selectedCollectionRow, section: 0) as IndexPath) as? RouteCell {
         cell.backgroundColor = AppTheme.THEME_ORANGE//AppTheme.NAVBAR_COLOR
         cell.btnRouteAction.setTitleColor(AppTheme.THEME_ORANGE, for: .normal)
        }
        selectedCollectionRow = indexPath.row

        self.loadRoutesDelegate?.loadRoutes(selectedRoute: selectedCollectionRow)
        self.reloadData()
    }
    
    @available(iOS 6.0, *)
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! RouteCell
        let title = "Route"
        cell.btnRouteAction.setTitle("\(title)\(indexPath.row+1)", for: .normal)
        print("\n Compare curIndex = Selected \(indexPath.row)  \(selectedCollectionRow)")
        
        if(indexPath.row == selectedCollectionRow){
            cell.backgroundColor = AppTheme.THEME_ORANGE
            cell.btnRouteAction.setTitleColor(AppTheme.THEME_ORANGE, for: .normal)
        }else{
            cell.backgroundColor = UIColor.clear
            cell.btnRouteAction.setTitleColor(UIColor.black, for: .normal)
        }
        return cell
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        self.collectionViewLayout.invalidateLayout()
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return routeInfoItems.count
    }
    
    func passData(_ notification: Notification) {
        if let data = notification.userInfo as? [String: Any] {
            routeInfoItems = data["RouteInfoItems"] as! [String : Any]
            selectedCollectionRow = data["SelectedCollectionRow"] as! Int
        }
    }
}

extension PlanRideCollectionView: UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(routeInfoItems.count >= 3){
            
            return CGSize(width: collectionView.frame.size.width/3, height: collectionView.frame.size.height)
        }else {
            return CGSize(width: collectionView.frame.size.width/CGFloat(routeInfoItems.count), height: collectionView.frame.size.height)
        }
    }
}
