//
//  PlanRideHandler.swift
//  Maximus
//
//  Created by Isha Ramdasi on 15/12/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//

import UIKit
protocol RideHandlerDelegate: class {
    func returnJsonObj(obj: [String: Any], type: LocationType)
    func locationDataFailed(obj: [String: Any])
}
class PlanRideHandler: NSObject {

    static let planRideHandlerObj = PlanRideHandler()
    weak var rideHandlerDelegate: RideHandlerDelegate?
    
    func setViaPointList(dataArray: [POIData])-> [Any] {
        var json: [Any] = []
        var i = 1
        for poi in dataArray {
            var requestObj: [String: Any] = [:]
            requestObj["name"] = poi.name
            requestObj["location"] = ["latitude": poi.location.coordinate.latitude,
                                      "longitude": poi.location.coordinate.longitude]
            
            requestObj["types"] = ["ROUTE_IDENTITY"]
            requestObj[StringConstant.ROUTE_ORDER] = i
            i = i + 1
            json.append(requestObj)
        }
        return json
    }
    
    func findLocationWithPlaceId(placeId:String, type:LocationType, address: String)
    {
        let googleObj = GooglePlacesService()
        googleObj.locationDetails(placeId: placeId, locationType: "", completionHandler: { (response) in
            
            print(response)
            if let value = response.result.value {
                if let theJSONData = try? JSONSerialization.data(
                    withJSONObject: value,
                    options: []) {
                    
                    guard let parsedData = try? JSONSerialization.jsonObject(with: theJSONData) as! [String:Any] else {
                        self.rideHandlerDelegate?.locationDataFailed(obj: [:])
                        return
                    }
                    
                    guard let result = parsedData["result"] as? [String:Any] else {
                        self.rideHandlerDelegate?.locationDataFailed(obj: [:])
                        return
                    }
                    
                    let geometricLocation = result["geometry"] as? [String:Any]
                    let location = geometricLocation?["location"] as? [String:Double]
                    var latlong = [String: Double]()
                    latlong["latitude"] = location?["lat"]
                    latlong["longitude"] = location?["lng"]
                    
                    var jsonObj:[String: Any] = [:]
                    jsonObj["name"] = address
                    if type == .viaPoints {
                        jsonObj["location"] = latlong
                    } else {
                        jsonObj["latlong"] = latlong
                    }
                    jsonObj["poi_id"] = result["place_id"]
                    self.rideHandlerDelegate?.returnJsonObj(obj: jsonObj, type: type)
                }
            }
        })
    }
    
}
