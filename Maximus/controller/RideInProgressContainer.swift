//
//  RideInProgressContainer.swift
//  Maximus
//
//  Created by kpit on 20/04/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class RideInProgressContainer: UIViewController, RidesRepoCallback, ReloadMyRidesDelegate {
    
    //MARK:- Properties
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var viewPlannedTabs: UIView!
    @IBOutlet weak var tabView: UIView!
    
    @IBOutlet weak var btnRideStatus: UIButton!
    @IBOutlet weak var btnRideRoute: UIButton!
    @IBOutlet weak var btnRide: UIButton!
    @IBOutlet weak var btnGang: UIButton!
    
    var rideVC: CurrentRideVC?
    var rideStatusVC: RideStatusVc?
    var rideRouteVC: RideRouteVC?
    var myGangVC: MyGangVC?
    
    var rideDetails:Rides?
    var bottomBorder:UIView?
    let rideObj = RideManager()
    var myRidesModel: MyRidesViewModel?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    let bikerID = MyDefaults.getInt (key: DefaultKeys.KEY_BIKERID)
//    let planRideRepo = PlanRideRepository()
//    var rideModel: PlanRideViewModel?
    let journeyManager = JourneyManager.sharedInstance
    let rideRepo = RideRepository()
    
    private var activeViewController: UIViewController? {
        didSet {
            removeInactiveViewController(inactiveViewController: oldValue)
            updateActiveViewController()
        }
    }
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        myRidesModel = MyRidesViewModel(rideRepoDelegate: rideRepo)
//        rideModel = PlanRideViewModel(planRideRepoDelegate: planRideRepo)
        // Do any additional setup after loading the view.
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Ham Menu"), style: .plain, target: self, action: #selector(menuTapped))
        self.navigationItem.title = "CURRENT RIDE"
        self.btnRideRoute.backgroundColor =  AppTheme.THEME_ORANGE
        self.btnGang.backgroundColor = AppTheme.THEME_ORANGE
        self.btnRide.backgroundColor = AppTheme.THEME_ORANGE
        self.navigationItem.leftBarButtonItem?.tintColor = .white

        self.bottomBorder = UIView()
        self.bottomBorder?.backgroundColor = UIColor.white
        self.tabView.addSubview(bottomBorder!)
        self.navigationController!.navigationBar.isTranslucent = false
        if self.rideDetails == nil {
            if journeyManager.isAnyRideInProgress(){
                self.rideDetails = journeyManager.currentRideDetails()
            }
        }
        
        self.rideVC = CurrentRideVC()
        self.rideVC?.parentContainerVc = self
        
        self.rideStatusVC = RideStatusVc()
        self.rideRouteVC = RideRouteVC()
        self.myGangVC = MyGangVC()
        //       checkIfAnyRideInProgress()
        
        
        if let rideInfo = self.rideDetails{
            self.navigationItem.title = rideInfo.ride_title?.uppercased()
            self.rideVC?.rideDetails = rideDetails
            self.rideStatusVC?.rideDetails = rideDetails
            self.rideRouteVC?.rideDetails = rideDetails
            self.myGangVC?.rideDetails = rideDetails
        }
        
        if let appDel =  UIApplication.shared.delegate as? AppDelegate {
            
            if (appDel.isThroughMyRides) {
                activeViewController = myGangVC
            } else if (appDel.editRideNotification) {
                activeViewController = rideRouteVC
            } else {
                activeViewController = rideVC
            }
        }
        //        activeViewController = (appDel?.isThroughMyRides)! ? myGangVC : (appDel?.editRideNotification ? rideRouteVC : rideVC)
        
        let vc:SideMenuVC =  self.slideMenuController()?.leftViewController as! SideMenuVC
        vc.showSelectedSideMenuIndex(index: 1)

        
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDel =  UIApplication.shared.delegate as? AppDelegate
        if (appDel?.isThroughMyRides)! {
            self.updateTabClick(button: btnGang)
            appDel?.isThroughMyRides = false

        }else if (appDel?.editRideNotification)! {
            self.updateTabClick(button: btnRideRoute)
        } else {
            self.updateTabClick(button: btnRide)
        }
        if self.rideDetails != nil {
            //    self.view.makeToastActivity(.center, isUserInteraction: false)
            getData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func checkIfAnyRideInProgress() {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
        if let ride = Rides.getMyProgressRide(context: context) {
//            myRidesModel.reloadDataDelegate = self
            myRidesModel?.getRide(withRideId: Int(ride.rideID!)!, andBikerId: MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID), request: "GetRideOfBiker", successCallback: { (responseData) in
                //
            }, failureCallback: { (errorData) in
                //
            })

        }
    }

    
    func getData() {
        DispatchQueue.global(qos: .background).async {
            //ride edited or past ride when POI are not cached
            if let rideCard = Rides.fetchRideWithID(rideID: String(self.rideDetails?.rideID?.toInt() ?? 0), context: self.context) {
                if let rideID = self.rideDetails?.rideID?.toInt(), let isPlanned = self.rideDetails?.planned, isPlanned == true {
//                    if rideCard.routeList == nil {
//                        self.view.makeToastActivity(.center, isUserInteraction: false)
//                        self.rideModel.saveRideDelegate = self
//                        self.rideModel.getRideRouteByID(rideId: Int(rideCard.rideID!)!, bikerId: self.bikerID, type: "GetRideRouteById")
//                    }
                    if !MyDefaults.getBoolData(key: "isPOICached") {
                        let rideRepo = RideRepository()
                        rideRepo.repoDelegate = self
                        MyDefaults.setInt(value:rideID , key: DefaultKeys.KEY_POI_RIDEID)
                        rideRepo.cachePOI(rideID: rideID, request: "POICaching", successCallback: {responseData in
                            
                        }, failureCallback:{ failureData in
                            
                        })
                    }
                }
              
            } else {
                self.myRidesModel?.editRide(rideID: String(self.rideDetails?.rideID?.toInt() ?? 0), type: "GetRideDetails", successCallback: {responseData in
                    
                }, failureCallback:{ failureData in
                    
                })
            }
            
        }
    }
    
    func menuTapped() {
        self.slideMenuController()?.openLeft()
    }
    
    public func hideTabView(){
        print("hideTabView")
        let tabHgt = self.viewPlannedTabs.frame.size.height
        self.viewPlannedTabs.frame.size.height = 0
        self.viewPlannedTabs.isHidden =  true
        self.contentView.frame.origin.y -= tabHgt
        self.contentView.frame.size.height += tabHgt
    }
    
    func updateTabClick(button:UIButton){
        self.bottomBorder?.frame = CGRect(x: button.frame.x , y: 49, width: button.frame.width, height: 2)
    }
    
    // MARK : Button Actions
    
    @IBAction func onRideClick(_ sender: Any) {
        activeViewController = self.rideVC
        self.updateTabClick(button: btnRide)
    }
    
    @IBAction func onStatusClick(_ sender: Any) {
//        activeViewController = self.rideStatusVC
//        self.updateTabClick(button: btnRideStatus)
    }
    
    @IBAction func onRouteClick(_ sender: Any) {
        self.rideRouteVC?.rideDetails = rideDetails
        activeViewController = self.rideRouteVC
        self.updateTabClick(button: btnRideRoute)
    }
    
    @IBAction func onMyGangClick(_ sender: Any) {
        self.myGangVC?.rideDetails = rideDetails
        activeViewController = self.myGangVC
        self.updateTabClick(button: btnGang)
    }
    
    // MARK : Handling VCs
    private func removeInactiveViewController(inactiveViewController: UIViewController?) {
        if let inActiveVC = inactiveViewController {
            // call before removing child view controller's view from hierarchy
            inActiveVC.willMove(toParentViewController: nil)
            
            inActiveVC.view.removeFromSuperview()
            
            // call after removing child view controller's view from hierarchy
            inActiveVC.removeFromParentViewController()
        }
    }
    
    private func updateActiveViewController() {
        if let activeVC = activeViewController {
            // call before adding child view controller's view as subview
            addChildViewController(activeVC)
            
            activeVC.view.frame = contentView.bounds
            contentView.addSubview(activeVC.view)
            
            // call before adding child view controller's view as subview
            activeVC.didMove(toParentViewController: self)
        }
    }
    
    func repoSuccessCallback(data: Any) {
        DispatchQueue.main.async {
            self.view.hideToastActivity()
        }
        if let dict = data as? [String: Any] {
            if let _ = dict["data"] as? RideCreateResponse, dict["RequestType"] as? String == "GetRideDetails" {
//                myRidesModel.reloadDataDelegate = self
                myRidesModel?.getRideMembers(rideID: rideDetails?.rideID?.toInt() ?? 0, type: "GetRideMembers", successCallback: {responseData in
                    
                }, failureCallback:{ failureData in
                    
                })
            }
        }
    }
    
    func repoFailureCallback(error: [String : Any]) {
        DispatchQueue.main.async {
            self.view.hideToastActivity()
        }
    }
    
    func saveRideSuccess(data: Any) {
        let dict = data as? [String: Any]
        if let _ = dict!["data"] as? SingleRouteDetails, dict!["RequestType"] as? String == "GetRideRouteById" {
//            myRidesModel.reloadDataDelegate = self
            myRidesModel?.getRideMembers(rideID: rideDetails?.rideID?.toInt() ?? 0, type: "GetRideMembers", successCallback: {responseData in
                
            }, failureCallback:{ failureData in
                
            })
        }
    }
    
    func failure(data: [String : Any]) {
        DispatchQueue.main.async {
            self.view.hideToastActivity()
        }
    }
    
    func reloadData(data: Any) {
        self.view.hideToastActivity()
        if let dict = data as? [String: Any] {
            if dict["RequestType"] as? String == "GetRideMembers" {
                // reload data
            }
        }
    }
}
