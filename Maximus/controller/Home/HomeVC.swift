//
//  HomeVC.swift
//  Maximus
//
//  Created by kpit on 09/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import ObjectMapper
import GoogleMaps
import Alamofire
import CoreData

class HomeVC: UIViewController, HomeViewModelDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet  var homeSearchBar: UISearchBar!
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var btnRideLater: UIButton!
    @IBOutlet weak var recenterBtn: UIButton!
    @IBOutlet weak var rideButtonStackView:UIView!
    
    
    var progressView: UIProgressView?
    var timer: Timer?
    var zoomLevel: Float = 10.0
    var sourceMarker = GMSMarker()
    var destiMarker = GMSMarker()
    let homeViewModel = HomeViewModel()
    var homeMapBounds: GMSCoordinateBounds?
    var bikeMarker = GMSMarker()
    var planRideTableVC = PlanRideTableViewController()
    
    var rideID = Int()
    var rideTitle = ""
    var bikerDetails:BikerDetails!
    
    var bikerID = 0//MyDefaults.getInt (key: DefaultKeys.KEY_BIKERID)
    var rideDetails:RideCreateResponse?
    var emailIDPopupToShow: Bool = false
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    let journeyManager = JourneyManager.sharedInstance
    let sinkManager = SinkManager.sharedInstance
    let repo = LocationRepository()
    var locationViewModel:LocationServicesViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.hideToastActivity()
        // bikerID = (BikerDetails.getBikerDetails(context: viewContxt!)?.biker_id?.toInt())!
        self.bikerDetails = BikerDetails.getBikerDetails(context: context)
        self.bikerID = (bikerDetails.biker_id?.toInt())!
        self.locationViewModel = LocationServicesViewModel(locationRepo: repo)
        
        self.homeSearchBar = UISearchBar()
//        self.homeSearchBar.set
        //self.homeSearchBar.delegate = self
        homeViewModel.delegate = self
       // btnStart.backgroundColor = AppTheme.THEME_ORANGE
        self.mapView.setDefaultCameraPosition()
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        //  self.showStatsData(data: [String:AnyObject]())
        NotificationCenter.default.addObserver(self, selector: #selector(userLocation), name: NSNotification.Name(rawValue: sinkManager.SINK_NOTIFICATION_LOCATION_UPDATE), object: nil)
        
        if (journeyManager.isAnyRideInProgress() && (journeyManager.currentRideDetails().planned)) {
            
        } else {
            if MyDefaults.getBoolData(key: DefaultKeys.KEY_CMREGISTER){
                self.btnStart.addLongPressGesture(target: self, action: #selector(self.longTap))
            }
        }
       
        MapTheme.shared.setTheme(map: mapView)///---Set Google map theme color
        self.mapView.padding = UIEdgeInsetsMake(0, 0, 60, 0)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("*******************viewWillAppear called****************")
        //journeyManager.journeyManagerDelegate = self
       // self.homeSearchBar.setImage( UIImage(named: "Profile"), for: .search , state: .highlighted)
       // UISearchBar.appearance().setImage(UIImage(named: "To"), for: UISearchBarIcon.search, state:.normal)
        if PlanRideDataManager.shared().isDestinationAdded() {
            PlanRideDataManager.shared().selectedDestinationFirstTime = true
            let vc = PlanRide()
            self.pushVC(vc)
        }
        RecommendedRideVC.recommendedObj = nil
        
        
        addObserverOnView()
        self.btnStart.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.5).cgColor
        self.btnRideLater.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.5).cgColor
        self.recenterBtn.layer.shadowColor = UIColor(red:0.05, green:0.05, blue:0.05, alpha:0.5).cgColor

        self.getRideStatus()
        if journeyManager.isAnyRideInProgress(){
            rideID = journeyManager.currentRideId()
            rideTitle = journeyManager.currentRideDetails().ride_title!
            if journeyManager.rideProgressType() == RideTypes.AdHoc{
               // enableStopButton()
            }
        }
        // access view size here
        //Add progress bar
        self.addControls()
        if !CLLocationManager.locationServicesEnabled() {
            recenterBtn.setBackgroundImage(#imageLiteral(resourceName: "recenter_offline"), for: .normal)
        } else {
//            self.mapView.setDefaultCameraPosition()
            recenterBtn.setBackgroundImage(#imageLiteral(resourceName: "recenter"), for: .normal)
        }
    }
    
    func userLocation() {
        if CLLocationManager.locationServicesEnabled() {
            recenterBtn.setBackgroundImage(#imageLiteral(resourceName: "recenter"), for: .normal)
            if !isLocationValid(location: destiMarker.position) {
                getDeviceCurrentLocation()
            }
        } else {
            recenterBtn.setBackgroundImage(#imageLiteral(resourceName: "recenter_offline"), for: .normal)
//            self.mapView.setDefaultCameraPosition()
        }
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        print("*******************viewDidDisappear called****************")
        if journeyManager.isAnyRideInProgress(){
            self.removeObserverOnView()
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationIdentifier.upgardeDmIdentifier), object: nil)
    }
    
    
    
    func getBikerSucess(response: [String : Any]) {
        
    }

    func rideStarted(data: [String : Any]) {
        //
        
        let rideDetails:Rides  = data["rideData"] as! Rides
       // self.rideDetails = rideDetails
        self.rideID = rideDetails.rideID?.toInt() ?? 0
        self.rideTitle = rideDetails.ride_title!
        
        
        //Add Observer to reload data once app comes in foregraound
        self.addObserverOnView()
        self.view.hideToastActivity()
        
        let vc:SideMenuVC =  self.slideMenuController()?.leftViewController as! SideMenuVC
        vc.rideDetails = rideDetails
        vc.selectMenuAtSection(index: 1, andDeslectSectionIndex: 0)
        enableStopButton()
    }
    
    func rideStopped(data: [String : Any]) {
        //
        self.removeObserverOnView()
        self.view.hideToastActivity()
        self.enableStartButton()
        let vc = AchievementsVC()
        vc.showOfflineStopMsg = true
        vc.rideID = String(self.rideID)//self.rideDetails?.id
        vc.rideTitle = self.rideTitle//self.rideDetails?.title
        self.pushVC(vc)
    }
    
    func rideInProgress(data: [String : Any]) {
        //
        if let rideDetails:Rides  = data["data"] as? Rides{
            
            if let flag:Bool = data["isRideInProgress"] as? Bool, flag {
                
                
                self.view.hideToastActivity()
                self.btnStart.isEnabled = true
                //Add Observer to reload data once app comes in foregraound
                self.addObserverOnView()
                
                
                if journeyManager.rideProgressType() == RideTypes.AdHoc {
                    
                    self.rideTitle = rideDetails.ride_title!
                    self.rideID = (rideDetails.rideID?.toInt()!)!
                   // self.enableStopButton()
                }
            }
            
        }
        
    }
    
    func getRideStatus() {
        print("Observer Called")
    //    self.homeViewModel.getRideInProgress()
//        if journeyManager.isAnyRideInProgress() && !journeyManager.currentRideDetails().planned {
//            self.showStatsData(data: [String:AnyObject]())
//        }
        getDeviceCurrentLocation()
    }
    
    func plotCurrentLocation() {
        //  homeMapBounds = GMSCoordinateBounds()
        let deviceLocation = DeviceLocationServiece.sharedInstance().getUserCurrentLocation()
        destiMarker.map = nil
        if deviceLocation.coordinate.latitude != 0 {
            destiMarker.position = deviceLocation.coordinate
            destiMarker.map = self.mapView
            destiMarker.title = StringConstant.MYLOCATION
            let resizedimage = UIImage.scaleTo(image: UIImage(named: "Source")!, w: 20.0, h: 20.0)
            destiMarker.icon = resizedimage
            self.mapView.animate(to: GMSCameraPosition.camera(withLatitude: CLLocationDegrees(exactly: deviceLocation.coordinate.latitude)!, longitude: CLLocationDegrees(exactly: deviceLocation.coordinate.longitude)!, zoom: 10.0))
        }
        
//        self.recenterBtn.setBackgroundImage(#imageLiteral(resourceName: "recenter"), for: .normal)
//        let rideId = journeyManager.currentRideId()
//        if journeyManager.isAnyRideInProgress(), rideId != 0, !journeyManager.currentRideDetails().planned {
//            let offlineStatsContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
//
//            if let offlineStats = AchievementDataObj.fetchStatsWithRideId(rideId: String(rideId), context: offlineStatsContext) {
//                guard let archivedLocs = offlineStats.rideLocations as? Data,
//                    let locs = NSKeyedUnarchiver.unarchiveObject(with: archivedLocs) as? [CLLocation] else {
//                        if self.homeMapBounds != nil {
//                            self.mapView.animate(with: GMSCameraUpdate.fit(homeMapBounds!))
//                        }
//                        return
//                }
//                if locs.count > 1 {
//                    for location in locs {
//                        homeMapBounds = homeMapBounds?.includingCoordinate(location.coordinate)
//                    }
//                    if homeMapBounds != nil {
//                        self.mapView.animate(with: GMSCameraUpdate.fit(homeMapBounds!))
//                    }
//                }
//            }
//        }
//        else {
//            self.mapView.setDefaultCameraPosition()
//            if let _ = homeMapBounds {
//                self.mapView.animate(with: GMSCameraUpdate.fit(homeMapBounds!))
//            }
//        }
        
    }
    
    func getDeviceCurrentLocation() {
        let isVirtualCM = MyDefaults.getBoolData(key: DefaultKeys.IS_CM_VIRTUAL)
        if !CLLocationManager.locationServicesEnabled() {
            if !MyDefaults.getBoolData(key: DefaultKeys.KEY_Location_ToastMessageShown){
                self.view.makeToast(NSLocalizedString("keyEnableLocationServices", comment: ""))
                MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_Location_ToastMessageShown)
                if !NetworkCheck.isNetwork() && !isVirtualCM && !MyDefaults.getBoolData(key: DefaultKeys.KEY_VirtualCm_Offline_ToastMessageShown){
                    self.view.makeToast("You are offline. Go online to view your bike location")
                    MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_VirtualCm_Offline_ToastMessageShown)
                }
            }
        } else {
            if !NetworkCheck.isNetwork(){
                bikeMarker.map = nil
                destiMarker.map = nil
                if !isVirtualCM && !MyDefaults.getBoolData(key: DefaultKeys.KEY_VirtualCm_Offline_ToastMessageShown){
                    self.view.makeToast("You are offline. Go online to view your bike location")
                    MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_VirtualCm_Offline_ToastMessageShown)
                }
            } else {
                if !isVirtualCM {
                    //  self.locationViewModel.getDeviceLocation(bikerID: bikerID, requestType: "GetCurrentDeviceLocation")
                    self.locationViewModel?.getDeviceLocation(bikerID: bikerID, requestType: "GetCurrentDeviceLocation", handler: { (data, errData) in
                        if let err = errData {
                            print("Error \(err)")
                            self.locationFailure(data: err)
                        }
                        
                        if let resData = data {
                            print("res data \(resData)")
                            self.locationSuccess(data: resData)
                        }
                        
                    })
                }
                self.plotCurrentLocation()
            }
        }
    }
    
    func menuTapped() {
        self.slideMenuController()?.openLeft()
    }
    
    @IBAction func onClickRecenter(_ sender: UIButton) {
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.locationServicesEnabled() {  // is location enabled
                if NetworkCheck.isNetwork() { // is network available
                    if let _ = homeMapBounds {
                        self.mapView.animate(with: GMSCameraUpdate.fit(homeMapBounds!))
                    } else {
                        if self.isLocationValid(location: destiMarker.position) {
                        self.mapView.animate(to: GMSCameraPosition.camera(withTarget: destiMarker.position, zoom: 10))
                        }else{
                            self.mapView.setDefaultCameraPosition()
                        }
                    }
                } else {
                    self.mapView.animate(to: GMSCameraPosition.camera(withTarget: destiMarker.position, zoom: 10))
                }
            }
            // entire route
        } else {
            self.mapView.setDefaultCameraPosition()
            self.showLocationTurnOnPopup()
        }
    }
    
    func isLocationValid(location: CLLocationCoordinate2D) -> Bool {
       return CLLocationCoordinate2DIsValid(location)
    }
    
    /*
     TODO:
     Tags
     100 - For Start Button (Default 100 is Set)
     200 - for Stop Button
     */
    @IBAction func btnStartAction(_ sender: Any) {
        if (journeyManager.isAnyRideInProgress())
        {
            self.view.makeToast("Stop the Current Ride to start a new one.", duration: 0.5,position: .bottom)
            if let gestureArray = btnStart.gestureRecognizers {
                for gesture in gestureArray {
                    if gesture is UILongPressGestureRecognizer {
                        self.btnStart.removeGestureRecognizer(gesture)
                    }
                }
            }
        }else{
            //Before Start check CM is registered or not
            let isCMRegisterd = MyDefaults.getBoolData(key: DefaultKeys.KEY_CMREGISTER)//-----Get CM Registration Status
            if isCMRegisterd {
                self.btnStart.addLongPressGesture(target: self, action: #selector(self.longTap))
                self.view.makeToast("Tap & Hold", duration: 0.5, position: .bottom)
            }else{
                self.showAlert()//--Show Alert for CM registration
                
            }
        }
    }
    
    @IBAction func btnRideLaterTapped(_sender: UIButton) {
         self.view.makeToast("Please enter destination.")
    }
    
    func showLegalAlert() {
        let alertController = UIAlertController(title: "Legal Notice", message: "WARNING: Please keep your eyes on the road while riding. Do not use the app or phone while riding.\n Legal Notice:It is your responsibility to observe safe driving practices. You are responsible to secure and use your product in a manner that will not cause accidents, personal injury or any damage to yourself or anyone around you.\nFor more information visit maximuspro.com/legal/termsofservice", preferredStyle: UIAlertControllerStyle.alert)
        let yesAction = UIAlertAction(title: "I Agree", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            self.view.makeToastActivity(.center ,isUserInteraction:false)
//            self.journeyManager.startAdHocRide()
            self.homeViewModel.startAdHocRide()
            let paramToSend = ["\(StringConstant.biker_id_recommendation_id)" : "\(MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID)) \(RecommendedRideVC.recommendedObj?.card_id ?? "")"]
            Analytics.logCustomEvent(withName: "ride_now_adhoc_tap", customAttributes: paramToSend)
        }
        alertController.addAction(yesAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertForOtp() {
        let strTitle = ""
        let alertController = UIAlertController(title: strTitle, message: "Please verify mobile number to start using MaximusPro.", preferredStyle: UIAlertControllerStyle.alert)
        let yesAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("OK")
            MyDefaults.setData(value: fromViewcontroller.HomeVC.rawValue, key: DefaultKeys.KEY_FROMWHERE)
            let navigationController = UINavigationController(rootViewController: PhoneVerificationVC())
            self.presentVC(navigationController)
        }
        alertController.addAction(yesAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func showAlert() {
        //Show Alert for CM registration
        let strTitle = "Your MaximusPro is not registered. To register, please pair Display Module with your phone by scanning the QR code on the Display Module. Make sure Bluetooth of your phone is turned ON"
        let alertController = UIAlertController(title: strTitle, message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        let yesAction = UIAlertAction(title: "YES", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("YES")
            
            let navigationController = UINavigationController(rootViewController: DeviceSetUpVC())
            self.presentVC(navigationController)
        }
        
        let laterAction = UIAlertAction(title: "LATER", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("LATER")
        }
        let buyAction = UIAlertAction(title: "BUY NOW", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("buy Now")
            if let amazonUrl = URL(string: NSLocalizedString("keyAmazonUrl", comment: "")) {
                UIApplication.shared.open(amazonUrl, options: [:]) { (status) in
                    print("Open amazon with status \(status)")
                }
            }
        }
       // buyAction.isEnabled = false
        alertController.addAction(yesAction)
        alertController.addAction(laterAction)
        alertController.addAction(buyAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func enableStartButton(){
        self.btnStart.setTitle("START", for: .normal)
        self.btnStart.tag = 100
        progressView?.progressImage = UIImage(named: "Start Green Line")
        progressView?.setProgress(0.0, animated: true)
    }
    
    func enableStopButton(){
        self.btnStart.setTitle("STOP", for: .normal)
        self.btnStart.tag = 200
        progressView?.progressImage = UIImage(named: "Progress Bar")
        progressView?.setProgress(0.0, animated: true)
    }
    
    func getCurrentRideStats(rideId: Int) -> RideStatsModel?{
        let rideStatsObj = RideStatsModel()
        let offlineStatsContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
        if let rideCard = Rides.fetchRideWithID(rideID: String(rideId), context: offlineStatsContext) {
            let achievement = rideCard.achievement
            rideStatsObj.distanceTravelled = achievement?.distance_travelled ?? 0
            rideStatsObj.travelledTime = TimeInterval(achievement?.time_duration ?? 0)
            rideStatsObj.topSpeed = Double(achievement?.top_speed ?? 0)
            rideStatsObj.averageSpeed = achievement?.average_speed ?? 0
            if  let archivedLocs = achievement?.rideLocations ,
                let locs = NSKeyedUnarchiver.unarchiveObject(with: archivedLocs as! Data) as? [CLLocation] {
                rideStatsObj.rideLocations = locs
            }else{
                
            }
            return rideStatsObj
        }
        return nil
    }
    
    //MARK: Long Tap gesture call
    func longTap(sender : UIGestureRecognizer){
        
        if sender.state == .ended {
            // Call when Release long Tap press button
            if(progressView?.progress == 1.0){
                DispatchQueue.main.async {
                    self.progressView?.setProgress(0.0, animated: false)
                }
                if(self.btnStart.tag == 100){
                    
                    let isOtpVerified = self.bikerDetails.is_otp_verified ////-----Get OTP Verification Status
                    if isOtpVerified || MyDefaults.getBoolData(key: DefaultKeys.KEY_OTPVERIFIED){
                        
                        if (!journeyManager.isAnyRideInProgress()){
                            if !CLLocationManager.locationServicesEnabled(){
                                self.view.makeToast(NSLocalizedString("keyEnableLocationServices", comment: ""))
                            }
                            self.showLegalAlert()
                            
                            
                        }else{
                            self.view.makeToast("Stop the Current Ride to start a new one.", duration: 0.5, position: .bottom)
                        }
                        
                    }else{
                        self.showAlertForOtp()
                    }
                }else{
                    self.view.makeToastActivity(.center ,isUserInteraction:false)
                    //self.journeyManager.stopRide(rideId: rideID)
                    //self.rideObj.stopRide(rideID: rideID)
                    self.homeViewModel.stopRide(rideId: rideID)
                }
            }
            else{
                DispatchQueue.main.async {
                    self.progressView?.setProgress(0.0, animated: true)
                }
            }
            timer?.invalidate()
        }
        // Call when Begin long Tap press button
        if sender.state == .began {
            DispatchQueue.main.async {
                self.progressView?.setProgress(0.0, animated: true)
                
            }
            timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(self.updateProgress), userInfo: nil, repeats: true)
        }
    }
    
    
    //MARK:- Add Progress Bar Progress
    func addControls() {
        // Create Progress View Control
        progressView = UIProgressView(progressViewStyle: UIProgressViewStyle.default)
        //progressView?.center = self.view.center
        progressView?.frame = CGRect(x: 0, y: self.btnStart.frame.origin.y - 40, width: self.view.frame.width, height: 10)
        //        progressView?.progressImage = UIImage(named: "Progress Bar")
        progressView?.progressImage = UIImage(named: "Start Green Line")
        progressView!.trackTintColor = UIColor.clear
        progressView?.barHeight = 6
        for subview in view.subviews {
            if subview is UIProgressView {
                progressView?.removeFromSuperview()
            }
        }
        self.rideButtonStackView.addSubview(progressView!)
    }
    //Update progress bar
    func updateProgress() {
        DispatchQueue.main.async {
            let frameChange: () -> () = {
                self.progressView?.progress = (self.progressView?.progress)! < 1.0 ? (self.progressView?.progress)! + 0.01 : 1.0
            }
            
            if #available(iOS 10.0, *) {
                UIViewPropertyAnimator(duration: 2.0, curve: .easeInOut, animations: frameChange)
                    .startAnimation()
            } else {
                UIView.animate(withDuration: 2.0,
                               delay: 0,
                               options: [UIViewAnimationOptions.curveEaseInOut],
                               animations: frameChange,
                               completion: nil);
            }
        }
    }
    
    
    
    func addObserverOnView(){
        print("*******************Observer Added****************")
        // add notification observers
        NotificationCenter.default.addObserver(self, selector: #selector(getRideStatus), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
    }
    
    func removeObserverOnView(){
        print("*******************Observer Removed****************")
        // Remove notification observers
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }

    
    func journeyNoRideInProgress() {
        
    }
    
    func journeyRideStartedSuccess(rideDetails:Rides) {
        self.rideID = journeyManager.currentRideId()
        self.rideTitle = rideDetails.ride_title!
        
        
        //Add Observer to reload data once app comes in foregraound
        self.addObserverOnView()
        self.view.hideToastActivity()
        
        let vc:SideMenuVC =  self.slideMenuController()?.leftViewController as! SideMenuVC
        vc.rideDetails = rideDetails
        vc.selectMenuAtSection(index: 1, andDeslectSectionIndex: 0)
        
    }
    
    func journeyRideStopedSuccess() {

    }
    
    func journeyRideRouteSuccess(tbt_Route: String) {
        print("journeyRideRouteSuccess callback")
    }
    
    func locationSuccess(data: Any) {
        if let deviceResponse = data as? [String: Any], let dict = deviceResponse["data"] as? DeviceDataResponse{
            bikeMarker.map = nil
            bikeMarker.position = CLLocationCoordinate2D(latitude: dict.latest_data?.first?.latitude ?? 0, longitude: dict.latest_data?.first?.longitude ?? 0)
            bikeMarker.map = self.mapView
            bikeMarker.title = StringConstant.BIKELOCATION
            let resizedimage = UIImage.scaleTo(image: UIImage(named: "Map Pointer")!, w: 40, h: 44)
            bikeMarker.icon = resizedimage
            self.plotCurrentLocation()
        }
    }
    
    func locationFailure(data: [String : Any]) {
        if let description = data["data"] as? String, data["RequestType"] as? String == "GetCurrentDeviceLocation" {
            bikeMarker.map = nil
            if !MyDefaults.getBoolData(key: DefaultKeys.KEY_VirtualCm_Offline_ToastMessageShown){
                self.view.makeToast(description)
                MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_VirtualCm_Offline_ToastMessageShown)
            }
        }
    }
}

extension HomeVC:UISearchBarDelegate {

    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        if NetworkCheck.isNetwork() {
            let viapointsList = ViaPointsListViewController()
            viapointsList.selectedSection = PlanRideDataSource.DestinationPlace
            viapointsList.passDelegate = planRideTableVC
            PlanRideDataManager.shared().selectedDestinationFirstTime = true
            let navigationController = UINavigationController(rootViewController: viapointsList)
            navigationController.title = StringConstant.STARTING_POINT
            present(navigationController, animated: false) {
                //
            }
        } else {
            self.view.makeToast(ToastMsgConstant.network)
        }
        
        return false
    }
    
    
    
}

