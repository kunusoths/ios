//
//  HomeViewModel.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 12/07/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import Foundation
import UIKit
protocol HomeViewModelDelegate:class {
    //
    func getBikerSucess(response: [String:Any])
    func rideStarted(data:[String:Any])
    func rideStopped(data: [String:Any])
    func rideInProgress(data: [String:Any])
}

class HomeViewModel:NSObject, BikerRepositoryDelegate, CurrentRideRepoDelegate {

    let bikerRepo = BikerRepository()
    let currentRideRepo = CurrentRideRepositery()
    let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.newBackgroundContext()
    var createRideID: String?
    let rideRequestHandler = RideRequestHandler()
    weak var delegate:HomeViewModelDelegate?
    
    override init() {
        super.init()
        print("HomeViewModel init")
        bikerRepo.bikerRepoDelegate = self
    }
    
    deinit {
        print("HomeViewModel deinit")
    }
    
    
    func homeGetBiker(bikerId: Int) {
        bikerRepo.repoGetBiker(bikerId: bikerId)
    }
    
    func startAdHocRide() {
        currentRideRepo.delegate = self
        currentRideRepo.startAdhocRide()
    }
    
    func offlineRideStartSuccess(data: [String : Any]) {
        
        let ride = data["data"] as! Rides
        let rideData:[String:Any] = ["type": RideStateAPIType.RIDE_START,
                                 "rideData": ride]
        self.delegate?.rideStarted(data: rideData)
    }
    
    func startRide(rideId: Int)  {
        currentRideRepo.delegate = self
        currentRideRepo.startRide(rideID: rideId)
    }
    
    func stopRide(rideId: Int){
        currentRideRepo.delegate = self
        currentRideRepo.stopRide(rideId: rideId)
        self.delegate?.rideStopped(data: [:])
    }
    
    func getRideInProgress(){
        self.currentRideRepo.delegate = self
        self.currentRideRepo.repoGetRideProgress()
    }

    // MARK:- BikerRepositoryDelegate Methods
    func repoAuthenticationFail(data: [String : Any]) {
        //
    }
    
    func bikerRepoResponse(response: [String : Any]) {
        //
        let type:BikerAPI = response["type"] as! BikerAPI
        
        switch type {
        case .GET_BIKER:
            
            //let bikerDetails:BikerDetails = response["bikerData"] as! BikerDetails
            self.delegate?.getBikerSucess(response: response)
            break
        default:
            break
        }
    }
    
    // MARK:- CurrentRideRepoDelegate Methods
    func repoSuccess(data: [String : Any]) {
        //
         let type:RideStateAPIType = data["type"] as! RideStateAPIType
        
        switch type {
            
        case .RIDE_START:
            //rideStartSucess
            RideRequest.deletePreviousRequest(rideID: data["rideID"] as! String, type: .Start, context: context!)
            self.delegate?.rideStarted(data: data)
            break
        
        case .RIDE_STOP:
            RideRequest.deletePreviousRequest(rideID: data["rideID"] as! String, type: .Stop, context: context!)
            self.delegate?.rideStopped(data: data)
            break
        
        case .RIDE_IN_PROGRESS:
            self.delegate?.rideInProgress(data: data)
            break
            
        default:
            break
        }
    }
    
    func repoFailed(data: [String : Any]) {
        //
    }
    
}
