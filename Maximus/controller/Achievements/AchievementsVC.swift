//
//  AchievementsVC.swift
//  Maximus
//
//  Created by Sriram G on 22/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import GoogleMaps
import Charts
import CoreData

class AchievementsVC: UIViewController ,CLLocationManagerDelegate, UITextFieldDelegate{
    
    @IBOutlet weak var collectionViewStickers: UICollectionView!
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblAvgSpeed1: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var vwGraphPopUp: UIView!
    @IBOutlet weak var lblTopSpeed: UILabel!
    @IBOutlet weak var lblAvgSpeed2: UILabel!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var vwBadgeAndRideDetails: UIView!
    @IBOutlet weak var imgVwDividerLine: UIImageView!
    @IBOutlet weak var activityIndicatorGraph: UIActivityIndicatorView!
    @IBOutlet weak var btnShowGraph: UIButton!
    @IBOutlet weak var imgVwDropDown: UIImageView!
    
    let cellIdentifier = "StikerBoardCell"
    let collectionViewHeight:CGFloat = 145.0
    let vwBadgeAndRideDetailsHeight:CGFloat = 264.0
    
    var isVirtualCM = MyDefaults.getBoolData(key: DefaultKeys.IS_CM_VIRTUAL)
    
    var originalMapviewHeight:CGFloat = 0.0
    var badgeHonorListArray:[HonorBadgeListResponse] = []
    var locationManager = CLLocationManager()
    var zoomLevel: Float = 15.0
    var rideID:String?
    var bikerID:Int?
    var rideTitle:String!
    var speedLattitude:[Double] = []
    var speedLongitude:[Double] = []
    var navTextField: UITextField!
    var sourceMarker = GMSMarker()
    var destiMarker = GMSMarker()
    var acheivementsData:AchievementDataObj?
    var honoredBadges:[[String: String]]? = [[:]]
    var strDistance     = "00"
    var strTime         = "00:00"
    var strAvgSpeed     = "00"
    var strTopSpeed     = "00"
    var routeData:RouteLatLngModel?
    var flagGraphSuccess = false
    var showOfflineStats = true
    let rideStatsModel = RideStatsViewModel()
    let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.newBackgroundContext()
    var showOfflineStopMsg = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        imgVwDropDown.isHidden = true
        btnShowGraph.isUserInteractionEnabled = false
        
        collectionViewStickers.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        
        originalMapviewHeight = mapView.frame.size.height
        self.showBadgeCollectionView(state: false)
        
        self.btnShare.setCornerRadius(radius: 20.0)
        vwGraphPopUp.isHidden = true
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "Ham Menu"), style: .plain, target: self, action: #selector(onHamTapped))
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        
        self.addNavTitleTextField()
        rideStatsModel.rideStatsDelegate = self
        
        bikerID = MyDefaults.getInt (key: DefaultKeys.KEY_BIKERID)
        MapTheme.shared.setTheme(map: mapView)///---Set Google map theme color
        

        if showOfflineStopMsg {
            if !NetworkCheck.isNetwork() {
                self.view.makeToast("Your ride has been saved offline.\n We can't keep your ride data forever.\n Go online at earliest to sync your ride data.")
            } else {
                
            }
        }
        
        acheivementsData = AchievementDataObj.fetchStatsWithRideId(rideId: rideID ?? "", context: context!)?.time_duration == 0 ? Rides.fetchRideWithID(rideID: rideID ?? "", context: context!)?.achievement : AchievementDataObj.fetchStatsWithRideId(rideId: rideID ?? "", context: context!) 
        speedLongitude = []
        speedLattitude = []
        var obj = LocationDetails.fetchLocationDetailsOfRideID(rideID: rideID!, context: context!)
        obj = obj.sorted(by: {$0.packettimestamp! < $1.packettimestamp!})
        if obj.count > 0{
            for speedData in obj {
                speedLattitude.append(speedData.latitude)
                speedLongitude.append(speedData.longitude)
            }
            imgVwDropDown.isHidden = false
            btnShowGraph.isUserInteractionEnabled = true
        } else {
            if NetworkCheck.isNetwork() {
                self.graphSpeedPints()
            }
            imgVwDropDown.isHidden = true
            btnShowGraph.isUserInteractionEnabled = false
        }
        if showOfflineStats {
            self.showLocalStats()
        }else{
            
            if let acheivedData = acheivementsData {
                self.showStatsData(data: [StringConstant.data:acheivedData])
                if let locData = acheivedData.rideLocations as? Data,
                    let locArray = NSKeyedUnarchiver.unarchiveObject(with: locData) as? [CLLocation], locArray.count > 0
                {
                    self.showPolyline(obj: acheivementsData!)
                } else {
                    self.updateLocationData()
                }
            }
            
            if ((honoredBadges?.count)! > 0) {
                let dataDict = honoredBadges![0]
                if dataDict.count > 0 {
                    self.showBadgeCollectionView(state: true)
                    self.collectionViewStickers.reloadData()
                }
            }
        }
        
        self.lineChartView.chartDescription?.text = "Speed Points"
        self.lineChartView.chartDescription?.textColor = UIColor.white
        self.lineChartView.chartDescription?.position = CGPoint.init(x: 130, y: 6)
        self.lineChartView.noDataText = ""
        self.lineChartView.noDataTextColor = UIColor.white
        self.lineChartView.delegate = self
        
        self.mapView.setDefaultCameraPosition()
    }
    
    func addNavTitleTextField()
    {
        // Ride title view
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Edit Button"), style: .plain, target: self, action: #selector(editRideName))
        self.navigationItem.rightBarButtonItem?.tintColor = .white
        navTextField = UITextField(frame:CGRect(x:0, y:0, width: (self.navigationController?.navigationBar.frame.width)! * 0.8, height: 20))
        navTextField.textColor = UIColor .white
        navTextField.isUserInteractionEnabled = false
        navTextField.autocapitalizationType = .words
        navTextField.returnKeyType = .done
        navTextField.keyboardAppearance = .dark
        navTextField.text = rideTitle
        navTextField.textAlignment = .center
        navTextField.delegate = self
        self.navigationItem.titleView = navTextField
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField.text == ""){
            self.view.makeToast("Ride title should not be empty", duration: 1.0, position: .center)
            return false
        }else{
           // rideTitle = textField.text!
            self.updateRideTitle(title: textField.text! as String)
            navTextField.isUserInteractionEnabled = false
            textField.resignFirstResponder()
            return true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true
        }
        if (textField.text?.length)! <= 24 {
            return true
        }
        return false
    }
    
    //MARK:Private Methods
    func onHamTapped() {
        self.slideMenuController()?.openLeft()
    }
    
    func editRideName(){
        if NetworkCheck.isNetwork() {
            print("Edit tapped")
            self.navigationItem.rightBarButtonItem?.image = #imageLiteral(resourceName: "Edit Button")
            self.navTextField.isUserInteractionEnabled = true
            navTextField.becomeFirstResponder()
        } else {
            self.view.makeToast(ToastMsgConstant.network)
        }
    }
    
    func showBadgeCollectionView(state: Bool){
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.view.layoutIfNeeded()
            if(state){
                if(self.vwBadgeAndRideDetails.frame.size.height != self.vwBadgeAndRideDetailsHeight){
                    self.collectionViewStickers.frame.size.height = self.collectionViewHeight
                    self.vwBadgeAndRideDetails.frame.size.height += self.collectionViewHeight
                    self.vwBadgeAndRideDetails.frame.origin.y -= self.collectionViewHeight
                    self.mapView.frame.size.height -= self.collectionViewHeight
                    self.imgVwDividerLine.isHidden = false
                }
            }else{
                if(self.vwBadgeAndRideDetails.frame.size.height == self.vwBadgeAndRideDetailsHeight){
                    self.collectionViewStickers.frame.size.height = 0
                    self.vwBadgeAndRideDetails.frame.size.height -= self.collectionViewHeight
                    self.vwBadgeAndRideDetails.frame.origin.y += self.collectionViewHeight
                    self.mapView.frame.size.height += self.collectionViewHeight
                    self.imgVwDividerLine.isHidden = true
                }
            }
        })
        
    }
    
    @IBAction func btnExpandGraphAction(_ sender: UIButton) {
        vwGraphPopUp.isHidden = false
        
        if((honoredBadges?.count)! == 0 && badgeHonorListArray.count == 0){
//            LoadingHandler.shared.startloading(interactionEnable: true, view: self.view)
            self.showBadgeCollectionView(state:true)
        }
        if(!flagGraphSuccess){
            self.graphSpeedPints()
        }
        
    }
    
    @IBAction func btnCloseGraphAction(_ sender: UIButton) {
        if((honoredBadges?.count)! == 0 && badgeHonorListArray.count == 0){
            self.showBadgeCollectionView(state:false)
        }
        vwGraphPopUp.isHidden = true
    }
    
    @IBAction func btnShareAction(_ sender: UIButton) {
        let vc = AchievementShareVC()
        vc.rideID       = self.rideID
        vc.rideTitle    = self.rideTitle
        self.pushVC(vc)
        //self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: vc), close: true)
    }
    
    private func updateRideTitle(title: String)
    {
        //Put ride title
        print("Ride Id", rideID ?? 222)
        
        let json :(Any)=["title": title,
                         "id": "\(rideID!)"]
        print("Json", json)
        self.rideStatsModel.putRideTitle(parameter: json as! [String: Any], request: "PutRideTitle")
    }
    
    private func updateLocationData()
    {
        //Get Ride Route data Call
        self.rideStatsModel.getRideRoute(rideID: Int(rideID ?? "")!, bikerID: bikerID ?? 0, request: "GetCompletedRideRoute")
    }
    
    private func graphSpeedPints(){
        //Get Garaph Speed points data Call
        if let rideCard = Rides.fetchRideWithID(rideID: rideID ?? "", context: context!) {
            if let locations  = rideCard.locationDetails, locations.allObjects.count > 0 {
                var chartDataEntries: [ChartDataEntry] = []
                var chartEntry: ChartDataEntry?
                if var locationArray = locations.allObjects as? [LocationDetails] {
                    locationArray = locationArray.sorted(by:{$0.packettimestamp!<$1.packettimestamp!})
                    for i in 0..<(locationArray.count) {
                        let obj = locationArray[i]
                        self.activityIndicatorGraph.stopAnimating()
                        
                        chartEntry  = ChartDataEntry(x: Double(i), y: (obj.max_ground_speed * 0.036).rounded())
                        chartDataEntries.append(chartEntry!)
                    }
                    self.setChartWithValues(dataPoints: chartDataEntries, lineChart: self.lineChartView)
                    self.lineChartView.delegate = self
                    flagGraphSuccess = true
                }
            } else {
                self.activityIndicatorGraph.startAnimating()
                self.rideStatsModel.getGraphSpeedPoints(rideID: Int(rideID ?? "")!, bikerID: bikerID ?? 0, request: "GetGraphSpeedPoints")
            }
        }
    }
    
    func setChartWithValues(dataPoints: [ChartDataEntry], lineChart: LineChartView)
    {
        let graphUtilityobj = GraphUtilit()
        
        graphUtilityobj.initChart(linechart: lineChart)
        graphUtilityobj.setLimitLine(val: 0, color: UIColor.white, linechart: lineChart)
        graphUtilityobj.setChart(lineChart: lineChart, data:dataPoints,arrOfGradientPosition: [25.0,35.0,45.0])
    }
    
}

// MARK:- UICollectionViewDataSource Delegate
extension AchievementsVC: UICollectionViewDataSource {
    
    @available(iOS 6.0, *)
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! StikerBoardCell
        cell.imgStickerIcon.image = nil
        cell.imgStickerIcon.tag = indexPath.row
        
        if (honoredBadges?.count)! > 0 {
            let dict = honoredBadges![indexPath.row]
            if dict.count > 0{
                cell.imgStickerIcon.loadImageUsingCacheWithUrlString(urlString: dict["url"]!,indexPath: indexPath.row)
                //                cell.imgStickerIcon.imageWithUrl(url: dict["url"]!)
            }
            
        }else {
            let objStikerHonored:HonorBadgeListResponse = self.badgeHonorListArray[indexPath.row]
            cell.imgStickerIcon.loadImageUsingCacheWithUrlString(urlString: (objStikerHonored.images?[0].url)!,indexPath: indexPath.row)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if (honoredBadges?.count)! > 0 {
            return honoredBadges!.count
        }else{
            return badgeHonorListArray.count
        }
    }
}

extension AchievementsVC:ChartViewDelegate {
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight)
    {
        
        if Int(entry.x) <=  speedLattitude.count - 1 {
            
            let curLat = speedLattitude[Int(entry.x)]
            let curLng = speedLongitude[Int(entry.x)]
            
            let originPos = CLLocationCoordinate2DMake(Double(curLng),Double(curLat))
            
            self.destiMarker.map = nil
            self.destiMarker = GMSMarker(position: originPos)
            self.destiMarker.map = self.mapView
            self.destiMarker.icon = UIImage(named: "Map Pointer")
            self.destiMarker.map = self.mapView
        }
    }
}

extension AchievementsVC: RideStatsDelegate {
    func failure(data: [String : Any]) {
        self.activityIndicatorGraph.stopAnimating()
        if let errModel:ErrorModel = data["data"] as? ErrorModel {
            print(errModel.message ?? " ")
            if let errorCode = errModel.errorCode,errorCode != RideErrors.ERROR_ANALYTICS_RIDE_NOT_STARTED{
                self.view.makeToast((errModel.description)!)
            }else{// showing local stats in case of low network when cloud data isn't synced with app data.
                if let requestType = data["RequestType"] as? String,requestType == "GetCompletedRideRoute"{
                    showLocalStats()
                }
            }
        } else {
            if let description = data["description"] as? String {
                self.view.makeToast(description)
            }
            navTextField.text = rideTitle
        }
    }
    
    func rideStatisticSuccess(data: Any) {
        if let dict = data as? [String: Any] {
            if let obj = dict["data"] as? RideCreateResponse, dict["RequestType"] as? String == "PutRideTitle" {
                navTextField.text = obj.title
                rideTitle = obj.title!
            } else if let obj = dict["data"] as? [String: Any], dict["RequestType"] as? String == "GetRideStats" {
                self.showStatsData(data: obj)
            } else if let obj = dict["data"] as? RouteLatLngModel, dict["RequestType"] as? String == "GetCompletedRideRoute"{
                self.routeData = obj
                if((self.routeData?.LocationList?.count)!>0){
                    if LocationDetails.fetchLocationDetailsOfRide(rideID: String(obj.RideId!), context: context!) {
                        imgVwDropDown.isHidden = false
                        btnShowGraph.isUserInteractionEnabled = true
                    }
                    self.mapView.drawRoute(obj: obj,markerSource:self.sourceMarker,markerDesti:self.destiMarker)
                }
                var locationArray: [CLLocation] = []
                for i in 0..<(routeData?.LocationList?.count)! {
                    let cllocation = CLLocation(latitude: CLLocationDegrees((routeData?.LocationList![i].lat ?? 0)), longitude: CLLocationDegrees((routeData?.LocationList![i].lng ?? 0)))
                    locationArray.append(cllocation)
                }
                AchievementDataObj.updateRideLocation(rideID: String(obj.RideId ?? 0), rideLocation:locationArray , context: context!)
            } else if let obj = dict["data"] as? [LocationDetails] {
                speedLongitude = []
                speedLattitude = []
                for speedData in obj {
                    speedLattitude.append(speedData.latitude)
                    speedLongitude.append(speedData.longitude)
                }
                if obj.count > 0 {
                    var chartDataEntries: [ChartDataEntry] = []
                    var chartEntry: ChartDataEntry?
                    for i in 0..<(obj.count) {
                        let location = obj[i]
                        chartEntry  = ChartDataEntry(x: Double(i), y: ((location.max_ground_speed)*0.036).rounded())
                        chartDataEntries.append(chartEntry!)
                    }
                    self.activityIndicatorGraph.stopAnimating()
                    
                    self.setChartWithValues(dataPoints: chartDataEntries, lineChart: self.lineChartView)
                    self.lineChartView.delegate = self
                    flagGraphSuccess = true
                    self.imgVwDropDown.isHidden = false
                    self.btnShowGraph.isUserInteractionEnabled = true
                }
            }
        }
    }
    
    func showStatsData(data:[String:Any]) {
        
        if(data.count>0){
            
            if let acheivedData = acheivementsData {
                
                let dist = acheivedData.distance_travelled
                if (dist != 0){
                    strDistance = String(format:"%.1f",Double(dist)/Double(1000))
                }
                
                
                let time = Int(acheivedData.time_duration)
                let (hours,minutes,_):(Int,Int,Int) = time.convertSecondsToHours()
                strTime   = String(format: "%02i:%02i", hours,minutes)
                
                
                let speed = acheivedData.average_speed
                if(speed != 0){
                    strAvgSpeed   = String(format:"%.0f", (Double(speed)*Double(3.6)).rounded())
                }
                
                let topSpeed = acheivedData.top_speed
                if(topSpeed != 0){
                    strTopSpeed = String(format:"%.0f", (Double(topSpeed)*Double(3.6)).rounded())
                }
                
            }else {
                let obj:RideStatisticsModel = data["data"] as! RideStatisticsModel
                if let distance = obj.distanceTravelled {
                    strDistance  = String(format:"%.1f",Double(distance)/Double(1000))
                }
                
                if let time = obj.timeDuration {
                    let (hours,minutes,_):(Int,Int,Int) = time.convertSecondsToHours()
                    strTime   = String(format: "%02i:%02i", hours,minutes)
                }
                if let speed = obj.averageSpeed {
                    strAvgSpeed   =  String(format:"%.0f", (Double(speed)*Double(3.6)).rounded())
                }
                if let topSpeed = obj.topSpeed {
                    strTopSpeed = String(format:"%.0f", (Double(topSpeed)*Double(3.6)).rounded())
                }
            }
        }
        self.lblDistance.text   = strDistance
        self.lblTime.text       = strTime
        self.lblAvgSpeed1.text  = strAvgSpeed
        self.lblAvgSpeed2.text  = strAvgSpeed
        self.lblTopSpeed.text   = strTopSpeed
    }
    
    func showLocalStats() {
            if let offlineStats = acheivementsData {
                
                let dist = offlineStats.distance_travelled
                let dist_km = dist
                self.lblDistance.text =  String(format: "%.1f", arguments: [Double(dist_km)/1000])
                
                
                let time = offlineStats.time_duration
                let mins = (time / 60).truncatingRemainder(dividingBy: 60)
                let hrs = time / 3600
                var strMin:String = "\(Int(mins))"
                var strHrs:String = "\(Int(hrs))"
                
                if mins < 10 {
                    strMin = "0\(Int(mins))"
                }
                
                if hrs < 10 {
                    strHrs = "0\(Int(hrs))"
                }
                self.lblTime.text = "\(strHrs):\(strMin)"
                
                
                let avgSpeed = offlineStats.average_speed
                let avgSpeed_kmph = Double(avgSpeed) * Double(3.6)
                if !(avgSpeed_kmph.isNaN || avgSpeed_kmph.isInfinite)  {
                    self.lblAvgSpeed1.text = "\(Int(avgSpeed_kmph))"
                    self.lblAvgSpeed2.text = "\(Int(avgSpeed_kmph))"
                }
                
                let topSpeed = offlineStats.top_speed
                let topSpeed_kmph = String(format:"%.0f", (Double(topSpeed)*Double(3.6)).rounded())
                
                if !(avgSpeed_kmph.isNaN || avgSpeed_kmph.isInfinite)  {
                    self.lblTopSpeed.text = topSpeed_kmph
                }
                
                self.showPolyline(obj: offlineStats)
            }
    }
    
    func showPolyline(obj: AchievementDataObj) {
        guard let locData = obj.rideLocations as? Data,
            let locs = NSKeyedUnarchiver.unarchiveObject(with: locData) as? [CLLocation]
            else{
                return
        }
        self.mapView.clear()
        self.mapView.drawPolylinewithDestiMarker(Locations: locs, sourceMArker: self.sourceMarker, destMarker: self.destiMarker)
        if LocationDetails.fetchLocationDetailsOfRide(rideID: rideID!, context: context!) {
            imgVwDropDown.isHidden = false
            btnShowGraph.isUserInteractionEnabled = true
        }
        if locs.count > 1 {
            
            var rideBounds = GMSCoordinateBounds()
            for location in locs {
                rideBounds = rideBounds.includingCoordinate(location.coordinate)
            }
            self.mapView.drawPolylinewithDestiMarker(Locations: locs, sourceMArker: self.sourceMarker, destMarker: self.destiMarker)
        }
    }
}
