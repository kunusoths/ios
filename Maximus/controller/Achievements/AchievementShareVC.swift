//
//  AchievementShareVC.swift
//  Maximus
//
//  Created by Namdev Jagtap on 13/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreData

class AchievementShareVC: UIViewController,CLLocationManagerDelegate,UITextFieldDelegate {
    
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet weak var lblTopSpeed: UILabel!
    @IBOutlet weak var lblAvgSpeed: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet var viewCapture: UIView!
    @IBOutlet var lblRideTitle: UILabel!
    
    var textFieldNav: UITextField!
    var rideID:String?
    var strDistance     = "00"
    var strTime         = "00:00"
    var strAvgSpeed     = "00"
    var topSpeed        = "00"
    var rideTitle       = "RIDE ON"
    var zoomLevel: Float = 10.0
    var dataRoute:RouteLatLngModel?
    var sourceMarker = GMSMarker()
    var destiMarker = GMSMarker()
    let rideStatsModel = RideStatsViewModel()
    
    //MARK:View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // Creates a marker in the center of the map.
        self.mapView.setDefaultCameraPosition()
        self.addNavTitleTextField()
        
        self.mapView?.settings.compassButton = true
        MapTheme.shared.setTheme(map: mapView)///---Set Google map theme color
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Prev Arrow"), style: .plain, target: self, action: #selector(onClickBack))
        self.navigationItem.leftBarButtonItem?.tintColor = .white

        rideStatsModel.rideStatsDelegate = self
        
        self.showRideStats()
    }
    
    
    func addNavTitleTextField() {
        // Ride title view
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Edit Button"), style: .plain, target: self, action: #selector(editRideName))
        
        self.navigationItem.rightBarButtonItem?.tintColor = .white
        textFieldNav = UITextField(frame:CGRect(x:0, y:0, width: (self.navigationController?.navigationBar.frame.width)! * 0.8, height: 20))
        textFieldNav.textColor = UIColor .white
        textFieldNav.returnKeyType = .done
        textFieldNav.isUserInteractionEnabled = false
        textFieldNav.text = self.rideTitle
        self.lblRideTitle.text = self.rideTitle
        textFieldNav.autocapitalizationType = .words
        textFieldNav.keyboardAppearance = .dark
        textFieldNav.textAlignment = .center
        textFieldNav.delegate = self
        self.navigationItem.titleView = textFieldNav
    }
    
    func editRideName(){
        if NetworkCheck.isNetwork() {
            self.navigationItem.rightBarButtonItem?.image = #imageLiteral(resourceName: "Edit Button")
            self.textFieldNav.isUserInteractionEnabled = true
            textFieldNav.becomeFirstResponder()
        } else {
           self.view.makeToast(ToastMsgConstant.network)
        }
    }
    
    func showRideStats() {
        if let  rideId = self.rideID {
            
            let ridestatsContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
            
            
            if let rideStats = AchievementDataObj.fetchStatsWithRideId(rideId: String(rideId), context: ridestatsContext)?.time_duration == 0 ? Rides.fetchRideWithID(rideID: rideID ?? "", context: ridestatsContext)?.achievement : AchievementDataObj.fetchStatsWithRideId(rideId: rideID ?? "", context: ridestatsContext) {
                
                strDistance  = String(format:"%.1f", Double(rideStats.distance_travelled)/Double(1000))
                
                let time = Int(rideStats.time_duration)
                let (hours,minutes,_):(Int,Int,Int) = time.convertSecondsToHours()
                strTime   = String(format: "%02i:%02i", hours,minutes)
                
                let speed = rideStats.average_speed
                strAvgSpeed   = String(format:"%.0f",(Double(speed)*Double(3.6)).rounded())
                
                let topspeed = rideStats.top_speed
                topSpeed = String(format:"%.0f",(Double(topspeed)*Double(3.6)).rounded())
                
                guard let locData = rideStats.rideLocations as? Data,
                    let locs = NSKeyedUnarchiver.unarchiveObject(with: locData) as? [CLLocation]
                    else{
                        return
                }
                self.mapView.clear()
                self.mapView.drawPolylinewithDestiMarker(Locations: locs, sourceMArker: self.sourceMarker, destMarker: self.destiMarker)
                
            }
            
            
            self.lblTopSpeed.text = topSpeed
            self.lblDistance.text = strDistance
            self.lblTime.text     = strTime
            self.lblAvgSpeed.text = strAvgSpeed
            
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField.text == ""){
            self.view.makeToast("Ride title should not be empty", duration: 1.0, position: .center)
            return false
        }else{
           // rideTitle = textField.text!
           // self.lblRideTitle.text = textField.text!
            self.updateRideTitle(title: textField.text! as String)
            textFieldNav.isUserInteractionEnabled = false
            textField.resignFirstResponder()
            return true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true
        }
        if (textField.text?.length)! <= 24 {
            return true
        }
        return false
    }
    
    // MARK: Side Menus
    func onClickBack() {
        self.popVC()
    }
    
    
    @IBAction func shareButtonAction(_ sender: UIButton) {
        takeScreenshot(view:viewCapture)
    }
    
    func takeScreenshot(view: UIView)  {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        // set up activity view controller
        let imageToShare = [ image! ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        // exclude some activity types from the list (optional)
        //        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    private func updateRideTitle(title: String)
    {
        //Put ride title
        print("Ride Id", rideID ?? 222)
        
        let json :(Any)=["title": title,
                         "id": "\(rideID!)"]
        print("Json", json)
        self.lblRideTitle.text = title
        self.view.makeToastActivity(.center, isUserInteraction: false)
        self.rideStatsModel.putRideTitle(parameter: json as! [String: Any], request: "PutRideTitle")
    }
}
extension AchievementShareVC: RideStatsDelegate {
    func rideStatisticSuccess(data: Any) {
        self.view.hideToastActivity()
        if let dict = data as? [String: Any] {
            if dict["RequestType"] as? String == "PutRideTitle" {
                if let obj:RideCreateResponse = dict["data"] as? RideCreateResponse {
                    if let rideTitle = obj.title {
                        self.lblRideTitle.text = rideTitle
                    }
                }
            }
        }
    }
    
    func failure(data: [String : Any]) {
        self.view.hideToastActivity()
        if let errModel:ErrorModel = data["data"] as? ErrorModel {
            print(errModel.message ?? " ")
            self.view.makeToast((errModel.description)!)
        } else {
            if let descriptionString = data["description"] as? String {
                self.view.makeToast(descriptionString)
            }
            self.lblRideTitle.text = rideTitle
            self.textFieldNav.text = rideTitle
        }
    }
    
    
}

