//
//  BatteryHealthViewVC.swift
//  Maximus
//
//  Created by Sriram G on 21/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import Alamofire
import Charts


class BatteryHealthViewVC: UIViewController,RideManagerDelegate
{
    // MARK:Memory Management
    
    @IBOutlet weak var ignitionOnChart: LineChartView!
    @IBOutlet weak var ignitionOffChart: LineChartView!
    @IBOutlet weak var lblOnDate: UILabel!
    @IBOutlet weak var lblOffDate: UILabel!

       var months: [String]!
       var date:Date?

    // MARK:View LifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Arrow White"), style: .plain, target: self, action: #selector(menuTapped))
        self.navigationItem.title = "BATTERY HEALTH"
        
        self.navigationItem.leftBarButtonItem?.tintColor = .white

        self.getBatteryHealth()
    
        self.ignitionOnChart.chartDescription?.text = "Voltage Levels for 15 Days"
        self.ignitionOnChart.chartDescription?.textColor = UIColor.white
        self.ignitionOnChart.chartDescription?.position = CGPoint.init(x: 130, y: 6)
        self.ignitionOnChart.noDataText = "No data provided"
        self.ignitionOffChart.chartDescription?.text = "Voltage Levels for 15 Days"
        self.ignitionOffChart.chartDescription?.position = CGPoint.init(x: 130, y: 6)
        self.ignitionOffChart.chartDescription?.textColor = UIColor.white
        self.ignitionOffChart.noDataText = "No data provided"
    }

    
    // MARK:Private Methods
    func menuTapped()
    {
       self.dismissVC(completion: nil)
        //self.slideMenuController()?.openLeft()
    }

    func getBatteryHealth()
    {
        LoadingHandler.shared.startloading(interactionEnable: true, view: self.view)
            let rideManagerObj = RideManager()
            rideManagerObj.rideManagerDelegate = self
            rideManagerObj.getBatteryHealth()
    }
    
    func sortBatteryItemsWithRespectToVoltage(batteryItem:[String:Any])
    {
        
        var batteryVoltageList = [BatteryVoltageData]()
        batteryVoltageList = batteryItem["data"] as! [BatteryVoltageData]
        date = batteryItem["prev_date"] as! Date?
        
        var counter:double_t?
        counter = 0
        var counter1:double_t?
        counter1 = 0

        var dataEntryON:ChartDataEntry
        var dataEntryOFF:ChartDataEntry
        
        var dataEntriesON: [ChartDataEntry] = []
        var dataEntriesOFF: [ChartDataEntry] = []
        
        for voltageDataObj in  batteryVoltageList {
            
            if  voltageDataObj.ignition_status == 0
            {
                dataEntryOFF  = ChartDataEntry(x: counter!, y: voltageDataObj.analog_ip_1!)
                dataEntryOFF.data = voltageDataObj
                dataEntriesOFF.append(dataEntryOFF)
                counter = counter! + 1

            }
            else
            {
                dataEntryON  = ChartDataEntry(x: counter1!, y: voltageDataObj.analog_ip_1!)
                dataEntryON.data = voltageDataObj
                dataEntriesON.append(dataEntryON)
                counter1 = counter1! + 1
            }
        }
            DispatchQueue.main.async{
            self.setChartWithValues(lineChart: self.ignitionOffChart , dataEntry:dataEntriesOFF)
            }
        
            DispatchQueue.main.async{
            self.setChartWithValues(lineChart: self.ignitionOnChart , dataEntry:dataEntriesON)
        }

        DispatchQueue.main.async{

            self.updateUI()
        }
    }
    
    func setChartWithValues(lineChart: LineChartView, dataEntry:[ChartDataEntry])
    {
        let graphUtilityobj = GraphUtilit()
        graphUtilityobj.initChart(linechart: lineChart)
        let yAxis = graphUtilityobj.addYAxis(linechart: lineChart)
        print(yAxis)
        
        //10,11,13
        graphUtilityobj.setLimitLine(val: 14, color: UIColor.green, linechart: lineChart)
        graphUtilityobj.setLimitLine(val: 13, color: UIColor.yellow, linechart: lineChart)
        graphUtilityobj.setLimitLine(val: 12, color: UIColor.red, linechart: lineChart)
        graphUtilityobj.setLimitLine(val: 0, color: UIColor.white, linechart: lineChart)
        graphUtilityobj.setChart(lineChart: lineChart, data:dataEntry ,arrOfGradientPosition: [12.0,13.0,14.0])
        
    }
    
    func updateUI()
    {
        if date != nil
        {
           let formattedDate:String = (date?.dateMediumFormat())!
            lblOnDate.text = formattedDate
            lblOffDate.text = formattedDate
        }
    }

    // MARK:Response Delegates

    internal func onFailure(Error: [String : Any])
    {
        print(Error["data"]!)
    }
    
    func sendBatteryVoltageList(batteryObj:[String:Any])
    {
        DispatchQueue.main.async
        {
            LoadingHandler.shared.stoploading()
        }
        self.sortBatteryItemsWithRespectToVoltage(batteryItem: batteryObj)
    }

}
