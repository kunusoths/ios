//
//  BatteryIgnitionOnVC.swift
//  Maximus
//
//  Created by Appsplanet Macbook on 07/05/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class BatteryIgnitionOnVC: UIViewController,RideManagerDelegate {
    
    //MARK: Properties
    @IBOutlet weak var viewTabView: UIView!
    @IBOutlet weak var btnIgnitionOff: UIButton!
    @IBOutlet weak var btnIgnitionOn: UIButton!
    @IBOutlet weak var imgViewBattery: UIImageView!
    @IBOutlet weak var lblBatteryVoltage: UILabel!
    @IBOutlet weak var lblBatteryMsg: UILabel!
    
    @IBOutlet weak var battery24StatusBtn: UIButton!
    var bottomBorder:UIView!
    var batteryDetails:MeanBikeBatteryVoltageModel?
    var userName = ""
    
    let isCMRegisterd = MyDefaults.getBoolData(key: DefaultKeys.KEY_CMREGISTER)
    let formatter = NumberFormatter()
    
    @IBOutlet weak var lblMyBikeFirst: UILabel!
    @IBOutlet weak var myBikeFirst: UIImageView!
    //MARK:Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Ham Menu"), style: .plain, target: self, action: #selector(menuTapped))
        self.navigationItem.title = "MY BIKE"
        self.bottomBorder = UIView()
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.navigationController!.navigationBar.isTranslucent = false
        self.bottomBorder.backgroundColor = .white
        self.viewTabView.addSubview(bottomBorder!)
        
        self.btnIgnitionOn.backgroundColor = AppTheme.THEME_ORANGE
        self.btnIgnitionOff.backgroundColor = AppTheme.THEME_ORANGE
        formatter.maximumFractionDigits = 1
        formatter.roundingMode = .down
        formatter.numberStyle = .decimal
        formatter.alwaysShowsDecimalSeparator = true
        formatter.minimumFractionDigits = 1
        
        userName = MyDefaults.getData(key: DefaultKeys.KEY_FIRST_NAME) as! String
        
        LoadingHandler.shared.startloading(interactionEnable: true, view: self.view)
        let rideManagerObj = RideManager()
        rideManagerObj.rideManagerDelegate = self
        rideManagerObj.getBatteryMeanBikeBatteryVoltage()
        battery24StatusBtn.contentHorizontalAlignment = .center
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.updateTabClick(button: self.btnIgnitionOn)
    }
    override func viewWillAppear(_ animated: Bool) {
        setBatteryIgnitionOnData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:Private Methods
    func menuTapped() {
        self.slideMenuController()?.openLeft()
    }
    
    func updateTabClick(button:UIButton){
        self.bottomBorder?.frame = CGRect(x: button.frame.x , y: 46, width: button.frame.width, height: 4)
    }
    
    func setBatteryIgnitionOnData(){
        
        if isCMRegisterd
        {
            if let batteryMean:MeanBikeBatteryVoltageModel = self.batteryDetails{
                let battery_ignition_on : Double = NSString(string: batteryMean.mean_bike_battery_ignition_on_voltage).doubleValue
                let voltage = formatter.string(from: NSNumber(value: battery_ignition_on))
                lblBatteryVoltage.text = voltage! + " V"
                
                if(battery_ignition_on == 0.0){
                    lblBatteryMsg.text = "Hey there, looks like you are not\n riding for last 24 hours"
                    lblBatteryVoltage.textColor = UIColor(colorLiteralRed: 155/255.0, green: 155/255.0, blue: 155/255.0, alpha: 1/1.0)
                    imgViewBattery.image = #imageLiteral(resourceName: "Neutral")
                }else if (battery_ignition_on > 12.5){
                    lblBatteryMsg.text = "Battery looks good \(userName)!"
                    lblBatteryVoltage.textColor = UIColor(colorLiteralRed: 0/255.0, green: 255/255.0, blue: 0/255.0, alpha: 1/1.0)
                    imgViewBattery.image = #imageLiteral(resourceName: "Happy")
                }else if(battery_ignition_on < 12.4 && battery_ignition_on > 11.5){
                    lblBatteryMsg.text = "You should check on\n battery \(userName)!"
                    lblBatteryVoltage.textColor = UIColor(colorLiteralRed: 255/255.0, green: 209/255.0, blue: 73/255.0, alpha: 1/1.0)
                    imgViewBattery.image = #imageLiteral(resourceName: "Not Good")
                }else {
                    lblBatteryMsg.text = "Battery needs urgent\n attention \(userName)!"
                    lblBatteryVoltage.textColor = UIColor(colorLiteralRed: 255/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1/1.0)
                    imgViewBattery.image = #imageLiteral(resourceName: "Sad")
                }
                
            }
        }
        else
        {
            myBikeFirst.isHidden = false
            lblMyBikeFirst.isHidden = false
        }
    }
    
    func setBatteryIgnitionOffData(){
    if isCMRegisterd{
        if let batteryMean:MeanBikeBatteryVoltageModel = self.batteryDetails{
            let battery_ignition_off : Double = NSString(string: batteryMean.mean_bike_battery_ignition_off_voltage).doubleValue
            let voltage = formatter.string(from: NSNumber(value: battery_ignition_off))
            lblBatteryVoltage.text = voltage! + " V"
            
            if(battery_ignition_off == 0.0){
                lblBatteryMsg.text = "Hey there, looks like you are not\n riding for last 24 hours"
                lblBatteryVoltage.textColor = UIColor(colorLiteralRed: 155/255.0, green: 155/255.0, blue: 155/255.0, alpha: 1/1.0)
                imgViewBattery.image = #imageLiteral(resourceName: "Neutral")
            }else if (battery_ignition_off > 12.5){
                lblBatteryMsg.text = "Battery looks good \(userName)!"
                lblBatteryVoltage.textColor = UIColor(colorLiteralRed: 0/255.0, green: 255/255.0, blue: 0/255.0, alpha: 1/1.0)
                imgViewBattery.image = #imageLiteral(resourceName: "Happy")
            }else if(battery_ignition_off < 12.4 && battery_ignition_off > 11.5){
                lblBatteryMsg.text = "You should check on\n battery \(userName)!"
                lblBatteryVoltage.textColor = UIColor(colorLiteralRed: 255/255.0, green: 209/255.0, blue: 73/255.0, alpha: 1/1.0)
                imgViewBattery.image = #imageLiteral(resourceName: "Not Good")
            }else {
                lblBatteryMsg.text = "Battery needs urgent\n attention \(userName)!"
                lblBatteryVoltage.textColor = UIColor(colorLiteralRed: 255/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1/1.0)
                imgViewBattery.image = #imageLiteral(resourceName: "Sad")
            }
            
        }
    }
        else
        {
            myBikeFirst.isHidden = false
            lblMyBikeFirst.isHidden = false
        }
    }
    
    
    // MARK:Response Delegates
    internal func onFailure(Error: [String : Any])
    {
        DispatchQueue.main.async{
            LoadingHandler.shared.stoploading()
        }
        print(Error["data"]!)
    }
    
    func getMeanBikeBatteryVoltageSuccess(data: [String : Any]) {
        
        DispatchQueue.main.async{
            LoadingHandler.shared.stoploading()
        }
        
        if let obj:MeanBikeBatteryVoltageModel = data["data"] as? MeanBikeBatteryVoltageModel {
            self.batteryDetails = obj
            setBatteryIgnitionOnData()
        }
    }
    
    // MARK:Actions
    @IBAction func btnIgnitionOffClicked(_ sender: UIButton) {
        self.updateTabClick(button: self.btnIgnitionOff)
        self.setBatteryIgnitionOffData()
        
    }
    
    @IBAction func btnIgnitionOnClicked(_ sender: UIButton) {
        self.updateTabClick(button: self.btnIgnitionOn)
        self.setBatteryIgnitionOnData()
        
    }
    @IBAction func btnBatteryHealthGraphClicked(_ sender: UIButton) {
        let navigationController = UINavigationController(rootViewController: BatteryHealthViewVC())
        self.presentVC(navigationController)
    }
    
}
