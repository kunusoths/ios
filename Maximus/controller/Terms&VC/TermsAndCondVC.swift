//
//  TermsAndCondVC.swift
//  Maximus
//
//  Created by Admin on 09/11/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//

import UIKit
import WebKit


class TermsAndCondVC: UIViewController {

    // MARK: - class properties
    
    fileprivate let keyLoading = "loading"
    fileprivate let keyEstimateProgress = "estimatedProgress"
    
    
    open var URLToLoad: String = ""
    open var progressTintColor : UIColor?
    open var trackTintColor : UIColor?
    fileprivate var webView: WKWebView
    var fileName: String = ""
    
    @IBOutlet fileprivate weak var loadingProgress: UIProgressView!
    @IBOutlet fileprivate weak var webViewContainer: UIView!
    
    
    // MARK: - life cycle
    
    required init?(coder aDecoder: NSCoder) {
        
        webView = WKWebView()
        super.init(coder: aDecoder)
        webView.navigationDelegate = self
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        
        webView = WKWebView()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        webView.navigationDelegate = self
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerSideMenu()
        viewConfigurations()
    }
    override func viewWillAppear(_ animated: Bool) {
        //registerObservers()
    }
    
    /*
     Register Side Menu
    */
    func registerSideMenu(){
        if self.title == ViewcontrollerNameConstant.aboutusScreen {
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationItem.setHidesBackButton(true, animated:false)
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Ham Menu"), style: .plain, target: self, action: #selector(menuTapped))
        } else {
             self.navigationItem.setHidesBackButton(true, animated:false)
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Arrow White"), style: .plain, target: self, action: #selector(popView))
            self.navigationItem.leftBarButtonItem?.tintColor = .white
        }
    }
    /*
     Pop view
     */
    func popView() {
        self.popVC()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        //removeObservers()
        visibleActivityIndicator(false)
        
    }
    
    // MARK: - private functions
    
    fileprivate func viewConfigurations() {
        
        
        //These are the changes for UnderTopBars & UnderBottomBars
        edgesForExtendedLayout = []
        extendedLayoutIncludesOpaqueBars = false
        automaticallyAdjustsScrollViewInsets = false
        
        loadingProgress.trackTintColor = trackTintColor
        loadingProgress.progressTintColor = progressTintColor
        
        webViewContainer.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        let attributes: [NSLayoutAttribute] = [.top, .bottom, .right, .left]
        
        NSLayoutConstraint.activate(attributes.map {
            NSLayoutConstraint(item: webView, attribute: $0, relatedBy: .equal, toItem: webViewContainer, attribute: $0, multiplier: 1, constant: 0)
        })
        
        let urlWeb:URL?
            ////Remove it, once you get the working URL of T&C
        if fileName == StringConstant.termsServiceFileName || fileName == "privacy_policy" {
             urlWeb = Bundle.main.url(forResource: fileName, withExtension: "html")
        } else {
             urlWeb = Bundle.main.url(forResource: fileName, withExtension: "docx")
        }
            webView.contentMode = .scaleAspectFit
            webView.load(URLRequest(url: urlWeb!))
        
    }
    //MARK:Private Methods
   @objc fileprivate func menuTapped() {
        self.slideMenuController()?.openLeft()
    }

    fileprivate func registerObservers() {
        webView.addObserver(self, forKeyPath: keyLoading, options: .new, context: nil)
        webView.addObserver(self, forKeyPath: keyEstimateProgress, options: .new, context: nil)
    }
    
    fileprivate func removeObservers() {
        webView.removeObserver(self, forKeyPath: keyLoading)
        webView.removeObserver(self, forKeyPath: keyEstimateProgress)
    }
    
    fileprivate func visibleActivityIndicator(_ visible: Bool) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = visible
    }
    
    fileprivate func showAlert(_ title: String, message: String) {
        let alertController: UIAlertController = UIAlertController(title: title,
                                                                   message: message,
                                                                   preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("keyOk", comment: ""), style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
        
    }
    
    // MARK: - Overridden Methods
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if (keyPath == keyEstimateProgress) {
            loadingProgress.isHidden = webView.estimatedProgress == 1
            loadingProgress.setProgress(Float(webView.estimatedProgress), animated: true)
        }
        
    }
    
    
}

extension TermsAndCondVC: WKNavigationDelegate {
    //MARK: WebView Delegates
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        visibleActivityIndicator(true)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        visibleActivityIndicator(false)
        showAlert("", message: error.localizedDescription)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        visibleActivityIndicator(false)
        loadingProgress.setProgress(0.0, animated: false)
    }
    
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        if navigationAction.navigationType == WKNavigationType.linkActivated {
            
            let url = navigationAction.request.url
            let shared = UIApplication.shared
            
            if shared.canOpenURL(url!) {
                shared.open(url!, options: [:], completionHandler: nil)
            }
            
            decisionHandler(WKNavigationActionPolicy.cancel)
            return
        }
        
        decisionHandler(WKNavigationActionPolicy.allow)
    }
}
