//
//  TCContainerViewController.swift
//  Maximus
//
//  Created by Isha Ramdasi on 16/11/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//

import UIKit

class TCContainerViewController: UIViewController {

    @IBOutlet weak var aboutBtn: UIButton!
    @IBOutlet weak var legalBtn: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tabView: UIView!
    var bottomBorder:UIView?
    var aboutVC = TermsAndCondVC()
    var legalVC = TermsAndCondVC()
    
    private var activeViewController: UIViewController? {
        didSet {
            removeInactiveViewController(inactiveViewController: oldValue)
            updateActiveViewController()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Ham Menu"), style: .plain, target: self, action: #selector(menuTapped))
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        
        self.legalBtn.backgroundColor = AppTheme.THEME_ORANGE
        self.aboutBtn.backgroundColor = AppTheme.THEME_ORANGE
        
        self.bottomBorder = UIView()
        self.bottomBorder?.backgroundColor = UIColor.white
        self.tabView.addSubview(bottomBorder!)
        updateTabClick(button: self.legalBtn)
        legalVC.fileName = StringConstant.termsServiceFileName
        legalVC.title = NSLocalizedString("keyLegalTitle", comment: "")
        activeViewController = legalVC
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = NSLocalizedString("keyTCTitle", comment: "")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
     Open Menu
     */
    func menuTapped()
    {
        self.slideMenuController()?.openLeft()
    }
    /*
     Highlight selected Tab
     @param: UIButton
     */
    func updateTabClick(button:UIButton){
        self.bottomBorder?.frame = CGRect(x: button.frame.x , y: 46, width: button.frame.width, height: 4)
    }
    
    //MARK: Button Methods
    /*
     Show Legal file content
     @param: UIButton
    */
    @IBAction func onClickLegal(_ sender: UIButton) {
         legalVC.fileName = StringConstant.termsServiceFileName
        legalVC.title = NSLocalizedString("keyLegalTitle", comment: "")
         activeViewController = legalVC
         updateTabClick(button: legalBtn)
    }
    /*
     Show About file content
     @param: UIButton
     */
    @IBAction func onClickAbout(_ sender: UIButton) {
        aboutVC.fileName = StringConstant.LicencesList
        aboutVC.title = NSLocalizedString("keyLicencesTitle", comment: "")
        activeViewController = aboutVC
        updateTabClick(button: aboutBtn)
    }
    
    // MARK : Handling VCs
    private func removeInactiveViewController(inactiveViewController: UIViewController?) {
        if let inActiveVC = inactiveViewController {
            // call before removing child view controller's view from hierarchy
            inActiveVC.willMove(toParentViewController: nil)
            
            inActiveVC.view.removeFromSuperview()
            
            // call after removing child view controller's view from hierarchy
            inActiveVC.removeFromParentViewController()
        }
    }
    
    private func updateActiveViewController() {
        if let activeVC = activeViewController {
            // call before adding child view controller's view as subview
            addChildViewController(activeVC)
            
            activeVC.view.frame = containerView.bounds
            containerView.addSubview(activeVC.view)
            
            // call before adding child view controller's view as subview
            activeVC.didMove(toParentViewController: self)
        }
    }


}
