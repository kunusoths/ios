//
//  WelcomeVC.swift
//  Maximus
//
//  Created by kpit on 23/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import CoreLocation
import SlideMenuControllerSwift

class ResizableButton: UIButton {
    override var intrinsicContentSize: CGSize {
        get {
            let labelSize = titleLabel?.sizeThatFits(CGSize(width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude)) ?? CGSize.zero
            let desiredButtonSize = CGSize(width: labelSize.width + titleEdgeInsets.left + titleEdgeInsets.right, height: labelSize.height + titleEdgeInsets.top + titleEdgeInsets.bottom)
            
            return desiredButtonSize
        }
    }
}

class WelcomeVC: UIViewController {
    
    @IBOutlet weak var lblThankYouUser: UILabel!
    @IBOutlet weak var btnSetUpDevice: UIButton!
    
    let welcomeContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()

    let greetMessage = NSLocalizedString("keyGreetMsg", comment: "")
    var userName = "😊"
    var greetReason = NSLocalizedString("keyGreetReason", comment: "")
    let userObject = UserManagerHandler()
    var bikerDetails:BikerDetails!
    let homeViewModel = HomeViewModel()
    var myRidesViewModel: MyRidesViewModel?
    var planRideModel: PlanRideViewModel?
    let rideRepo = RideRepository()
    let planRideRepo = PlanRideRepository()
    let rideStatsModel = RideStatsViewModel()
    var notStartedRides: [Rides] = []
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    var routeData:RouteLatLngModel?
    var rideLogs: [Rides]?
    
    @IBOutlet weak var pairDisplayMsg: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        myRidesViewModel = MyRidesViewModel(rideRepoDelegate: rideRepo)
        planRideModel = PlanRideViewModel(planRideRepoDelegate: planRideRepo)
        self.navigationItem.title = NSLocalizedString("keyTitleSignUp", comment: "")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.bikerDetails = BikerDetails.getBikerDetails(context: welcomeContext)
        homeViewModel.delegate = self
        homeViewModel.getRideInProgress()
        if bikerDetails.cm_paired ?? false {//MyDefaults.getBoolData(key: DefaultKeys.KEY_CMREGISTER) {
            btnSetUpDevice.setTitle(NSLocalizedString("keyAllSetToHitTheRoad", comment: ""), for: .normal)
            greetReason = "!"
            pairDisplayMsg.isHidden = true
        }
        self.updateWelcomeLabel()
        rideRepo.repoDelegate = self
        rideStatsModel.rideStatsDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.sendFcmTokenToCloud()
    }
    
    //MARK - Button Actions
    /*
     Caches previous data and pushes next view of data is already cached
     @param: UIButton
    */
    @IBAction func btnSetUpDeviceAction(_ sender: UIButton)
    {
        if NetworkCheck.isNetwork() && !MyDefaults.getBoolData(key: StringConstant.firstTimeDataFetch) {
            self.view.makeToastActivity(.center, isUserInteraction: false)
            self.cacheDataOfNotStartedRides()
        } else if !NetworkCheck.isNetwork() && !MyDefaults.getBoolData(key: StringConstant.firstTimeDataFetch){
            self.view.makeToast("Sorry, could not fetch all data.")
            pushNextView()
        } else {
           pushNextView()
        }
    }
    
    //MARK://Private Methods
    /*
     Update welcome label as per biker name
     */
    func updateWelcomeLabel() {
        
        userName = (bikerDetails.first_name ?? "") + " " + (bikerDetails.last_name ?? "")
        
        let attributedString = NSMutableAttributedString(string:greetMessage)
        
        let attrs = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 15)]
        
        
        let boldString = NSMutableAttributedString(string:userName, attributes:attrs)
        boldString.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: (userName as NSString).range(of: userName))
        
        attributedString.append(boldString)
        attributedString.append(NSMutableAttributedString(string: "!", attributes:attrs))
        
        lblThankYouUser.attributedText = attributedString
    }
    /*
     Load Home screen as next screen
     @param: UIViewController
     */
    func pushHomeVC(viewController: UIViewController) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let navigationController = UINavigationController(rootViewController:viewController)
        let mainViewController = navigationController
        let leftViewController = SideMenuVC()
        let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
        slideMenuController.changeLeftViewWidth(self.view.size.width-60)
        
        appDelegate.window?.rootViewController = slideMenuController
    }
    /*
     Send FCM token to cloud
     */
    func sendFcmTokenToCloud() {
        if let fcmToken =  MyDefaults.getData(key: DefaultKeys.FCM_TOKEN) as? String{
            userObject.postFcmToken(parameter:["device_os":"ios", "token":fcmToken])
            print("token in welcome screen\(fcmToken)")
        }
        else{
            print("login success but token is empty")
        }
    }
}

extension WelcomeVC:HomeViewModelDelegate, ReloadMyRidesDelegate, RidesRepoCallback,RideStatsDelegate {
    //MARK: Delegate methods of API callbacks
    func getBikerSucess(response: [String : Any]) {
        //
    }
    
    func rideStarted(data: [String : Any]) {
        //
    }
    
    func rideStopped(data: [String : Any]) {
        //
    }
    
    func rideInProgress(data: [String : Any]) {
        self.view.hideToastActivity()
//         if let flag:Bool = data[StringConstant.isRideInProgress] as? Bool, flag {
//            self.pushHomeVC(viewController: RideInProgressContainer())
//         } else {
//            self.pushHomeVC(viewController: HomeContainerVC())
//        }
    }
    
    func cacheDataOfNotStartedRides() {
//        myRidesViewModel.reloadDataDelegate = self
        myRidesViewModel?.getAllRidesOfBiker(request: URLRequestType.GetPlannedRides, pageNo: 10000, bikerID: Int(self.bikerDetails.biker_id!)!, size: 0, successCallback: {responseData in
            self.reloadData(data: responseData)
        }, failureCallback:{ failureData in
            self.failure(data: failureData)
        })
    }
    
    func reloadData(data: Any) {
        if  let dict = data as? [String: Any] , let request = dict[StringConstant.keyRequestType] as? String, request == URLRequestType.GetPlannedRides,let arr = dict[StringConstant.data] as? [Rides], arr.count > 0 {
            notStartedRides = Rides.getAllNotSyncedRides(context: context)
            getFutureRides()
            saveDataOfAllNotStartedRides()
        } else if let _ = data as? [RideMemberDetails] {
            let testContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
            Rides.updateSyncFlag(rideID:notStartedRides.first?.rideID ?? "" , context: testContext)
            if notStartedRides.count > 0 {
                notStartedRides.removeFirst()
                self.saveDataOfAllNotStartedRides()
            } else {
//                pushNextView()
                rideStatsModel.getRideStatsFor(bikerID: Int(self.bikerDetails.biker_id!)!, page: 0, size: 10, request: URLRequestType.GetRideStats)
            }
        } else {
            // new user or no rides planned but completed rides
            rideStatsModel.getRideStatsFor(bikerID: Int(self.bikerDetails.biker_id!)!, page: 0, size: 10, request: URLRequestType.GetRideStats)
        }
    }
    
    func saveDataOfAllNotStartedRides() {
        if let rideCard = notStartedRides.first {
            if  rideCard.isSynced == 0 {
                planRideModel?.getRideRouteById(rideID: Int(rideCard.rideID ?? "")!, bikerID: Int(self.bikerDetails.biker_id ?? "")!, type: URLRequestType.GetRideRouteById, successCallback: { (data) in
                    self.saveRideSuccess(data: data)
                }) { (error) in
                    self.failure(data: error)
                }
            } else {
                self.view.hideToastActivity()
                pushNextView()
            }
        } else {
            rideStatsModel.getRideStatsFor(bikerID: Int(self.bikerDetails.biker_id!)!, page: 0, size: 10, request: URLRequestType.GetRideStats)
        }
    }
    
    func failure(data: [String : Any]) {
        self.view.hideToastActivity()
        self.view.makeToast("Sorry, could not fetch all data.")
        pushNextView()
    }
    
    func saveRideSuccess(data: Any) {
        if  let dict = data as? [String: Any], let request = dict[StringConstant.keyRequestType] as? String, request == URLRequestType.GetRideRouteById{
            let rideCard = dict[StringConstant.data] as? SingleRouteDetails
            myRidesViewModel?.getRideMembers(rideID: (rideCard?.id)!, type: URLRequestType.GetRideMembers, successCallback: {responseData in
                self.reloadData(data: responseData)
            }, failureCallback:{ failureData in
                self.failure(data: failureData)
            })
            MyDefaults.setInt(value:rideCard?.id ?? 0 , key: DefaultKeys.KEY_POI_RIDEID)
            rideRepo.cachePOI(rideID: rideCard?.id ?? 0, request: URLRequestType.CreateRide, successCallback: {responseData in
                
            }, failureCallback:{ failureData in
                self.failure(data: failureData)
            })
        }
    }
    
    func repoSuccessCallback(data: Any) {
        MyDefaults.setBoolData(value: true, key: StringConstant.firstTimeDataFetch)
    }
    
    func repoFailureCallback(error: [String : Any]) {
        self.view.hideToastActivity()
        self.view.makeToast("Sorry, could not fetch all data.")
       pushNextView()
    }
    
    func pushNextView() {
        if self.bikerDetails.cm_paired {
            if let rideProgress = Rides.getMyProgressRide(context: context) {
                //push current ride vc
                JourneyManager.sharedInstance.refreshCurrentRideDetails()
                self.pushHomeVC(viewController: RideInProgressContainer())
            } else {
                //push home vc
                self.pushHomeVC(viewController: HomeContainerVC())
            }
        }else{
            self.pushVC(DeviceSetUpVC())
        }
    }
    
    func getFutureRides() {
        let todaysDays = NSDate()
        var futureDayRides: [Rides] = []
        var presentDayRides: [Rides] = []
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "MMM d, yyyy"
        let strTodaysDate = dateFormatter.string(from: todaysDays as Date)
        for ride in notStartedRides {
            let rideDate = ride.start_date?.getFormatedDate()
            let strRideDate = dateFormatter.string(from: rideDate ?? Date())
            
            let dateRideStartDate = dateFormatter.date(from: strRideDate) ?? Date() as Date
            let dateTodaysDate = dateFormatter.date(from: strTodaysDate) ?? Date() as Date
            if(dateTodaysDate > dateRideStartDate){
                //past
            }else if dateTodaysDate == dateRideStartDate {
                //present
                presentDayRides.append(ride)
            } else {
                //future
                futureDayRides.append(ride)
            }
        }
        notStartedRides = futureDayRides + presentDayRides
    }
    
    func rideStatisticSuccess(data: Any) {
        if let dict = data as? [String: Any] {
            if let obj = dict[StringConstant.data] as? RouteLatLngModel, dict[StringConstant.keyRequestType] as? String == URLRequestType.GetCompletedRideRoute{
                self.routeData = obj
                var locationArray: [CLLocation] = []
                for i in 0..<(routeData?.LocationList?.count)! {
                    let cllocation = CLLocation(latitude: CLLocationDegrees((routeData?.LocationList![i].lat ?? 0)), longitude: CLLocationDegrees((routeData?.LocationList![i].lng ?? 0)))
                    locationArray.append(cllocation)
                }
                AchievementDataObj.updateRideLocation(rideID: String(obj.RideId ?? 0), rideLocation:locationArray , context: context)
                fetchRideRoute()
            } else {
                rideLogs = Rides.getLastTenCompletedRides(context: context)
                fetchRideRoute()
            }
        }
    }
    
    func fetchRideRoute() {
        if (rideLogs?.count)! > 0 {
            self.rideStatsModel.getRideRoute(rideID: Int(rideLogs![0].rideID ?? "")!, bikerID: Int(self.bikerDetails.biker_id!)!, request: URLRequestType.GetCompletedRideRoute)
            rideLogs?.removeFirst()
        } else {
            self.view.hideToastActivity()
            MyDefaults.setBoolData(value: true, key: StringConstant.firstTimeDataFetch)
            pushNextView()
        }
    }
}

