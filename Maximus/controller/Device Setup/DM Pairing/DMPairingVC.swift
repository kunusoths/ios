//
//  DMPairingVC.swift
//  Maximus
//
//  Created by kpit on 23/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import CoreBluetooth
import SlideMenuControllerSwift

@objc protocol UpdateErrorMsgs {
    func updateErrorMsg(msg: String)
}

class DMPairingVC: UIViewController {
    
    @IBOutlet weak var btnScanAgain: UIButton!
    @IBOutlet weak var viewPasskeyinfo: UIView!
    @IBOutlet weak var imgVwPrevArrow: UIImageView!
    @IBOutlet weak var lblSixDigitPassKey: UILabel!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var btnRetry: UIButton!
    
    var serialNumber = ""
    var nameDM = ConstantBLEName.Name_DM
    var timer: Timer? = nil
    var seconds = 0
    weak var delegate: UpdateErrorMsgs?
    
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var skipLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        MyDefaults.setBoolData(value: false, key: "NonceSuccess")
        // btnRetry.isHidden = true
        self.navigationItem.title = "DEVICE SETUP"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        
        let image1 = #imageLiteral(resourceName: "Prev Arrow").withRenderingMode(.alwaysTemplate)
        imgVwPrevArrow.image = image1
        imgVwPrevArrow.tintColor = UIColor.white
        //Activity Hide initially
        self.loadingActivity.isHidden = true
        self.runTimer()
        skipLbl.textColor = UIColor.gray
        skipButton.isUserInteractionEnabled = false
        /////*********** Serial Number extraction ******************
        if !serialNumber.isEmpty && serialNumber.length > 6 {
            let passKey = serialNumber.substring(from:serialNumber.index(serialNumber.endIndex, offsetBy: -6))
            lblSixDigitPassKey.text = passKey
            
            nameDM = nameDM + passKey
            print(nameDM)
        }
        
        //Start Scan for BLE
        BluetoothManager.sharedInstance().configureBLEService(name: nameDM)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.registerObserverforScreen(register: true)
        NotificationCenter.default.addObserver(self, selector: #selector(updateScreen), name: NSNotification.Name(rawValue: "UpdateScreen"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.registerObserverforScreen(register: false)
        NotificationCenter.default.removeObserver(self)
        timer?.invalidate()
        timer = nil
    }
    
    func updateScreen() {
        self.loadingActivity.isHidden = true
        let status = BluetoothManager.sharedInstance().getBLEState()
        updateUI(state: status)
    }


    //MARK: Button Actions
    @IBAction func btnScanAgainClicked(_ sender: UIButton) {
        BluetoothManager.sharedInstance().stopScanning()
        BluetoothManager.sharedInstance().unpaireBLE()
        self.popVC()
    }
    
    
    @IBAction func btnRetryAction(_ sender: Any) {
        self.loadingActivity.startAnimating()
        self.loadingActivity.isHidden = false
    }
    
    @IBAction func btnSkipAction(_ sender: Any) {
        
        BluetoothManager.sharedInstance().disconnectPeripheral()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let navigationController = UINavigationController(rootViewController: HomeContainerVC())
        let mainViewController = navigationController
        let leftViewController = SideMenuVC()
        let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
        slideMenuController.changeLeftViewWidth(self.view.size.width-60)
        appDelegate.window?.rootViewController = slideMenuController
    }
    
    
    // MARK:Notification OFF Bluetooth Handlers
    func eventBLEStatusUpdate(notification:Notification){
        let status:BluetoothState = notification.userInfo!["data"] as! BluetoothState
        updateUI(state: status)

    }
    
    func updateUI(state: BluetoothState) {
        print("DMPairingVC State: \(state)")
        switch state {
        case .CONNECTED_NP:
            print("CONNECTED_NP")
            break
        case .CONNECTED_P:
            self.loadingActivity.isHidden = true
            self.DMPairingSucces()
            print("CONNECTED_P")
            self.registerObserverforScreen(register: false)
            break
        case .DISCONNECT_NP:
            self.delegate?.updateErrorMsg(msg: "Could not pair. Please try again.")
            self.popView()
            print("DISCONNECT_NP")
            break
        case .DISCONNECT_P:
            print("DISCONNECT_P")
            self.popView()
            break
        case .DISCONNECT_E:
            self.delegate?.updateErrorMsg(msg: "Please unpair your device from ‘My Support’ and try pairing again.")
            print("DISCONNECT_E")
            self.popView()
            break
        case .DISCONNECT_N:
            print("DISCONNECT_N")
            self.delegate?.updateErrorMsg(msg: "Please unpair your device from ‘My Support’ and try pairing again.")
            self.popView()
            break
        case .DISCONNECT_O:
            print("DISCONNECT_O")
            self.delegate?.updateErrorMsg(msg: "Device is already mapped with other vehicle.")
            self.popView()
            break
        case .DISCONNECT_BOP:
            print("DISCONNECT_BOP")
            self.popView()
            break
        case .DISCONNECT_BONP:
            print("DISCONNECT_BONP")
            self.popView()
            break
        case .NONE:
            print("NONE")
            // self.popView()
            break
            
        case .DISCONNECT_BOFFP:
            break
        case .DISCONNECT_BOFFNP:
            break
        case .DISCONNECT_BOFFP_NC:
            break
        case .DISCONNECT_BOFFNP_NC:
            break
        case .CONNECT_BOFFP:
            break
        case .CONNECT_BOFFNP:
            self.sendNonce()
            print("CONNECT_BOFFNP")
            break
        case .CONNECT_BONP_NC:
            break
        case .CONNECT_BONNP_NC:
            // self.popView()
            break
        }
    }
    ///Register observer on View
    func registerObserverforScreen(register:Bool){
        let sinkManager = SinkManager.sharedInstance
        if(register){
            
            sinkManager.addObserver(observer: self, notificationKey: SinkManager.sharedInstance.SINK_NOTIFICATION_BLUETOOTH_STATE_UPDATE, selectorName:eventBLEStatusUpdate)
            
            
        }else{
            
            sinkManager.removeObserver(observer: self, notificationKey: SinkManager.sharedInstance.SINK_NOTIFICATION_BLUETOOTH_STATE_UPDATE)
        }
    }
    
    
    //MARK: Local Function calls
    ///Pop the current screen
    fileprivate func popView(){
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    ///Push to next Screen on success
    fileprivate func DMPairingSucces(){
        DispatchQueue.main.async {
            let successVC = CMPairedSuccessVC()
            successVC.serialNumber = self.serialNumber
            self.pushVC(successVC)
        }
    }
    
    func runTimer() {
        print("Start nonce timer")
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 15, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: false)
        }
    }
    
    func updateTimer() {
        // disconnect BLE
        if !MyDefaults.getBoolData(key: "NonceSuccess") {
            print("Invalidate nonce timer")
            Thread.sleep(forTimeInterval: 1.0)
            BluetoothManager.sharedInstance().disconnectPeripheral()
            timer?.invalidate()
            self.delegate?.updateErrorMsg(msg: "Unable to register you. Please contact customer care")
            self.popView()
        }
    }
    
    func sendNonce() {
        runTimer()
        generateNonce()
    }
    
    func generateNonce() {
            let passkey = Array((lblSixDigitPassKey.text)!.utf8)
            var byteArray:[UInt8] = []
            for i in 0..<16 {
                if i < 6 {
                    byteArray.append(passkey[i] - 48)
                } else {
                    byteArray.append(0)
                }
            }
        
            let nonce =  AES_encryptionEcb128(&byteArray)
            print(nonce?.hashValue ?? 0)
            let data = Data(bytes: nonce!, count:  16)
            
            MyDefaults.setDataType(value: data, key: "NONCE")
            print("NONCE send: \(data)")
            let _ = BluetoothServices.sharedInstance().writeToBleChar(infoData: Data(bytes: nonce!, count:  16), typeOfChar: BLEChar.nonceChar)
            self.timer?.invalidate()
            self.timer = nil
            MyDefaults.setBoolData(value: true, key: "NonceSuccess")
    }
}
