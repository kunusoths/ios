//
//  DeviceSetUpVC.swift
//  Maximus
//
//  Created by kpit on 23/09/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import AVFoundation
import ObjectMapper
import SlideMenuControllerSwift

class DeviceSetUpVC: UIViewController, AVCaptureMetadataOutputObjectsDelegate, CMRegistrationDelegate,UserManagerDelegate,UpdateErrorMsgs, BikerViewModelDelegate  {
    @IBOutlet weak var checklistPopup: UIView!
    
    @IBOutlet weak var imgVwNextArrow: UIImageView!
    @IBOutlet weak var vwBarcodeCapture: UIView!
    @IBOutlet weak var lblPairingNotSuccessful: UILabel!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    
    @IBOutlet weak var chceklistTxt: UILabel!
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    var trackFlow = 0
    var serialNumber:String = ""
    var waitForResponse = 0
    let supportedCodeTypes = [AVMetadataObjectTypeUPCECode,
                              AVMetadataObjectTypeCode39Code,
                              AVMetadataObjectTypeCode39Mod43Code,
                              AVMetadataObjectTypeCode93Code,
                              AVMetadataObjectTypeCode128Code,
                              AVMetadataObjectTypeEAN8Code,
                              AVMetadataObjectTypeEAN13Code,
                              AVMetadataObjectTypeAztecCode,
                              AVMetadataObjectTypePDF417Code,
                              AVMetadataObjectTypeQRCode]
    
    let bikerModel = BikerViewModel()
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if(trackFlow == 0){
            trackFlow = 1
            setUpAndStartBarcodeScanner()
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(refreshUI), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        bikerModel.bikerDelegate = self
        indicatorView.isHidden = true
        self.navigationItem.title = "DEVICE SETUP"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        let image = #imageLiteral(resourceName: "Next Arrow").withRenderingMode(.alwaysTemplate)
        imgVwNextArrow.image = image
        imgVwNextArrow.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.showChecklistAlert()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            self.view.makeToastActivity(.center, isUserInteraction: false)
            self.refreshUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        captureSession?.stopRunning()
    }
    
    func refreshUI() {
            if let _ = self.captureSession{
                if !(self.captureSession?.isRunning)!{
                    self.captureSession?.startRunning()
                }
            }
            
            self.waitForResponse = 0
            self.lblPairingNotSuccessful.isHidden = true
            self.view.hideToastActivity()
    }
    
    func showChecklistAlert() {
        checklistPopup.isHidden = false
        if MyDefaults.getBoolData(key: DefaultKeys.KEY_DMREGISTER) {
            chceklistTxt.text = StringConstant.checklistTxtOnline
        } else {
            chceklistTxt.text = StringConstant.checklistTxtOffline
        }
    }
    
    @IBAction func onClickContinue(_ sender: UIButton) {
        checklistPopup.isHidden = true
    }
    
    func setUpAndStartBarcodeScanner() {
        
        print("Setting up scanner")
        
        vwBarcodeCapture.isHidden = false
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video as the media type parameter.
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            
            // Set the input device on the capture session.
            captureSession?.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
            //captureMetadataOutput.rectOfInterest = previewLayer.metadataOutputRectOfInterestForRect(vwBarcodeCapture.bounds)
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer?.frame = vwBarcodeCapture.layer.bounds
            vwBarcodeCapture.layer.addSublayer(videoPreviewLayer!)
            
            
            // Start video capture.
            captureSession?.startRunning()
            let visibleRect = videoPreviewLayer?.metadataOutputRectOfInterest(for: vwBarcodeCapture.bounds)
            captureMetadataOutput.rectOfInterest = visibleRect!
            
            
            
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
                qrCodeFrameView.layer.borderWidth = 2
                vwBarcodeCapture.addSubview(qrCodeFrameView)
                vwBarcodeCapture.bringSubview(toFront: qrCodeFrameView)
            }
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print("CM ERROR :************\(error.localizedDescription)************")
            self.showCameraAlert()
            return
        }
    }
    
    func showCameraAlert(){
        let alertController = UIAlertController (title: ToastMsgConstant.alertcamPermMsg , message: ToastMsgConstant.alertcamPermInfo , preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
        //   print(error)
        
    }
    // MARK: - AVCaptureMetadataOutputObjectsDelegate Methods
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
       // print("********************************Multiple Calls*****************************")
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            print("No QR/barcode is detected")
            
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            if let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj){
                qrCodeFrameView?.frame = barCodeObject.bounds
            }
            
            if metadataObj.stringValue != nil {
                if(waitForResponse == 0){
                    waitForResponse = 1
                    print(metadataObj.stringValue)
                    serialNumber = metadataObj.stringValue

                    
                    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
                    do{
                        let dmDetailsArray = try context.fetch(DMDetails.fetchRequest())
                        if dmDetailsArray.count > 0 {
                            if let dmObj = dmDetailsArray.first as? DMDetails {
                                print("a: \(String(describing: dmObj.sr_no)) b: \(serialNumber)")
                                if dmObj.sr_no == "" || dmObj.sr_no == nil {
                                    captureSession?.stopRunning()
                                    vwBarcodeCapture.isHidden = true
                                    indicatorView.isHidden = false
                                    indicatorView.startAnimating()
                                    lblPairingNotSuccessful.isHidden = true
                                    serialNumber = metadataObj.stringValue
                                    let vehicleID = MyDefaults.getInt(key: DefaultKeys.KEY_DefaultVehicleID)
                                    let cmRegistration = CMregistrationhandler()
                                    cmRegistration.cmRegistrationDelegate = self
                                    cmRegistration.validateCM(serialNumber: serialNumber,vehicleID:vehicleID)
                                } else if dmObj.sr_no == serialNumber {
                                    captureSession?.stopRunning()
                                    vwBarcodeCapture.isHidden = true
                                    indicatorView.isHidden = false
                                    indicatorView.startAnimating()
                                    lblPairingNotSuccessful.isHidden = true
                                    serialNumber = metadataObj.stringValue
                                    showDevicePairingForNoInternet()
                                } else {
                                    waitForResponse = 0
                                    // captureSession?.stopRunning()
                                    self.updateErrorMsg(msg:NSLocalizedString("keyWrongDevice", comment: "")  )
                                }
                                
                            }

                        }
                        
                    }
                    catch {
                        
                    }

                    //////*************** Testing Part *********************

//                    indicatorView.isHidden = true
//                    indicatorView.stopAnimating()
//                    vwBarcodeCapture.isHidden = false
//                    captureSession?.startRunning()
//                    vwBarcodeCapture.removeSubviews()
//
//                    let successVC = DMPairingVC()
//                    successVC.serialNumber = serialNumber
//                    MyDefaults.setData( value: serialNumber, key: DefaultKeys.KEY_CMSERIAL_NO)
//                    self.pushVC(successVC)

                    //////************************************
                }
                waitForResponse = 0
            }
        }
    }
    
    func showDevicePairingForNoInternet() {
        print("Success DM")
        indicatorView.isHidden = true
        indicatorView.stopAnimating()
        vwBarcodeCapture.isHidden = false
        captureSession?.startRunning()
        vwBarcodeCapture.removeSubviews()

        let successVC = DMPairingVC()
        successVC.delegate = self
        successVC.serialNumber = serialNumber
        //MyDefaults.setData( value: serialNumber, key: DefaultKeys.KEY_CMSERIAL_NO)
        self.pushVC(successVC)

    }
    
    func onCMRegistrationSuccess(data:[String:Any]){
        print("Suceess CM\(data)")
        indicatorView.isHidden = true
        indicatorView.stopAnimating()
        vwBarcodeCapture.isHidden = false
        captureSession?.startRunning()
        vwBarcodeCapture.removeSubviews()
        
        let userObject = UserManagerHandler()
        userObject.userManagerDelegate = self
        userObject.getActiveDeviceMapping()
        AppDetailsHandler.sharedInstance.isValueChanged(changedValue: serialNumber, infoToChange: DMInfo.SerialNo)
        bikerModel.getBiker(bikerID: MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID))
        
        let successVC = DMPairingVC()
        successVC.delegate = self
        successVC.serialNumber = serialNumber
        //MyDefaults.setData( value: serialNumber, key: DefaultKeys.KEY_CMSERIAL_NO)
        self.pushVC(successVC)
    }
    
    func onFailure(Error:[String:Any]){
        print(Error)
        
        let font = lblPairingNotSuccessful.font
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: "Not Successful")
        
        let mid = (font?.descender)! + (font?.capHeight)!
        attachment.bounds = CGRect(x: 0, y: (font?.descender)! - (attachment.image?.size.height)! / 2 + mid + 2, width: (attachment.image?.size.width)!, height: (attachment.image?.size.height)!).integral
        
        
        let attachmentString = NSAttributedString(attachment: attachment)
        let errModel:ErrorModel = Error["data"] as! ErrorModel
        waitForResponse = 0
        lblPairingNotSuccessful.isHidden = false
        if(errModel.description != ""){
            let myString = NSMutableAttributedString(string: errModel.description!+"  ")
            myString.append(attachmentString)
            lblPairingNotSuccessful.attributedText = myString
            lblPairingNotSuccessful.fitHeight()
        }
        vwBarcodeCapture.isHidden = false
        indicatorView.isHidden = true
        indicatorView.stopAnimating()
        captureSession?.startRunning()
        vwBarcodeCapture.removeSubviews()
        // Delay 5 seconds
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            self.lblPairingNotSuccessful.isHidden = true
            let myString = NSMutableAttributedString(string: ToastMsgConstant.failPairingMsg)
            myString.append(attachmentString)
            self.lblPairingNotSuccessful.attributedText = myString
            self.lblPairingNotSuccessful.fitHeight()
        }
    }
    
    func updateErrorMsg(msg: String) {
        DispatchQueue.main.async {
            self.lblPairingNotSuccessful.isHidden = false
            self.lblPairingNotSuccessful.text = msg
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            self.lblPairingNotSuccessful.isHidden = true
            //self.captureSession?.startRunning()
        }
    }
    
    @IBAction func btnSkipAction(_ sender: UIButton) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if (appDelegate.window?.rootViewController is HomeContainerVC)
        {
            self.dismissVC(completion: nil)
        }
        else
        {
            let navigationController = UINavigationController(rootViewController: HomeContainerVC())
            let mainViewController = navigationController
            let leftViewController = SideMenuVC()
            let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
            slideMenuController.changeLeftViewWidth(self.view.size.width-60)
            appDelegate.window?.rootViewController = slideMenuController
        }
    }
    
    @IBAction func btnBuyDeviceAction(_ sender: UIButton) {
        
        if let amazonUrl = URL(string: NSLocalizedString("keyAmazonUrl", comment: "")) {
            UIApplication.shared.open(amazonUrl, options: [:]) { (status) in
                print("Open amazon with status \(status)")
            }
        }
        
    }
    
    
    
    func activeDeviceIsMapped(data: [String : Any]) {
        
        if let activeModel:ActiveDevicesModel = data[StringConstant.data] as! ActiveDevicesModel?{
            
            if activeModel.device_unit_id != nil{
                MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_CMREGISTER)//-----Save CM Registration Status
                MyDefaults.setData( value: activeModel.device_unit_id!, key: DefaultKeys.KEY_CMSERIAL_NO)
            }

            
            if activeModel.display_module_unit_id != nil{
                MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_DMREGISTER)//-----Save DM Registration Status
            }
        }
    }
    
    func noActiveDeviceIsMapped(Error: [String : Any]) {
        
        MyDefaults.setBoolData(value: false, key: DefaultKeys.KEY_CMREGISTER)//-----Save CM Registration Status
        MyDefaults.setBoolData(value: false, key: DefaultKeys.KEY_DMREGISTER)//-----Save DM Registration Status
    }
    
    func bikerRegistrationSuccess() {
        
    }
    
    func authenticationFail(data: [String : Any]) {
        let successVC = DMPairingVC()
        successVC.delegate = self
        successVC.serialNumber = serialNumber
        //MyDefaults.setData( value: serialNumber, key: DefaultKeys.KEY_CMSERIAL_NO)
        self.pushVC(successVC)
    }
    
}
