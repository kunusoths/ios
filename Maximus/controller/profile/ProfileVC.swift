 //
 //  ProfileVC.swift
 //  Maximus
 //
 //  Created by kpit on 21/02/17.
 //  Copyright © 2017 com.kpit.maximus. All rights reserved.
 //
 
 import UIKit
 import ObjectMapper
 import Contacts
 import UserNotifications
 import CropViewController
 
 class ProfileVC: UIViewController, UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
    
    // MARK: UIViewControl Reference Outlet
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var profileImgEditBtn: UIButton!
    @IBOutlet weak var lblUserProfileName: UILabel!
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var switchShareLocation: UISwitch!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnPhoneNumber: UIButton!
    @IBOutlet weak var btnAddContacts: UIButton!
    
    @IBOutlet weak var profileView: UIView!
    
    @IBOutlet weak var bikeModelBtn: UIButton!
    @IBOutlet weak var bikeMakeBtn: UIButton!
    @IBOutlet weak var viewScrollContent: UIView!
    @IBOutlet weak var btnMonth: UIButton!
    @IBOutlet weak var btnYear: UIButton!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var viewPickerView: UIView!
    
    @IBOutlet weak var lblBikeMake: UILabel!
    @IBOutlet weak var lblBikeModel: UILabel!
    @IBOutlet weak var btnDone: UIBarButtonItem!
    
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var addContactVerticalConstant: NSLayoutConstraint!
    @IBOutlet weak var emailTxtField: UITextField!
    var flagMonthYearPicker = 0
    @IBOutlet weak var addContactsViewHtConstraint: NSLayoutConstraint!
    @IBOutlet weak var invalidEmail: UILabel!
    
    // MARK: Class variable
    var strShareLocationAllowed : Bool? = nil
    var allPhoneBookContact = [PhoneBookContact]()
    var userObject = UserManagerHandler()
    var bikerID = Int()
    var contentInsetDefault:UIEdgeInsets!
    var tagPhoneBookContact = [PhoneBookContact]()
    var counterEmergencyCnt = Int()
    var bikerData:Biker?
    var isOtpValidate = false
    var profileImgUpdated = false
    
    var pickerData: [String] = [String]()
    var bikeModelList = [BikeModelList]()
    var month:Int? = nil
    var filterString:[String] = []
    var bikeModels:[String] = []
    let tapGesture = UITapGestureRecognizer()
    let switchDisableColor = UIColor(r: 14, g: 22, b: 28)
    let journeyManager = JourneyManager.sharedInstance
    var selectedBikeMake = ""
    var selectedMonthBeforeSave = -1
    var selectedYearBeforeSave = -1
    var selectedRow: Int?
    var oldEmailID: String?
    let profileContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    var bikerDetails:BikerDetails!
    let imgUploaderObj = ImageUploader()
    let imagePicker = UIImagePickerController()
    var croppingStyle = CropViewCroppingStyle.default
    
    var croppedRect = CGRect.zero
    var croppedAngle = 0
    
    // MARK: override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setBikeMake(bikeType: "")
        self.setBikeModel(bikeModel: "")
        self.navigationController!.navigationBar.isTranslucent = false
        self.viewPickerView.isHidden = true
        self.profileView.backgroundColor = AppTheme.THEME_ORANGE
        self.addNavigationBar(isEdit: false)
        self.slideMenuController()?.addLeftGestures()
        
        bikerDetails = BikerDetails.getBikerDetails(context: profileContext)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        imgProfile.setCornerRadius(radius: self.imgProfile.frame.width/2)
        
        self.btnPhoneNumber.setTitle(bikerDetails.phone_no, for: .normal)
        self.btnPhoneNumber.addTarget(self, action: #selector(onPhoneButtonClick), for: UIControlEvents.touchUpInside)
        
        tagListView.delegate = self
        tagListView.textFont = UIFont.systemFont(ofSize: 13)
        self.bikerDetails = BikerDetails.getBikerDetails(context: profileContext)
        bikerID = Int(bikerDetails.biker_id ?? "0")!
        userObject.userManagerDelegate = self
        
        oldEmailID = (MyDefaults.getData(key: DefaultKeys.KEY_BIKER_EMAIL) as? String)?.lowercased()
        // imgProfile.loadImageUsingCacheWithUrlString(urlString: MyDefaults.getData(key: DefaultKeys.KEY_PROFILE_URL) as! String, indexPath: 0)
        
        imgProfile.loadImageUsingCacheWithUrlString(urlString: MyDefaults.getData(key: DefaultKeys.KEY_PROFILE_URL) as! String, indexPath: 0)
        self.getBikeList()
        MyDefaults.setBoolData(value: false, key: DefaultKeys.KEY_RELOGIN)
        
        if NetworkCheck.isNetwork() {
            self.getBikeList()
            self.view.makeToastActivity(.center ,isUserInteraction:false)
            userObject.getBiker(bikeID:bikerID)
        } else {
            updateUIForOfflineMode()
        }
        
        tapGesture.addTarget(self, action:#selector(self.hidePickerOnTap))
        tapGesture.numberOfTouchesRequired = 1
        tapGesture.numberOfTapsRequired = 1
        self.view!.addGestureRecognizer(tapGesture)
        emailTxtField.delegate = self
        
        if MyDefaults.getBoolData(key: "isEditModeOn") {
            self.addNavigationBar(isEdit: true)
            emailTxtField.becomeFirstResponder()
            MyDefaults.setBoolData(value: false, key: "isEditModeOn")
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.navigationItem.rightBarButtonItem?.tintColor = .white
        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.width / 2.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //  lblUserProfileName.text = MyDefaults.getData(key: DefaultKeys.KEY_USERNAME) as? String
        lblUserProfileName.text =  (bikerDetails.first_name ?? "") + " " + (bikerDetails.last_name ?? "")
        
        switchShareLocation.addTarget(self, action: #selector(ProfileVC.switchValueDidChange(_:)), for: .valueChanged)
        addButtonUIUpdate()
        self.updateViewAsPerLogin()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func updateViewAsPerLogin() {
        if bikerDetails.auth_method ?? AUTH_METHOD.UNKNOWN.rawValue == AUTH_METHOD.USERNAME_PASSWORD.rawValue{
            //show email
            self.emailHeightConstraint.constant = 80.0
            self.addContactVerticalConstant.constant = 81.0
            self.emailView.isHidden = false
        } else {
            // hide email
            self.emailView.isHidden = true
            self.emailHeightConstraint.constant = 0.0
            self.addContactVerticalConstant.constant = 0.0
        }
        adjustContactView()
        self.view.layoutIfNeeded()
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if(lblBikeMake.text != ""){
            self.lblBikeMake.isHidden = false
        }
        
        if(lblBikeModel.text != ""){
            self.lblBikeModel.isHidden = false
        }
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.scrollView.isScrollEnabled = true
    }
    
    func emailValidation(text: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: text)
    }
    
    @IBAction func onClickBikeMake(_ sender: UIButton) {
        if bikeModelList.count > 0 {
            let detailsVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BikeDataTableViewController") as? BikeDataTableViewController
            detailsVC?.bikeModelData = bikeModelList
            detailsVC?.isBikeMake = false
            detailsVC?.delegate = self
            self.pushVC(detailsVC!)
        } else {
            if NetworkCheck.isNetwork(){
                self.view.makeToast(NSLocalizedString("keyBikeMakeNotAvailable", comment: ""))
            }else{
                self.view.makeToast(ToastMsgConstant.network)
            }
        }
        
    }
    @IBAction func onClickBikeModel(_ sender: UIButton) {
        if selectedBikeMake == "" {
            self.view.makeToast("Please select Bike Make")
        } else {
            if bikeModelList.count > 0 {
                let detailsVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BikeDataTableViewController") as? BikeDataTableViewController
                detailsVC?.bikeModelData = bikeModelList
                detailsVC?.isBikeMake = true
                detailsVC?.selectedBike = selectedBikeMake.trimmingCharacters(in: .whitespacesAndNewlines)
                detailsVC?.delegate = self
                self.pushVC(detailsVC!)
            } else {
                self.view.makeToast(ToastMsgConstant.network)
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.scrollView.isScrollEnabled = true
    }
    // it hides the pickerview on anywhere tapping
    func hidePickerOnTap(_ sender: UITapGestureRecognizer) {
        self.viewPickerView.isHidden = true
    }
    
    @IBAction func btnCancelPickerClicked(_ sender: UIBarButtonItem) {
        self.viewPickerView.isHidden = true
    }
    
    @IBAction func btnDonePickerClicked(_ sender: UIBarButtonItem) {
        self.viewPickerView.isHidden = true
        if(flagMonthYearPicker == 0){
            if let row = selectedRow {
                month = row + 1
                if !(row > pickerData.count) {
                    btnMonth.setTitle(pickerData[row], for: UIControlState.normal)
                }
            } else {
                btnMonth.setTitle("Month", for: UIControlState.normal)
            }
            
        }else{
            if let row = selectedRow {
                if !(row > pickerData.count) {
                    btnYear.setTitle(pickerData[row], for: UIControlState.normal)
                }
            } else {
                btnYear.setTitle("Year", for: UIControlState.normal)
            }
        }
    }
    
    @IBAction func btnMonthPickerClicked(_ sender: UIButton) {
        view.endEditing(true)
        
        flagMonthYearPicker = 0
        self.viewPickerView.isHidden = false
        let formatter = DateFormatter()
        let monthComponents = formatter.monthSymbols
        pickerData = monthComponents!
        
        self.pickerView.reloadAllComponents()
    }
    
    
    @IBAction func btnYearPickerClicked(_ sender: UIButton) {
        view.endEditing(true)
        flagMonthYearPicker = 1
        self.viewPickerView.isHidden = false
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let array = (1947...year).map { "\($0)" }
        
        pickerData = array.reversed()
        
        self.pickerView.reloadAllComponents()
    }
    
    func setBikeMake(bikeType: String){
        
        if(bikeType != ""){
            self.lblBikeMake.isHidden = false
            self.lblBikeMake.text = bikeType.trimmingCharacters(in: .whitespacesAndNewlines)
        }else{
            self.lblBikeMake.isHidden = true
        }
    }
    
    func setBikeModel(bikeModel: String){
        
        if(bikeModel != ""){
            self.lblBikeModel.isHidden = false
            self.lblBikeModel.text = bikeModel.trimmingCharacters(in: .whitespacesAndNewlines)
        }else{
            self.lblBikeModel.isHidden = true
        }
    }
    
    //MARK:PickerView Delegate & DataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedRow = row
        print(pickerData[row])
    }
    
    //MARK:- Functional Methods
    
    func getBikeList() {
        if NetworkCheck.isNetwork() {
            userObject.getAllBikes()
        }
    }
    
    
    func addNavigationBar(isEdit:Bool)  {
        
        if isEdit {
            
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationItem.setHidesBackButton(true, animated:false)
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title:"Cancel", style: .plain, target: self, action: #selector(backTapped))
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "SAVE", style: .plain, target: self, action: #selector(onSaveClicked))
            self.navigationItem.rightBarButtonItem?.accessibilityIdentifier = "btn_save_profile"
            self.navigationItem.leftBarButtonItem?.accessibilityIdentifier = "btn_cancel_profile"
            self.navigationItem.title = "EDIT PROFILE"
            self.navigationItem.leftBarButtonItem?.tintColor = .white
            self.navigationItem.rightBarButtonItem?.tintColor = .white
            self.viewScrollContent.isUserInteractionEnabled = true
            btnAddContacts.isUserInteractionEnabled = true
        }
        else{
            
            
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationItem.setHidesBackButton(true, animated:false)
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Ham Menu"), style: .plain, target: self, action: #selector(menuTapped))
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Edit Button"), style: .plain, target: self, action: #selector(onEditClicked))
            self.navigationItem.rightBarButtonItem?.accessibilityIdentifier = "btn_edit_profile"
            self.navigationItem.leftBarButtonItem?.accessibilityIdentifier = "btn_hamb_menu"
            self.navigationItem.title = "MY PROFILE"
            self.viewScrollContent.isUserInteractionEnabled = false
            btnAddContacts.isUserInteractionEnabled = false
        }
    }
    
    //MARK: - Keyborad Methods
    var isKyboardshown:Bool = false
    func keyboardWillShow(notification:NSNotification){
        //give room at the bottom of the scroll view, so it doesn't cover up anything the user needs to tap
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        if !isKyboardshown{
            isKyboardshown = true
            contentInsetDefault = self.scrollView.contentInset
            var contentInset:UIEdgeInsets = self.scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            self.scrollView.contentInset = contentInset
        }
        
    }
    
    func keyboardWillHide(notification:NSNotification){
        //let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        if isKyboardshown {
            isKyboardshown = false
            self.scrollView.contentInset = contentInsetDefault
            self.scrollView.layoutSubviews()
            
        }
    }
    
    // MARK: - Private Methods
    
    func showCameraOptions()  {
        self.croppingStyle = .circular
        let alertController = UIAlertController(title: nil, message: nil , preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: NSLocalizedString("keyTakePhoto", comment: ""), style: .default){
            (result : UIAlertAction) -> Void in
            self.openCamera()
        }
        let libraryAction = UIAlertAction(title:NSLocalizedString("keyChooseFromGallery", comment: ""), style: .default){
            (result : UIAlertAction) -> Void in
            self.openPhotoLibrary()
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("keyCancel", comment: ""), style: .cancel, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(libraryAction)
        alertController.addAction(cancelAction)

        
        present(alertController, animated: true, completion: nil)
    }
    
    func openCamera(){
        self.imagePicker.sourceType = .camera
        self.imagePicker.allowsEditing = false
        self.imagePicker.delegate = self
        self.present(imagePicker, animated: true)
    }
    
    func openPhotoLibrary() {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        self.present(imagePicker, animated: true)
    }
    
    func uploadImage(image:UIImage)  {
        let imageData = UIImageJPEGRepresentation(image, 0.2)
        let uploadUrl = "\(RequestURL.URL_BASE.rawValue)\(RequestURL.URL_BIKER.rawValue)\(RequestURL.URL_PROFILE_IMAGE.rawValue)"
        
        imgUploaderObj.uploadImage(data: imageData!, url: uploadUrl, successCallback: { (data) in
            MyDefaults.setData(value: ((data as! [String:Any])["data"] as! ImageUploadModel).imgPath!, key: DefaultKeys.KEY_PROFILE_URL)
            self.bikerDetails.profile_image_url = ((data as! [String:Any])["data"] as! ImageUploadModel).imgPath
            self.view.hideToastActivity()
        }) { (error) in
            self.view.hideToastActivity()
        }
    }
    
    func onPhoneButtonClick() {
        isOtpValidate = true
        MyDefaults.setData(value: fromViewcontroller.ProfileVC.rawValue, key: DefaultKeys.KEY_FROMWHERE)
        let navigationController = UINavigationController(rootViewController: PhoneVerificationVC())
        self.presentVC(navigationController)
    }
    
    // MARK: Navigation Bar Actions
    func backTapped(){
        profileImgUpdated = false
        if tagPhoneBookContact.count > 0 {
            tagPhoneBookContact.removeAll()
        }
        viewDidLoad()
        addButtonUIUpdate()
        viewWillLayoutSubviews()
    }
    
    func menuTapped() {
        self.slideMenuController()?.openLeft()
    }
    
    func onEditClicked() {
        self.addNavigationBar(isEdit: true)
        btnAddContacts.setImage(#imageLiteral(resourceName: "Add Contact White"), for: .normal)
        profileImgEditBtn.isHidden = false
        profileImgUpdated = false
    }
    
    func onSaveClicked() {
        
        print("tagPhoneBookContact count - ",tagPhoneBookContact.count)
        
        var isUpdate:Bool = true
        
        if !(lblBikeMake.text?.isEmpty)! && (lblBikeModel.text?.isEmpty)! {
            isUpdate = false
            self.view.makeToast("Bike model should not be empty")
        }
        else if ((btnYear.titleLabel?.text == "Year" && btnMonth.titleLabel?.text != "Month")||(btnYear.titleLabel?.text != "Year" && btnMonth.titleLabel?.text == "Month")){
            isUpdate = false
            print("txt ",Int((btnYear.titleLabel?.text!)!) ?? 0)
            let year = Int((btnYear.titleLabel?.text!)!) ?? 0
            if year < 1 {
                self.view.makeToast("Please Enter Registration year")
                return
            }
            print("txt ",Int((btnMonth.titleLabel?.text!)!) ?? 0)
            let month = Int((btnMonth.titleLabel?.text!)!) ?? 0
            if month < 1 {
                self.view.makeToast("Please Enter Registration month")
            }
            
        }
        
        self.viewPickerView.isHidden = true
        
        if isUpdate == true
        {
            if NetworkCheck.isNetwork() {
                if !emailValidation(text: emailTxtField.text!) && MyDefaults.getData(key: DefaultKeys.KEY_FBACCESSTOKEN) as! String == "" {
                    self.view.endEditing(true)
                    self.view.makeToast("Enter a valid email address.")
                }
                else  {
                    selectedMonthBeforeSave = Int((btnMonth.titleLabel?.text!)!) ?? 0
                    selectedYearBeforeSave = Int((btnYear.titleLabel?.text!)!) ?? 0
                    self.addNavigationBar(isEdit: false)
                    btnAddContacts.setImage(#imageLiteral(resourceName: "Add Contacts"), for: .normal)
                    profileImgEditBtn.isHidden = true
                    if journeyManager.isAnyRideInProgress()
                    {
                        if NetworkCheck.isNetwork() {
                            //tfBikeMake.isUserInteractionEnabled = false
                            //tfBikeModel.isUserInteractionEnabled = false
                            btnMonth.isUserInteractionEnabled = false
                            btnYear.isUserInteractionEnabled = false
                            self.upDateProfile()
                            
                        }
                    }else{
                        if NetworkCheck.isNetwork() {
                            ///tfBikeMake.isUserInteractionEnabled = true
                            //tfBikeModel.isUserInteractionEnabled = true
                            btnMonth.isUserInteractionEnabled = true
                            btnYear.isUserInteractionEnabled = true
                            self.upDateProfile()
                            self.updateVehicle()
                        }
                    }
                }
            } else {
                self.view.makeToast(ToastMsgConstant.network)
            }
        }
        
        
    }
    
    func onCloseClicked() {
        if tagPhoneBookContact.count <= 0 {
            self.view.makeToast("Please Add Top 3 Contacts")
        }else{
            self.addNavigationBar(isEdit: false)
            btnAddContacts.setImage(#imageLiteral(resourceName: "Add Contacts"), for: .normal)
            profileImgEditBtn.isHidden = true
        }
        
        if let biker = self.bikerData {
            self.updateUI(biker: biker)
        }
        
        
    }
    func addEmerencyContactTags() {
        print("addEmerencyContactTags count ",tagPhoneBookContact.count)
        
        tagListView.removeAllTags()
        for item in tagPhoneBookContact {
            self.tagListView.addTag(item.name)
        }
        adjustContactView()
    }
    
    func upDateProfile()  {
        var contactArray = [Any]()
        
        for  element in self.tagPhoneBookContact
        {
            let contactDict = NSMutableDictionary()
            contactDict.setValue(element.name, forKey: "name")
            contactDict.setValue(element.phoneNo, forKey: "phone")
            contactArray.append(contactDict)
        }
        var json :[String:Any] = [:]
        if MyDefaults.getData(key: DefaultKeys.KEY_FBACCESSTOKEN) as! String == "" {
            // email
            if let biker = self.bikerDetails {
                json=["share_location":strShareLocationAllowed ?? true,
                      "emergency_contacts":contactArray,
                      "id":bikerID,
                      "email1": emailTxtField.text ?? (biker.email ?? ""),
                      "zone_offset": TimeZone.current.abbreviation()!]
            }
        } else {
            //facebook
            json=["share_location":strShareLocationAllowed ?? true,
                  "emergency_contacts":contactArray,
                  "id":bikerID,
                  "zone_offset": TimeZone.current.abbreviation()!]
        }
        
        print("json ",json)
        if NetworkCheck.isNetwork() {
            self.view.makeToastActivity(.center ,isUserInteraction:false)
            userObject.updateBiker(parameter: json)
            //self.view.makeToastActivity(.center ,isUserInteraction:false)
            if imgProfile.image != nil && (MyDefaults.getData(key: DefaultKeys.KEY_FBACCESSTOKEN) as! String != "" || MyDefaults.getData(key: DefaultKeys.KEY_TOKEN) as! String != "") && profileImgUpdated {
                self.uploadImage(image: self.imgProfile.image!)
            }
        } else {
            self.view.makeToast(ToastMsgConstant.network)
        }
    }
    
    func updateVehicle()  {
        let vehicleID = self.bikerDetails.default_vehicle_id //MyDefaults.getInt(key: DefaultKeys.KEY_DefaultVehicleID)
        
        let json : [String:Any] = ["id":vehicleID,
                                   "registration_month":month ?? "",
                                   "registration_year":Int((btnYear.titleLabel?.text!)!) ?? "",
                                   "company":lblBikeMake.text ?? "" ,
                                   "model":lblBikeModel.text ?? "" ]
        print("JSON ",json)
        if NetworkCheck.isNetwork() {
            //self.view.makeToastActivity(.center ,isUserInteraction:false)
            userObject.updateVehicles(parameter: json)
        } else {
            self.view.makeToast(ToastMsgConstant.network)
        }
    }
    
    //MARK: - Action methods
    @IBAction func onClickEditProfileImage(_ sender: Any) {
        self.view.endEditing(true)
        showCameraOptions()
    }
    
    @IBAction func btnAddContactsAction(_ sender: UIButton) {
        isOtpValidate = false
        print("lblBikeModel ",lblBikeModel.text ?? "abc")
        print("lblBikeMake ",lblBikeMake.text ?? "abc")
        
        if tagPhoneBookContact.count < 3 {
            self.counterEmergencyCnt = self.tagPhoneBookContact.count
            let contactPickerScene = EPContactsPicker(delegate: self, multiSelection:true, subtitleCellType: SubtitleCellValue.email,count: self.tagPhoneBookContact.count)
            let navigationController = UINavigationController(rootViewController: contactPickerScene)
            self.present(navigationController, animated: true, completion: nil)
        }
        else{
            self.counterEmergencyCnt = 0
            self.view.makeToast("You can add upto 3 contacts")
        }
    }
    
    // MARK: - Share location Switch Listener
    
    func switchValueDidChange(_ sender: UISwitch) {
        
        if sender.isOn
        {
            strShareLocationAllowed = true
            print("onShareLocationValueChanged : true")
        }else
        {
            strShareLocationAllowed = false
            print("onShareLocationValueChanged : false")
        }
    }
    
    // MARK: UIView Control Validation
    func validatePhoneNumber() {
        let navigationController = UINavigationController(rootViewController: PhoneVerificationVC())
        self.presentVC(navigationController)
    }
    
    func updateUI(biker:Biker){
        
        self.btnPhoneNumber.setTitle(biker.phone_mobile1, for: .normal)
        //   lblUserProfileName.text = MyDefaults.getData(key: DefaultKeys.KEY_USERNAME) as? String
        lblUserProfileName.text =  (bikerDetails.first_name ?? "") + " " + (bikerDetails.last_name ?? "")
        MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_EMERGENCYCONTACTADDED)
        // after adding emergency contacts remove local notification
        Constants.appDelegate?.cancelNotification(identifierString: NotificationIdentifier.contactIdentifier)
        if let isLocationOn = biker.share_location {
            switchShareLocation.isOn =  isLocationOn
        }
        
        if let isEmailVerified = biker.email1_verified {
            self.emailTxtField.isUserInteractionEnabled = true
            self.invalidEmail.isHidden = true
            if !isEmailVerified {
                self.invalidEmail.isHidden = false
            } else if isEmailVerified {
                MyDefaults.setData(value: biker.email1!, key: DefaultKeys.KEY_BIKER_EMAIL)
            }
        } else {
            //pending
            self.emailTxtField.isUserInteractionEnabled = false
            self.invalidEmail.text = "Verification is in progress."
        }
        
        if MyDefaults.getData(key: DefaultKeys.KEY_FBACCESSTOKEN) as! String == "" {
            self.emailTxtField.text = biker.email1 ?? ""
            oldEmailID = biker.email1?.lowercased()
        }
        
        if let vehicle:DefaultVehicle = biker.default_vehicle  {
            
            if vehicle.registration_month != nil && vehicle.registration_year != nil {
                month = vehicle.registration_month!
                self.updateVehicleUI(vehicle: vehicle)
                
            }
            setBikeMake(bikeType: vehicle.company ?? "company")
            setBikeModel(bikeModel: vehicle.model ?? "model")
        }
        
        if tagPhoneBookContact.count > 0 {
            tagPhoneBookContact.removeAll()
        }
        
        for elements in biker.emergency_contacts! {
            let eCnt = PhoneBookContact()
            eCnt.name = elements.name!
            eCnt.phoneNo = elements.phone!
            tagPhoneBookContact.append(eCnt)
        }
        
        self.addEmerencyContactTags()
        
        if let count = biker.emergency_contacts?.count, count > 0 {
            MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_EMERGENCYCONTACTADDED)
        } else {
            MyDefaults.setBoolData(value: false, key: DefaultKeys.KEY_EMERGENCYCONTACTADDED)
            self.fireEmergencyContactNotification()
        }
        
        if imgProfile.image != nil && (MyDefaults.getData(key: DefaultKeys.KEY_FBACCESSTOKEN) as! String != "" || MyDefaults.getData(key: DefaultKeys.KEY_TOKEN) as! String != "") {
            //            self.uploadImage(image: self.imgProfile.image!)
        }
        else{
            if !profileImgUpdated{
                self.view.hideToastActivity()
            }
        }
        
    }
    
    func updateVehicleUI(vehicle:DefaultVehicle)  {
        if let make = vehicle.company {
            selectedBikeMake = make
            self.bikeModelBtn.isUserInteractionEnabled = true
        }
        setBikeMake(bikeType: vehicle.company ?? "company")
        setBikeModel(bikeModel: vehicle.model ?? "model")
        selectedYearBeforeSave = vehicle.registration_year ?? -1
        selectedMonthBeforeSave = vehicle.registration_month ?? -1
        if let regYear = vehicle.registration_year, selectedYearBeforeSave != -1 {
            btnYear.setTitle(String(describing: regYear), for: UIControlState.normal)
        } else {
            btnYear.setTitle("Year", for: .normal)
        }
        if let regMonth = vehicle.registration_month, selectedMonthBeforeSave != -1 {
            let monthName = DateFormatter().monthSymbols[regMonth - 1]
            print(monthName)
            btnMonth.setTitle(String(describing: monthName), for: UIControlState.normal)
        } else {
            btnMonth.setTitle("Month", for: .normal)
        }
        if !profileImgUpdated{
            self.view.hideToastActivity()
        }
    }
    
    func updateUIForOfflineMode() {
        let vehicle = self.bikerDetails.vehicleObj//BikerDetails.getVehicle(context: profileContext)
        //setBikeMake(bikeType: MyDefaults.getData(key: "Make") as! String )
        setBikeMake(bikeType: vehicle?.make ?? "")
        //setBikeModel(bikeModel: MyDefaults.getData(key: "Model") as! String)
        setBikeModel(bikeModel: vehicle?.model ?? "")
        //selectedYearBeforeSave = MyDefaults.getInt(key: "RegYear")
        selectedYearBeforeSave = Int((vehicle?.registrationYear) ?? "-1") ?? -1
        //selectedMonthBeforeSave = MyDefaults.getInt(key: "RegYear")
        
        selectedMonthBeforeSave = Int((vehicle?.registrationMonth) ?? "-1") ?? -1
        if selectedYearBeforeSave != -1 {
            let regYear = Int((vehicle?.registrationYear)!)! //MyDefaults.getInt(key: "RegYear")
            btnYear.setTitle(String(describing: regYear), for: UIControlState.normal)
        } else {
            btnYear.setTitle("Year", for: .normal)
        }
        if selectedMonthBeforeSave != -1 {
            var regMonth = Int((vehicle?.registrationMonth!)!)! //MyDefaults.getInt(key: "RegMonth")
            if regMonth == 0 {
                regMonth = 1
            }
            let monthName = DateFormatter().monthSymbols[regMonth - 1]
            month = regMonth
            btnMonth.setTitle(String(describing: monthName), for: UIControlState.normal)
        } else {
            btnMonth.setTitle("Month", for: .normal)
        }
        
        
        let contacts:[Any]  = bikerDetails.emergency_contacts as! [Any]
        
        // let contactArray: [Any] = MyDefaults.getArray(key: "Contacts")
        for i in 0 ..< contacts.count {
            let eCnt = PhoneBookContact()
            let dict:[String: Any] = contacts[i] as! [String : Any]
            //let contact = contacts[i]
            eCnt.name = dict["name"] as! String
            eCnt.phoneNo = dict["phone"] as! String
            self.tagPhoneBookContact.append(eCnt)
        }
        let isEmailVerified = bikerDetails.is_email_verified
        switch isEmailVerified {
        case -1:
            //pending
            self.emailTxtField.isUserInteractionEnabled = false
            self.invalidEmail.text = "Verification is in progress."
            break
        case 0:
            self.emailTxtField.isUserInteractionEnabled = true
            self.invalidEmail.isHidden = false
            break
        case 1:
            self.emailTxtField.isUserInteractionEnabled = true
            self.invalidEmail.isHidden = true
            break
        default:
            break
        }
        
        
        switchShareLocation.isOn = bikerDetails.share_location //MyDefaults.getBoolData(key: "ShareLocation")
        self.addEmerencyContactTags()
        if MyDefaults.getData(key: DefaultKeys.KEY_FBACCESSTOKEN) as! String == "" {
            //self.emailTxtField.text = MyDefaults.getData(key: DefaultKeys.KEY_FB_EMAIL) as? String
            self.emailTxtField.text = bikerDetails.email
        }
        
    }
    
    func fireEmergencyContactNotification() {
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = NotificationConstant.Title_Maximus
        notificationContent.subtitle = ""
        notificationContent.body = NotificationConstant.emergency_Notificatio_body
        notificationContent.sound = UNNotificationSound.default()
        notificationContent.categoryIdentifier = NotificationCategoyConstant.categroy_emergencyContacts
        notificationContent.userInfo = ["type":NotificationCategoyConstant.categroy_emergencyContacts]
        
        // Add Trigger immediately
        let notificationTrigger =  UNTimeIntervalNotificationTrigger(timeInterval: 24*60*7, repeats: true)
        
        let notificationRequest = UNNotificationRequest.init(identifier: NotificationIdentifier.contactIdentifier, content: notificationContent, trigger: notificationTrigger)
        
        let center = UNUserNotificationCenter.current()
        center.add(notificationRequest)
    }
 }
 
 
 // MARK: - Image Picker Delegate Method
 extension ProfileVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate,CropViewControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = (info[UIImagePickerControllerOriginalImage] as? UIImage) else { return }
        
        let cropController = CropViewController(croppingStyle: croppingStyle, image: image)
        cropController.delegate = self
        cropController.title = "Move and Scale"
        cropController.aspectRatioPreset = .presetSquare; //Set the initial aspect ratio as a square
        cropController.aspectRatioLockEnabled = true // The crop box is locked to the aspect ratio and can't be resized away from it
        cropController.toolbar.doneTextButton.setTitleColor(UIButton(type: UIButtonType.system).titleColor(for: .normal)!, for: .normal)
        cropController.rotateButtonsHidden = true
        cropController.toolbar.resetButton.isHidden = true
        cropController.aspectRatioPickerButtonHidden = true
        cropController.toolbar.cancelTextButton.setTitleColor(.white, for: .normal)
        cropController.toolbar.doneTextButton.setTitleColor(.white, for: .normal)
        //If profile picture, push onto the same navigation stack
        if picker.sourceType == .camera {
            picker.dismiss(animated: false, completion: {
                self.pushVC(cropController)
            })
        } else {
            picker.pushViewController(cropController, animated: true)
        }
        
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        self.imgProfile.image = image
        profileImgUpdated = true
          if imagePicker.sourceType == .camera {
            cropViewController.popVC()
          }else{
            cropViewController.dismiss(animated: true, completion: nil)
        }
    }
 }
 
 extension ProfileVC:TagListViewDelegate{
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        
        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
        print("count ",self.tagPhoneBookContact.count)
        for  (index  , element) in self.tagPhoneBookContact.enumerated()
        {
            print("index - ",index," elements ",element.name)
            if element.name == title {
                self.tagPhoneBookContact.remove(at: index)
            }
        }
        print("count ",self.tagPhoneBookContact.count)
        addButtonUIUpdate()
        adjustContactView()
    }
    
    func addButtonUIUpdate() {
        if btnAddContacts.isUserInteractionEnabled {
            btnAddContacts.setImage(#imageLiteral(resourceName: "Add Contact White"), for: .normal)
            profileImgEditBtn.isHidden = false
        } else {
            btnAddContacts.setImage(#imageLiteral(resourceName: "Add Contacts"), for: .normal)
            profileImgEditBtn.isHidden = true
        }
    }
    func adjustContactView() {
        if tagListView.tagBackgroundViews.count > 1 {
            var totalWidth: CGFloat = 0
            for view in tagListView.tagBackgroundViews {
                totalWidth = totalWidth + view.frame.size.width
            }
            if Constants.DeviceType.IS_IPHONE_5 {
                if tagListView.tagBackgroundViews.count > 2{
                    self.addContactsViewHtConstraint.constant = (totalWidth > 280) ?  (CGFloat(37 * (tagListView.tagBackgroundViews.count - 1)) + 150.0) : 170
                    tagListView.h = (totalWidth > 280) ?  (CGFloat(37 * (tagListView.tagBackgroundViews.count - 1)) + 150.0) : 170
                }else{
                    self.addContactsViewHtConstraint.constant = (totalWidth > 230) ?  (CGFloat(37 * (tagListView.tagBackgroundViews.count - 1)) + 150.0) : 125
                    tagListView.h = (totalWidth > 230) ?  (CGFloat(37 * (tagListView.tagBackgroundViews.count - 1)) + 150.0) : 125
                }
            } else {
                if tagListView.tagBackgroundViews.count > 2{
                    self.addContactsViewHtConstraint.constant = (totalWidth > 320) ?  (CGFloat(37 * (tagListView.tagBackgroundViews.count - 1)) + 140.0) : 170
                    tagListView.h = (totalWidth > 320) ?  (CGFloat(37 * (tagListView.tagBackgroundViews.count - 1)) + 140.0) : 170
                }else{
                    self.addContactsViewHtConstraint.constant = (totalWidth > 280) ?  (CGFloat(37 * (tagListView.tagBackgroundViews.count - 1)) + 140.0) : 125
                    tagListView.h = (totalWidth > 280) ?  (CGFloat(37 * (tagListView.tagBackgroundViews.count - 1)) + 140.0) : 125
                }
            }
        } else{
            self.addContactsViewHtConstraint.constant = 125
        }
    }
 }
 
 extension ProfileVC:EPPickerDelegate {
    //MARK: EPContactsPicker delegates
    func epContactPicker(_: EPContactsPicker, didContactFetchFailed error : NSError)
    {
        print("Failed with error \(error.description)")
    }
    
    func epContactPicker(_: EPContactsPicker, didSelectContact contact : EPContact)
    {
        print("Contact \(contact.displayName()) has been selected")
    }
    
    func epContactPicker(_: EPContactsPicker, didCancel error : NSError)
    {
        print("User canceled the selection");
        setBikeModel(bikeModel: lblBikeModel.text!)
        setBikeMake(bikeType: lblBikeMake.text!)
    }
    
    func epContactPicker(_: EPContactsPicker, didSelectMultipleContacts contacts: [EPContact]) {
        print("The following contacts are selected")
        
        setBikeModel(bikeModel: lblBikeModel.text!)
        setBikeMake(bikeType: lblBikeMake.text!)
        
        for contact in contacts {
            print("\(contact.displayName())")
            let eContact = PhoneBookContact()
            eContact.name = contact.displayName()
            
            var isAval = true
            
            for  elememt in tagPhoneBookContact {
                
                if elememt.name == eContact.name {
                    isAval = false
                    break
                }
            }
            
            if isAval {
                tagListView.addTag(contact.displayName())
                for (item , element ) in contact.displayPhoneNumbers().enumerated() {
                    print (item , element.value.value(forKey: "digits") ??  "aa")
                    if  !( element.value.value(forKey: "digits") as! String).isEmpty {
                        eContact.phoneNo = element.value.value(forKey: "digits") as! String
                        break
                    }
                }
                tagPhoneBookContact.append(eContact)
            }
        }
    }
 }
 
 extension ProfileVC:UserManagerDelegate{
    // MARK: - API Delegate Methods
    func onGetBikerSuccess(data:[String:Any]) {
        if !profileImgUpdated{
            self.view.hideToastActivity()
        }
        if let biker:Biker = data["data"] as? Biker {
            self.bikerData = biker
            var totalContactList:[Any] = []
            for contact in biker.emergency_contacts! {
                var contactDict: [String: Any] = [:]
                contactDict["name"] = contact.name
                contactDict["phone"] = contact.phone
                totalContactList.append(contactDict)
            }
            MyDefaults.setArray(value: totalContactList, key:"Contacts")
            MyDefaults.setBoolData(value: biker.share_location!, key: "ShareLocation")
            strShareLocationAllowed = biker.share_location
            if let verified = biker.email1_verified {
                MyDefaults.setBoolData(value: verified, key: "EmailVerification")
            } else {
                MyDefaults.removeForKey(key: "EmailVerification")
            }
            self.showReloginPopup(biker: biker)
            BikerDetails.updateBiker(context: profileContext, biker: biker)
        }
    }
    
    func showReloginPopup(biker: Biker) {
        if oldEmailID != biker.email1 {
            // show login
            MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_RELOGIN)
            MyDefaults.setData(value: biker.email1!, key: DefaultKeys.KEY_BIKER_EMAIL)
            let alert =  UIAlertController.init(title: "", message: "Email updated successfully. Please login again.", preferredStyle: .alert)
            let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: { (action) in
                self.pushVC(LoginViewController())
            })
            alert.addAction(okAction)
            self.presentVC(alert)
        } else {
            self.updateUI(biker: biker)
        }
    }
    
    func updateAllBikesList(data:[String:Any])  {
        print(data)
        bikeModelList.removeAll()
        if let bikeModels:AllBikesList = data["data"] as? AllBikesList {
            if  let count = bikeModels.bikeList?.count, count > 0 {
                bikeModelList =  bikeModels.bikeList!
                bikeModelList = bikeModelList.sorted { $0.company?.localizedCaseInsensitiveCompare($1.company!) == ComparisonResult.orderedAscending }
                let index = bikeModelList.index(where: { (item) -> Bool in
                    item.company == "Others"
                })
                if index != nil {
                    bikeModelList.append(bikeModelList[index!])
                    bikeModelList.remove(at: index!)
                }
                filterString.removeAll()
                for (_ , elements) in (bikeModelList.enumerated()) {
                    filterString.append(elements.company!)
                }
            }
        }
        if !profileImgUpdated{
            self.view.hideToastActivity()
        }
    }
    
    func onVehiclesUpdate(data:[String:Any]){
        print("Profile:\(data)")
        if let vehicle:DefaultVehicle = data["data"] as? DefaultVehicle {
            self.updateVehicleUI(vehicle: vehicle)
            BikerDetails.updateBikerVehicle(context: profileContext, vehicle: vehicle)
        }
        if !profileImgUpdated{
            self.view.hideToastActivity()
        }
    }
    
    func onFailure(Error:[String:Any]){
        self.view.hideToastActivity()
        print("ERROR:\(Error)")
        let errModel:ErrorModel = Error["data"] as! ErrorModel
        self.view.makeToast((errModel.description)!)
    }
 }
 
 // extension ProfileVC: ImageUploadDelegate{
 //
 //    func uploadImageSucess(data:[String:Any]) {
 //        print("ProfileVC:\(data)")
 //        self.view.hideToastActivity()
 //    }
 //
 //    func uploadImageFailed(Error:[String:Any]){
 //        print("Profile:\(Error)")
 //        self.view.hideToastActivity()
 //    }
 // }
 
 extension ProfileVC: passSelectedValueDelegate {
    func selectedBikeData(name: String) {
        selectedBikeMake = name
        bikeModelBtn.isUserInteractionEnabled = true
        self.setBikeMake(bikeType: name)
        self.lblBikeModel.isHidden = true
        self.lblBikeModel.text = ""
        self.bikeModelBtn.setTitle("", for: .normal)
        self.bikeModelBtn.setTitleColor(.white, for: .normal)
        self.bikeModelBtn.contentHorizontalAlignment = .left
        if selectedBikeMake == "Others" {
            selectedBikeModel(name: "Others")
        }
        self.bikeModelBtn.isUserInteractionEnabled = selectedBikeMake == "Others" ? false : true
    }
    
    func selectedBikeModel(name: String) {
        self.lblBikeModel.isHidden = false
        self.bikeModelBtn.setTitle("", for: .normal)
        self.setBikeModel(bikeModel: name)
    }
 }
 
