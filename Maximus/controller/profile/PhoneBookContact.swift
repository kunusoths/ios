//
//  PhoneBookContact.swift
//  Maximus
//
//  Created by Namdev Jagtap on 08/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import Foundation

class PhoneBookContact{
    
    var name        = String()
    var phoneNo     = String()
}
