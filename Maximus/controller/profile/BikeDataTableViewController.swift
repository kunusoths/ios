//
//  BikeDataTableViewController.swift
//  Maximus
//
//  Created by kpit on 01/11/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

@objc protocol passSelectedValueDelegate {
    func selectedBikeData(name: String)
    func selectedBikeModel(name: String)
}

class BikeDataTableViewController: UITableViewController {

    weak var delegate: passSelectedValueDelegate!
    var bikeModelData: [BikeModelList] = []
    var bikeMakeData: [String] = []
    var isBikeMake: Bool = false
    var selectedBike: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Arrow White"), style: .plain, target: self, action: #selector(menuTapped))
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        bikeModelData = isBikeMake ? bikeModelData.filter({$0.company! == selectedBike}) : bikeModelData
        if bikeModelData.count > 0 {
            bikeMakeData = bikeModelData[0].model!
            bikeMakeData = bikeMakeData.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
            let index = bikeMakeData.index(where: { (item) -> Bool in
                item == "Others"
            })
            if index != nil {
                bikeMakeData.append(bikeMakeData[index!])
                bikeMakeData.remove(at: index!)
            }
        }
        self.title = isBikeMake ? "BIKE MODEL" : "BIKE MAKE"
        self.tableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func menuTapped() {
        self.popVC()
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isBikeMake ?  bikeMakeData.count : bikeModelData.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BikeDataCell", for: indexPath)
        cell.textLabel?.text = isBikeMake ? bikeMakeData[indexPath.row] : bikeModelData[indexPath.row].company
        cell.textLabel?.textColor = .white
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isBikeMake ? self.delegate.selectedBikeModel(name: bikeMakeData[indexPath.row]) : self.delegate.selectedBikeData(name: bikeModelData[indexPath.row].company!)
        self.popVC()
    }

}
