//
//  HomeContainerVC.swift
//  Maximus
//
//  Created by kpit on 12/04/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import CoreBluetooth

class HomeContainerVC: UIViewController, ReloadMyRidesDelegate,HomeViewModelDelegate {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var collectionViewTabBar: UICollectionView!
    
    var deviceList = [Any]()

    var previousVc = ""
    var bottomBorder:UIView!
    var rideType:String!
    
    let cellIdentifier = TableViewCells.RouteCell
    var selectedCollectionRow = 0
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
//    let rideRepo = RideRepository()
//    var ridesModel: MyRidesViewModel?
    var bikerDetails: BikerDetails!
    let homeViewModel = HomeViewModel()

    //RouteCell
    @IBOutlet weak var vwButtonBorder: UIView!
    
    private var activeViewController: UIViewController? {
        didSet {
            removeInactiveViewController(inactiveViewController: oldValue)
            updateActiveViewController()
        }
    }
    
    // MARK : ViewController Lifecycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        ridesModel.reloadDataDelegate = self
//        ridesModel = MyRidesViewModel(rideRepoDelegate: rideRepo)
      //  self.checkIfAnyRideInProgress()
        DeviceLocationServiece.sharedInstance().configureDeviceLocation()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Ham Menu"), style: .plain, target: self, action: #selector(menuTapped))
         self.navigationItem.leftBarButtonItem?.accessibilityIdentifier = "btn_hamb_menu"
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.navigationController!.navigationBar.isTranslucent = false
        self.navigationItem.title = NSLocalizedString("keyRideOn", comment: "")
        self.automaticallyAdjustsScrollViewInsets = false
        print("PrivousVc", previousVc)
        if(previousVc == StringConstant.rideDetailsVc)
        {
            selectedCollectionRow = 1
            switchVC()
        }else{
            selectedCollectionRow = 0
            switchVC()
        }
        // clear plan ride data if any
        PlanRideDataManager.destoy()
        collectionViewTabBar.backgroundColor = AppTheme.THEME_ORANGE
        collectionViewTabBar.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        
        if let vc:SideMenuVC =  self.slideMenuController()?.leftViewController as? SideMenuVC {
            vc.showSelectedSideMenuIndex(index: 0)
        }
        self.bikerDetails = BikerDetails.getBikerDetails(context: context)
        
        sendSystemDetailsToCloud()
        
    }

    // MARK:Notification Handlers
    func updateBlelistNotification(notification:NSNotification){
        
        if let deviceListArray = notification.object as? [Any] {
            deviceList = deviceListArray
        }
    }
    
    func BleConnectNotification(notification:NSNotification){
        print("Connected Ble device is  \(notification.object ?? "")")
    }

    
    override func viewWillAppear(_ animated: Bool) {

        self.setApplicationBadgeIcon()
        //
        NotificationCenter.default.addObserver(self, selector:
            #selector(self.upgradeDM), name: NSNotification.Name(rawValue: NotificationIdentifier.upgardeDmIdentifier), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showEmailVerifyPopup), name: NSNotification.Name(rawValue: NotificationConstant.showEmailPopup), object: nil)
        showEmailNotVerifiedPopup()
        NotificationCenter.default.removeObserver(
            self, name: NSNotification.Name(rawValue: NotificationConstant.ble_list_notification),
            object: nil)
        
        NotificationCenter.default.removeObserver(
            self, name: NSNotification.Name(rawValue: NotificationConstant.ble_connect_notification),
            object: nil)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionViewTabBar.layer.shadowColor = UIColor.gray.cgColor
        collectionViewTabBar.layer.shadowOpacity = 0.8
        collectionViewTabBar.layer.shadowRadius = 3.0
        collectionViewTabBar.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //
    }
    
    func sendSystemDetailsToCloud() {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
        do{
            let deviceDetailsArray = try context.fetch(DeviceDetails.fetchRequest())
            if deviceDetailsArray.count > 0 {
                let deviceDetails = deviceDetailsArray.first as? DeviceDetails
                if !(deviceDetails?.isSynced)! {
                    let parameters = AppDetailsHandler.sharedInstance.getAppDetailsObj()
                    AppDetailsHandler.sharedInstance.sendSystemDetails(rider: Int(self.bikerDetails.biker_id ?? "")!, details: parameters)
                }
            }
            if !AppDetailsHandler.sharedInstance.getSyncFlagStatus() {
                let parameters = AppDetailsHandler.sharedInstance.getDmDetailsObj()
                AppDetailsHandler.sharedInstance.sendDMDetails(details: parameters)
            }
            
        }catch{
            print("Fetching failed")
        }
    }
    
    func showEmailNotVerifiedPopup() {
        if bikerDetails.auth_method ?? AUTH_METHOD.UNKNOWN.rawValue == AUTH_METHOD.USERNAME_PASSWORD.rawValue {
            let bikerID = self.bikerDetails.biker_id
            self.homeViewModel.delegate = self
            self.homeViewModel.homeGetBiker(bikerId: Int(bikerID ?? "")!)
        }
    }
    
    //MARK:- HomeViewModelDelegate Methods
    
    func getBikerSucess(response: [String : Any]) {
        
        let biker:BikerDetails = response[StringConstant.bikerData] as! BikerDetails
        
        switch biker.is_email_verified {
        case -1:
            break
            
        case 1:
            MyDefaults.setData(value: biker.email ?? "", key: DefaultKeys.KEY_BIKER_EMAIL)
            
            break
            
        case 0:
            //MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_EMAIL_VERIFY)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationConstant.showEmailPopup), object: nil)
            break
        default:
            break
        }
        
    }
    
    func rideStarted(data: [String : Any]) {
        
    }
    
    func rideStopped(data: [String : Any]) {
        
    }
    
    func rideInProgress(data: [String : Any]) {
        
    }

    
    func reloadData(data: Any) {
        //ride updated
    }
    
    func failure(data: [String : Any]) {
        
    }
   
    func menuTapped() {
       self.slideMenuController()?.openLeft()
    }

    // MARK : Button Actions
    func switchVC(){
        switch selectedCollectionRow {
        case 0:
            activeViewController = HomeVC()
            break
            
        case 1:
            activeViewController = MyRidesVC()
            break
            
        case 2:
            activeViewController = RecommendedRideVC()
            break
            
        default:
            break
        }
    }

    
    // MARK : Handling VCs
    
    private func removeInactiveViewController(inactiveViewController: UIViewController?) {
        if let inActiveVC = inactiveViewController {
            // call before removing child view controller's view from hierarchy
            inActiveVC.willMove(toParentViewController: nil)
            
            inActiveVC.view.removeFromSuperview()
            
            // call after removing child view controller's view from hierarchy
            inActiveVC.removeFromParentViewController()
        }
    }
    
    private func updateActiveViewController() {
        if let activeVC = activeViewController {
            // call before adding child view controller's view as subview
            addChildViewController(activeVC)
            
            activeVC.view.frame = contentView.bounds
            contentView.addSubview(activeVC.view)
            
            // call before adding child view controller's view as subview
            activeVC.didMove(toParentViewController: self)
        }
    }
    
}

extension HomeContainerVC: UICollectionViewDataSource,UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        print("\ndid Deselect index \(indexPath.row)")
        if let cell = collectionViewTabBar.cellForItem(at: indexPath){
            cell.backgroundColor = UIColor.white//UIColor(colorLiteralRed: 17.0/250.0, green: 17.0/250.0, blue: 17.0/250.0, alpha: 1.0)
            
        }
        collectionView.reloadItems(at: [indexPath])
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("\ndid select index \(indexPath.row)")
        
        if let cell = collectionViewTabBar.cellForItem(at: NSIndexPath(row: selectedCollectionRow, section: 0) as IndexPath){
            cell.backgroundColor = AppTheme.THEME_ORANGE
            
        }
        selectedCollectionRow = indexPath.row
        self.switchVC()
        if let cell = collectionViewTabBar.cellForItem(at: indexPath){
            cell.backgroundColor = UIColor.white
        }
        DispatchQueue.main.async {
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }

    }
    
    @available(iOS 6.0, *)
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print("\n****************cell for index \(indexPath.row)")
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? RouteCell {
        
            self.setTabButtonName(index: indexPath.row, cell: cell)
            cell.btnRouteAction.setTitleColor(UIColor.white, for: .normal)
            if(indexPath.row == selectedCollectionRow){
                cell.backgroundColor = UIColor.white
            }else{
                cell.backgroundColor = AppTheme.THEME_ORANGE
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func setTabButtonName(index:Int, cell:RouteCell){
        switch index {
        case 0:
            cell.btnRouteAction.setTitle(NSLocalizedString("keyPlanRideTitle", comment: ""), for: .normal)
            cell.btnRouteAction.backgroundColor = AppTheme.THEME_ORANGE
            break
            
        case 1:
            cell.btnRouteAction.setTitle(NSLocalizedString("keyMyRides", comment: ""), for: .normal)
            cell.btnRouteAction.backgroundColor = AppTheme.THEME_ORANGE
            break
            
        case 2:
            cell.btnRouteAction.setTitle(NSLocalizedString("keyRecommended", comment: ""), for: .normal)
            cell.btnRouteAction.backgroundColor = AppTheme.THEME_ORANGE
            break
            
        default:
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 3
    }
    
}


//MARK:ColectionView Flow Layout delegate methods
extension HomeContainerVC: UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.frame.size.width/3).rounded(), height: collectionView.frame.size.height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
         cell.accessibilityIdentifier = "cltn_home_tabs"
    }
}


