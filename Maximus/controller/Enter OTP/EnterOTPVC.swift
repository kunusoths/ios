//
//  EnterOTPVC.swift
//  Maximus
//
//  Created by kpit on09/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//


import UIKit
import Alamofire


class EnterOTPVC: UIViewController,UITextFieldDelegate,IOtpListner, BikerViewModelDelegate {
    

    @IBOutlet weak var tfEnterOTP: UITextField!
    @IBOutlet var btnEnterOtp: UIButton!
    
    let errorMessage = "Wrong OTP, Please re-enter!"
    let bikerModel = BikerViewModel()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationItem.title = "PHONE NUMBER VERIFICATION"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:UIImage(named: "Back Arrow White"), style: .plain, target: self, action: #selector(backTapped))
        self.tfEnterOTP.delegate = self
        tfEnterOTP.addTarget(self, action: #selector(EnterOTPVC.textFieldDidChange(_:)),
                                for: UIControlEvents.editingChanged)
        tfEnterOTP.setBottomBorder()
        tfEnterOTP.textColor = UIColor.white
        if #available(iOS 12.0, *) {
            tfEnterOTP.textContentType = .oneTimeCode
        } else {
            // Fallback on earlier versions
        }

        self.tfEnterOTP.becomeFirstResponder()
        let borderFrame = CGRect(x:0, y:0, width:self.view.bounds.width, height:1)
        tfEnterOTP.addUpperBorderToKeyaboard(viewFrame: borderFrame)
        bikerModel.bikerDelegate = self
    
    }

    func backTapped() {
        self.view.endEditing(true)
        self.popVC()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.topItem?.title = "PHONE NUMBER VERIFICATION"
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    // MARK: Private Methods

    func postOtpToServer(otp:String)
    {
        let userDefaults = Foundation.UserDefaults.standard
        let mobileNo  = userDefaults.string(forKey: "mobileno")
        let json: [String: String] = ["mobile": mobileNo!, "otp":otp]
        let otpHandlerObj = OtpHandler()
        otpHandlerObj.optListnerDelegate = self
        self.view.makeToastActivity(.center, isUserInteraction: true)
        otpHandlerObj.validateOtp(subUrl:RequestURL.URL_VALIDATEOTP.rawValue, parameter:json)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField.text == "Wrong OTP, Please re-enter!"){
            textField.text = ""
            textField.textColor = UIColor.white
        }
    }
    
    func textFieldDidChange(_ textField: UITextField){
        
        textField.textColor = UIColor.white
        if textField.text?.length == 6
        {
            DispatchQueue.main.async
            {
                self.btnEnterOtp.setTitle("Submit OTP",for: .normal)
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let maxLength = 6
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    @IBAction func btnResendOTP(_ sender: UIButton) {
        
        if (sender.titleLabel?.text == "Submit OTP"){
        self.postOtpToServer(otp: tfEnterOTP.text!)
        }else{
            getOtpFromServer()
        }
    }
    
    func putPhoneNo(mobile:String)
    {
        let json: [String: Any] = ["phone_mobile1": mobile,
                                   "id": MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID)
        ]
        let otpHandlerObj = OtpHandler()
        otpHandlerObj.optListnerDelegate = self
        print("json", json)
        otpHandlerObj.putPhoneNumber(subUrl:"/bikers", parameter: json)
    }
    
    func getOtpFromServer()
    {
        let userDefaults = Foundation.UserDefaults.standard
        let mobileNo  = userDefaults.string(forKey: "mobileno")
        let json: [String: String] = ["mobile": mobileNo!]
        let otpHandlerObj = OtpHandler()
        otpHandlerObj.optListnerDelegate = self
        self.view.makeToastActivity(.center, isUserInteraction: true)
        otpHandlerObj.getOtpForMobile(subUrl:RequestURL.URL_GETOTP.rawValue, parameter:json)
    }
    
    //MARK:OTP Listner Delegates
    
    internal func onFailure(response: DataResponse<Any>) {
        
    }
    
    internal func onSuccess(response: DataResponse<Any>)
    {
        let jsonResult:NSDictionary = response.result.value as! NSDictionary
        self.view.hideToastActivity()

        if response.response?.statusCode == 200
        {
            let status:String = jsonResult["status"] as! String
            if status == "Validation failed - Invalid OTP!" || status == "Validation failed - Otp expired already!"
            {
               self.wrongOtpMessage()
            }
            else
            {
                self.putPhoneNo(mobile:jsonResult["mobile"] as! String)
                let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
                BikerDetails.updateOTPStatus(context: context, status: true)
                let biker = BikerDetails.getBikerDetails(context: context)
                bikerModel.getBiker(bikerID: Int(biker?.biker_id ?? "")!)
                DispatchQueue.main.async
                {
                    MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_OTPVERIFIED)
                    self.pushVC(PhoneVerificationCompleteVC())
                }
            }
        }
        else
        {
            self.wrongOtpMessage()

        }
       
        print(response);
    }
    
    func wrongOtpMessage() {
        DispatchQueue.main.async
            {
                self.tfEnterOTP.text = "Wrong OTP, Please re-enter!"
                self.tfEnterOTP.textColor = UIColor.red
                self.btnEnterOtp.setTitle("Re-Send OTP",for: .normal)
                self.tfEnterOTP.resignFirstResponder()
                
                // Delay 5 seconds
                DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                    if self.tfEnterOTP.text == "Wrong OTP, Please re-enter!"
                    {
                       self.tfEnterOTP.text = ""
                       self.tfEnterOTP.textColor = UIColor.white
                    }
                }
        }
    }
    
    func onPutPhoneNumberSuccess(response:DataResponse<Any>)
    {
    }
    
    func onPutPhoneNumberFailure(response:DataResponse<Any>)
    {
    }
    
    internal func onGenerateOtpSuccess(response: DataResponse<Any>) {
        
        self.view.hideToastActivity()
        if response.response?.statusCode == 200
        {
      
        }
        else
        {
            DispatchQueue.main.async
                {
                    self.showErrorMessage()
            }
        }
    }
    
    func showErrorMessage() {
        self.tfEnterOTP.text =  "Invalid number"
        self.tfEnterOTP.textColor = UIColor.red
    }
    
    func bikerRegistrationSuccess() {
        
    }
    
    func authenticationFail(data: [String : Any]) {
        
    }
    
}


