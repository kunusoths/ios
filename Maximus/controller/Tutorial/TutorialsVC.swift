//
//  TutorialsVC.swift
//  Maximus
//
//  Created by kpit on 08/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
//import Crashlytics

class TutorialsVC: UIViewController, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var pageController: UIPageControl!
    @IBOutlet var skipButton: UIButton!
   
    let cellIdentifier = TableViewCells.TutorialCell
    let arrNavTitles = [NSLocalizedString("keyTrackAchievements", comment: ""),NSLocalizedString("keyTrackBikeHealth", comment: ""),NSLocalizedString("keyTheftLocationAlerts", comment: "")]
    
    var flowlayout = UICollectionViewFlowLayout()
    var arrOfTutorial = [TutorialData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Crashlytics.sharedInstance().crash()
        self.navBar?.isHidden = false
        self.navigationItem.title = arrNavTitles[0]

        flowlayout.scrollDirection = UICollectionViewScrollDirection.horizontal
        flowlayout.minimumLineSpacing = 0
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isPagingEnabled = true
        collectionView.setCollectionViewLayout(flowlayout, animated: false)
        
        pageController.numberOfPages = 3
        pageController.currentPage = 0
        pageController.hidesForSinglePage = true
        
        let nib = UINib (nibName:TableViewCells.TutorialCollectionCell, bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: cellIdentifier)
        
        let arrTitle = [NSLocalizedString("keyTutorialPage1", comment: ""),NSLocalizedString("keyTutorialPage2", comment: ""),NSLocalizedString("keyTutorialPage3", comment: "")]
        
        let arrDescription = [NSLocalizedString("keyTutorialPage1Details", comment: ""),NSLocalizedString("keyTutorialPage2Details", comment: ""),NSLocalizedString("keyTutorialPage3Details", comment: "")]
        
        
        let arrImageName = [ImageNames.ImgStar,ImageNames.ImgBikeHealth,ImageNames.ImgTheft]
        
        for i in 0...2 {
            let objTuto = TutorialData()
            objTuto.title = arrTitle[i]
            objTuto.discription = arrDescription[i]
            objTuto.imageName = arrImageName[i]
            arrOfTutorial.insert(objTuto, at: i)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    /*
     This method skips pages on tutorial screen and goes to next screen
     @param: Any
    */
    @IBAction func skipBtnTapped(_ sender: Any) {
        MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_TUTORIAL_SEEN)
        let vc = SignUpViewController()
        self.pushVC(vc)
    }
}

class TutorialData{
    var imageName = String()
    var title:String = String()
    var discription:String = String()
}

extension TutorialsVC : UICollectionViewDelegate , UICollectionViewDataSource{
    //MARK:ColectionView delegate methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        print("\n\n\n\n\n\(collectionView.bounds.size)")
        return collectionView.bounds.size
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        return pageController.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! TutorialsCollectionViewCell
        
        var obj = TutorialData()
        obj = arrOfTutorial[indexPath.row]
        
        cell.titleLabel.text = obj.title
        cell.descriptionLabel.text = obj.discription
        cell.imageVw.image = UIImage(named: obj.imageName)
        
        return cell
    }
    
    //MARK: ScrollView Delegate Method
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let pageNumber = Int(targetContentOffset.pointee.x / view.frame.width)
        pageController.currentPage = pageNumber
        
        self.navigationItem.title = arrNavTitles[pageNumber]
        if(pageNumber == 2){
            skipButton.setTitle(NSLocalizedString("keyDone", comment: ""), for: .normal)
        }else {
            skipButton.setTitle(NSLocalizedString("keySkip", comment: ""), for: .normal)
        }
    }
    
}




