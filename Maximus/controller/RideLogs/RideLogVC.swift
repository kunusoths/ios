//
//  RideLogVC.swift
//  Maximus
//
//  Created by Admin on 07/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import ObjectMapper
import CoreData
import CoreLocation

class RideLogVC: UIViewController {
    
    @IBOutlet weak var tableViewrideLog: UITableView!
    @IBOutlet weak var rideLogFirst: UIImageView!
    @IBOutlet weak var lblRideLogFirst: UILabel!
    
    var arrLogList = [Rides]()
    
    let isCMRegisterd = MyDefaults.getBoolData(key: DefaultKeys.KEY_CMREGISTER)
    let cellReuseIdentifier = "RideLogCell"
    var page = 0
    var size = 20
    let rideStatsModel = RideStatsViewModel()
    let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.newBackgroundContext()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.navigationController!.navigationBar.isTranslucent = false
        self.tableViewrideLog.register(UINib(nibName: cellReuseIdentifier, bundle: nil), forCellReuseIdentifier:cellReuseIdentifier)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Ham Menu"), style: .plain, target: self, action: #selector(menuTapped))
        self.navigationItem.title = "RIDELOG"
        rideStatsModel.rideStatsDelegate = self
        if isCMRegisterd {
            //Get RideLog Data API call
            let bikerID = MyDefaults.getInt (key: DefaultKeys.KEY_BIKERID)
            arrLogList = Rides.getAllCompletedRides(context: context!)
           // sortAndSetData(arrayLogs: arrLogList)
            reloadData()
            rideStatsModel.getRideStatsFor(bikerID: bikerID, page: page, size: size, request: "GetRideStats")
            self.tableViewrideLog.backgroundView = nil
        }
        else
        {
            rideLogFirst.isHidden = false
            lblRideLogFirst.isHidden = false
        }
    }
    
    override func viewWillLayoutSubviews() {
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.automaticallyAdjustsScrollViewInsets = false
        
    }
    func reloadLogs() {
        self.tableViewrideLog.reloadData()
    }
    //MARK:Private Methods
    func menuTapped() {
        self.slideMenuController()?.openLeft()
    }
    
}

extension RideLogVC {
    
    
    func sortAndSetData(arrayLogs:[Rides]){
        arrLogList.append(contentsOf: arrayLogs)
        
        var rideIdArray = Set<String>()
        var unique = [Rides]()
        for message in arrLogList {
            if let rideID = message.rideID {
                if !rideIdArray.contains(rideID) {
                    unique.append(message)
                    rideIdArray.insert(rideID)
                }
            }
        }
        arrLogList = unique.sorted(by: { $0.start_date! > $1.start_date! })
//        arrLogList = unique.sorted(by: { $0.achievement?.ride_end_date > $1.achievement?.ride_end_date  })
        
    }
    
    
    func getCurrentRideStats(rideId: String) -> Rides?{
        let offlineStatsContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
        if let achievementObj = Rides.fetchRideWithID(rideID: rideId, context: offlineStatsContext) {
            return achievementObj
        }
        return nil
    }
    
    
    func onFailure(Error:[String:Any]) {
        self.view.hideToastActivity()
        let errModel:ErrorModel = Error["data"] as! ErrorModel
        print(errModel.message ?? " ")
        self.view.makeToast((errModel.description)!)
        if arrLogList.count == 0 {
            rideLogFirst.isHidden = false
            lblRideLogFirst.isHidden = false
        }
    }
}

//MARK:- Tableview Delegate & DataSource Methods
extension RideLogVC : UITableViewDelegate ,UITableViewDataSource {
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrLogList.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! RideLogCell
        if arrLogList.count - 2 == indexPath.row {
            let bikerID = MyDefaults.getInt (key: DefaultKeys.KEY_BIKERID)
            page = page + 1
            if NetworkCheck.isNetwork() {
            rideStatsModel.getRideStatsFor(bikerID: bikerID, page: page, size: size, request: "GetRideStats")
            } else {
                self.view.makeToast(ToastMsgConstant.network)
            }
        }
            //Stickers
            cell.imgViewsticker_3.image = nil
            cell.imgViewsticker_2.image = nil
            cell.imgViewsticker_1.image = nil
            cell.imgViewsticker_3.tag = indexPath.row
            cell.imgViewsticker_2.tag = indexPath.row
            cell.imgViewsticker_1.tag = indexPath.row
            
            let rideLogDetails = RideLogDetails()
            rideLogDetails.rideTitle = arrLogList[indexPath.row].ride_title ?? ""
            rideLogDetails.rideStartData = arrLogList[indexPath.row].start_date
            print("ride title : \(rideLogDetails.rideTitle ?? "")")
        let acheivementsData = AchievementDataObj.fetchStatsWithRideId(rideId: arrLogList[indexPath.row].rideID!, context: context!)?.time_duration == 0 ? Rides.fetchRideWithID(rideID: arrLogList[indexPath.row].rideID!, context: context!)?.achievement : AchievementDataObj.fetchStatsWithRideId(rideId: arrLogList[indexPath.row].rideID!, context: context!)
            if let _ = arrLogList[indexPath.row].rideID{
                
                 let localStats = arrLogList[indexPath.row]
                    if let endDate = localStats.achievement?.last_processed_time {
                        if  endDate.timeIntervalSinceNow > -1000 {
                            rideLogDetails.rideDistance = ((acheivementsData?.distance_travelled ?? 0))
                            rideLogDetails.rideTime = Int(acheivementsData?.time_duration ?? 0)
                            rideLogDetails.rideTopSpeed = (acheivementsData?.top_speed ?? 0)
                        }else{
                            //  cell.objlog = arrLogList[indexPath.row]
                            rideLogDetails.rideDistance = acheivementsData?.distance_travelled ?? 0
                            rideLogDetails.rideTime = Int(acheivementsData?.time_duration ?? 0)
                            rideLogDetails.rideTopSpeed =  (acheivementsData?.top_speed ?? 0)
                            self.assignBadges(indexPath: indexPath,cell: cell)
                            
                        }
                    } else {
                        rideLogDetails.rideDistance = acheivementsData?.distance_travelled ?? 0
                        rideLogDetails.rideTime = Int(acheivementsData?.time_duration ?? 0)
                        rideLogDetails.rideTopSpeed = (acheivementsData?.top_speed ?? 0)

                        self.assignBadges(indexPath: indexPath,cell: cell)
                    }
            }else{
                rideLogDetails.rideDistance = acheivementsData?.distance_travelled ?? 0
                rideLogDetails.rideTime = Int(acheivementsData?.time_duration ?? 0)
                rideLogDetails.rideTopSpeed = (acheivementsData?.top_speed ?? 0)
                self.assignBadges(indexPath: indexPath,cell: cell)
            }
            
            
            cell.objlog = rideLogDetails
            
        return cell
    }
    
    
    func assignBadges(indexPath: IndexPath, cell:RideLogCell){
        let objlog:Rides = arrLogList[indexPath.row]
        do {
            
            guard let data:Data = objlog.honorBadge as? Data else {
                return
            }
            let badgeData = NSKeyedUnarchiver.unarchiveObject(with: data)
            
            let badgeImgArray = badgeData as? [[String: String]]
            if (badgeImgArray?.count)! > 0 {
                
                for index in 0 ..< (badgeImgArray?.count)! {
                    
                    let obj = badgeImgArray![index]
                    if let url = obj["url"] {
                        if(index == 0){
                            cell.imgViewsticker_3.loadImageUsingCacheWithUrlString(urlString: url,indexPath: indexPath.row)
                        }
                        if(index == 1){
                            cell.imgViewsticker_2.loadImageUsingCacheWithUrlString(urlString: url,indexPath: indexPath.row)
                        }
                        if(index == 2){
                            cell.imgViewsticker_1.loadImageUsingCacheWithUrlString(urlString: url,indexPath: indexPath.row)
                        }
                    }
                }
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let acheived = AchievementsVC()
        
        if let stats = self.getCurrentRideStats(rideId: arrLogList[indexPath.row].rideID!) {
            if let lastPktTime  = stats.achievement?.last_processed_time {
                if  lastPktTime.timeIntervalSinceNow > -1000 {
                    
                    acheived.showOfflineStats   = true
                    
                }else{
                    acheived.showOfflineStats   = false
                }
                
            }else{
                acheived.showOfflineStats   = false
            }
            
        }else{
            acheived.showOfflineStats   = false
        }
        
        
        let objlog = arrLogList[indexPath.row]
        if let _ = (objlog.honorBadge) as? Data {
            let badgeData = NSKeyedUnarchiver.unarchiveObject(with: ((objlog.honorBadge) as? Data)!)
            acheived.honoredBadges      = badgeData as? [[String: String]]
        }
        acheived.acheivementsData   = objlog.achievement
        acheived.rideID             = objlog.rideID
        acheived.rideTitle          = objlog.ride_title!
        acheived.showOfflineStopMsg = false
        self.pushVC(acheived)
    }
    
}

extension RideLogVC: RideStatsDelegate {
    func rideStatisticSuccess(data: Any) {
        self.view.hideToastActivity()
        if let dict = data as? [String: Any] {
            arrLogList = []
            if let objList:[Rides] = dict["data"] as? [Rides] {
                arrLogList = objList
               // self.sortAndSetData(arrayLogs: objList)
            }
        }
       reloadData()
    }
    
    func failure(data: [String : Any]) {
        self.view.hideToastActivity()
        if let errModel:ErrorModel = data["data"] as? ErrorModel {
            print(errModel.message ?? " ")
            self.view.makeToast((errModel.description)!)
        } else {
            if let descriptionString = data["description"] as? String {
                self.view.makeToast(descriptionString)
            }
        }
    }
    
    func reloadData() {
    DispatchQueue.main.async{
            if self.arrLogList.count == 0 {
                self.rideLogFirst.isHidden = false
                self.lblRideLogFirst.isHidden = false
            } else {
                self.rideLogFirst.isHidden = true
                self.lblRideLogFirst.isHidden = true
            }
            self.tableViewrideLog.reloadData()
        }
    }
}
