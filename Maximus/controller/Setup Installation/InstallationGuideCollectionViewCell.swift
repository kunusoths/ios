//
//  InstallationGuideCollectionViewCell.swift
//  Maximus
//
//  Created by Isha Ramdasi on 15/11/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//

import UIKit

class InstallationGuideCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
