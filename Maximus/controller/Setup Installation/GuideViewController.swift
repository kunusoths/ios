//
//  GuideViewController.swift
//  Maximus
//
//  Created by Isha Ramdasi on 15/11/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//

import UIKit

class GuideViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var flowlayout = UICollectionViewFlowLayout()
    let cellIdentifier = TableViewCells.InstallationGuideCell
    var selectionOption: Int = -1
    var dictData : [Any]?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Arrow White"), style: .plain, target: self, action: #selector(menuTapped))
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        
        // Do any additional setup after loading the view.
        let nib = UINib (nibName:TableViewCells.InstallationGuideCell, bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: cellIdentifier)
        flowlayout.scrollDirection = UICollectionViewScrollDirection.horizontal
        flowlayout.minimumLineSpacing = 0
        
        collectionView.isPagingEnabled = true
        collectionView.setCollectionViewLayout(flowlayout, animated: false)
        
        pageControl.currentPage = 0
        pageControl.hidesForSinglePage = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let dataPath = Bundle.main.path(forResource: StringConstant.InstallationGuide, ofType: "plist")
        if let arrayOfItems = NSArray(contentsOfFile: dataPath!) as? [Any] {
            let arrayOfDict = arrayOfItems[0] as? [String: Any]
            getDataArray(fromDict:arrayOfDict!)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func menuTapped()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getDataArray(fromDict dataDict: [String: Any]) {
        collectionView.delegate = nil
        collectionView.dataSource = nil
        switch selectionOption {
        case InstallationGuide.DMUsage.rawValue:
            self.navigationItem.title = NSLocalizedString("keyDMUsage", comment: "")
            dictData = dataDict[StringConstant.DMUsage] as? [Any]
            break
        
        case InstallationGuide.DMInstallation.rawValue:
            self.navigationItem.title = NSLocalizedString("keyDMInstallation", comment: "")
            dictData = dataDict[StringConstant.DMInstallation] as? [ Any]
            break
        
        case InstallationGuide.CMInstallation.rawValue:
            self.navigationItem.title = NSLocalizedString("keyCMInstallation", comment: "")
            dictData = dataDict[StringConstant.CMInstallation] as? [Any]
            break
        
        default:
            break
        }
        pageControl.numberOfPages = (dictData?.count)!
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }
    @IBAction func onClickSkip(_ sender: UIButton) {
        self.popVC()
    }

        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return dictData?.count ?? 0
        }
    
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            print("\n\n\n\n\n\(collectionView.bounds.size)")
            return collectionView.bounds.size
        }
    
    
        func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
            return pageControl.currentPage = indexPath.item
        }
    
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! InstallationGuideCollectionViewCell
            if let dictionaryDetails = dictData![indexPath.item] as? [String: Any] {
                cell.titleLbl.text = dictionaryDetails[StringConstant.Title] as? String
                cell.descriptionLbl.text = dictionaryDetails[StringConstant.Description] as? String
                cell.imgView.image = UIImage(named: (dictionaryDetails[StringConstant.Img] as? String)!)
            }
            return cell
        }
    
    
        func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
            
            let pageNumber = Int(targetContentOffset.pointee.x / view.frame.width)
            pageControl.currentPage = pageNumber
            
            // self.navigationItem.title = arrNavTitles[pageNumber]
            if(pageNumber == (dictData?.count)! - 1){
                skipBtn.setTitle(NSLocalizedString("keyDone", comment: ""), for: .normal)
            }else {
                skipBtn.setTitle(NSLocalizedString("keySkip", comment: ""), for: .normal)
            }
        }

}

