//
//  SetupAndInstallationViewController.swift
//  Maximus
//
//  Created by Isha Ramdasi on 14/11/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//

import UIKit

class SetupAndInstallationViewController: UIViewController {

    @IBOutlet weak var usageGuideButton: UIButton!
    @IBOutlet weak var installationButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Ham Menu"), style: .plain, target: self, action: #selector(menuTapped))
        self.navigationItem.leftBarButtonItem?.tintColor = .white
      
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        usageGuideButton.isHidden = true
        installationButton.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func menuTapped()
    {
        self.slideMenuController()?.openLeft()
    }
    //MARK: Button Actions
    /*
     Show usage guide
     @param: UIButton
    */
    @IBAction func onClickUsageGuide(_ sender: UIButton) {
        usageGuideButton.isHidden = true
        installationButton.isHidden = true
        let guideView = GuideViewController()
        guideView.selectionOption = InstallationGuide.DMUsage.rawValue
        self.navigationController?.pushViewController(guideView, animated: true)
    }
    /*
     Show installation guide
     @param: UIButton
     */
    @IBAction func onClickInstallationGuide(_ sender: UIButton) {
        usageGuideButton.isHidden = true
        installationButton.isHidden = true
        let guideView = GuideViewController()
        guideView.selectionOption = InstallationGuide.DMInstallation.rawValue
        self.pushVC(guideView)
    }
    /*
     Show hide display module button
     @param: UIButton
     */
    @IBAction func onClickDisplayModule(_ sender: UIButton) {
        
        
        if usageGuideButton.isHidden {
            usageGuideButton.isHidden = false
        } else {
            usageGuideButton.isHidden = true
        }
        if installationButton.isHidden {
            installationButton.isHidden = false
        } else {
            installationButton.isHidden = true
        }
    }
    /*
     Show connectivity module guide
     @param: UIButton
     */
    @IBAction func onClickConnectivityModule(_ sender: UIButton) {
        usageGuideButton.isHidden = true
        installationButton.isHidden = true
        let guideView = GuideViewController()
        guideView.selectionOption = InstallationGuide.CMInstallation.rawValue
        self.navigationController?.pushViewController(guideView, animated: true)
    }
    
}
