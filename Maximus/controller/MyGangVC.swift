//
//  MyGangVC.swift
//  Maximus
//
//  Created by Abhilash G on 6/30/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import GoogleMaps
import ObjectMapper

class MyGangVC: UIViewController {
    
    @IBOutlet weak var myGangMapView: GMSMapView!
    @IBOutlet weak var tableViewJoinedRiders: UITableView!
    @IBOutlet weak var lblRiderName: UILabel!    
    @IBOutlet weak var vwRidersInfo: UIView!
    @IBOutlet weak var lblBikerTimestamp: UILabel!
    @IBOutlet weak var btnPhoneNumber: UIButton!
    
    var rideDetails:Rides?
    
    let bikerID = MyDefaults.getInt (key: DefaultKeys.KEY_BIKERID)
    var poiStartLoc = StartLocation()
    var poiEndLoc = EndLocation()
    var poiRideID = Int()
    var joinedBikers = [RideMemberDetails]()
    var marker:GMSMarker?
    var markerList = [GMSMarker]()
    let journeyManager = JourneyManager.sharedInstance
    var distance: String?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    var currentRideObj: Rides?
    var rideModel: MyRidesViewModel?
    let rideRepo = RideRepository()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rideModel = MyRidesViewModel(rideRepoDelegate: rideRepo)
        if rideDetails != nil{
        }else{
            if(journeyManager.isAnyRideInProgress()){
                self.rideDetails = journeyManager.currentRideDetails()
            }
        }
        currentRideObj = Rides.fetchRideWithID(rideID: String(self.rideDetails?.rideID?.toInt() ?? 0), context: context)
        self.myGangMapView.delegate = self
        MapTheme.shared.setTheme(map: myGangMapView)///---Set Google map theme color

        self.tableViewJoinedRiders.register(UINib(nibName: "JoinedRidersCell", bundle: nil), forCellReuseIdentifier:"JoinedRidersCell")
        self.tableViewJoinedRiders.backgroundColor = AppTheme.THEME_COLOR
        // Do any additional setup after loading the view.
        if rideDetails != nil{
            plotPolyline()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if rideDetails != nil {
//            rideModel.reloadDataDelegate = self
            rideModel?.getRideMembers(rideID: (self.rideDetails?.rideID?.toInt() ?? 0), type: "GetRideMembers", successCallback: {responseData in
                self.reloadData(data: responseData)
            }, failureCallback:{ failureData in
                self.failure(data: failureData)
            })
        }
        
        
        if journeyManager.isAnyRideInProgress(){
            let currentRideObj = journeyManager.currentRideDetails()
            if currentRideObj.rideID?.toInt() == self.rideDetails?.rideID?.toInt() {
                self.myGangMapView.isHidden = false
            }else{
                self.myGangMapView.isHidden = true
            }
        }else{
            self.myGangMapView.isHidden = true
        }
    }
    
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        if let mrkrData: Int = marker.userData as? Int {
            if mrkrData == 0  || mrkrData == 1  {
                if let customInfoWindow = Bundle.main.loadNibNamed("MarkerCustomView", owner: self, options: nil)?.first as? MarkerCustomView {
                    customInfoWindow.locationLbl.text = self.distance
                    return customInfoWindow
                }
            }
        }
        return nil
    }
    
    @IBAction func onPhoneNumberClick(_ sender: Any) {
        
        let phoneNumber = "tel://" + (self.btnPhoneNumber.titleLabel?.text)!
        UIApplication.shared.open(NSURL(string: phoneNumber)! as URL)
    }
    
    @IBAction func closeRidersInfo(_ sender: Any) {
        self.vwRidersInfo.isHidden = true
    }
    
    func removeAllMarkerFromMap(markerList : [GMSMarker]){
        for each in markerList {
            each.map = nil;
        }
    }
    
    func plotGangMembers(arrRideMember:[RideMemberDetails]){
        
        if markerList.count > 0 {
            removeAllMarkerFromMap(markerList: markerList);
            markerList.removeAll();
        }
        
        if arrRideMember.count > 0 {
            for members in arrRideMember{
                
                
                let markerInfo:NSMutableDictionary = [:]
                
                if let phoneNumber = members.phone_no{
                    markerInfo["phone_number"] = phoneNumber
                }else{
                    markerInfo["phone_number"] = "Not Available"
                }
                
                if let name = members.name{
                    markerInfo["name"] = name
                    
                }else{
                    markerInfo["name"] = "Not Available"
                }
                
                if let timeStamp = members.location_timestamp {
                    let time = "Location as of " + timeStamp.getFormatedDateString(format: "hh:mm a dd/MM")
                    markerInfo["time"] = time
                }else{
                    
                }
                marker = GMSMarker()
                marker?.userData = markerInfo
                if String(bikerID) == members.biker_id {
                    marker?.icon = #imageLiteral(resourceName: "Map Pointer")
                    marker?.isTappable = false
                    if members.latitude != 0 && members.longitude != 0 {
                        marker?.position = CLLocationCoordinate2D(latitude: (members.latitude), longitude: (members.longitude))
                    }
                    
                }
                let status = Int(members.status)
                switch  status {
                case RideMemberStatus.STARTED:
                    if String(bikerID) == members.biker_id {
                        marker?.icon = #imageLiteral(resourceName: "Map Pointer")
                        marker?.isTappable = false
                    }
                    else
                    {
                        marker?.icon = #imageLiteral(resourceName: "Fellow Riders")
                        if members.latitude != 0 || members.longitude != 0 {
                            marker?.position = CLLocationCoordinate2D(latitude: (members.latitude), longitude: (members.longitude))
                        }
                    }
                    break
                    
                default:
                    break
                }
                marker?.map = self.myGangMapView
                markerList.append(marker!);
                //                arrMarkersWavePoints.append(marker)
            }
        }
        
    }
}


extension MyGangVC:UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return joinedBikers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:JoinedRidersCell = tableView.dequeueReusableCell(withIdentifier: "JoinedRidersCell") as! JoinedRidersCell
        cell.joinedRider = self.joinedBikers[indexPath.row]
        
        if String(self.rideDetails?.admin_biker_id?.toInt() ?? 0) == self.joinedBikers[indexPath.row].biker_id
        {
            cell.lblRideAdmin.isHidden = false
        }else{
            cell.lblRideAdmin.isHidden = true
        }
        return cell
    }
}

extension MyGangVC:GMSMapViewDelegate{
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        if let markerInfo:NSMutableDictionary = (marker.userData as? NSMutableDictionary) {
            self.vwRidersInfo.isHidden = false
            self.lblRiderName.text = markerInfo["name"] as? String
            self.btnPhoneNumber.setTitle(markerInfo["phone_number"] as? String, for: .normal)
            self.lblBikerTimestamp.text = markerInfo["time"] as? String
        }
        
        return true
    }
}

extension MyGangVC:RideManagerDelegate {
    
    func plotPolyline()
    {
        var bounds = GMSCoordinateBounds()
       
        
        //Source Marker
        let sourceMarker = GMSMarker()
        sourceMarker.position = CLLocationCoordinate2D(latitude: currentRideObj?.startLocation_lat ?? 0, longitude: currentRideObj?.startLocation_long ?? 0)
        sourceMarker.title = "Starting point"
        sourceMarker.map = self.myGangMapView
        sourceMarker.icon = UIImage(named: "From")
        sourceMarker.tracksViewChanges = true
        sourceMarker.userData = 0
        
        //Destination Marker
        let destiMarker = GMSMarker()
        destiMarker.position = CLLocationCoordinate2D(latitude: currentRideObj?.endLocation_lat ?? 0, longitude: currentRideObj?.endLocation_long ?? 0)
        destiMarker.title = "End point"
        destiMarker.map = self.myGangMapView
        destiMarker.icon = UIImage(named: "To")
        destiMarker.tracksViewChanges = true
        destiMarker.userData = 1
       
        if var viaPointArray = self.currentRideObj?.waypoints?.allObjects as? [WaypointsDetails] {
            for j in 0..<(viaPointArray.count) {
                bounds = bounds.includingCoordinate(CLLocationCoordinate2D(latitude: (viaPointArray[j].latitude), longitude: (viaPointArray[j].longitude)))
            }
        }
        
        if let list = currentRideObj?.routeList {
            let route = NSKeyedUnarchiver.unarchiveObject(with: list as! Data) as? [String]
            for i in 0..<(route?.count)! {
                let path: GMSPath = GMSPath(fromEncodedPath: route![i])!
                let routePolyline = GMSPolyline(path: path)
                routePolyline.spans = [GMSStyleSpan(color: AppTheme.THEME_ORANGE)]
                routePolyline.strokeWidth = 3.0
                routePolyline.map = self.myGangMapView
                routePolyline.geodesic = true
            }
        }
       // self.myGangMapView.drawPolylinewithLatLong(rideObj: currentRideObj!, sourceMArker: GMSMarker(), destMarker: GMSMarker())
        bounds = bounds.includingCoordinate(sourceMarker.position)
        bounds = bounds.includingCoordinate(destiMarker.position)
        self.myGangMapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 150))
        
    }
    
    func getRideMemberList(data: [String:Any]){
        
        if let memberList:[RideMember] = data["data"] as? [RideMember] {
            // print("member count ",memberList.rideMember?.count ?? 0)
            Rides.addRideMemberDetails(rideID: String(self.rideDetails?.rideID?.toInt() ?? 0), context: context, obj: memberList)
            self.joinedBikers = []
            let memberListDB = currentRideObj?.rideMemberList?.allObjects as? [RideMemberDetails]
            for ridemember in memberListDB!{
                
                if (ridemember.status == RideMemberStatus.ACCEPTED) || (ridemember.status == RideMemberStatus.STARTED) || (ridemember.status == RideMemberStatus.FINISHED){
                    self.joinedBikers.append(ridemember)
                }
            }
            self.tableViewJoinedRiders.reloadData()
            
             joinedBikers.sort { $0.name! < $1.name! }
            self.plotGangMembers(arrRideMember: memberListDB!)
        }
        
        LoadingHandler.shared.stoploading()
        
    }
    
    func onFailure(Error:[String:Any]){
        self.view.hideToastActivity()
        let errModel:ErrorModel = Error["data"] as! ErrorModel
        print(errModel.message ?? " ")
        self.view.makeToast((errModel.description)!)
    }
}

extension MyGangVC: ReloadMyRidesDelegate {
    func failure(data: [String : Any]) {
        self.view.hideToastActivity()
        if let errModel:ErrorModel = data["data"] as? ErrorModel {
            print(errModel.message ?? " ")
            self.view.makeToast((errModel.description)!)
        } else {
            if let descriptionString = data["description"] as? String {
                self.view.makeToast(descriptionString)
            }
        }
    }
    
    func reloadData(data: Any) {
        self.view.hideToastActivity()
        self.joinedBikers = []
        var nonAdmins: [RideMemberDetails] = []
        if let memberListFromApi = data as? [RideMemberDetails] {
            let memberDetails = self.sortAndSetData(arrayLogs: memberListFromApi)
            let memberList = memberDetails //{
            for ridemember in memberList{
                if ridemember.biker_id == String(self.rideDetails?.admin_biker_id?.toInt() ?? 0) {
                    self.joinedBikers.append(ridemember)
                } else {
                    if (ridemember.status == RideMemberStatus.ACCEPTED) || (ridemember.status == RideMemberStatus.STARTED) || (ridemember.status == RideMemberStatus.FINISHED){
                        nonAdmins.append(ridemember)
                    }
                }
            }
            self.joinedBikers.append(contentsOf: nonAdmins)
            self.tableViewJoinedRiders.reloadData()
            self.plotGangMembers(arrRideMember: memberList)
        }
       
        //}
    }
    
    func sortAndSetData(arrayLogs:[RideMemberDetails]) -> [RideMemberDetails] {
        //  arrLogList.append(contentsOf: arrayLogs)
        var rideIdArray = Set<String>()
        var unique = [RideMemberDetails]()
        for message in arrayLogs {
            if !rideIdArray.contains(message.biker_id!) {
                    unique.append(message)
                    rideIdArray.insert(message.biker_id!)
                }
        }
        unique.sort { $0.name! < $1.name! }
        return unique
    }
}
