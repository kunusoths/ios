//
//  PhoneVerificationCompleteVC.swift
//  Maximus
//
//  Created by kpit on09/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class PhoneVerificationCompleteVC: UIViewController
{
    @IBOutlet weak var lblHitRoads: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var imgPreviousArrow: UIImageView!
    @IBOutlet weak var rideNowButton: UIButton!
    @IBOutlet weak var imgVwNextArrow: UIImageView!
    
    let appDelegate = Constants.appDelegate//UIApplication.shared.delegate as! AppDelegate
    let strFromwhere:String = MyDefaults.getData(key: DefaultKeys.KEY_FROMWHERE) as! String
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
               self.navigationItem.title = "PHONE NUMBER VERIFICATION"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
    
        let image = #imageLiteral(resourceName: "Next Arrow").withRenderingMode(.alwaysTemplate)
        imgVwNextArrow.image = image
        imgVwNextArrow.tintColor = UIColor.white
        
        let image1 = #imageLiteral(resourceName: "Prev Arrow").withRenderingMode(.alwaysTemplate)
        imgPreviousArrow.image = image1
        imgPreviousArrow.tintColor = UIColor.white
        
        let userDefaults = Foundation.UserDefaults.standard
        let mobileNo  = userDefaults.string(forKey: "mobileno")
        
        self.rideNowButton.addTarget(self, action: #selector(onRideNowClick), for: UIControlEvents.touchUpInside)
        
        let index: String.Index = mobileNo!.index(mobileNo!.startIndex, offsetBy: 2)
        let substring = mobileNo!.substring(from: index)
        let slicedString = "+" + substring
        lblPhoneNumber.text = slicedString
        MyDefaults.setData(value: slicedString, key: DefaultKeys.KEY_PHONENUMBER)
        setButtons()
        
}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // self.navigationController?.navigationBar.topItem?.title = "PHONE NUMBER VERIFICATION"
    }
    
    func setButtons() {
  
        if (strFromwhere == fromViewcontroller.ProfileVC.rawValue)
        {
            self.lblHitRoads.text = "BACK TO PROFILE"
            imgPreviousArrow.isHidden = true
            rideNowButton.isHidden = true

            
        }else{
            rideNowButton.isHidden = true
            imgPreviousArrow.isHidden = true
            lblHitRoads.text = "YOU ARE ALL SET TO HIT ROADS!"
        }
    }
    
    func onRideNowClick(){
        
        let navigationController = UINavigationController(rootViewController: HomeContainerVC())
        let mainViewController = navigationController
        let leftViewController = SideMenuVC()
        
        let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
        slideMenuController.changeLeftViewWidth(self.view.size.width-60)
        
        appDelegate?.window?.rootViewController = slideMenuController
        
    }
    
    @IBAction func btnAllSetAction(_ sender: UIButton) {
        
        if (strFromwhere == fromViewcontroller.HomeVC.rawValue || strFromwhere == fromViewcontroller.ProfileVC.rawValue)
        {
            self.dismissVC(completion: nil)
        } else {
        let navigationController = UINavigationController(rootViewController: HomeContainerVC())

        let mainViewController = navigationController
        let leftViewController = SideMenuVC()
        
        let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
        slideMenuController.changeLeftViewWidth(self.view.size.width-60)
        
        appDelegate?.window?.rootViewController = slideMenuController
        }
    }
   

}

