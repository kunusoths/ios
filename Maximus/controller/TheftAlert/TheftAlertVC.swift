//
//  TheftAlertVC.swift
//  Maximus
//
//  Created by Sriram G on 09/03/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit

class TheftAlertVC: UIViewController
{

    @IBOutlet weak var mapWebView:UIWebView!
    
    public var urlString:String?
    public var message:String?

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Ham Menu"), style: .plain, target: self, action: #selector(menuTapped))
        self.navigationItem.title = "BIKE LOCATION"
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.setWebViewWithUrl()
        
    }

    func menuTapped()
    {
        self.slideMenuController()?.openLeft()
    }
    
    func setWebViewWithUrl()
    {
        UIWebView.loadRequest(mapWebView)(NSURLRequest(url: NSURL(string: self.urlString!)! as URL) as URLRequest)
    }
    
    @IBAction func shareBtnTapped(_ sender: Any)
    {
         let vc = UIActivityViewController(activityItems: [Constants.theftAlertMessage, urlString!], applicationActivities: [])
         present(vc, animated: true)
    }

}
