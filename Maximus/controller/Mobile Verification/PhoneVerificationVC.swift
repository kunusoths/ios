//
//  PhoneVerificationVC.swift
//  Maximus
//
//  Created by kpit on 09/02/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//


import UIKit
import Alamofire
import SlideMenuControllerSwift

class PhoneVerificationVC: UIViewController,UITextFieldDelegate,IOtpListner, ADCountryPickerDelegate {
    
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var btnSend:UIButton!
    @IBOutlet weak var lblErrorMsg: UILabel!
    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var lblDialCode: UILabel!
    
    
    //MARK:viewlifecycle
    
    fileprivate lazy var CallingCodes = { () -> [[String: String]] in
        let resourceBundle = Bundle(for: CountryPicker.classForCoder())
        guard let path = resourceBundle.path(forResource: "CallingCodes", ofType: "plist") else { return [] }
        return NSArray(contentsOfFile: path) as! [[String: String]]
    }()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationItem.title = "PHONE NUMBER VERIFICATION"

        tfPhoneNumber.delegate = self
        
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.navigationItem.rightBarButtonItem?.tintColor = .white
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:UIImage(named: "Back Arrow White"), style: .plain, target: self, action: #selector(backTapped))
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.navigationItem.rightBarButtonItem?.tintColor = .white
        let borderFrame = CGRect(x:0, y:0, width:self.view.bounds.width, height:1)
        tfPhoneNumber.addUpperBorderToKeyaboard(viewFrame: borderFrame)
        let cuurentCountry = Locale.current.regionCode ?? "IN"
        
        self.lblDialCode.text = self.getDialCode(countryCode: cuurentCountry)
        
        
        if let clearButton = tfPhoneNumber.value(forKey: "_clearButton") as? UIButton {
            let templateImage = #imageLiteral(resourceName: "Clear")
            clearButton.setImage(templateImage, for: .normal)
            clearButton.tintColor = .black
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tfPhoneNumber.becomeFirstResponder()
        //self.navigationController?.navigationBar.topItem?.title = "PHONE NUMBER VERIFICATION"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    func getDialCode(countryCode: String) -> String {
        var _dialCode = ""
        let countryData =  CallingCodes.filter { $0["code"] == countryCode }
        if countryData.count > 0,
            let dialCode = countryData[0]["dial_code"] {
            _dialCode = dialCode
        } else {
            
        }
        
        return _dialCode
    }
    
    @IBAction func selectDialCode(_ sender: UIButton){
        let picker = CountryPicker()
        picker.delegate = self
        let pickerNavigationController = UINavigationController(rootViewController: picker)
        self.present(pickerNavigationController, animated: true, completion: nil)
    }
    
    func backTapped() {
        self.view.endEditing(true)
        let appDelegate = Constants.appDelegate//UIApplication.shared.delegate as! AppDelegate
        if (appDelegate?.window?.rootViewController is SlideMenuController)
        {
            self.dismissVC(completion:nil)
        } else {
            self.popVC()
        }
    }
    //MARK:Private Methods
    
    func showErrorMessage(error:String){
        self.btnSend.isHidden = true
        self.lblErrorMsg.isHidden = false
        self.lblErrorMsg.text = error
        
        // Delay 5 seconds
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.lblErrorMsg.isHidden = true
            self.btnSend.isHidden = false
        }
    }

    
    func getOtpFromServer(mobile:String)
    {
        let json: [String: String] = ["mobile": mobile]
        let otpHandlerObj = OtpHandler()
        otpHandlerObj.optListnerDelegate = self
        otpHandlerObj.getOtpForMobile(subUrl:RequestURL.URL_GETOTP.rawValue, parameter:json)
    }

    
    //MARK: IBAction handlers
    
    @IBAction func btnSendOTPAction(_ sender: UIButton)
    {
        if NetworkCheck.isNetwork() {
            self.view.makeToastActivity(.center, isUserInteraction: false)
            let phoneNumb = lblDialCode.text! + tfPhoneNumber.text!
            self.getOtpFromServer(mobile: phoneNumb)
        } else {
            self.view.makeToast(ToastMsgConstant.network)
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let maxLength = 10
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    //MARK:Textfield delegates

    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK:OTP Response delegates
    internal func onFailure(response: DataResponse<Any>)
    {
        
    }
    
    internal func onGenerateOtpSuccess(response: DataResponse<Any>) {
        
        let jsonResult:NSDictionary = response.result.value as! NSDictionary
        self.view.hideToastActivity()
        if response.response?.statusCode == 200
        {
            let mobileNo:String = jsonResult["mobile"] as! String
            let timestamp:String = jsonResult["generated_at"] as! String
            
            let userDefaults = Foundation.UserDefaults.standard
            userDefaults.set( mobileNo, forKey: "mobileno")
            userDefaults.set(timestamp, forKey:"generated_at")
            DispatchQueue.main.async
            {
                self.pushVC(EnterOTPVC())
            }
        }
        else
        {
            DispatchQueue.main.async
                {
                    if let msg = jsonResult["description"] as? String {
                        self.showErrorMessage(error:msg)
                    }
               }
        }
    }
    
    func onPutPhoneNumberSuccess(response:DataResponse<Any>)
    {
    }
    
    func onPutPhoneNumberFailure(response:DataResponse<Any>)
    {
        
    }
    
    internal func onSuccess(response: DataResponse<Any>) {
        
    }
    
    func countryPicker(_ picker: CountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        //print
        
        print("Dial code : \(dialCode) Name : \(name)")
        self.lblDialCode.text = dialCode
       // self.lblCountryName.text = name
    }
    
}

class TextField: UITextField {
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(paste(_:)){
            return false
        }
        return false
    }
}

