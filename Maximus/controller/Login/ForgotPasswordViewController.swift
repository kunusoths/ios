//
//  ForgotPasswordViewController.swift
//  Maximus
//
//  Created by Isha Ramdasi on 24/01/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    var loginHandler = LoginHandler()
    
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var forgotPasswordView: UIView!
    @IBOutlet weak var noAccountErrMsg: UILabel!
    @IBOutlet weak var signUpView: UIView!
    @IBOutlet weak var successView: UIView!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var successMsgLbl: UILabel!
    var enteredEmail: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        self.navigationItem.title = NSLocalizedString("keyForgotPassword", comment: "")
        
        self.navigationController!.navigationBar.isTranslucent = false
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Arrow White"), style: .plain, target: self, action: #selector(backClicked))
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.emailTxtField.delegate = self
        self.noAccountErrMsg.isHidden = true
        self.signUpView.isHidden = true
        if MyDefaults.getBoolData(key: DefaultKeys.KEY_RELOGIN) {
            emailTxtField.isUserInteractionEnabled = false
        }
        if enteredEmail != "" {
            emailTxtField.text = enteredEmail
            submitButton.backgroundColor = AppTheme.THEME_ORANGE
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARKS: Helper Methods
    /*
     Pop View to previous screen
    */
    func backClicked() {
        self.popVC()
    }
    
    //MARK: Button Actions
    /*
     Validates input with forgot password API
     @param: UIButton
     */
    @IBAction func clickForgotPassword(_ sender: UIButton) {
        self.view.endEditing(true)
        self.forgotPasswordView.isHidden = false
        self.noAccountErrMsg.isHidden = true
        if let enteredEmail = self.emailTxtField.text {
            if emailValidation(text: enteredEmail) {
                self.view.makeToastActivity(.center, isUserInteraction: false)
                loginHandler.loginDelegate = self
                loginHandler.forgotPassword(email: enteredEmail)
            } else {
                self.view.makeToast(NSLocalizedString("keyPleaseEnterValidEmail", comment: ""))
            }
        } else {
            self.view.makeToast(NSLocalizedString("keyEnterEmailAddressToProceed", comment: ""))
        }
    }
    /*
     Go back to login view
     @param: UIButton
     */
    @IBAction func onClickLogin(_ sender: UIButton) {
        self.popVC()
    }
    /*
     Push sign up view
     @param: UIButton
     */
    @IBAction func onClickSignup(_ sender: UIButton) {
        let signUpVIew = SignUpViewController()
        self.pushVC(signUpVIew)
    }
}

extension ForgotPasswordViewController: UITextFieldDelegate {
    //MARK: Text Field Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.forgotPasswordView.isHidden = false
        self.noAccountErrMsg.isHidden = true
        self.signUpView.isHidden = true
        submitButton.backgroundColor = AppTheme.THEME_DARK_GRAY
        if emailValidation(text: (textField.text?.appending(string))!) {
            submitButton.backgroundColor = AppTheme.THEME_ORANGE
        }
        return true
    }
    
    /*
     Validates email id
     @param: String
     */
    func emailValidation(text: String) -> Bool {
        let emailRegEx = StringConstant.emailValidationExpression
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: text)
    }
    
}

extension ForgotPasswordViewController: ForgotPasswordCallback {
    /*
     Password changed success
     @param: String
     */

    func passwordChangedSuccess(message: String) {
        self.view.hideToastActivity()
        self.signUpView.isHidden = true
        self.successView.isHidden = false
        self.forgotPasswordView.isHidden = true
    }
    /*
     Failure of API
     @param: String
     */

    func onFailure(Error: String) {
            print(Error)
        self.view.hideToastActivity()
        if Error == NSLocalizedString("keyTimeOut", comment: "") {
           // self.view.makeToast(Error)
            self.view.makeToast(ToastMsgConstant.network)
        } else if Error == NSLocalizedString("keyEmailNotRegisteredForgotPassword", comment: "") {
            self.successView.isHidden = true
            self.forgotPasswordView.isHidden = false
            self.noAccountErrMsg.isHidden = false
            self.signUpView.isHidden = false
        } else if Error == NSLocalizedString("keyEnterValidEmail", comment: ""){
            self.successView.isHidden = true
            self.forgotPasswordView.isHidden = false
            self.signUpView.isHidden = true
            self.noAccountErrMsg.text = Error
            self.noAccountErrMsg.isHidden = false
        } else {
            self.successView.isHidden = false
            self.successMsgLbl.text = Error
            self.forgotPasswordView.isHidden = true
            self.noAccountErrMsg.isHidden = true
            self.signUpView.isHidden = true
        }
    }
}
