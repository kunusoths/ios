//
//  SignUpViewController.swift
//  Maximus
//
//  Created by Isha Ramdasi on 27/12/17.
//  Copyright © 2017 com.maximuspro.com. All rights reserved.
//

import UIKit
import Crashlytics
import CoreData

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var confirmPwdTxtField: SIgnUpTextField!
    @IBOutlet weak var passwordTxtField: SIgnUpTextField!
    @IBOutlet weak var emailTxtField: UITextField!
    
    @IBOutlet weak var loginEmailTxtField: UITextField!
    @IBOutlet weak var loginPwdTxtField: SIgnUpTextField!
    
    @IBOutlet weak var txtFieldView: UIView!
    
    @IBOutlet weak var agreeTCBtn: UIButton!
    @IBOutlet weak var agreePrivacyBtn: UIButton!
    
    @IBOutlet weak var loginView: UIView!
    
    @IBOutlet weak var signUpView: UIView!
    
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var lblTermsOfService: UILabel!
    @IBOutlet weak var lblPrivacyPolicy: UILabel!
    
    @IBOutlet weak var scrollView:UIScrollView!
    var isTermsAgreed = false
    var isPrivacyAgreed = false
    var termsText:NSMutableAttributedString?
    var privacyText: NSMutableAttributedString?
    var activeTextField: UITextField?
    let userObject = UserManagerHandler()
    var isFieldValidationSuccess = false
    var isToastAlreadyShown = false
    
    let bikerViewModel = BikerViewModel()
    
    let signupContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()

    override func viewDidLoad() {
        super.viewDidLoad()
        nameTxtField.delegate = self
        emailTxtField.delegate = self
        passwordTxtField.delegate = self
        confirmPwdTxtField.delegate = self
        
        loginEmailTxtField.delegate = self
        loginPwdTxtField.delegate = self
        
        loginView.isHidden = true
        self.navigationItem.hidesBackButton = true
        self.navigationItem.title = NSLocalizedString("keyTitleSignUp", comment: "")
        
        self.navigationController!.navigationBar.isTranslucent = false
        customizeLabelForTapGesture()
        //force logout
        if MyDefaults.getBoolData(key: DefaultKeys.KEY_ISLOGIN) {
            if !MyDefaults.getBoolData(key: DefaultKeys.KEY_FORCED_LOGOUT) {
                clearDatabase(entity: "Rides")
                clearDatabase(entity: "DMDetails")
                clearDatabase(entity: "DeviceDetails")
                clearDatabase(entity: "OfflineStats")
                clearDatabase(entity: "VCMPacket")
                clearDatabase(entity: "RidePacket")
                AppDetailsHandler.sharedInstance.createAppDetailsObj()
                AppDetailsHandler.sharedInstance.createDMDetailsObj()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !NetworkCheck.isNetwork() {
            ToastPopUp.showNetworkPOPup(message:ToastMsgConstant.network ,view: (Constants.appDelegate?.window?.rootView())!)
        }
        // add keyboard notifications
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowNotification(_:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHideNotification(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    /*
     Customize label for terms and privacy to add tap gesture and underline
     */
    func customizeLabelForTapGesture() {
        lblTermsOfService.isUserInteractionEnabled = true
        lblTermsOfService.addTapGesture(target: self, action: #selector(lblTermsAndConditionsTapAction(_:)))
        lblPrivacyPolicy.isUserInteractionEnabled = true
        lblPrivacyPolicy.addTapGesture(target: self, action: #selector(lblPrivacyTapAction(_:)))
        // create your attributed text and keep an ivar of your "link" text range
       
        let attributes : [String : Any] = [NSFontAttributeName : UIFont.Font(FontName.Roboto, type: FontType.MediumItalic, size: 12.0), NSForegroundColorAttributeName : UIColor.black, NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
        
        privacyText = NSMutableAttributedString.init(string: NSLocalizedString("keyPrivacyPolicy", comment: ""), attributes: attributes)
        termsText = NSMutableAttributedString.init(string: NSLocalizedString("keyTermsPolicy", comment: ""), attributes: attributes)
        
        
        lblTermsOfService.attributedText = termsText
        lblPrivacyPolicy.attributedText = privacyText
        // ivar -- keep track of the target range so you can compare in the callback
        _ = NSMakeRange((privacyText?.length)!, (termsText?.length)!)
    }
    
    //MARK- Button Actions
    /*
     Privacy is tapped to see privacy content
     @param: UITapGestureRecognizer
     */
    func lblPrivacyTapAction(_ sender: UITapGestureRecognizer) {
        let textPrivacy = (lblPrivacyPolicy.text)!
        
        let privacyRange = (textPrivacy as NSString).range(of: "I have read and accepted PRIVACY POLICY")
        if sender.didTapAttributedTextInLabel(label: lblPrivacyPolicy, inRange: privacyRange) {
            let vc = TermsAndCondVC()
            vc.fileName = "Maximuspro-Privacy Policy"
            vc.title = NSLocalizedString("keyPrivacyTitle", comment: "")
            self.pushVC(vc)
        }
    }
    
    /*
     T&C is tapped to see T&C content
     @param: UITapGestureRecognizer
     */
    func lblTermsAndConditionsTapAction(_ sender: UITapGestureRecognizer) {
        let text = (lblTermsOfService.text)!
        
        let termsRange = (text as NSString).range(of: NSLocalizedString("keyTermsPolicy", comment: ""))
        
        if sender.didTapAttributedTextInLabel(label: lblTermsOfService, inRange: termsRange) {
            let vc = TermsAndCondVC()
            vc.fileName = StringConstant.termsServiceFileName
            vc.title = NSLocalizedString("keyTermsTitle", comment: "")
            self.pushVC(vc)
        }
    }
    
    /*
     This method changes state of privacy button on selecting and deselecting
     @param: UIButton
     */
    @IBAction func onClickAgreePrivacyBtn(_ sender: UIButton) {
        if sender.isSelected {
            isPrivacyAgreed = false
            agreePrivacyBtn.setImage(#imageLiteral(resourceName: "checkBox"), for: .normal)
        } else {
            isPrivacyAgreed = true
            agreePrivacyBtn.setImage(#imageLiteral(resourceName: "checkOn"), for: .normal)
        }
        sender.isSelected = !sender.isSelected
        updateLoginBtn()
    }
    /*
     This method changes state of terms and condition button on selecting and deselecting
     @param: UIButton
     */
    @IBAction func onClickAgreeTermsBtn(_ sender: UIButton) {
        if sender.isSelected {
            isTermsAgreed = false
            agreeTCBtn.setImage(#imageLiteral(resourceName: "checkBox"), for: .normal)
        } else {
            isTermsAgreed = true
            agreeTCBtn.setImage(#imageLiteral(resourceName: "checkOn"), for: .normal)
        }
        sender.isSelected = !sender.isSelected
        updateLoginBtn()
    }
    /*
     This method registers new user after successful validation
     @param: UIButton
     */
    @IBAction func onClickContinue(_ sender: UIButton) {
        self.view.endEditing(true)
        if isTermsAgreed && isPrivacyAgreed {
            if !nameTxtField.text!.trimmingCharacters(in: .whitespaces).isEmpty && emailTxtField.text != "" && passwordTxtField.text != "" && confirmPwdTxtField.text != "" {
                // go for validation
                if !emailValidation(text: emailTxtField.text!) {
                    self.view.makeToast(NSLocalizedString("keyEnterValidEmail", comment: ""))
                } else if !passwordValidation(text: passwordTxtField.text!) {
                    self.view.makeToast(NSLocalizedString("keyInvalidPassword", comment: ""))
                } else if passwordTxtField.text != confirmPwdTxtField.text {
                    self.view.makeToast(NSLocalizedString("keyPasswordDontMatch", comment: ""))
                } else {
                    // API calling
                    self.view.makeToastActivity(.center ,isUserInteraction:false)
                    
                    KeychainService.shared[StringConstant.keyPassword] = confirmPwdTxtField.text!
                    MyDefaults.setData(value: emailTxtField.text!.trimmingCharacters(in: .whitespaces), key: DefaultKeys.KEY_BIKER_EMAIL)
                    
                    bikerViewModel.bikerDelegate = self
                    let metaData = [StringConstant.firstName : nameTxtField.text!.trimmingCharacters(in: .whitespaces),
                                    StringConstant.password: confirmPwdTxtField.text!,
                                    StringConstant.email: emailTxtField.text!.trimmingCharacters(in: .whitespaces),
                                    StringConstant.login: emailTxtField.text!.trimmingCharacters(in: .whitespaces)]
                    
                    bikerViewModel.registerUser(loginMethod: .USERNAME_PASSWORD, biker: metaData)
                }
            } else {
                self.view.makeToast(NSLocalizedString("keyFillDetails", comment: ""))
            }
        } else {
            self.view.makeToast(NSLocalizedString("keyAcceptTC", comment: ""))
        }
    }
    
    /*
     This method logs in user after successful validation
     @param: UIButton
     */
    @IBAction func onClickLoginUser(_ sender: UIButton) {
        self.view.endEditing(true)
        if isTermsAgreed && isPrivacyAgreed {
            if (loginEmailTxtField.text?.length)! > 0 && (loginPwdTxtField.text?.length)! > 0 {
                if !emailValidation(text: loginEmailTxtField.text!) {
                    self.view.makeToast(NSLocalizedString("keyEnterValidEmail", comment: ""))
                } else {
                    var loginModel: [String: Any] = [:]
                    loginModel[StringConstant.username] = loginEmailTxtField.text!.trimmingCharacters(in: .whitespaces)
                    loginModel[StringConstant.password] = loginPwdTxtField.text!.trimmingCharacters(in: .whitespaces)
                    loginModel[StringConstant.rememberMe] = "true"
                    KeychainService.shared[StringConstant.keyPassword] = loginPwdTxtField.text!
                    self.bikerViewModel.bikerDelegate = self
                    MyDefaults.setData(value: loginEmailTxtField.text!.trimmingCharacters(in: .whitespaces), key: DefaultKeys.KEY_BIKER_EMAIL)
                    bikerViewModel.authenticateUser(loginMethod: .USERNAME_PASSWORD, biker: loginModel)
                }
            } else {
                self.view.makeToast(NSLocalizedString("keyFillDetails", comment: ""))
            }
        } else {
            self.view.makeToast(NSLocalizedString("keyAcceptTC", comment: ""))
        }
    }
    
    /*
     This method shows sign up view
     @param: UIButton
     */
    @IBAction func onClickSignUp(_ sender: UIButton) {
        self.view.endEditing(true)
        self.loginView.isHidden = true
        self.signUpView.isHidden = false
        self.navigationItem.title = NSLocalizedString("keyTitleSignUp", comment: "")
    }
    /*
     This method shows login view
     @param: UIButton
     */
    @IBAction func onClickLogin(_ sender: UIButton) {
        self.view.endEditing(true)
        self.loginView.isHidden = false
        self.signUpView.isHidden = true
        self.navigationItem.title = NSLocalizedString("keyTitleLogin", comment: "")

    }
    /*
     This method registers user with facebook
     @param: UIButton
     */
    @IBAction func onClickLoginWithFB(_ sender: UIButton) {
        self.view.endEditing(true)
        if isTermsAgreed && isPrivacyAgreed {
            if NetworkCheck.isNetwork(){
                bikerViewModel.bikerDelegate = self
                self.bikerViewModel.registerWithFacebook(viewController: self)
            }else{
                self.view.makeToast(ToastMsgConstant.network)
            }
        } else {
            self.view.makeToast(NSLocalizedString("keyAcceptTC", comment: ""))
        }
    }
    /*
     This method shows user forgot password view
     @param: UIButton
     */
    @IBAction func onClickForgotPassword(_ sender: UIButton) {
        var forgotPassword = ForgotPasswordViewController()
        if let enteredTxt = loginEmailTxtField.text {
            forgotPassword.enteredEmail = enteredTxt.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        self.pushVC(forgotPassword)
    }
    
    /*
     This method updates login button UI as per user interaction
     */
    func updateLoginBtn() {
        if isTermsAgreed && isPrivacyAgreed {
            fbBtn.setImage(#imageLiteral(resourceName: "Facebook"), for: .normal)
            signUpBtn.backgroundColor = AppTheme.THEME_ORANGE
            loginBtn.backgroundColor = AppTheme.THEME_ORANGE
        } else {
            fbBtn.setImage(#imageLiteral(resourceName: "facebook_gray"), for: .normal)
            signUpBtn.backgroundColor = AppTheme.THEME_DARK_GRAY
            loginBtn.backgroundColor = AppTheme.THEME_DARK_GRAY
        }
    }
    
    //MARK: Keyboard Notifications

    /*
     This method adjusts UI when keyboard is shown
     @param: Notification
     */
    @objc func keyboardWillShowNotification(_ notification: Notification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height + 30, right: 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        var aRect = self.view.frame
        aRect.size.height -= keyboardSize.height
        if let activeTextField = self.activeTextField,(!aRect.contains(self.activeTextField!.frame.origin) ) {
            self.scrollView.scrollRectToVisible(self.activeTextField!.frame, animated: true)
        }
    }

    /*
     This method adjusts UI when keyboard is hidden
     @param: Notification
     */
    @objc func keyboardWillHideNotification(_ notification: Notification) {
        self.scrollView.contentInset = .zero
        self.scrollView.scrollIndicatorInsets = .zero
    }
}

extension SignUpViewController: BikerViewModelDelegate {
    /*
     This method loads welcome screen on biker registration success
     */
    func bikerRegistrationSuccess() {
        MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_FORCED_LOGOUT)
        self.view.hideToastActivity()
        self.pushVC(WelcomeVC())//-Jump to next View
    }
    /*
     This method clears database for force logout
     */
    func clearDatabase(entity: String) {
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        do {
            let result = try signupContext.execute(request)
            print("Clear core data \(result)")
        } catch {
            
        }
    }
    
    /*
     Authentication failure callback
     */
    func authenticationFail(data: [String : Any]) {
        
        print("Authentication Failed : \(data)")
        self.view.hideToastActivity()
        let errModel:ErrorModel = data[StringConstant.data] as! ErrorModel
        if errModel.errorDescription != "" {
            if errModel.errorDescription == NSLocalizedString("keyInvalidUserCredentials", comment: ""){
                self.view.makeToast(NSLocalizedString("keyInvalidCredentials", comment: ""))
            }else if errModel.errorDescription == NSLocalizedString("keyUserAlreadyExists", comment: ""){
                self.view.makeToast(NSLocalizedString("keyEmailAlreayRegistered", comment: ""))
            }else{
                self.view.makeToast(errModel.errorDescription!)
            }
        }else if errModel.message == NSLocalizedString("keyUserExistsUsername", comment: "") || errModel.message == NSLocalizedString("keyUserExistsEmail", comment: "") || errModel.message == NSLocalizedString("keyUserExistsUsernameEmail", comment: "") || errModel.message == NSLocalizedString("keyEmailAlreadyInUse", comment: "") || errModel.message == NSLocalizedString("keyLoginAlreadyInUse", comment: ""){
            self.view.makeToast(NSLocalizedString("keyEmailAlreayRegistered", comment: ""))
        } else if errModel.authenticationException == NSLocalizedString("keyBadCredentials", comment: "") && errModel.authMetadata == StringConstant.facebook {
            self.view.makeToast(NSLocalizedString("keyFbUserTryingToUseEmailLogin", comment: ""))
        } else if errModel.authenticationException == NSLocalizedString("keyBadCredentials", comment: "") {
            self.view.makeToast(NSLocalizedString("keyInvalidCredentials", comment: ""))
        } else if errModel.errorCode == 3 {
            self.view.makeToast(NSLocalizedString("keyPleaseEnterValidEmail", comment: ""))
        }else if errModel.message == NSLocalizedString("keyEmailInUse", comment: "") {
            self.view.makeToast(NSLocalizedString("keyEmailAddressInUse", comment: ""))
        } else if errModel.errorCode == 0 {
            self.view.makeToast(ToastMsgConstant.network)
        }else{
            self.view.makeToast((errModel.description)!)
        }
    }
}

extension SignUpViewController: UITextFieldDelegate {
    /*
     This method handles validation for all the text fields on tap of return button
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField.text?.length)! > 0 {
            isToastAlreadyShown = false
            if (textField.tag == 1 && (textField.text?.length)! > 63) {
                isFieldValidationSuccess = true
                self.view.endEditing(true)
                self.view.makeToast(NSLocalizedString("keyNameCharacterLimit", comment: ""))
                isToastAlreadyShown = true
            } else  if ((textField.tag == 2 || textField.tag == 10) && !emailValidation(text: textField.text!) ) {
                isFieldValidationSuccess = true
                self.view.endEditing(true)
                self.view.makeToast(NSLocalizedString("keyEnterValidEmail", comment: ""))
                isToastAlreadyShown = true
            } else if textField.tag == 3 && !passwordValidation(text: textField.text!){
                isFieldValidationSuccess = true
                self.view.endEditing(true)
                self.view.makeToast(NSLocalizedString("keyInvalidPassword", comment: ""))
                isToastAlreadyShown = true
            }  else if textField.tag == 4 && textField.text != passwordTxtField.text {
                self.view.endEditing(true)
                self.view.makeToast(NSLocalizedString("keyPasswordDontMatch", comment: ""))
                isToastAlreadyShown = true
            }
        }
        if !isFieldValidationSuccess {
            if textField.tag == 1  {
                emailTxtField.becomeFirstResponder()
                return true
            } else if textField.tag == 2 {
                passwordTxtField.becomeFirstResponder()
                return true
            } else if textField.tag == 3 {
                confirmPwdTxtField.becomeFirstResponder()
                return true
            } else if textField.tag == 10 {
                loginPwdTxtField.becomeFirstResponder()
                return true
            }
        }
        textField.resignFirstResponder()
        return true
    }
    /*
     This method validates user input while entering the text
     */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true
        }
        if textField.tag == 1 && (textField.text?.length)! < 64 {
            return nameValidation(text: string)
        } else if (textField.tag == 1 && (textField.text?.length)! > 63) {
            self.view.endEditing(true)
            self.view.makeToast(NSLocalizedString("keyNameCharacterLimit", comment: ""))
            return false
        } else if textField.tag == 2 || textField.tag == 10{
            return true
        } else if ((textField.tag == 3 || textField.tag == 4) && (textField.text?.length)! < 20) {
                return isPasswordValid(text: string)
        } else if textField.tag == 20 {
            return true
        }
        return false
    }
    /*
     This method validates user starts entering text
     */
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField

        if isFieldValidationSuccess && (textField.text?.length)! > 0 && !isToastAlreadyShown {
            isFieldValidationSuccess = false
            if (textField.tag == 1 && (textField.text?.length)! > 63) {
                self.view.endEditing(true)
                self.view.makeToast(NSLocalizedString("keyNameCharacterLimit", comment: ""))
            } else  if ((textField.tag == 2 || textField.tag == 10) && !emailValidation(text: textField.text!) ) {
                self.view.endEditing(true)
                self.view.makeToast(NSLocalizedString("keyEnterValidEmail", comment: ""))
            } else if textField.tag == 3 && !passwordValidation(text: textField.text!){
                self.view.endEditing(true)
                self.view.makeToast(NSLocalizedString("keyInvalidPassword", comment: ""))
            } else if textField.tag == 4 && textField.text != passwordTxtField.text {
                self.view.endEditing(true)
                self.view.makeToast(NSLocalizedString("keyPasswordDontMatch", comment: ""))
            }
        }
    }
    /*
     This method resets active text field when text entering is completed
     */
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTextField = nil
    }
    //MARK: Validations
    /*
     This method validates username
     @param: Text to validate
     */
    func nameValidation(text: String)-> Bool {
        do
        {
            let regex = try NSRegularExpression(pattern: StringConstant.nameValidationExpression, options: .caseInsensitive)
            if regex.matches(in: text, options: [], range: NSMakeRange(0, text.length)).count > 0 {return true}
        }
        catch {}
        return false
    }
    /*
     This method validates email address
     @param: Text to validate
     */
    func emailValidation(text: String) -> Bool {
        let emailRegEx = StringConstant.emailValidationExpression
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: text)
    }
    /*
     This method validates password
     @param: Text to validate
     */
    func isPasswordValid(text: String) -> Bool {
        let notAllowedCharacters = StringConstant.passwordValidationExpression
        if !notAllowedCharacters.contains(text) {
            return true
        }
        return false
    }
    /*
     This method validates password
     @param: Text to validate
     */
    func passwordValidation(text: String) -> Bool {
        let notAllowedCharacters = StringConstant.notAllowedCharactersInPassword
        var specialCharPresent = false
        for i in 0..<notAllowedCharacters.length {
            let index = text.getIndexOf(notAllowedCharacters[i])
            if index != nil {
                 specialCharPresent = true
                break
            }
        }
       
        if text.containsAlphabets() && text.rangeOfCharacter(from: NSCharacterSet.decimalDigits)
 != nil && text.length >= 8 && specialCharPresent{
            return true
        }
        return false
    }
    
}

extension UITapGestureRecognizer {
    /*
     This method adds range to label to make some part tappable and some not tappable
     @param: UILabel, NSRange
     */
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                          y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y:
            locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
}
