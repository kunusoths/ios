//
//  LoginHandler.swift
//  Maximus
//
//  Created by Isha Ramdasi on 24/01/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import ObjectMapper

@objc protocol ForgotPasswordCallback {
    @objc optional func passwordChangedSuccess(message: String)
    @objc optional func onFailure(Error:String)
}

class LoginHandler: NSObject {
    var cloudHandlerObj = CloudHandler()
    weak var loginDelegate: ForgotPasswordCallback?
    
    func forgotPassword(email: String) {
        let suburl = "\(RequestURL.URL_FORGOT_PASSWORD.rawValue)"
        
        let parameteres: [String: Any] = ["mail": email]
    
        cloudHandlerObj.makeCloudRequestForStringResponse(subUrl: suburl, parameter:parameteres, isheaderAUTH: false, requestType: .post){ response in

            if let json = response?.result.value {
                if let statusCode = response?.response?.statusCode {
                    if statusCode >= 200 && statusCode < 300 {
                        self.loginDelegate?.passwordChangedSuccess!(message: json)
                    } else {
                        self.loginDelegate?.onFailure!(Error: json)
                    }
                }
                print("Forgot password \(json)")
            }else{
                self.errorCall(value: RequestTimeOut().json)
            }
        }
    }
    
    func errorCall(value:[String : Any]!){
        let ErrorModelObj = Mapper<ErrorModel>().map(JSON:value)
        let errorData:[String:Any] = ["data":ErrorModelObj ?? " "]
        let errModel:ErrorModel = errorData["data"] as! ErrorModel
        self.loginDelegate?.onFailure!(Error: errModel.message!)
    }
}
