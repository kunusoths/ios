//
//  LoginViewController.swift
//  Maximus
//
//  Created by Isha Ramdasi on 08/02/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import Crashlytics

class LoginViewController: UIViewController,UserManagerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var emailTxtField: UITextField!
    
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var passwordTxtField: SIgnUpTextField!
    
    let userObject = UserManagerHandler()
    let loginContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    let bikerViewModel = BikerViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordTxtField.becomeFirstResponder()
        self.navigationItem.hidesBackButton = true
        emailTxtField.text = MyDefaults.getData(key: DefaultKeys.KEY_BIKER_EMAIL) as? String
        passwordTxtField.delegate = self
        self.slideMenuController()?.removeLeftGestures()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: Button actions
    /*
     Show forgot password screen
     @param: UIButton
     */
    @IBAction func onClickForgotPassword(_ sender: UIButton) {
        let forgotPassword = ForgotPasswordViewController()
        if let enteredTxt = emailTxtField.text {
            forgotPassword.enteredEmail = enteredTxt.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        self.pushVC(forgotPassword)
    }
    
    /*
     Login user after validation
     @param: UIButton
     */
    @IBAction func onClickLogin(_ sender: UIButton) {
        self.view.endEditing(true)
        if emailTxtField.text != "" || passwordTxtField.text != "" {
            if !emailValidation(text: emailTxtField.text!) {
                self.view.makeToast("Enter a valid email address.")
            } else {
                bikerViewModel.bikerDelegate = self
                var loginModel: [String: Any] = [:]
                loginModel["username"] = emailTxtField.text!.trimmingCharacters(in: .whitespaces)
                loginModel["password"] = passwordTxtField.text!.trimmingCharacters(in: .whitespaces)
                loginModel["remember_me"] = "true"
                KeychainService.shared["Password"] = passwordTxtField.text!
                MyDefaults.setData(value: emailTxtField.text!.trimmingCharacters(in: .whitespaces), key: DefaultKeys.KEY_BIKER_EMAIL)
                bikerViewModel.authenticateUser(loginMethod: .USERNAME_PASSWORD, biker: loginModel)
            }
        } else {
            self.view.makeToast("Please fill in all the details.")
        }
        
    }
    
    /*
     Email validation
     @param: String
     */
    func emailValidation(text: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: text)
    }
    // MARK: TextField Validation
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: ************  Cloud Request Responce Delegate ***********
    /*
     Login success of facebook user
     @param: data:[String:Any]
     */
    func onSocialLoginAuthenticationSuccess(data:[String:Any]) {
        
        if let user:AuthenticationModel = data["data"] as? AuthenticationModel{
            MyDefaults.setData( value: user.id_token! , key: DefaultKeys.KEY_TOKEN)
            MyDefaults.setInt ( value: user.id!       , key: DefaultKeys.KEY_BIKERID)
            
            
            if user.id != 0 {
                userObject.getBiker(bikeID: user.id!)//-API Call to get Biker Details
            }else{
                self.view.hideToastActivity()
            }
        }
    }
    /*
     Get biker details success
     @param: data:[String:Any]
     */
    func onGetBikerSuccess(data:[String:Any]) {
        
        self.view.hideToastActivity()
        if let objBiker:Biker = data["data"] as? Biker {
            if let id = objBiker.default_vehicle?.id {
                MyDefaults.setInt ( value: id, key: DefaultKeys.KEY_DefaultVehicleID)
            }
            ///Send Biker User ID & Name to Crashalytics and Appsee
            if let bikerID = objBiker.id {
                let strUserIDCrash = "Biker ID: " + String(describing: bikerID)
                Crashlytics.sharedInstance().setUserIdentifier(strUserIDCrash)
                Crashlytics.sharedInstance().setUserEmail(objBiker.email1)
                
                BikerDetails.updateBiker(context: loginContext, biker: objBiker)
                
                if MyDefaults.getData(key: DefaultKeys.KEY_USERNAME) as? String == "" {
                    MyDefaults.setData(value: (objBiker.first_name!), key: DefaultKeys.KEY_USERNAME)
                }

                Crashlytics.sharedInstance().setUserName((MyDefaults.getData(key: DefaultKeys.KEY_USERNAME) as! String))
                
                
                MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_ISLOGIN)//-----Save Login Session
                
            }
        }
        
    }
    /*
     Failure response of login API
     @param: Error:[String:Any]
     */
    func onFailure(Error:[String:Any]){
        print(Error)
        self.view.hideToastActivity()
        let errModel:ErrorModel = Error["data"] as! ErrorModel
        if errModel.authMetadata == "" && errModel.message == "" {
            self.view.makeToast("Email is not registered with us. Please Sign Up.")
        } else if errModel.message == "login already in use." {
            self.view.makeToast("Email address already registered with us. Please Login.")
        } else if errModel.authenticationException == "Bad credentials" {
            self.view.makeToast("Invalid credentials.")
        } else {
            self.view.makeToast((errModel.authenticationException)!)
        }
    }
}

extension LoginViewController:BikerViewModelDelegate {
    /*
     Authentication failure response of login API
     @param: data:[String:Any]
     */
    func authenticationFail(data: [String : Any]) {
        //
        print(data)
        self.view.hideToastActivity()
        let errModel:ErrorModel = data["data"] as! ErrorModel
        if errModel.authMetadata == "" && errModel.message == "" {
            self.view.makeToast("Email is not registered with us. Please Sign Up.")
        } else if errModel.message == "login already in use." {
            self.view.makeToast("Email address already registered with us. Please Login.")
        } else if errModel.authenticationException == "Bad credentials" {
            self.view.makeToast("Invalid credentials.")
        } else {
            self.view.makeToast((errModel.authenticationException)!)
        }
    }
    /*
     Biker registration success
     */
    func bikerRegistrationSuccess() {
        //
        self.pushVC(ProfileVC())
    }
    
}
