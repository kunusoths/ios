//
//  RideRouteVC.swift
//  Maximus
//
//  Created by kpit on 20/04/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import CoreLocation
import ObjectMapper
import GoogleMaps
import GooglePlaces


class RideRouteVC: UIViewController,GMSMapViewDelegate {
    
    @IBOutlet weak var recenterBtn: UIButton!
    @IBOutlet weak var bottomCollectionViewConstant: NSLayoutConstraint!
    @IBOutlet weak var bottomViewinfoConstant: NSLayoutConstraint!
    @IBOutlet weak var mapViewPlan: GMSMapView!
    @IBOutlet weak var lblSourceDestination: UILabel!
    @IBOutlet weak var lblStartEndDates: UILabel!
    @IBOutlet var collectionViewPOI: RideRouteCollectionView!
    @IBOutlet var viewPOIMarkerinfo: UIView!
    @IBOutlet weak var imgViewContact: UIImageView!
    
    @IBOutlet weak var imagviewBusinessHours: UIImageView!
    @IBOutlet weak var imgViewNoRating: UIImageView!
    @IBOutlet weak var viewInfohightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblMarkerInfoTitle: UILabel!
    @IBOutlet weak var lblNoRatings: UILabel!
    @IBOutlet weak var viewStarRating: CosmosView!
    @IBOutlet weak var btnCancelPreview: UIButton!
    @IBOutlet weak var lblContactNumber: UILabel!
    
    @IBOutlet weak var lblOpenNow: UILabel!
    
    @IBAction func btnCancelInfoWindowAction(_ sender: Any) {
        self.showMarkerInfoView(showView: false)
    }
    var rideDetails:Rides?
    let deviceLocation = DeviceLocationServiece.sharedInstance()
    //Wavepoint list and markers array
    var arrWavePoints:[WaypointsDetails] = []
    var arrMarkersWavePoints = [GMSMarker]()
    
    var poiRideID = Int()
    
    var arrMarkersWayPointsRest = [GMSMarker]()
    var arrMarkersWayPointsPolice = [GMSMarker]()
    var arrMarkersWayPointsFuel = [GMSMarker]()
    var arrMarkersWayPointsHosp = [GMSMarker]()
    var arrMarkersWayPointsHotel = [GMSMarker]()
    
    var markerCurrentSelected = GMSMarker()
    var selectedPOIIndex:NSInteger = 0
    var selectedPOI:POIListResponse?
    var currentMarker = GMSMarker()
    
    var isPlotRest = true
    var isPlotHosp = true
    var isPlotPolice = true
    var isPlotFuel = true
    var isPlotHotel = true
    
    
    let journeyManager = JourneyManager.sharedInstance
    var distance: String?
    var planRideBounds: GMSCoordinateBounds?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    var currentRideObj: Rides?
    let planRideRepo = PlanRideRepository()
    var planRideModel: PlanRideViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.rideDetails = Rides.fetchRideWithID(rideID: (self.rideDetails?.rideID)!, context: context)
        //viewInfohightConstraint.constant = 0
        planRideModel = PlanRideViewModel(planRideRepoDelegate: planRideRepo)
        collectionViewPOI.passDataDelegate = self
        planRideBounds = GMSCoordinateBounds()
        mapViewPlan.delegate = self
        if let _ = rideDetails{
            
        }else{
            if(journeyManager.isAnyRideInProgress()){
                self.rideDetails = journeyManager.currentRideDetails()
            }
        }
        
        MapTheme.shared.setTheme(map: mapViewPlan)///---Set Google map theme color
        
        self.updateUI()
        self.mapViewPlan.setDefaultCameraPosition()
        self.drawPolyLineWithWaypoints()
        deviceLocation.configureDeviceLocation()
        let rideRepo = RideRepository()
        rideRepo.repoDelegate = self
        self.view.makeToastActivity(.center, isUserInteraction: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getCurrentLocation()
        self.viewInfohightConstraint.constant = 150
        self.recenterBtn.layer.shadowColor = UIColor(red:0.05, green:0.05, blue:0.05, alpha:0.5).cgColor

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getCurrentLocation() {
//        let isVirtualCM = MyDefaults.getBoolData(key: DefaultKeys.IS_CM_VIRTUAL)
//        let bikerId = MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID)
//        if isVirtualCM {
            let deviceLocation = DeviceLocationServiece.sharedInstance().getUserCurrentLocation()
            self.plotCurrentLocationOfDevice(latitude: Double(deviceLocation.coordinate.latitude), longitude: Double(deviceLocation.coordinate.longitude))
//        }else{
//            planRideModel.getDeviceCurrentLocation(bikerID: bikerId, type: "GetDeviceCurrentLocation")
//        }
    }
    
    func updateUI() {
        if let rideInfo = rideDetails{
            
            let source:String = (rideInfo.startLocation_name)!
            let destination:String = (rideInfo.endLocation_name)!
            
            lblSourceDestination.text = "\(source) to \(destination)"
            
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "E, d MMM yyyy"
            
            let rideStartDate = rideInfo.start_date?.getFormatedDate()
            let strtDt = dateFormatter.string(from: rideStartDate ?? Date())
            
            let endDate = rideInfo.finish_date?.getFormatedDate() //
            let endDt = dateFormatter.string(from: endDate ?? Date())
            
            lblStartEndDates.text = "\(strtDt) to \(endDt)"
        }
    }
    
    func drawPolyLineWithWaypoints() {
        if let rideID = rideDetails?.rideID?.toInt() {
            self.view.makeToastActivity(.center, isUserInteraction: false)
            poiRideID = rideID
            collectionViewPOI.poiRideID = poiRideID
            currentRideObj = Rides.fetchRideWithID(rideID: String(rideID), context: context)
            if currentRideObj != nil {
                if currentRideObj?.routeList != nil {
                    self.distance = String(Int((currentRideObj?.total_ride_distance)!)!/1000) + " km"
                 //   self.mapViewPlan.drawPolylinewithLatLong(rideObj: currentRideObj!, sourceMArker: GMSMarker(), destMarker: GMSMarker())
                    arrWavePoints = (currentRideObj?.waypoints?.allObjects as? [WaypointsDetails])!
                    DispatchQueue.main.async {
                        [weak self] in
                        guard let strongSelf = self else { return }
                        strongSelf.plotWavePoints()
                    }
                  //  self.perform(#selector(plotPolyline), with: nil, afterDelay: 0.5)
                    self.mapViewPlan.drawPolylinewithLatLong(rideObj: currentRideObj!, sourceMArker: GMSMarker(), destMarker: GMSMarker())
                } else {
                    planRideModel?.getRideRouteByID(rideId: Int((currentRideObj?.rideID)!)!, bikerId: MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID), type: "GetRideRouteById", successCallback: { (data) in
                        self.saveRideRouteDetails(data: data)
                    }) { (error) in
                        self.failure(data: error)
                    }
                  //  planRideModel.getRideRouteDetails(rideCard: currentRideObj!, type: "SaveRide")
                }
            } 
        }
    }
    
    func plotPolyline() {
        currentRideObj = Rides.fetchRideWithID(rideID: String(rideDetails?.rideID?.toInt() ?? 0), context: context)
        DispatchQueue.main.async {
        if let _ = self.currentRideObj?.routeList {
            if let route = NSKeyedUnarchiver.unarchiveObject(with: (self.currentRideObj?.routeList!) as! Data) as? [String] {
                for i in 0..<(route.count) {
                    let path: GMSPath = GMSPath(fromEncodedPath: route[i])!
                    let routePolyline = GMSPolyline(path: path)
                    routePolyline.spans = [GMSStyleSpan(color: AppTheme.THEME_ORANGE)]
                    routePolyline.strokeWidth = 3.0
                    routePolyline.map = self.mapViewPlan
                    routePolyline.geodesic = true
                }
            }
            
        }
            self.perform(#selector(self.resignActivityIndicator), with: nil, afterDelay: 1.0)
        }
    }
    
    func resignActivityIndicator() {
        self.view.hideToastActivity()
    }
    
    func savePoiDetails(data: Any) {
        let dataDict = data as? [String: Any]
        if dataDict!["RequestType"] as? String == "GetPOIDetails", let poiData = dataDict!["data"] as? GogglePOIDetailsModel {
            self.poiDetailsSuccess(data: poiData)
        }
    }
    
    func saveRideRouteDetails(data: Any) {
        let dataDict = data as? [String: Any]
        if dataDict!["RequestType"] as? String == "GetRideRouteById" {
            currentRideObj = Rides.fetchRideWithID(rideID: (currentRideObj?.rideID)!, context: context)
            //self.distance = String(Int((currentRideObj?.total_ride_distance) ?? 0)!/1000) + " km"// AppCrashed
            self.mapViewPlan.drawPolylinewithLatLong(rideObj: currentRideObj!, sourceMArker: GMSMarker(), destMarker: GMSMarker())
            arrWavePoints = (currentRideObj?.waypoints?.allObjects as? [WaypointsDetails])!
            DispatchQueue.main.async {
                [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.plotWavePoints()
            }
            self.perform(#selector(plotPolyline), with: nil, afterDelay: 1.0)
        }
    }
    
    func saveRideSuccess(data: Any) {
        let dataDict = data as? [String: Any]
        if  let obj:DeviceDataResponse = dataDict![StringConstant.data] as? DeviceDataResponse, dataDict!["RequestType"] as? String == "GetDeviceCurrentLocation" {
            if (obj.latest_data?.count)! > 0 {
                let objlatest:LatestData  = (obj.latest_data![0]) as LatestData
                self.plotCurrentLocationOfDevice(latitude: objlatest.latitude!, longitude: objlatest.longitude!)
            }
        } else if dataDict!["RequestType"] as? String == "GetRideRouteById" {
            currentRideObj = Rides.fetchRideWithID(rideID: (currentRideObj?.rideID)!, context: context)
            //self.distance = String(Int((currentRideObj?.total_ride_distance) ?? 0)!/1000) + " km"// AppCrashed
            self.mapViewPlan.drawPolylinewithLatLong(rideObj: currentRideObj!, sourceMArker: GMSMarker(), destMarker: GMSMarker())
            arrWavePoints = (currentRideObj?.waypoints?.allObjects as? [WaypointsDetails])!
            DispatchQueue.main.async {
                [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.plotWavePoints()
            }
            self.perform(#selector(plotPolyline), with: nil, afterDelay: 1.0)
        } else if dataDict!["RequestType"] as? String == "GetPOIDetails", let poiData = dataDict!["data"] as? GogglePOIDetailsModel {
            self.poiDetailsSuccess(data: poiData)
        }
    }
    
    func failure(data: [String : Any]) {
        self.view.hideToastActivity()
        if let errModel:ErrorModel = data["data"] as? ErrorModel {
            print(errModel.message ?? " ")
            self.view.makeToast((errModel.description)!)
        } else {
            if let descriptionString = data["description"] as? String {
                self.view.makeToast(descriptionString)
            }
        }
    }
    
    @IBAction func onClickRecenter(_ sender: UIButton) {
        if let isValid = self.planRideBounds?.isValid, isValid {
            self.mapViewPlan.animate(with: GMSCameraUpdate.fit(planRideBounds!))
        } 
    }
    
    func plotWavePoints(){
        var markerIcon = #imageLiteral(resourceName: "Waypoint")
        var viaPointCounter = 1
        var viaPointArray = arrWavePoints.filter({ (($0.types)) as! [String] == [StringConstant.VIA_POINT]})
        viaPointArray = sortAndSetData(arrayLogs: viaPointArray).sorted(by: {Int($0.route_order!)! < Int($1.route_order!)!})
        if arrWavePoints.count > 0{
            for i in 0 ..< arrWavePoints.count {
                let type = arrWavePoints[i].types as? [String]
                if((type?.contains(StringConstant.PLACES_OF_INTEREST))!){
                    
                    if arrWavePoints[i].categories != nil {
                        let categoriesObj = arrWavePoints[i].categories as? [String]
                        if (categoriesObj?.contains(StringConstant.POI_CATEGORY_FUEL_PUMPS))!{
                            markerIcon = #imageLiteral(resourceName: "Petrol Small")
                        }
                        else if (categoriesObj?.contains(StringConstant.POI_CATEGORY_RESTAURANTS))!{
                            markerIcon = #imageLiteral(resourceName: "Restaurants Small")
                        }
                        else if (categoriesObj?.contains(StringConstant.POI_CATEGORY_LODGING))!{
                            markerIcon = #imageLiteral(resourceName: "Hotel Small")
                        }

                    }
                    
                    let marker = GMSMarker()
                    marker.position = CLLocationCoordinate2D(latitude: (arrWavePoints[i].latitude), longitude: (arrWavePoints[i].longitude))
                    marker.icon = markerIcon
                    //Set Marker Info Data
                    let markerInfo:NSMutableDictionary = [:]
                    markerInfo[StringConstant.POI_TITLE] = arrWavePoints[i].name
                    markerInfo[StringConstant.POI_LAT] = (arrWavePoints[i].latitude)
                    markerInfo[StringConstant.POI_LONG] = (arrWavePoints[i].longitude)
                    markerInfo[StringConstant.POI_ID]   = (arrWavePoints[i].poi_id)
                    
                    markerInfo[StringConstant.MARKER_POI_TYPE] = StringConstant.VIA_POINT
                    
                    marker.userData = markerInfo
                    //Info data setting done
                    
                    marker.map = self.mapViewPlan
                    planRideBounds = planRideBounds?.includingCoordinate(marker.position)
                    arrMarkersWavePoints.append(marker)
                } else if (type?.contains(StringConstant.VIA_POINT))! {
                    
                    
                }
                
            }
            
            for j in 0..<viaPointArray.count {
                let viaPoint = GMSMarker()
                viaPoint.position = CLLocationCoordinate2D(latitude: (viaPointArray[j].latitude), longitude: (viaPointArray[j].longitude))
                viaPoint.map = self.mapViewPlan
                let markerInfo:NSMutableDictionary = [:]
                markerInfo[StringConstant.POI_TITLE] = viaPointArray[j].name
                markerInfo[StringConstant.POI_LAT] = (viaPointArray[j].latitude)
                markerInfo[StringConstant.POI_LONG] = (viaPointArray[j].longitude)
                markerInfo[StringConstant.POI_ID]   = (viaPointArray[j].poi_id)
                
                markerInfo[StringConstant.MARKER_POI_TYPE] = StringConstant.PLACES_OF_INTEREST
                
                viaPoint.userData = markerInfo
                arrMarkersWavePoints.append(viaPoint)
                
                if let markerView: ViaPointMarkerView = Bundle.main.loadNibNamed("ViaPointMarkerView", owner: nil, options: nil)![0] as? ViaPointMarkerView {
                    markerView.markerText.setTitle("\(j+1)", for: .normal)
                    viaPoint.iconView = markerView
                }
                planRideBounds = planRideBounds?.includingCoordinate(viaPoint.position)
                viaPoint.tracksViewChanges = true
                viaPointCounter = viaPointCounter + 1
            }
            planRideBounds =  planRideBounds?.includingCoordinate(CLLocationCoordinate2D(latitude: (currentRideObj?.startLocation_lat)!, longitude: (currentRideObj?.startLocation_long)!))
            planRideBounds = planRideBounds?.includingCoordinate(CLLocationCoordinate2D(latitude: (currentRideObj?.endLocation_lat)!, longitude: (currentRideObj?.endLocation_long)!))
            mapViewPlan.animate(with: GMSCameraUpdate.fit(planRideBounds!))
        }
        LoadingHandler.shared.stoploading()
    }
    
    func sortAndSetData(arrayLogs:[WaypointsDetails]) -> [WaypointsDetails] {
        //  arrLogList.append(contentsOf: arrayLogs)
        var rideIdArray = Set<String>()
        var unique = [WaypointsDetails]()
        for message in arrayLogs {
            if !rideIdArray.contains(message.name!) {
                unique.append(message)
                rideIdArray.insert(message.name!)
            }
        }
        
        return unique
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        if let mrkrData: Int = marker.userData as? Int {
            if mrkrData == 0  || mrkrData == 1  {
                if let customInfoWindow = Bundle.main.loadNibNamed("MarkerCustomView", owner: self, options: nil)?.first as? MarkerCustomView {
                    customInfoWindow.locationLbl.text = self.distance
                    return customInfoWindow
                }
            }
        }
        return nil
    }
    
    
    
    
    //MARK:- MapView Delagates
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if NetworkCheck.isNetwork() {
            if(marker != markerCurrentSelected){// && CURRENET_POI_SELECTED >= 0){
                
                if(arrMarkersWayPointsFuel.contains(marker)){
                    selectedPOIIndex = arrMarkersWayPointsFuel.index(where: {$0 === marker})!
                }
                else if(arrMarkersWayPointsHosp.contains(marker)){
                    selectedPOIIndex = arrMarkersWayPointsHosp.index(where: {$0 === marker})!
                }
                else if(arrMarkersWayPointsPolice.contains(marker)){
                    selectedPOIIndex = arrMarkersWayPointsPolice.index(where: {$0 === marker})!
                }
                else if(arrMarkersWavePoints.contains(marker)) {
                    selectedPOIIndex = 0
                }
                
                if(selectedPOIIndex >= 0){
                    viewStarRating.rating = 0//-default set to zero & Initially UserInteraction is disabled
                    markerCurrentSelected.map = nil
                    selectedPOIIndex = -1
                    
                    
                    if let markerInfo = marker.userData as? NSMutableDictionary {
                        if let string = markerInfo[StringConstant.MARKER_POI_TYPE] as? String, (string == StringConstant.VIA_POINT || string == StringConstant.PLACES_OF_INTEREST){
                            if let placeID = markerInfo[StringConstant.POI_ID] as? String, string == StringConstant.VIA_POINT {
                                self.view.makeToastActivity(.center ,isUserInteraction:false)
                                planRideModel?.getPOIDetails(poiID: placeID, request: "GetPOIDetails", successCallback: { (data) in
                                    self.savePoiDetails(data: data)
                                    self.showMarkerInfoView(showView: true)
                                }) { (error) in
                                    self.failure(data: error)
                                }
                            }else if string == StringConstant.PLACES_OF_INTEREST{
                                mapView.selectedMarker = marker
                                mapView.selectedMarker!.snippet = (markerInfo[StringConstant.POI_TITLE] as! String)
                            }else{
                                showMarkerInfoView(showView: false)
                            }
                        } else {
                            showMarkerInfoView(showView: false)
                        }
                    }
                    
                    self.view.hideToastActivity()
                } 
                
            }
        } else {
            self.view.hideToastActivity()
            self.view.makeToast(ToastMsgConstant.network)
        }
        return true
    }
    
    
}
//MARK:ColectionView Flow Layout delegate methods
extension RideRouteVC {
    
    //MARK:POI Ploting
    func plotWayPoints(arrWayPoints:[PointsOfInterests],catagoryType:SQlitePoiCatagoryType)  {
        var markerIcon = #imageLiteral(resourceName: "Waypoint")
        
        switch catagoryType {
        case SQlitePoiCatagoryType.KEY_POI_RESTAURANT:
            isPlotRest = false
            arrMarkersWayPointsRest.removeAll()
            collectionViewPOI.arrMarkersWayPointsRest.removeAll()
            markerIcon = #imageLiteral(resourceName: "Resturant Small Selected")
            break
            
        case SQlitePoiCatagoryType.KEY_POI_HOTEL:
            isPlotHotel = false
            arrMarkersWayPointsHotel.removeAll()
            collectionViewPOI.arrMarkersWayPointsHotel.removeAll()
            
            markerIcon = #imageLiteral(resourceName: "Hotel Small Selected")
            break
            
        case SQlitePoiCatagoryType.KEY_POI_GAS_STATION:
            isPlotFuel = false
            arrMarkersWayPointsFuel.removeAll()
            collectionViewPOI.arrMarkersWayPointsFuel.removeAll()
            
            markerIcon = #imageLiteral(resourceName: "Fuel Pump Small")
            break
            
        case SQlitePoiCatagoryType.KEY_POI_POLICE:
            isPlotPolice = false
            arrMarkersWayPointsPolice.removeAll()
            collectionViewPOI.arrMarkersWayPointsPolice.removeAll()
            
            markerIcon = #imageLiteral(resourceName: "Police Station_Small")
            break
            
        case SQlitePoiCatagoryType.KEY_POI_HOSPITAL:
            isPlotHosp = false
            arrMarkersWayPointsHosp.removeAll()
            collectionViewPOI.arrMarkersWayPointsHosp.removeAll()
            
            markerIcon = #imageLiteral(resourceName: "Hospitals Small")
            break
        }
        
        for wayPoint in arrWayPoints {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: wayPoint.poiLatitude, longitude: wayPoint.poiLongitude)
            marker.icon = markerIcon
            //Set Marker Info Data
            let markerInfo:NSMutableDictionary = [:]
            markerInfo[StringConstant.POI_TITLE] = wayPoint.poiName
            markerInfo[StringConstant.POI_LAT] = wayPoint.poiLatitude
            markerInfo[StringConstant.POI_LONG] = wayPoint.poiLongitude
            markerInfo[StringConstant.POI_ID] = wayPoint.poiPlaceID
            markerInfo[StringConstant.MARKER_POI_TYPE] = StringConstant.VIA_POINT
            print("wayPoint.poiPlaceID ",wayPoint.poiPlaceID)
            marker.userData = markerInfo
            //Info data setting done
            marker.map = self.mapViewPlan
            
            switch catagoryType {
            case SQlitePoiCatagoryType.KEY_POI_RESTAURANT:
                arrMarkersWayPointsRest.append(marker)
                collectionViewPOI.arrMarkersWayPointsRest.append(marker)
                break
            case SQlitePoiCatagoryType.KEY_POI_GAS_STATION:
                arrMarkersWayPointsFuel.append(marker)
                collectionViewPOI.arrMarkersWayPointsFuel.append(marker)
                break
            case SQlitePoiCatagoryType.KEY_POI_HOTEL:
                arrMarkersWayPointsHotel.append(marker)
                collectionViewPOI.arrMarkersWayPointsHotel.append(marker)
                break
            case SQlitePoiCatagoryType.KEY_POI_POLICE:
                arrMarkersWayPointsPolice.append(marker)
                collectionViewPOI.arrMarkersWayPointsPolice.append(marker)
                break
            case SQlitePoiCatagoryType.KEY_POI_HOSPITAL:
                arrMarkersWayPointsHosp.append(marker)
                collectionViewPOI.arrMarkersWayPointsHosp.append(marker)
                break
            }
        }
        var dict:[String: Any] = [:]
        dict["category"] = catagoryType
        dict["isSelected"] = false
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SelectedWaypoint"), object: nil, userInfo: dict)
    }
    
    
    func showMarkerInfoView(showView:Bool){
        if(showView){
            bottomViewinfoConstant.constant = 0
            bottomCollectionViewConstant.constant = 14
        }else{
            bottomCollectionViewConstant.constant = 0
            bottomViewinfoConstant.constant = -153
        }
    }
}

//MARK: Delegate Method
extension RideRouteVC:RideManagerDelegate, RidesRepoCallback,PassWaypointsDataToController
{
    func setMarkerToNil() {
        markerCurrentSelected.map = nil
    }
    
    
    func poiDetailsSuccess(data:GogglePOIDetailsModel)
    {
        LoadingHandler.shared.stoploading()
        viewStarRating.rating = 0//-default set to zero & Initially UserInteraction is disabled
        
        lblMarkerInfoTitle.text = data.result?.name
        imgViewContact.isHidden = false
        imagviewBusinessHours.isHidden = false
        
        if let buissnessPhoneNumber = data.result?.formatted_phone_number{
            lblContactNumber.text = buissnessPhoneNumber
        }else{
            lblContactNumber.text = "Contact Details not available"
        }
        
        if let rating = data.result?.rating {
            viewStarRating.isHidden = false
            lblNoRatings.isHidden = true
            imgViewNoRating.isHidden = true
            viewStarRating.rating = rating//set exact Rating
        }else{
            viewStarRating.isHidden = true
            lblNoRatings.isHidden = false
            imgViewNoRating.isHidden = false
        }
        
        if let openHours = data.result?.opening_hours?.weekday_text {
            let today:String = Date().dayOfWeek()!
            if(openHours.count > 0){
                for day in openHours{
                    if day.contains(today){
                        lblOpenNow.text = day
                        break
                    }
                }
            }
        }
        
        if lblOpenNow.text == ""{
            lblOpenNow.text = (data.result?.formatted_address ?? "")
            imagviewBusinessHours.image = #imageLiteral(resourceName: "Address POI Business Card")
        }else{
            imagviewBusinessHours.image = #imageLiteral(resourceName: "Business Hours")
        }
        lblOpenNow.fitHeight()
        
    }
    
    
    func onFailure(Error:[String:Any]){
        LoadingHandler.shared.stoploading()
        let errModel:ErrorModel = Error["data"] as! ErrorModel
        print(errModel.message ?? " ")
        self.view.makeToast((errModel.description)!)
    }
    
    func plotCurrentLocationOfDevice(latitude:  Double, longitude: Double) {
        self.view.hideToastActivity()
        //current Marker
        let originPos = CLLocationCoordinate2DMake(latitude,longitude)
        currentMarker.map = nil
        currentMarker = GMSMarker(position: originPos)
        currentMarker.title = "My Location"
        currentMarker.map = self.mapViewPlan
        currentMarker.icon = nil
        currentMarker.position = originPos
        let resizedimage = UIImage.scaleTo(image:#imageLiteral(resourceName: "Map Pointer"), w: 40.0, h: 44.0)
        currentMarker.icon = resizedimage
    }
    
    func leaveRideSuccess() {
        self.view.hideToastActivity()
    }
    
    func repoSuccessCallback(data: Any) {
    }
    
    func repoFailureCallback(error: [String : Any]) {
        self.view.hideToastActivity()
    }
    
    func passData(category:SQlitePoiCatagoryType, arrOfWavepoints: [PointsOfInterests]) {
        self.plotWayPoints(arrWayPoints:arrOfWavepoints,catagoryType:category)
    }
}
