//
//  RideStatusVc.swift
//  Maximus
//
//  Created by kpit on 18/04/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import ObjectMapper
import GooglePlaces

typealias JSONDictionary = [String:Any]

class RideStatusVc: UIViewController,CLLocationManagerDelegate {
    
    //MARK:- Properties
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lblWayPoints: UILabel!
    @IBOutlet weak var lblCurrentCity: UILabel!
    @IBOutlet weak var lblCurrentCityTemperature: UILabel!
    @IBOutlet weak var lblWayPointCount: UILabel!
    @IBOutlet weak var lblFisrtPoiName: UILabel!
    @IBOutlet weak var lblFirstPoiTemp: UILabel!
    @IBOutlet weak var lblSecondPOIname: UILabel!
    @IBOutlet weak var lblSecondPOITemp: UILabel!
    @IBOutlet weak var lblThirdPOIName: UILabel!
    @IBOutlet weak var lblThirdPOITemp: UILabel!
    @IBOutlet weak var lblTimeToNextPOI: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    let sinkManager = SinkManager.sharedInstance
    let journeyManager = JourneyManager.sharedInstance
    let rideStateHandler = RideStateHandler.sharedInstance
    let navLocator = NavigationLocator.sharedInstance()
    
    let weatherObj = WeatherManager()
    let bikerID = MyDefaults.getInt (key: DefaultKeys.KEY_BIKERID)
    
    var rideDetails:Rides?
    
    var placesClient: GMSPlacesClient!
    var currentLocation: CLLocation!
    var markerCurrentLocation = GMSMarker()
    
    var currentCity:String?
    
    // MARK:- Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblWayPoints.text = ""
        if let title = rideDetails?.ride_title {
            self.navigationItem.title = title.uppercased()
        }else {
            self.navigationItem.title = "RIDE ON"
        }
        
        self.placesClient = GMSPlacesClient.shared()
        self.mapView.setDefaultCameraPosition()
        self.navLocator.navTestDelegate = self
        MapTheme.shared.setTheme(map: mapView)///---Set Google map theme color

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.makeToastActivity(.center, isUserInteraction: false)
        
        if journeyManager.isAnyRideInProgress(){
            if rideDetails?.rideID?.toInt() == journeyManager.currentRideId(){
                
                self.registerToSinkUpdates(register: true)
                
                self.rideDetails = journeyManager.currentRideDetails()
                //self.getStopNames()
                
                let totalStops = rideStateHandler.getTotalStops()
                if (totalStops > 1){
                    self.lblWayPointCount.text = "\(totalStops) Stops"
                }else{
                    self.lblWayPointCount.text = "No Stops"
                }
                self.loadPolyline(indexesToBePlote: rideStateHandler.getPlottedIndexes())
            }
            self.updatePoi1()
            self.updatePoi2()
            self.updatePoi3()
        }
        
        self.callPlaceName()
        
        NotificationCenter.default.addObserver(self, selector:#selector(callPlaceName), name: .UIApplicationDidBecomeActive, object: nil)
        
        self.view.hideToastActivity()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if journeyManager.isAnyRideInProgress(){
            if rideDetails?.rideID?.toInt() == journeyManager.currentRideId(){
                DispatchQueue.main.async {
                    self.getStopNames()
                }
            }
        }
        
    }
    override func viewWillLayoutSubviews(){
        super.viewWillLayoutSubviews()
        self.scrollView.contentSize = CGSize(width: self.view.frame.width , height: 352)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.registerToSinkUpdates(register: false)
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .UIApplicationDidBecomeActive, object: nil)
    }
    
    
    //MARK:- Private methods
    
    private func registerToSinkUpdates(register:Bool){
        sinkManager.changeRegisterationForDailyIterneryUpdates(dailyIternary: self, register: register)
    }
    
    func callPlaceName(){
        
        placesClient.currentPlace(callback: { (placeLikelihoodList, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            self.lblCurrentCity.text = "No current place"
            
            var currentPlaceName:String?
            
            if let placeLikelihoodList = placeLikelihoodList {
                let place = placeLikelihoodList.likelihoods.first?.place
                if let place = place {
                    if let addressComponents = place.addressComponents{
                        for component in addressComponents{
                            if (component.type == "sublocality_level_2"){
                                currentPlaceName = component.name
                                break
                            }
                        }
                    }
                    
                    if let name = currentPlaceName{
                        if !place.name.contains(name){
                            self.lblCurrentCity.text = "You are at: " + place.name + ", " + name
                        }
                    }else{
                        self.lblCurrentCity.text = "You are at: " + place.name
                    }
                    let lat = place.coordinate.latitude
                    let lon = place.coordinate.longitude
                    self.setTempraturefromLatLong(latitude: lat, longitude: lon, lable: self.lblCurrentCityTemperature)
                }
            }
        })
    }
    
    //MARK:- Poi Details Methods
    func updatePoi1(){
        let poiDic:[String:Any] = rideStateHandler.getFirstPoiDetails()
        
        let poiName:String = poiDic[StringConstant.FIRST_POI] as! String
        let poiLat:Double = poiDic[StringConstant.LATITUDE] as! Double
        let poiLong:Double = poiDic[StringConstant.LONGITUDE] as! Double
        
        self.setTempraturefromLatLong(latitude: poiLat, longitude: poiLong, lable: self.lblFirstPoiTemp)
        DispatchQueue.main.async{
            self.lblFisrtPoiName.text  = " \(poiName)"
            
        }
    }
    
    func updatePoi2(){
        let poiDic:[String:Any] = rideStateHandler.getSecondPoiDetails()
        
        let poiName:String = poiDic[StringConstant.SECOND_POI] as! String
        let poiLat:Double = poiDic[StringConstant.LATITUDE] as! Double
        let poiLong:Double = poiDic[StringConstant.LONGITUDE] as! Double
        
        self.setTempraturefromLatLong(latitude: poiLat, longitude: poiLong, lable: self.lblSecondPOITemp)
        DispatchQueue.main.async{
            self.lblSecondPOIname.text  = " \(poiName)"
        }
    }
    
    func updatePoi3(){
        let poiDic:[String:Any] = rideStateHandler.getThirdPoiDetails()
        
        let poiName:String = poiDic[StringConstant.THIRD_POI] as! String
        let poiLat:Double = poiDic[StringConstant.LATITUDE] as! Double
        let poiLong:Double = poiDic[StringConstant.LONGITUDE] as! Double
        
        self.setTempraturefromLatLong(latitude: poiLat, longitude: poiLong, lable: self.lblThirdPOITemp)
        DispatchQueue.main.async{
            self.lblThirdPOIName.text  = " \(poiName)"
        }
    }
    
    
    //MARK:- Weather related Methods
    //Call to Accuweather API and get Temprature from Lat & Long
    func setTempraturefromLatLong(latitude:Double?, longitude:Double?, lable:UILabel){
        
        if (latitude != 0 && longitude != 0) && ((latitude != nil) && (longitude != nil))
        {
            self.weatherObj.getCurrentTemperatureFromOW(lat: latitude!, lng: longitude!) { temprature in
                var temp = ""
                if temprature == "" {
                    temp =  "Temp :NA"
                }else{
                    temp =  "Temp :" + temprature + "°C"
                }
                DispatchQueue.main.async{
                    lable.text = temp
                }
            }
        }else {
            DispatchQueue.main.async{
                lable.text = "Temp :NA"
            }
        }
    }
    
    //MARK:- Map Related Methods
    
    func loadPolyline(indexesToBePlote:[Int]){
        
        self.mapView.clear()
        if let objroute:RouteDetails = Mapper<RouteDetails>().map(JSONString: journeyManager.getTBTRoute()){
            if let route = objroute.routeList?[0] {
                
                let legs:[Legs] = route.legs!
                
                for index in 0..<legs.count{
                    
                    if (indexesToBePlote.contains(index)){
                        
                        let steps:[Steps] = legs[index].steps!
                        if steps.count > 0{
                            for step in steps{
                                let polylinePoint:StepPolyline = step.polylinePoints!
                                if let point = polylinePoint.points{
                                    let path: GMSPath = GMSPath(fromEncodedPath: point)!
                                    let routePolyline = GMSPolyline(path: path)
                                    routePolyline.spans = [GMSStyleSpan(color:AppTheme.THEME_ORANGE)]
                                    routePolyline.strokeWidth = 5.0
                                    routePolyline.map = self.mapView
                                    routePolyline.geodesic = true
                                }
                            }
                        }
                    }
                }
            }
        }
        
        let dataDic:[String:Any] = rideStateHandler.getStartEndDailyDestinationLocations()
        
        if dataDic.count > 0 {
            let startLoc = dataDic["StartLocation"] as! StartLocation
            let startName = dataDic["StartLocationName"] as! String
            let endLoc = dataDic["EndLocation"] as! EndLocation
            let endLocName = dataDic["EndLocationName"] as! String
            let points = dataDic["Points"] as! String
            self.plotRouteEndMarkers(startLoc: startLoc, startLocName: startName, endLoc: endLoc, endLocName: endLocName, points: points)
        }
    }
    
    func plotRouteEndMarkers(startLoc:StartLocation,startLocName:String,endLoc:EndLocation,endLocName:String,points:String)
    {
        //Source Marker
        let sourceMarker = GMSMarker()
        sourceMarker.position = CLLocationCoordinate2DMake((startLoc.lat)!, (startLoc.lng)!)
        sourceMarker.title = startLocName
        sourceMarker.map = self.mapView
        sourceMarker.icon = UIImage(named: "From")//#imageLiteral(resourceName: "Source")//GMSMarker.markerImage(with: UIColor.green)
        sourceMarker.tracksViewChanges = true
        
        //Destination Marker
        let destiMarker = GMSMarker()
        destiMarker.position = CLLocationCoordinate2DMake((endLoc.lat)!, (endLoc.lng)!)
        destiMarker.title = endLocName
        destiMarker.map = self.mapView
        destiMarker.icon = UIImage(named: "To")
        destiMarker.tracksViewChanges = true
        
        
        let googleObj = GooglePlacesService()
        googleObj.getAddressForLatLng(latitude: "\((endLoc.lat)!)", longitude: "\((endLoc.lng)!)"){ json in
            if let result = json["results"] as? NSArray {
                if let details = result[0] as? NSDictionary{
                    if let addressComponants = details["address_components"] as? NSArray{
                        for component in addressComponants{
                            if let compo = component as? NSDictionary{
                                if let types = compo["types"] as? NSArray{
                                    if types.contains("locality"){
                                        if let city = (component as! NSDictionary)["short_name"]{
                                          self.lblTimeToNextPOI.text = "Arrival To \(city) : -- hour -- mins"
                                        }
                                        break
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        
        
        
        let path: GMSPath = GMSPath(fromEncodedPath: points)!
        
        var bounds = GMSCoordinateBounds()
        for index in 0...path.count() {
            bounds = bounds.includingCoordinate(path.coordinate(at: index))
        }
        self.mapView.animate(with: GMSCameraUpdate.fit(bounds))
    }
    
    func getStopNames(){
        
        var arrWavePointNames:[String] = []
        let obj:PlannedWavePointsModel = rideStateHandler.getPlannedWayPointsModel()
        if (obj.WavePointsList != nil){
            if let wavePointList = obj.WavePointsList{
                if (wavePointList.count) > 0 {
                    for wavePoint in wavePointList{
                        /* var findName = true
                         if let wtypes = wavePoint.types{
                         
                         if wtypes.contains("PLACES_OF_INTEREST"){
                         findName = false
                         }
                         }*/
                        
                        //if findName {
                        let loc:CLLocation = CLLocation(latitude: (wavePoint.location?.latitude)!, longitude: (wavePoint.location?.longitude)!)
                        self.currentLocation = loc
                        let googleObj = GooglePlacesService()
                        googleObj.getAddressForLatLng(latitude: "\((wavePoint.location?.latitude)!)", longitude: "\((wavePoint.location?.longitude)!)"){ json in
                            if let result = json["results"] as? NSArray {
                                if let details = result[0] as? NSDictionary{
                                    if let addressComponants = details["address_components"] as? NSArray{
                                        for component in addressComponants{
                                            if let compo = component as? NSDictionary{
                                                if let types = compo["types"] as? NSArray{
                                                    if types.contains("locality"){
                                                        if let city = (component as! NSDictionary)["short_name"]{
                                                            arrWavePointNames.append(city as! String)
                                                            if (arrWavePointNames.count) > 0 {
                                                                self.lblWayPoints.text = arrWavePointNames.joined(separator: "-")
                                                            }
                                                        }
                                                        break
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        //}
                    }
                }
            }
        }
    }
    
    
    //MARK:- Sink Manager Notifications
    
    func catchNotificationPlotStatusRoute(notification:Notification) -> Void{
        
        guard let plotLegsWithIndexes:[Int] = notification.userInfo!["Data"] as! [Int]? else { return }
        self.loadPolyline(indexesToBePlote: plotLegsWithIndexes)
    }
    
    func catchNotificationPlotStatusRouteEndPoints(notification:Notification) -> Void{
        
        guard let dataDic:[String:Any] = notification.userInfo!["Data"] as! [String:Any]? else { return }
        
        let startLoc = dataDic["StartLocation"] as! StartLocation
        let startName = dataDic["StartLocationName"] as! String
        let endLoc = dataDic["EndLocation"] as! EndLocation
        let endLocName = dataDic["EndLocationName"] as! String
        let points = dataDic["Points"] as! String
        
        self.plotRouteEndMarkers(startLoc: startLoc, startLocName: startName, endLoc: endLoc, endLocName: endLocName, points: points)
    }
    
    func catchNotificationLegProgressCompleted(notification:Notification) -> Void{
        self.updatePoi1()
        self.updatePoi2()
        self.updatePoi3()
        self.loadPolyline(indexesToBePlote: rideStateHandler.getPlottedIndexes())
    }
    
}

//MARK:- Navigation Locator Delegate
extension RideStatusVc : NavigationLocatorDelegate {
    
    func updateCurrentLocation(location:CLLocation){
        DispatchQueue.main.async{
            let currentPosition = CLLocationCoordinate2DMake(Double(location.coordinate.latitude),Double(location.coordinate.longitude))
            self.markerCurrentLocation.map = nil
            self.markerCurrentLocation = GMSMarker(position: currentPosition)
            self.markerCurrentLocation.title = "My Location"
            self.markerCurrentLocation.map = self.mapView
            let resizedimage = UIImage.scaleTo(image: #imageLiteral(resourceName: "Map Pointer"), w: 40.0, h: 44.0)
            self.markerCurrentLocation.icon = resizedimage
        }
    }
}


/*
 
 //Get Current Device Location
 self.rideObj.getCurrentDeviceLocation(bikerID:self.bikerID)
 
 func getdeviceLocationSuccess(data:[String:Any]){
 self.view.hideToastActivity()
 if let obj:DeviceDataResponse = data["data"] as? DeviceDataResponse {
 if (obj.latest_data?.count)! > 0 {
 let objlatest:LatestData  = (obj.latest_data![0]) as LatestData
 
 //current Marker
 let originPos = CLLocationCoordinate2DMake(Double(objlatest.latitude!),Double(objlatest.longitude!))
 
 self.currentMarker.map = nil
 self.currentMarker = GMSMarker(position: originPos)
 self.currentMarker.title = "My Location"
 self.currentMarker.map = self.mapView
 let resizedimage = UIImage.scaleTo(image: #imageLiteral(resourceName: "Map Pointer"), w: 40.0, h: 44.0)
 self.currentMarker.icon = resizedimage
 
 let camera = GMSCameraPosition.camera(withLatitude: Double(objlatest.latitude!), longitude: Double(objlatest.longitude!), zoom: 12.0)
 
 self.mapView?.animate(to: camera)
 
 }
 }
 }*/


/*func getAdress(completion: @escaping (_ address: JSONDictionary?, _ error: Error?) -> ()) {
 
 let geoCoder = CLGeocoder()
 
 geoCoder.reverseGeocodeLocation(self.currentLocation) { placemarks, error in
 
 if let e = error {
 
 completion(nil, e)
 
 } else {
 
 let placeArray = placemarks!
 
 var placeMark: CLPlacemark!
 
 placeMark = placeArray[0]
 
 guard let address = placeMark.addressDictionary as? JSONDictionary else {
 return
 }
 completion(address, nil)
 }
 }
 }*/
