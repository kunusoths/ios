    //
//  CurrentRideVC.swift
//  Maximus
//
//  Created by Admin on 20/04/17.
//  Copyright © 2017 com.kpit.maximus. All rights reserved.
//

import UIKit
import GoogleMaps
import Charts
import Alamofire
import CoreData
import ObjectMapper

class CurrentRideVC: UIViewController ,AlertApiManagerDelegate,UserManagerDelegate, CurrentRideViewModelDelegate {

    //MARK:- UI Outlets
    @IBOutlet weak var btnPro: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var viewNoRideInProgress: UIView!
    @IBOutlet weak var lbLAvgSpeed: UILabel!
    @IBOutlet weak var btnOptOut: UIButton!
    @IBOutlet weak var btnStartNavigation: UIButton!
    @IBOutlet weak var vwOptOutStrtNav: UIView!
    @IBOutlet weak var btnSOS: UIButton!
    @IBOutlet weak var recenterBtn: UIButton!
    @IBOutlet weak var lblMute: UILabel!
    @IBOutlet weak var btnMute: UIButton!
    //MARK:- Properties
    
    let currentRideViewModel = CurrentRideViewModel()
    
    let alertManager:AlertApiManager = AlertApiManager()
    var generatedAlertId:String?
    var uuid:String?
    let userObject = UserManagerHandler()
    var emailIDPopupToShow: Bool = false
    let deviceLocation = DeviceLocationServiece.sharedInstance()
    let bikerID = MyDefaults.getInt (key: DefaultKeys.KEY_BIKERID)
    let journeyManager = JourneyManager.sharedInstance
    let sinkManager = SinkManager.sharedInstance
    
    let btnSOSColor = UIColor(r: 244, g: 67, b: 54)
    
    var parentContainerVc:RideInProgressContainer?
    var rideDetails:Rides?
    var progressView: UIProgressView?
    var timer: Timer?
    
    var sourceMarker = GMSMarker()
    var destiMarker = GMSMarker()
    var currentMarker = GMSMarker()
    var speedLattitude:[Double] = []
    var speedLongitude:[Double] = []
    
    // var showNavAssistance:Bool = false
    var strRideDate = ""
    var doOnece:Int = 1
    
    
    var flagGraphSuccess = false
    var _direction_TBTRoute:String?//----To save Planned direction Route
    var rideWayPointsList = [WaypointsDetails]()
    var rideBounds: GMSCoordinateBounds?
    var currentRideStats:RideStatsModel?
    var totalDistanceInMeters: Int = 0
    var totalTime: Int = 0
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
    var bikerDetails:BikerDetails!
    let repo = LocationRepository()
    var locationViewModel:LocationServicesViewModel?
    var bikeMarker = GMSMarker()
    
    var ridesViewModel:MyRidesViewModel!
    let rideRepo = RideRepository()
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Ham Menu"), style: .plain, target: self, action: #selector(menuTapped))
        if let title = rideDetails?.ride_title?.uppercased() {
            self.navigationItem.title = title
        }else {
            self.navigationItem.title = "CURRENT RIDE"
        }
        self.ridesViewModel = MyRidesViewModel(rideRepoDelegate: rideRepo)
        NotificationCenter.default.addObserver(self, selector:
            #selector(self.upgradeDM), name: NSNotification.Name(rawValue: NotificationIdentifier.upgardeDmIdentifier), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(userLocation), name: NSNotification.Name(rawValue: StringConstant.LOCATION_SERVICE_ENABLED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(userLocation), name: NSNotification.Name(rawValue: sinkManager.SINK_NOTIFICATION_LOCATION_UPDATE), object: nil)
        self.lblMute.layer.masksToBounds = true
        self.lblMute.layer.cornerRadius = 6
        viewNoRideInProgress.isHidden = true
        btnSOS.isHidden = true
        self.btnPro.isHidden = !ISSimulationON
        self.bikerDetails = BikerDetails.getBikerDetails(context: context)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showEmailVerifyPopup), name: NSNotification.Name(rawValue: "showEmailPopup"), object: nil)
        showEmailNotVerifiedPopup()

        alertManager.alertManagerDelegate = self
        
        MapTheme.shared.setTheme(map: mapView)///---Set Google map theme color
        sinkManager.addObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_CLOSED, selectorName: self.catchNotificationAlertClosed)
        btnStart.backgroundColor = AppTheme.THEME_ORANGE
        self.locationViewModel = LocationServicesViewModel(locationRepo: repo)
        
        //ride edited or past ride when POI are not cached
        if let rideCard = Rides.fetchRideWithID(rideID: String(rideDetails?.rideID?.toInt() ?? 0), context: context) {
            if let array = rideCard.waypoints?.allObjects as? [WaypointsDetails] {
                 rideWayPointsList =  array
            }
        }
        
        //
        if self.rideDetails?.status == 1 && self.rideDetails?.biker_status == Int16(RideMemberStatus.STARTED) {
            self.enableStopButton()
            if rideDetails?.planned ?? false {
                //journeyManager.startNavigationEngine(tbt_route: (rideDetails?.tbt_route)!)
            }
        }else{
            if self.rideDetails?.planned ?? false{
                self.setRideStartButton()
            }
        }
        
        let alertDbManager = AlertDatabaseManager()
        let arrResult = alertDbManager.getAlertsFromReceivedAlertTableWithStatus(status: AlertConstant.DB_STATE_ACKNOWLEDGED)
        let alertsArray = arrResult as! [ReceiveAlert]
        if alertsArray.count > 0{
            
            if let rideId = rideDetails?.rideID?.toInt() {
                if let ackRideId = alertsArray.get(at: 0)?.alertRideId{
                    
                    if (Int32(rideId) == ackRideId){
                        
                        if let alertId =  alertsArray.get(at: 0)?.alertId{
                            self.generatedAlertId = "\(alertId)"
                        }
                        if let uuid =  alertsArray.get(at: 0)?.alertUUID{
                            self.uuid = uuid
                        }
                        updateAppAlertUI()
                    }
                }
            }
        }
        
        
        if journeyManager.isQuickRide {
           // journeyManager.isQuickRide = false
            self.currentRideViewModel.delegate = self
            self.currentRideViewModel.startRide(type: .PLANNED, rideId: (self.rideDetails?.rideID?.toInt()!)!)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.updateStats()
        self.addObserverOnView()
        //to refres ride details
        self.refreshRideDetails()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.addControls()
        self.recenterBtn.layer.shadowColor = UIColor(red:0.05, green:0.05, blue:0.05, alpha:0.5).cgColor
    }

    override func viewDidAppear(_ animated: Bool) {
    
        super.viewDidAppear(animated)
        
        if self.journeyManager.isAnyRideInProgress() {
            if self.journeyManager.rideProgressType() == RideTypes.Planned {
                if self.rideDetails?.rideID?.toInt() == self.journeyManager.currentRideId(){
                    
                    self.setupAudioButton()
                    if let rideJoiners = self.rideDetails?.joined_users {
                        if rideJoiners > 1{
                            self.btnSOS.isHidden = false
                        }
                    }
                }
            }
        } else {
            if let parentVc = self.parentContainerVc, self.rideDetails == nil {
                parentVc.hideTabView()
                self.viewNoRideInProgress.isHidden = false
            }
            self.btnStart.addLongPressGesture(target: self, action: #selector(self.longTap))
        }
        
        getCurrentLocationOfUser()
        
        if self.rideDetails?.planned ?? false {
            
        }else{
            if let parentVc = self.parentContainerVc {
                parentVc.hideTabView()
            }
        }

    }
    


    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if journeyManager.isQuickRide {
        journeyManager.isQuickRide = false
        PlanRideDataManager.destoy()
        }
        SinkManager.sharedInstance.removeObserver(observer: self, notificationKey: SinkManager.sharedInstance.SINK_NOTIFICATION_NETWORK_STATE_CHANE)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("*******************viewDidDisappear called****************")
        if journeyManager.isAnyRideInProgress(){
            self.removeObserverOnView()
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationIdentifier.upgardeDmIdentifier), object: nil)
        NotificationCenter.default.removeObserver(self)
    }
    
    deinit {
        sinkManager.removeObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_CLOSED)
        sinkManager.removeObserver(observer: self, notificationKey: sinkManager.ALERT_NOTIFICATION_ACKNOWLEDGE)
        
    }
    
    func refreshRideDetails() {
        if let rideID = self.rideDetails?.rideID?.toInt() {
            if self.rideDetails?.rideID?.toInt() != self.journeyManager.currentRideId(){
                self.ridesViewModel.getRide(withRideId: rideID, andBikerId: bikerID, request: "GetRideOfBiker", successCallback: { (data) in
                    //
                }) { (error) in
                    //
                }
                
            }
        }
    }
    
    //MARK:- CurrentRideViewModel Delegate Methods
    
    func startRideSuccess(data: [String : Any]) {
        //
        guard let rideDetails:Rides = data["rideData"] as? Rides else{
            return
        }
        self.rideDetails = rideDetails
        self.parentContainerVc?.rideDetails = rideDetails
        let rideJoiners = rideDetails.joined_users
        
            if rideJoiners > 1 {
                self.btnSOS.isHidden = false
            }else{
            }
        
        if rideDetails.planned {
            self.setupAudioButton()
        }
        self.view.hideToastActivity()
        self.enableStopButton()


    }
    
    func stopRideSuccess(data: [String : Any]) {
        
        self.removeObserverOnView()
        self.view.hideToastActivity()
        self.enableStartButton()
        let vc = AchievementsVC()
        vc.showOfflineStats = true
        vc.showOfflineStopMsg = true
        vc.rideID = self.rideDetails?.rideID
        vc.rideTitle = self.rideDetails?.ride_title?.uppercased()
        self.pushVC(vc)
    }
    
    func rideInProgressSuccess(data: [String : Any]) {
        //
        print("Current RideVC : rideInProgressSuccess")
        self.view.hideToastActivity()

        let rideDetails:Rides = data["data"] as! Rides
        
        self.btnStart.isEnabled = true
        //Add Observer to reload data once app comes in foregraound
//        self.addObserverOnView()
        
        self.rideDetails = rideDetails
        
        if rideDetails.planned{
            
            if let rideJoiners = self.rideDetails?.joined_users{
                
                if rideJoiners > 1 {
                    self.btnSOS.isHidden = false
                }
                
            }
            
            if NavigationLocator.sharedInstance().getStatusOfNavigation() != .NAVIGATORSTATEACTIVE {
                journeyManager.getJourneyRideRouteById(rideID: (self.rideDetails?.rideID?.toInt()) ?? 0, bikerId:bikerID)
            }else{
                self._direction_TBTRoute = journeyManager.getTBTRoute()
            }
            
            //self.btnShowTopSpeed.isHidden = false
            
        }else{
            
            //Ad-Hoc Ride Logic is Here
            //self.btnShowTopSpeed.isHidden = true
            
            if let parentVc = self.parentContainerVc {
                parentVc.hideTabView()
            }
            
        }

        
        self.navigationController?.topViewController?.navigationItem.title = rideDetails.ride_title?.uppercased()
        self.rideDetails = rideDetails
        self.addControls()
      //  self.getRideStatandRouteData()
        self.enableStopButton()
        self.loadView()
        
    }
    
    func rideAPIFailed(data: [String : Any]) {
        //
        self.view.hideToastActivity()
        if let errModel:ErrorModel = data["data"] as? ErrorModel {
            print(errModel.message ?? " ")
            self.view.makeToast((errModel.description)!)
        } else {
            if let descriptionString = data["description"] as? String {
                self.view.makeToast(descriptionString)
            }
        }
    }
    
    func setupAudioButton() {
        self.btnMute.isHidden = false
        self.btnMute.isSelected = true
        let isAudioEnabled = MyDefaults.getBoolData(key: DefaultKeys.KEY_AUDIO_STATUS)
        self.toggleAudioButton(enabled: isAudioEnabled)
    }
    
    func toggleAudioButton(enabled: Bool) {
        self.btnMute.isSelected = enabled

        if enabled {
            self.btnMute.setImage(UIImage(named: "Audio-on"), for: .normal)
            self.lblMute.text = NSLocalizedString("keyAudioUnmuteText", comment: "")
        }else{
            self.btnMute.setImage(UIImage(named: "Audio-off"), for: .normal)
            self.lblMute.text = NSLocalizedString("keyAudioMuteText", comment: "")
        }
        
    }
    
    func animateMuteLable() {
        UIView.animate(withDuration: 2, animations: {
            //
            self.btnMute.isUserInteractionEnabled = false
            self.lblMute.alpha = 0.4
            self.lblMute.isHidden = false
            
        }) { (fimished) in
            UIView.animate(withDuration: 1,  animations: {
                self.lblMute.alpha = 0//
            }) { (finished) in
                self.btnMute.isUserInteractionEnabled = true
                self.lblMute.isHidden = true
            }
            
        }
    }
    
    // MARK:- IB outlet methods
    
    @IBAction func onMuteClick(_ sender: UIButton) {
        
        if !sender.isSelected {
            // Audio is off hence enable Audio
             NavigationLocator.sharedInstance().enableAudioAttributes()
            self.toggleAudioButton(enabled: true)
            sender.isSelected = true
           
        } else {
            // Audio is on hence disable audio
            NavigationLocator.sharedInstance().disableAudioAttributes()
            self.toggleAudioButton(enabled: false)
            //SpeechService.sharedInstance.stop("")
            sender.isSelected = false
        }
        self.animateMuteLable()
        
    }
    
    @IBAction func btnSOSPressed(_ sender: Any) {
        
        let vc = MyAlertVC(alertCategory: CP_AlertTypeInfoTypeDef(rawValue: 0) , AlertID: 0 , isAutoGenerateAlert:false)
        let navigationController = UINavigationController(rootViewController:vc)
        self.presentVC(navigationController)
    }
    
    @IBAction func onClickRecenter(_ sender: UIButton) {
        if (NetworkReachabilityManager()?.isReachable)! {
            if CLLocationManager.locationServicesEnabled() {

                if let _ = rideBounds {
                    self.mapView.animate(with: GMSCameraUpdate.fit(rideBounds!))
                } else {
                    if destiMarker.position.latitude != -180 && destiMarker.position.latitude != 0 {
                        //recenter after starting ride and location off
                        self.mapView.animate(to: GMSCameraPosition.camera(withTarget: destiMarker.position, zoom: 10))
                    }
                }
            }else {
                self.showLocationTurnOnPopup()
            }
        } else {
            // entire route
            if CLLocationManager.locationServicesEnabled() {
                if let _ = rideBounds {
                    self.mapView.animate(with: GMSCameraUpdate.fit(rideBounds!))
                } else if destiMarker.position.latitude != -180 && destiMarker.position.latitude != 0{
                    self.mapView.animate(to: GMSCameraPosition.camera(withTarget: destiMarker.position, zoom: 10))
                } else {
                    self.mapView.setDefaultCameraPosition()
                }
            } else {
                self.showLocationTurnOnPopup()
            }
        }
    }
    
    @IBAction func btnProAction(_ sender: Any) {
        print("ProClick Action")
        if let route = self._direction_TBTRoute {
            let vc = DMDisplayVC()
            vc.tbtDirectionRoute = route
            self.pushVC(vc)
            //let nav = UINavigationController(rootViewController: vc)
            //self.presentVC(nav)
        }else{
            self.view.makeToast("No Ride Started yet", duration: 0.5, position: .bottom)
        }
    }
    
    @IBAction func btnOptOutPressed(_ sender: Any) {
        
        /*self.vwOptOutStrtNav.isHidden = true
         
         
         
         let alertDbManager = AlertDatabaseManager()
         let arrResult = alertDbManager.getAlertsFromReceivedAlertTableWithStatus(status: AlertConstant.DB_STATE_ACKNOWLEDGED)
         let alertsArray = arrResult as! [ReceiveAlert]
         if alertsArray.count > 0{
         
         if let rideId = rideDetails?.id{
         if let ackRideId = alertsArray.get(at: 0)?.alertRideId{
         
         if (Int32(rideId) == ackRideId){
         
         let alertContext:AlertContext = AlertContext()
         alertContext.changeStateToAlertDiscard(alertId: Int32((alertsArray.get(at: 0)?.alertId)!))
         }
         }
         }
         }*/
    }
    
    @IBAction func btnStartNavigation(_ sender: Any) {
        
    }
    /*
     TODO:
     Tags
     100 - For Start Button (Default 100 is Set)
     200 - for Stop Button
     */
    @IBAction func btnStart_Action(_ sender: Any) {
        print("Normal Stop button Action Performed")
        //self.updateDMStatus(status: BluetoothManager.sharedInstance().getBLEState())
        let isCMRegisterd = MyDefaults.getBoolData(key: DefaultKeys.KEY_CMREGISTER)//-----Get CM Registration Status
    
        if(isCMRegisterd){
            let isOtpVerified = self.bikerDetails.is_otp_verified ////-----Get OTP Verification Status
            if isOtpVerified || MyDefaults.getBoolData(key: DefaultKeys.KEY_OTPVERIFIED){
                if btnStart.tag == 100 &&  journeyManager.isAnyRideInProgress() {
                    self.view.makeToast("Stop the Current Ride to start a new one.", duration: 0.5,position: .bottom)
                    if let gestureArray = btnStart.gestureRecognizers {
                        for gesture in gestureArray {
                            if gesture is UILongPressGestureRecognizer {
                                self.btnStart.removeGestureRecognizer(gesture)
                            }
                        }
                    }
                    
                } else {
                    self.view.makeToast("Tap & Hold", duration: 0.5, position: .bottom)
                    self.btnStart.addLongPressGesture(target: self, action: #selector(self.longTap))
                }
            }else{
                self.showAlertForOtp()
            }
        }else{
            self.showAlert()
        }
        
    }

    
    //MARK:- Private Methods
    func showEmailNotVerifiedPopup(){
        if bikerDetails.auth_method ?? AUTH_METHOD.UNKNOWN.rawValue == AUTH_METHOD.USERNAME_PASSWORD.rawValue {
            let bikerID = MyDefaults.getInt(key: DefaultKeys.KEY_BIKERID)
            userObject.userManagerDelegate = self
            userObject.getBiker(bikeID: bikerID)
        }
    }
    
    // MARK:- UserManagerDelegate methods
    func onGetBikerSuccess(data:[String:Any]) {
        if let biker:Biker = data["data"] as? Biker {
            if let verified = biker.email1_verified {
                if !verified {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showEmailPopup"), object: nil)
                } else if verified {
                    MyDefaults.setData(value: biker.email1!, key: DefaultKeys.KEY_BIKER_EMAIL)
                }
            }
        }
    }
    
    func onFailure(Error: [String : Any]) {
        //
        print("Api failure \(Error)")
    }
    
    func updateDMStatus(status:BluetoothState){
        if status == .DISCONNECT_NP || status == .DISCONNECT_E || status == .DISCONNECT_N || status == .DISCONNECT_BONP || status == .NONE {
            let alert = UIAlertController(title: "Pair Display Module", message:"Connect your phone to the Display for enhanced ride experience. Make sure bluetooth of your device is turned ON", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(ok)
            self.presentVC(alert)
        }
    }
    
    func updateAppAlertUI() {
        
        let optOptVC = OptOutVC()
        optOptVC.rideID = self.rideDetails?.rideID?.toInt()//self.alertDetail?.ride_id
        optOptVC.alertId = self.generatedAlertId
        
        let navAlertVC: UINavigationController = UINavigationController(rootViewController: optOptVC)
        
        weak var pvc = UIApplication.topViewController()?.presentingViewController
        
        if pvc != nil {
            UIApplication.topViewController()?.dismiss(animated: true, completion: {
                pvc?.present(navAlertVC, animated: true, completion: nil)
            })
        }else{
            UIApplication.topViewController()?.presentVC(navAlertVC)
            
        }
        
    }
    
    func getCurrentRideStats(rideId: Int) -> RideStatsModel?{
        let rideStatsObj = RideStatsModel()
        let offlineStatsContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.newBackgroundContext()
        if let rideCard = Rides.fetchRideWithID(rideID: String(rideId), context: offlineStatsContext) {
                let achievement = rideCard.achievement
            rideStatsObj.distanceTravelled = achievement?.distance_travelled ?? 0
            rideStatsObj.travelledTime = TimeInterval(achievement?.time_duration ?? 0)
            rideStatsObj.topSpeed = Double(achievement?.top_speed ?? 0)
            rideStatsObj.averageSpeed = achievement?.average_speed ?? 0
            if  let archivedLocs = achievement?.rideLocations ,
                let locs = NSKeyedUnarchiver.unarchiveObject(with: archivedLocs as! Data) as? [CLLocation] {
                rideStatsObj.rideLocations = locs
            }else{
                
            }
            return rideStatsObj
        }
        return nil
    }
    
    func catchNotificationAlertClosed(notification:Notification) -> Void{
        print("Alert Closed")
        vwOptOutStrtNav.isHidden = true
        
    }
    
    func updateStats() {
        
        if self.rideDetails?.biker_status == 6 {
//            self.rideDetails = journeyManager.currentRideDetails()
            if let  rideId = self.rideDetails?.rideID?.toInt() {
                self.currentRideStats = self.getCurrentRideStats(rideId: rideId)
                self.showLocalStats()
            }
        }
        else
        {
            
        }
    }
    
    func appEntersForeGround(){
        self.updateStats()
    }

    func getRideStatus(){
        print("Observer Called")
        //Check ride is in progress or not
        
        if let rideDetail = self.rideDetails {
            
            if(rideDetail.planned){
                if rideDetails != nil {
                    self.sendNextLegInfo()
                }
            }else{
            }
            
        }else{
        }
        
        //Get Current Device Location
        if CLLocationManager.locationServicesEnabled() {
            getCurrentLocation()
        } else {
            //rideBounds = nil
            self.mapView.setDefaultCameraPosition()
            recenterBtn.setBackgroundImage(#imageLiteral(resourceName: "recenter_offline"), for: .normal)
        }
    }
    
    func getCurrentLocationOfUser() {
        //Get Current Device Location
        if CLLocationManager.locationServicesEnabled() {
            getCurrentLocation()
            recenterBtn.setBackgroundImage(#imageLiteral(resourceName: "recenter"), for: .normal)
            if let _ = rideBounds {
                self.mapView.animate(with: GMSCameraUpdate.fit(rideBounds!))
            }
        } else {
            //rideBounds = nil
            self.mapView.setDefaultCameraPosition()
            recenterBtn.setBackgroundImage(#imageLiteral(resourceName: "recenter_offline"), for: .normal)
        }
    }
    
    func userLocation() {
        if CLLocationManager.locationServicesEnabled() {
            if self.destiMarker.position.latitude != -180 && self.destiMarker.position.latitude != 0{
                NotificationCenter.default.removeObserver(
                    self, name: NSNotification.Name(rawValue: sinkManager.SINK_NOTIFICATION_LOCATION_UPDATE),
                    object: nil)
            }
            recenterBtn.setBackgroundImage(#imageLiteral(resourceName: "recenter"), for: .normal)
            getCurrentLocation()
        } else {
            recenterBtn.setBackgroundImage(#imageLiteral(resourceName: "recenter_offline"), for: .normal)
        }
    }
    
    func getCurrentLocation() {
        self.plotCurrentLocation()
    }
    
    func getDeviceLocation() {
        destiMarker.map = nil
        let deviceLocation = DeviceLocationServiece.sharedInstance().getUserCurrentLocation()
        if deviceLocation.coordinate.latitude != 0 {
            destiMarker.position = deviceLocation.coordinate
            destiMarker.map = self.mapView
            destiMarker.title = StringConstant.MYLOCATION
            let resizedimage = UIImage.scaleTo(image: UIImage(named: "Source")!, w: 20.0, h: 20.0)
            destiMarker.icon = resizedimage
            if let _ = rideBounds, CLLocationManager.locationServicesEnabled() {
                self.mapView.animate(with: GMSCameraUpdate.fit(rideBounds!))
            } else {
                self.mapView.animate(to: GMSCameraPosition.camera(withLatitude: CLLocationDegrees(exactly: deviceLocation.coordinate.latitude)!, longitude: CLLocationDegrees(exactly: deviceLocation.coordinate.longitude)!, zoom: 10.0))
            }
        } else {
            self.mapView.setDefaultCameraPosition()
        }
        
    }
    
    func plotCurrentLocation() {
        
        let isVirtualCM = MyDefaults.getBoolData(key: DefaultKeys.IS_CM_VIRTUAL)
        if !isVirtualCM {
            //  self.locationViewModel.getDeviceLocation(bikerID: bikerID, requestType: "GetCurrentDeviceLocation")
            self.locationViewModel?.getDeviceLocation(bikerID: bikerID, requestType: "GetCurrentDeviceLocation", handler: { (data, errData) in
                if let err = errData {
                    print("Error \(err)")
                    self.locationFailure(data: err)
                }
                
                if let resData = data {
                    print("res data \(resData)")
                    self.locationSuccess(data: resData)
                }
                
            })
        }
        self.getDeviceLocation()
        
//        let deviceLocation = DeviceLocationServiece.sharedInstance().getUserCurrentLocation()
//        if deviceLocation.coordinate.latitude != 0 {
//            currentMarker.map = nil
//            currentMarker.position = deviceLocation.coordinate
//            currentMarker.title = StringConstant.MYLOCATION
//            currentMarker.map = self.mapView
//            let resizedimage = UIImage.scaleTo(image: UIImage(named: "Source")!, w: 20.0, h: 20.0)
//            currentMarker.icon = resizedimage
//        }
//        self.recenterBtn.setBackgroundImage(#imageLiteral(resourceName: "recenter"), for: .normal)
//
//            let locs = self.currentRideStats?.rideLocations ?? []
//            if locs.count > 0 {
//
//                for location in locs {
//                    rideBounds = rideBounds?.includingCoordinate(location.coordinate)
//                }
//                if let _ = rideBounds {
//                    self.mapView.animate(with: GMSCameraUpdate.fit(rideBounds!))
//                }
//            }else{
//                let bounds = GMSCoordinateBounds()
//                bounds.includingCoordinate(currentMarker.position)
//                if let _ = rideBounds, NetworkCheck.isNetwork() {
//                    self.mapView.animate(with: GMSCameraUpdate.fit(bounds))
//                } else {
//                    self.mapView.setDefaultCameraPosition()
//                }
//            }
        DispatchQueue.main.async {
            self.view.hideToastActivity()
        }
    }
    
    func locationSuccess(data: Any) {
        if let deviceResponse = data as? [String: Any], let dict = deviceResponse["data"] as? DeviceDataResponse{
            bikeMarker.map = nil
            bikeMarker.position = CLLocationCoordinate2D(latitude: dict.latest_data?.first?.latitude ?? 0, longitude: dict.latest_data?.first?.longitude ?? 0)
            bikeMarker.map = self.mapView
            bikeMarker.title = StringConstant.BIKELOCATION
            let resizedimage = UIImage.scaleTo(image: UIImage(named: "Map Pointer")!, w: 40, h: 44)
            bikeMarker.icon = resizedimage
            self.getDeviceLocation()
        }
    }
    
    func locationFailure(data: [String : Any]) {
        if let description = data["data"] as? String, data["RequestType"] as? String == "GetCurrentDeviceLocation" {
            bikeMarker.map = nil
            if !MyDefaults.getBoolData(key: DefaultKeys.KEY_VirtualCm_Offline_ToastMessageShown){
                self.view.makeToast(description)
                MyDefaults.setBoolData(value: true, key: DefaultKeys.KEY_VirtualCm_Offline_ToastMessageShown)
            }
        }
    }
    
    func menuTapped() {
        self.slideMenuController()?.openLeft()
    }
    
    func showLegalAlert() {
        let alertController = UIAlertController(title: "Legal Notice", message: "WARNING: Please keep your eyes on the road while riding. Do not use the app or phone while riding.\n Legal Notice:It is your responsibility to observe safe driving practices. You are responsible to secure and use your product in a manner that will not cause accidents, personal injury or any damage to yourself or anyone around you.\nFor more information visit maximuspro.com/legal/termsofservice", preferredStyle: UIAlertControllerStyle.alert)
        let yesAction = UIAlertAction(title: "I Agree", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
//            self.view.makeToastActivity(.center ,isUserInteraction:false)
            //self.journeyManager.startPlannedRide(rideId: (self.rideDetails?.id)!, rideDetails: self.rideDetails!)
            self.currentRideViewModel.delegate = self
            self.currentRideViewModel.startRide(type: .PLANNED, rideId: (self.rideDetails?.rideID?.toInt()!)!)
        }
        
        alertController.addAction(yesAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func sendNextLegInfo(){
        var city = ""
        if !journeyManager.isAnyRideInProgress() {
            if (self.rideDetails?.planned)!{
                if self.rideWayPointsList.count > 0 {
                    var parsedArray = self.rideWayPointsList.filter({$0.route_order != "-1" && $0.route_order != "0"})
                    parsedArray = parsedArray.sorted(by: {$0.route_order! < $1.route_order!})
                    if  parsedArray.count == 0 {
                        let destination = self.rideWayPointsList.filter({$0.route_order == "-1"})
                        let nextStop = destination[0]
                        city = nextStop.name ?? "Milestone"
                    } else {
                        let nextStop = parsedArray[0]
                        city = nextStop.name ?? "Milestone"
                    }
                }else{
                    /// if there is no waypoint then show destination name as next name
                    if let name = self.rideDetails?.endLocation_name{
                        city = name
                    }
                }
                MaximusDM.sharedInstance.sendNextCityName(nextCity: city)
            }
        }
    }
    
    func showAlertForOtp(){
        let strTitle = ""
        let alertController = UIAlertController(title: strTitle, message: "Please verify mobile number to start using MaximusPro.", preferredStyle: UIAlertControllerStyle.alert)
        let yesAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("OK")
            MyDefaults.setData(value: fromViewcontroller.HomeVC.rawValue, key: DefaultKeys.KEY_FROMWHERE)
            let navigationController = UINavigationController(rootViewController: PhoneVerificationVC())
            self.presentVC(navigationController)
        }
        alertController.addAction(yesAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func showAlert(){
        //Show Alert for CM registration
        let strTitle = "Your MaximusPro is not registered. To register, please pair Display Module with your phone by scanning the QR code on the Display Module. Make sure Bluetooth of your phone is turned ON"
        let alertController = UIAlertController(title: strTitle, message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        let yesAction = UIAlertAction(title: "YES", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("YES")
            
            let navigationController = UINavigationController(rootViewController: DeviceSetUpVC())
            self.presentVC(navigationController)
        }
        
        let laterAction = UIAlertAction(title: "LATER", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("LATER")
        }
        let buyAction = UIAlertAction(title: "BUY NOW", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("buy Now")
            if let amazonUrl = URL(string: NSLocalizedString("keyAmazonUrl", comment: "")) {
                UIApplication.shared.open(amazonUrl, options: [:]) { (status) in
                    print("Open amazon with status \(status)")
                }
            }
        }
        //buyAction.isEnabled = false
        alertController.addAction(yesAction)
        alertController.addAction(laterAction)
        alertController.addAction(buyAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    func enableStartButton(){
        DispatchQueue.main.async {
            self.view.hideToastActivity()
        }
        self.btnStart.setTitle("START", for: .normal)
        self.btnStart.tag = 100
        progressView?.progressImage = UIImage(named: "Start Green Line")
    }
    
    func enableStopButton(){
        DispatchQueue.main.async {
            self.view.hideToastActivity()
            
            self.btnStart.setTitle("STOP", for: .normal)
            self.btnStart.tag = 200
            self.btnStart.addLongPressGesture(target: self, action: #selector(self.longTap))
            self.progressView?.progressImage = UIImage(named: "Progress Bar")
        }
    }

    
    
    func showLocalStats() {
        if let  rideId = self.rideDetails?.rideID?.toInt() , rideId == journeyManager.currentRideId() {
            
            if let dist = self.currentRideStats?.distanceTravelled {
                let dist_km =  dist
                self.lblDistance.text =  String(format: "%.1f", arguments: [Double(dist_km)/1000])
            }
            if let time = self.currentRideStats?.travelledTime {
                let mins = (time / 60).truncatingRemainder(dividingBy: 60)
                let hrs = time / 3600
                    
                    var strMin:String = "\(Int(mins))"
                    var strHrs:String = "\(Int(hrs))"
                    
                    if mins < 10 {
                        strMin = "0\(Int(mins))"
                    }
                    
                    if hrs < 10 {
                        strHrs = "0\(Int(hrs))"
                    }
                    self.lblTime.text = strHrs + ":" + strMin
            }
            
                    let avgSpeed = self.currentRideStats?.averageSpeed ?? 00//offlineStats.averageSpeed
                    let avgSpeed_kmph = Double(avgSpeed) * Double(3.6)
            
                if !(avgSpeed_kmph.isNaN || avgSpeed_kmph.isInfinite)  {
                
                    self.lbLAvgSpeed.text = "\(Int(avgSpeed_kmph))"
                }
                    
                    let srcMArker = GMSMarker()
                    let destMarker = GMSMarker()
            
                    let locs = currentRideStats?.rideLocations ?? []
                
//                    self.mapView.clear()
                    self.mapView.drawPolylinewith(Locations: locs, sourceMArker: srcMArker, destMarker: destMarker)
                    if locs.count > 1 {
                        rideBounds = GMSCoordinateBounds()
                        for location in locs {
                            rideBounds = rideBounds?.includingCoordinate(location.coordinate)
                        }
                        if let _ = rideBounds {
                            self.mapView.animate(with: GMSCameraUpdate.fit(rideBounds!))
                        }
                    }
            
        }else{
            print("Not Current Ride")
        }
        
    }
    
    
    //MARK: Long Tap gesture call
    func longTap(sender : UIGestureRecognizer){
        
        if sender.state == .ended {
            // Call when Release long Tap press button
            timer?.invalidate()
            if(progressView?.progress == 1.0){
                DispatchQueue.main.async {
                    self.progressView?.setProgress(0.0, animated: false)
                }
                if(self.btnStart.tag == 100){
                    // check whether ride start point is away from current location
                    if !CLLocationManager.locationServicesEnabled(){
                        self.view.makeToast(NSLocalizedString("keyEnableLocationServices", comment: ""))
                    }
                    self.showLegalAlert()
                   
                    
                }else{
                    //self.journeyManager.stopRide(rideId: (self.rideDetails?.rideID?.toInt()) ?? 0)journeyManager
                        currentRideViewModel.delegate = self

                        self.rideDetails = JourneyManager.sharedInstance.currentRideDetails()
                        if self.rideDetails != nil {
                            currentRideViewModel.stopRide(rideID: (self.rideDetails?.rideID?.toInt())!)
                        }
                }
            }else{
                DispatchQueue.main.async {
                    self.progressView?.setProgress(0.0, animated: true)
                }
            }
        }
        // Call when Begin long Tap press button
        if sender.state == .began {
            if (self.btnStart.tag == 100){
                let isCMRegisterd = MyDefaults.getBoolData(key: DefaultKeys.KEY_CMREGISTER)//-----Get CM Registration Status
                
                if(isCMRegisterd){
                    let isOtpVerified = self.bikerDetails.is_otp_verified //MyDefaults.getBoolData(key: DefaultKeys.KEY_OTPVERIFIED)//-----Get OTP Verification Status
                    if isOtpVerified || MyDefaults.getBoolData(key: DefaultKeys.KEY_OTPVERIFIED){
                        if(compareTodaysDateWithRideStartDate()){
                            DispatchQueue.main.async {
                                self.progressView?.setProgress(0.0, animated: true)
                            }
                            timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(self.updateProgress), userInfo: nil, repeats: true)
                        }else{
                            self.view.hideToastActivity()
                            self.view.makeToast("You can start ride on \(self.strRideDate) !!")
                        }
                    }else{
                        self.showAlertForOtp()
                    }
                }else{
                    self.showAlert()
                }
                
            }else{
                DispatchQueue.main.async {
                    self.progressView?.setProgress(0.0, animated: true)
                }
                timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(self.updateProgress), userInfo: nil, repeats: true)
            }
        }
    }
    
    func journeySetup(rideDetails: Rides){
        if rideDetails.planned == true && !journeyManager.isQuickRide {
            if !journeyManager.isAnyRideInProgress() {
                MaximusDM.sharedInstance.journeyStartSetUp(rideDetailsObj: rideDetails, totalDuration: Int(rideDetails.total_ride_distance ?? "") ?? 0, totalTime: Int(rideDetails.total_ride_time ?? "") ?? 0)
            }
        }
    }
    
    func setRideStartButton() {
        if (rideDetails != nil) {
            let todaysDays = NSDate()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM d, yyyy"
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            let strTodaysDate = dateFormatter.string(from: todaysDays as Date)
            
            let rideDate = rideDetails?.start_date?.getFormatedDate()
            self.strRideDate = dateFormatter.string(from: rideDate!)
            let rideEndDate = dateFormatter.string(from: (rideDetails?.finish_date?.getFormatedDate())!)
            
            let dateRideStartDate = dateFormatter.date(from: strRideDate)! as Date
            let dateTodaysDate = dateFormatter.date(from: strTodaysDate)! as Date
            let dateRideEndDate = dateFormatter.date(from: rideEndDate)! as Date
            
            if( dateTodaysDate >= dateRideStartDate  && dateTodaysDate <= dateRideEndDate ){
                //todays ride
                self.btnStart.setTitle("START", for: .normal)
                self.journeySetup(rideDetails: self.rideDetails!)
                self.btnStart.isEnabled = true
                getRideStatus()
            }
            else if (dateTodaysDate   > dateRideEndDate) {
                //past ride
                let startButtonRide:String = "RIDE EXPIRED"
                self.btnStart.setTitle(startButtonRide, for: .normal)
                
                self.btnStart.isEnabled = false
            }else
            {
                //future ride
                self.btnStart.isEnabled = false
                let hours = (rideDate?.hours(from: todaysDays as Date))! % 24
                let days = rideDate?.days(from:todaysDays as Date)
                
                var remainingTime = ""
                if days! >= 1{
                    if(days == 1)
                    {
                        remainingTime = remainingTime + "\(days!) DAY "
                    }
                    else
                    {
                        remainingTime = remainingTime + "\(days!) DAYS "
                    }
                }
                
                if days! < 1  {
                    
                    if hours > 0 {
                        remainingTime = remainingTime + "\(hours) HRS "
                    }
                    else
                    {
                        remainingTime = "LESS THAN AN HOUR"
                    }
                }
                self.btnStart.setTitle(remainingTime + "TO GO", for: .normal)
            }
        }
    }
    
    func compareTodaysDateWithRideStartDate() -> Bool{
        let todaysDays = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let strTodaysDate = dateFormatter.string(from: todaysDays as Date)
        
        let rideDate = rideDetails?.start_date?.getFormatedDate()
        self.strRideDate = dateFormatter.string(from: rideDate!)
        
        let dateRideStartDate = dateFormatter.date(from: strRideDate)! as Date
        let dateTodaysDate = dateFormatter.date(from: strTodaysDate)! as Date
        
        if(dateTodaysDate >= dateRideStartDate){
            return true
        }else{
            return false
        }
    }
    
    //MARK:- Add Progress Bar Progress
    func addControls() {
        // Create Progress View Control
        progressView = UIProgressView(progressViewStyle: UIProgressViewStyle.default)
        //progressView?.center = self.view.center
        progressView?.frame = CGRect(x: 0, y: self.btnStart.frame.origin.y, width: self.view.frame.width, height: 10)
        progressView?.progressImage = UIImage(named: "Start Green Line")
        progressView!.trackTintColor = UIColor.clear
        progressView?.barHeight = 6
        view.addSubview(progressView!)
    }
    
    //Update progress bar
    func updateProgress() {
        DispatchQueue.main.async {
            self.progressView?.progress += 0.01
            self.progressView?.setProgress((self.progressView?.progress)!, animated: true)
        }
    }
    
    func addObserverOnView(){
        print("*******************Observer Added****************")
        self.removeObserverOnView()//--Remove observer before adding if any, also if not any observer added before then it also fine to call that remove method
        // add notification observers
        NotificationCenter.default.addObserver(self, selector: #selector(appEntersForeGround), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    func removeObserverOnView(){
        print("*******************Observer Removed****************")
        // Remove notification observers
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    //MARK:- AlertAPIManager Delegates
    func alertRespondSuccess(message: String) {
        if message == "Done"{
            vwOptOutStrtNav.isHidden = true
        }else{
            
            let alert = UIAlertController(title: "MaximusPro", message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action in
                self.vwOptOutStrtNav.isHidden = true
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}


