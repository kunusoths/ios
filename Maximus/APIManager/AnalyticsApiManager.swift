//
//  RecommendedApiManager.swift
//  Maximus
//
//  Created by APPLE on 01/10/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import ObjectMapper

typealias successData = (Any) -> Void
typealias errorData = ([String: Any]) -> Void
/*
 Recommendation API callback protocol
 */


class AnalyticsApiManager: NSObject {
    var cloudHandlerObj = CloudHandler()
    /*
     Get Recommended rides for biker with pagination
     @param: BikerID : Int, RequestType: String, pageNo: Int, size: Int
     */
    func getRecommendedRides(bikerID: Int, requestType: String, pageNo: Int, size: Int, successCallback: @escaping successData, failureCallback:@escaping errorData) {
        let suburl = "\(RequestURL.URL_ANALYTICS_BIKERS.rawValue)\(bikerID)/\(RequestURL.URL_FEED.rawValue)?page=\(pageNo)&size=\(size)&sort=score,desc"
        
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get) { (cachedResponse) in
            if let response = cachedResponse, let data = response.data {
                let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                if let statusCode = (response.response?.statusCode) {
                    if statusCode >= 200 && statusCode < 300
                    {
                        let parsedData = self.sendSuccessCallback(jsonString: jsonString ?? "", requestType: requestType)
                        successCallback(parsedData)
                    }else{
                        let parsedErrData = self.sendFailureCallback(jsonString: jsonString ?? "")
                        failureCallback(parsedErrData)
                    }
                } else {
                    let parsedErrData = self.sendFailureCallback(jsonString: jsonString ?? "")
                    failureCallback(parsedErrData)
                }
            }else{
                failureCallback(RequestTimeOut().json)
            }
        }
    }
    
    func sendSuccessCallback(jsonString: NSString, requestType: String) -> [String: Any] {
        let user = Mapper<RecommendedResponse>().mapArray(JSONString: jsonString as String)
        var data:[String:Any] = [StringConstant.data:user ?? ""]
        data["RequestType"] = requestType
        return data
    }
    
    func sendFailureCallback(jsonString: NSString) -> [String: Any] {
        let ErrorModelObj = Mapper<ErrorModel>().map(JSONString: jsonString as String)
        let errorData:[String:Any] = ["data":ErrorModelObj ?? " "]
        return errorData
    }
}
