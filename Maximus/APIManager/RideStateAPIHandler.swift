//
//  RideStateAPIHandler.swift
//  Maximus
//
//  Created by Abhilash Ghogale on 16/07/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import Foundation
import ObjectMapper

enum RideStateAPIType:String {
    case NONE = "None"
    case RIDE_START = "RideStart"
    case RIDE_STOP = "RideStop"
    case RIDE_IN_PROGRESS = "RideInProgress"
}

protocol RideStateAPIHandlerDelegate:class {
    //
    func rideStateAPISuccess(response:[String:Any])
    func rideStateAPIFailed(response:[String:Any])
}

class RideStateAPIHandler: NSObject {
    
    let cloudHandler = CloudHandler()
    weak var delegate:RideStateAPIHandlerDelegate?
    
    override init() {
        
    }
    
    deinit {
        
    }
    
    func apiStartRide(type: RideType ,rideID: Int, parameter:[String:Any]) {
        print(" Start ride req param \(parameter)")
        //TODO:need to put dynamic rideid, bikerid
        let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)\(RequestURL.URL_START.rawValue)"
        
        cloudHandler.makeCloudRequest(subUrl: suburl, parameter:parameter, isheaderAUTH: true, requestType: .post){ response in
            
            if let json = response?.result.value {
                print("Start Ride Responce : \(json)")
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    MyDefaults.setInt (value: rideID , key: DefaultKeys.KEY_RIDEID)
                    let user = Mapper<RideStartResponse>().map(JSON:json as! [String : Any])!
                    let data:[String:Any] = [StringConstant.data:user,
                                             "type": RideStateAPIType.RIDE_START,
                                             "rideID": rideID]
                    self.delegate?.rideStateAPISuccess(response: data)
                   // self.rideManagerDelegate?.rideStartSuccess!(data: data)
                }
                else
                {
                    //self.errorCall(value: json as! [String : Any])
                    let data: [String: Any] = [StringConstant.data: json as! [String: Any],
                                               "type": RequestType.Start,
                                               "rideID": rideID]
                    self.delegate?.rideStateAPIFailed(response: data) //error: (json as? [String: Any])!
                }
            }else{
                self.delegate?.rideStateAPIFailed(response: RequestTimeOut().json)
            }
        }
    }
    
    func apiGetProgreesRide(bikerID:Int)
    {
        //TODO:need to put dynamic rideid
        //let context = Constants.appDelegate?.persistentContainer.newBackgroundContext()
        //let bikerID = (BikerDetails.getBikerDetails(context: context!)?.biker_id?.toInt()) ?? 0
        
        let suburl = "\(RequestURL.URL_RIDE_BIKER.rawValue)\(bikerID)\(RequestURL.URL_CURENT_RIDEPROGRES.rawValue)"
        self.cloudHandler.makeCloudRequest(subUrl: suburl, parameter:["":""], isheaderAUTH: true, requestType: .get){ response in
            
            if let json = response?.result.value {

                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    print("Print Get Progress Ride Responce: \(json)")
                    let json1 = [StringConstant.rideInprogresList:json]
                    let user = Mapper<RideInprogressModel>().map(JSON:json1)!
                    let data:[String:Any] = [StringConstant.data:user,
                                             "type": RideStateAPIType.RIDE_IN_PROGRESS,
                                             "rideID":0]
                    self.delegate?.rideStateAPISuccess(response: data)
                    //self.rideManagerDelegate?.getRideProgressSuccess!(data: data)
                }
                else{
                    let data:[String:Any] = [StringConstant.data:json as! [String : Any],
                                             "type": RideStateAPIType.RIDE_IN_PROGRESS,
                                             "rideID":0]
                    self.delegate?.rideStateAPIFailed(response: data)
                }
            }else{
                print(response?.result.error ?? " ")
                self.delegate?.rideStateAPIFailed(response: RequestTimeOut().json)
            }
        }
    }
    
    func apiStopRide(rideID:Int, parameter:[String:Any]){
        
        //TODO:need to put dynamic rideid, bikerid
        let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)\(RequestURL.URL_STOP.rawValue)"
        self.cloudHandler.makeCloudRequest(subUrl: suburl, parameter:parameter, isheaderAUTH: true, requestType: .post){ response in
            print(response ?? "")
            if let json = response?.result.value {
                print("Stop Ride Responce : \(json)")

                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let user = Mapper<RideStartResponse>().map(JSON:json as! [String : Any])!
                    let data:[String:Any] = [StringConstant.data:user,
                                             "type": RideStateAPIType.RIDE_STOP,
                                             "rideID": rideID]
                    self.delegate?.rideStateAPISuccess(response: data)
                    //self.rideManagerDelegate?.rideStopSuccess!(data: data)
                }
                else{
                    let data: [String: Any] = [StringConstant.data: json as! [String: Any],
                                               "type": RequestType.Stop,
                                               "rideID": rideID]
                    self.delegate?.rideStateAPIFailed(response: data)
                }
            }else{
                self.delegate?.rideStateAPIFailed(response: RequestTimeOut().json)
            }
        }
    }
    
    
}
