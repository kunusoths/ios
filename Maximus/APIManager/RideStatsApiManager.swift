//
//  RideStatsApiManager.swift
//  Maximus
//
//  Created by Isha Ramdasi on 16/07/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import ObjectMapper

@objc protocol apiCallback{
    func cloudSuccess(data: [String:Any])
    func cloudFailure(error: [String:Any])
}

class RideStatsApiManager: NSObject {
    static let ridesStatsApiSharedInstance = RideStatsApiManager()
    var cloudHandlerObj = CloudHandler()
    weak var cloudCallbackDelegate:apiCallback?
    
    func getRideLogStats(bikerID:Int, pageNo: Int, size: Int, request: String)
    {
        //TODO:need to put dyamic rideid
        let suburl = "\(RequestURL.URL_ANALYTICS_BIKERS.rawValue)\(bikerID)/ride_stats?page=\(pageNo)&size=\(size)&sort=id,desc"
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get) { (response) in
            if let json = response?.result.value {
                print("Ride log Responce \(json)")

                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let jsonString = NSString(data: (response?.data)!, encoding: String.Encoding.utf8.rawValue)
                    let user = Mapper<RideLogModel>().mapArray(JSONString: jsonString! as String)
                    var data:[String:Any] = [StringConstant.data:user!]
                    data["RequestType"] = request
                    self.cloudCallbackDelegate?.cloudSuccess(data: data)
                }
                else
                {
                    self.cloudCallbackDelegate?.cloudFailure(error: (json as? [String: Any])!)
                }
            } else{
                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
            }
        }
    }
    
    
    func putRideTitle(parameter:[String:Any], request: String){
        let subUrl = "/rides"
        print("put request json", parameter)
        self.cloudHandlerObj.makeCloudRequest(subUrl: subUrl, parameter: parameter, isheaderAUTH: true, requestType: .put){
            response in
            
            if let json = response?.result.value {
                
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let user = Mapper<RideCreateResponse>().map(JSON:json as! [String : Any])!
                    var data:[String:Any] = [StringConstant.data:user]
                    data["RequestType"] = request
                    self.cloudCallbackDelegate?.cloudSuccess(data: data)
                }
                else
                {
                    self.cloudCallbackDelegate?.cloudFailure(error: (json as? [String: Any])!)
                }
            }else{
                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
            }
        }
    }
    
    func getRideStats(rideID:Int, bikerID:Int, request: String)
    {
        let suburl = "\(RequestURL.URL_RIDE_STATUS.rawValue)\(rideID)\(RequestURL.URL_BIKER.rawValue)\(bikerID)\(RequestURL.URL_STATS.rawValue)"
        
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ response in
            if let json = response?.result.value {
                print("Ride log Responce \(json)")
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let user = Mapper<RideStatisticsModel>().map(JSON:json as! [String : Any])!
                    var data:[String:Any] = [StringConstant.data:user]
                    data["RequestType"] = request
                    self.cloudCallbackDelegate?.cloudSuccess(data: data)
                }
                else{
                    self.cloudCallbackDelegate?.cloudFailure(error: (json as? [String: Any])!)
                }
            }else{
                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
            }
        }
    }
    
    func getRideRoute(rideID:Int, bikerID:Int, request: String)
    {
        let suburl = "\(RequestURL.URL_RIDE_STATUS.rawValue)\(rideID)\(RequestURL.URL_BIKER.rawValue)\(bikerID)\(RequestURL.URL_ROUTE.rawValue)"
        
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ cachedResponse in
            
            if let response = cachedResponse {
                
                let jsonString = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                
                if (response.response?.statusCode)! >= 200 && (response.response?.statusCode)! < 300
                {
                    let user = Mapper<RouteLatLngModel>().map(JSONString: jsonString! as String)
                    var data:[String:Any] = [StringConstant.data:user!]
                    data["RequestType"] = request
                    self.cloudCallbackDelegate?.cloudSuccess(data: data)
                }else{
                    
                    let ErrorModelObj = Mapper<ErrorModel>().map(JSONString: jsonString! as String)
                    var errorData:[String:Any] = ["data":ErrorModelObj ?? " "]
                    errorData["RequestType"] = request
                    self.cloudCallbackDelegate?.cloudFailure(error: errorData)
                }
            }else{
                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
            }
        }
    }
    
    func getGraphSpeedPoints(rideID:Int, bikerID:Int, request: String)
    {
        let suburl = "\(RequestURL.URL_RIDE_STATUS.rawValue)\(rideID)\(RequestURL.URL_BIKER.rawValue)\(bikerID)\(RequestURL.URL_SPEEDPOINT.rawValue)"
        
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ cachedResponse in
            
            if let response = cachedResponse {
                let jsonString = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                
                if (response.response?.statusCode)! >= 200 && (response.response?.statusCode)! < 300
                {
                    let user = Mapper<SpeedPointsModel>().map(JSONString: jsonString! as String)
                    var data:[String:Any] = [StringConstant.data:user!]
                    data["RequestType"] = request
                    self.cloudCallbackDelegate?.cloudSuccess(data: data)
                }else{
                    
                    let ErrorModelObj = Mapper<ErrorModel>().map(JSONString: jsonString! as String)
                    let errorData:[String:Any] = ["data":ErrorModelObj ?? " "]
                    self.cloudCallbackDelegate?.cloudFailure(error: errorData)
                }
            }else{
                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
            }
        }
    }
    
    func getStickerBoard(bikerID:Int, requestType: String)
    {
        //TODO:need to put dynamic rideid
        let suburl = "\(RequestURL.URL_ANALYTICS_BIKERS.rawValue)\(bikerID)\(RequestURL.URL_STICKERBOARD.rawValue)"
        self.cloudHandlerObj.makeCloudGetWithCacheRequest(subUrl: suburl, parameter:["":""], isheaderAUTH: true, requestType: .get){ cachedResponse, statusCode in
            
            if let response = cachedResponse {
                
                let jsonString = NSString(data: response.data, encoding: String.Encoding.utf8.rawValue)
                if statusCode! >= 200 && statusCode! < 300
                {
                    let user = Mapper<StickerBoardResponse>().map(JSONString: jsonString as! String)
                    var data:[String:Any] = [StringConstant.data:user!]
                    data["RequestType"] = requestType
                    self.cloudCallbackDelegate?.cloudSuccess(data: data)
                }else{
                    let ErrorModelObj = Mapper<ErrorModel>().map(JSONString: jsonString! as String)
                    let errorData:[String:Any] = ["data":ErrorModelObj ?? " "]
                    self.cloudCallbackDelegate?.cloudFailure(error: errorData)
                }
            } else {
                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
            }
        }
    }
}
