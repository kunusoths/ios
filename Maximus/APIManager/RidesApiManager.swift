//
//  RidesApiManager.swift
//  Maximus
//
//  Created by Isha Ramdasi on 26/06/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire

//@objc protocol CloudCallback{
//    func cloudSuccess(data: [String:Any])
//    func cloudFailure(error: [String:Any])
//    @objc optional func cachingPOISuccess()
//}

class RidesApiManager: NSObject {
//    static let ridesApiSharedInstance = RidesApiManager()
    var cloudHandlerObj = CloudHandler()
   // weak var cloudCallbackDelegate:CloudCallback?
    
    func getMyPlannedRides(type:String, pageNo: Int, size: Int, bikerID: String, request: String, successCallback: @escaping successData, failureCallback: @escaping errorData)
    {
        let pageCount1 = pageNo == 10000 ? 0 : pageNo
        let size1 = size == 0 ? 10000 : size
        var suburl = ""
        if type == "RECOMMENDED" {
            suburl = "\(RequestURL.URL_RIDE_BIKER.rawValue)\(Int(bikerID) ?? 0)\(RequestURL.URL_RECOMMENDED.rawValue)"
        } else {
            suburl = "\(RequestURL.URL_RIDE_BIKER.rawValue)\(Int(bikerID) ?? 0)/joined?page=\(pageCount1)&size=\(size1)&sort=startDate,asc"
        }
        
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get) { cachedResponse in
            
            if let response = cachedResponse {
                let jsonString = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                print("My rides resp \(jsonString ?? "")")
                if let status = response.response?.statusCode {
                    if  (status >= 200 && status < 300)
                    {
//                        if (user?.count)! > 0 {
                           // self.cloudCallbackDelegate?.cloudSuccess(data: data)
//                        }
                        successCallback(jsonString!)
                    }else{
                        let ErrorModelObj = Mapper<ErrorModel>().map(JSONString: jsonString! as String)
                        let errorData:[String:Any] = ["data":ErrorModelObj ?? " "]
                        failureCallback(errorData)
//                        self.cloudCallbackDelegate?.cloudFailure(error: errorData)
                    }
                }
            }else{
                failureCallback(RequestTimeOut().json)
//                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
            }
          }
    }
    
    func getRideDetails(rideID: Int, bikerID: Int, type: String, successCallback: @escaping successData, failureCallback:@escaping errorData) {
        var suburl = ""
        if (bikerID == 0) {
            suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)"
        }else{
            suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)\(RequestURL.URL_BIKER.rawValue)\(bikerID)"
        }
        
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get, completionHandler: { response in
            if let json = response?.result.value {
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    successCallback(json as! [String: Any])
//                    self.cloudCallbackDelegate?.cloudSuccess(data: directionDetailsData)
                } else {
                    failureCallback((json as? [String: Any])!)
//                    self.cloudCallbackDelegate?.cloudFailure(error: (json as? [String: Any])!)
                }
            } else {
                failureCallback(RequestTimeOut().json)
//                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
            }
        })
    }
    
    func leaveRide(rideID: Int, request: String, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)\(RequestURL.URL_LEAVE_RIDE.rawValue)"
        
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .post){ response in
            if let json = response?.result.value {
                print("leave ride REsponse: \(json) ")
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    successCallback([:])
//                    self.cloudCallbackDelegate?.cloudSuccess(data: data)
                }
                else{
                    failureCallback(json as? [String: Any] ?? [:])
//                    self.cloudCallbackDelegate?.cloudFailure(error: data)
                }
            }else{
//                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
                failureCallback(RequestTimeOut().json)
            }
        }
    }
    
    func cloneRide(rideID:Int,data:[String:Any], request: String, successCallback: @escaping successData, failureCallback: @escaping errorData)  {
        
        let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)/clone"
        
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: data, isheaderAUTH: true, requestType: .post){ response in
            if let json = response?.result.value {
                print("JSON ",json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let cloneRide = Mapper<RideCreateResponse>().map(JSON:json as! [String : Any])!
                    
                    if(cloneRide.planned)!{
                        MyDefaults.setInt(value: cloneRide.id!, key: DefaultKeys.KEY_POI_RIDEID)
                        self.getPOI(rideID: cloneRide.id!,isInsertintoDB: true, request: request, successCallback: {responseData in
                            
                        }, failureCallback: {failureData in
                            
                        })
                    }
                    successCallback(json as? [String: Any] ?? [:])
                    //self.cloudCallbackDelegate?.cloudSuccess(data: data)
                }
                else{
//                    self.cloudCallbackDelegate?.cloudFailure(error: (json as? [String: Any])!)
                    failureCallback(json as? [String: Any] ?? [:])
                }
            }else{
//                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
                failureCallback(RequestTimeOut().json)
            }
        }
        
    }
    
    func getRide(rideID:Int,request: String, successCallback: @escaping successData, failureCallback: @escaping errorData)  {
        
        let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)"
        
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ response in
            if let json = response?.result.value {
                print("JSON ",json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    
                    //self.cloudCallbackDelegate?.cloudSuccess(data: data)
                    successCallback(json as? [String: Any] ?? [:])
                }
                else{
                    //self.cloudCallbackDelegate?.cloudFailure(error: (json as? [String: Any])!)
                    failureCallback(json as? [String: Any] ?? [:])
                }
            }else{
//                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
                failureCallback(RequestTimeOut().json)
            }
        }
        
    }
    
    func getPOI(rideID:Int,isInsertintoDB:Bool, request: String, successCallback: @escaping successData, failureCallback: @escaping errorData)  {
        DispatchQueue.global(qos: .background).async {
            let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)\(RequestURL.URL_POI.rawValue)"
            self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ response in
                if let json = response?.result.value {
                    print("poi response", json)
                    if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                    {
                        let json1 = [StringConstant.getPOIList:json]
                        let poi = Mapper<GetRidePOIModel>().map(JSON:json1 )!
                        let data:[String:Any] = [StringConstant.data:poi]
                        MyDefaults.setInt(value: rideID, key: DefaultKeys.KEY_POI_RIDEID)
                        if isInsertintoDB{
                            let rideManager = RideManager()
                            rideManager.insertPOIintoDB(data: data, poi_rideID: rideID)
                        }
                        if request != "CreateRide"{
                            //self.cloudCallbackDelegate?.cachingPOISuccess!()
                            successCallback(Data())
                        }
                    }
                    else
                    {
                        failureCallback(json as? [String: Any] ?? [:])
//                        self.cloudCallbackDelegate?.cloudFailure(error: (json as? [String: Any])!)
                    }
                }else{
//                    self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
                    failureCallback(RequestTimeOut().json)
                }
            }
        }
    }
    
    func getRideMembers(rideID:Int, request: String, successCallback: @escaping successData, failureCallback: @escaping errorData)  {
        
        let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)\(RequestURL.URL_BIKER.rawValue)\(RequestURL.URL_SUMMARY.rawValue)"
        cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get) { (response) in
            if let json = response?.result.value {
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    if let data = response?.data, let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                        successCallback(jsonString)
                        //self.cloudCallbackDelegate?.cloudSuccess(data: data)
                    }
                }
                else
                {
//                    self.cloudCallbackDelegate?.cloudFailure(error: (json as? [String: Any])!)
                    failureCallback(json as? [String: Any] ?? [:])
                }
            } else{
                failureCallback(RequestTimeOut().json)
//                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
        }
        }
    }
    
    /*
    func getCurrentDeviceLocation(bikerID:Int, request: String)
    {
        let suburl = "\(RequestURL.URL_ANALYTICS_BIKERS.rawValue)\(bikerID)\(RequestURL.URL_DEVICE_DATA.rawValue)"
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter:["":""], isheaderAUTH: true, requestType: .get){ response in
            
            if let json = response?.result.value {
                print(json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let user = Mapper<DeviceDataResponse>().map(JSON:json as! [String : Any])!
                    var data:[String:Any] = [StringConstant.data:user]
                    data["RequestType"] = request
                    self.cloudCallbackDelegate?.cloudSuccess(data: data)
                }
                else {
                    self.cloudCallbackDelegate?.cloudFailure(error: (json as? [String: Any])!)
                }
            }else{
                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
            }
        }
    }*/
    
    func getRoutesForLatLng(data:[String:Any], request: String, successCallback: @escaping successData, failureCallback:@escaping errorData)
    {
        let suburl = "\(RequestURL.URL_ROUTING.rawValue)"
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: data, isheaderAUTH: false, requestType: .post)
        { (response) in
            
            if let json = response?.result.value {
                print(json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300{
//                    self.cloudCallbackDelegate?.cloudSuccess(data: directionDetailsData)
                    successCallback((json as? [String : Any] ?? [:]))

                }else{
//                    self.cloudCallbackDelegate?.cloudFailure(error: (json as? [String: Any])!)
                    failureCallback((json as? [String: Any])!)
                }
            }else{
//                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
                failureCallback(RequestTimeOut().json)
            }
        }
    }
    
    func getPOIWithPolyline(polyline: String, request: String, successCallback: @escaping successData, failureCallback:@escaping errorData) {
        
        let suburl = "\(RequestURL.URL_LOCATION.rawValue)\(RequestURL.URL_POI_POLYLINE.rawValue)?polyline=\(polyline.urlEncoded())&radius=1000"
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter:[:],isheaderAUTH: true, requestType: .get) { response in
            if let json = response?.result.value {
                //  print(json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
//                    self.cloudCallbackDelegate?.cloudSuccess(data: data)
                    successCallback(json)
                }
                else{
//                    self.cloudCallbackDelegate?.cloudFailure(error: (json as? [String: Any])!)
                    failureCallback((json as? [String: Any])!)
                }
            }else{
//                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
                failureCallback(RequestTimeOut().json)
            }
        }
    }
    
    func getPOIDetails(poiId:String, request: String, successCallback: @escaping successData, failureCallback:@escaping errorData)
    {
        let suburl = Constants.apiKeyGooglePOIDetailsURL+poiId+Constants.apiKeyGooglePOIDetailsURLPART2+Constants.APIKEY_MAPS!
        
        self.cloudHandlerObj.makeAccuWeatherCloudRequest(subUrl: suburl, parameter:["":""], isheaderAUTH: false, requestType: .get){ response in
            
            if let json = response?.result.value {
                //print(json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
//                    self.cloudCallbackDelegate?.cloudSuccess(data: data)
                    successCallback((json as! [String : Any]))
                }
                else {
//                    self.cloudCallbackDelegate?.cloudFailure(error: (json as? [String: Any])!)
                    failureCallback((json as? [String: Any])!)
                }
            }else{
//                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
                failureCallback(RequestTimeOut().json)
            }
        }
    }
    
    func getPlannedWavepoints(rideID:Int, request: String, successCallback: @escaping successData, failureCallback:@escaping errorData)  {
        
        let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)\(RequestURL.URL_PlANNED_WAVEPOINTS.rawValue)"
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get){ response in
            if let json = response?.result.value {
                //  print(json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
//                    self.cloudCallbackDelegate?.cloudSuccess(data: data)
                    successCallback(json as? [String: Any] ?? [:])
                }
                else{
//                    self.cloudCallbackDelegate?.cloudFailure(error: (json as? [String: Any])!)
                    failureCallback((json as? [String: Any])!)
                }
            }else{
//                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
                failureCallback(RequestTimeOut().json)
            }
        }
        
    }
    
    func createRide(parameter: [String:Any], rqstType:HTTPMethod, request: String, successCallback: @escaping successData, failureCallback:@escaping errorData)
    {
        let suburl = "\("/rides")"
        cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: parameter, isheaderAUTH: true, requestType: rqstType){ response in
            
            if let json = response?.result.value {
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    let user = Mapper<RideCreateResponse>().map(JSON:json as! [String : Any])!
                    
                    var data:[String:Any] = [StringConstant.data:user]
                    
                    let createModel:RideCreateResponse = data["data"] as! RideCreateResponse
                    let rideid = createModel.id
                    MyDefaults.setInt(value: rideid!, key: DefaultKeys.KEY_POI_RIDEID)
                    
                    if(createModel.planned)!{
                        self.getPOI(rideID: rideid!,isInsertintoDB: true, request: request, successCallback: {responseData in
                            
                        }, failureCallback: {failureData in
                            
                        })
                    }
                    data["RequestType"] = request
//                    self.cloudCallbackDelegate?.cloudSuccess(data: data)
                    successCallback(json as? [String: Any] ?? [:])
                }
                else
                {
                    let data: [String: Any] = [StringConstant.data: json as! [String: Any],
                                               "type": RequestType.Create]
//                    self.cloudCallbackDelegate?.cloudFailure(error: data)
                    failureCallback(data)
                }
            }else{
//                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
                failureCallback(RequestTimeOut().json)
            }
        }
    }
    
    
    func joinRide(rideID:Int,parameter: [String:Any], requestType: String, successCallback: @escaping successData, failureCallback: @escaping errorData)  {
        
        let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)\(RequestURL.URL_JOIN.rawValue)"
        
        cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter:parameter, isheaderAUTH: true, requestType: .post){ response in
            
            if let json = response?.result.value {
                print("join ride responce \(json)")
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    successCallback(json as? [String: Any] ?? [:])
                   // self.cloudCallbackDelegate?.cloudSuccess(data: data)
                    DispatchQueue.main.async {
                        MyDefaults.setInt(value: rideID, key: DefaultKeys.KEY_POI_RIDEID)
                        self.getPOI(rideID: rideID,isInsertintoDB: true, request: requestType, successCallback: {responseData in
                            
                        }, failureCallback: {failureData in
                            
                        })
                    }
                }
                else
                {
                    var dict:[String: Any] = [:]
                    dict["data"] = json as? [String: Any] ?? [:]
                    dict["RequestType"] = requestType
                    failureCallback(json as? [String:Any] ?? [:])
//                    self.cloudCallbackDelegate?.cloudFailure(error: data)
                }
            }else{
                failureCallback(RequestTimeOut().json)
//                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
            }
        }
    }
    
    func rejectRide(rideID:Int,parameter: [String:Any], requestType: String, successCallback: @escaping successData, failureCallback: @escaping errorData)  {
        
        let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)\(RequestURL.URL_REJECT.rawValue)"
        
        cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter:parameter, isheaderAUTH: true, requestType: .post){ response in
            
            if let json = response?.result.value {
                print("json ",json)
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    successCallback(json as? [String: Any] ?? [:])
                   // self.cloudCallbackDelegate?.cloudSuccess(data: data)                    //                    self.getPOI(rideID: rideID,isInsertintoDB: true)
                    
                }
                else
                {
                    failureCallback((json as? [String: Any])!)
                    //self.cloudCallbackDelegate?.cloudFailure(error: data)
                }
            }else{
                failureCallback(RequestTimeOut().json)
                //self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
            }
        }
    }
    
    func getAllRidesOfBiker(bikerID: Int, requestType: String, pageNo: Int, size: Int, successCallback: @escaping successData, failureCallback: @escaping errorData) {
        
        let pageCount1 = pageNo == 10000 ? 0 : pageNo
        let size1 = size == 0 ? 10000 : size
        
        let suburl = "\(RequestURL.URL_RIDE.rawValue)\(RequestURL.URL_BIKER_MYRIDES.rawValue)\(bikerID)?page=\(pageCount1)&size=\(size1)&sort=planned,startDate,desc"
        cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter:["":""], isheaderAUTH: true, requestType: .get){ response in
            
            if let json = response?.result.value {
                print("My Rides json: \(json)")
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    if let data = response?.data, let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                        successCallback(jsonString)
                    }
                    //self.cloudCallbackDelegate?.cloudSuccess(data: data)
                }
                else
                {
                    failureCallback(json as? [String: Any] ?? [:])
                   // self.cloudCallbackDelegate?.cloudFailure(error: data)
                }
            }else{
                failureCallback(RequestTimeOut().json)
//                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
            }
        }
    }
    
    func getRideOfBiker(rideID: Int, bikerId: Int, requestType: String,successCallback: @escaping successData, failureCallback: @escaping errorData) {
        let suburl = "\(RequestURL.URL_RIDE.rawValue)\(rideID)\(RequestURL.URL_BIKER.rawValue)\(bikerId)"
        
        
        self.cloudHandlerObj.makeCloudRequest(subUrl: suburl, parameter: ["":""], isheaderAUTH: true, requestType: .get, completionHandler: { response in
            if let json = response?.result.value {
                if (response?.response?.statusCode)! >= 200 && (response?.response?.statusCode)! < 300
                {
                    successCallback(json as? [String: Any] ?? [:])
                    //self.cloudCallbackDelegate?.cloudSuccess(data: data)
                } else {
                    failureCallback(json as? [String: Any] ?? [:])
                    //self.cloudCallbackDelegate?.cloudFailure(error: data)
                }
            } else {
                failureCallback(RequestTimeOut().json)
//                self.cloudCallbackDelegate?.cloudFailure(error: RequestTimeOut().json)
            }
        })
        
    }
}
