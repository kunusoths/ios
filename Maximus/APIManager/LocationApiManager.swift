//
//  LocationApiManager.swift
//  Maximus
//
//  Created by APPLE on 03/10/18.
//  Copyright © 2018 com.maximuspro.com. All rights reserved.
//

import UIKit
import ObjectMapper

@objc protocol LocationCloudCallback {
    func locationCloudSuccess(data: [String:Any])
    func locationCloudFailure(error: [String:Any])
}

class LocationApiManager: NSObject {
    weak var locationCloudCallback: LocationCloudCallback?
    var cloudHandler = CloudHandler()
    typealias resultData = ([String : Any]?, [String : Any]?) -> Void
    
    func getCurrentDeviceLocation(bikerID:Int, requestType: String, complition:@escaping resultData)
    {
        let suburl = "\(RequestURL.URL_ANALYTICS_BIKERS.rawValue)\(bikerID)\(RequestURL.URL_DEVICE_DATA.rawValue)"
        self.cloudHandler.makeCloudRequest(subUrl: suburl, parameter:["":""], isheaderAUTH: true, requestType: .get){ response in
            
            if let json = response?.result.value, let statusCode = (response?.response?.statusCode) {
                print(json)
                if statusCode >= 200 && statusCode < 300
                {
                    let user = Mapper<DeviceDataResponse>().map(JSON:json as! [String : Any])!
                    var data:[String:Any] = [StringConstant.data:user]
                    data["RequestType"] = requestType
                    complition(data, nil)
                }
                else {
                    complition(nil, json as? [String: Any])
                }
            }else{
                complition(nil, RequestTimeOut().json)
            }
        }
    }
}
