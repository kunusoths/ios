// AUTOGENERATED FILE - DO NOT MODIFY!
// This file generated by Djinni from interfaces.djinni

#import <Foundation/Foundation.h>


/**
 *##########################################################
 * Environment
 *##########################################################
 */
@interface NSEnvironment : NSObject

+ (nonnull NSString *)getFilesDir;

+ (nonnull NSString *)getCacheFilesDir;

+ (void)setFilesDir:(nonnull NSString *)filesDir;

+ (void)setCacheFilesDir:(nonnull NSString *)cacheFilesDir;

@end
