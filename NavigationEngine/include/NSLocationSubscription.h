// AUTOGENERATED FILE - DO NOT MODIFY!
// This file generated by Djinni from interfaces.djinni

#import <Foundation/Foundation.h>

@interface NSLocationSubscription : NSObject
- (nonnull instancetype)initWithPriority:(int32_t)Priority
                        FrequencyMinimum:(int64_t)FrequencyMinimum
                        FrequencyMaximum:(int64_t)FrequencyMaximum
                      ExpirationDuration:(int64_t)ExpirationDuration
                          ExpirationTime:(int64_t)ExpirationTime
                            Displacement:(double)Displacement;
+ (nonnull instancetype)LocationSubscriptionWithPriority:(int32_t)Priority
                                        FrequencyMinimum:(int64_t)FrequencyMinimum
                                        FrequencyMaximum:(int64_t)FrequencyMaximum
                                      ExpirationDuration:(int64_t)ExpirationDuration
                                          ExpirationTime:(int64_t)ExpirationTime
                                            Displacement:(double)Displacement;

@property (nonatomic, readonly) int32_t Priority;

@property (nonatomic, readonly) int64_t FrequencyMinimum;

@property (nonatomic, readonly) int64_t FrequencyMaximum;

@property (nonatomic, readonly) int64_t ExpirationDuration;

@property (nonatomic, readonly) int64_t ExpirationTime;

@property (nonatomic, readonly) double Displacement;

@end
