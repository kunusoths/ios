// AUTOGENERATED FILE - DO NOT MODIFY!
// This file generated by Djinni from interfaces.djinni

#import "NSDirectionsOptions.h"
#import <Foundation/Foundation.h>
@class NSDirections;
@class NSDirectionsProvider;
@protocol NSDirectionsProviderClient;
@protocol NSNetworkService;


@interface NSDirectionsProvider : NSObject

+ (nonnull NSArray<NSString *> *)availableProviders;

+ (nullable NSDirectionsProvider *)getInstance:(nonnull NSString *)provider
                                           key:(nonnull NSString *)key;

- (nonnull NSString *)getName;

- (nullable id<NSNetworkService>)getNetworkService;

- (void)setNetworkService:(nullable id<NSNetworkService>)networkService;

- (nullable NSDirections *)parse:(nonnull NSString *)directions;

- (void)getDirections:(nonnull NSString *)origin
          destination:(nonnull NSString *)destination
               client:(nullable id<NSDirectionsProviderClient>)client;

- (void)getDirectionsWithOptions:(nonnull NSString *)origin
                     destination:(nonnull NSString *)destination
                         options:(nonnull NSDirectionsOptions *)options
                          client:(nullable id<NSDirectionsProviderClient>)client;

@end
