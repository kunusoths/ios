// AUTOGENERATED FILE - DO NOT MODIFY!
// This file generated by Djinni from interfaces.djinni

#import <Foundation/Foundation.h>

/** Known components */
extern NSString * __nonnull const NSPlaceCITY;
extern NSString * __nonnull const NSPlaceCOUNTY;
extern NSString * __nonnull const NSPlaceSTATE;

@interface NSPlace : NSObject

/** AddressComponents */
- (BOOL)hasAddressComponent:(nonnull NSString *)key;

- (nonnull NSString *)getAddressComponent:(nonnull NSString *)key;

@end
