// AUTOGENERATED FILE - DO NOT MODIFY!
// This file generated by Djinni from interfaces.djinni

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, NSProgressType)
{
    NSProgressTypePROGRESSTYPESTARTED,
    NSProgressTypePROGRESSTYPEINTERMEDIATE,
    NSProgressTypePROGRESSTYPECOMPLETED,
    /** Added May 2018 */
    NSProgressTypePROGRESSTYPESKIPPED,
};
