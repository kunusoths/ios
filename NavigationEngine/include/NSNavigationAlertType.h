// AUTOGENERATED FILE - DO NOT MODIFY!
// This file generated by Djinni from interfaces.djinni

#import <Foundation/Foundation.h>

/**
 *##########################################################
 * Navigation Alerts
 *##########################################################
 */
typedef NS_ENUM(NSInteger, NSNavigationAlertType)
{
    NSNavigationAlertTypeNAVIGATIONALERTTYPESHARPBENDAHEAD,
    NSNavigationAlertTypeNAVIGATIONALERTTYPEWINDINGROADAHEAD,
    NSNavigationAlertTypeNAVIGATIONALERTTYPEWINDINGROADBEGIN,
    NSNavigationAlertTypeNAVIGATIONALERTTYPEWINDINGROADEND,
};
