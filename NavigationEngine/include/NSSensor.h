// AUTOGENERATED FILE - DO NOT MODIFY!
// This file generated by Djinni from interfaces.djinni

#import "NSSensorReportMode.h"
#import "NSSensorStatus.h"
#import "NSSensorType.h"
#import <Foundation/Foundation.h>

@interface NSSensor : NSObject
- (nonnull instancetype)initWithId:(double)Id
                              Name:(nonnull NSString *)Name
                              Unit:(nonnull NSString *)Unit
                              Type:(NSSensorType)Type
                            Status:(NSSensorStatus)Status
                              Mode:(NSSensorReportMode)Mode;
+ (nonnull instancetype)SensorWithId:(double)Id
                                Name:(nonnull NSString *)Name
                                Unit:(nonnull NSString *)Unit
                                Type:(NSSensorType)Type
                              Status:(NSSensorStatus)Status
                                Mode:(NSSensorReportMode)Mode;

@property (nonatomic, readonly) double Id;

@property (nonatomic, readonly, nonnull) NSString * Name;

@property (nonatomic, readonly, nonnull) NSString * Unit;

@property (nonatomic, readonly) NSSensorType Type;

@property (nonatomic, readonly) NSSensorStatus Status;

@property (nonatomic, readonly) NSSensorReportMode Mode;

@end
