// AUTOGENERATED FILE - DO NOT MODIFY!
// This file generated by Djinni from interfaces.djinni

#import <Foundation/Foundation.h>
@class NSSpeechManager;
@protocol NSSpeechService;
@protocol NSUtterance;
@protocol NSUtteranceSource;


@interface NSSpeechManager : NSObject

/** Constructor */
+ (nullable NSSpeechManager *)getInstance;

/** Configuration */
- (void)setSpeechService:(nullable id<NSSpeechService>)service;

/** Add the source for text information */
- (nonnull NSString *)add:(nullable id<NSUtteranceSource>)source;

- (void)remove:(nonnull NSString *)token;

/** Text-to-speech interface */
- (void)speak:(nullable id<NSUtterance>)utterance;

- (void)stop:(nullable id<NSUtterance>)utterance;

/** Enables/disables speech manager */
- (void)setEnabled:(BOOL)flag;

- (BOOL)getEnabled;

/** Reset. Stops activity and purges queues */
- (void)reset;

@end
